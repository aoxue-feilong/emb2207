#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char const *argv[])
{
    time_t seconds; 
    struct tm *tmp ; 
    long  line = 0 ; 
    char buf[1024] = {0}; 
    if (argc != 2)
    {
        fprintf(stderr, "程序错误:运行程序时请带入参数(./24-time filename)\n");
        exit(-1);
    }
    FILE *fp = fopen(argv[1], "a+");  
    if (fp == NULL)
    {
        perror("fopen");
        exit(-1);
    }

    //读取文件内一共有多少行 
    while(fgets(buf,2048,fp) != NULL)
    {
        line++; // 计算多少行
    }

    while (1)
    {
        line ++; 
        // long int seconds ;
        seconds = time(NULL); // 获取系统时间的秒数

        // localtime 的功能是把seconds 这个变量解析成一个结构体 struct tm
        tmp = localtime(&seconds); //
        fprintf(fp, "%ld, %04d-%02d-%02d %02d:%02d:%02d\n",line, tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday,
               tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
        fflush(fp); // 刷新文件  
        sleep(1); 
    }
    fclose(fp);
    return 0;
}