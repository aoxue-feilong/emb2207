#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ./10-fseek  file1    file2   file3
// argv[0]     argv[1]  argv[2] argv[3]
int main(int argc, char const *argv[])
{
    FILE *fp1, *fp2, *fp3;
    if (argc != 4)
    {
        fprintf(stderr, "运行程序时,请带入参数 ./10-fseek file1 file2 file3\n");
        exit(-1);
    }
    // 想要计算文件的大小, 首先需要把文件的光标移动到文件的末尾
    // r : 文件光标被设置到开始
    // w : 文件光标被设置到开始
    // a : 文件光标被设置到末尾
    fp1 = fopen(argv[1], "r");
    if (fp1 == NULL)
    {
        perror("fopen");
        exit(-1);
    }

    fp2 = fopen(argv[2], "r");
    if (fp2 == NULL)
    {
        perror("fopen");
        exit(-1);
    }

    fp3 = fopen(argv[3], "w");
    if (fp3 == NULL)
    {
        perror("fopen");
        exit(-1);
    }
    int ch=0; 
    // 每次读取一个字符, 光标自动往后移动一个元素
    while( (ch = fgetc(fp1))  != EOF )
    {
        fputc(ch,fp3); 
    }
    // 文件复制后, 光标已经移动到了fp3 的末尾 

    while( (ch = fgetc(fp2))  != EOF )
    {
        fputc(ch,fp3); 
    }

    fclose(fp1);
    fclose(fp2);
    fclose(fp3);
    return 0;
}