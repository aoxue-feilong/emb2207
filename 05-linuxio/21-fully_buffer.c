#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
// ./12-lseek  file1
// argv[0]     argv[1]
int main(int argc, char const *argv[])
{
    FILE *fp;
    int ret=0;
    FILE * fp1 ; 
    if (argc != 2)
    {
        fprintf(stderr, "运行程序时,请带入参数 ./21-fully_buffer file1\n");
        exit(-1);
    }
    fp=fopen(argv[1],"w+");
    if(fp == NULL)
    {
        perror("fopen");
        exit(-1);

    }
    int i=0;
    char buf[20]={0};
    while (1)
    {
        i++;
        sprintf(buf,"i=%07d\n",i);
        printf("buf=%s\n",buf);
        ret = fwrite(buf,1,10,fp);
        if (ret<0)
        {
            perror("fwrite");
            exit(-1);
        }
        fflush(fp);
        usleep(1000*100);
    }
    
    fclose(fp);
    return 0;
}