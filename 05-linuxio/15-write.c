#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define  N  128

// ./03-fgetc file1    file2
// argv[0]    argv[1]  argv[2]
int main(int argc, char const *argv[])
{
    int fd_src; // source file
    int fd_dst; // destination file
    int nbytes;
    char buf[N] = {0}; 
    if (argc != 3) // 判断参数的个数必须是3个参数
    {
        printf("运行程序时请带入参数: ./15-write  file1 file2 \n");
        exit(-1);
    }
    fd_src = open(argv[1],O_RDONLY|O_EXCL); // 只读方式打开文件
    if (fd_src < 0)
    {
        perror("fd_src:open");
        exit(-1);
    }

    // fopen(argv[2],"w")
    fd_dst = open(argv[2],O_WRONLY|O_CREAT|O_TRUNC,0664); // 只写的方式打开文件, 文件不存在创建, 文件存在清空
    if (fd_dst < 0)
    {
        perror("fp_dst:fopen");
        exit(-1);
    }

    // read 读到文件末尾返回 0 
    // read 没有读到文件末尾返回读回来的字节数 
    // N : 表示要读128 的元素 
    // >0 表示没有到达文件末尾  
    // == 0 表示达到文件末尾 
    // nbytes : 表示实际读回来的字节数 
    while(  (nbytes = read(fd_src,buf,N))  > 0 ) 
    {
        // 把读回来的数据写入到文件内  
        // 读回来多少字节写入多少字节  
        write(fd_dst,buf,nbytes); 

    }
    close(fd_src);
    close(fd_dst);

    return 0;
}