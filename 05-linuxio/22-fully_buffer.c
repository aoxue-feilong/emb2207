#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
// ./12-lseek  file1
// argv[0]     argv[1]
int main(int argc, char const *argv[])
{
    int ret=0;
    int i=0;
    char buf[20]={0};
    while (1)
    {
        i++;
        sprintf(buf,"i=%08d\n",i);
        ret = fwrite(buf,1,10,stdout);
        if (ret<0)
        {
            perror("fwrite");
            exit(-1);
        }
        fflush(stdout);
        usleep(1000*100);
    }
    
    fclose(stdout);
    return 0;
}