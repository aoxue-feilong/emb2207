#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
//这个程序有bug，路径后面不加斜杠会出错，具体看程序30-myls.c代码
//这个程序用的切换目录的操作，还可以拼目录，看30-myls.c程序
int main(int argc, char const *argv[])
{
    struct stat st;
    struct dirent *dt; //结构体指针
    int ret = 0;
    DIR *dirp;
    if (argc == 3) //实现ls -l的功能
    {
        chdir(argv[2]); //切换到linux目录

        if (chdir(argv[2]) != 0)
        {
            perror("chdir");
            exit(-1);
        }
        DIR *dirp = opendir(argv[2]); //打开这个目录并接受他的返回值
        if (dirp == NULL)
        {
            perror("opdir");
            exit(-1);
        }
        while ((dt = readdir(dirp)) != NULL)
        {
            if (strncmp(dt->d_name, ".", 1) == 0)
                continue;
            ret = stat(dt->d_name, &st);
            if (ret < 0)
            {
                perror("stat");
                exit(-1);
            }
            switch (st.st_mode & S_IFMT)
            {
            case S_IFBLK:
                printf("b");
                break;
            case S_IFCHR:
                printf("c");
                break;
            case S_IFDIR:
                printf("d");
                break;
            case S_IFIFO:
                printf("p");
                break;
            case S_IFLNK:
                printf("l");
                break;
            case S_IFREG:
                printf("-");
                break;
            case S_IFSOCK:
                printf("s");
                break;
            default:
                printf("unknown?\n");
                break;
            }
            /*************用户权限************************************/
            // 获取用户的可读权限
            if (st.st_mode & S_IRUSR)
                printf("r");
            else
                printf("-");

            // 获取用户的可写权限
            if (st.st_mode & S_IWUSR)
                printf("w");
            else
                printf("-");

            // 获取用户的可执行权限
            if (st.st_mode & S_IXUSR)
                printf("x");
            else
                printf("-");

            /*****************组的权限*******************************/
            // 获取组的可读权限
            if (st.st_mode & S_IRGRP)
                printf("r");
            else
                printf("-");

            // 获取组的可写权限
            if (st.st_mode & S_IWGRP)
                printf("w");
            else
                printf("-");

            // 获取组的可执行权限
            if (st.st_mode & S_IXGRP)
                printf("x");
            else
                printf("-");
            /*****************其他用户的权限*******************************/
            // 获取其它的可读权限
            if (st.st_mode & S_IROTH)
                printf("r");
            else
                printf("-");

            // 获取其它的可写权限
            if (st.st_mode & S_IWOTH)
                printf("w");
            else
                printf("-");

            // 获取其它的可执行权限
            if (st.st_mode & S_IXOTH)
                printf("x");
            else
                printf("-");
            //硬链接个数
            printf("%3ld ", st.st_nlink);
            // 根据uid
            struct passwd *pw = getpwuid(st.st_uid);
            printf("%s ", pw->pw_name);
            // 根据gid
            struct group *grp = getgrgid(st.st_gid);
            printf("%s", grp->gr_name);
            //输出文件大小
            printf("%10ld ", st.st_size);
            //输出时间
            struct tm *tmp = localtime(&st.st_mtime); //   st_mtime  最后更改时间
            printf("%02d月 %02d %02d:%02d  ", tmp->tm_mon + 1, tmp->tm_mday, tmp->tm_hour, tmp->tm_min);
            //输出文件名字
            printf("%s  \n", dt->d_name);
        }
    }
    else if (argc == 2) //实现ls 的功能
    {
        chdir(argv[1]); //切换到linux目录
        if (chdir(argv[1]) != 0)
        {
            perror("chdir");
            exit(-1);
        }
        DIR *dirp = opendir(argv[1]); //打开这个目录并接受他的返回值
        if (dirp == NULL)
        {
            perror("opdir");
            exit(-1);
        }
        while ((dt = readdir(dirp)) != NULL)
        {
            if (strncmp(dt->d_name, ".", 1) == 0) //跳过隐藏文件
                continue;
            printf("%s  ", dt->d_name);
        }
    }

    else
    {
        fprintf(stderr, "错误:运行时带上目录\n");
        exit(-1);
    }
    printf("\n");
    closedir(dirp);
    return 0;
}
