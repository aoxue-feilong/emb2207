#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

// ./03-fgetc file1    file2
// argv[0]    argv[1]  argv[2]      实现cp的功能
int main(int argc, char const *argv[])
{
    FILE *fp_src; // source file      源文件（source 来源 出处
    FILE *fp_dst; // destination file 目标文件（destination 目的地，终点，目标）
    int ch;
    if (argc != 3) // 判断参数的个数必须是3个参数
    {
        printf("运行程序时请带入参数: ./fputc  file1 file2 \n");
        exit(-1);//错误返回-1.结束程序
    }
    fp_src = fopen(argv[1], "r"); // 只读方式打开文件
    if (fp_src == NULL)//判断fp_src是否为空
    {
        perror("fp_src:fopen");//可以打印错误提示，前提是设置erron
        exit(-1);//错误返回-1，结束程序
    }

    fp_dst = fopen(argv[2], "w"); //（目标文件） 只写的方式打开文件, 文件不存在创建, 文件存在清空
    if (fp_dst == NULL)
    {
        perror("fp_dst:fopen");//可以打印错误提示，前提是设置erron
        exit(-1);
    }

    while ((ch = fgetc(fp_src)) != EOF) // 判断是否到达文件末尾时, 返回EOF则到达文件末尾
    {
        fputc(ch,fp_dst) ; // 把ch读到的字符 写入到fp_dst 文件内
    }
    fclose(fp_src);//关闭源文件
    fclose(fp_dst);//关闭目标文件

    return 0;
}