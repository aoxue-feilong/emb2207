#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char const *argv[])
{
    time_t seconds; 
    struct tm *tmp ; 
    long  line = 0 ; 
    char buf[1024] = {0}; 
    int ret  ; 
    if (argc != 2)
    {
        fprintf(stderr, "程序错误:运行程序时请带入参数(./25-time filename)\n");
        exit(-1);
    }
    int fd = open(argv[1], O_RDWR|O_CREAT|O_APPEND,0664);
    if (fd < 0)
    {
        perror("fopen");
        exit(-1);
    }
    FILE * fp =  fdopen(fd,"a+");  // 把fd转成fp 
    if(fp == NULL )
    {
        perror("fdopen"); 
        exit(-1); 
    }

    //读取文件内一共有多少行 
    while(fgets(buf,1024,fp) != NULL)
    {
        line++; // 计算多少行
    }

    while (1)
    {
        line ++; 
        // long int seconds ;
        seconds = time(NULL); // 获取系统时间的秒数

        // localtime 的功能是把seconds 这个变量解析成一个结构体 struct tm
        tmp = localtime(&seconds); //
        sprintf(buf, "%ld, %04d-%02d-%02d %02d:%02d:%02d\n",line, tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday,
               tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
        ret = write(fd,buf,strlen(buf));
        if(ret < 0)
        {
            perror("write");
            exit(-1);
        }
        sleep(1); 
    }
    close(fd);
    return 0;
}