#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
// ./12-lseek  file1
// argv[0]     argv[1]
int main(int argc, char const *argv[])
{
//1.往标准输出写入数据，缓存区没有满，数据不输出
   printf("hello world");
//2.强制刷新行缓存
//   fflush(stdout);

//3.也可以用\n进行刷新
//  printf("\n");

//4.exit程序退出时，会自动刷新行缓存和全缓存
//  exit(0);

//5.return 0  函数正常返回时，也会进行刷新行缓存和全缓存
//return 0;

//6._exit(0)  程序退出时，不进行任何刷新操作
_exit(0);
}