#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char const *argv[])
{
    FILE *fp;
    int i = 0;
    char filename[100] = {0}; //定义一个数组并且初始化
    while (1)
    {
        sprintf(filename, "file%d", i); //这个函数将filei,放到filename这个数组里面
        printf("filename=%s\n", filename);//将数组里面的文件名输出出来
        fp = fopen(filename, "w");//用只写的方式打开这个文件，文件不存在则创建，文件存在则不清空内容，光标移动到文件末尾
        if (fp == NULL)
        {
            perror("fopen"); //可以打印错误提示，前提是设置errno
            exit(-1);        //结束程序返回-1
        }
        i++;
    }

    return 0;
}