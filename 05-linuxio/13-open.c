#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
  int fd;
  //  fp =  fopen("1.txt","r") ; // 只读方式打开文件, 文件不存在, 报错
  // fd=open("1.txt",O_RDONLY|O_EXCL);

  //  fp =  fopen("1.txt","r+") ; // 可读可写方式打开文件, 文件不存在, 报错
  // fd=open("1.txt",O_RDWR|O_EXCL);

  //  fp =  fopen("1.txt","w") ; // 只写方式打开文件, 文件不存在则创建文件, 文件存在清空文件内容
  // fd=open("1.txt",O_WRONLY|O_CREAT|O_TRUNC,0664);

  //  fp =  fopen("1.txt","w+") ; // 可读可写方式打开文件, 文件不存在则创建文件, 文件存在清空文件内容
  // fd=open("1.txt",O_RDONLY|O_CREAT|O_TRUNC,0664);

  //  fp =  fopen("1.txt","a") ; // 只写方式打开文件, 文件不存在则创建文件, 文件存在不清空文件内容, 光标移动文件末尾
  //  fd = open("1.txt",O_WRONLY|O_CREAT|O_APPEND,0664);

  // fp =  fopen("1.txt","a+") ; // 可读可写方式打开文件, 文件不存在则创建文件, 文件存在不清空文件内容, 光标移动文件末尾
  fd = open("1.txt", O_RDWR | O_CREAT | O_APPEND, 0664);
  if (fd < 0) // 错误处理
  {
    perror("fopen"); // 可以打印错误提示, 前提是设置errno
    exit(-1);        // 结束程序 , 返回-1
  }
  close(fd); // 关闭文件

  return 0;
}