#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <errno.h> 


#define  N   1024
// ./03-fgetc filename 
// argv[0]    argv[1]     实现cat功能
int main(int argc, char const *argv[])
{
    FILE * fp ; 
    int ch;
    char buf[N]={0};//初始化数组 
    if(argc != 2) // 判断参数的个数必须是2个参数 
    {
        printf("运行程序时请带入参数: ./fgetc filename\n"); 
        exit(-1);//错误返回-1，结束程序
    }
    fp = fopen(argv[1],"r"); //以只读方式打开文件，文件不存在则报错
    if(fp == NULL)//判断fp是否为空
    {
        perror("fopen"); 
        exit(-1); 
    }

    // fgets 不等于NULL , 说明没有读到文件末尾 
    // fgets 等于NULL , 说明读到文件末尾 
    while( fgets(buf,N,fp) != NULL  )//将fp中的内容读到数组里面
    {
        printf("%s",buf);//输出数组里面的内容 
    }
    fclose(fp); 



    return 0;
}