#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ./12-fseek  file1
// argv[0]     argv[1]
int main(int argc, char const *argv[])
{
    FILE *fp1;
    if (argc != 2)
    {
        fprintf(stderr, "运行程序时,请带入参数 ./12-fseek file1\n");
        exit(-1);
    }
    // 想要计算文件的大小, 首先需要把文件的光标移动到文件的末尾
    // r : 文件光标被设置到开始
    // w : 文件光标被设置到开始
    // a : 文件光标被设置到末尾
    fp1 = fopen(argv[1], "r+");//可读可写文件
    if (fp1 == NULL)
    {
        perror("fopen");
        exit(-1);
    }

    // 先计算文件的大小
    fseek(fp1, 0, SEEK_END);    // 把文件位置设置到未见末尾
    long file_pos = ftell(fp1); // 获取文件位置的返回值, 就是文件大小
    // 想要对一个文件进行逆序, 需要交换file_pos/2次 即可实现
    int t1,t2; 
    for(long i =0;i<file_pos/2;i++) 
    {
        //1. 把文件光标移动到i的位置 
        fseek(fp1,i, SEEK_SET);   

        //2. 读取文件中的第i个字符  
        t1 = fgetc(fp1); 

        //3. 再把光标移动到file_pos-i的位置上
        fseek(fp1,file_pos-i-1, SEEK_SET);   

        //4. 读取文件中的第file_pos-i的字符  
        t2 = fgetc(fp1); 

        //printf("t1=%c , t2=%c \n",t1,t2);

        //5. 把文件光标移动到i的位置 
        fseek(fp1,i, SEEK_SET); 

        //6. 把文件光标移动到i的位置 
        fputc(t2,fp1);

        //7. 再把光标移动到file_pos-i的位置上
        fseek(fp1,file_pos-i-1, SEEK_SET);   

        //8. 读取文件中的第file_pos-i-1的字符  
        fputc(t1,fp1);
    }

    fclose(fp1);
    return 0;
}
