#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int main(int argc, char const *argv[])
{
    mode_t mask = umask(0777);
    printf("mask = %#o\n", mask);
    int fd = open("1.txt", O_RDWR | O_CREAT | O_APPEND, 0777);
    if (fd < 0)
    {
        perror("1:open");
        exit(-1);
    }
    printf("fd=%d\n", fd);

    umask(0);

    int fd2 = open("2.txt", O_RDWR | O_CREAT | O_APPEND, 0777);
    if (fd2 < 0)
    {
        perror("2:open");
        exit(-1);
    }
    printf("fd2=%d\n", fd2);

    umask(02);

    int fd3 = open("3.txt", O_RDWR | O_CREAT | O_APPEND, 0777);
    if (fd3 < 0)
    {
        perror("3:open");
        exit(-1);
    }
    printf("fd3=%d\n", fd3);

    umask(03);

    return 0;
}
