#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

int main(int argc, char const *argv[])
{
    struct dirent *dt;//结构体指针
    if (argc != 2)//判断是否为2个参数
    {
        fprintf(stderr, "错误:运行程序时请带入参数（./29-opendir path)\n");
        exit(-1);
    }
    DIR *dirp = opendir(argv[1]);//打开这个目录，接受他的返回值
    if (dirp == NULL)//判断是否空
    {
        perror("opendir");
        exit(-1);
    }
    while ((dt = readdir(dirp)) != NULL)//用dt这个结构体指针接受这个返回值，为空代表目录已经读完
    {
        if (strncmp(dt->d_name, ".", 1) == 0)//判断是否是隐藏文件，带.的是隐藏文件
            continue;
        printf("%s  ", dt->d_name);
    }
    printf("\n");
    closedir(dirp);//关闭目录指针
    return 0;
}
