#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "程序错误:运行程序时请带入参数(./26-fopen filename)\n");
        exit(-1);
    }
    FILE *fp = fopen(argv[1], "a+"); //
    if (fp == NULL)
    {
        perror("fopen");
        exit(-1);
    }
    int data = 0;
    char buf[100] = {0};
    while (1)
    {
        data++;
        //把指定格式的字符串写入到文件内
        fprintf(fp, "data=%d\n", data);
        fflush(fp);//强制刷新行缓存（标准IO有缓存）
        //等价与printf的功能
        fprintf(stdout, "data=%d\n", data);
        //把指定格式的字符串写入到数组内
        sprintf(buf, "data=%d\n", data);
        //等价与printf的功能
        printf("data=%d\n", data);
        sleep(1);
    }
    return 0;
}