#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{ 
    if (argc != 2)
    {
        fprintf(stderr, "程序错误:运行程序时请带入参数(./26-fopen filename)\n");
        exit(-1);
    }
    //a : 不可读, 可写, 不存在创建, 不能修改原有内容, 只能在结尾追加写, 文件指针无效
    // 读文件: 文件指针在文件末尾
    // 写文件: 文件指针在文件末尾

    //FILE * fp = fopen(argv[1],"a");  
    //a+ : 可读可写, 不存在创建, 不能修改原有内容, 只能在结尾追加写, 
    // 特殊点: 用于读取的初始文件位置位于文件的开头
    // 读文件: 文件位置在文件开头
    // 写文件: 文件位置在文件末尾
     FILE * fp = fopen(argv[1],"a+");  // 
    if(fp == NULL)
    {
        perror("fopen"); 
        exit(-1); 
    }
    // 获取文件大小  
    // 读文件  使用 a 时 ,  文件位置在文件的结尾, 等到的结果为实际大小
    // 读文件  使用 a+ 时 , 文件位置为文件开头, 等到的结果为0 
    long file_size = ftell(fp); 
    printf("file size:%ld\n",file_size);  
    fclose(fp); 



    return 0;
}