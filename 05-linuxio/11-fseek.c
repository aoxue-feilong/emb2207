#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ./10-fseek  file1    file2   file3
// argv[0]     argv[1]  argv[2] argv[3]
// file1 = file2 + file3 
int main(int argc, char const *argv[])
{
    FILE *fp1, *fp2, *fp3;
    if (argc != 4)
    {
        fprintf(stderr, "运行程序时,请带入参数 ./10-fseek file1 file2 file3\n");
        exit(-1);
    }
    // 想要计算文件的大小, 首先需要把文件的光标移动到文件的末尾
    // r : 文件光标被设置到开始
    // w : 文件光标被设置到开始
    // a : 文件光标被设置到末尾
    fp1 = fopen(argv[1], "r");
    if (fp1 == NULL)
    {
        perror("fopen");
        exit(-1);
    }

    fp2 = fopen(argv[2], "w");
    if (fp2 == NULL)
    {
        perror("fopen");
        exit(-1);
    }

    fp3 = fopen(argv[3], "w");
    if (fp3 == NULL)
    {
        perror("fopen");
        exit(-1);
    }

    // 先计算文件的大小 
    fseek(fp1,0,SEEK_END) ; // 把文件位置设置到未见末尾
    long file_pos = ftell(fp1); // 获取文件位置的返回值, 就是文件大小 

    fseek(fp1,0,SEEK_SET) ;     // 把文件光标移动到开始  
    for(long i =0;i<file_pos/2;i++) // 文件的前一半  
    {
        fputc(fgetc(fp1), fp2); // 文件的前一半 
    }

    for(long i =file_pos/2;i<file_pos;i++) // 文件的后一半  
    {
        fputc(fgetc(fp1), fp3); // 文件的后一半 
    }

    fclose(fp1);
    fclose(fp2);
    fclose(fp3);
    return 0;
}