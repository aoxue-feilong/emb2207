#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
int main(int argc, char const *argv[])
{
    if (argc != 2) //判断是否为2个参数
    {
        fprintf(stderr, "错误:运行程序时请带入参数（./32-access filename)\n");
        exit(-1);
    }
    if (access(argv[1], F_OK) == 0)
    {
        printf("%s:文件存在\n", argv[1]);
        remove(argv[1]);
    }
    int fd = open(argv[1], O_RDWR | O_CREAT | O_APPEND, 0664);
    if (fd < 0)
    {
        perror("1:open");
        exit(-1);
    }
    printf("fd=%d\n", fd);
    return 0;
}
