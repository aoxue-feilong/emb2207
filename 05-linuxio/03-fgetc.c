#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <errno.h> 

// ./03-fgetc filename 
// argv[0]    argv[1]   实现cat的功能
int main(int argc, char const *argv[])
{
    FILE * fp ; 
    int ch;
    if(argc != 2) // 判断参数的个数必须是2个参数 
    {
        printf("运行程序时请带入参数: ./fgetc filename\n"); 
        exit(-1);//错误返回-1，结束程序
    }
    fp = fopen(argv[1],"r"); // "r"只读方式打开文件，不存在则报错
    if(fp == NULL)//判断返回值是否为空
    {
        perror("fopen");// 可以打印错误提示, 前提是设置errno 
        exit(-1);//错误返回-1，结束程序 
    }
    while( (ch=fgetc(fp)) != EOF ) //ch=fgetc(fp):把fp中的字符读出来，(ch=fgetc(fp)) != EOF：说明没有到文件末尾
    {                              //fgctc到达文件末尾会返回EOF，前提是设置erron，返回EOF
        printf("%c",ch);           //读回来后打印到屏幕上，就实现了cat的功能
    }
    fclose(fp);//关闭文件



    return 0;
}