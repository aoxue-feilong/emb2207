#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define  N  1024

// ./03-fgetc file1    file2
// argv[0]    argv[1]  argv[2]    实现cp的功能
int main(int argc, char const *argv[])
{
    FILE *fp_src; // source file     （源文件）
    FILE *fp_dst; // destination file（目标文件）
    int ch;
    char buf[N] = {0};//定义一个数组并且初始化
    if (argc != 3) // 判断参数的个数必须是3个参数
    {
        printf("运行程序时请带入参数: ./fputc  file1 file2 \n");
        exit(-1);//错误返回-1，结束程序
    }
    fp_src = fopen(argv[1], "r"); // 只读方式打开文件，不存在则报错
    if (fp_src == NULL)//判断源文件是否为空
    {
        perror("fp_src:fopen");
        exit(-1);//错误返回-1，结束程序
    }

    fp_dst = fopen(argv[2], "w"); // 只写的方式打开文件, 文件不存在创建, 文件存在清空
    if (fp_dst == NULL)//判断目标文件是否为空
    {
        perror("fp_dst:fopen");
        exit(-1);//错误返回-1，结束程序
    }

    // fgets 不等于NULL , 说明没有读到文件末尾 
    // fgets 等于NULL , 说明读到文件末尾 
    while ( fgets(buf,N,fp_src) != NULL ) //将fp_src里面的内容读到buf里面
    {
        fputs(buf,fp_dst);//将buf里面的内容输出到fp_dst里面
    }
    fclose(fp_src);//关闭源文件
    fclose(fp_dst);//关闭目标文件

    return 0;
}