#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define  N     128 


int main(int argc, char const *argv[])
{
    char buf[N]={0};
    if(argc != 2)
    {
        fprintf(stderr,"错误:运行程序时请带入参数(./14-read filename)\n"); 
        exit(-1);
    }
    int fd  ; // 定义一个文件表述符号 
    //fopen("1.txt","r")  只读方式打开文件, 文件不存在, 报错
    fd =  open(argv[1],O_RDONLY|O_EXCL) ;
    if(fd < 0 ) // 错误处理
    {
        perror("fopen") ; // 可以打印错误提示, 前提是设置errno 
        exit(-1); // 结束程序 , 返回-1 
    }

    // read 返回值为0 表示到达文件末尾 
    // read 返回值为-1 表示出错
    while( read(fd,buf,N-1) >0) 
    {
        printf("%s",buf); 
        memset(buf,0,N); 
    }
    close(fd) ; // 关闭文件

    return 0;
}