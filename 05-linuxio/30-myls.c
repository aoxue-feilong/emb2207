#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
/*写程序先搭框架，再调试
接目录两种方法：1.chdir函数，2，拼接字符串的方法sprintf，*/

int popcessls(const char *arg) // ls
{
    struct dirent *dt;   //结构体指针
    DIR *dirp;           //目录结构体DIR,dirp结构体指针
    dirp = opendir(arg); //打开这个目录，接受他的返回值
    if (dirp == NULL)    //判断是否空
    {
        perror("opendir");
        exit(-1);
    }
    while ((dt = readdir(dirp)) != NULL) //用dt这个结构体指针接受这个返回值，为空代表目录已经读完
    {
        if (strncmp(dt->d_name, ".", 1) == 0) //判断是否是隐藏文件，带.的是隐藏文件（strncmp字符串比较函数）
            continue;
        printf("%s  ", dt->d_name); //输出文件名
    }
    printf("\n");
    closedir(dirp); //关闭目录指针
    return 0;
}
int popcessls_l(const char *arg) // ls -l
{
    printf("argv=%s\n", arg);
    /****************************************************************/
    struct dirent *dt;
    struct stat st;
    char filename[300] = {0};
    struct tm *tmp;
    DIR *dirp = opendir(arg);
    if (dirp == NULL)
    {
        perror("opendir");
        exit(-1);
    }
    while ((dt = readdir(dirp)) != NULL) //用dt这个结构体指针接受这个返回值，为空代表目录已经读完
    {
        if (strncmp(dt->d_name, ".", 1) == 0) //判断是否是隐藏文件，带.的是隐藏文件（strncmp字符串比较函数）
            continue;

        if (arg[strlen(arg) - 1] == '/') //判断穿参的数，最后一个字符是否为'/'
        {
            sprintf(filename, "%s%s", arg, dt->d_name); //后面两个字符串，写到第一个字符串里面，即字符串的拼接功能
        }
        else //不是'/'，给它加上，减少bug，否则少'/'程序结果会出错
        {
            sprintf(filename, "%s/%s", arg, dt->d_name);
        }

        // printf("%s\n", filename);

        int ret = stat(filename, &st);
        if (ret < 0)
        {
            perror("stat");
            exit(-1);
        }
        switch (st.st_mode & S_IFMT)
        {
        case S_IFBLK:
            printf("b");
            break;
        case S_IFCHR:
            printf("c");
            break;
        case S_IFDIR:
            printf("d");
            break;
        case S_IFIFO:
            printf("p");
            break;
        case S_IFLNK:
            printf("l");
            break;
        case S_IFREG:
            printf("-");
            break;
        case S_IFSOCK:
            printf("s");
            break;
        default:
            printf("unknown?\n");
            break;
        }
        /*************用户权限************************************/
        // 获取用户的可读权限
        if (st.st_mode & S_IRUSR)
            printf("r");
        else
            printf("-");

        // 获取用户的可写权限
        if (st.st_mode & S_IWUSR)
            printf("w");
        else
            printf("-");

        // 获取用户的可执行权限
        if (st.st_mode & S_IXUSR)
            printf("x");
        else
            printf("-");

        /*****************组的权限*******************************/
        // 获取组的可读权限
        if (st.st_mode & S_IRGRP)
            printf("r");
        else
            printf("-");

        // 获取组的可写权限
        if (st.st_mode & S_IWGRP)
            printf("w");
        else
            printf("-");

        // 获取组的可执行权限
        if (st.st_mode & S_IXGRP)
            printf("x");
        else
            printf("-");
        /*****************其他用户的权限*******************************/
        // 获取其它的可读权限
        if (st.st_mode & S_IROTH)
            printf("r");
        else
            printf("-");

        // 获取其它的可写权限
        if (st.st_mode & S_IWOTH)
            printf("w");
        else
            printf("-");

        // 获取其它的可执行权限
        if (st.st_mode & S_IXOTH)
            printf("x");
        else
            printf("-");
        printf(" ");
        printf(" %ld", st.st_nlink);
        /*****************文件所有者*******************************/
        struct passwd *pw = getpwuid(st.st_uid); // 根据uid获取用户名
        printf(" %s", pw->pw_name);
        struct group *grp = getgrgid(st.st_gid); // 根据gid获取 组的名称
        printf(" %s ", grp->gr_name);
        /*******************文件的大小*****************************/
        printf("%4ld ", st.st_size);
        /*******************文件的修改时间**************************/

        tmp = localtime(&st.st_mtime); //   st_mtime 就是秒数 最后更改时间
        printf("%02d月%02d %02d:%02d ", tmp->tm_mon + 1, tmp->tm_mday,
               tmp->tm_hour, tmp->tm_min);

        printf("%s\n", dt->d_name);
    }
    closedir(dirp);
    return 0;
}
int main(int argc, char const *argv[])
{
    if (argc == 2) //判断参数，2个实现ls的功能
    {
        popcessls(argv[1]);
    }
    else if (argc == 3) //判断参数，3个实现ls -l的功能
    {
        popcessls_l(argv[2]);
    }
    else
    {
        fprintf(stderr, "错误:运行时带上目录 ./30-myls /home/linux\n");
        exit(-1);
    }
    return 0;
}
