#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
    int fd ;
    if (argc != 2)
    {
        printf("运行程序时请带入参数:./18-fdopen filename  \n");
        exit(-1);
    }
    fd = open(argv[1], O_RDWR | O_CREAT | O_TRUNC, 0664);
    if (fd < 0)
    {
        perror("open");
        exit(-1);
    }
    int ret = 0;
    int i = 0;
    char buf[20] = {0};
    while (1)
    {
        i++;
        sprintf(buf, "i=%07d\n", i);
        printf("buf= %s\n", buf);
        ret = write(fd, buf, 10);
        if (ret < 0)
        {
            perror("write");
            exit(-1);
        }
        usleep(1000 * 100);
    }

    close(fd);

    return 0;
}
