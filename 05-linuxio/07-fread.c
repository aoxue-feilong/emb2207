#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <errno.h> 

#define  N   128 

// ./03-fgetc filename 
// argv[0]    argv[1]    实现cat的功能
int main(int argc, char const *argv[])
{
    FILE * fp ; 
    int ch;
    int nbytes ; 
    char buf[N]={0}; 
    if(argc != 2) // 判断参数的个数必须是2个参数 
    {
        printf("运行程序时请带入参数: ./fgetc filename\n"); 
        exit(-1); 
    }
    fp = fopen(argv[1],"r");//以只读方式打开文件，不存在则报错
    if(fp == NULL)
    {
        perror("fopen"); 
        exit(-1); 
    }

    // fread 读到文件末尾返回 0 
    // fread 没有读到文件末尾返回读回来的字节数 
    // N : 表示要读128 -1 的元素 
    // 1 : 每一个元素占用1个字节的大小
    // >0 表示没有到达文件末尾  
    // == 0 表示达到文件末尾 
    // nbytes : 表示实际读回来的字节数 
    while(  (nbytes = fread(buf,1,N-1,fp))  > 0 ) 
    {
        printf("%s",buf); // 把buf中的内容当成字符串输出, fread不会自动加上'\0'
        // 必须每次清空buf 
        memset(buf,0,sizeof(buf)); // 不这么做程序会有乱码
    }
    fclose(fp); 



    return 0;
}