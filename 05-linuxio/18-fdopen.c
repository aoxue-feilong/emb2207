#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
// ./12-lseek  file1
// argv[0]     argv[1]
int main(int argc, char const *argv[])
{
    int fd1,ret=0;
    FILE * fp1 ; 
    if (argc != 2)
    {
        fprintf(stderr, "运行程序时,请带入参数 ./18-fdopen file1\n");
        exit(-1);
    }
    // 想要计算文件的大小, 首先需要把文件的光标移动到文件的末尾
    // r : 文件光标被设置到开始
    // w : 文件光标被设置到开始
    // a : 文件光标被设置到末尾
    fd1 = open(argv[1], O_RDWR|O_EXCL); //open(argv[1], "r+")
    if (fd1 < 0)
    {
        perror("fopen");
        exit(-1);
    }
    fp1 =  fdopen(fd1,"r+");  // 把fd1转成fp1 
    if(fp1 == NULL )
    {
        perror("fdopen"); 
        exit(-1); 
    }
 

    // 先计算文件的大小
    long file_pos = lseek(fd1, 0, SEEK_END);    // 把文件位置设置到未见末尾fd1


    // 想要对一个文件进行逆序, 需要交换file_pos/2次 即可实现
    int t1,t2; 
    for(long i =0;i<file_pos/2;i++) 
    {
        //1. 把文件光标移动到i的位置 
        fseek(fp1,i, SEEK_SET);   

        //2. 读取文件中的第i个字符  
        t1 = fgetc(fp1);  


        //3. 再把光标移动到file_pos-i的位置上
        fseek(fp1,file_pos-i-1, SEEK_SET);   

        //4. 读取文件中的第file_pos-i的字符  
        t2 = fgetc(fp1);

        //printf("t1=%c , t2=%c \n",t1,t2);

        //5. 把文件光标移动到i的位置 
        fseek(fp1,i, SEEK_SET); 

        //6. 把文件光标移动到i的位置 
        fputc(t2,fp1);

        //7. 再把光标移动到file_pos-i的位置上
        fseek(fp1,file_pos-i-1, SEEK_SET);   

        //8. 读取文件中的第file_pos-i-1的字符  
        fputc(t1,fp1);
    }

    close(fd1);
    return 0;
}