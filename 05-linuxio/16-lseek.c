#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

// ./16-lseek  file1    file2   file3
// argv[0]     argv[1]  argv[2] argv[3]
// file1 = file2 + file3 
int main(int argc, char const *argv[])
{
    int fd1, fd2, fd3;
    int ch,ret=0;
    if (argc != 4)
    {
        fprintf(stderr, "运行程序时,请带入参数 ./16-lseek file1 file2 file3\n");
        exit(-1);
    }
    // 想要计算文件的大小, 首先需要把文件的光标移动到文件的末尾
    // r : 文件光标被设置到开始
    // w : 文件光标被设置到开始
    // a : 文件光标被设置到末尾
    fd1 = open(argv[1],O_RDONLY|O_EXCL); //fopen(argv[1], "r")
    if (fd1  < 0 )
    {
        perror("fd1:open");
        exit(-1);
    }

    fd2 = open(argv[2], O_WRONLY|O_CREAT|O_TRUNC,0664); //fopen(argv[2], "w");
    if (fd2 < 0)
    {
        perror("fd2:open");
        exit(-1);
    }

    fd3 = open(argv[3], O_WRONLY|O_CREAT|O_TRUNC,0664); //fopen(argv[3], "w");
    if (fd3 < 0)
    {
        perror("fd2:open");
        exit(-1);
    }

    // 先计算文件的大小 
    long file_pos = lseek(fd1,0,SEEK_END) ; // 把文件位置设置到未见末尾

    lseek(fd1,0,SEEK_SET) ;     // 把文件光标移动到开始  
    for(long i =0;i<file_pos/2;i++) // 文件的前一半  
    {
        ret = read(fd1,&ch,1); // 一次都一个字节的字符
        if(ret < 0)
        {
            perror("read");
            exit(-1);
        }
        ret = write(fd2, &ch, 1); // 一次写一个字节的字符
        if (ret < 0)
        {
            perror("write");
            exit(-1);
        } // 文件的前一半
    }

    for(long i =file_pos/2;i<file_pos;i++) // 文件的后一半  
    {
        ret = read(fd1, &ch, 1); // 一次都一个字节的字符
        if (ret < 0)
        {
            perror("read");
            exit(-1);
        }
        ret = write(fd3, &ch, 1); // 一次写一个字节的字符
        if (ret < 0)
        {
            perror("write");
            exit(-1);
        } // 文件的后一半
    }

    close(fd1);
    close(fd2);
    close(fd3);
    return 0;
}