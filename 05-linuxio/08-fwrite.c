#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define  N  128

// ./03-fgetc file1    file2
// argv[0]    argv[1]  argv[2]
int main(int argc, char const *argv[])
{
    FILE *fp_src; // source file     （源文件）
    FILE *fp_dst; // destination file（目标文件）
    int nbytes;
    char buf[N] = {0}; //定义数组初始化
    if (argc != 3) // 判断参数的个数必须是3个参数
    {
        printf("运行程序时请带入参数: ./fputc  file1 file2 \n");
        exit(-1);
    }
    fp_src = fopen(argv[1], "r"); // 只读方式打开文件，不存在则报错
    if (fp_src == NULL)//判断源文件是否为空
    {
        perror("fp_src:fopen");
        exit(-1);//错误返回-1，结束程序
    }

    fp_dst = fopen(argv[2], "w"); // 只写的方式打开文件, 文件不存在创建, 文件存在清空
    if (fp_dst == NULL)
    {
        perror("fp_dst:fopen");
        exit(-1);//错误返回-1，结束程序
    }

    // fread 读到文件末尾返回 0 
    // fread 没有读到文件末尾返回读回来的字节数 
    // N : 表示要读128 -1 的元素 
    // 1 : 每一个元素占用1个字节的大小
    // >0 表示没有到达文件末尾  
    // == 0 表示达到文件末尾 
    // nbytes : 表示实际读回来的字节数 
    while(  (nbytes = fread(buf,1,N,fp_src))  > 0 ) //将fp_src中的字符读到buf里面
    {
        // 把读回来的数据写入到文件内  
        // 读回来多少字节写入多少字节  
        fwrite(buf,1,nbytes,fp_dst); //将读回来的字符输出去到fp_dst里面

    }
    fclose(fp_src);//关闭源文件
    fclose(fp_dst);//关闭目标文件

    return 0;
}