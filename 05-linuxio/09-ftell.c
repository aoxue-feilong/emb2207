#include <stdio.h>
#include <stdlib.h>
#include <string.h> 


// ./09-ftell  filename 
// argv[0]     argv[1]
int main(int argc, char const *argv[])
{
    FILE * fp ; 
    if(argc != 2)
    {
        fprintf(stderr,"运行程序时,请带入参数 ./09-ftell filename \n"); 
        exit(-1); 
    }
    // 想要计算文件的大小, 首先需要把文件的光标移动到文件的末尾 
    // r : 文件光标被设置到开始
    // w : 文件光标被设置到开始
    // a : 文件光标被设置到末尾
    fp = fopen(argv[1],"a"); 
    if(fp == NULL)
    {
        perror("fopen"); 
        exit(-1); 
    }
    long file_pos =  ftell(fp) ; // 获取文件光标当前的位置
    printf("filesize:%ld\n",file_pos); 
    fclose(fp); 
    return 0;
}