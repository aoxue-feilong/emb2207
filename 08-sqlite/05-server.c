#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sqlite3.h>
#include <time.h>

#define N 128
#define DATABASE "my.db"

typedef struct
{
    char mtype;    //类型
    char mname[N]; //名字
    char mtext[N]; //密码
} msg_t;

typedef struct
{
    int newsockfd;
    int flag;
    msg_t *msgp;
} ds_t;

int process_register(sqlite3 *db, msg_t *msgp, int newsockfd) //注册函数
{
    int ret;
    char *errmsg;
    char sql[512] = {0};

    char **retp;
    int nrow = 0, ncolumn = 0;
    sprintf(sql, "select * from usr where usr='%s';", msgp->mname);
    ret = sqlite3_get_table(db, sql, &retp, &nrow, &ncolumn, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    if (nrow > 0) // nrow>0说明在数据库中存在该用户
    {
        sprintf(msgp->mtext, "用户名已存在");
    }
    else if (nrow == 0) // nrow=0则表示数据库中没有该用户
    {
        sprintf(sql, " insert into usr values('%s','%s');", msgp->mname, msgp->mtext);
        printf("sql=%s\n", sql);
        ret = sqlite3_exec(db, sql, NULL, NULL, &errmsg); //执行数据库语句
        if (ret != SQLITE_OK)
        {
            printf("%s\n", errmsg);
            exit(-1);
        }
        sprintf(msgp->mtext, "注册成功");
    }

    ret = send(newsockfd, msgp, sizeof(msg_t), 0); // 发送给客户端
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }

    return 0;
}

int process_login(sqlite3 *db, msg_t *msgp, int newsockfd) //登录函数
{
    int ret;
    char *errmsg;
    char sql[512] = {0};

    char **retp;
    int nrow = 0, ncolumn = 0;
    sprintf(sql, "select * from usr where usr='%s' and passwd ='%s';", msgp->mname, msgp->mtext);
    ret = sqlite3_get_table(db, sql, &retp, &nrow, &ncolumn, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    if (nrow > 0)
    {
        sprintf(msgp->mtext, "登录成功");
    }
    else if (nrow == 0)
    {
        sprintf(msgp->mtext, "登录失败,账号密码错误");
    }

    ret = send(newsockfd, msgp, sizeof(msg_t), 0); // 发送给客户端
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }

    return 0;
}

char *search_word(char *word) //搜素单词函数
{
    static char text[1024] = {0};
    char newword[128] = {0};
    strcpy(newword, word);
    strcat(newword, " "); // 加上空格表示一个单词的结束

    FILE *fp = fopen("dict.txt", "r"); //用只读方式打开文件
    if (fp == NULL)
    {
        perror("fopen");
        exit(-1);
    }
    while (fgets(text, 1024, fp) != NULL) //将fp的内容写到text里面
    {
        if (strncmp(text, newword, strlen(newword)) == 0)
        {
            return text;
        }
    }
    fclose(fp);
    return NULL;
}

char *get_time(void) //获取系统时间的函数
{
    static char buf[128] = {0};

    time_t seconds = time(NULL);

    struct tm *tmp = localtime(&seconds);
    sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d", tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday,
            tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
    return buf;
}

int process_query(sqlite3 *db, msg_t *msgp, int newsockfd) //查单词函数
{
    int ret;
    char *errmsg;
    char sql[512] = {0};
    char *textp = NULL;
    //字符串拼接函数
    sprintf(sql, " insert into record values('%s','%s','%s');", msgp->mname, msgp->mtext, get_time());
    printf("sql=%s\n", sql);
    ret = sqlite3_exec(db, sql, NULL, NULL, &errmsg); //执行数据库语句
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }

    textp = search_word(msgp->mtext); //查找单词，函数返回值为空，表示未找到词汇
    if (textp == NULL)
    {
        sprintf(msgp->mtext, "word not found\n");
    }
    else//找到单词
    {
        sprintf(msgp->mtext, "%s\n", textp);//将textp写到指定内存msgp->mtext
    }

    ret = send(newsockfd, msgp, sizeof(msg_t), 0); // 发送给客户端
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }

    return 0;
}
//这儿用到结构体传参，可以传好几个参数
int callback(void *arg, int f_num, char **f_value, char **f_name)//回调函数
{
    int ret;
    char buf[N] = {0};
    ds_t *dsp = (ds_t *)arg;

    memset(dsp->msgp->mtext, 0, sizeof(dsp->msgp->mtext));
    // 输出列名  , 只输出一次
    if (dsp->flag) // *(int *)arg == 1
    {
        dsp->flag = 0; // 写0  , 外部的flag 的值0  , 能够保证  if( *(int *)arg ) 只能被执行一次
        for (int i = 0; i < f_num; i++)
        {

            sprintf(buf, "|%8s", f_name[i]);
            strcat(dsp->msgp->mtext, buf);//拼接到dsp->msgp->mtex这里面
        }
        sprintf(buf, "|\n");
        strcat(dsp->msgp->mtext, buf);
    }

    for (int i = 0; i < f_num; i++)
    {
        sprintf(buf, "|%8s", f_value[i]);
        strcat(dsp->msgp->mtext, buf);//拼接到dsp->msgp->mtex这里面
    }
    sprintf(buf, "|\n");
    strcat(dsp->msgp->mtext, buf);

    ret = send(dsp->newsockfd, dsp->msgp, sizeof(msg_t), 0); // 发送给客户端
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }

    return 0; // 一定要有返回值 , 成功返回0
}

int process_history(sqlite3 *db, msg_t *msgp, int newsockfd) //查历史记录
{
    int ret;
    char *errmsg;
    char sql[512] = {0};
    char *textp = NULL;

    ds_t ds;
    ds.flag = 1;
    ds.newsockfd = newsockfd;
    ds.msgp = msgp;

    sprintf(sql, "select *from record where usr='%s';", msgp->mname);
    printf("sql=%s\n", sql);
    ret = sqlite3_exec(db, sql, callback, (void *)&ds, &errmsg);//执行数据库语句
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    
    close(newsockfd);
    return 0;
}

// 01-socket 192.168.1.2  8000
// argv[0]   arv[1]       argv[2]
int main(int argc, char const *argv[])
{

    int sockfd, ret, newsockfd;
    struct sockaddr_in myaddr, client_addr;
    socklen_t addrlen;
    char buf[N] = {0};
    msg_t msg;
    memset(&msg, 0, sizeof(msg));

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./05-server ip port)\n");
        exit(-1);
    }

    sqlite3 *db;
    ret = sqlite3_open(DATABASE, &db);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", sqlite3_errmsg(db));
        exit(-1);
    }

    // 创建一个socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&myaddr, 0, sizeof(myaddr));           // 清空结构体
    memset(&client_addr, 0, sizeof(client_addr)); // 清空结构体
    myaddr.sin_family = AF_INET;                  // 什么类型的通信
    myaddr.sin_port = htons(atoi(argv[2]));       // 设置socket 的固定端口
    myaddr.sin_addr.s_addr = inet_addr(argv[1]);  // 把字符串ip转换成网络二进制的ip地址
    ret = bind(sockfd, (struct sockaddr *)&myaddr, sizeof(myaddr));
    if (ret < 0)
    {
        perror("bind");
        exit(-1);
    }
    ret = listen(sockfd, 10);
    if (ret < 0)
    {
        perror("listen");
        exit(-1);
    }
    // system("netstat -ant");
    addrlen = sizeof(client_addr); //这个赋值如果缺少, 可能会引发程序错误
    while (1)
    {
        newsockfd = accept(sockfd, (struct sockaddr *)&client_addr, &addrlen); // 接收客户端tcp连接请求
        if (newsockfd < 0)
        {
            perror("accept");
            exit(-1);
        }
        printf("newsockfd=%d\n", newsockfd);
        if (fork() == 0) // 子进程
        {
            while (1)
            {
                ret = recv(newsockfd, &msg, sizeof(msg), 0); // 接收客户端发来的数据
                if (ret < 0)
                {
                    perror("read");
                    exit(-1);
                }
                else if (ret == 0)
                {
                    break;
                }

                printf("%s & %d :%c\n", inet_ntoa(client_addr.sin_addr),
                       ntohs(client_addr.sin_port), msg.mtype);

                switch (msg.mtype)
                {
                case 'R':
                    process_register(db, &msg, newsockfd); //注册函数
                    break;
                case 'L':
                    process_login(db, &msg, newsockfd); //登录函数
                    break;
                case 'Q':
                    process_query(db, &msg, newsockfd); //查单词函数
                    break;
                case 'H':
                    process_history(db, &msg, newsockfd); //查历史记录
                    exit(0);
                    break;
                default:
                    break;
                }
            }
            close(newsockfd);
            exit(0);
        }

        close(newsockfd); // 关闭客户端连接
    }

    close(sockfd);

    return 0;
}
