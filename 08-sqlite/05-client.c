#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define N 128

typedef struct
{
    char mtype;
    char mname[N];
    char mtext[N];
} msg_t;

int process_register(msg_t *msgp, int sockfd) //注册函数
{
    int ret;
    msgp->mtype = 'R'; //消息类型
    printf("请输入你的账号>:");
    fgets(msgp->mname, N, stdin);
    msgp->mname[strlen(msgp->mname) - 1] = 0; //消除\n
    printf("请输入你的密码>:");
    fgets(msgp->mtext, N, stdin);
    msgp->mtext[strlen(msgp->mtext) - 1] = 0;   //消除\n
    ret = send(sockfd, msgp, sizeof(msg_t), 0); //发送给服务器
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }
    memset(msgp, 0, sizeof(msg_t));             //清空结构体
    ret = recv(sockfd, msgp, sizeof(msg_t), 0); // 接收服务器发来的数据
    if (ret < 0)
    {
        perror("read");
        exit(-1);
    }
    printf("%s\n", msgp->mtext);
    return 0;
}

int process_query(msg_t *msgp, int sockfd)//查单词函数
{
    int ret;
    msgp->mtype = 'Q';//消息类型

    while (1)
    {
        printf("请输入单词>:");
        fgets(msgp->mtext, N, stdin);
        msgp->mtext[strlen(msgp->mtext) - 1] = 0;

        if (strncmp(msgp->mtext, "#", 1) == 0)//输入#结束查单词
            break;

        ret = send(sockfd, msgp, sizeof(msg_t), 0);//发送给服务器
        if (ret < 0)
        {
            perror("send");
            exit(-1);
        }

        memset(msgp, 0, sizeof(msg_t));//清空结构体

        ret = recv(sockfd, msgp, sizeof(msg_t), 0); // 接收服务器发来的数据
        if (ret < 0)
        {
            perror("read");
            exit(-1);
        }

        printf("%s", msgp->mtext);
    }

    return 0;
}

int process_history(msg_t *msgp, int sockfd)//查历史记录
{
    int ret;
    msgp->mtype = 'H';//消息类型

    ret = send(sockfd, msgp, sizeof(msg_t), 0);
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }

    memset(msgp, 0, sizeof(msg_t));//清空结构体
    while (recv(sockfd, msgp, sizeof(msg_t), 0) > 0)//对方主动关闭socket，recv返回为0
    {
        printf("%s", msgp->mtext);
    }

    return 0;
}

int process_level2(msg_t *msgp, int sockfd)//二级菜单
{
    int num = 0;
    while (1)
    {
        printf("****************************************\n");
        printf(" **1:query word 2: query history 3:quit**\n");
        printf("******************************************\n");
        printf(">:");
        scanf("%d", &num);
        scanf("%*[^\n]"); // 抑制除了 ‘\n’以外的所有字符
        scanf("%*c");     // 再抑制一个 ‘\n’
        switch (num)
        {
        case 1:
            process_query(msgp, sockfd);//查单词函数
            break;
        case 2:
            process_history(msgp, sockfd);//查历史记录
            close(sockfd);
            exit(0);
            break;
        case 3:
            close(sockfd);
            break;
        defualt:
            break;
        }
    }
    close(sockfd);
}

int process_login(msg_t *msgp, int sockfd)//登录函数
{
    int ret;
    msgp->mtype = 'L';//消息类型
    printf("请输入你的账号>:");
    fgets(msgp->mname, N, stdin);
    msgp->mname[strlen(msgp->mname) - 1] = 0;
    printf("请输入你的密码>:");
    fgets(msgp->mtext, N, stdin);
    msgp->mtext[strlen(msgp->mtext) - 1] = 0;
    ret = send(sockfd, msgp, sizeof(msg_t), 0);//发送给服务器
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }
    memset(msgp, 0, sizeof(msg_t));//清空结构体
    ret = recv(sockfd, msgp, sizeof(msg_t), 0); // 接收服务器发来的数据
    if (ret < 0)
    {
        perror("read");
        exit(-1);
    }
    printf("%s\n", msgp->mtext);
    //比较看服务器是不是发送过来的是登录成功（也可以用sizeof（13），一个汉字占4字节）
    if (strncmp(msgp->mtext, "登录成功", sizeof("登录成功")) == 0)
    {
        process_level2(msgp, sockfd);//进行查单词
    }

    return 0;
}

// 01-socket 192.168.1.2 8000
// argv[0]   arv[1]      argv[2]
int main(int argc, char const *argv[])
{

    int sockfd, ret;
    struct sockaddr_in server_addr;
    socklen_t addrlen;
    char buf[N] = {0};
    msg_t msg;
    memset(&msg, 0, sizeof(msg));

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./05-client  ip port)\n");
        exit(-1);
    }

    // 创建一个socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&server_addr, 0, sizeof(server_addr));     // 清空结构体
    server_addr.sin_family = AF_INET;                 // 什么类型的通信
    server_addr.sin_port = htons(atoi(argv[2]));      // 设置socket 的固定端口
    server_addr.sin_addr.s_addr = inet_addr(argv[1]); // 把字符串ip转换成网络二进制的ip地址
    ret = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (ret < 0)
    {
        perror("connect");
        exit(-1);
    }

    int num = 0;
    while (1)
    {
        printf("************************************\n");
        printf(" **1:register 2: login 3:quit********\n");
        printf("************************************\n");
        printf(">:");
        scanf("%d", &num);
        scanf("%*[^\n]"); // 抑制除了 ‘\n’以外的所有字符
        scanf("%*c");     // 再抑制一个 ‘\n’
        switch (num)
        {
        case 1:
            process_register(&msg, sockfd);
            break;
        case 2:
            process_login(&msg, sockfd);
            break;
        case 3:
            close(sockfd);
            exit(0);
            break;
        defualt:
            break;
        }
    }
    close(sockfd);

    return 0;
}
