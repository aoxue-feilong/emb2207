#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sqlite3.h>

int main(int argc, char const *argv[])
{
    sqlite3 *db;
    int ret = 0;
    if (argc != 2)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./01-seqlite3 filename)\n");
        exit(-1);
    }
    ret = sqlite3_open(argv[1], &db);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", sqlite3_errmsg(db));
        exit(-1);
    }
    sqlite3_close(db);
    return 0;
}
