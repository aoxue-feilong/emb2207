#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define    N   128 
char * search_word(char *word)//查单词
{
    static char text[1024]={0};
    char newword[128]={0};
    strcpy(newword,word);//赋值函数（cp命令）
    strcat(newword," "); // 加上空格表示一个单词的结束 （字符串拼接函数）

    FILE * fp = fopen("dict.txt","r");//打开这个目录
    if(fp == NULL)
    {
        perror("fopen");
        exit(-1);
    }
    while( fgets(text,1024,fp) != NULL)//fp的写到text里面
    {
        if(strncmp(text,newword,strlen(newword)) ==0 )//text，newword两者比较。单词与词库的单词相同即输出
        {
            return text;
        }
    }
    fclose(fp);
    return NULL;
}

int main(int argc, char const *argv[])
{
    char buf[N]={0};
    char *textp = NULL;
    while(1)
    {
        printf("input your word >:");
        fgets(buf,N,stdin);//从屏幕当中获取，写到buf里面
        buf[strlen(buf)-1]='\0';
        if( strncmp(buf,"#",1) ==0) break;//字符比较函数，将buf,里面的第一个字符和#相比较，相同即函数返回为0
        printf("buf=%s\n",buf);
        textp = search_word(buf);
        if(textp == NULL)
        {
            printf("word not found\n");
        }
        else 
        {
            printf("%s\n",textp); 
        }
        
    }
    return 0;
}
