#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sqlite3.h>
#define N 128

int callback(void *arg, int f_num, char **f_value, char **f_name)//回调函数
{
    if (*(int *)arg)
    {
        *(int *)arg = 0;
        for (int i = 0; i < f_num; i++)
        {
            printf("|%s", f_name[i]);
        }
        printf("|\n");
    }
    for (int i = 0; i < f_num; i++)
    {
        printf("|%s", f_value[i]);
    }
    printf("|\n");

    return 0;
}

int main(int argc, char const *argv[])
{
    sqlite3 *db;
    char *errmsg;
    int ret = 0;
    char sq1[N] = {0};
    if (argc != 2)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./02-seqlite3 filename)\n");
        exit(-1);
    }
    ret = sqlite3_open(argv[1], &db);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", sqlite3_errmsg(db));
        exit(-1);
    }
    //增
    sprintf(sq1, "insert into student values(%d,'%s','%s',%d,%f,'%s');",
            5, "li", "female", 22, 90.5, "15593399025");//将拼接好的字符串写到sq1里面
    printf("sq1=%s\n", sq1);
    ret = sqlite3_exec(db, sq1, NULL, NULL, &errmsg);//执行数据库语句
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    //删
    sprintf(sq1, "delete from student where number=%d;", 1);
    printf("sq1=%s\n", sq1);
    ret = sqlite3_exec(db, sq1, NULL, NULL, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    //修
    sprintf(sq1, "update student set score=60 where number=%d;", 2);
    printf("sq1=%s\n", sq1);
    ret = sqlite3_exec(db, sq1, NULL, NULL, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    //查找
    sprintf(sq1, "select * from student;");
    printf("sq1=%s\n", sq1);
    int flag = 1;
    ret = sqlite3_exec(db, sq1, callback, (void *)&flag, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    sqlite3_close(db);
    return 0;
}
