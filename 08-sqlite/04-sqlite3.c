#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sqlite3.h>

#define N 128

int process_insert(sqlite3 *db)
{
    // 插入记录
    int ret;
    char *errmsg;
    char sql[N] = {0};
    int number, age;
    float score;
    char name[N] = {0}, sex[10] = {0}, tel[20];
    printf("请输入学生的学号>:");
    scanf("%d", &number);
    scanf("%*[^\n]"); // 抑制除了 ‘\n’以外的所有字符
    scanf("%*c");     // 再抑制一个 ‘\n’
    printf("请输入学生的姓名>:");
    fgets(name, N, stdin);
    name[strlen(name) - 1] = 0; // 消除'\n'
    printf("请输入学生的性别>:");
    fgets(sex, N, stdin);
    sex[strlen(sex) - 1] = 0; // 消除'\n'
    printf("请输入学生的年龄>:");
    scanf("%d", &age);
    scanf("%*[^\n]"); // 抑制除了 ‘\n’以外的所有字符
    scanf("%*c");     // 再抑制一个 ‘\n’
    printf("请输入学生的成绩>:");
    scanf("%f", &score);
    scanf("%*[^\n]"); // 抑制除了 ‘\n’以外的所有字符
    scanf("%*c");     // 再抑制一个 ‘\n’
    printf("请输入学生的电话>:");
    fgets(tel, N, stdin);
    tel[strlen(tel) - 1] = 0; // 消除'\n'

    sprintf(sql, "insert into student values(%d,'%s','%s',%d,%f,'%s');",
            number, name, sex, age, score, tel);
    printf("sql=%s\n", sql);
    ret = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    return 0;
}
int process_delete(sqlite3 *db)
{
    // 插入记录
    int ret;
    char *errmsg;
    char sql[N] = {0};
    int number, age;
    float score;
    char name[N] = {0}, sex[10] = {0}, tel[20];
    printf("请输入学生的学号>:");
    scanf("%d", &number);
    scanf("%*[^\n]"); // 抑制除了 ‘\n’以外的所有字符
    scanf("%*c");     // 再抑制一个 ‘\n’

    // 删除记录
    sprintf(sql, "delete from student where number=%d;", number);
    printf("sql=%s\n", sql);
    ret = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    return 0;
}

int callback(void *arg, int f_num, char **f_value, char **f_name) /* Callback function */
{
    // 输出列名  , 只输出一次
    if (*(int *)arg) // *(int *)arg == 1
    {
        *(int *)arg = 0; // 写0  , 外部的flag 的值0  , 能够保证  if( *(int *)arg ) 只能被执行一次
        for (int i = 0; i < f_num; i++)
        {
            printf("|%8s", f_name[i]);
        }
        printf("|\n");
    }

    for (int i = 0; i < f_num; i++)
    {
        printf("|%8s", f_value[i]);
    }
    printf("|\n");
    return 0; // 一定要有返回值 , 成功返回0
}
int process_show(sqlite3 *db)
{
    // 插入记录
    int ret;
    char *errmsg;
    char sql[N] = {0};
    // 查找记录 , 需要使用回调函数
    sprintf(sql, "select * from student;");
    printf("sql=%s\n", sql);
    int flag = 1;
    // 在做记录查询时, 需要判断 这个用户名在数据库中是否存在, 用sqlite3_get_table 比sqlite3_exec
    // 简单 , 可以直接判断 pnRow 的返回值是否大于0 , 大于0 表示有记录, 等于0 表示没有记录 
    char **retp; 
    int nrow = 0 , ncolumn =0 ; 
    ret = sqlite3_get_table(db,sql,&retp,&nrow,&ncolumn,&errmsg) ; 
    //ret = sqlite3_exec(db, sql, callback, (void *)&flag, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    if(nrow >0) // norw > 0 表示找打了记录 , 可以使用这种办法查询用户登录和注册 
    {
        for(int i=0;i<ncolumn;i++) // 显示列名 
        {
            printf("|%5s", retp[i]);
        }
        printf("|\n");

        // 显示内容
        for (int k = 0; k < nrow; k++)  // 一共几行 
        {
            for (int i = 0; i < ncolumn; i++) // 一共几列 
            {
                printf("|%5s", retp[ (k+1)*ncolumn + i]);
            }
            printf("|\n");
        }
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    sqlite3 *db; // data base

    int ret;
    char *errmsg;
    if (argc != 2)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./01-sqlite3 filename )\n");
        exit(-1);
    }
    ret = sqlite3_open(argv[1], &db);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", sqlite3_errmsg(db));
        exit(-1);
    }
    int num = 0;
    while (1)
    {
        printf("***********************************************\n");
        printf("**1:插入记录 2: 删除记录 3:显示记录 4:退出程序**\n");
        printf("***********************************************\n");
        printf(">:");
        scanf("%d", &num);
        scanf("%*[^\n]"); // 抑制除了 ‘\n’以外的所有字符
        scanf("%*c");     // 再抑制一个 ‘\n’
        switch (num)
        {
        case 1:
            process_insert(db);
            break;
        case 2:
            process_delete(db);
            break;
        case 3:
            process_show(db);
            break;
        case 4:
            sqlite3_close(db);

            exit(0); // 退出程序
            break;
        default:
            break;
        }
    }

    return 0;
}
