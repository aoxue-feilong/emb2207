#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sqlite3.h>
#define N 128

int insert_student(sqlite3 *db) //增加
{
    int id = 0;         //学号
    int age = 0;        //年龄
    char name[N] = {0}; //名字
    float score = 0;    //成绩
    char sq1[N] = {0};
    char *errmsg;
    int ret;
    printf("请输入学生的学号:>");
    scanf("%d", &id);

    printf("请输入学生的姓名:>");
    scanf("%s", name);

    printf("请输入学生的年龄:>");
    scanf("%d", &age);

    printf("请输入学生的成绩:>");
    scanf("%f", &score);
    sprintf(sq1, "insert into student values(%d,'%s',%d,%f)", id, name, age, score);
    printf("sq1=%s\n", sq1);
    ret = sqlite3_exec(db, sq1, NULL, NULL, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    return 0;
}
int delete_student(sqlite3 *db) //删除
{
    int id = 0;         //学号
    int age = 0;        //年龄
    char name[N] = {0}; //名字
    float score = 0;    //成绩
    char sq1[N] = {0};
    char *errmsg;
    int ret;
    printf("请输入要删除学生的学号:");
    scanf("%d", &id);
    sprintf(sq1, "delete from student where id=%d;", id);
    printf("sq1=%s\n", sq1);
    ret = sqlite3_exec(db, sq1, NULL, NULL, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }
    return 0;
}
int callback(void *arg, int f_num, char **f_value, char **f_name) //回调
{
    if (*(int *)arg)
    {
        *(int *)arg = 0;
        for (int i = 0; i < f_num; i++)
        {
            printf("|%s", f_name[i]);
        }
        printf("|\n");
    }
    for (int i = 0; i < f_num; i++)
    {
        printf("|%s", f_value[i]);
    }
    printf("|\n");

    return 0;
}

int show_student(sqlite3 *db) //显示
{
    int id = 0;         //学号
    int age = 0;        //年龄
    char name[N] = {0}; //名字
    float score = 0;    //成绩
    char sq1[N] = {0};
    char *errmsg;
    int ret;
    sprintf(sq1, "select * from student;");
    printf("sq1=%s\n", sq1);
    int flag = 1;
    ret = sqlite3_exec(db, sq1, callback, (void *)&flag, &errmsg);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", errmsg);
        exit(-1);
    }

    return 0;
}

int quit_student(sqlite3 *db) //退出
{
    sqlite3_close(db);
    return 0;
}
int main(int argc, char const *argv[])
{
    sqlite3 *db;
    char *errmsg;
    int ret = 0;
    int choose = 0;
    char sq1[N] = {0};

    if (argc != 2)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./02-seqlite3 filename)\n");
        exit(-1);
    }
    ret = sqlite3_open(argv[1], &db);
    if (ret != SQLITE_OK)
    {
        printf("%s\n", sqlite3_errmsg(db));
        exit(-1);
    }

    while (1)
    {
        printf("***********************************************\n");
        printf("**1:插入记录 2:删除记录 3：显示记录 4：退出程序\n");
        printf("***********************************************\n");
        printf("input >:");
        scanf("%d", &choose);
        switch (choose)
        {
        case 1:
            insert_student(db);
            continue;
        case 2:
            delete_student(db);
            continue;
        case 3:
            show_student(db);
            continue;
        case 4:
            quit_student(db);
            break;
        default:
            break;
        }
        break;
    }
    return 0;
}
