﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QTextCodec>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // 设置myProcess的信号处理函数
    // 表示这个进程有数据可读, 表示标准输出有数据了, 表示进程输出数据了
    connect(&myProcess, &QProcess::readyRead, this, &MainWindow::showResult);
    // 进程状态改变时, 会被激发
    connect(&myProcess, &QProcess::stateChanged,this, &MainWindow::showState);
    // 进程有错误时会发送这个信号
    connect(&myProcess, &QProcess::errorOccurred,this, &MainWindow::showError);
    // 进程退出时的状态
    connect(&myProcess, SIGNAL(finished(int,QProcess::ExitStatus)),
            this, SLOT(showFinished(int, QProcess::ExitStatus)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString program = "D:/build/build-06-notepad-Exe/notepad.exe";
    QStringList arguments;
    //arguments << "/c dir&pause";
    myProcess.start(program, arguments);
}

void MainWindow::showResult()
{
    QTextCodec *codec = QTextCodec::codecForLocale();
    qDebug() << "showResult: " << endl << codec->toUnicode(myProcess.readAll());
}

void MainWindow::showState(QProcess::ProcessState state)
{
    qDebug() << "showState: ";
    if (state == QProcess::NotRunning) {
        qDebug() << "Not Running";
    } else if (state == QProcess::Starting) {
        qDebug() << "Starting";
    }  else {
        qDebug() << "Running";
    }
}

void MainWindow::showError()
{
    qDebug() << "showError: " << endl << myProcess.errorString();
}

void MainWindow::showFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    qDebug() << "showFinished: " << endl << exitCode << exitStatus;
}


