﻿#include "widget.h"
#include "ui_widget.h"
#include <QMouseEvent>
#include <QDebug>
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    QCursor cursor;                      // 创建光标对象
    cursor.setShape(Qt::OpenHandCursor); // 设置光标形状
    setCursor(cursor);                   // 使用光标
}

Widget::~Widget()
{
    delete ui;
}

void Widget::mousePressEvent(QMouseEvent *event) // 鼠标按下事件
{
    if(event->button() == Qt::LeftButton){       // 如果是鼠标左键按下
        qDebug()<<"鼠标左键被按下";
        QCursor cursor;
        cursor.setShape(Qt::ClosedHandCursor); // 设置光标形状为关闭的手
        QApplication::setOverrideCursor(cursor); // 使鼠标指针暂时改变形状
        //globalPos()这个坐标点返回的是桌面的坐标点，就是屏幕上的坐标点
        //我们的屏幕是1920 X 1080 的屏幕，返回基于这个坐标
        qDebug()<<"globalPos()"<<event->globalPos() ; // 获取全局坐标, 这个坐标是屏幕的坐标点
        qDebug()<<"pos()      "<< pos(); //鼠标所在窗体中的位置, 窗体的左上角为0
        //pos 是基于这个窗体的坐标，返回的窗体左上角的点 离 桌面坐标点的位置
        offset = event->globalPos() - pos();    // 获取指针位置和窗口位置的差值
        qDebug()<<"offset="<<offset;
    }
    else if(event->button() == Qt::RightButton){ // 如果是鼠标右键按下
        QCursor cursor(QPixmap(":/images/logo.png"));
        QApplication::setOverrideCursor(cursor);// 使用自定义的图片作为鼠标指针
    }
}

// 可设置点击后移动, 认为是鼠标移动
// 也可以实时的捕捉
void Widget::mouseMoveEvent(QMouseEvent *event) // 鼠标移动事件
{
    qDebug()<<"鼠标移动";
    if(event->buttons() & Qt::LeftButton){      // 这里必须使用buttons()
        QPoint temp;
        temp = event->globalPos() - offset;
        move(temp);// 使用鼠标指针当前的位置减去差值，就得到了窗口应该移动的位置
    }
}

void Widget::mouseReleaseEvent(QMouseEvent *event) // 鼠标释放事件
{
    Q_UNUSED(event);
    QApplication::restoreOverrideCursor();         // 恢复鼠标指针形状
}

void Widget::mouseDoubleClickEvent(QMouseEvent *event) // 鼠标双击事件
{
    if(event->button() == Qt::LeftButton){             // 如果是鼠标左键按下
        if(windowState() != Qt::WindowFullScreen)      // 如果现在不是全屏
            setWindowState(Qt::WindowFullScreen);      // 将窗口设置为全屏
        else setWindowState(Qt::WindowNoState);        // 否则恢复以前的大小
    }
}

void Widget::wheelEvent(QWheelEvent *event)    // 滚轮事件
{
    if(event->delta() > 0)
    {                                  // 当滚轮远离使用者时
        ui->textEdit->zoomIn();                // 进行放大
    }
    else
    {                                     // 当滚轮向使用者方向旋转时
        ui->textEdit->zoomOut();               // 进行缩小
    }
}

