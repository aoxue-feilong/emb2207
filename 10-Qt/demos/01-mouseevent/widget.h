﻿#ifndef WIDGET_H
#define WIDGET_H
#pragma execution_character_set("utf-8")//处理编码乱的问题

#include <QWidget>

namespace Ui
{
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    QPoint offset;                       // 用来储存鼠标指针位置与窗口位置的差值

protected:
    // 这几个函数是虚函数, 这里时重新实现QWidget的函数, 子类覆盖父类
    void mousePressEvent(QMouseEvent *event);  // 鼠标按压事件
    void mouseReleaseEvent(QMouseEvent *event); // 鼠标释放事件
    void mouseDoubleClickEvent(QMouseEvent *event); // 鼠标双击事件
    void mouseMoveEvent(QMouseEvent *event);   // 鼠标移动事件
    void wheelEvent(QWheelEvent *event);   //滚轮事件

};

#endif // WIDGET_H
