﻿#include <QtCore>
#include <stdio.h>
#include <stdlib.h>
#include <QDebug>

const int DataSize = 10;
const int BufferSize = 5;
char buffer[BufferSize];
QSemaphore freeBytes(BufferSize);  // 创建有5个资源的信号量, 信号量的值为5
QSemaphore usedBytes; // 信号量的值默认为0

// 生产者线程
class Producer : public QThread
{
public:
    // 重新实现run方法
    void run();
};

void Producer::run()
{
    // qsrand 随机数产生
    // QTime
    // QTime(0,0,0).secsTo(QTime::currentTime()) 把时间转换成秒数
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime())); // 安装随机因子
    for (int i = 0; i < DataSize; ++i)
    {
        // freeBytes 这个信号量的值为5 , 最多可以被申请5次,
        freeBytes.acquire(); // 申请1个信号量

        // (int)qrand() % 4 这个随机值为 0-3 之间的数
        // "ACGT"[(int)qrand() % 4] 随机访问字符串中的一个字符
        buffer[i % BufferSize] = "ACGT"[(int)qrand() % 4];
        qDebug() << QString("%0:producer: %1").arg(i+1).arg(buffer[i % BufferSize]);
        usedBytes.release(); // 释放信号量, 一共被释放了5次, 最后的值为5
    }
}

// 消费者线程
class Consumer : public QThread
{
public:
    // 重新实现run方法
    void run();
};

void Consumer::run()
{
    for (int i = 0; i < DataSize; ++i) {
        //usedBytes 的值在消费者线程中一共被释放了5次, 因此消费者也可以执行5次
        usedBytes.acquire();
        qDebug() << QString("%0:consumer: %1").arg(i+1).arg(buffer[i % BufferSize]);
        freeBytes.release();
    }
}


int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    // Producer 生产者 , 定义这个类对象
    Producer producer;

    // consumer 消费者 , 定义这个类对象
    Consumer consumer;
    producer.start(); //  会执行Producer 的run函数
    consumer.start(); //  会执行Consumer 的run函数
    producer.wait();
    consumer.wait();
    return app.exec();
}
