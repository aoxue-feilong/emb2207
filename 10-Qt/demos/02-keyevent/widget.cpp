﻿#include "widget.h"
#include "ui_widget.h"


widget::widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::widget)
{
    ui->setupUi(this);
}

widget::~widget()
{
    delete ui;
}

void widget::keyPressEvent(QKeyEvent *event)//键盘按下事件
{
    qDebug()<<"QKeyEvent";
    if(event->modifiers() ==Qt::ControlModifier)//是否按下Ctrl键
    {
        if(event->key() == Qt::Key_M)//是否按下M键
        {
            setWindowState(Qt::WindowMaximized);//窗口最大化
        }
        else if(event->key() == Qt::Key_N)//是否按下M键
        {
            setWindowState(Qt::WindowNoState);//窗口正常
        }
        else if(event->key() == Qt::Key_B)//是否按下B键
        {
            setWindowState(Qt::WindowMinimized);//窗口最小化
        }
        else if(event->key() == Qt::Key_V)//是否按下V键
        {
            setWindowState(Qt::WindowFullScreen);//窗口全屏化
        }
        else if(event->key() == Qt::Key_C)//是否按下C键
        {
            setWindowState(Qt::WindowActive);//窗口被选中
        }
    }

}
void widget::keyReleaseEvent(QKeyEvent *event)//按键释放事件
{
    qDebug()<<"keyReleaseEvent";
}
