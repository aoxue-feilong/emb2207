﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

// 启动线程按钮
void Dialog::on_startButton_clicked()
{
    // 调用start 函数后,thread会自动执行run函数,相当于执行run函数
    thread.start();
    ui->startButton->setEnabled(false);
    ui->stopButton->setEnabled(true);

}

//　终止线程按钮
void Dialog::on_stopButton_clicked()
{
    // 判断线程是否在运行, 运行时返回为真, 否则返回为假
    if (thread.isRunning())
    {
        // 停止线程
        thread.stop();
        ui->startButton->setEnabled(true);
        ui->stopButton->setEnabled(false);
    }
}
