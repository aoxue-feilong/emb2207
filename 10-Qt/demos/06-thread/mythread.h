﻿#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>

class MyThread : public QThread
{
    Q_OBJECT
public:
    explicit MyThread(QObject *parent = 0);
    void stop();
protected:
    // 这是一个虚函数, 需要去重新实现
    // 在调用start函数时, 默认会调用run函数 , 因此要重新实现run函数功能
    void run();
private:
    volatile bool stopped;
};


#endif // MYTHREAD_H
