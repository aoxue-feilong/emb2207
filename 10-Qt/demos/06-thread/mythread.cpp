﻿#include "mythread.h"
#include <QDebug>

MyThread::MyThread(QObject *parent) :
    QThread(parent)
{
    stopped = false;
}

void MyThread::run()
{
    qreal i = 0;  // double 类型
    while (!stopped)
    {
        qDebug() << QString("in MyThread: %1").arg(i);
        msleep(1000); // 延时1000ms
        i++;
    }
    stopped = false;
}

void MyThread::stop()
{
    stopped = true;
}
