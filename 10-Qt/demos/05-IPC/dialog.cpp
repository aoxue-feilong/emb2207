﻿#include "dialog.h"
#include "ui_dialog.h"
#include <QFileDialog>
#include <QBuffer>
#include <QDebug>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    // 就是唯一id号, 类似于 linux进程间通信的ipc的key
    sharedMemory.setKey("QSharedMemoryExample");
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::loadFromFile()
{
    // 这个共享内存是否被映射
    // 如果这个共享内存被映射了,则取消映射
    if (sharedMemory.isAttached())
        detach();
    ui->label->setText(tr("选择一个图片文件！"));
    QString fileName = QFileDialog::getOpenFileName(0, QString(), QString(),
                                                    tr("Images (*.png *.jpg)"));
    QImage image;
    if (!image.load(fileName)) {
        ui->label->setText(tr("选择的文件不是图片，请选择图片文件！"));
        return;
    }
    ui->label->setPixmap(QPixmap::fromImage(image));

    // 将图片加载到共享内存
    // 把QBuffer理解成一个文件
    QBuffer buffer;
    buffer.open(QBuffer::ReadWrite);

    // 把QDataStream 理解成cout即可
    QDataStream out(&buffer);
    out << image; // 把图片写入到buffer内 ,buffer是一个文件

    int size = buffer.size(); // 计算文件的大小

    // 创建指定字节数的共享内存
    if (!sharedMemory.create(size)) {
        ui->label->setText(tr("无法创建共享内存段！"));
        return;
    }

    sharedMemory.lock(); // 对共享内存上锁 , 让制别的进程访问, 主要目的是实现互斥

    char *to = (char*)sharedMemory.data();  // 把共享内存转换成char* 的指针
    const char *from = buffer.data().data();// 获取文件的内容的地址

    // 文件的内容写入到共享内存中
    // to 共享内存地址
    // from 文件的内存地
    memcpy(to, from, qMin(sharedMemory.size(), size));
    sharedMemory.unlock(); // 解锁共享内存 , 别的进程或线程可以访问这块共享内存
}

void Dialog::loadFromMemory()
{
    // 映射一块共享内存
    if (!sharedMemory.attach())
    {
        ui->label->setText(tr("无法连接到共享内存段，\n"
                              "请先加载一张图片！"));
        return;
    }
    QBuffer buffer;
    QDataStream in(&buffer);
    QImage image;

    sharedMemory.lock(); // 上锁共享内存

    // 把共享内存的数据写入到buffer内
    buffer.setData((char*)sharedMemory.constData(), sharedMemory.size());

    // 打开buffer 文件
    buffer.open(QBuffer::ReadOnly);

    // in 就是buffer的流形式
    // 把buffer中的内容写入到image内
    in >> image;
    sharedMemory.unlock(); // 解锁共享内存

    sharedMemory.detach(); // 取消共享内存映射
    ui->label->setPixmap(QPixmap::fromImage(image)); // 把图片显示到label中
}

void Dialog::detach()
{
    if (!sharedMemory.detach())
        ui->label->setText(tr("无法从共享内存中分离！"));
}


void Dialog::on_loadFromFileButton_clicked()
{
    // 单击按钮后, 加载一张图片
    loadFromFile();
}

void Dialog::on_loadFromSharedMemoryButton_clicked()
{
    // 单击按钮后, 加载一块共享内存
    loadFromMemory();
}
