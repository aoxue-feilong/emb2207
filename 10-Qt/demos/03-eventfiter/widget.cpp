﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //把textEdit 这个控件加入到监控集合内，和linux中的select类似
    //加入集合内以后，如果textEdit和spinBox 触发事件后，在Widget中使用eventFilter函数去处理事件的发生
    ui->textEdit->installEventFilter(this);//为编辑器部位在本窗口上安装过滤器
    ui->spinBox->installEventFilter(this);//为编辑器部位在本窗口上安装过滤器

    QKeyEvent myEvent(QEvent::KeyPress, Qt::Key_Up,Qt::NoModifier);
    qApp->sendEvent(ui->spinBox,&myEvent);//发送键盘事件到spinBox部件

}

Widget::~Widget()
{
    delete ui;
}
// obj 表示发生事件的对象
// event 发生了什么事件
bool Widget::eventFilter(QObject *obj,QEvent *event)//事件过滤器
{
    if(obj == ui->textEdit)//判断部件
    {
        if(event->type()==QEvent::Wheel)//判断事件
        {
            //c++的强制类型转换
            //将event强制为发生的事件的类型
            QWheelEvent *wheelEvent = static_cast<QWheelEvent*>(event);
            if(wheelEvent->delta()>0)//当滚轮远离使用者时
            {
                ui->textEdit->zoomIn();//进行放大
            }
            else//当滚轮靠近使用者时
            {
                ui->textEdit->zoomOut();//进行缩小
            }
            return true;//该事件已经被处理
        }
        else
        {
            return false;//如果是其他事件，可以进一步的处理
        }
    }
    else if(obj == ui->spinBox)
    {
        if(event->type()==QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
            if(keyEvent->key()==Qt::Key_Space)//如果是空格
            {
                ui->spinBox->setValue(0);
                return true;
            }
            else
            {
                return false;
            }
        } else {
            return false;
        }
    }
    else return QWidget::eventFilter(obj,event);
}


