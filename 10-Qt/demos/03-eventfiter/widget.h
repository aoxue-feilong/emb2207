﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<QKeyEvent>
#include<QWheelEvent>


QT_BEGIN_NAMESPACE
namespace Ui {
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    //设置事件过滤器，就是可以使用Widget 来监控其他控件的事件
    bool eventFilter(QObject *obj,QEvent *event);

private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
