﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma execution_character_set("utf-8")//解决中文问题
#include <QMainWindow>
#include <QSerialPort>

#include <QSettings>
#include <QSerialPortInfo>
#include <QDebug>
#include <QMessageBox>
#include <QQueue>
#include <QTimer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QHostInfo>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:


    void newConnection_slot();//有新客户端连接的信号处理函数

    void client_readyRead_slot();//收到客户端的数据

    void client_disconnected_slot();//客户端断开连接后的信号处理函数

    void on_pushButton_open_clicked(bool checked);

    void servial_time_slot();//定时时间到了的信号处理函数


    void on_pushButton_led1_clicked(bool checked);

    void on_pushButton_led2_clicked(bool checked);

    void on_pushButton_beep_clicked(bool checked);



private:
    Ui::MainWindow *ui;
    QQueue<char> serverQueue;
    QTimer *serverTimer=nullptr;
    QTcpServer *serverSocket=nullptr;
    QTcpSocket *clinetSocket=nullptr;
};
#endif // MAINWINDOW_H
