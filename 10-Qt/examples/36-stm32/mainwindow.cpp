﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //定义一个定时器
    serverTimer=new QTimer;
    connect(serverTimer,&QTimer::timeout,this,&MainWindow::servial_time_slot);
    // 获取本地的ip地址
    // hostname 就是计算机名称
    qDebug()<<"hostname:"<< QHostInfo::localHostName();
    // 获取主机名的目的是为了获取主机的ip地址, 可以使用方法fromName
    // fromName 函数实现了, 根据主机名获取主机的ip地址
    // fromName 函数返回值为  QHostInfo,  , 一个主机可以有多个ip地址
    // 再使用方法addresses() 获取这个主机名的所有地址信息
    QHostInfo info =  QHostInfo::fromName(QHostInfo::localHostName()) ; // 返回一个QHostInfo对象
    foreach (QHostAddress addr, info.addresses())
    {
        //addr.protocol()里面有iPV6信号
        qDebug()<<"addr.protocol()="<<addr.protocol();
        //判断是否IPV4信号（过滤一下）
        if(addr.protocol() == QAbstractSocket::IPv4Protocol) // 只获取ipv4地址类型
        {
            //输出主机的ip地址
            qDebug()<<"addr"<< addr;
            //将地址输出到comboBox_ip上面
            ui->comboBox_ip->addItem(addr.toString());

        }

    }
    // 设置端口
    ui->comboBox_port->setEditText("8000");
    //绘制背景图片
    QPalette PAllbackground=this->palette();
    QImage ImgAllbackground(QString::fromUtf8(":/images/pack.jpg"));
    QImage pix=ImgAllbackground.scaled(this->width(),this->height(),
                                       Qt::IgnoreAspectRatio);
    PAllbackground.setBrush(QPalette::Window,QBrush(pix));
    this->setPalette(PAllbackground);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::servial_time_slot()
{
    //每个一秒发送一次全部传感器命令
    static quint32 count=0;
    count ++;
    if(count>=4)//一秒计时时间到
    {
        count=0;
        QString cmd="a7ba8ba9baAbaBbaCbaDb";
        if(clinetSocket!=nullptr)
        {
            clinetSocket->write(cmd.toUtf8());
        }

    }

    while(!serverQueue.isEmpty())
    {
        if(serverQueue.dequeue()=='c')//取第一个数
        {
            char s0,s1,s2;
            if(serverQueue.isEmpty())  return;
            s0=serverQueue.dequeue();
            if(s0 == '1')
            {
                if(serverQueue.isEmpty())  return;//每一次取词判断队列是否为空，为空立即返回
                s1=serverQueue.dequeue();
                if(serverQueue.isEmpty())  return;
                s2=serverQueue.dequeue();

                if(QString(s1)+QString(s2)=="sd")//led1 亮灯成功
                {
                    ui->pushButton_led1->setIcon(QIcon(QString::fromUtf8(":/images/ledon.png")));
                    qDebug()<<"led1 on ";
                }
                else if(QString(s1)+QString(s2)=="fd")
                {
                    qDebug()<<"led1 on failure";
                }
            }
            else if(s0 == '2')//led1 亮灯失败
            {
                if(serverQueue.isEmpty())  return;
                s1=serverQueue.dequeue();
                if(serverQueue.isEmpty())  return;
                s2=serverQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")
                {
                    ui->pushButton_led1->setIcon(QIcon(QString::fromUtf8(":/images/ledoff.png")));
                    qDebug()<<"led1 on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//led1 亮灯失败
                {
                    qDebug()<<"led1 on failure";
                }
            }
            else if(s0 == '3')//led2 亮灯
            {
                if(serverQueue.isEmpty())  return;
                s1=serverQueue.dequeue();
                if(serverQueue.isEmpty())  return;
                s2=serverQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")//蜂鸣器响
                {
                    ui->pushButton_led2->setIcon(QIcon(QString::fromUtf8(":/images/ledon.png")));
                    qDebug()<<"led2 on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//蜂鸣器不响
                {
                    qDebug()<<"led2 on failure";
                }
            }
            else if(s0 == '4')//led2 亮灯失败
            {
                if(serverQueue.isEmpty())  return;
                s1=serverQueue.dequeue();
                if(serverQueue.isEmpty())  return;
                s2=serverQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")//蜂鸣器响
                {
                    ui->pushButton_led2->setIcon(QIcon(QString::fromUtf8(":/images/ledoff.png")));
                    qDebug()<<"led2 on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//蜂鸣器不响
                {
                    qDebug()<<"led2 on failure";
                }
            }
            else if(s0 == '5')//蜂鸣器响
            {
                if(serverQueue.isEmpty())  return;
                s1=serverQueue.dequeue();
                if(serverQueue.isEmpty())  return;
                s2=serverQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")//蜂鸣器响
                {
                    ui->pushButton_beep->setIcon(QIcon(QString::fromUtf8(":/images/beepon.png")));
                    qDebug()<<"beep on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//蜂鸣器不响
                {
                    qDebug()<<"beep on failure";
                }
            }
            else if(s0 == '6')//蜂鸣器不响
            {
                if(serverQueue.isEmpty())  return;
                s1=serverQueue.dequeue();
                if(serverQueue.isEmpty())  return;
                s2=serverQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")//蜂鸣器响
                {
                    ui->pushButton_beep->setIcon(QIcon(QString::fromUtf8(":/images/beepoff.png")));
                    qDebug()<<"beep on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//蜂鸣器不响
                {
                    qDebug()<<"beep on failure";
                }
            }
            else if(s0 == 'A')//锂电池电压
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serverQueue.isEmpty()) && ((s1=serverQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_BAT->display(data);

            }
            else if(s0 == 'B')//温度
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serverQueue.isEmpty()) && ((s1=serverQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_tem->display(data);

            }
            else if(s0 == 'C')//湿度
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serverQueue.isEmpty()) && ((s1=serverQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_hum->display(data);

            }
            else if(s0 == 'D')//光线强度
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serverQueue.isEmpty()) && ((s1=serverQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_LX->display(data);

            }
            else if(s0 == 'E')//大气压
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serverQueue.isEmpty()) && ((s1=serverQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_air->display(data);

            }
            else if(s0 == 'F')//电位器
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serverQueue.isEmpty()) && ((s1=serverQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_VR->display(data);

            }
            else if(s0 == 'G')//cpu温度
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serverQueue.isEmpty()) && ((s1=serverQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_CPU->display(data);

            }
            else if(s0 == 'H')//RFID
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serverQueue.isEmpty()) && ((s1=serverQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lineEdit_entrance->setText(data);

            }


        }

    }





}
//有客户端连接时的信号处理函数
void MainWindow::newConnection_slot()
{
    qDebug()<<"new client is connected";

    if(serverSocket != nullptr)
    {
        clinetSocket =  serverSocket->nextPendingConnection() ; // 等于linux c 的accept 函数功能
        // 安装client 的信号处理函数
        // 接收到数据时, 发送 readyRead() 信号
        connect(clinetSocket,&QTcpSocket::readyRead,this,&MainWindow::client_readyRead_slot);

        //设置客户端的断开连接后的信号处理函数disconnected()
        connect(clinetSocket,&QTcpSocket::disconnected,this,&MainWindow::client_disconnected_slot);

        ui->pushButton_client->setIcon(QIcon(QString::fromUtf8(":/images/open.png")));

        // 串口成功打开后, 启动定时器开始计时
        serverTimer->start(500) ;  // 启动定时器, 定时时间1秒


    }
}
//客户端断开连接后的信号处理函数
void MainWindow::client_disconnected_slot()
{
    //把客户端的指示灯熄灭
    ui->pushButton_client->setIcon(QIcon(QString::fromUtf8(":/images/close.png")));
    // 关闭串口后 , 关闭定时器
    serverTimer->stop() ;  // 关闭定时器
}
//串口收到数据后的信号处理函数
void MainWindow::client_readyRead_slot()
{
    if(clinetSocket != nullptr)
    {
        QByteArray data=clinetSocket->readAll();
        for (int i = 0; i < data.count(); i++)
        {
            serverQueue.enqueue(char(data.at(i))) ;//入队
        }
        qDebug()<<"data"<<data;
    }
}

void MainWindow::on_pushButton_open_clicked(bool checked)
{
    qDebug()<<"checked="<<checked ;
    if(checked) // 为真时
    {

        // 启动TCP server 服务器
        serverSocket = new QTcpServer(this);
        // 设置信号处理函数
        // 客户端连接成功时
        connect(serverSocket,&QTcpServer::newConnection,this,&MainWindow::newConnection_slot);
        QHostAddress addr(ui->comboBox_ip->currentText());
        quint16 port  = ui->comboBox_port->currentText().toUShort();
        if( serverSocket->listen(addr,port) )  // 启动监听, 这个函数等于linux c 的bind 和listen两个函数
        {

            // 切换pushButton_open的图片为开状态图片 ledon.png
            QIcon icon1(QString::fromUtf8(":/images/open.png"));
            ui->pushButton_open->setIcon(icon1);
            ui->pushButton_open->setText(tr("关闭TCP服务器"));
        }
        else // 启动失败
        {
            serverSocket->close();
            ui->pushButton_open->setChecked(false);// 把按钮状态设置位弹出  (按下与弹出)
        }


    }
    else // 为假
    {
        // 切换pushButton_open的图片为关状态图片 close.png
        QIcon icon1(QString::fromUtf8(":/images/close.png"));
        ui->pushButton_open->setIcon(icon1);
        ui->pushButton_open->setText(tr("打开TCP服务器"));
        if(serverSocket != nullptr)
        {
            serverSocket->close(); //关闭服务器
            delete serverSocket ;
            serverSocket = nullptr ;
        }


    }


}




void MainWindow::on_pushButton_led1_clicked(bool checked)
{
    QString cmd="";
    if(checked)//为真开灯
    {
        cmd="a1b";
    }
    else//为假关灯
    {
        cmd="a2b";
    }
    if(serverSocket !=nullptr)
    {
        clinetSocket->write(cmd.toUtf8());
    }

}


void MainWindow::on_pushButton_led2_clicked(bool checked)
{
    QString cmd="";
    if(checked)//为真开灯
    {
        cmd="a3b";
    }
    else//为假关灯
    {
        cmd="a4b";
    }
    if(serverSocket !=nullptr)
    {
        clinetSocket->write(cmd.toUtf8());
    }
}

void MainWindow::on_pushButton_beep_clicked(bool checked)
{
    QString cmd="";
    if(checked)//为真响
    {
        cmd="a5b";
    }
    else//为假不响
    {
        cmd="a6b";
    }
    if(serverSocket !=nullptr)
    {
        clinetSocket->write(cmd.toUtf8());
    }
}


