﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->comboBox_gender->setCurrentIndex(0);
    ui->lineEdit_name->setText(tr("李成龙"));
    ui->lineEdit_age->setText(tr("22"));
    ui->lineEdit_score->setText(tr("100"));
    ui->lineEdit_number ->setText(tr("207001"));
    QStringList lists=QSqlDatabase::drivers();
    foreach(QString l,lists)
    {
        qDebug()<<"支持驱动"<<l;
    }
    db=QSqlDatabase::addDatabase(("QSQLITE"));
    db.setDatabaseName("my.db");
    bool ok=db.open();
    if(!ok)
    {
        QMessageBox::warning(this,tr("sqlitr3"),tr("数据库打开失败"));
    }
    model = new QStandardItemModel(this);
    /*设置列字段名*/
    model->setColumnCount(5);   // 设置 一行有5列
    model->setHeaderData(0,Qt::Horizontal, tr("学号"));  // 第一列名 学号
    model->setHeaderData(1,Qt::Horizontal, tr("姓名"));  // 第二列名 姓名
    model->setHeaderData(2,Qt::Horizontal, tr("性别"));  // 第二列名 性别
    model->setHeaderData(3,Qt::Horizontal, tr("年龄"));  // 第二列名 年龄
    model->setHeaderData(4,Qt::Horizontal, tr("成绩"));  // 第二列名 成绩

    ui->tableView->horizontalHeader()->setDefaultSectionSize(ui->tableView->size().width()/5-1); // 设置水平头的宽度
    ui->tableView->verticalHeader()->setHidden(true); // 行名隐藏
    ui->tableView->setModel(model) ; // 给tableview 安装一个模型 model
    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    //设置表格的单元为只读属性，即不能编辑
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    showAll();
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_create_clicked()
{
    //创建数据库的表
    //执行一个sql语句，使用qsqlquery
    QString sql="create table student(number int primary key,name char,"
                "gender char,age int,score float);";
    QSqlQuery query;//创建一个执行sql语句的对象
    bool ok=query.exec(sql);
    if(!ok)
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("创建表失败"),query.lastError().text());
        return;
    }
    else
    {
        QMessageBox::information(this,tr("创建表成功"),tr("创建student表成功"));
    }
    showAll();
}

//显示数据库中所有的成员
void Widget::showAll()
{
    // 清空原有记录（显示之前把原来的清空，不然会重复）
    model->removeRows(0,lineCount);//从0行 一直到删除到指定的行
    lineCount=0;//行号清空
    //创建数据库的表
    //执行一个sql语句，使用qsqlquery
    QString sql="select * from student;";

    QSqlQuery query;//创建一个执行sql语句的对象
    bool ok=query.exec(sql);
    if(!ok)
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("查询表失败"),query.lastError().text());
        return;
    }
    else
    {
        while(query.next())//为真表示找到记录
        {
            qDebug()<<"number="<<query.value(0).toInt();//第1列的值
            qDebug()<<"name="<<query.value(1).toString();//第2列的值
            qDebug()<<"gender="<<query.value(2).toString();//第3列的值
            qDebug()<<"age="<<query.value(3).toInt();//第4列的值
            qDebug()<<"score="<<query.value(4).toFloat();//第5列的值
            if(model != nullptr)
            {
                /*设置一条数据*/
                model->setItem(lineCount, 0, new QStandardItem(query.value(0).toString()));
                model->setItem(lineCount, 1, new QStandardItem(query.value(1).toString()));
                model->setItem(lineCount, 2, new QStandardItem(query.value(2).toString()));
                model->setItem(lineCount, 3, new QStandardItem(query.value(3).toString()));
                model->setItem(lineCount, 4, new QStandardItem(query.value(4).toString()));

                model->item(lineCount, 0)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                model->item(lineCount, 1)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                model->item(lineCount, 2)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                model->item(lineCount, 3)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                model->item(lineCount, 4)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐

                ui->tableView->setModel(model) ; // 给tableview 安装一个模型 model
                lineCount ++;
            }
        }

    }
}
void Widget::on_pushButton_insert_clicked()
{
    //创建数据库的表
    //执行一个sql语句，使用qsqlquery
    QString sql=tr("insert into student values(%0,'%1','%2',%3,%4);")
            .arg(ui->lineEdit_number->text().toInt())
            .arg(ui->lineEdit_name->text())
            .arg(ui->comboBox_gender->currentText())
            .arg(ui->lineEdit_age->text().toInt())
            .arg(ui->lineEdit_score->text().toFloat());
    QSqlQuery query;//创建一个执行sql语句的对象
    bool ok=query.exec(sql);
    if(!ok)
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("创建表失败"),query.lastError().text());
        return;
    }
    else
    {
        QMessageBox::information(this,tr("插入表成功"),tr("插入记录成功"));
    }
    showAll();
}


void Widget::on_pushButton_showAll_clicked()
{
    showAll();
}




void Widget::on_pushButton_delete_clicked()
{
    // 创建数据库的表
        // 执行一个sql语句 , 使用qsqlquery
        QString sql = tr("delete from student where number = %0;")
                        .arg(ui->lineEdit_number->text().toInt());

        QSqlQuery query; // 创建一个执行sql语句的对象
        bool ok =  query.exec(sql);
        if(!ok) // 执行sql 语句失败
        {
            qDebug()<<query.lastError().text();
            QMessageBox::critical(this,tr("删除记录错误"),query.lastError().text());
            return  ;
        }
        else
        {
            QMessageBox::information(this,tr("删除成功"),tr("删除记录成功") );
        }
        showAll();

}


void Widget::on_pushButton_modify_clicked()
{
    // 创建数据库的表
        // 执行一个sql语句 , 使用qsqlquery
        // update student set number=4,score=60 where number=5;
        QString sql = tr("update student set name='%1',gender='%2',age=%3,score=%4 where number=%5;")

                        .arg(ui->lineEdit_name->text())
                        .arg(ui->comboBox_gender->currentText())
                        .arg(ui->lineEdit_age->text().toInt())
                        .arg(ui->lineEdit_score->text().toFloat())
                        .arg(ui->lineEdit_number->text().toInt())  ;

        QSqlQuery query; // 创建一个执行sql语句的对象
        bool ok =  query.exec(sql);
        if(!ok) // 执行sql 语句失败
        {
            qDebug()<<query.lastError().text();
            QMessageBox::critical(this,tr("修改记录错误"),query.lastError().text());
            return  ;
        }
        else
        {
            QMessageBox::information(this,tr("修改成功"),tr("修改记录成功") );
        }
        showAll();
}


void Widget::on_pushButton_search_clicked()
{
    // 创建数据库的表
        // 执行一个sql语句 , 使用qsqlquery
        // select * from student where number=1;
        QString sql = tr("select * from student where number=%0;")
                        .arg(ui->lineEdit_number->text().toInt()) ;

        QSqlQuery query; // 创建一个执行sql语句的对象
        bool ok =  query.exec(sql);
        if(!ok) // 执行sql 语句失败
        {
            qDebug()<<query.lastError().text();
            QMessageBox::critical(this,tr("删除记录错误"),query.lastError().text());
            return  ;
        }
        else
        {

            // 下一个记录是否有效, 有效返回为真, 无效返回为假
            if(query.next()) // 为真表示找到了记录
            {
                qDebug()<<"number="<<query.value(0).toInt();  // 第1列的值
                qDebug()<<"name  ="<<query.value(1).toString();  // 第2列的值
                qDebug()<<"gender="<<query.value(2).toString();  // 第3列的值
                qDebug()<<"age   ="<<query.value(3).toInt();  // 第4列的值
                qDebug()<<"score ="<<query.value(4).toFloat();  // 第5列的值

                model->removeRows(0,lineCount);  // 从0行 一直删除到 指定的行
                lineCount = 0 ; // 行号清空
                /*设置一条数据*/
                model->setItem(lineCount, 0, new QStandardItem(query.value(0).toString())  );
                model->setItem(lineCount, 1, new QStandardItem(query.value(1).toString()) );
                model->setItem(lineCount, 2, new QStandardItem(query.value(2).toString()) );
                model->setItem(lineCount, 3, new QStandardItem(query.value(3).toString()) );
                model->setItem(lineCount, 4, new QStandardItem(query.value(4).toString()) );

                model->item(lineCount, 0)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                model->item(lineCount, 1)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                model->item(lineCount, 2)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                model->item(lineCount, 3)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                model->item(lineCount, 4)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
                ui->tableView->setModel(model) ; // 给tableview 安装一个模型 model
                lineCount ++;
            }
            else
            {
                QMessageBox::information(this,tr("查找成功"),tr("没有找到记录") );
                model->removeRows(0,lineCount);  // 从0行 一直删除到 指定的行
                lineCount = 0 ; // 行号清空
                return ;
            }

        }
}

