#include "widget.h"
#include "ui_widget.h"

widget::widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::widget)
{
    ui->setupUi(this);
    //对指针变量初始化
    tts=new QTextToSpeech(this);//让这个QTextToSpeech的父类本应用程序，当程序结束时，这个tts也会自动结束
    QStringList list=QTextToSpeech::availableEngines();//获取有效的引擎
    for(QString &engine:list)
    {
      qDebug()<<"engine"<<engine;
      ui->comboBox_engine->addItem(engine);
    }
    //对音量 音速 音高设置初始值
    ui->label_speed->setNum(ui->horizontalSlider_speed->value());
    ui->label_pitch->setNum(ui->horizontalSlider_pitch->value());
    ui->label_volume->setNum(ui->horizontalSlider_volume->value());

}

widget::~widget()
{
    delete ui;
}

void widget::on_comboBox_engine_activated(int index)
{
  qDebug()<<"on_comboBox_engine_activated:"<<index;
  //每次点击”语音引擎“ 按钮是 . 清空 "声音语言" 下拉列表中的内容
  ui->comboBox_language->clear();
  QVector<QLocale> language = tts->availableLocales();//获取语音引擎支持的语言

  for(QLocale local:language)
  {
      qDebug()<<"local"<<local;//显示所有
      qDebug()<<"local language:"<<local.language();//显示语言
      qDebug()<<"local country:"<<local.country();//显示国家
      qDebug()<<"country"<<QLocale::countryToString(local.country());//显示国家
      qDebug()<<"language"<<QLocale::languageToString(local.language());//显示语言
      qDebug()<<"script:"<<QLocale::scriptToString(local.script());//显示描述信息
      ui->comboBox_language->addItem(QString(tr("%0-%1-%2")
                                     .arg(QLocale::countryToString(local.country()))
                                     .arg(QLocale::languageToString(local.language()))
                                     .arg(QLocale::scriptToString(local.script()))
                                     ),QVariant(local)
                                     );

  }
  QVector<QVoice> voices = tts->availableVoices();//获取语音引擎支持的语言
  for(QVoice voice:voices)//C++的迭代器
  {
     // qDebug()<<"local"<<local;//显示所有
      //qDebug()<<"local language:"<<local.language();//显示语言
        qDebug()<<"genderName:"<<QVoice::genderName(voice.gender());//显示性别
        qDebug()<<"name"<<voice.name();//显示姓名
        ui->comboBox_gender->addItem(QString(tr("%0-%1-%2")
                                       .arg(voice.name())
                                       .arg(QVoice::genderName(voice.gender()))
                                       )
                                       );
 }
}

void widget::on_comboBox_language_activated(int index)
{
    qDebug()<<":on_comboBox_language_activated"<<index;
    if(tts!=nullptr)
    {

        tts->setLocale(ui->comboBox_language->currentData().toLocale());//设置 语音引擎要使用的语言
    }

}

void widget::on_comboBox_gender_activated(int index)
{
    qDebug()<<":on_comboBox_gender_activated"<<index;
    if(tts!=nullptr)
    {
        QVector<QVoice> voices = tts->availableVoices();//获取语音引擎支持的语言
        tts->setVoice(voices[index]);//设置 语音引擎要使用的语言
    }
}

void widget::on_pushButton_play_clicked()
{
    if(tts!=nullptr)
    {
        tts->say(ui->textEdit->toPlainText());//把文本控件中的内容语音播报出来
    }

}

void widget::on_pushButton_pause_clicked()
{
    if(tts!=nullptr)
    {
        tts->pause();//暂停播放
    }
}

void widget::on_pushButton_resume_clicked()
{
    if(tts!=nullptr)
    {
        tts->resume();//继续播放
    }
}

void widget::on_pushButton_stop_clicked()
{
    if(tts!=nullptr)
    {
        tts->stop();//停止播放
    }
}

void widget::on_horizontalSlider_speed_valueChanged(int value)
{
    //qDebug()<<"on_horizontalSlider_speed_valueChanged"<<value;
    ui->label_speed->setNum(value);//设置现实的数字
    //让0~100 的值转成-1.0到1.0之间的数
    double newvalue=(value-50)/50.0;//0~100的数减去50=-50到50 再除以50.0即可
    if(tts!=nullptr)
    {
        tts->setRate(newvalue);//设置播放语速
    }
}

void widget::on_horizontalSlider_volume_valueChanged(int value)
{

    ui->label_volume->setNum(value);//设置现实的数字
    //让0~100 的值转成0.0到1.0之间的数
    double newvalue=value/100.0;//0~100的数除以100.0即为0.0到1.0之间的数
    if(tts!=nullptr)
    {
        tts->setVolume(newvalue);//设置播放音量
    }
}


void widget::on_horizontalSlider_pitch_valueChanged(int value)
{
    ui->label_pitch->setNum(value);//设置现实的数字
    //让0~100 的值转成-1.0到1.0之间的数
    double newvalue=(value-50)/50.0;//0~100的数减去50=-50到50 再除以50.0即可
    if(tts!=nullptr)
    {
        tts->setPitch(newvalue);//设置播放音调
    }
}

