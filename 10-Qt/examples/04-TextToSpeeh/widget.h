#ifndef WIDGET_H
#define WIDGET_H
#include <QWidget>
#include <QTextToSpeech>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class widget; }
QT_END_NAMESPACE

class widget : public QWidget
{
    Q_OBJECT

public:
    widget(QWidget *parent = nullptr);
    ~widget();

private slots:
    void on_comboBox_engine_activated(int index);

    void on_comboBox_language_activated(int index);

    void on_comboBox_gender_activated(int index);

    void on_pushButton_play_clicked();

    void on_pushButton_pause_clicked();

    void on_pushButton_resume_clicked();

    void on_pushButton_stop_clicked();

    void on_horizontalSlider_speed_valueChanged(int value);

    void on_horizontalSlider_volume_valueChanged(int value);

    void on_horizontalSlider_pitch_valueChanged(int value);

private:
    Ui::widget *ui;
    QTextToSpeech *tts;
};
#endif // WIDGET_H
