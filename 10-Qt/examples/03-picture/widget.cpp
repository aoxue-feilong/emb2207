#include "widget.h"
#include "./ui_widget.h"

widget::widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::widget)
{
    ui->setupUi(this);
    ui->radioButton_32->setChecked(true);

}

widget::~widget()
{
    delete ui;
}


void widget::on_pushButton_flie_clicked()
{
    qDebug()<<"on_pushButton_file_clicked()" ;

         fileName = QFileDialog::getOpenFileName(this,
                  tr("打开图片"),"./",tr("Image Files(*.png*.jpg*.pmp);;All files(*.*)"));
         qDebug()<<"fileName="<<fileName;
         ui->label_filename->setText(fileName);
}


void widget::on_pushButton_pre_clicked()
{
    if(ui->radioButton_16->isChecked())
        {
            this->width=16;
            this->height=16;
        }else if(ui->radioButton_32->isChecked())
        {
            this->width=32;
            this->height=32;
        }else if(ui->radioButton_64->isChecked())
        {
            this->width=64;
            this->height=64;
        }else if(ui->radioButton_128->isChecked())
        {
            this->width=128;
            this->height=128;
        }else if(ui->radioButton_256->isChecked())
        {
            this->width=256;
            this->height=256;
        }
        qDebug()<<"width ="<<width;
        qDebug()<<"height ="<<height;

        if(fileName.isEmpty())
       {
         qDebug()<<"文件名为空.  获取文件失败";
         return;
       }

        QPixmap map(fileName);
        QPixmap newmap = map.scaled(width,height);
        ui->label_preview->setPixmap(newmap);
}





void widget::on_pushButton_del_clicked()
{
    ui->label_filename->clear();
    ui->label_preview->clear();
}


void widget::on_pushButton_exit_clicked()
{
    this->close();
}


void widget::on_pushButton_save_clicked()
{
    if(ui->radioButton_16->isChecked())
        {
            this->width=16;
            this->height=16;
        }else if(ui->radioButton_32->isChecked())
        {
            this->width=32;
            this->height=32;
        }else if(ui->radioButton_64->isChecked())
        {
            this->width=64;
            this->height=64;
        }else if(ui->radioButton_128->isChecked())
        {
            this->width=128;
            this->height=128;
        }else if(ui->radioButton_256->isChecked())
        {
            this->width=256;
            this->height=256;
        }
        qDebug()<<"width ="<<width;
        qDebug()<<"height ="<<height;

        QPixmap map(fileName);
        QPixmap newmap = map.scaled(width,height);
       // ui->label_preview->setPixmap(newmap);


       fileName.clear();
       fileName = QFileDialog::getSaveFileName(this,tr("保存文件"),
                                      ".",tr("Images (*.ico )"));

       if(fileName.isEmpty())
       {
           qDebug()<<"文件名为空.  获取文件失败";
           return;
       }
       newmap.save(fileName);

}

