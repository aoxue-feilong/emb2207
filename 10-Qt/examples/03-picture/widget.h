#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDebug>
#include <QFileDialog>
#include <QPixmap>


QT_BEGIN_NAMESPACE
namespace Ui { class widget; }
QT_END_NAMESPACE

class widget : public QWidget
{
    Q_OBJECT

public:
    widget(QWidget *parent = nullptr);
    ~widget();

private slots:
    void on_pushButton_flie_clicked();

    void on_pushButton_pre_clicked();

    void on_pushButton_del_clicked();

    void on_pushButton_exit_clicked();

    void on_pushButton_save_clicked();

private:
    Ui::widget *ui;
    QString fileName;
    qint32 width,height;
};
#endif // WIDGET_H
