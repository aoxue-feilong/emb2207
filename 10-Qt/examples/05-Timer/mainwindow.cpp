﻿#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    appPointer = nullptr; // 对指针赋初始值

    QString data =  QString::asprintf("%02d:%02d.%02d",min,sec,ms);   // 拼接字符串
    ui->lcdNumber->display(data);


    // 定义一个定时器 , 等价于C语言的malloc
    timer = new QTimer(this);


    // 设置定时器的信号处理函数
    // 参数1  :  发送信号者 timer
    // 参数2  :  发送信号者 发送什么信号 , &类名::信号
    // 参数3  :  接收信号者 this, 就是这个应用程序MainWindow
    // 参数4  :  信号处理函数  用于自定义
    connect(timer,&QTimer::timeout,this ,&MainWindow::timerout_slot);

    // 启动定时器 , 10ms 执行一次信号处理函数
    // timer->start(10);


    model = new QStandardItemModel(this);
    /*设置列字段名*/
    model->setColumnCount(2);   // 设置 一行有2列
    model->setHeaderData(0,Qt::Horizontal, tr("序号"));  // 第一列名 序号
    model->setHeaderData(1,Qt::Horizontal, tr("数值"));  // 第二列名 数值


    ui->tableView->horizontalHeader()->setDefaultSectionSize(199); // 设置水平头的宽度
    ui->tableView->verticalHeader()->setHidden(true); // 行名隐藏
    ui->tableView->setModel(model) ; // 给tableview 安装一个模型 model


    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    //设置表格的单元为只读属性，即不能编辑
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);


}

MainWindow::~MainWindow()
{
    delete ui;
}


// 这个函数是信号处理函数
// 让定时器10ms执行一次timerout_slot
void MainWindow::timerout_slot()
{
    //qDebug()<<"timerout_slot():";
    ms++;
    if(ms >=100)
    {
        ms = 0 ;
        sec ++;
        if(sec >=60)
        {
            sec =0 ;
            min ++;
            if(min >=60)
            {
                min=0;
            }
        }
    }

    QString data =  QString::asprintf("%02d:%02d.%02d",min,sec,ms);   // 拼接字符串
    ui->lcdNumber->display( data );
    //qDebug()<<data;
}

void MainWindow::on_pushButton_start_clicked(bool checked)
{
    qDebug()<<"on_pushButton_start_clicked():"<<checked;
    if(checked) // true 按键第一次按下去后, 为true
    {
        // true 1: 中文   false 0:是英文 , 默认是中文
        if(language) // 中文
        {
            ui->pushButton_reset->setText(tr("计数"));
            ui->pushButton_start->setText(tr("停止计时"));
        }
        else // 英文
        {
            ui->pushButton_reset->setText(tr("Count"));
            ui->pushButton_start->setText(tr("StopTimer"));
        }

        if(timer != nullptr)
        {
            // 启动定时器 , 10ms 执行一次信号处理函数
            timer->start(10) ; // 定时器开始工作
        }
    }
    else // false
    {
        // true 1: 中文   false 0:是英文 , 默认是中文
        if(language) // 中文
        {
            ui->pushButton_reset->setText(tr("复位计数"));
            ui->pushButton_start->setText(tr("启动计时"));
        }
        else // 英文
        {
            ui->pushButton_reset->setText(tr("Reset"));
            ui->pushButton_start->setText(tr("StartTimer"));
        }

        if(timer != nullptr)
        {
            timer->stop() ; // 定时器停止工作
        }
    }
}


void MainWindow::on_pushButton_reset_clicked()
{
    qDebug()<<"on_pushButton_reset_clicked():";


    // pushButton_start是否被按下, 如果按下返回为真 , 否则返回为假
    if(ui->pushButton_start->isChecked()) //
    {
        qDebug()<<"pushButton_start被按下";
        // 计数
        qDebug()<< QString::asprintf("%02d:%02d.%02d",min,sec,ms);   // 拼接字符串
        if(model != nullptr)
        {
            /*设置一条数据*/
            model->setItem(lineCount, 0, new QStandardItem(QString::asprintf("%03d",lineCount+1)));
            model->setItem(lineCount, 1, new QStandardItem(QString::asprintf("%02d:%02d.%02d",min,sec,ms)));
            model->item(lineCount, 0)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 1)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            ui->tableView->setModel(model) ; // 给tableview 安装一个模型 model
            lineCount ++;
        }

    }
    else
    {
        qDebug()<<"pushButton_start没有被按下";
        // 清空计数
        min = 0 ;
        sec =0 ;
        ms =0 ;
        ui->lcdNumber->display( QString::asprintf("%02d:%02d.%02d",min,sec,ms)); // 拼接字符串

        model->removeRows(0,lineCount);  // 从0行 一直删除到 指定的行
        lineCount = 0 ; // 行号清空
    }
}


void MainWindow::getQApplication(QApplication *p)
{
    appPointer = p ;
}

// 这个菜单中做中英文切换
void MainWindow::on_action_language_triggered()
{
    qDebug()<<"on_action_language_triggered()";

    // true 1: 中文   false 0:是英文 , 默认是中文
    if(language) // 切换到英文
    {
        language = !language ;
        qDebug()<<"切换到英文";

        QTranslator translator; // 语言翻译类
        translator.load("en_US.qm"); // 去当前目录下去查

        // 以下两种办法都可以, 静态方法更加适用
#if   1
        QApplication::installTranslator(&translator);
#else
        if(appPointer != nullptr)
        {
            appPointer->installTranslator(&translator); // 安装英文
        }
#endif


        ui->retranslateUi(this); // 重新初始化界面
        model->clear();  // 把model中的所有内容清空
        model->setColumnCount(2); // 设置列数 ,一共是2列
        model->setHeaderData(0,Qt::Horizontal,tr("NO"));    // 第1列的列名
        model->setHeaderData(1,Qt::Horizontal,tr("Value")); // 第2列的列名
        ui->tableView->setModel(model);
    }
    else //false  切换到中文
    {
        language = !language ;
        qDebug()<<"切换到中文";
        // 以下两种办法都可以, 静态方法更加适用
#if   1
        QApplication::installTranslator(nullptr);
#else
        if(appPointer != nullptr)
        {
            appPointer->installTranslator(NULL); // 不安装语言包, 使用默认语言中文
        }
#endif

        ui->retranslateUi(this);
        model->clear();  // 把model中的所有内容清空
        model->setColumnCount(2);   // 设置 一行有2列
        model->setHeaderData(0,Qt::Horizontal, tr("序号"));  // 第一列名 序号
        model->setHeaderData(1,Qt::Horizontal, tr("数值"));  // 第二列名 数值
        ui->tableView->setModel(model);

    }

}



