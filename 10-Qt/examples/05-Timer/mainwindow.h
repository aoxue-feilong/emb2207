﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#pragma execution_character_set("utf-8")

#include <QMainWindow>
#include <qdebug.h>
#include <QTimer>
#include <QStandardItemModel>
#include <QTranslator>



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void getQApplication(QApplication *p);

private slots:
    void on_pushButton_start_clicked(bool checked);

    // 自定义一个信号处理函数
    void timerout_slot();


    void on_pushButton_reset_clicked();

    void on_action_language_triggered();


private:
    Ui::MainWindow *ui;
    QTimer *timer ; // 定义一个定时器指针
    qint32 sec=0,min=0,ms=0 ; // 对秒和分钟进行计时
    QStandardItemModel  * model ;     // QStandardItemModel 这是一个标准的model/view的框架
    qint32 lineCount = 0 ; // 要插入记录的行号
    bool language  = true ; // true 1: 中文   false 0:是英文 , 默认是中文
    QApplication *appPointer; // 在类当中获取应用程序的地址 QApplication a(argc, argv);
};
#endif // MAINWINDOW_H
