<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="29"/>
        <source>秒表计时器 1.2</source>
        <translation>MeterSecondTimer 1.2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="65"/>
        <location filename="mainwindow.cpp" line="99"/>
        <source>启动计时</source>
        <translation>Start Timer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <location filename="mainwindow.cpp" line="98"/>
        <source>复位计数</source>
        <translation>Reset Count</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="109"/>
        <source>语言</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>切换到英文</source>
        <oldsource>切换到中文</oldsource>
        <translation>switchChinese</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="33"/>
        <source>序号</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>数值</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="88"/>
        <source>计数</source>
        <translation>Count</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="89"/>
        <source>停止计时</source>
        <translation>StopTimer</translation>
    </message>
</context>
</TS>
