#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDebug>
#include <QFileDialog>
#include <QPixmap>
QT_BEGIN_NAMESPACE
namespace Ui { class widget; }
QT_END_NAMESPACE

class widget : public QWidget
{
    Q_OBJECT

public:
    widget(QWidget *parent = nullptr);
    ~widget();

private slots:
    void on_radioButton_file_clicked();

    void on_radioButton_pre_clicked();

    void on_radioButton_save_clicked();

    void on_radioButton_del_clicked();

    void on_radioButton_exit_clicked();

private:
    Ui::widget *ui;
    QString fileName;
    qint32 width,height;
};
#endif // WIDGET_H
