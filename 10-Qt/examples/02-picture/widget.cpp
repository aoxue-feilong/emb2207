#include "widget.h"
#include "./ui_widget.h"

widget::widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::widget)
{
    ui->setupUi(this);
    //设置默认选项
    ui->radioButton_32->setChecked(true);
}

widget::~widget()
{
    delete ui;
}

//这个就是信号处理函数
//单击按钮on_pushButton_file 要执行下面的函数
void widget::on_radioButton_file_clicked()
{
    qDebug()<<"on_pushButton_file_clicked()" ;//等价与c++的cout
    //需要一个打开文件的对话框
         fileName = QFileDialog::getOpenFileName(this,
                  tr("打开图片"),"./",tr("Image Files(*.png*.jpg*.pmp);;All files(*.*)"));
         qDebug()<<"fileName="<<fileName;
         //向标签内写入内容
         ui->label_filename->setText(fileName);
}

//预览函数
void widget::on_radioButton_pre_clicked()
{
       if(ui->radioButton_16->isChecked())//这个按钮是否被选中
        {
            this->width=16;
            this->height=16;
        }else if(ui->radioButton_32->isChecked())//这个按钮是否被选中
        {
            this->width=32;
            this->height=32;
        }else if(ui->radioButton_64->isChecked())//这个按钮是否被选中
        {
            this->width=64;
            this->height=64;
        }else if(ui->radioButton_128->isChecked())//这个按钮是否被选中
        {
            this->width=128;
            this->height=128;
        }else if(ui->radioButton_256->isChecked())//这个按钮是否被选中
        {
            this->width=256;
            this->height=256;
        }
       if(fileName.isEmpty())
       {
           qDebug()<<"文件名为空，获取文件失败";
           return;
       }
       //显示图片 QPixmap
        QPixmap map(fileName);//创建一个图片对象
        QPixmap newmap = map.scaled(width,height);//对图片进行缩放
        ui->label_preview->setPixmap(newmap);//把现实的图片放到label上面
}

//保存函数
void widget::on_radioButton_save_clicked()
{
        if(ui->radioButton_16->isChecked())//这个按钮是否被选中
        {
            this->width=16;
            this->height=16;
        }else if(ui->radioButton_32->isChecked())//这个按钮是否被选中
        {
            this->width=32;
            this->height=32;
        }else if(ui->radioButton_64->isChecked())//这个按钮是否被选中
        {
            this->width=64;
            this->height=64;
        }else if(ui->radioButton_128->isChecked())//这个按钮是否被选中
        {
            this->width=128;
            this->height=128;
        }else if(ui->radioButton_256->isChecked())//这个按钮是否被选中
        {
            this->width=256;
            this->height=256;
        }
        qDebug()<<"width ="<<width;
        qDebug()<<"height ="<<height;
        //显示图片 QPixmap
        QPixmap map(fileName);//创建一个图片对象
        QPixmap newmap = map.scaled(width,height);//对图片进行缩放
        ui->label_preview->setPixmap(newmap);//把这个图片显示到label_preview

       fileName.clear();//把字符串清空
       // QFileDialog::getSaveFileName（）获取保存的文件
       fileName = QFileDialog::getSaveFileName(this,tr("保存文件"),
                                      ".",tr("Images (*.ico )"));
       if(fileName.isEmpty())
       {
           qDebug()<<"文件名为空.  获取文件失败";
           return;
       }
       newmap.save(fileName);//把照片保存到新文件中
}

//删除函数
void widget::on_radioButton_del_clicked()
{
    ui->label_filename->clear();//清除显示
    ui->label_preview->clear();//清除图片的显示
}
//退出程序
void widget::on_radioButton_exit_clicked()
{
    this->close();
}

