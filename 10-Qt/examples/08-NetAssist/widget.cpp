﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    // 获取本地的ip地址
    // hostname 就是计算机名称
    qDebug()<<"hostname:"<< QHostInfo::localHostName();
    // 获取主机名的目的是为了获取主机的ip地址, 可以使用方法fromName
    // fromName 函数实现了, 根据主机名获取主机的ip地址
    // fromName 函数返回值为  QHostInfo,  , 一个主机可以有多个ip地址
    // 再使用方法addresses() 获取这个主机名的所有地址信息
    QHostInfo info =  QHostInfo::fromName(QHostInfo::localHostName()) ; // 返回一个QHostInfo对象
    foreach (QHostAddress addr, info.addresses())
    {
        //addr.protocol()里面有iPV6信号
        qDebug()<<"addr.protocol()="<<addr.protocol();
        //判断是否IPV4信号（过滤一下）
        if(addr.protocol() == QAbstractSocket::IPv4Protocol) // 只获取ipv4地址类型
        {
            //输出主机的ip地址
            qDebug()<<"addr"<< addr;
            //将地址输出到comboBox_ip上面
            ui->comboBox_ip->addItem(addr.toString());

        }

    }
    // 设置端口
    ui->comboBox_port->setEditText("9000");
    //    QIcon icon1;
    //    icon1.addFile(QString::fromUtf8(":/images/ledoff.png"), QSize(), QIcon::Normal, QIcon::Off);
    //    pushButton_open->setIcon(icon1);

    // 对server 指针初始化为null
    server = nullptr ; // 设置指针为空
    client = nullptr ; // 设置指针为空
    tcpClient = nullptr ; //设置指针为空
    udpSocket = nullptr ; //设置指针为空


    // 隐藏客户端的ip和端口
    ui->groupBox_client->hide();


    // 如果系统已经保存了 软件的配置信息, 则需要自动加载配置信息
    // 如果没有保存, 则使用默认的信息
    // 创建一个config.ini文件 , 文件不存在则创建, 文件存在则打开
    QSettings set("NetAssist.ini",QSettings::IniFormat);
    set.beginGroup("Config");// 开始的组
    //如果Config有type 这个key , 说明我们配置文件中已经保存了操作配置 , 此时就需要我们加载即可
    if(set.contains("type")) //  可以在NetAssist.ini 进行搜索 , 找到和没找到两种
    {

        // UDP  , TCP Client , TCP Server
        QString type = set.value(tr("type")).toString();
        QString ip = set.value(tr("ip")).toString();
        QString port = set.value(tr("port")).toString();

        qDebug()<<"type="<<type;
        qDebug()<<"ip  ="<<ip;
        qDebug()<<"port="<<port;
        if(type == "UDP")
        {
            ui->comboBox_type->setCurrentIndex(0); // UDP
        }
        else if(type == "TCP Client")
        {
            ui->comboBox_type->setCurrentIndex(1); // TCP Client
        }
        else if(type == "TCP Server")
        {
            ui->comboBox_type->setCurrentIndex(2); // TCP Server
        }

        ui->comboBox_ip->setCurrentText(ip);
        ui->comboBox_port->setCurrentText(port);



    }
    else // 如果没有就创建 这些值
    {

        set.setValue(tr("type"),ui->comboBox_type->currentText());
        set.setValue(tr("ip"),ui->comboBox_ip->currentText());
        set.setValue(tr("port"),ui->comboBox_port->currentText());


    }

    set.endGroup();


    //设置ascii显示还是Hex显示
    ui->radioButton_recv_ascii->setChecked(true); // 被选中
    ui->radioButton_send_ascii->setChecked(true); // 被选中



}

Widget::~Widget()
{
    delete ui;
}

//设置newConnection_slot() 信号处理函数，客户端连接到服务器后的信号处理函数
void Widget::newConnection_slot()
{
    qDebug()<<"new client is connected";
    ui->textEdit_recv->insertPlainText("new client is connected");
    if(server != nullptr)
    {
        client =  server->nextPendingConnection() ; // 等于linux c 的accept 函数功能
        // 安装client 的信号处理函数
        // 接收到数据时, 发送 readyRead() 信号
        connect(client,&QTcpSocket::readyRead,this,&Widget::readyRead_slot);

        //设置客户端的断开连接后的信号处理函数disconnected()
        connect(client,&QTcpSocket::disconnected,this,&Widget::disconnected_slot);

        // 把客户端插入到vector 内
        clients.append(client);

        QHostAddress addr =  client->peerAddress() ; // tcp 连接的对方的地址, 就是客户端的地址
        quint16 port =  client->peerPort() ; // 获取对方的端口号
        ui->comboBox_client_ip->addItem(tr("%0:%1").arg(addr.toString()).arg(port));



    }

}

void Widget::readyRead_slot()
{
    qDebug()<<"服务器收到客户端发送的数据";
    for(int i=0;i<clients.count();i++)
    {

        // 判断socket 的数据是否可读
        if(clients.at(i)->bytesAvailable() > 0) // 表示数据可读
        {

            QByteArray data =  clients.at(i)->readAll() ; // 读出所有的数据
            qDebug()<<"data="<<QString(data);
            if(ui->radioButton_recv_ascii->isChecked()) // 是否被选中 , ascii显示
            {
                ui->textEdit_recv->insertPlainText(QString(data));
            }
            else if(ui->radioButton_recv_hex->isChecked())
            {
                QString hexstring  ;
                convertAsciiToHex(QString(data),  hexstring);
                ui->textEdit_recv->insertPlainText(hexstring);
            }

        }

    }


}

void Widget::disconnected_slot()
{
    qDebug()<<"客户端已经断开连接";
    // 客户端断开连接后, 需要删除对象的ip和端口
    for(int i=0;i<clients.count();i++)
    {
        if(clients.at(i)->state() == QAbstractSocket::UnconnectedState) // 断开连接后的状态
        {
            QString hostinfo = ui->comboBox_client_ip->currentText() ; // 这里保存的是ip和端口
            QString info = tr("%0:%1").arg(clients.at(i)->peerAddress().toString())
                    .arg(clients.at(i)->peerPort() ) ;

            if(hostinfo == info)
            {

                clients.remove(i);
                ui->comboBox_client_ip->removeItem(i);

            }
        }


    }
}

void Widget::connected_slot()
{
    qDebug()<<"客户端已经连接服务器";


    // 切换pushButton_open的图片为开状态图片 ledon.png
    QIcon icon1(QString::fromUtf8(":/images/ledon.png"));
    ui->pushButton_open->setIcon(icon1);
    ui->pushButton_open->setText(tr("断开"));
    ui->comboBox_type->setEnabled(false) ;// 置灰 不可操作
    ui->comboBox_ip->setEnabled(false) ; // 置灰 不可操作
    ui->comboBox_port->setEnabled(false); // 置灰 不可操作
}
void Widget::errorOccurred_slot(QAbstractSocket::SocketError socketError)
{
    qDebug()<<"socketError:"<<socketError;
    if( socketError == QAbstractSocket::ConnectionRefusedError) // 连接拒绝
    {
        qDebug()<<"服务器没有找到";
        ui->comboBox_type->setEnabled(true) ;// 恢复操作
        ui->comboBox_ip->setEnabled(true) ; // 恢复操作
        ui->comboBox_port->setEnabled(true); // 恢复操作
        QIcon icon1(QString::fromUtf8(":/images/ledoff.png"));
        ui->pushButton_open->setIcon(icon1);
        if(tcpClient != nullptr)
        {
            tcpClient->close();// 关闭socket
        }
        // 设置按钮的状态为
        ui->pushButton_open->setChecked(true);

    }

}

void Widget::tcpclient_readyRead_slot()
{
    qDebug()<<"客户端收到服务器的数据";
    if(tcpClient != nullptr)
    {
        QByteArray data =  tcpClient->readAll() ; // 读出所有的数据
        qDebug()<<"data="<<QString(data);
        if(ui->radioButton_recv_ascii->isChecked()) // 是否被选中 , ascii显示
        {
            ui->textEdit_recv->insertPlainText(QString(data));
        }
        else if(ui->radioButton_recv_hex->isChecked())
        {
            QString hexstring  ;
            convertAsciiToHex(QString(data),  hexstring);
            ui->textEdit_recv->insertPlainText(hexstring);
        }
    }

}

void Widget::udpsocket_readyRead_slot()
{
    qDebug()<<"udp收到数据";
    if(udpSocket != nullptr)
    {
        udpSocket->readDatagram(recvBuF,128); // 读出所有的数据
        qDebug()<<"data="<<QString(recvBuF);

        if(ui->radioButton_recv_ascii->isChecked()) // 是否被选中 , ascii显示
        {
            ui->textEdit_recv->insertPlainText(QString(recvBuF));
        }
        else if(ui->radioButton_recv_hex->isChecked())
        {
            QString hexstring  ;
            convertAsciiToHex(QString(recvBuF),  hexstring);
            ui->textEdit_recv->insertPlainText(hexstring);
        }


    }


}

void Widget::on_pushButton_open_clicked(bool checked)
{
    qDebug()<<"checked="<<checked;


    //currentIndex() == 0  : UDP
    //currentIndex() == 1  : TCP Client
    //currentIndex() == 2  : TCP Server
    qDebug()<<"currentIndex() == "<< ui->comboBox_type->currentIndex() ;

    if(ui->comboBox_type->currentIndex() == 0) // UDP
    {
        if(checked) // 为真时
        {

            // 打开UDP Socket
            udpSocket = new QUdpSocket(this);
            // 设置信号处理函数

            // 接收到数据时, 发送 readyRead() 信号
            connect(udpSocket,&QUdpSocket::readyRead,this,&Widget::udpsocket_readyRead_slot);


            // 为真 , 表示成功启动socket
            // 把 客户端的信息窗口显示
            ui->comboBox_client_ip->clear() ; // 清除已经存在的成员
            ui->groupBox_client->show();// 显示控件
            ui->comboBox_client_ip->setEditable(true) ; // 设置可编辑
            QString info = tr("%0:%1").arg(ui->comboBox_ip->currentText()).arg(ui->comboBox_port->currentText() ) ;
            ui->comboBox_client_ip->setEditText(info);
            ui->label_client->setText(tr("远程主机"));

            QHostAddress addr(ui->comboBox_ip->currentText());
            quint16 port  = ui->comboBox_port->currentText().toUShort();
            // udp socket 需要绑定ip 和端口
            udpSocket->bind(addr,port);


            ui->comboBox_type->setEnabled(false) ;// 置灰 不可操作
            ui->comboBox_ip->setEnabled(false) ; // 置灰 不可操作
            ui->comboBox_port->setEnabled(false); // 置灰 不可操作

            // 切换pushButton_open的图片为开状态图片 ledon.png
            QIcon icon1(QString::fromUtf8(":/images/ledon.png"));
            ui->pushButton_open->setIcon(icon1);
            ui->pushButton_open->setText(tr("关闭"));


        }
        else // 为假
        {

            // 切换pushButton_open的图片为关状态图片 ledoff.png
            QIcon icon1(QString::fromUtf8(":/images/ledoff.png"));
            ui->pushButton_open->setIcon(icon1);
            ui->pushButton_open->setText(tr("打开"));

            ui->comboBox_type->setEnabled(true) ;// 恢复操作
            ui->comboBox_ip->setEnabled(true) ; // 恢复操作
            ui->comboBox_port->setEnabled(true); // 恢复操作

            // 隐藏窗体
            ui->groupBox_client->hide();
            if(udpSocket != nullptr)
            {
                udpSocket->close(); //关闭socket
                delete udpSocket ;
                udpSocket = nullptr ;
            }

        }

        // 保存操作的配置信息
        QSettings set("NetAssist.ini",QSettings::IniFormat);
        set.beginGroup("Config");// 开始的组
        set.setValue(tr("type"),ui->comboBox_type   ->currentText());
        set.setValue(tr("ip"),ui->comboBox_ip->currentText());
        set.setValue(tr("port"),ui->comboBox_port->currentText());
        set.endGroup();

    }
    else if(ui->comboBox_type->currentIndex() == 1) // TCP Client
    {
        if(checked) // 为真时
        {

            // 启动TCP 连接到服务器
            tcpClient = new QTcpSocket(this);
            // 设置信号处理函数
            // 客户端连接成功时
            connect(tcpClient,&QTcpSocket::connected,this,&Widget::connected_slot);

            // 连接服务器失败, 或则错误时, 也有信号发出
            connect(tcpClient,&QTcpSocket::errorOccurred,this,&Widget::errorOccurred_slot);

            // 接收到数据时, 发送 readyRead() 信号
            connect(tcpClient,&QTcpSocket::readyRead,this,&Widget::tcpclient_readyRead_slot);

            QHostAddress addr(ui->comboBox_ip->currentText());
            quint16 port  = ui->comboBox_port->currentText().toUShort();
            tcpClient->connectToHost(addr,port); // 启动tcp 连接 , 这个函数等于linux c 的connect函数

        }
        else // 为假
        {

            // 切换pushButton_open的图片为关状态图片 ledoff.png
            QIcon icon1(QString::fromUtf8(":/images/ledoff.png"));
            ui->pushButton_open->setIcon(icon1);
            ui->pushButton_open->setText(tr("连接"));

            ui->comboBox_type->setEnabled(true) ;// 恢复操作
            ui->comboBox_ip->setEnabled(true) ; // 恢复操作
            ui->comboBox_port->setEnabled(true); // 恢复操作

            // 隐藏窗体
            ui->groupBox_client->hide();

            ui->pushButton_open->setChecked(false);// 把按钮状态设置位弹出  (按下与弹出)

            if(tcpClient != nullptr)
            {
                tcpClient->close(); //关闭socket
                delete tcpClient ;
                tcpClient = nullptr ;
            }

        }

        // 保存操作的配置信息
        QSettings set("NetAssist.ini",QSettings::IniFormat);
        set.beginGroup("Config");// 开始的组
        set.setValue(tr("type"),ui->comboBox_type->currentText());
        set.setValue(tr("ip"),ui->comboBox_ip->currentText());
        set.setValue(tr("port"),ui->comboBox_port->currentText());
        set.endGroup();

    }
    else if(ui->comboBox_type->currentIndex() == 2) // TCP Server
    {



        if(checked) // 为真时
        {

            // 启动TCP server 服务器
            server = new QTcpServer(this);
            // 设置信号处理函数
            // 客户端连接成功时
            connect(server,&QTcpServer::newConnection,this,&Widget::newConnection_slot);
            QHostAddress addr(ui->comboBox_ip->currentText());
            quint16 port  = ui->comboBox_port->currentText().toUShort();
            if( server->listen(addr,port) )  // 启动监听, 这个函数等于linux c 的bind 和listen两个函数
            {
                // 为真 , 表示成功启动socket
                // 把 客户端的信息窗口显示
                ui->comboBox_client_ip->clear() ; // 清除已经存在的成员
                ui->groupBox_client->show();// 显示控件

                // 切换pushButton_open的图片为开状态图片 ledon.png
                QIcon icon1(QString::fromUtf8(":/images/ledon.png"));
                ui->pushButton_open->setIcon(icon1);
                ui->pushButton_open->setText(tr("关闭"));
            }
            else // 启动失败
            {
                server->close();
                ui->pushButton_open->setChecked(false);// 把按钮状态设置位弹出  (按下与弹出)
            }

        }
        else // 为假
        {
            // 把 客户端的信息窗口隐藏
            ui->groupBox_client->hide();// 隐藏控件

            // 切换pushButton_open的图片为关状态图片 ledoff.png
            QIcon icon1(QString::fromUtf8(":/images/ledoff.png"));
            ui->pushButton_open->setIcon(icon1);
            ui->pushButton_open->setText(tr("打开"));
            if(server != nullptr)
            {
                server->close(); //关闭服务器
                delete server ;
                server = nullptr ;
            }

            // 隐藏窗体
            ui->groupBox_client->hide();

        }

        // 保存操作的配置信息
        QSettings set("NetAssist.ini",QSettings::IniFormat);
        set.beginGroup("Config");// 开始的组
        set.setValue(tr("type"),ui->comboBox_type->currentText());
        set.setValue(tr("ip"),ui->comboBox_ip->currentText());
        set.setValue(tr("port"),ui->comboBox_port->currentText());
        set.endGroup();


    }


}


void Widget::on_pushButton_send_clicked()
{

    if(ui->comboBox_type->currentIndex() == 0) // UDP
    {
        if(udpSocket != nullptr)
        {
            QString hostinfo = ui->comboBox_client_ip->currentText() ; // 这里保存的是ip和端口
            QStringList list =  hostinfo.split(":");
            foreach (QString l, list) {
                qDebug()<<"l="<<l;
            }
            QHostAddress addr(list[0]);
            quint16 port  = list[1].toUShort();

            if(ui->radioButton_send_ascii->isChecked()) // 发送ascii的字符串
            {
                udpSocket->writeDatagram(ui->textEdit_send->toPlainText().toUtf8(),addr,port);
            }
            else if(ui->radioButton_send_hex->isChecked()) // 发送hex的字符串
            {
                QByteArray data ;
                convertStringToHex(ui->textEdit_send->toPlainText(), data);
                udpSocket->writeDatagram(data,addr,port);
            }


        }

    }
    else if(ui->comboBox_type->currentIndex() == 1) // TCP Client
    {
        if(tcpClient != nullptr)
        {


            if(ui->radioButton_send_ascii->isChecked()) // 发送ascii的字符串
            {
                tcpClient->write(ui->textEdit_send->toPlainText().toUtf8());
            }
            else if(ui->radioButton_send_hex->isChecked()) // 发送hex的字符串
            {
                QByteArray data ;
                convertStringToHex(ui->textEdit_send->toPlainText(), data);
                tcpClient->write(data);
            }

        }


    }
    else if(ui->comboBox_type->currentIndex() == 2) // TCP Server
    {
        for(int i=0;i<clients.count();i++)
        {
            QString hostinfo = ui->comboBox_client_ip->currentText() ; // 这里保存的是ip和端口
            QString info = tr("%0:%1").arg(clients.at(i)->peerAddress().toString())
                    .arg(clients.at(i)->peerPort() ) ;

            if(hostinfo == info)
            {



                if(ui->radioButton_send_ascii->isChecked()) // 发送ascii的字符串
                {
                    clients.at(i)->write(ui->textEdit_send->toPlainText().toUtf8());
                }
                else if(ui->radioButton_send_hex->isChecked()) // 发送hex的字符串
                {
                    QByteArray data ;
                    convertStringToHex(ui->textEdit_send->toPlainText(), data);
                    clients.at(i)->write(data);
                }


            }

        }

    }

}



void Widget::on_comboBox_type_currentIndexChanged(int index)
{
    if(ui->comboBox_type->currentIndex() == 0) // udp
    {
        ui->pushButton_open->setText(tr("打开"));


    }
    else if(ui->comboBox_type->currentIndex() == 1) // tcp client
    {
        // 选中tcp client 时, 按钮要提示成连接
        ui->pushButton_open->setText(tr("连接"));
        ui->comboBox_ip->setEditable(true); // 让ip 变得可以编辑

    }
    else if(ui->comboBox_type->currentIndex() == 2) // tcp server
    {
        // 选中tcp server 时, 按钮要提示成打开
        ui->pushButton_open->setText(tr("打开"));
        ui->comboBox_ip->setEditable(false); // 让ip 变得不可以编辑
    }


    // 保存操作的配置信息
    QSettings set("NetAssist.ini",QSettings::IniFormat);
    set.beginGroup("Config");// 开始的组
    set.setValue(tr("type"),ui->comboBox_type->currentText());
    set.endGroup();//结束组
}

//接收清空函数
void Widget::on_pushButton_recv_clear_clicked()
{
    ui->textEdit_recv->clear();
}

//发送清空函数
void Widget::on_pushButton_send_clear_clicked()
{
    ui->textEdit_send->clear();
}


// 把字符串转成16进制字符串
// str1 是ascii的字符串
// str2 是hex 的字符串
void Widget::convertAsciiToHex(const QString &str1,  QString &str2)
{
    str2 = "";
    for(int i=0;i<str1.size();i++)
    {
        str2  +=  QString::asprintf("%02X",str1.at(i).toLatin1());
        if(i < (str1.size()-1) )  // 最后一个字符不加空格
        {
            str2  += " ";
        }

    }
}
// 把16进制的字符串 转成 ascii的字符串
// str1 是hex 的字符串
// str2 是ascii 的字符串
void Widget::convertHexToAscii(const QString &str1,  QString &str2)
{
    str2 = "" ;
    QStringList list =   str1.split(" ") ; // 使用空格进行拆分
    qDebug()<<"list="<<list ;
    for(int i=0;i<list.size();i++)
    {
        qDebug()<<"char="<< QString::asprintf("%c",list.at(i).toInt() ) ;
        qDebug()<<"list.at(i)="<<list.at(i) ;
        bool ok ;
        qDebug()<<"list.at(i).toint="<<list.at(i).toInt(&ok,16) ;
        str2 += QString::asprintf("%c",list.at(i).toInt(&ok,16) ) ;
    }
    qDebug()<<"str2="<<str2 ;

}


char convertCharToHex(char ch)
{
    if((ch >= '0') && (ch <= '9'))
        return ch-0x30;
    else if((ch >= 'A') && (ch <= 'F'))
        return ch-'A'+10;
    else if((ch >= 'a') && (ch <= 'f'))
        return ch-'a'+10;
    else return (-1);
}

// 把16进制的字符串转成16进制的QByteArray
void Widget::convertStringToHex(const QString &str, QByteArray &byteData)
{
    int hexdata,lowhexdata;
    int hexdatalen = 0;
    int len = str.length();
    byteData.resize(len/2);
    char lstr,hstr;
    for(int i=0; i<len; )
    {
        //char lstr,
        hstr=str[i].toLatin1();
        if(hstr == ' ')
        {
            i++;
            continue;
        }
        i++;
        if(i >= len)
            break;
        lstr = str[i].toLatin1();
        hexdata = convertCharToHex(hstr);
        lowhexdata = convertCharToHex(lstr);
        if((hexdata == 16) || (lowhexdata == 16))
            break;
        else
            hexdata = hexdata*16+lowhexdata;
        i++;
        byteData[hexdatalen] = (char)hexdata;
        hexdatalen++;
    }
    byteData.resize(hexdatalen);
}


void Widget::on_radioButton_recv_ascii_clicked()
{
    qDebug()<<"on_radioButton_recv_ascii_clicked()";
}


void Widget::on_radioButton_send_ascii_clicked()
{
    //sendHexAsciiFlag = false :表示ascii 字符串
    //sendHexAsciiFlag = true :表示hex 字符串
    if(sendHexAsciiFlag)  // 如果发送文本是hex 进制字符串需要转换成ascii格式
    {
        // 把发送区的内容转成ascii 字符串类型
        QString str2;
        convertHexToAscii(ui->textEdit_send->toPlainText(),str2);
        ui->textEdit_send->setText(str2);
        sendHexAsciiFlag= false;//  转换后, 设置标志位 , 表示字符串已经是ascii 字符串了
    }

}


void Widget::on_radioButton_send_hex_clicked()
{
    //sendHexAsciiFlag = false :表示ascii 字符串
    //sendHexAsciiFlag = true :表示hex 字符串
    qDebug()<<"on_radioButton_send_hex_clicked()";
    if(!sendHexAsciiFlag)  // 如果发送文本是hex 进制字符串需要转换成ascii格式
    {
        // 把发送区的内容转成ascii 字符串类型
        QString str2;
        convertAsciiToHex(ui->textEdit_send->toPlainText(),str2);
        ui->textEdit_send->setText(str2);
        sendHexAsciiFlag= true;//  转换后, 设置标志位 , 表示字符串已经是hex 字符串了
    }

}

