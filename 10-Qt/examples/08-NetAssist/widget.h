﻿#ifndef WIDGET_H
#define WIDGET_H
#pragma execution_character_set("utf-8")
#include <QWidget>
#include <QDebug>
#include <QHostInfo>
#include <QTcpServer>
#include <QTcpSocket>
#include <QSettings>
#include <QString>
#include <QUdpSocket>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    //把字符串转成16进制的字符串
    void convertAsciiToHex(const QString &str1,QString &str2);

    //把16进制的字符串 转成 ASCII的字符串
    void convertHexToAscii(const QString &str1,QString &str2);
    void convertStringToHex(const QString &str, QByteArray &byteData);

private slots:
    //单击打开按钮时的函数
    void on_pushButton_open_clicked(bool checked);

    //设置newConnection_slot() 信号处理函数，客户端连接到服务器后的信号处理函数
    void newConnection_slot();

    //设置dreadyRead_slot() 信号处理函数，收到数据发送这个信号
    void readyRead_slot();

    //设置disconnected 信号处理函数，socket断开连接后发送这个信号
    void disconnected_slot();

    //单击发送按钮时的处理函数
    void on_pushButton_send_clicked();

    void on_comboBox_type_currentIndexChanged(int index);

    //客户端连接服务器后，发送连接成功信号
    void connected_slot();

    //错误发生时的信号处理
    void errorOccurred_slot(QAbstractSocket::SocketError socketError);

    //设置Tcp Client，收到数据后发送这个信号
    void tcpclient_readyRead_slot();

    //设置udpsocket_readyRead_slot()  udp 类型的socket信号处理函数
    void udpsocket_readyRead_slot();


    void on_pushButton_recv_clear_clicked();

    void on_pushButton_send_clear_clicked();

    void on_radioButton_recv_ascii_clicked();

    void on_radioButton_send_ascii_clicked();

    void on_radioButton_send_hex_clicked();



private:
    Ui::Widget *ui;
    QTcpServer *server;
    QTcpSocket *client;
    QTcpSocket *tcpClient;//Tcp Client 类型的socket
    QUdpSocket *udpSocket;  //udp 类型的socket
    //创建一个vector 保存客户端的地址信息
    QVector<QTcpSocket *> clients;
    char recvBuF[1024];
    bool sendHexAsciiFlag=false;  //表示文本默认 ascii格式
};
#endif // WIDGET_H
