﻿#ifndef APP_H
#define APP_H

#include "head.h"

class App
{
public:
    static QString ConfigFile;  //配置文件文件路径及名称
    static QString Version;     //版本号
    static QString TitleFlag;   //标题标识

    //全局配置
    static int RowHeight;       //行高
    static int RightWidth;      //右侧宽度
    static int FormWidth;       //窗体宽度
    static int FormHeight;      //窗体高度

    //搜索配置
    static QString UserName;    //用户名称
    static QString UserPwd;     //用户密码
    static QString LocalIP;     //指定网卡
    static QString DeviceIP;    //指定设备
    static QString SearchFilter;//搜索条件过滤
    static QString SearchFilters;//搜索条件集合
    static int SearchInterval;  //搜索间隔 单位毫秒
    static bool SearchClear;    //搜索清空

    //基础配置
    static int TabIndex;        //页面索引
    static int TabIndex1;       //页面索引1
    static int TabIndex2;       //页面索引2
    static bool IsMax;          //最大化显示
    static bool IsFull;         //全屏显示
    static QString VideoType;   //画面分割类型
    static int BufferWidth;     //VLC回调模式分辨率宽度
    static int BufferHeight;    //VLC回调模式分辨率高度

    //视频配置
    static QString RtspAddr;    //视频流地址
    static QString Hardware;    //解码名称
    static QString Transport;   //传输协议
    static bool FillImage;      //拉伸填充
    static bool Callback;       //回调
    static int Caching;         //缓存时间
    static int ImageFlag;       //图片质量
    static int CheckTime;       //打开超时时间

    //视频配置1
    static QString RtspAddr1;   //视频流地址
    static QString FileName1;   //存储文件名
    static bool SaveFile1;      //保存文件
    static bool SaveInterval1;  //定时保存
    static QString Hardware1;   //解码名称
    static QString Transport1;  //传输协议
    static bool FillImage1;     //拉伸填充
    static bool Callback1;      //回调
    static int Caching1;        //缓存时间
    static int ImageFlag1;      //图片质量
    static int CheckTime1;      //打开超时时间

    //视频配置2
    static QString RtspAddr2;   //视频流地址
    static QString FileName2;   //存储文件名
    static bool SaveFile2;      //保存文件
    static bool SaveInterval2;  //定时保存
    static QString Hardware2;   //解码名称
    static QString Transport2;  //传输协议
    static bool FillImage2;     //拉伸填充
    static bool Callback2;      //回调
    static int Caching2;        //缓存时间
    static int ImageFlag2;      //图片质量
    static int CheckTime2;      //打开超时时间

    //视频配置3
    static QString RtspAddr3;   //视频流地址
    static QString FileName3;   //存储文件名
    static bool SaveFile3;      //保存文件
    static bool SaveInterval3;  //定时保存
    static QString Hardware3;   //解码名称
    static QString Transport3;  //传输协议
    static bool FillImage3;     //拉伸填充
    static bool Callback3;      //回调
    static int Caching3;        //缓存时间
    static int ImageFlag3;      //图片质量
    static int CheckTime3;      //打开超时时间

    //视频配置4
    static QString RtspAddr4;   //视频流地址
    static QString FileName4;   //存储文件名
    static bool SaveFile4;      //保存文件
    static bool SaveInterval4;  //定时保存
    static QString Hardware4;   //解码名称
    static QString Transport4;  //传输协议
    static bool FillImage4;     //拉伸填充
    static bool Callback4;      //回调
    static int Caching4;        //缓存时间
    static int ImageFlag4;      //图片质量
    static int CheckTime4;      //打开超时时间

    //播放器配置
    static QString RtspAddr5;   //视频流地址
    static QString Hardware5;   //解码名称
    static bool FillImage5;     //拉伸填充
    static bool Callback5;      //回调

    //多端复用配置
    static QString RtspAddr6;   //视频流地址
    static QString Hardware6;   //解码名称
    static bool FillImage6;     //拉伸填充
    static bool Callback6;      //回调

    static void readConfig();   //读取配置文件,在main函数最开始加载程序载入
    static void writeConfig();  //写入配置文件,在更改配置文件程序关闭时调用
    static bool checkConfig();  //校验配置文件
    static void checkRatio();   //校验分辨率

    //获取和设置配置参数
    static void readConfig(const QString &objName,
                           QString &rtspAddr, QString &fileName,
                           bool &saveFile, bool &saveInterval,
                           QString &hardware, QString &transport,
                           bool &fillImage, bool &callback,
                           int &caching, int &imageFlag);
    static void writeConfig(const QString &objName,
                            QString rtspAddr, QString fileName,
                            bool saveFile, bool saveInterval,
                            QString hardware, QString transport,
                            bool fillImage, bool callback,
                            int caching, int imageFlag);

    //获取url集合
    static QStringList getUrls(QStringList &newurls);
    static void appendUrl(QStringList &urls, const QString &url);

    //设置字体+编码+居中
    static void setFont();
    static void setCode();
    static void setFormInCenter(QWidget *frm);
};

#endif // APP_H
