﻿#include "app.h"

QString App::ConfigFile = "config.ini";     //配置文件文件路径及名称
QString App::Version = "V20210119";         //版本号
QString App::TitleFlag = "(QQ: 517216493 WX: feiyangqingyun)";//标题标识

//全局配置
int App::RowHeight = 20;               //行高
int App::RightWidth = 180;             //右侧宽度
int App::FormWidth = 950;              //窗体宽度
int App::FormHeight = 650;             //窗体高度

QString App::UserName = "admin";//用户名
QString App::UserPwd = "admin";
QString App::LocalIP = "192.168.1.22";//网卡
QString App::DeviceIP = "192.168.1.128";//设备
QString App::SearchFilter = "none";                       //搜索条件过滤
QString App::SearchFilters = "none|:80|:8000|:2000|.1.";  //搜索条件集合
int App::SearchInterval = 300;//搜索间隔 单位毫秒
bool App::SearchClear = true; //搜索清空
//基础配置
int App::TabIndex = 0;//页面索引
int App::TabIndex1 = 0;//页面索引1
int App::TabIndex2 = 0;//页面索引2
bool App::IsMax = false;//显示最大化
bool App::IsFull = false;//显示全屏
QString App::VideoType = "1_16";//画面分割类型
int App::BufferWidth = 1280;//VLC回调模式分辨率宽度
int App::BufferHeight = 720;//VLC回调模式分辨率高度

QString App::RtspAddr = "f:/mp4/1.mp4";//视频流地址
QString App::Hardware = "qsv";         //设置硬件解码
QString App::Transport = "tcp";        //传输协议
bool App::FillImage = true;            //拉伸填充
bool App::Callback = false;            //回调
int App::Caching = 500;                //缓存时间
int App::ImageFlag = 0;                //图片质量
int App::CheckTime = 10000;            //超时时间
//视屏配置1
QString App::RtspAddr1 = "https://hls01open.ys7.com/openlive/6e0b2be040a943489ef0b9bb344b96b8.hd.m3u8";
QString App::FileName1 = "d:/1.mp4";//文件名
bool App::SaveFile1 = false;
bool App::SaveInterval1 = false;
QString App::Hardware1 = "none";
QString App::Transport1 = "tcp";
bool App::FillImage1 = true;
bool App::Callback1 = false;
int App::Caching1 = 500;
int App::ImageFlag1 = 0;
int App::CheckTime1 = 10000;
//视屏配置2
QString App::RtspAddr2 = "http://vfx.mtime.cn/Video/2019/03/18/mp4/190318231014076505.mp4";
QString App::FileName2 = "d:/2.mp4";
bool App::SaveFile2 = false;
bool App::SaveInterval2 = false;
QString App::Hardware2 = "none";
QString App::Transport2 = "tcp";
bool App::FillImage2 = true;
bool App::Callback2 = false;
int App::Caching2 = 500;
int App::ImageFlag2 = 0;
int App::CheckTime2 = 10000;
//视屏配置3
QString App::RtspAddr3 = "http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";
QString App::FileName3 = "d:/3.mp4";
bool App::SaveFile3 = false;
bool App::SaveInterval3 = false;
QString App::Hardware3 = "none";
QString App::Transport3 = "tcp";
bool App::FillImage3 = true;
bool App::Callback3 = false;
int App::Caching3 = 500;
int App::ImageFlag3 = 0;
int App::CheckTime3 = 10000;
//视屏配置4
QString App::RtspAddr4 = "http://vfx.mtime.cn/Video/2019/03/14/mp4/190314223540373995.mp4";
QString App::FileName4 = "d:/4.mp4";
bool App::SaveFile4 = false;
bool App::SaveInterval4 = false;
QString App::Hardware4 = "none";
QString App::Transport4 = "tcp";
bool App::FillImage4 = true;
bool App::Callback4 = false;
int App::Caching4 = 500;
int App::ImageFlag4 = 0;
int App::CheckTime4 = 10000;
//视屏配置5
QString App::RtspAddr5 = "http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";
QString App::Hardware5 = "none";
bool App::FillImage5 = false;
bool App::Callback5 = false;
//视屏配置6
QString App::RtspAddr6 = "http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";
QString App::Hardware6 = "none";
bool App::FillImage6 = false;
bool App::Callback6 = false;
//读取配置文件,在main函数最开始加载程序载入
void App::readConfig()
{
//checkConfig()配置为空，函数返回值为false,配置文件不为空，函数返回值为true
    if (!checkConfig())//如果配置文件为空直接返回，配置文件不为空，进行读配置文件
    {
        return;
    }

    QSettings set(App::ConfigFile, QSettings::IniFormat);

    set.beginGroup("AppConfig");//开始的组
    App::RowHeight = set.value("RowHeight", App::RowHeight).toInt();//行号
    App::RightWidth = set.value("RightWidth", App::RightWidth).toInt();//右侧宽度
    App::FormWidth = set.value("FormWidth", App::FormWidth).toInt();//窗体宽度
    App::FormHeight = set.value("FormHeight", App::FormHeight).toInt();//窗体高度
    set.endGroup();//结束的组

    set.beginGroup("SearchConfig");
    App::UserName = set.value("UserName", App::UserName).toString();
    App::UserPwd = set.value("UserPwd", App::UserPwd).toString();
    App::LocalIP = set.value("LocalIP", App::LocalIP).toString();
    App::DeviceIP = set.value("DeviceIP", App::DeviceIP).toString();
    App::SearchFilter = set.value("SearchFilter", App::SearchFilter).toString();
    App::SearchFilters = set.value("SearchFilters", App::SearchFilters).toString();
    App::SearchInterval = set.value("SearchInterval", App::SearchInterval).toInt();
    App::SearchClear = set.value("SearchClear", App::SearchClear).toBool();
    set.endGroup();

    set.beginGroup("BaseConfig");
    App::TabIndex = set.value("TabIndex", App::TabIndex).toInt();
    App::TabIndex1 = set.value("TabIndex1", App::TabIndex1).toInt();
    App::TabIndex2 = set.value("TabIndex2", App::TabIndex2).toInt();
    App::IsMax = set.value("IsMax", App::IsMax).toBool();
    App::IsFull = set.value("IsFull", App::IsFull).toBool();
    App::VideoType = set.value("VideoType", App::VideoType).toString();
    App::BufferWidth = set.value("BufferWidth", App::BufferWidth).toInt();
    App::BufferHeight = set.value("BufferHeight", App::BufferHeight).toInt();
    set.endGroup();

    set.beginGroup("VideoConfig");
    App::RtspAddr = set.value("RtspAddr", App::RtspAddr).toString();
    App::Hardware = set.value("Hardware", App::Hardware).toString();
    App::Transport = set.value("Transport", App::Transport).toString();
    App::FillImage = set.value("FillImage", App::FillImage).toBool();
    App::Callback = set.value("Callback", App::Callback).toBool();
    App::Caching = set.value("Caching", App::Caching).toInt();
    App::ImageFlag = set.value("ImageFlag", App::ImageFlag).toInt();
    App::CheckTime = set.value("CheckTime", App::CheckTime).toInt();
    set.endGroup();

    set.beginGroup("VideoConfig1");
    App::RtspAddr1 = set.value("RtspAddr1", App::RtspAddr1).toString();
    App::FileName1 = set.value("FileName1", App::FileName1).toString();
    App::SaveFile1 = set.value("SaveFile1", App::SaveFile1).toBool();
    App::SaveInterval1 = set.value("SaveInterval1", App::SaveInterval1).toBool();
    App::Hardware1 = set.value("Hardware1", App::Hardware1).toString();
    App::Transport1 = set.value("Transport1", App::Transport1).toString();
    App::FillImage1 = set.value("FillImage1", App::FillImage1).toBool();
    App::Callback1 = set.value("Callback1", App::Callback1).toBool();
    App::Caching1 = set.value("Caching1", App::Caching1).toInt();
    App::ImageFlag1 = set.value("ImageFlag1", App::ImageFlag1).toInt();
    App::CheckTime1 = set.value("CheckTime1", App::CheckTime1).toInt();
    set.endGroup();

    set.beginGroup("VideoConfig2");
    App::RtspAddr2 = set.value("RtspAddr2", App::RtspAddr2).toString();
    App::FileName2 = set.value("FileName2", App::FileName2).toString();
    App::SaveFile2 = set.value("SaveFile2", App::SaveFile2).toBool();
    App::SaveInterval2 = set.value("SaveInterval2", App::SaveInterval2).toBool();
    App::Hardware2 = set.value("Hardware2", App::Hardware2).toString();
    App::Transport2 = set.value("Transport2", App::Transport2).toString();
    App::FillImage2 = set.value("FillImage2", App::FillImage2).toBool();
    App::Callback2 = set.value("Callback2", App::Callback2).toBool();
    App::Caching2 = set.value("Caching2", App::Caching2).toInt();
    App::ImageFlag2 = set.value("ImageFlag2", App::ImageFlag2).toInt();
    App::CheckTime2 = set.value("CheckTime2", App::CheckTime2).toInt();
    set.endGroup();

    set.beginGroup("VideoConfig3");
    App::RtspAddr3 = set.value("RtspAddr3", App::RtspAddr3).toString();
    App::FileName3 = set.value("FileName3", App::FileName3).toString();
    App::SaveFile3 = set.value("SaveFile3", App::SaveFile3).toBool();
    App::SaveInterval3 = set.value("SaveInterval3", App::SaveInterval3).toBool();
    App::Hardware3 = set.value("Hardware3", App::Hardware3).toString();
    App::Transport3 = set.value("Transport3", App::Transport3).toString();
    App::FillImage3 = set.value("FillImage3", App::FillImage3).toBool();
    App::Callback3 = set.value("Callback3", App::Callback3).toBool();
    App::Caching3 = set.value("Caching3", App::Caching3).toInt();
    App::ImageFlag3 = set.value("ImageFlag3", App::ImageFlag3).toInt();
    App::CheckTime3 = set.value("CheckTime3", App::CheckTime3).toInt();
    set.endGroup();

    set.beginGroup("VideoConfig4");
    App::RtspAddr4 = set.value("RtspAddr4", App::RtspAddr4).toString();
    App::FileName4 = set.value("FileName4", App::FileName4).toString();
    App::SaveFile4 = set.value("SaveFile4", App::SaveFile4).toBool();
    App::SaveInterval4 = set.value("SaveInterval4", App::SaveInterval4).toBool();
    App::Hardware4 = set.value("Hardware4", App::Hardware4).toString();
    App::Transport4 = set.value("Transport4", App::Transport4).toString();
    App::FillImage4 = set.value("FillImage4", App::FillImage4).toBool();
    App::Callback4 = set.value("Callback4", App::Callback4).toBool();
    App::Caching4 = set.value("Caching4", App::Caching4).toInt();
    App::ImageFlag4 = set.value("ImageFlag4", App::ImageFlag4).toInt();
    App::CheckTime4 = set.value("CheckTime4", App::CheckTime4).toInt();
    set.endGroup();

    set.beginGroup("VideoConfig5");
    App::RtspAddr5 = set.value("RtspAddr5", App::RtspAddr5).toString();
    App::Hardware5 = set.value("Hardware5", App::Hardware5).toString();
    App::FillImage5 = set.value("FillImage5", App::FillImage5).toBool();
    App::Callback5 = set.value("Callback5", App::Callback5).toBool();
    set.endGroup();

    set.beginGroup("VideoConfig6");
    App::RtspAddr6 = set.value("RtspAddr6", App::RtspAddr6).toString();
    App::Hardware6 = set.value("Hardware6", App::Hardware6).toString();
    App::FillImage6 = set.value("FillImage6", App::FillImage6).toBool();
    App::Callback6 = set.value("Callback6", App::Callback6).toBool();
    set.endGroup();

    checkRatio();//进行校验分辨率
}
//写入配置文件
void App::writeConfig()
{
    checkRatio();//进行校验分辨率

    // 如果系统已经保存了 软件的配置信息, 则需要自动加载配置信息
    // 如果没有保存, 则使用默认的信息
    // 创建一个Appfig文件 , 文件不存在则创建, 文件存在则打开
    QSettings set(App::ConfigFile, QSettings::IniFormat);

    set.beginGroup("AppConfig");
    set.setValue("RowHeight", App::RowHeight);//行高
    set.setValue("RightWidth", App::RightWidth);//右侧宽度
    set.setValue("FormWidth", App::FormWidth);//窗体宽度
    set.setValue("FormHeight", App::FormHeight);//窗体高度
    set.endGroup();

    set.beginGroup("SearchConfig");
    set.setValue("UserName", App::UserName);
    set.setValue("UserPwd", App::UserPwd);
    set.setValue("LocalIP", App::LocalIP);
    set.setValue("DeviceIP", App::DeviceIP);
    set.setValue("SearchFilter", App::SearchFilter);
    set.setValue("SearchFilters", App::SearchFilters);
    set.setValue("SearchInterval", App::SearchInterval);
    set.setValue("SearchClear", App::SearchClear);
    set.endGroup();

    set.beginGroup("BaseConfig");
    set.setValue("TabIndex", App::TabIndex);//页面索引
    set.setValue("TabIndex1", App::TabIndex1);
    set.setValue("TabIndex2", App::TabIndex2);
    set.setValue("IsMax", App::IsMax);//显示最大化
    set.setValue("IsFull", App::IsFull);//显示全屏
    set.setValue("VideoType", App::VideoType);
    set.setValue("BufferWidth", App::BufferWidth);
    set.setValue("BufferHeight", App::BufferHeight);
    set.endGroup();

    set.beginGroup("VideoConfig");
    set.setValue("RtspAddr", App::RtspAddr);
    set.setValue("Hardware", App::Hardware);
    set.setValue("Transport", App::Transport);
    set.setValue("FillImage", App::FillImage);
    set.setValue("Callback", App::Callback);
    set.setValue("Caching", App::Caching);
    set.setValue("ImageFlag", App::ImageFlag);
    set.setValue("CheckTime", App::CheckTime);
    set.endGroup();

    set.beginGroup("VideoConfig1");
    set.setValue("RtspAddr1", App::RtspAddr1);
    set.setValue("FileName1", App::FileName1);
    set.setValue("SaveFile1", App::SaveFile1);
    set.setValue("SaveInterval1", App::SaveInterval1);
    set.setValue("Hardware1", App::Hardware1);
    set.setValue("Transport1", App::Transport1);
    set.setValue("FillImage1", App::FillImage1);
    set.setValue("Callback1", App::Callback1);
    set.setValue("Caching1", App::Caching1);
    set.setValue("ImageFlag1", App::ImageFlag1);
    set.setValue("CheckTime1", App::CheckTime1);
    set.endGroup();

    set.beginGroup("VideoConfig2");
    set.setValue("RtspAddr2", App::RtspAddr2);
    set.setValue("FileName2", App::FileName2);
    set.setValue("SaveFile2", App::SaveFile2);
    set.setValue("SaveInterval2", App::SaveInterval2);
    set.setValue("Hardware2", App::Hardware2);
    set.setValue("Transport2", App::Transport2);
    set.setValue("FillImage2", App::FillImage2);
    set.setValue("Callback2", App::Callback2);
    set.setValue("Caching2", App::Caching2);
    set.setValue("ImageFlag2", App::ImageFlag2);
    set.setValue("CheckTime2", App::CheckTime2);
    set.endGroup();

    set.beginGroup("VideoConfig3");
    set.setValue("RtspAddr3", App::RtspAddr3);
    set.setValue("FileName3", App::FileName3);
    set.setValue("SaveFile3", App::SaveFile3);
    set.setValue("SaveInterval3", App::SaveInterval3);
    set.setValue("Hardware3", App::Hardware3);
    set.setValue("Transport3", App::Transport3);
    set.setValue("FillImage3", App::FillImage3);
    set.setValue("Callback3", App::Callback3);
    set.setValue("Caching3", App::Caching3);
    set.setValue("ImageFlag3", App::ImageFlag3);
    set.setValue("CheckTime3", App::CheckTime3);
    set.endGroup();

    set.beginGroup("VideoConfig4");
    set.setValue("RtspAddr4", App::RtspAddr4);
    set.setValue("FileName4", App::FileName4);
    set.setValue("SaveFile4", App::SaveFile4);
    set.setValue("SaveInterval4", App::SaveInterval4);
    set.setValue("Hardware4", App::Hardware4);
    set.setValue("Transport4", App::Transport4);
    set.setValue("FillImage4", App::FillImage4);
    set.setValue("Callback4", App::Callback4);
    set.setValue("Caching4", App::Caching4);
    set.setValue("ImageFlag4", App::ImageFlag4);
    set.setValue("CheckTime4", App::CheckTime4);
    set.endGroup();

    set.beginGroup("VideoConfig5");
    set.setValue("RtspAddr5", App::RtspAddr5);
    set.setValue("Hardware5", App::Hardware5);
    set.setValue("FillImage5", App::FillImage5);
    set.setValue("Callback5", App::Callback5);
    set.endGroup();

    set.beginGroup("VideoConfig6");
    set.setValue("RtspAddr6", App::RtspAddr6);
    set.setValue("Hardware6", App::Hardware6);
    set.setValue("FillImage6", App::FillImage6);
    set.setValue("Callback6", App::Callback6);
    set.endGroup();
}
//校验配置文件
//判断配置文件的大小，false表示配置文件为空，true表示有配置文件中有内容
bool App::checkConfig()
{
    //如果配置文件大小为0,则以初始值继续运行,并生成配置文件
    QFile file(App::ConfigFile);
    if (file.size() == 0)//判断文件大小大小是否为零
    {
        writeConfig();//写配置文件
        return false;
    }
    //如果配置文件不完整,则以初始值继续运行,并生成配置文件
    if (file.open(QFile::ReadOnly)) //以只读方式打开
    {
        bool ok = true;
        //到达文件末尾返回true,否则返回false
        while (!file.atEnd())//file.atEnd()到达文件末尾为true,否则为false
        {
            QString line = file.readLine();
            line = line.replace("\r", "");
            line = line.replace("\n", "");
            QStringList list = line.split("=");

            if (list.count() == 2) {
                if (list.at(1) == "") {
                    ok = false;
                    break;
                }
            }
        }

        if (!ok)
        {
            writeConfig();//调用写入配置文件函数
            return false;
        }
    }
    else
    {
        writeConfig();//调用写入配置文件函数
        return false;
    }
    return true;
}
//校验分辨率
void App::checkRatio()
{
    //根据分辨率设定宽高
    int width = qApp->desktop()->geometry().width();
    if (width > 1440)//判断宽度
    {
        App::RowHeight = App::RowHeight < 25 ? 25 : App::RowHeight;
        App::RightWidth = App::RightWidth < 220 ? 220 : App::RightWidth;
        App::FormWidth = App::FormWidth < 1300 ? 1300 : App::FormWidth;
        App::FormHeight = App::FormHeight < 800 ? 800 : App::FormHeight;
    }
}
//读取配置文件
void App::readConfig(const QString &objName,
                     QString &rtspAddr, QString &fileName,
                     bool &saveFile, bool &saveInterval,
                     QString &hardware, QString &transport,
                     bool &fillImage, bool &callback,
                     int &caching, int &imageFlag)
{
    if (objName == "video1") {
        rtspAddr = App::RtspAddr1;
        fileName = App::FileName1;
        saveFile = App::SaveFile1;
        saveInterval = App::SaveInterval1;
        hardware = App::Hardware1;
        transport = App::Transport1;
        fillImage = App::FillImage1;
        callback = App::Callback1;
        caching = App::Caching1;
        imageFlag = App::ImageFlag1;
    } else if (objName == "video2") {
        rtspAddr = App::RtspAddr2;
        fileName = App::FileName2;
        saveFile = App::SaveFile2;
        saveInterval = App::SaveInterval2;
        hardware = App::Hardware2;
        transport = App::Transport2;
        fillImage = App::FillImage2;
        callback = App::Callback2;
        caching = App::Caching2;
        imageFlag = App::ImageFlag2;
    } else if (objName == "video3") {
        rtspAddr = App::RtspAddr3;
        fileName = App::FileName3;
        saveFile = App::SaveFile3;
        saveInterval = App::SaveInterval3;
        hardware = App::Hardware3;
        transport = App::Transport3;
        fillImage = App::FillImage3;
        callback = App::Callback3;
        caching = App::Caching3;
        imageFlag = App::ImageFlag3;
    } else if (objName == "video4") {
        rtspAddr = App::RtspAddr4;
        fileName = App::FileName4;
        saveFile = App::SaveFile4;
        saveInterval = App::SaveInterval4;
        hardware = App::Hardware4;
        transport = App::Transport4;
        fillImage = App::FillImage4;
        callback = App::Callback4;
        caching = App::Caching4;
        imageFlag = App::ImageFlag4;
    }
}
//写入配置文件
void App::writeConfig(const QString &objName,
                      QString rtspAddr, QString fileName,
                      bool saveFile, bool saveInterval,
                      QString hardware, QString transport,
                      bool fillImage, bool callback,
                      int caching, int imageFlag)
{
    if (objName == "video1") {
        App::RtspAddr1 = rtspAddr;
        App::FileName1 = fileName;
        App::SaveFile1 = saveFile;
        App::SaveInterval1 = saveInterval;
        App::Hardware1 = hardware;
        App::Transport1 = transport;
        App::FillImage1 = fillImage;
        App::Callback1 = callback;
        App::Caching1 = caching;
        App::ImageFlag1 = imageFlag;
    } else if (objName == "video2") {
        App::RtspAddr2 = rtspAddr;
        App::FileName2 = fileName;
        App::SaveFile2 = saveFile;
        App::SaveInterval2 = saveInterval;
        App::Hardware2 = hardware;
        App::Transport2 = transport;
        App::FillImage2 = fillImage;
        App::Callback2 = callback;
        App::Caching2 = caching;
        App::ImageFlag2 = imageFlag;
    } else if (objName == "video3") {
        App::RtspAddr3 = rtspAddr;
        App::FileName3 = fileName;
        App::SaveFile3 = saveFile;
        App::SaveInterval3 = saveInterval;
        App::Hardware3 = hardware;
        App::Transport3 = transport;
        App::FillImage3 = fillImage;
        App::Callback3 = callback;
        App::Caching3 = caching;
        App::ImageFlag3 = imageFlag;
    } else if (objName == "video4") {
        App::RtspAddr4 = rtspAddr;
        App::FileName4 = fileName;
        App::SaveFile4 = saveFile;
        App::SaveInterval4 = saveInterval;
        App::Hardware4 = hardware;
        App::Transport4 = transport;
        App::FillImage4 = fillImage;
        App::Callback4 = callback;
        App::Caching4 = caching;
        App::ImageFlag4 = imageFlag;
    }
    App::writeConfig();
}
//获取播放视屏的地址
QStringList App::getUrls(QStringList &newurls)
{
    //读取配置文件地址列表
    static QStringList urls;
    //已经读取过一次就不用在读取
    int count = newurls.count();
    if (count == 0 && urls.count() > 0)
    {
        return urls;
    }

    QFile file(AppPath + "/url.txt");
    if (file.open(QFile::ReadOnly))//已只读打开文件
    {
        while (!file.atEnd())
        {
            QString line = file.readLine();
            line = line.trimmed();
            line = line.replace("\r", "");
            line = line.replace("\n", "");
            if (!line.isEmpty() && !line.startsWith("#")) {
                QStringList list = line.split(":");
                int ch = list.at(0).toInt();
                if (ch > 0) {
                    QString url = line.mid(line.indexOf(":") + 1, line.length());
                    urls << url;

                    //替换掉新的url
                    if (count > ch - 1) {
                        newurls[ch - 1] = url;
                    }
                }
            }
        }

        file.close();
    }

#if 0
    //由于视频源不稳定暂时屏蔽
    //cctv 格式可以自行增加后缀 cctc1 cctv2 依次下去
    appendUrl(urls, "rtmp://58.200.131.2:1935/livetv/cctv1");
    appendUrl(urls, "rtmp://58.200.131.2:1935/livetv/cctv6");
    appendUrl(urls, "rtmp://58.200.131.2:1935/livetv/hunantv");
    appendUrl(urls, "http://ivi.bupt.edu.cn/hls/cctv1hd.m3u8");
    appendUrl(urls, "http://ivi.bupt.edu.cn/hls/cctv6hd.m3u8");
    appendUrl(urls, "http://112.17.40.140/PLTV/88888888/224/3221226557/index.m3u8");
    appendUrl(urls, "http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear2/prog_index.m3u8");
    appendUrl(urls, "https://hls01open.ys7.com/openlive/6e0b2be040a943489ef0b9bb344b96b8.hd.m3u8");
    appendUrl(urls, "http://vts.simba-cn.com:280/gb28181/21100000001320000002.m3u8");
#endif
    //视屏流播放器的地址
    appendUrl(urls, "http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4");
    appendUrl(urls, "http://vfx.mtime.cn/Video/2019/03/18/mp4/190318231014076505.mp4");
    appendUrl(urls, "http://vfx.mtime.cn/Video/2019/03/19/mp4/190319212559089721.mp4");
    appendUrl(urls, "http://vfx.mtime.cn/Video/2019/03/17/mp4/190317150237409904.mp4");
    appendUrl(urls, "http://vfx.mtime.cn/Video/2019/03/14/mp4/190314223540373995.mp4");

    appendUrl(urls, "http://192.168.1.22:8083");
    appendUrl(urls, "rtsp://192.168.1.108:554/cam/realmonitor?channel=1&subtype=0&unicast=true&proto=Onvif");
    appendUrl(urls, "rtsp://admin:Admin123456@192.168.1.64:554/Streaming/Channels/101?transportmode=unicast&profile=Profile_2");
    appendUrl(urls, "rtsp://admin:Admin123456@192.168.1.64:554/Streaming/Channels/102?transportmode=unicast&profile=Profile_2");
    appendUrl(urls, "rtsp://admin:12345@192.168.1.15:554/media/video1");
    appendUrl(urls, "rtsp://admin:12345@192.168.1.15:554/media/video2");
    appendUrl(urls, "rtsp://192.168.1.247:554/av0_0");
    appendUrl(urls, "rtsp://192.168.1.247:554/av0_1");
    appendUrl(urls, "rtsp://192.168.1.18:554/1/h264major");
    appendUrl(urls, "rtsp://192.168.1.18:554/1/h264minor");

    //格式 搜索到的 https://music.163.com/#/song?id=179768
    appendUrl(urls, "http://music.163.com/song/media/outer/url?id=179768.mp3");
    appendUrl(urls, "http://music.163.com/song/media/outer/url?id=281951.mp3");
    appendUrl(urls, "http://music.163.com/song/media/outer/url?id=447925558.mp3");

    //后面的都是win系统上的路径和格式 其他系统无意义
#ifndef Q_OS_WIN//判断这个宏，定义宏执行其后的代码
    appendUrl(urls, "/dev/video0");
    appendUrl(urls, "/dev/video1");
    return urls;
#endif

    appendUrl(urls, "f:/mp3/1.mp3");
    appendUrl(urls, "f:/mp3/1.wav");
    appendUrl(urls, "f:/mp3/1.wma");
    appendUrl(urls, "f:/mp3/1.mid");

    appendUrl(urls, "f:/mp4/1.mp4");
    appendUrl(urls, "f:/mp4/1000.mkv");
    appendUrl(urls, "f:/mp4/1001.wmv");
    appendUrl(urls, "f:/mp4/1002.mov");
    appendUrl(urls, "f:/mp4/1003.mp4");
    appendUrl(urls, "f:/mp4/1080.mp4");

    appendUrl(urls, "f:/mp5/1.ts");
    appendUrl(urls, "f:/mp5/1.asf");
    appendUrl(urls, "f:/mp5/1.mp4");
    appendUrl(urls, "f:/mp5/5.mp4");
    appendUrl(urls, "f:/mp5/1.rmvb");
    appendUrl(urls, "f:/mp5/h264.aac");
    appendUrl(urls, "f:/mp5/h264.mp4");
    appendUrl(urls, "f:/mp5/haikang.mp4");
    appendUrl(urls, "f:/mp4/新建文件夹/1.mp4");

    appendUrl(urls, "dshow://:dshow-vdev='Default'");
    appendUrl(urls, "video=LIHAPPE8-316A");
    appendUrl(urls, "video=USB2.0 PC CAMERA");
    appendUrl(urls, "video=Logitech HD Webcam C270");
    return urls;
}

void App::appendUrl(QStringList &urls, const QString &url)
{
    if (!urls.contains(url))
    {
        urls << url;
    }
}
//设置字体
void App::setFont()
{
    //qApp引用唯一应用程序对象的全局指针。它等价于QCoreApplication::instance()，
    //但转换为QApplication指针，因此仅当唯一应用程序对象是QApplication时有效。
    qApp->setPalette(QPalette("#FFFFFF"));

    QFont font;//通过QFont可以部分Qt控件的文字的大小、字体、字间距、下划线等属性
    font.setFamily("MicroSoft Yahei");//设置字体为微软雅黑
#ifdef Q_OS_ANDROID     //宏定义，执行其后的语句
    font.setPixelSize(15);
#elif __arm__
    font.setPixelSize(25);
#else
    font.setPixelSize(12);//setPixelSize 设置字体高度
#endif

#ifndef rk3399 //rk3399 如果宏没有定义，执行后面的代码
    qApp->setFont(font);
#endif
}

//设置编码
void App::setCode()
{
#if (QT_VERSION <= QT_VERSION_CHECK(5,0,0))
#if _MSC_VER
    QTextCodec *codec = QTextCodec::codecForName("gbk");
#else
    QTextCodec *codec = QTextCodec::codecForName("utf-8");
#endif
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);
#else
    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    QTextCodec::setCodecForLocale(codec);
#endif
}
//设置居中
void App::setFormInCenter(QWidget *frm)
{
    int frmX = frm->width();
    int frmY = frm->height();
    QDesktopWidget w;
    int deskWidth = w.availableGeometry().width();
    int deskHeight = w.availableGeometry().height();
    QPoint movePoint(deskWidth / 2 - frmX / 2, deskHeight / 2 - frmY / 2);
    frm->move(movePoint);

    //其他系统自动最大化
#ifndef Q_OS_WIN
    QTimer::singleShot(100, frm, SLOT(showMaximized()));
#endif
}
