﻿#ifndef FRMPLAYER_H
#define FRMPLAYER_H

#include <QWidget>

namespace Ui
{
    class frmPlayer;
}

class frmPlayer : public QWidget
{
    Q_OBJECT

public:
    explicit frmPlayer(QWidget *parent = 0);
    ~frmPlayer();

protected:
    void fullScreen();
    void keyReleaseEvent(QKeyEvent *key);//切换视屏播放模式
    bool eventFilter(QObject *watched, QEvent *event);//鼠标双击即全屏

private:
    Ui::frmPlayer *ui;
    bool full;

private slots:
    void initForm();//初始化窗体
    void initConfig();//初始化配置文件
    void saveConfig();//保存配置文件

private slots:
    //播放成功
    void receivePlayStart();
    //播放失败
    void receivePlayError();
    //播放结束
    void receivePlayFinsh();

    //总时长
    void fileLengthReceive(qint64 length);
    //当前播放时长
    void filePositionReceive(qint64 position);
    //音量大小
    void fileVolumeReceive(int volume, bool muted);    

private slots:
    void on_btnSelect_clicked();//选择功能
    void on_btnPlay_clicked();//播放
    void on_btnStop_clicked();//停止
    void on_btnPause_clicked();//暂停
    void on_btnNext_clicked();//继续
    void on_btnAbout_clicked();//关于
    void on_ckMuted_clicked();//静音
    void on_ckCallback_clicked();//回调功能
    void on_btnVersion_clicked();//版本
    void on_sliderPosition_clicked();//视屏当前时长函数
    void on_sliderVolume_clicked();//音量大小函数
    void on_sliderPosition_sliderMoved(int position);//当前时长的滑块移动函数
    void on_sliderVolume_sliderMoved(int position);//声音大小的移动函数

signals:
    void fullScreen(bool full);//设置全屏信号处理函数
};

#endif // FRMPLAYER_H
