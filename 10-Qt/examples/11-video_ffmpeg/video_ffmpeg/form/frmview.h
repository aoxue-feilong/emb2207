﻿#ifndef FRMVIEW_H
#define FRMVIEW_H

#include <QWidget>

class QMenu;
class FFmpegWidget;

namespace Ui {
class frmView;
}

class frmView : public QWidget//frmView这个类继承与QWidget
{
    Q_OBJECT

public:
    //explicit 关键字，在构造函数中声明explicit时，这个构造函数调用时只能显示调用，不能隐式调用
    explicit frmView(QWidget *parent = 0);
    ~frmView();

protected:
    void keyReleaseEvent(QKeyEvent *key);
    void showEvent(QShowEvent *);
    bool eventFilter(QObject *watched, QEvent *event);

private:
    Ui::frmView *ui;

    bool videoMax;
    int videoCount;
    QMenu *videoMenu;
    QAction *actionFull;
    QAction *actionPoll;
    QList<FFmpegWidget *> widgets;

private slots:
    void initForm();
    void initMenu();
    void full();
    void poll();

private slots:
    void play_video_all();//播放所有视屏
    void snapshot_video_one();//截图单一视屏
    void snapshot_video_all();//截图所有视屏

    void show_video_all();//显示所有视屏
    //显示画面的个数
    void show_video_4();
    void show_video_6();
    void show_video_8();
    void show_video_9();
    void show_video_13();
    void show_video_16();
    void show_video_25();
    void show_video_36();
    void show_video_64();
    //隐藏所有视屏
    void hide_video_all();
    //视屏改变
    void change_video(int index, int flag);
    //改变成相对应的视屏通道
    void change_video_4(int index);
    void change_video_6(int index);
    void change_video_8(int index);
    void change_video_9(int index);
    void change_video_13(int index);
    void change_video_16(int index);
    void change_video_25(int index);
    void change_video_36(int index);
    void change_video_64(int index);

signals:
    void fullScreen(bool full);//全屏幕设置信号
};

#endif // FRMVIEW_H
