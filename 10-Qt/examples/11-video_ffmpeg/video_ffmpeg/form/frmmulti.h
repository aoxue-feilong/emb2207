﻿#ifndef FRMMULTI_H
#define FRMMULTI_H

#include <QWidget>

namespace Ui {
class frmMulti;
}

class frmMulti : public QWidget
{
    Q_OBJECT

public:
    explicit frmMulti(QWidget *parent = 0);
    ~frmMulti();

protected:
    void showEvent(QShowEvent *);//显示事件

private:
    Ui::frmMulti *ui;

private slots:
    void initForm();//初始化窗口
    void initConfig();//初始化配置文件
    void playStart();//播放开始
    void playFinsh();//播放结束
};

#endif // FRMMULTI_H
