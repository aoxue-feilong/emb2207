﻿#include "frmmulti.h"
#include "ui_frmmulti.h"
#include "head.h"

//多段复用

frmMulti::frmMulti(QWidget *parent) : QWidget(parent), ui(new Ui::frmMulti)
{
    ui->setupUi(this);
    this->initForm();//初始化窗体
    this->initConfig();//初始化配置文件
}

frmMulti::~frmMulti()
{
    delete ui;
}
//显示事件
void frmMulti::showEvent(QShowEvent *)
{
    if (!ui->playWidget1->getIsPlaying())
    {
        ui->playWidget1->open();
    }
}
//初始化窗体
void frmMulti::initForm()
{
    QList<FFmpegWidget *> widgets;
    widgets << ui->playWidget2 << ui->playWidget3 << ui->playWidget4;
    foreach (FFmpegWidget *widget, widgets)
    {
        connect(ui->playWidget1, SIGNAL(receiveImage(QImage)), widget, SLOT(updateImage(QImage)));
        connect(ui->playWidget1, SIGNAL(receiveFrame(AVFrame *)), widget, SLOT(updateFrame(AVFrame *)));
    }

    connect(ui->playWidget1, SIGNAL(receivePlayStart()), this, SLOT(playStart()));
    connect(ui->playWidget1, SIGNAL(receivePlayFinsh()), this, SLOT(playFinsh()));
}
//初始化配置文件
void frmMulti::initConfig()
{
    //App::RtspAddr6 = "f:/mp5/1.mp4";
    //App::Hardware6 = "dxva2";
    QList<FFmpegWidget *> widgets;
    widgets << ui->playWidget1 << ui->playWidget2 << ui->playWidget3 << ui->playWidget4;
    foreach (FFmpegWidget *widget, widgets)
    {
        widget->setUrl(App::RtspAddr6);//设置视屏播放
        widget->setHardware(App::Hardware6);//设置视屏硬件解码
        widget->setFillImage(App::FillImage6);//设置是否拉伸填充
        widget->setCallback(App::Callback6);//设置是否采用回调
    }
}
//播放成功
void frmMulti::playStart()
{
    //需要将视频宽高传给其他几个控件
    FFmpegThread *thread = ui->playWidget1->getThread();//获取采集线程
    int width = thread->getVideoWidth();
    int height = thread->getVideoHeight();

    QList<FFmpegWidget *> widgets;
    widgets << ui->playWidget2 << ui->playWidget3 << ui->playWidget4;
    foreach (FFmpegWidget *widget, widgets)
    {
        widget->setMultiMode(true, width, height);//设置复用模式
        QMetaObject::invokeMethod(widget, "playStart");//调用这个窗体的播放成功函数
    }
}
//播放结束
void frmMulti::playFinsh()
{
    QList<FFmpegWidget *> widgets;
    widgets << ui->playWidget2 << ui->playWidget3 << ui->playWidget4;
    foreach (FFmpegWidget *widget, widgets)
    {
        QMetaObject::invokeMethod(widget, "playFinsh");//调用这个窗体的播放结束函数
        widget->setMultiMode(false, 0, 0);//设置复用模式
    }
}
