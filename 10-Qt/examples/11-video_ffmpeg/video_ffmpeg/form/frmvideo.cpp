﻿#include "frmvideo.h"
#include "ui_frmvideo.h"
#include "head.h"
#include "ffmpegwidget.h"

frmVideo::frmVideo(QWidget *parent) : QWidget(parent), ui(new Ui::frmVideo)
{
    ui->setupUi(this);
    this->initForm();//初始化窗体
    QTimer::singleShot(100, this, SLOT(initConfig()));//100毫秒自动结束函数
}

frmVideo::~frmVideo()
{
    delete ui;
}
//初始化窗体
void frmVideo::initForm()
{
    QStringList urls;
    ui->cboxUrl->addItems(App::getUrls(urls));

    QFont font;
    //设置字体大小为pixelsize像素
    font.setPixelSize(qApp->font().pixelSize() + 2);
    font.setBold(true);//为真字体加粗

    ui->btnOSD1->setFont(font);//默认文字属性
    ui->btnOSD2->setFont(font);
    //背景颜色
    ui->btnOSD1->setStyleSheet(QString("background:#000000;color:%1;").arg(ui->btnOSD1->text()));
    ui->btnOSD2->setStyleSheet(QString("background:#000000;color:%1;").arg(ui->btnOSD2->text()));
    ui->cboxOSD1->setCurrentIndex(ui->cboxOSD1->findText("右上角"));
    ui->cboxOSD2->setCurrentIndex(ui->cboxOSD2->findText("左下角"));

    //设置背景图片或者背景文字
    ui->playWidget->setBgText("视频监控");
    ui->playWidget->setBgImage(QImage(":/image/bg_novideo.png"));

    //设置OSD图片
    ui->playWidget->setOSD1Image(QImage(":/image/bg_novideo.png"));
    ui->playWidget->setOSD2Image(QImage(":/image/bg_novideo.png"));

    //设置边框宽度和颜色
    ui->playWidget->setBorderWidth(3);
    ui->playWidget->setBorderColor(QColor(0, 0, 0));

    //设置悬浮条可见,绑定顶部工具栏按钮单击事件
    ui->playWidget->setFlowEnable(true);
    connect(ui->playWidget, SIGNAL(fileDrag(QString)), this, SLOT(fileDrag(QString)));
    connect(ui->playWidget, SIGNAL(btnClicked(QString)), this, SLOT(btnClicked(QString)));
    ui->tabWidget->setCurrentIndex(0);

    //绑定开始播放+停止播放+截图信号
    connect(ui->playWidget, SIGNAL(receivePlayStart()), this, SLOT(receivePlayStart()));
    connect(ui->playWidget, SIGNAL(receivePlayError()), this, SLOT(receivePlayError()));
    connect(ui->playWidget, SIGNAL(receivePlayFinsh()), this, SLOT(receivePlayFinsh()));
    connect(ui->playWidget, SIGNAL(snapImage(QImage)), this, SLOT(snapImage(QImage)));

    //安卓屏幕比较小尽量空出位置
#ifdef Q_OS_ANDROID
    ui->labFillImage->setVisible(false);
    ui->cboxFillImage->setVisible(false);
    ui->btnOSD1->setVisible(false);
    ui->btnOSD2->setVisible(false);
    this->layout()->setContentsMargins(0, 0, 0, 0);
#endif
}
//初始化配置文件
void frmVideo::initConfig()
{
    QString rtspAddr = App::RtspAddr1;//视频流地址
    QString fileName = App::FileName1;//文件名
    bool saveFile = App::SaveFile1;//保存文件名
    bool saveInterval = App::SaveInterval1;//定时保存
    QString hardware = App::Hardware1;//视屏硬件解码名称
    QString transport = App::Transport1;//传输协议
    bool fillImage = App::FillImage1;//伸拉填充
    bool callback = App::Callback1;//回调
    int caching = App::Caching1;//缓存时间
    int imageFlag = App::ImageFlag1;//图片质量
    QString objName = this->objectName();//
    App::readConfig(objName, rtspAddr, fileName, saveFile, saveInterval, hardware, transport, fillImage, callback, caching, imageFlag);

    ui->cboxUrl->lineEdit()->setText(rtspAddr);
    ui->txtFileName->setText(fileName);
    ui->ckSaveFile->setChecked(saveFile);
    ui->ckSaveInterval->setChecked(saveInterval);
    ui->cboxHardware->setCurrentIndex(ui->cboxHardware->findText(hardware));
    ui->cboxTransport->setCurrentIndex(ui->cboxTransport->findText(transport));
    ui->cboxFillImage->setCurrentIndex(fillImage ? 0 : 1);
    ui->cboxCallback->setCurrentIndex(callback ? 1 : 0);
    ui->cboxImageFlag->setCurrentIndex(imageFlag);

    connect(ui->cboxUrl->lineEdit(), SIGNAL(textChanged(QString)), this, SLOT(saveConfig()));
    connect(ui->txtFileName, SIGNAL(textChanged(QString)), this, SLOT(saveConfig()));
    connect(ui->ckSaveFile, SIGNAL(stateChanged(int)), this, SLOT(saveConfig()));
    connect(ui->ckSaveInterval, SIGNAL(stateChanged(int)), this, SLOT(saveConfig()));
    connect(ui->cboxHardware, SIGNAL(currentIndexChanged(int)), this, SLOT(saveConfig()));
    connect(ui->cboxTransport, SIGNAL(currentIndexChanged(int)), this, SLOT(saveConfig()));
    connect(ui->cboxFillImage, SIGNAL(currentIndexChanged(int)), this, SLOT(saveConfig()));
    connect(ui->cboxCallback, SIGNAL(currentIndexChanged(int)), this, SLOT(saveConfig()));
    connect(ui->cboxImageFlag, SIGNAL(currentIndexChanged(int)), this, SLOT(saveConfig()));
}
//保存配置文件
void frmVideo::saveConfig()
{
    QString rtspAddr = ui->cboxUrl->currentText().trimmed();//视频流地址
    QString fileName = ui->txtFileName->text().trimmed();//文件名
    bool saveFile = ui->ckSaveFile->isChecked();//保存文件名
    bool saveInterval = ui->ckSaveInterval->isChecked();//定时保存
    QString hardware = ui->cboxHardware->currentText();//视频硬件解码名称
    QString transport = ui->cboxTransport->currentText();//传输协议
    bool fillImage = (ui->cboxFillImage->currentIndex() == 0);//伸拉填充
    bool callback = (ui->cboxCallback->currentIndex() == 1);//回调
    int caching = 0;//缓存时间
    int imageFlag = ui->cboxImageFlag->currentIndex();//图片质量、
    QString objName = this->objectName();
    App::writeConfig(objName, rtspAddr, fileName, saveFile, saveInterval, hardware, transport, fillImage, callback, caching, imageFlag);
}
//接收到拖曳文件
void frmVideo::fileDrag(const QString &url)
{
    ui->cboxUrl->lineEdit()->setText(url);
    qDebug() << TIMEMS << "拖进文件名称" << url;
}
//工具栏单击
void frmVideo::btnClicked(const QString &btnName)
{
    qDebug() << TIMEMS << "当前按下按钮" << btnName;
}
//播放成功
void frmVideo::receivePlayStart()
{
    ui->btnOpen->setText("关闭");
}
//播放错误
void frmVideo::receivePlayError()
{
    ui->btnOpen->setText("打开");
}
//播放结束
void frmVideo::receivePlayFinsh()
{
    ui->btnOpen->setText("打开");
}
//截图信号
void frmVideo::snapImage(const QImage &image)
{
    QString fileName = QString("%1/snap/%2.jpg").arg(AppPath).arg(QDateTime::currentDateTime().toString("yyyy-MM-dd-HH-mm-ss-zzz"));
    if (!image.isNull())
    {
        image.save(fileName, "jpg");
    }
}
//得到视频播放链接
QString frmVideo::getUrl() const
{
    //cboxUrl 打开地址 currentText()当前combox（打开地址）的文本
    return ui->cboxUrl->currentText();//返回当前combox（打开地址）的文本
}
//设置视频播放链接
void frmVideo::setUrl(const QString &url)
{
    ui->cboxUrl->lineEdit()->setText(url);
}

void frmVideo::setName(const QString &name)
{
    this->setObjectName(name);
    ui->playWidget->setObjectName(name + "_ffmpeg");
}
//设置视屏硬件解码
void frmVideo::setHardware(const QString &hardware)
{
    //setCurrentIndex 设置当前索引值
    //findText 返回包括给定文本的项的索引，否则返回-1
    ui->cboxHardware->setCurrentIndex(ui->cboxHardware->findText(hardware));
}
//标签1 状态改变
void frmVideo::on_ckOSD1_stateChanged(int arg1)
{
    //setOSD1Visible 设置标签1 是否可见
    ui->playWidget->setOSD1Visible(arg1 != 0);
}
//标签2 状态改变
void frmVideo::on_ckOSD2_stateChanged(int arg1)
{
    //setOSD2Visible 设置标签2 是否可见
    ui->playWidget->setOSD2Visible(arg1 != 0);
}
//标签功能中的标签1，cboxFont1（字体的下拉列表） 下拉列表中的索引值发生改变
void frmVideo::on_cboxFont1_currentIndexChanged(int index)
{
    //setOSD1FontSize   设置标签1文字字号
    ui->playWidget->setOSD1FontSize(ui->cboxFont1->currentText().toInt());
}
//标签功能中的标签2，cboxFont2（字体的下拉列表） 下拉列表中的索引值发生改变
void frmVideo::on_cboxFont2_currentIndexChanged(int index)
{
    //setOSD1FontSize   设置标签1文字字号
    ui->playWidget->setOSD2FontSize(ui->cboxFont2->currentText().toInt());
}

//标签功能中的标签1，cboxFont1（格式的下拉列表） 下拉列表中的索引值发生改变
void frmVideo::on_cboxFormat1_currentIndexChanged(int index)
{
    // setOSD1Format 设置标签1的格式（日期 ，图片等等）
    ui->playWidget->setOSD1Format((FFmpegWidget::OSDFormat)index);
}
//标签功能中的标签2，cboxFont2（格式的下拉列表） 下拉列表中的索引值发生改变
void frmVideo::on_cboxFormat2_currentIndexChanged(int index)
{
    ui->playWidget->setOSD2Format((FFmpegWidget::OSDFormat)index);
}

//在ui界面中标签1显示的位置（左右上下角 4个位置）
void frmVideo::on_cboxOSD1_currentIndexChanged(int index)
{
    //setOSD1Position 设置标签1的位置
    ui->playWidget->setOSD1Position((FFmpegWidget::OSDPosition)index);
}
//在ui界面中标签2显示的位置（左右上下角 4个位置）
void frmVideo::on_cboxOSD2_currentIndexChanged(int index)
{
    //setOSD2Position 设置标签2的位置
    ui->playWidget->setOSD2Position((FFmpegWidget::OSDPosition)index);
}

//setOSD1Position 设置标签1的文本
void frmVideo::on_txtOSD1_textChanged(const QString &arg1)
{
    //setOSD1Text 设置标签1的文本
    ui->playWidget->setOSD1Text(arg1);
}

void frmVideo::on_txtOSD2_textChanged(const QString &arg1)
{
    //setOSD2Text 设置标签2的文本
    ui->playWidget->setOSD2Text(arg1);
}

//  标签1中设置颜色的按钮的颜色被单击，自动跳出来设置文本颜色的对话框
void frmVideo::on_btnOSD1_clicked()
{
    QColor color = QColorDialog::getColor();//显示颜色对话框
    //isValid() 判断该颜色是否有效，有效返回值true，否则返回false
    if (color.isValid())
    {
        //setOSD1Color 设置标签1的文字颜色
        ui->playWidget->setOSD1Color(color);
        //setText 设置显示内容
        ui->btnOSD1->setText(color.name().toUpper());
        //设置图形界面的外观：
        ui->btnOSD1->setStyleSheet(QString("background:#000000;color:%1;").arg(ui->btnOSD1->text()));
    }
}
//  标签2中设置颜色的按钮的颜色被单击，自动跳出来设置文本颜色的对话框
void frmVideo::on_btnOSD2_clicked()
{
    QColor color = QColorDialog::getColor();//显示颜色对话框
    //isValid() 判断该颜色是否有效，有效返回值true，否则返回false
    if (color.isValid())
    {
        ui->playWidget->setOSD2Color(color);
        ui->btnOSD2->setText(color.name().toUpper());
        ui->btnOSD2->setStyleSheet(QString("background:#000000;color:%1;").arg(ui->btnOSD2->text()));
    }
}
//在基本中的功能   点击打开
void frmVideo::on_btnOpen_clicked()
{
    if (ui->btnOpen->text() == "打开")
    {
        //设置视频流地址
        ui->playWidget->setUrl(ui->cboxUrl->currentText().trimmed());
        //设置是否开启保存文件
        ui->playWidget->setSaveFile(ui->ckSaveFile->isChecked());
        //设置保存文件名称
        ui->playWidget->setFileName(ui->txtFileName->text().trimmed());
        //设置硬件加速
        ui->playWidget->setHardware(ui->cboxHardware->currentText());
        //设置tcp还是udp处理
        ui->playWidget->setTransport(ui->cboxTransport->currentText());
        //设置图片质量类型,速度优先+质量优先+均衡
        ui->playWidget->setImageFlag((FFmpegThread::ImageFlag)ui->cboxImageFlag->currentIndex());

        //设置超时时间 暂时没有留出界面接口所以挨个判断
        QString objName = this->objectName();
        if (objName == "video1")
        {
            ui->playWidget->setCheckTime(App::CheckTime1);
        } else if (objName == "video2")
        {
            ui->playWidget->setCheckTime(App::CheckTime2);
        } else if (objName == "video3")
        {
            ui->playWidget->setCheckTime(App::CheckTime3);
        } else if (objName == "video4")
        {
            ui->playWidget->setCheckTime(App::CheckTime4);
        }

        //设置深拷贝图像,在一些配置低的电脑上需要设置图像才不会断层
        //ui->playWidget->setCopyImage(true);

        //根据不同的选择设定不同的保存策略
        if (ui->ckSaveInterval->isChecked())
        {
            //设置按照时间段存储,单位秒钟,在当前程序目录下以类对象名称命名的文件夹
            ui->playWidget->setSavePath(AppPath + "/" + objName);
            ui->playWidget->setFileFlag(objName);
            ui->playWidget->setSaveInterval(1 * 30);
        } else if (ui->ckSaveTime->isChecked()) {
            //如果只需要存储一次文件则设置保存文件时间
            ui->playWidget->setSaveTime(QDateTime::currentDateTime().addSecs(30));
        }

        //有些地址比较特殊获取不到fps 需要手动设置
        if (ui->playWidget->getUrl().contains("ys7.com")) {
            ui->playWidget->setVideoFps(15);
        }

        //设置拉伸填充
        ui->playWidget->setFillImage(ui->cboxFillImage->currentIndex() == 0);
        //设置回调
        ui->playWidget->setCallback(ui->cboxCallback->currentIndex() == 1);

        ui->playWidget->open();
        ui->btnOpen->setText("关闭");
    }
    else
    {
        ui->playWidget->close();
        ui->btnOpen->setText("打开");
    }
}
//在基本中的功能   点击暂停
void frmVideo::on_btnPause_clicked()
{
    if (ui->btnPause->text() == "暂停")
    {
        ui->playWidget->pause();
        ui->btnPause->setText("继续");
    }
    else
    {
        ui->playWidget->next();//继续
        ui->btnPause->setText("暂停");
    }
}
//在基本中的功能   点击截图
void frmVideo::on_btnSnap_clicked()
{
    QString fileName = QString("%1/snap/%2.jpg").arg(AppPath).arg(QDateTime::currentDateTime().toString("yyyy-MM-dd-HH-mm-ss-zzz"));
    QImage img = ui->playWidget->getImage();//得到文件名
    if (!img.isNull())//判断文件名不为空
    {
        img.save(fileName, "jpg");//保存图片
    }
}
//在基本中的功能   点击截屏
void frmVideo::on_btnScreen_clicked()
{
    QString fileName = QString("%1/snap/%2.jpg").arg(AppPath).arg(QDateTime::currentDateTime().toString("yyyy-MM-dd-HH-mm-ss-zzz"));
    QPixmap pix = ui->playWidget->getPixmap();
    if (!pix.isNull())
    {
        pix.save(fileName, "jpg");//保存图片
    }
}
//在基本中的功能 点击打开地址一列的选择（单击后自动跳出来选择文件对话框）
void frmVideo::on_btnUrl_clicked()
{
    //跳出来打开文件的对话框
    QString fileName = QFileDialog::getOpenFileName(this, "打开文件", AppPath);
    if (!fileName.isEmpty())
    {
        ui->cboxUrl->insertItem(0, fileName);//插入
        ui->cboxUrl->setCurrentIndex(0);//设置默认选项
    }
}
//在基本中的功能 点击保存地址一列的选择（单击后自动跳出来保存文件对话框）
void frmVideo::on_btnSave_clicked()
{
    //跳出来保存文件的对话框
    QString fileName = QFileDialog::getSaveFileName(this, "保存文件", AppPath);
    if (!fileName.isEmpty())
    {
        if (!fileName.endsWith(".mp4"))
        {
            fileName = fileName + ".mp4";
        }
        ui->txtFileName->setText(fileName);//显示文件名
    }
}
//在基本中的功能 点击人脸的功能
void frmVideo::on_btnFace_clicked()
{
    //人脸框粗细
    ui->playWidget->setFaceBorder(3);
    //人脸边框颜色
    //ui->playWidget->setFaceColor(Qt::green);

    //如果有人脸则清空人脸 这里为了演示
    QList<QRect> faceRects;
    if (ui->playWidget->getFaceRects().count() == 0)
    {
        int size = 50;
        for (int i = 0; i < 3; i++)
        {
            int x = qrand() % 300;
            int y = qrand() % 200;
            faceRects << QRect(x, y, size, size);
        }
        ui->playWidget->setFaceRects(faceRects);
    } else {
        ui->playWidget->setFaceRects(faceRects);
    }
}

void frmVideo::on_ckSaveInterval_stateChanged(int arg1)
{
    if (arg1 != 0) {
        ui->ckSaveFile->setChecked(true);
        ui->ckSaveTime->setChecked(false);
        ui->ckSaveHand->setChecked(false);
    }
}

void frmVideo::on_ckSaveTime_stateChanged(int arg1)
{
    if (arg1 != 0) {
        ui->ckSaveFile->setChecked(true);
        ui->ckSaveInterval->setChecked(false);
        ui->ckSaveHand->setChecked(false);
    }
}

void frmVideo::on_ckSaveHand_stateChanged(int arg1)
{
    if (arg1 != 0) {
        ui->ckSaveFile->setChecked(true);
        ui->ckSaveInterval->setChecked(false);
        ui->ckSaveTime->setChecked(false);
    }

    if (arg1 != 0) {
        //每次设置不一样的文件名称,不然一样的话会覆盖
        QString savePath = QString("%1/%2").arg(AppPath).arg(objectName());
        QString fileName = QString("%1/%2.mp4").arg(savePath).arg(STRDATETIMEMS);
        ui->playWidget->setSaveFile(true);
        ui->playWidget->setSavePath(savePath);
        ui->playWidget->setFileName(fileName);
        ui->playWidget->startSave();
    } else {
        ui->playWidget->stopSave();
    }
}
