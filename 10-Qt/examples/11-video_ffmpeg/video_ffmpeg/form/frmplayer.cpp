﻿#include "frmplayer.h"
#include "ui_frmplayer.h"
#include "head.h"
#include "ffmpeghelper.h"

frmPlayer::frmPlayer(QWidget *parent) : QWidget(parent), ui(new Ui::frmPlayer)
{
    ui->setupUi(this);
    this->initForm();//初始化窗体
    this->initConfig();//初始化配置文件
}

frmPlayer::~frmPlayer()
{
    delete ui;
}
//全屏设置信号处理函数
void frmPlayer::fullScreen()
{
    full = !full;
    emit fullScreen(full);// emit 调用该信号处理函数

    if (full)
    {
        ui->playWidget->setBorderWidth(0);//设置边框宽度
        ui->frameRight->setVisible(false);//框架右侧不显示
        ui->frameBottom->setVisible(false);//框架底部不显示
        this->layout()->setMargin(0);//控件与窗体之间的左右边距
        this->layout()->setSpacing(0);//控件与窗体之间的上下间距
    } else
    {
        ui->playWidget->setBorderWidth(3);
        ui->frameRight->setVisible(true);
        ui->frameBottom->setVisible(true);
        this->layout()->setMargin(6);
        this->layout()->setSpacing(6);
    }
}
//切换视屏播放模式
void frmPlayer::keyReleaseEvent(QKeyEvent *key)
{
    //当按下esc键并且当前处于全屏模式则切换到正常模式
    if (key->key() == Qt::Key_Escape && full)
    {
        fullScreen();
    }
}
//判断鼠标事件
bool frmPlayer::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == ui->playWidget)
    {
        if (event->type() == QEvent::MouseButtonDblClick)//双击全屏
        {
            fullScreen();//全屏设置信号
        }
    }

    return QWidget::eventFilter(watched, event);
}
//初始化窗体
void frmPlayer::initForm()
{
    full = false;
    ui->playWidget->installEventFilter(this);//注册监视对象。

    QStringList urls;
    ui->cboxUrl->addItems(App::getUrls(urls));

    //以下方法可以设置背景图片或者背景文字
    ui->playWidget->setBgText("视频监控");
    ui->playWidget->setBgImage(QImage(":/image/bg_novideo.png"));

    //以下方法可以设置OSD为图片
    ui->playWidget->setOSD1Image(QImage(":/image/bg_novideo.png"));
    ui->playWidget->setOSD1Format(FFmpegWidget::OSDFormat_Image);
    //ui->playWidget->setOSD1Visible(true);

    //设置边框宽度和颜色
    ui->playWidget->setBorderWidth(3);
    ui->playWidget->setBorderColor(QColor(0, 0, 0));

    //设置是否播放声音 默认真
    //ui->playWidget->setPlayAudio(false);

    connect(ui->playWidget, SIGNAL(receivePlayStart()), this, SLOT(receivePlayStart()));
    connect(ui->playWidget, SIGNAL(receivePlayError()), this, SLOT(receivePlayError()));
    connect(ui->playWidget, SIGNAL(receivePlayFinsh()), this, SLOT(receivePlayFinsh()));
    connect(ui->playWidget, SIGNAL(fileLengthReceive(qint64)), this, SLOT(fileLengthReceive(qint64)));
    connect(ui->playWidget, SIGNAL(filePositionReceive(qint64)), this, SLOT(filePositionReceive(qint64)));
    connect(ui->playWidget, SIGNAL(fileVolumeReceive(int, bool)), this, SLOT(fileVolumeReceive(int, bool)));

    //安卓屏幕比较小尽量空出位置
#ifdef Q_OS_ANDROID
    ui->btnVersion->setVisible(false);
    ui->btnAbout->setVisible(false);
    ui->verticalSpacer->changeSize(0, 0, QSizePolicy::Ignored, QSizePolicy::Ignored);

    QList<QPushButton *> btns = ui->frame->findChildren<QPushButton *>();
    foreach (QPushButton *btn, btns) {
        btn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    }
#endif
}
//初始化配置文件
void frmPlayer::initConfig()
{
    ui->playWidget->setCheckTime(30 * 1000);
    ui->playWidget->setHardware(App::Hardware5);

    ui->cboxUrl->lineEdit()->setText(App::RtspAddr5);
    connect(ui->cboxUrl->lineEdit(), SIGNAL(textChanged(QString)), this, SLOT(saveConfig()));

    ui->ckCallback->setChecked(App::Callback5);
    connect(ui->ckCallback, SIGNAL(stateChanged(int)), this, SLOT(saveConfig()));

    ui->ckFillImage->setChecked(App::FillImage5);
    connect(ui->ckFillImage, SIGNAL(stateChanged(int)), this, SLOT(saveConfig()));
}
//保存配置文件
void frmPlayer::saveConfig()
{
    App::RtspAddr5 = ui->cboxUrl->lineEdit()->text();
    App::Callback5 = ui->ckCallback->isChecked();
    App::FillImage5 = ui->ckFillImage->isChecked();
    App::writeConfig();
}
//视屏播放成功
void frmPlayer::receivePlayStart()
{
    qDebug() << TIMEMS << "播放开始";
}
//视屏播放出错
void frmPlayer::receivePlayError()
{
    qDebug() << TIMEMS << "播放出错";
}
//视屏播放结束
void frmPlayer::receivePlayFinsh()
{
    ui->labTimePlay->setText("00:00");
    ui->labTimeAll->setText("00:00");
    ui->sliderPosition->setValue(0);//设置时长滑块的当前值
    qDebug() << TIMEMS << "播放结束";
}
//总时长函数
void frmPlayer::fileLengthReceive(qint64 length)
{
    //设置进度条最大进度以及总时长,自动在总时长前补零

    //sliderPosition 控件（slider 滑块），
    ui->sliderPosition->setMaximum(length);//滑块的最大值，setMinimum设置滑块最小值
    ui->sliderPosition->setValue(0);//设置时长滑块的当前值

    length = length / 1000;
    //length 转成min 和 sec（秒）
    QString min = QString("%1").arg(length / 60, 2, 10, QChar('0'));
    QString sec = QString("%2").arg(length % 60, 2, 10, QChar('0'));
    //设置滑块右边的标签的（用来显示播放视屏的分秒）
    ui->labTimeAll->setText(QString("%1:%2").arg(min).arg(sec));
}
//当前播放时长函数
void frmPlayer::filePositionReceive(qint64 position)
{
    //设置当前进度及已播放时长,自动在已播放时长前补零
    ui->sliderPosition->setValue(position);//设置时长滑块当前值

    position = position / 1000;
    //position 转成min 和 sec
    QString min = QString("%1").arg(position / 60, 2, 10, QChar('0'));
    QString sec = QString("%2").arg(position % 60, 2, 10, QChar('0'));
    //设置滑块左边的标签的（用来显示当前播放的时间）
    ui->labTimePlay->setText(QString("%1:%2").arg(min).arg(sec));
}
//音量大小
void frmPlayer::fileVolumeReceive(int volume, bool muted)
{
    ui->sliderVolume->setValue(volume);//设置音量滑块的当前值
    //ckMuted 静音
    ui->ckMuted->setChecked(muted);
}
//选择的功能
void frmPlayer::on_btnSelect_clicked()
{
    QString file = QFileDialog::getOpenFileName(this);//打开文件对话框
    if (!file.isEmpty())//判断文件名是否为空
    {
        //在下拉列表（cboxUrl）中显示文件名（视屏链接）
        ui->cboxUrl->lineEdit()->setText(file);

        on_btnStop_clicked();//停止按钮
        on_btnPlay_clicked();//继续播放按钮
    }
}
//播放功能
void frmPlayer::on_btnPlay_clicked()
{
    if (!ui->playWidget->getIsPlaying())
    {
        ui->playWidget->setUrl(ui->cboxUrl->currentText());
        ui->playWidget->setCallback(ui->ckCallback->isChecked());
        ui->playWidget->setFillImage(ui->ckFillImage->isChecked());
        ui->playWidget->setPlayRepeat(ui->ckPlayRepeat->isChecked());
        ui->playWidget->open();
    }
}
//停止功能
void frmPlayer::on_btnStop_clicked()
{
    ui->playWidget->close();
}
//暂停功能
void frmPlayer::on_btnPause_clicked()
{
    ui->playWidget->pause();
}
//继续功能
void frmPlayer::on_btnNext_clicked()
{
    ui->playWidget->next();
}
//版本功能
void frmPlayer::on_btnVersion_clicked()
{
    QMessageBox::information(this, "提示", QString("当前内核版本: %1").arg(getVersion()));
}
//关于功能
void frmPlayer::on_btnAbout_clicked()
{
    qApp->aboutQt();
}
//静音功能
void frmPlayer::on_ckMuted_clicked()
{
    //isChecked() 当静音被勾中时，返回值为true
    //setMuted() 是否将该音效设置为静音，true为静音 false将以指定音量进行
    ui->playWidget->setMuted(ui->ckMuted->isChecked());
}
//回调功能
void frmPlayer::on_ckCallback_clicked()
{
    ui->playWidget->setCallback(ui->ckCallback->isChecked());
}
//视屏当前时长函数
void frmPlayer::on_sliderPosition_clicked()
{
    //setPosition（） 视屏会跟着进度条动
    ui->playWidget->setPosition(ui->sliderPosition->value());
}
//音量大小函数
void frmPlayer::on_sliderVolume_clicked()
{
    //sliderVolume 音量大小滑块   value（）获取当前值
    ui->playWidget->setVolume(ui->sliderVolume->value());//设置音量滑块的当前值
}
//当前时长的滑块移动函数
void frmPlayer::on_sliderPosition_sliderMoved(int position)
{
    //setPosition（） 视屏会跟着进度条动
    ui->playWidget->setPosition(position);
}
//声音大小的移动函数
void frmPlayer::on_sliderVolume_sliderMoved(int position)
{
    ui->playWidget->setVolume(position);//设置音量滑块的当前值
}
