﻿ #include "frmview.h"
#include "ui_frmview.h"
#include "head.h"
#include "ffmpegwidget.h"
#include "videoffmpeg.h"
//视屏监控画面（功能）

frmView::frmView(QWidget *parent) : QWidget(parent), ui(new Ui::frmView)
{
    ui->setupUi(this);
    this->initForm();//初始化窗体
    this->initMenu();
    this->show_video_all();//显示所有视屏
}

frmView::~frmView()
{
    delete ui;
}
//切换视屏播放模式
void frmView::keyReleaseEvent(QKeyEvent *key)
{
    //当按下esc键并且当前处于全屏模式则切换到正常模式
    if (key->key() == Qt::Key_Escape && actionFull->text() == "切换正常模式")
    {
        actionFull->trigger();
    }
}
//显示事件
void frmView::showEvent(QShowEvent *)
{
    static bool isLoad = false;
    if (!isLoad)
    {
        isLoad = true;
        //play_video_all()播放所有视屏
        //1秒后，自动结束这个程序
        QTimer::singleShot(1000, this, SLOT(play_video_all()));
    }
}
//判断鼠标的事件
bool frmView::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonDblClick)//判断鼠标事件，是否双击
    {
        FFmpegWidget *widget = (FFmpegWidget *) watched;
        if (!videoMax)//双击显示最大化
        {
            videoMax = true;
            hide_video_all();//隐藏所有视屏
            //gridLayout 网格布局
            ui->gridLayout->addWidget(widget, 0, 0);//将窗体定位在0行0列点
            widget->setVisible(true);//使窗体可见
        } else
        {
            videoMax = false;
            show_video_all();//显示所有视屏
        }
        //eg：焦点，浏览百度网页时，不需要单击鼠标就可以直接输入文字等信息
        //判断一个字窗体中是否有焦点事件，返回bool值
        widget->setFocus();
    } else if (event->type() == QEvent::MouseButtonPress)//单击事件
    {
        if (qApp->mouseButtons() == Qt::RightButton)//右击
        {
            videoMenu->exec(QCursor::pos());
        }
    }

    return QWidget::eventFilter(watched, event);
}
//初始化窗体
void frmView::initForm()
{
    ui->frame->setStyleSheet("border:2px solid #000000;");

    videoMax = false;
    videoCount = 64;

    QStringList urls;    //字符串列表对象
    for (int i = 0; i < videoCount; i++) {
        FFmpegWidget *widget = new FFmpegWidget;
        widget->installEventFilter(this);
        widget->setBorderWidth(3);//设置边框宽度
        widget->setBgText(QString("通道 %1").arg(i + 1));//设置通道
        widget->setBgImage(QImage(":/image/bg_novideo.png"));//设置背景图片
        widget->setObjectName(QString("video%1").arg(i + 1));

        //widget->setCopyImage(true);
        widget->setFillImage(App::FillImage);
        widget->setCallback(App::Callback);
        widget->setHardware(App::Hardware);//获取硬件解码的信息
        widget->setTransport(App::Transport);
        widget->setImageFlag((FFmpegThread::ImageFlag)App::ImageFlag);
        widget->setCheckTime(App::CheckTime);

        //设置是否循环播放
        //widget->setPlayRepeat(true);
        //设置是否播放声音
        widget->setPlayAudio(false);

        urls.append(""); //在字符串列表中添加一个空字符串
        widgets.append(widget);//列表的尾部添加元素
    }
    App::getUrls(urls);//获取播放视屏链接
#ifdef Q_OS_ANDROID
    //安卓上写死四通道地址测试用
    App::VideoType = "1_4";
    urls[0] = "http://hls01open.ys7.com/openlive/f01018a141094b7fa138b9d0b856507b.hd.m3u8";
    urls[1] = "http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear2/prog_index.m3u8";
    urls[2] = "http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";
    urls[3] = "http://vts.simba-cn.com:280/gb28181/21100000001320000002.m3u8";
#endif
    VideoFFmpeg::Instance()->setUrls(urls);
    VideoFFmpeg::Instance()->setWidgets(widgets);
    VideoFFmpeg::Instance()->setVideoCount(videoCount);

    //还可以设置超时时间 打开间隔 重连间隔
    VideoFFmpeg::Instance()->setTimeout(10);
    VideoFFmpeg::Instance()->setOpenInterval(200);
    VideoFFmpeg::Instance()->setCheckInterval(5);

#ifdef Q_OS_ANDROID
    VideoFFmpeg::Instance()->setOpenInterval(2000);
#endif
}
//初始化鼠标
void frmView::initMenu()
{
    videoMenu = new QMenu(this);

    actionFull = new QAction("切换全屏模式", videoMenu);
    connect(actionFull, SIGNAL(triggered(bool)), this, SLOT(full()));
    actionPoll = new QAction("启动轮询视频", videoMenu);
    connect(actionPoll, SIGNAL(triggered(bool)), this, SLOT(poll()));

    videoMenu->addAction(actionFull);
    videoMenu->addAction(actionPoll);
    videoMenu->addSeparator();

    videoMenu->addAction("截图当前视频", this, SLOT(snapshot_video_one()));
    videoMenu->addAction("截图所有视频", this, SLOT(snapshot_video_all()));
    videoMenu->addSeparator();

    QMenu *menu4 = videoMenu->addMenu("切换到4画面");
    menu4->addAction("通道1-通道4", this, SLOT(show_video_4()));
    menu4->addAction("通道5-通道8", this, SLOT(show_video_4()));
    menu4->addAction("通道9-通道12", this, SLOT(show_video_4()));
    menu4->addAction("通道13-通道16", this, SLOT(show_video_4()));

    QMenu *menu6 = videoMenu->addMenu("切换到6画面");
    menu6->addAction("通道1-通道6", this, SLOT(show_video_6()));
    menu6->addAction("通道6-通道11", this, SLOT(show_video_6()));
    menu6->addAction("通道11-通道16", this, SLOT(show_video_6()));

    QMenu *menu8 = videoMenu->addMenu("切换到8画面");
    menu8->addAction("通道1-通道8", this, SLOT(show_video_8()));
    menu8->addAction("通道9-通道16", this, SLOT(show_video_8()));

    QMenu *menu9 = videoMenu->addMenu("切换到9画面");
    menu9->addAction("通道1-通道9", this, SLOT(show_video_9()));
    menu9->addAction("通道8-通道16", this, SLOT(show_video_9()));

    QMenu *menu13 = videoMenu->addMenu("切换到13画面");
    menu13->addAction("通道1-通道13", this, SLOT(show_video_13()));
    menu13->addAction("通道4-通道16", this, SLOT(show_video_13()));

    videoMenu->addAction("切换到16画面", this, SLOT(show_video_16()));
    videoMenu->addAction("切换到25画面", this, SLOT(show_video_25()));
    videoMenu->addAction("切换到36画面", this, SLOT(show_video_36()));
    videoMenu->addAction("切换到64画面", this, SLOT(show_video_64()));
}
//全屏模式
void frmView::full()
{
    if (actionFull->text() == "切换全屏模式")
    {
        emit fullScreen(true);//emit 调用该信号函数
        actionFull->setText("切换正常模式");
        this->layout()->setMargin(0);
    } else
    {
        emit fullScreen(false);
        actionFull->setText("切换全屏模式");
        this->layout()->setMargin(6);
    }
}
//poll 轮询
void frmView::poll()
{
    if (actionPoll->text() == "启动轮询视频")
    {
        actionPoll->setText("停止轮询视频");//设置显示到界面
    } else
    {
        actionPoll->setText("启动轮询视频");//设置显示到界面
    }
}
//播放所有视屏
void frmView::play_video_all()
{
    VideoFFmpeg::Instance()->start();
}
//截图单个视屏
void frmView::snapshot_video_one()
{
    for (int i = 0; i < videoCount; i++)
    {
        if (widgets.at(i)->hasFocus())
        {
            QString fileName = QString("%1/snap/Ch%2_%3.jpg").arg(AppPath).arg(i + 1).arg(STRDATETIME);
            VideoFFmpeg::Instance()->snap(i, fileName);
            break;
        }
    }
}
//截图所有视屏
void frmView::snapshot_video_all()
{
    for (int i = 0; i < videoCount; i++) {
        QString fileName = QString("%1/snap/Ch%2_%3.jpg").arg(AppPath).arg(i + 1).arg(STRDATETIME);
        VideoFFmpeg::Instance()->snap(i, fileName);
    }
}
//显示所有视屏
void frmView::show_video_all()
{
    QString videoType = App::VideoType;
    if (videoType == "1_4") {
        change_video_4(0);
    } else if (videoType == "5_8") {
        change_video_4(4);
    } else if (videoType == "9_12") {
        change_video_4(8);
    } else if (videoType == "13_16") {
        change_video_4(12);
    } else if (videoType == "1_6") {
        change_video_6(0);
    } else if (videoType == "6_11") {
        change_video_6(5);
    } else if (videoType == "11_16") {
        change_video_6(10);
    } else if (videoType == "1_8") {
        change_video_8(0);
    } else if (videoType == "9_16") {
        change_video_8(8);
    } else if (videoType == "1_9") {
        change_video_9(0);
    } else if (videoType == "8_16") {
        change_video_9(7);
    } else if (videoType == "1_13") {
        change_video_13(0);
    } else if (videoType == "4_16") {
        change_video_13(3);
    } else if (videoType == "1_16") {
        change_video_16(0);
    } else if (videoType == "1_25") {
        change_video_25(0);
    } else if (videoType == "1_36") {
        change_video_36(0);
    } else if (videoType == "1_64") {
        change_video_64(0);
    }
}

void frmView::show_video_4()
{
    videoMax = false;
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道4") {
        index = 0;
        videoType = "1_4";
    } else if (name == "通道5-通道8") {
        index = 4;
        videoType = "5_8";
    } else if (name == "通道9-通道12") {
        index = 8;
        videoType = "9_12";
    } else if (name == "通道13-通道16") {
        index = 12;
        videoType = "13_16";
    }

    if (App::VideoType != videoType)
    {
        App::VideoType = videoType;
        App::writeConfig();//写入配置文件
        change_video_4(index);
    }
}

void frmView::show_video_6()
{
    videoMax = false;
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道6") {
        index = 0;
        videoType = "1_6";
    } else if (name == "通道6-通道11") {
        index = 5;
        videoType = "6_11";
    } else if (name == "通道11-通道16") {
        index = 10;
        videoType = "11_16";
    }

    if (App::VideoType != videoType) {
        App::VideoType = videoType;
        App::writeConfig();
        change_video_6(index);
    }
}

void frmView::show_video_8()
{
    videoMax = false;
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道8") {
        index = 0;
        videoType = "1_8";
    } else if (name == "通道9-通道16") {
        index = 8;
        videoType = "9_16";
    }

    if (App::VideoType != videoType) {
        App::VideoType = videoType;
        App::writeConfig();
        change_video_8(index);
    }
}

void frmView::show_video_9()
{
    videoMax = false;
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道9") {
        index = 0;
        videoType = "1_9";
    } else if (name == "通道8-通道16") {
        index = 7;
        videoType = "8_16";
    }

    if (App::VideoType != videoType) {
        App::VideoType = videoType;
        App::writeConfig();
        change_video_9(index);
    }
}

void frmView::show_video_13()
{
    QString videoType;
    int index = 0;

    QAction *action = (QAction *)sender();
    QString name = action->text();

    if (name == "通道1-通道13") {
        index = 0;
        videoType = "1_13";
    } else if (name == "通道4-通道16") {
        index = 3;
        videoType = "4_16";
    }

    if (App::VideoType != videoType)
    {
        App::VideoType = videoType;
        App::writeConfig();
        change_video_13(index);
    }
}

void frmView::show_video_16()
{
    videoMax = false;
    QString videoType;
    int index = 0;
    videoType = "1_16";

    if (App::VideoType != videoType) {
        App::VideoType = videoType;
        App::writeConfig();
        change_video_16(index);
    }
}

void frmView::show_video_25()
{
    videoMax = false;
    QString videoType;
    int index = 0;
    videoType = "1_25";

    if (App::VideoType != videoType) {
        App::VideoType = videoType;
        App::writeConfig();
        change_video_25(index);
    }
}

void frmView::show_video_36()
{
    videoMax = false;
    QString videoType;
    int index = 0;
    videoType = "1_36";

    if (App::VideoType != videoType) {
        App::VideoType = videoType;
        App::writeConfig();
        change_video_36(index);
    }
}

void frmView::show_video_64()
{
    videoMax = false;
    QString videoType;
    int index = 0;
    videoType = "1_64";

    if (App::VideoType != videoType) {
        App::VideoType = videoType;
        App::writeConfig();
        change_video_64(index);
    }
}
//隐藏所有视屏
void frmView::hide_video_all()
{
    for (int i = 0; i < videoCount; i++)
    {
        ui->gridLayout->removeWidget(widgets.at(i));
        widgets.at(i)->setVisible(false);//使窗体不可见
    }
}
//改变视屏通道
void frmView::change_video(int index, int flag)
{
    int count = 0;
    int row = 0;//行
    int column = 0;//列

    for (int i = 0; i < videoCount; i++)
    {
        if (i >= index)
        {
            ui->gridLayout->addWidget(widgets.at(i), row, column);
            widgets.at(i)->setVisible(true);//设置为窗体可视

            count++;
            column++;
            if (column == flag)
            {
                row++;
                column = 0;
            }
        }

        if (count == (flag * flag))
        {
            break;
        }
    }
}

void frmView::change_video_4(int index)
{
    hide_video_all();
    change_video(index, 2);
}

void frmView::change_video_6(int index)
{
    hide_video_all();
    if (index == 0)
    {
        ui->gridLayout->addWidget(widgets.at(0), 0, 0, 2, 2);
        ui->gridLayout->addWidget(widgets.at(1), 0, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(2), 1, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(3), 2, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(4), 2, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(5), 2, 0, 1, 1);

        for (int i = 0; i < 6; i++)
        {
            widgets.at(i)->setVisible(true);
        }
    } else if (index == 5) {
        ui->gridLayout->addWidget(widgets.at(5), 0, 0, 2, 2);
        ui->gridLayout->addWidget(widgets.at(6), 0, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(7), 1, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(8), 2, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(9), 2, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(10), 2, 0, 1, 1);

        for (int i = 5; i < 11; i++) {
            widgets.at(i)->setVisible(true);
        }
    } else if (index == 10) {
        ui->gridLayout->addWidget(widgets.at(10), 0, 0, 2, 2);
        ui->gridLayout->addWidget(widgets.at(11), 0, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(12), 1, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(13), 2, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(14), 2, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(15), 2, 0, 1, 1);

        for (int i = 10; i < 16; i++) {
            widgets.at(i)->setVisible(true);
        }
    }
}

void frmView::change_video_8(int index)
{
    hide_video_all();
    if (index == 0) {
        ui->gridLayout->addWidget(widgets.at(0), 0, 0, 3, 3);
        ui->gridLayout->addWidget(widgets.at(1), 0, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(2), 1, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(3), 2, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(4), 3, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(5), 3, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(6), 3, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(7), 3, 0, 1, 1);

        for (int i = 0; i < 8; i++) {
            widgets.at(i)->setVisible(true);
        }
    } else if (index == 8) {
        ui->gridLayout->addWidget(widgets.at(8), 0, 0, 3, 3);
        ui->gridLayout->addWidget(widgets.at(9), 0, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(10), 1, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(11), 2, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(12), 3, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(13), 3, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(14), 3, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(15), 3, 0, 1, 1);

        for (int i = 8; i < 16; i++) {
            widgets.at(i)->setVisible(true);
        }
    }
}

void frmView::change_video_9(int index)
{
    hide_video_all();
    change_video(index, 3);
}

void frmView::change_video_13(int index)
{
    hide_video_all();
    if (index == 0) {
        ui->gridLayout->addWidget(widgets.at(0), 0, 0, 1, 1);
        ui->gridLayout->addWidget(widgets.at(1), 0, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(2), 0, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(3), 0, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(4), 1, 0, 1, 1);
        ui->gridLayout->addWidget(widgets.at(5), 2, 0, 1, 1);
        ui->gridLayout->addWidget(widgets.at(6), 1, 1, 2, 2);
        ui->gridLayout->addWidget(widgets.at(7), 1, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(8), 2, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(9), 3, 0, 1, 1);
        ui->gridLayout->addWidget(widgets.at(10), 3, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(11), 3, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(12), 3, 3, 1, 1);

        for (int i = 0; i < 13; i++) {
            widgets.at(i)->setVisible(true);
        }
    } else if (index == 3) {
        ui->gridLayout->addWidget(widgets.at(3), 0, 0, 1, 1);
        ui->gridLayout->addWidget(widgets.at(4), 0, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(5), 0, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(6), 0, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(7), 1, 0, 1, 1);
        ui->gridLayout->addWidget(widgets.at(8), 2, 0, 1, 1);
        ui->gridLayout->addWidget(widgets.at(9), 1, 1, 2, 2);
        ui->gridLayout->addWidget(widgets.at(10), 1, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(11), 2, 3, 1, 1);
        ui->gridLayout->addWidget(widgets.at(12), 3, 0, 1, 1);
        ui->gridLayout->addWidget(widgets.at(13), 3, 1, 1, 1);
        ui->gridLayout->addWidget(widgets.at(14), 3, 2, 1, 1);
        ui->gridLayout->addWidget(widgets.at(15), 3, 3, 1, 1);

        for (int i = 3; i < 16; i++) {
            widgets.at(i)->setVisible(true);
        }
    }
}

void frmView::change_video_16(int index)
{
    hide_video_all();
    change_video(index, 4);
}

void frmView::change_video_25(int index)
{
    hide_video_all();
    change_video(index, 5);
}

void frmView::change_video_36(int index)
{
    hide_video_all();
    change_video(index, 6);
}

void frmView::change_video_64(int index)
{
    hide_video_all();
    change_video(index, 8);
}
