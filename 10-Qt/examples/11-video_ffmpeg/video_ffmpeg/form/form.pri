FORMS += \
    $$PWD/frmmain.ui \
    $$PWD/frmmulti.ui \
    $$PWD/frmplayer.ui \
    $$PWD/frmtab.ui \
    $$PWD/frmtool.ui \
    $$PWD/frmvideo.ui \
    $$PWD/frmview.ui

HEADERS += \
    $$PWD/frmmain.h \
    $$PWD/frmmulti.h \
    $$PWD/frmplayer.h \
    $$PWD/frmtab.h \
    $$PWD/frmtool.h \
    $$PWD/frmvideo.h \
    $$PWD/frmview.h

SOURCES += \
    $$PWD/frmmain.cpp \
    $$PWD/frmmulti.cpp \
    $$PWD/frmplayer.cpp \
    $$PWD/frmtab.cpp \
    $$PWD/frmtool.cpp \
    $$PWD/frmvideo.cpp \
    $$PWD/frmview.cpp
