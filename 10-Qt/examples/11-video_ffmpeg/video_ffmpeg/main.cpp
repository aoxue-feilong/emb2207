﻿#include "head.h"
#include "frmtab.h"
#include "frmmain.h"
#include "frmview.h"
#include "frmmulti.h"
#include "frmplayer.h"
#include "frmvideo.h"
#include "frmtool.h"



//动态设置权限
bool checkPermission(const QString &permission)
{
#ifdef Q_OS_ANDROID
#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
    QtAndroid::PermissionResult result = QtAndroid::checkPermission(permission);
    if (result == QtAndroid::PermissionResult::Denied) {
        QtAndroid::requestPermissionsSync(QStringList() << permission);
        result = QtAndroid::checkPermission(permission);
        if (result == QtAndroid::PermissionResult::Denied)
        {
            return false;
        }
    }
#endif
#endif
    return true;
}

void initStyle()
{
    //复选框单选框滑块等指示器大小
    QStringList list;
    int rbtnWidth = 20;
    int ckWidth = 18;
    list.append(QString("QRadioButton::indicator{width:%1px;height:%1px;}").arg(rbtnWidth));
    list.append(QString("QCheckBox::indicator,QGroupBox::indicator,QTreeWidget::indicator,QListWidget::indicator{width:%1px;height:%1px;}").arg(ckWidth));

    QString normalColor = "#E8EDF2";
    QString grooveColor = "#1ABC9C";
    QString handleColor = "#1ABC9C";
    int sliderHeight = 12;
    int sliderRadius = sliderHeight / 2;
    int handleWidth = (sliderHeight * 3) / 2 + (sliderHeight / 5);
    int handleRadius = handleWidth / 2;
    int handleOffset = handleRadius / 2;

    list.append(QString("QSlider::horizontal{min-height:%1px;}").arg(sliderHeight * 2));
    list.append(QString("QSlider::groove:horizontal{background:%1;height:%2px;border-radius:%3px;}")
                .arg(normalColor).arg(sliderHeight).arg(sliderRadius));
    list.append(QString("QSlider::add-page:horizontal{background:%1;height:%2px;border-radius:%3px;}")
                .arg(normalColor).arg(sliderHeight).arg(sliderRadius));
    list.append(QString("QSlider::sub-page:horizontal{background:%1;height:%2px;border-radius:%3px;}")
                .arg(grooveColor).arg(sliderHeight).arg(sliderRadius));
    list.append(QString("QSlider::handle:horizontal{width:%2px;margin-top:-%3px;margin-bottom:-%3px;border-radius:%4px;"
                        "background:qradialgradient(spread:pad,cx:0.5,cy:0.5,radius:0.5,fx:0.5,fy:0.5,stop:0.6 #FFFFFF,stop:0.8 %1);}")
                .arg(handleColor).arg(handleWidth).arg(handleOffset).arg(handleRadius));

    //偏移一个像素
    handleWidth = handleWidth + 1;
    list.append(QString("QSlider::vertical{min-width:%1px;}").arg(sliderHeight * 2));
    list.append(QString("QSlider::groove:vertical{background:%1;width:%2px;border-radius:%3px;}")
                .arg(normalColor).arg(sliderHeight).arg(sliderRadius));
    list.append(QString("QSlider::add-page:vertical{background:%1;width:%2px;border-radius:%3px;}")
                .arg(grooveColor).arg(sliderHeight).arg(sliderRadius));
    list.append(QString("QSlider::sub-page:vertical{background:%1;width:%2px;border-radius:%3px;}")
                .arg(normalColor).arg(sliderHeight).arg(sliderRadius));
    list.append(QString("QSlider::handle:vertical{height:%2px;margin-left:-%3px;margin-right:-%3px;border-radius:%4px;"
                        "background:qradialgradient(spread:pad,cx:0.5,cy:0.5,radius:0.5,fx:0.5,fy:0.5,stop:0.6 #FFFFFF,stop:0.8 %1);}")
                .arg(handleColor).arg(handleWidth).arg(handleOffset).arg(handleRadius));

    qApp->setStyleSheet(list.join(""));
}

int main(int argc, char *argv[])
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
    //setAttribute：如果on为真，则设置attribute属性;否则清除该属性。
    //AA_EnableHighDpiScaling   高亮属性
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
#if (QT_VERSION > QT_VERSION_CHECK(5,4,0))//如果QT版本大于5.4
    //设置opengl模式 AA_UseDesktopOpenGL AA_UseSoftwareOpenGL AA_UseOpenGLES
    //QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
#endif
    QApplication a(argc, argv);

    App::setFont();//设置字体
    App::setCode();//设置编码
    App::ConfigFile = AppPath + "/video_ffmpeg.ini";//工程配置文件
    App::readConfig();//设置居中


    //frmTab w;
    //frmView w; // 单独把监控拿出来使用
    //frmPlayer w; // 单独把播放器拿出来使用
    //frmMulti w ;// 单独把多端复用拿出来使用
    //frmMain w ;// 单独把4个视频流器拿出来使用
    frmVideo w ;// 单独把一个视频流拿出来使用
    //frmTool  w ;// 单独把一个视频流拿出来使用

#ifdef Q_OS_ANDROID //ifdef  判断宏是否被定义，若定义，执行其后的代码
    //请求权限
    checkPermission("android.permission.READ_EXTERNAL_STORAGE");
    checkPermission("android.permission.WRITE_EXTERNAL_STORAGE");

    QString strDir = AppPath + "/snap";
    QDir dir(strDir);
    if (!dir.exists()) {
        dir.mkpath(strDir);
    }

    initStyle();
    w.showMaximized();
#else
    w.resize(1050, 700);
    w.setWindowTitle(QString("qt+ffmpeg 示例 %1 %2").arg(App::Version).arg(App::TitleFlag));
    App::setFormInCenter(&w);
    w.show();
#endif

    return a.exec();
}
