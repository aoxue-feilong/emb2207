#-------------------------------------------------
#
# Project created by QtCreator 2016-09-29T09:37:26
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia
android {QT += androidextras}

TARGET      = video_ffmpeg
TEMPLATE    = app
MOC_DIR     = temp/moc
RCC_DIR     = temp/rcc
UI_DIR      = temp/ui
OBJECTS_DIR = temp/obj
!android {
DESTDIR     = $$PWD/../bin
}

SOURCES     += main.cpp
HEADERS     += head.h
RESOURCES   += main.qrc
CONFIG      += warn_off

INCLUDEPATH += $$PWD
INCLUDEPATH += $$PWD/form
INCLUDEPATH += $$PWD/../core_common
INCLUDEPATH += $$PWD/../core_ffmpeg
INCLUDEPATH += $$PWD/../core_opengl

include ($$PWD/h3.pri)
include ($$PWD/form/form.pri)
include ($$PWD/../core_common/common.pri)
include ($$PWD/../core_ffmpeg/ffmpeg.pri)
include ($$PWD/../core_opengl/opengl.pri)

android {
DISTFILES += android/AndroidManifest.xml
DISTFILES += android/build.gradle
DISTFILES += android/res/values/libs.xml
ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
}

DISTFILES += \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat
