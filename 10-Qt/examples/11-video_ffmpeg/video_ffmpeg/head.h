﻿#include <QtCore>
#include <QtGui>
#include <QtNetwork>
#if (QT_VERSION > QT_VERSION_CHECK(5,0,0))
#include <QtWidgets>
#endif

#include "app.h"
#pragma execution_character_set("utf-8")

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras>
//sd卡存储的路径是 /sdcard/Android
//自身存储的路径是 /storage/emulated/0
#define AppPath QString("/storage/emulated/0/%1").arg("video_ffmpeg")
#else
#define AppPath qApp->applicationDirPath()
#endif
