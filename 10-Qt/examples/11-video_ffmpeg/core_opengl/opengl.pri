#不需要opengl的话屏蔽下面这行即可
DEFINES += opengl

#判断当前qt版本号
QT_VERSION = $$[QT_VERSION]
QT_VERSION = $$split(QT_VERSION, ".")
QT_VER_MAJ = $$member(QT_VERSION, 0)
QT_VER_MIN = $$member(QT_VERSION, 1)

greaterThan(QT_VER_MAJ, 5) {
greaterThan(QT_VER_MIN, 4) {
#Qt5.4及以上版本才有但是要5.5及以上版本才稳定
DEFINES += openglnew
}}

lessThan(QT_VER_MAJ, 5) {
lessThan(QT_VER_MIN, 8) {
DEFINES -= opengl
message(当前Qt版本$$QT_VER_MAJ . $$QT_VER_MIN 暂不支持 QGLFunctions)
}}

#2020-12-12 暂时限定存在qopenglwidget模块才能启用
#因为qglwidget如何绘制实时视频没有搞定需要换成回调模式
!contains(DEFINES, openglnew) {
DEFINES -= opengl
message(当前Qt版本$$QT_VER_MAJ . $$QT_VER_MIN 暂不支持 QOpenGLWidget)
}

#表示arm平台构建套件
contains(QT_ARCH, arm) {
strInclude = include
#arm上一般没有opengl或者支持度不友好或者没有qopenglwidget
#如果都有可以自行屏蔽下面这行
DEFINES -= opengl openglnew
}

contains(DEFINES, opengl){
    contains(DEFINES, openglnew){
        HEADERS += $$PWD/nv12openglwidget.h
        HEADERS += $$PWD/yuvopenglwidget.h

        SOURCES += $$PWD/nv12openglwidget.cpp
        SOURCES += $$PWD/yuvopenglwidget.cpp
    }else{
        #旧版本的opengl采用编译时链接 新版本的采用动态运行链接
        QT += opengl
        HEADERS += $$PWD/nv12glwidget.h
        HEADERS += $$PWD/yuvglwidget.h

        SOURCES += $$PWD/nv12glwidget.cpp
        SOURCES += $$PWD/yuvglwidget.cpp
}}
