QT       += core gui
QT += printsupport
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

TRANSLATIONS += \
    en_US.ts
CONFIG += lrelease
#CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images.qrc
RC_ICONS = images/logo.ico

DISTFILES += \
    images/bold.png \
    images/copy.png \
    images/create.png \
    images/cut.png \
    images/edit_redo.png \
    images/edit_undo.png \
    images/exit.png \
    images/font.png \
    images/info.png \
    images/italic.png \
    images/logo.ico \
    images/new.png \
    images/open.png \
    images/paste.png \
    images/pencil.png \
    images/print.png \
    images/save.png \
    images/save_as.png \
    images/underline.png
