﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma execution_character_set("utf-8")
#include <QMainWindow>
#include <QFileDialog>
#include <QFile>
#include <QDebug>
#include <QMessageBox>
#include <QCloseEvent>
#include <QPrintDialog>
#include <QPrinter>
#include <QFontDialog>
#include <QFont>
#include <QTranslator>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_action_open_triggered();

    void on_action_save_triggered();

    void on_textEdit_textChanged();

    void on_action_new_triggered();

    void on_action_saveas_triggered();

    void on_action_quit_triggered();

    void closeEvent(QCloseEvent *event);

    void on_action_print_triggered();

    void on_action_copy_triggered();

    void on_action_paste_triggered();

    void on_action_cut_triggered();

    void on_action_undo_triggered();

    void on_action_redo_triggered();

    void on_action_font_triggered();

    void on_action_bold_triggered(bool checked);

    void on_action_italic_triggered(bool checked);

    void on_action_underline_triggered(bool checked);

    void on_action1_triggered();

    void on_action_aboutQt_triggered();

    void on_action_language_triggered(bool checked);

private:
    Ui::MainWindow *ui;
    QString fileName;//打开的文件名
    bool saveFlag=true;//文件是否保存标志位，有true：表示存盘；false：表示为存盘
};
#endif // MAINWINDOW_H
