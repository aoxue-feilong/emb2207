<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="50"/>
        <source>文件(&amp;F)</source>
        <translation>file(&amp;F)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="65"/>
        <source>编辑(&amp;E)</source>
        <translation>compile(&amp;E)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="75"/>
        <source>格式(&amp;G)</source>
        <translation>format(&amp;G)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <source>帮助(&amp;H)</source>
        <translation>help(&amp;H)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <source>toolBar</source>
        <translation>toolbar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="130"/>
        <source>新建</source>
        <translation>new</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="133"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="142"/>
        <location filename="mainwindow.cpp" line="23"/>
        <source>打开</source>
        <translation>open</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="145"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="154"/>
        <location filename="mainwindow.cpp" line="45"/>
        <source>保存</source>
        <translation>save</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="157"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <location filename="mainwindow.cpp" line="175"/>
        <source>另存为</source>
        <translation>save as</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="169"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="177"/>
        <source>切换到英文</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>打印</source>
        <translation>print</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="189"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="198"/>
        <source>退出</source>
        <translation>quit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="201"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="210"/>
        <source>复制</source>
        <translation>copy</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="222"/>
        <source>粘贴</source>
        <translation>paste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="225"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="234"/>
        <source>剪切</source>
        <translation>cut</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="246"/>
        <source>撤销</source>
        <translation>undo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="249"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="258"/>
        <source>恢复</source>
        <translation>redo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="261"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <source>字体</source>
        <translation>font</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="282"/>
        <source>粗体</source>
        <translation>bild</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="294"/>
        <source>斜体</source>
        <translation>italic</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="306"/>
        <source>下划线</source>
        <translation>underline</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <source>关于笔记本</source>
        <translation>About the Notepad</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="320"/>
        <source>关于Qt</source>
        <translation>About the Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="9"/>
        <location filename="mainwindow.cpp" line="89"/>
        <location filename="mainwindow.cpp" line="307"/>
        <source>记事本</source>
        <translation>notepad</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="33"/>
        <location filename="mainwindow.cpp" line="56"/>
        <location filename="mainwindow.cpp" line="66"/>
        <location filename="mainwindow.cpp" line="72"/>
        <location filename="mainwindow.cpp" line="102"/>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="145"/>
        <location filename="mainwindow.cpp" line="151"/>
        <location filename="mainwindow.cpp" line="165"/>
        <location filename="mainwindow.cpp" line="186"/>
        <source>-记事本</source>
        <translation>notepad</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="66"/>
        <location filename="mainwindow.cpp" line="102"/>
        <location filename="mainwindow.cpp" line="108"/>
        <location filename="mainwindow.cpp" line="145"/>
        <location filename="mainwindow.cpp" line="151"/>
        <location filename="mainwindow.cpp" line="165"/>
        <source>无标题</source>
        <translation>untitled-Notepad</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="307"/>
        <source>这是记事本 1.5</source>
        <translation>
This is Notepad  1.5</translation>
    </message>
</context>
</TS>
