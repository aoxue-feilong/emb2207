﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("记事本"));//设置程序的标题
    fileName="";//对文件名进行初始化

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_action_open_triggered()
{
    //打开文件
    fileName=QFileDialog::getOpenFileName(this,tr("打开"),".","Text file (*.txt);; All Files (*.*)");
    qDebug()<<"fileName"<<fileName;
    if(!fileName.isEmpty())//获取到了文件名,才可以操作
    {
        QFile file(fileName);//创建一个文件对象
        if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
            return;
        QByteArray data = file.readAll();//data就是一个数组，带编码形式的数组，需要解码
        ui->textEdit->setText(QString(data));//显示到控件上
        QFileInfo fileinfo(fileName);
        this->setWindowTitle(fileName+tr("-记事本"));//设置程序的标题
        file.close();//关闭文件
        saveFlag=true;//数据已存盘
    }
}

//保存文件
void MainWindow::on_action_save_triggered()
{
    //判断文件名是否为空,为空表示这个文件没有被保存,需要弹窗保存
    //文件名不为空,表示这个文件是打开的文件,那么直接保存即可,不需要弹窗保存
    if(fileName.isEmpty())//文件名为空,要弹窗
    {
        fileName=QFileDialog::getSaveFileName(this,tr("保存"),".","Text file (*.txt);; All Files (*.*)");
        if(fileName.isEmpty())//如果文件名为空
            return;
    }
    QFile file(fileName);//创建一个文件对象
    if (!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate ))
        return;
    //把QString 转换成 QByteArray ,需要对字符串进行编码即可开
    file.write(ui->textEdit->toPlainText().toUtf8());//把数据写到文件内
    file.close();//关闭文件
    saveFlag=true;//文件保存，已存盘
    this->setWindowTitle(QFileInfo(fileName).fileName()+tr("-记事本"));
}



//文本发生改变的信号处理函数
void MainWindow::on_textEdit_textChanged()
{
    if(fileName.isEmpty())    //如果文件名是空
    {
        if(ui->textEdit->toPlainText().isEmpty()) //且内容也为空
        {
            this->setWindowTitle(tr("无标题")+tr(" - 记事本"));   //在标题上显示 *无标题-记事本
            saveFlag=true;   //数据存盘
        }
        else
        {
            this->setWindowTitle("*"+tr("无标题")+tr(" - 记事本"));//在标题上显示  *文件名-记事本
            saveFlag=false;  //数据没存盘
        }

    }
    else    //如果文件名存在
    {
        QFileInfo fileinfo(fileName);
        this->setWindowTitle("*"+fileinfo.fileName()+tr(" - 记事本"));   //在标题上显示  *文件名-记事本
        saveFlag=false;    //数据没存盘
    }
}

//新建文件
void MainWindow::on_action_new_triggered()
{
    if(fileName.isEmpty())//文件名为空
    {
        //且 textedit 中有内容，需要弹窗，提示是否需要保存
        if(!ui->textEdit->toPlainText().isEmpty())//textEdit 有内容
        {
            //弹窗
            qDebug()<<"要弹窗";
            QMessageBox msgBox;
            //遗留问题如何改变弹窗大小，目前改变不了
            msgBox.setWindowTitle(tr("记事本"));
            msgBox.setText("The document has been modified.");
            msgBox.setInformativeText("Do you want to save your changes?");
            msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Save);
            int ret = msgBox.exec();
            switch (ret) {
            case QMessageBox::Save:
                // Save was clicked
                on_action_save_triggered();
                //fileName 字符串清空
                fileName.clear();
                ui->textEdit->clear();
                this->setWindowTitle(tr("无标题")+tr("-记事本"));//更改程序的标题
                break;
            case QMessageBox::Discard:
                // Don't Save was clicked
                fileName.clear();
                ui->textEdit->clear();
                this->setWindowTitle(tr("无标题")+tr("-记事本"));//更改程序的标题
                break;
            case QMessageBox::Cancel:
                // Cancel was clicked
                break;
            default:
                // should never be reached
                break;
            }


        }
        else//textEdit 无内容
        {
            qDebug()<<"文件名为空，textEdit 无内容，不需要弹窗";
        }
    }
    else//文件名不为空
    {
        //文件是否存盘 true 存盘，false 没有存盘。
        if(!saveFlag)//文件没有存盘
        {
            qDebug()<<"文件名不为空，文件没有存盘，需要弹窗";
            QMessageBox msgBox;
            msgBox.setText("The document has been modified.");
            msgBox.setInformativeText("Do you want to save your changes?");
            msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Save);
            int ret = msgBox.exec();
            switch (ret)
            {
            case QMessageBox::Save:
                // Save was clicked
                on_action_save_triggered();
                //fileName 字符串清空
                fileName.clear();
                ui->textEdit->clear();
                this->setWindowTitle(tr("无标题")+tr("-记事本"));//更改程序的标题
                break;
            case QMessageBox::Discard:
                // Don't Save was clicked
                fileName.clear();
                ui->textEdit->clear();
                this->setWindowTitle(tr("无标题")+tr("-记事本"));//更改程序的标题
                break;
            case QMessageBox::Cancel:
                // Cancel was clicked
                break;
            default:
                // should never be reached
                break;
            }
        }
        else//文件已存盘
        {
            fileName.clear();
            ui->textEdit->clear();
            this->setWindowTitle(tr("无标题")+tr("-记事本"));//更改程序的标题
        }
    }
}

//另存为
void MainWindow::on_action_saveas_triggered()
{
    if(fileName.isEmpty())//文件名为空,要弹窗
    {
        fileName=QFileDialog::getSaveFileName(this,tr("另存为"),".","Text file (*.txt);; All Files (*.*)");
        if(fileName.isEmpty())//如果文件名为空
            return;
    }
    QFile file(fileName);//创建一个文件对象
    if (!file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate ))//弹出文件夹弹窗
        return;
    //把QString 转换成 QByteArray ,需要对字符串进行编码即可开
    file.write(ui->textEdit->toPlainText().toUtf8());//把数据写到文件内
    file.close();//关闭文件
    saveFlag=true;//文件保存，已存盘
    this->setWindowTitle(QFileInfo(fileName).fileName()+tr("-记事本"));
}

void MainWindow::on_action_quit_triggered()
{
    this->close();//关闭窗体即可
}
//关闭弹窗
//要实现关闭窗口时，提示是否保存文件，必须使用重新实现 MainWindow 中的一个办法 closeEvent
//每次关闭窗口时，发送一个事件QCloseEvent，应用程序可以接受这个事件，并对这个事件进行处理

void MainWindow::closeEvent(QCloseEvent *event)
{
    qDebug()<<"closeEvent被触发";

    //

    if(fileName.isEmpty()) // 文件名为空
    {
        // 且 textedit 中有内容, 需要弹窗, 提示是否需要保存
        if(!ui->textEdit->toPlainText().isEmpty()) //textedit 中有内容
        {
            // 弹窗
            qDebug()<<"要弹窗";
            QMessageBox msgBox;
            // 遗留问题1: 如何改变弹窗的尺寸, 目前改不了
            msgBox.setWindowTitle(tr("记事本"));
            //msgBox.setText("The document has been modified.");
            msgBox.setInformativeText("Do you want to save your changes?");
            msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Save); // 默认选择
            int ret = msgBox.exec();
            switch (ret)
            {
            case QMessageBox::Save:
                // Save was clicked
                on_action_save_triggered();
                // fileName 字符串清空
                fileName.clear();
                ui->textEdit->clear();
                this->setWindowTitle(tr("无标题")+tr(" - 记事本"));
                // 继续让事件传递下去
                event->accept();
                break;
            case QMessageBox::Discard:
                // Don't Save was clicked
                fileName.clear();
                ui->textEdit->clear();
                this->setWindowTitle(tr("无标题")+tr(" - 记事本"));
                // 继续让事件传递下去
                event->accept();
                break;
            case QMessageBox::Cancel:
                // Cancel was clicked
                event->ignore(); // 忽略关闭窗体的操作
                break;
            default:
                // should never be reached
                break;
            }
        }
        else  // 文件名为空 , textedit没有内容, 不需要弹窗
        {
            qDebug()<<"文件名为空 , textedit没有内容, 不需要弹窗";
        }

    }
    else  // 文件名不为空
    {
        // 文件是否存盘
        // 文件是否保存标志位, true: 表示存盘 , false : 没有存盘
        if(!saveFlag) // 条件满足 , 表示没有存盘 , 需要弹窗提示保存
        {
            qDebug()<<"文件名不为空 文件没有保存,需要弹窗";
            QMessageBox msgBox;
            // 遗留问题1: 如何改变弹窗的尺寸, 目前改不了
            //msgBox.setText("The document has been modified.");
            msgBox.setInformativeText("Do you want to save your changes?");
            msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Save); // 默认选择
            int ret = msgBox.exec();
            switch (ret)
            {
            case QMessageBox::Save:
                // Save was clicked
                on_action_save_triggered();
                // fileName 字符串清空
                fileName.clear();
                ui->textEdit->clear();
                this->setWindowTitle(tr("无标题")+tr(" - 记事本"));
                // 继续让事件传递下去
                event->accept();
                break;
            case QMessageBox::Discard:
                // Don't Save was clicked
                fileName.clear();
                ui->textEdit->clear();
                this->setWindowTitle(tr("无标题")+tr(" - 记事本"));
                // 继续让事件传递下去
                event->accept();
                break;
            case QMessageBox::Cancel:
                // Cancel was clicked
                event->ignore(); // 忽略关闭窗体的操作
                break;
            default:
                // should never be reached
                break;
            }
        }
        else // 文件已存盘
        {
            fileName.clear();
            ui->textEdit->clear();
            this->setWindowTitle(tr("无标题")+tr(" - 记事本"));
        }
    }
}
//打印
void MainWindow::on_action_print_triggered()
{
    QPrinter printer;//定义一个打印机
    QPrintDialog printerDialog(&printer,this);//定义一个对话窗口
    if(printerDialog.exec()==QDialog::Accepted)//点击确定是开始打印
    {
        ui->textEdit->print(&printer);
    }
}

//复制
void MainWindow::on_action_copy_triggered()
{
    ui->textEdit->copy();
}

//粘贴
void MainWindow::on_action_paste_triggered()
{
    ui->textEdit->paste();
}

//剪切
void MainWindow::on_action_cut_triggered()
{
    ui->textEdit->cut();
}

//撤销
void MainWindow::on_action_undo_triggered()
{
    ui->textEdit->undo();
}

//恢复
void MainWindow::on_action_redo_triggered()
{
    ui->textEdit->redo();
}

//设置字体
void MainWindow::on_action_font_triggered()
{
    bool ok;
    //获取字体
    QFont font = QFontDialog::getFont(
                &ok, QFont("Helvetica [Cronyx]", 10), this);
    if (ok) {
        // the user clicked OK and font is set to the font the user selected
    } else {
        // the user canceled the dialog; font is set to the initial
        // value, in this case Helvetica [Cronyx], 10
    }
    qDebug()<<"font="<<font;
    ui->textEdit->setFont(font);//对选中的文本设置字体
}

//设置粗体
void MainWindow::on_action_bold_triggered(bool checked)
{
    qDebug()<<"checked="<<checked;
    if(checked)
    {
        ui->textEdit->setFontWeight(QFont::Normal);//设置粗体
    }
    else
    {
        ui->textEdit->setFontWeight(QFont::Bold);//取消设置粗体
    }
}

//设置斜体
void MainWindow::on_action_italic_triggered(bool checked)
{
    qDebug()<<"checked="<<checked;
    if(checked)
    {
        ui->textEdit->setFontItalic(true);//设置斜体
    }
    else
    {
        ui->textEdit->setFontItalic(false);//取消设置斜体
    }
}

//设置下划线
void MainWindow::on_action_underline_triggered(bool checked)
{
    qDebug()<<"checked="<<checked;
    if(checked)
    {
        ui->textEdit->setFontUnderline(true);//设置下划线
    }
    else
    {
        ui->textEdit->setFontUnderline(false);//取消设置下划线
    }
}


void MainWindow::on_action1_triggered()
{
    QMessageBox::about(this,tr("记事本"),tr("这是记事本 1.5"));
}


void MainWindow::on_action_aboutQt_triggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_action_language_triggered(bool checked)
{
    qDebug()<<"on_action_language_triggered()";
    if(checked) // 切换到英文
    {
        qDebug()<<"切换到英文";
        QTranslator translator; // 语言翻译类   //QTranslator语言翻译类
        translator.load("en_US.qm"); // 去当前目录下去查
        QApplication::installTranslator(&translator);
        ui->retranslateUi(this);//重新初始化界面

    }
    else
    {

        qDebug()<<"切换到中文";
        QApplication::installTranslator(nullptr);
        ui->retranslateUi(this);
    }
}

