﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma execution_character_set("utf-8")

#include "polygon.h"
#include <QMainWindow>
#include <QFileDialog>
#include <QPaintEvent>
#include <QDebug>
#include <QPainter>
#include <QSettings>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected slots:
    // 这个函数是虚函数, 子类重新实现父类的方法
    // 实现绘画事件 paintEvent
    // 在窗口被显示的时候调用一次 ,  窗口发生改变时都会被调用
    // 手动使用update 函数时 也会被调用
    void paintEvent(QPaintEvent *event) ;


    // 这几个函数是虚函数, 这里时重新实现QWidget的函数, 子类覆盖父类
    void mousePressEvent(QMouseEvent *event);  // 鼠标按压事件
    void mouseReleaseEvent(QMouseEvent *event); // 鼠标释放事件
    void mouseMoveEvent(QMouseEvent *event);   // 鼠标移动事件
private slots:
    void on_action_picture_triggered();



    void on_action_line_triggered();

    void on_action_polygon_triggered();

    // 设置 add_newpolygon 信号处理函数
    void add_newpolygon_slot() ;

private:
    Ui::MainWindow *ui;
    QString fileName; // 用来保存文件名
    QLine *linep ;
    bool drawLineFlag = false; // true: 可以画线 , false :不可以画线
    bool drawPolygonFlag = false; // true: 可以画多边形 , false :不可以画多边形
    qint32 linePointIndex = 0 ; // 0: 没有选中 , 1: p1被中 , 2:p2被选中
    qint32 polygonPointIndex=0; // 选中多边形的点 , 0: 表示没选中, 1-10 表示点1-10 被选中
    polygon * polygonP; // 这个指针指向弹窗
    // 定义一个存放多边形的容器 vector
    QVector<QPoint > Polygons;

};
#endif // MAINWINDOW_H
