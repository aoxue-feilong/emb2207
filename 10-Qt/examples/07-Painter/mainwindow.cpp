﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowState(Qt::WindowMaximized); // 让窗体最大化

    // 在构造函数中, 对fileName进行赋值, 这样可以自动加载这个图片
    fileName = ":/images/6.jpg"; // 设置为空

    // 对linep 进行初始化
    linep = nullptr;
    // linep  = new QLine(100,100,200,200);

    // 指针初始化为空
    polygonP = nullptr;

    // 初始化 Polygons
}

MainWindow::~MainWindow()
{
    delete ui;
}
//加载图片函数
void MainWindow::on_action_picture_triggered()
{

    fileName = QFileDialog::getOpenFileName(this, tr("打开图片"), ".",
                                            "Picture Files (*.jdp *.png *.bmp);; All Files (*.*)");
    if (!fileName.isEmpty()) //不等于空, 要把这个图片显示到控件内
    {
        update(); // 这个函数被调用后, 会产生一个绘画事件 paintEvent ,paintEvent 事件 是一个虚函数, 我们要重新实现这个函数
    }
    qDebug() << "fileName=" << fileName;
}

// 实现绘画事件 paintEvent
// 在窗口被显示的时候调用一次 ,  窗口发生改变时都会被调用
// 手动使用update 函数时 也会被调用
void MainWindow::paintEvent(QPaintEvent *event)
{
    qDebug() << "paintEvent() 被调用";
    QPainter painter(this); // 定义一个画笔, 这个画笔可以画出很多种形状, 图片
    // 把背景图片显示到控件上
    if (!fileName.isEmpty()) //判断文件名是否存在
    {
        QPixmap map(fileName);                 // 定义一个pixmap对象
        painter.drawPixmap(QPoint(0, 0), map); // 把图片绘制到窗体中
    }
    if (drawLineFlag) //为真时表示要画线
    {
        if (linep != nullptr)
        {
            QPen pen;
            pen.setColor(Qt::red); // 设置画笔的颜色
            pen.setWidth(5);       // 设置笔的宽度, 以像素为单位
            painter.setPen(pen);   // 安装一个画笔
            painter.drawLine(linep->p1(), linep->p2());
            painter.drawEllipse(linep->p1(), 4, 4); // 画出线的弧度, 同心的圆
            painter.drawEllipse(linep->p2(), 4, 4); // 画出线的弧度, 同心的圆

            // 保存线的信息到文件内
            QSettings set("config.ini", QSettings::IniFormat);
            set.beginGroup("Line1"); // 开始的组
            set.setValue("x1", linep->p1().x());
            set.setValue("y1", linep->p1().y());
            set.setValue("x2", linep->p2().x());
            set.setValue("y2", linep->p2().y());
            set.endGroup(); //结束的组
        }
    }
    if (drawPolygonFlag) // 为真时表示要画多边形
    {

        QPen pen;
        pen.setColor(Qt::yellow);                      // 设置画笔的颜色
        pen.setWidth(5);                               // 设置笔的宽度, 以像素为单位
        painter.setPen(pen);                           // 安装一个画笔
        painter.setRenderHint(QPainter::Antialiasing); // 边缘抗锯齿 在绘制各种形状时 可以设置抗锯齿
        // 先构建一个多边形 QPolygon
        QPolygon pol(Polygons);   // 用Polygons 这个容器, 去构建一个pol
        painter.drawPolygon(pol); // 画多边形
        for (int i = 0; i < Polygons.size(); i++)
        {
            painter.drawEllipse(Polygons.at(i), 4, 4); // 画出线的弧度, 同心的圆
        }

        // 保存对变形信息
        QSettings set("config.ini", QSettings::IniFormat);
        set.beginGroup("Polygon1"); // 开始的组
        // 获取子窗体多边形的吧边数
        for (int i = 0; i < Polygons.count(); i++)
        {
            qint32 x = Polygons.at(i).x(); // 读出x 的坐标
            qint32 y = Polygons.at(i).y(); // 读出y 的坐标
            set.setValue(tr("x%0").arg(i + 1), x);
            set.setValue(tr("y%0").arg(i + 1), y);
        }
        set.endGroup(); //结束的组
    }
}
//直线的槽函数 line
void MainWindow::on_action_line_triggered()
{
    // 设置一个标志位 , 花点的实现要在paintEvent函数内实现, 在这里只需要设置update即可
    drawLineFlag = true; // 表示可以画线
    update();            // 更新绘画 , 调用paintEvent函数

    // 如果系统已经保存了 line的信息, 则需要自动加载线的信息
    // 如果没有保存, 则使用默认的线信息
    // 创建一个config.ini文件 , 文件不存在则创建, 文件存在则打开
    QSettings set("config.ini", QSettings::IniFormat);
    set.beginGroup("Line1"); // 开始的组
    //如果Line1有x1 这个key , 说明我们配置文件中已经保存了线 , 此时就需要我们加载即可
    if (set.contains("x1")) //  可以在config.ini 进行搜索 , 找到和没找到两种
    {
        qint32 x1 = set.value("x1").toInt();
        qint32 y1 = set.value("y1").toInt();
        qint32 x2 = set.value("x2").toInt();
        qint32 y2 = set.value("y2").toInt();
        linep = new QLine(x1, y1, x2, y2);
    }
    else // 如果没有就创建 这些值
    {
        qint32 x1 = 100;
        qint32 y1 = 100;
        qint32 x2 = 200;
        qint32 y2 = 200;
        linep = new QLine(x1, y1, x2, y2);
        set.setValue("x1", x1);
        set.setValue("y1", y1);
        set.setValue("x2", x2);
        set.setValue("y2", y2);
    }
    set.endGroup(); //结束的组
}
// 鼠标按下事件
void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) // 如果是鼠标左键按下
    {
        qDebug() << "鼠标左键被按下";
        qDebug() << "鼠标被单击:" << event->pos(); // 打印鼠标的位置
        if (linep != nullptr)
        {
            // 鼠标点击的位置 和 线的坐标位置保持误差在10以内 我们认为就是点击了这个点
            if ((abs(event->pos().x() - linep->p1().x()) < 10) &&
                (abs(event->pos().y() - linep->p1().y()) < 10))
            {
                qDebug() << "p1 被选中";
                linePointIndex = 1;         //表示p1被选中
                setCursor(Qt::CrossCursor); // 设置鼠标为十字光标
            }
            else if ((abs(event->pos().x() - linep->p2().x()) < 10) &&
                     (abs(event->pos().y() - linep->p2().y()) < 10))
            {
                qDebug() << "p2 被选中";
                linePointIndex = 2;         //表示p2被选中
                setCursor(Qt::CrossCursor); // 设置鼠标为十字光标
            }
            else
            {
                linePointIndex = 0;         //没有点被选中
                setCursor(Qt::ArrowCursor); // 正常鼠标样式
            }
        }

        // 鼠标点击的位置 和 多边形的坐标位置保持误差在10以内 我们认为就是点击了这个点
        for (int i = 0; i < Polygons.count(); i++)
        {
            if ((abs(event->pos().x() - Polygons.at(i).x()) < 10) &&
                (abs(event->pos().y() - Polygons.at(i).y()) < 10))
            {
                polygonPointIndex = i + 1; // 记录哪一个点被选中
                qDebug() << "Polygons 被选中:" << polygonPointIndex;
                setCursor(Qt::CrossCursor); // 设置鼠标为十字光标
                break;                      // 选中点后 , 退出循环
            }
            else
            {
                polygonPointIndex = 0;
                setCursor(Qt::ArrowCursor); // 正常鼠标样式
            }
        }
    }
}
// 鼠标移动事件
// 可设置点击后移动, 认为是鼠标移动
// 也可以实时的捕捉
void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    qDebug() << "鼠标移动";
    // 是判断鼠标在单击的过程中移动了鼠标
    if (event->buttons() & Qt::LeftButton) // 这里必须使用buttons()
    {
        drawLineFlag = true;     // 设置线绘制标志位 , 会对线进行重新绘制
        if (linePointIndex == 1) // 表示选中的p1点
        {
            linep->setP1(QPoint(event->pos())); // 使用当前鼠标的坐标来替换线段的点
            update();                           // 产生一次绘图事件
        }
        else if (linePointIndex == 2) // 表示选中的p1点
        {
            linep->setP2(QPoint(event->pos())); // 使用当前鼠标的坐标来替换线段的点
            update();                           // 产生一次绘图事件
        }

        drawPolygonFlag = true;    // 设置绘制多边形标志位 , 会对多边形进行重新绘制
        if (polygonPointIndex > 0) // 表示多边形有点被选中
        {
            qDebug() << "mouseMoveEvent:polygonPointIndex=" << polygonPointIndex;
            Polygons.replace(polygonPointIndex - 1, QPoint(event->pos()));
            update(); // 产生一次绘图事件
        }
    }
}
// 鼠标释放事件
void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    // 释放鼠标后 , 要恢复鼠标的形状
    setCursor(Qt::ArrowCursor); // 正常鼠标样式
}
//多边形的槽函数 polygon
void MainWindow::on_action_polygon_triggered()
{
    if (polygonP != nullptr)
    {
        delete polygonP; // 释放内存
    }
    //要做一个弹窗, 在qt 中需要一个子窗体时, 需要单独创建一个窗体类
    polygonP = new polygon; //这里父窗体不用this,使用了this以后, 会出现内嵌的情况, 让这个窗体独立显示
    // polygon 设置信号处理函数
    // polygonP(谁发信号 小窗体)  &polygon::add_newpolygon（发送什么信号）
    // this(谁接受信号 本应用程序)    &MainWindow::add_newpolygon_slot（信号处理函数）
    connect(polygonP, &polygon::add_newpolygon, this, &MainWindow::add_newpolygon_slot);

    polygonP->show(); // 显示控件 , 让子窗体显示
}
// polygon多边形的信号处理函数
void MainWindow::add_newpolygon_slot()
{
    qDebug() << "add_newpolygon_slot被调用";

    // 如果系统已经保存了 polygon的信息, 则需要自动加载多边形的信息
    // 如果没有保存, 则使用默认的多边形信息
    // 创建一个config.ini文件 , 文件不存在则创建, 文件存在则打开
    QSettings set("config.ini", QSettings::IniFormat);
    set.beginGroup("Polygon1"); //开始的组

    //如果Polygon1有x1 这个key , 说明我们配置文件中已经保存了多边形 , 此时就需要我们加载即可
    if (set.contains("x1")) //可以在config.ini 进行搜索 , 找到和没找到两种
    {
        if (polygonP != nullptr)
        {
            Polygons.clear();                            // 把vector清空
            qint32 count = polygonP->getPolygonPoints(); // 获取要画多边形的点数
            for (int i = 0; i < count; i++)
            {
                qint32 x = set.value(tr("x%0").arg(i + 1)).toInt();
                qint32 y = set.value(tr("y%0").arg(i + 1)).toInt();
                Polygons.append(QPoint(x, y)); // 向vector中添加点
            }
            drawPolygonFlag = true; // 设置画多边形的标志位 ,paintEvent 才可以绘制出多边形
        }
    }
    else // 如果没有就创建 这些值
    {

        qint32 xx[] = {208, 514, 804, 743, 568, 239, 140, 120, 100, 80};
        qint32 yy[] = {289, 208, 344, 673, 772, 712, 280, 240, 200, 160};

        // 获取子窗体多边形的吧边数
        if (polygonP != nullptr)
        {
            qint32 count = polygonP->getPolygonPoints(); // 获取要画多边形的点数
            for (int i = 0; i < count; i++)
            {
                Polygons.append(QPoint(xx[i], yy[i])); // 向vector中添加点
                set.setValue(tr("x%0").arg(i + 1), xx[i]);
                set.setValue(tr("y%0").arg(i + 1), yy[i]);
            }
            drawPolygonFlag = true; // 设置画多边形的标志位 ,paintEvent 才可以绘制出多边形
        }
    }

    set.endGroup(); //结束的组
}
