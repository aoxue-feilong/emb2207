﻿#ifndef POLYGON_H
#define POLYGON_H
#pragma execution_character_set("utf-8")
#include <QWidget>
#include <QDebug>

namespace Ui {
class polygon;
}

class polygon : public QWidget
{
    Q_OBJECT

public:
    explicit polygon(QWidget *parent = nullptr);
    ~polygon();
    qint32 getPolygonPoints();

signals:
    void add_newpolygon();//自定义信号

private slots:
    void on_pushButton_ok_clicked();

    void on_pushButton_cancel_clicked();



private:
    Ui::polygon *ui;
};

#endif // POLYGON_H
