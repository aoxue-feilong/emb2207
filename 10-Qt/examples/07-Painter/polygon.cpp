﻿#include "polygon.h"
#include "ui_polygon.h"

polygon::polygon(QWidget *parent) : QWidget(parent),
                                    ui(new Ui::polygon)
{
    ui->setupUi(this);
}

polygon::~polygon()
{
    delete ui;
}
//确定的槽函数
void polygon::on_pushButton_ok_clicked()
{
    // 点击ok一次，发送一次信号
    emit add_newpolygon(); // 10 信号带入的参数
    qDebug() << "add_newpolygon() 信号被发送";
    this->hide(); // 隐藏窗口
}

//取消的槽函数
void polygon::on_pushButton_cancel_clicked()
{
    this->close(); // 关闭窗体
}

qint32 polygon::getPolygonPoints()
{
    //返回多边形点数的索引
    return ui->comboBox->currentIndex() + 3;
}
