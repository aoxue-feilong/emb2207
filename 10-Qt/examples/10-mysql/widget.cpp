﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //设置默认值
    ui->lineEdit_number->setText(tr("20210001"));
    ui->lineEdit_name->setText(tr("李成龙"));
    ui->comboBox_gender->setCurrentIndex(0);
    ui->lineEdit_age->setText(tr("22"));
    ui->lineEdit_score->setText(tr("88.9"));

    QStringList lists =  QSqlDatabase::drivers() ;
    foreach (QString l, lists)
    {
        qDebug()<<"支持驱动为:"<<l;
    }
    // 运行软件时打开数据库
    db = QSqlDatabase::addDatabase("QMYSQL");// 设置要打开数据库的类型, 这个类型必须是支持的类型
    db.setDatabaseName("student"); // 要打开的数据库名字 , 不存在创建, 存在则打开
    db.setHostName("localhost");   //计算机名称   localhost 是本机ip
    db.setPort(3306);              //设置mysql 后台服务的端口
    db.setUserName("root");        //设置mysql 登录的用户名
    db.setPassword("123456");      //设置mysql 登录时的密码

    bool ok = db.open();
    if(!ok) // 打开数据库失败
    {
        QMessageBox::warning(this,tr("sqlite3"),tr("数据库打开失败"));
    }

    // 登录数据库后, 要先创建一个数据库
    // 登陆数据库成功后 ,要先创建一个数据库 , 可以使用命令
    // mysql -u root -p
    // 输入密码:123456
    // create DATABASE student;
    // 注意 命令要有一个 ";"  作为结束符
    // 数据库不能以.db形式出现 , 不能有后缀名
    // 选择要使用的数据库

    QSqlQuery query(db); // 要使用哪一个数据库实例
    QString sql = tr("use student;"); // 指定要选择的数据库
    ok = query.exec(sql); // 执行sql语句
    if(!ok)
    {
        QMessageBox::warning(this,"use student","语法错误");
    }
    model = new QStandardItemModel(this);
    /*设置列字段名*/
    model->setColumnCount(5);   // 设置 一行有2列
    model->setHeaderData(0,Qt::Horizontal, tr("学号"));  // 第1列名 序号
    model->setHeaderData(1,Qt::Horizontal, tr("姓名"));  // 第2列名 数值
    model->setHeaderData(2,Qt::Horizontal, tr("性别"));  // 第3列名 数值
    model->setHeaderData(3,Qt::Horizontal, tr("年龄"));  // 第4列名 数值
    model->setHeaderData(4,Qt::Horizontal, tr("成绩"));  // 第5列名 数值

    ui->tableView->horizontalHeader()->setDefaultSectionSize(ui->tableView->size().width()/5-1); //设置水平头的宽度
    ui->tableView->verticalHeader()->setHidden(true); // 行名隐藏
    ui->tableView->setModel(model) ; // 给tableview 安装一个模型 model

    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    //设置表格的单元为只读属性，即不能编辑
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    showAll();
}

Widget::~Widget()
{
    delete ui;
}


void Widget::showAll()
{
    // 清空原有记录

    model->removeRows(0,lineCount);  // 从0行 一直删除到 指定的行
    lineCount = 0 ; // 行号清空

    // 创建数据库的表
    // 执行一个sql语句 , 使用qsqlquery
    QString sql = "select * from student;";
    QSqlQuery query; // 创建一个执行sql语句的对象
    bool ok =  query.exec(sql);
    if(!ok) // 执行sql 语句失败
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("查询表错误"),query.lastError().text());
        return  ;
    }
    else
    {

        // 下一个记录是否有效, 有效返回为真, 无效返回为假
        while(query.next()) // 为真表示找到了记录
        {
            qDebug()<<"number="<<query.value(0).toInt();  // 第1列的值
            qDebug()<<"name  ="<<query.value(1).toString();  // 第2列的值
            qDebug()<<"gender="<<query.value(2).toString();  // 第3列的值
            qDebug()<<"age   ="<<query.value(3).toInt();  // 第4列的值
            qDebug()<<"score ="<<query.value(4).toFloat();  // 第5列的值
            /*设置一条数据*/
            model->setItem(lineCount, 0, new QStandardItem(query.value(0).toString())  );
            model->setItem(lineCount, 1, new QStandardItem(query.value(1).toString()) );
            model->setItem(lineCount, 2, new QStandardItem(query.value(2).toString()) );
            model->setItem(lineCount, 3, new QStandardItem(query.value(3).toString()) );
            model->setItem(lineCount, 4, new QStandardItem(query.value(4).toString()) );

            model->item(lineCount, 0)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 1)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 2)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 3)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 4)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            ui->tableView->setModel(model) ; // 给tableview 安装一个模型 model
            lineCount ++;
        }
    }

}


void Widget::on_pushButton_create_clicked()
{
    // 创建数据库的表
    // 执行一个sql语句 , 使用qsqlquery
    QString sql = "create table student(number int primary key,name char(20),gender char(10),age int,score float);";
    QSqlQuery query; // 创建一个执行sql语句的对象
    bool ok =  query.exec(sql);
    if(!ok) // 执行sql 语句失败
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("创建表错误"),query.lastError().text());
        return  ;
    }
    else
    {
        QMessageBox::information(this,tr("创建表成功"),tr("创建student表成功") );
    }
}


void Widget::on_pushButton_insert_clicked()
{
    // 创建数据库的表
    // 执行一个sql语句 , 使用qsqlquery
    QString sql = tr("insert into student values(%0,'%1','%2',%3,%4);")
                    .arg(ui->lineEdit_number->text().toInt())
                    .arg(ui->lineEdit_name->text())
                    .arg(ui->comboBox_gender->currentText())
                    .arg(ui->lineEdit_age->text().toInt())
                    .arg(ui->lineEdit_score->text().toFloat()) ;

    QSqlQuery query; // 创建一个执行sql语句的对象
    bool ok =  query.exec(sql);
    if(!ok) // 执行sql 语句失败
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("插入表错误"),query.lastError().text());
        return  ;
    }
    else
    {
        QMessageBox::information(this,tr("插入表成功"),tr("插入记录成功") );
    }
    showAll();


}

void Widget::on_pushButton_showall_clicked()
{
    showAll();
}


void Widget::on_pushButton_delete_clicked()
{
    // 创建数据库的表
    // 执行一个sql语句 , 使用qsqlquery
    QString sql = tr("delete from student where number = %0;")
                    .arg(ui->lineEdit_number->text().toInt());

    QSqlQuery query; // 创建一个执行sql语句的对象
    bool ok =  query.exec(sql);
    if(!ok) // 执行sql 语句失败
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("删除记录错误"),query.lastError().text());
        return  ;
    }
    else
    {
        QMessageBox::information(this,tr("删除成功"),tr("删除记录成功") );
    }
    showAll();

}


void Widget::on_pushButton_modify_clicked()
{
    // 创建数据库的表
    // 执行一个sql语句 , 使用qsqlquery
    // update student set number=4,score=60 where number=5;
    QString sql = tr("update student set name='%1',gender='%2',age=%3,score=%4 where number=%5;")

                    .arg(ui->lineEdit_name->text())
                    .arg(ui->comboBox_gender->currentText())
                    .arg(ui->lineEdit_age->text().toInt())
                    .arg(ui->lineEdit_score->text().toFloat())
                    .arg(ui->lineEdit_number->text().toInt())  ;

    QSqlQuery query; // 创建一个执行sql语句的对象
    bool ok =  query.exec(sql);
    if(!ok) // 执行sql 语句失败
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("修改记录错误"),query.lastError().text());
        return  ;
    }
    else
    {
        QMessageBox::information(this,tr("修改成功"),tr("修改记录成功") );
    }
    showAll();

}


void Widget::on_pushButton_search_clicked()
{
    // 创建数据库的表
    // 执行一个sql语句 , 使用qsqlquery
    // select * from student where number=1;
    QString sql = tr("select * from student where number=%0;")
                    .arg(ui->lineEdit_number->text().toInt()) ;

    QSqlQuery query; // 创建一个执行sql语句的对象
    bool ok =  query.exec(sql);
    if(!ok) // 执行sql 语句失败
    {
        qDebug()<<query.lastError().text();
        QMessageBox::critical(this,tr("删除记录错误"),query.lastError().text());
        return  ;
    }
    else
    {

        // 下一个记录是否有效, 有效返回为真, 无效返回为假
        if(query.next()) // 为真表示找到了记录
        {
            qDebug()<<"number="<<query.value(0).toInt();  // 第1列的值
            qDebug()<<"name  ="<<query.value(1).toString();  // 第2列的值
            qDebug()<<"gender="<<query.value(2).toString();  // 第3列的值
            qDebug()<<"age   ="<<query.value(3).toInt();  // 第4列的值
            qDebug()<<"score ="<<query.value(4).toFloat();  // 第5列的值

            model->removeRows(0,lineCount);  // 从0行 一直删除到 指定的行
            lineCount = 0 ; // 行号清空
            /*设置一条数据*/
            model->setItem(lineCount, 0, new QStandardItem(query.value(0).toString())  );
            model->setItem(lineCount, 1, new QStandardItem(query.value(1).toString()) );
            model->setItem(lineCount, 2, new QStandardItem(query.value(2).toString()) );
            model->setItem(lineCount, 3, new QStandardItem(query.value(3).toString()) );
            model->setItem(lineCount, 4, new QStandardItem(query.value(4).toString()) );

            model->item(lineCount, 0)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 1)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 2)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 3)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            model->item(lineCount, 4)->setTextAlignment(Qt::AlignCenter); // 设置文本居中对齐
            ui->tableView->setModel(model) ; // 给tableview 安装一个模型 model
            lineCount ++;
        }
        else
        {
            QMessageBox::information(this,tr("查找成功"),tr("没有找到记录") );
            model->removeRows(0,lineCount);  // 从0行 一直删除到 指定的行
            lineCount = 0 ; // 行号清空
            return ;
        }

    }

}

