﻿#ifndef WIDGET_H
#define WIDGET_H
#pragma execution_character_set("utf-8")
#include <QWidget>
#include <QSqlDatabase>
#include <QDebug>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlError>
#include <QStandardItemModel>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget(); // 构造函数
    void showAll();// 显示数据库中所有的成员信息

private slots:
    void on_pushButton_create_clicked();

    void on_pushButton_insert_clicked();

    void on_pushButton_showall_clicked();

    void on_pushButton_delete_clicked();

    void on_pushButton_modify_clicked();

    void on_pushButton_search_clicked();

private:
    Ui::Widget *ui;
    QSqlDatabase db ; // 创建数据库对象

    QStandardItemModel  * model ;     // QStandardItemModel 这是一个标准的model/view的框架

    qint32 lineCount = 0 ; // 要插入记录的行号
};
#endif // WIDGET_H
