﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma execution_character_set("utf-8")//解决中文问题
#include <QMainWindow>
#include <QSerialPort>

#include <QSettings>
#include <QSerialPortInfo>
#include <QDebug>
#include <QMessageBox>
#include <QQueue>
#include <QTimer>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void servial_readyRead_slot();// 串口收到数据后, 执行这个函数来处理数据

    void on_pushButton_open_clicked(bool checked);

    void servial_time_slot();//定时时间到了的信号处理函数


    void on_pushButton_led1_clicked(bool checked);

    void on_pushButton_led2_clicked(bool checked);

    void on_pushButton_beep_clicked(bool checked);

    void on_pushButton_refresh_clicked();

private:
    Ui::MainWindow *ui;

    QSerialPort *serial=nullptr;
    QQueue<char> serialQueue;
    QTimer *serialTimer=nullptr;
};
#endif // MAINWINDOW_H
