﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    serial = new QSerialPort;//这个类定义一个对象
    connect(serial,&QSerialPort::readyRead,this,&MainWindow::servial_readyRead_slot) ;
    QList<QSerialPortInfo> serialPorts = QSerialPortInfo::availablePorts(); // 获取系统有效的串口
    foreach (QSerialPortInfo port, serialPorts)// 遍历所有的serialPorts 中所有的成员
    {
        qDebug()<<"port="<<port.portName();
        qDebug()<<"port="<<port.description();
        //ui->comboBox_com->addItem(port.portName()+"#"+port.description(),QVariant());
        ui->comboBox_com->addItem(port.portName()+" #"+port.description(),QVariant(port.portName()));
    }
    //定义一个定时器
    serialTimer=new QTimer;
    connect(serialTimer,&QTimer::timeout,this,&MainWindow::servial_time_slot);

    //绘制背景图片
    QPalette PAllbackground=this->palette();
    QImage ImgAllbackground(QString::fromUtf8(":/images/pack.jpg"));
    QImage pix=ImgAllbackground.scaled(this->width(),this->height(),
                                       Qt::IgnoreAspectRatio);
    PAllbackground.setBrush(QPalette::Window,QBrush(pix));
    this->setPalette(PAllbackground);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::servial_time_slot()
{
    //每个一秒发送一次全部传感器命令
    static quint32 count=0;
    count ++;
    if(count>=4)//一秒计时时间到
    {
        count=0;
        QString cmd="a7ba8ba9baAbaBbaCbaDb";
        if(serial!=nullptr)
        {
            serial->write(cmd.toUtf8());
        }

    }

    while(!serialQueue.isEmpty())
    {
        if(serialQueue.dequeue()=='c')//取第一个数
        {
            char s0,s1,s2;
            if(serialQueue.isEmpty())  return;
            s0=serialQueue.dequeue();
            if(s0 == '1')
            {
                if(serialQueue.isEmpty())  return;//每一次取词判断队列是否为空，为空立即返回
                s1=serialQueue.dequeue();
                if(serialQueue.isEmpty())  return;
                s2=serialQueue.dequeue();

                if(QString(s1)+QString(s2)=="sd")//led1 亮灯成功
                {
                    ui->pushButton_led1->setIcon(QIcon(QString::fromUtf8(":/images/ledon.png")));
                    qDebug()<<"led1 on ";
                }
                else if(QString(s1)+QString(s2)=="fd")
                {
                    qDebug()<<"led1 on failure";
                }
            }
            else if(s0 == '2')//led1 亮灯失败
            {
                if(serialQueue.isEmpty())  return;
                s1=serialQueue.dequeue();
                if(serialQueue.isEmpty())  return;
                s2=serialQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")
                {
                    ui->pushButton_led1->setIcon(QIcon(QString::fromUtf8(":/images/ledoff.png")));
                    qDebug()<<"led1 on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//led1 亮灯失败
                {
                    qDebug()<<"led1 on failure";
                }
            }
            else if(s0 == '3')//led2 亮灯
            {
                if(serialQueue.isEmpty())  return;
                s1=serialQueue.dequeue();
                if(serialQueue.isEmpty())  return;
                s2=serialQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")//蜂鸣器响
                {
                    ui->pushButton_led2->setIcon(QIcon(QString::fromUtf8(":/images/ledon.png")));
                    qDebug()<<"led2 on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//蜂鸣器不响
                {
                    qDebug()<<"led2 on failure";
                }
            }
            else if(s0 == '4')//led2 亮灯失败
            {
                if(serialQueue.isEmpty())  return;
                s1=serialQueue.dequeue();
                if(serialQueue.isEmpty())  return;
                s2=serialQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")//蜂鸣器响
                {
                    ui->pushButton_led2->setIcon(QIcon(QString::fromUtf8(":/images/ledoff.png")));
                    qDebug()<<"led2 on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//蜂鸣器不响
                {
                    qDebug()<<"led2 on failure";
                }
            }
            else if(s0 == '5')//蜂鸣器响
            {
                if(serialQueue.isEmpty())  return;
                s1=serialQueue.dequeue();
                if(serialQueue.isEmpty())  return;
                s2=serialQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")//蜂鸣器响
                {
                    ui->pushButton_beep->setIcon(QIcon(QString::fromUtf8(":/images/beepon.png")));
                    qDebug()<<"beep on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//蜂鸣器不响
                {
                    qDebug()<<"beep on failure";
                }
            }
            else if(s0 == '6')//蜂鸣器不响
            {
                if(serialQueue.isEmpty())  return;
                s1=serialQueue.dequeue();
                if(serialQueue.isEmpty())  return;
                s2=serialQueue.dequeue();
                if(QString(s1)+QString(s2)=="sd")//蜂鸣器响
                {
                    ui->pushButton_beep->setIcon(QIcon(QString::fromUtf8(":/images/beepoff.png")));
                    qDebug()<<"beep on ";
                }
                else if(QString(s1)+QString(s2)=="fd")//蜂鸣器不响
                {
                    qDebug()<<"beep on failure";
                }
            }
            else if(s0 == 'A')//锂电池电压
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serialQueue.isEmpty()) && ((s1=serialQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_BAT->display(data);

            }
            else if(s0 == 'B')//温度
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serialQueue.isEmpty()) && ((s1=serialQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_tem->display(data);

            }
            else if(s0 == 'C')//湿度
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serialQueue.isEmpty()) && ((s1=serialQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_hum->display(data);

            }
            else if(s0 == 'D')//光线强度
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serialQueue.isEmpty()) && ((s1=serialQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_LX->display(data);

            }
            else if(s0 == 'E')//大气压
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serialQueue.isEmpty()) && ((s1=serialQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_air->display(data);

            }
            else if(s0 == 'F')//电位器
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serialQueue.isEmpty()) && ((s1=serialQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_VR->display(data);

            }
            else if(s0 == 'G')//cpu温度
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serialQueue.isEmpty()) && ((s1=serialQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lcdNumber_CPU->display(data);

            }
            else if(s0 == 'H')//RFID
            {
                QString data="";
                //队列不为空且接到‘d'
                while((!serialQueue.isEmpty()) && ((s1=serialQueue.dequeue()) !='d'))
                {
                    data += QString(s1);
                }
                ui->lineEdit_entrance->setText(data);

            }


        }

    }





}
//串口收到数据后的信号处理函数
void MainWindow::servial_readyRead_slot()
{
    if(serial != nullptr)
    {
        QByteArray data=serial->readAll();
        for (int i = 0; i < data.count(); i++)
        {
            serialQueue.enqueue(char(data.at(i))) ;//入队
        }
        qDebug()<<"data"<<data;
    }
}

void MainWindow::on_pushButton_open_clicked(bool checked)
{
    qDebug()<<"checked="<<checked ;
    if(checked)
    {
        // 检查combox 中是否有内容 ,  只有combox 不为空时, 才可以正常操作串口
        if( ui->comboBox_com->count() > 0 ) // 里面有内容
        {
            // 打开串口
            qDebug()<<"data:"<<ui->comboBox_com->currentData().toString() ;
            serial->setPortName(ui->comboBox_com->currentData().toString()); // 要打开com3 或则com4

            // 判断串口是否被打开, 如果打开就关闭串口, 重新打开
            if(serial->isOpen())
            {
                serial->close() ; // 关闭串口
            }
            if(serial->open(QIODevice::ReadWrite) ) // 成功返回真 , 失败为假
            {
                // 切换pushButton_open的图片为开状态图片 open.png
                ui->pushButton_open->setIcon(QIcon(QString::fromUtf8(":/images/open.png")));
                ui->pushButton_open->setText(tr("关闭"));
    //            ui->groupBox_stm->show(); // show stm system
    //            stm32ControlFlag = 1; //可以和stm32进行通信
                serial->setBaudRate(QSerialPort::Baud115200);//设置波特率
                serial->setDataBits(QSerialPort::Data8);//设置数据位8
                serial->setParity(QSerialPort::NoParity); //校验位设置 NONE
                serial->setStopBits(QSerialPort::OneStop);//停止位设置为1
                serial->setFlowControl(QSerialPort::NoFlowControl);//设置为无流控制

                // 串口成功打开后, 启动定时器开始计时
                serialTimer->start(500) ;  // 启动定时器, 定时时间1秒


            }else{
                ui->pushButton_open->setChecked(false); // 把按钮设置位弹起状态
                QMessageBox::critical(this,tr("串口打开失败"),tr("串口不存在或被其他程序占用,请关闭其他进程再重试"));

                return ;
            }

        }
        else // combox   没有内容,
        {
            ui->pushButton_open->setChecked(false); // 把按钮设置位弹起状态
            QMessageBox::critical(this,tr("串口打开失败"),tr("串口不存在或被其他程序占用,请关闭其他进程再重试"));
        }


    }
    else
    {
        // 切换pushButton_open的图片为开状态图片 close.png
        ui->pushButton_open->setIcon(QIcon(QString::fromUtf8(":/images/close.png")));
        ui->pushButton_open->setText(tr("打开"));


        // 关闭串口后 , 关闭定时器
        serialTimer->stop() ;  // 关闭定时器
    }



}




void MainWindow::on_pushButton_led1_clicked(bool checked)
{
    QString cmd="";
    if(checked)//为真开灯
    {
        cmd="a1b";
    }
    else//为假关灯
    {
        cmd="a2b";
    }
    if(serial !=nullptr)
    {
        serial->write(cmd.toUtf8());
    }

}


void MainWindow::on_pushButton_led2_clicked(bool checked)
{
    QString cmd="";
    if(checked)//为真开灯
    {
        cmd="a3b";
    }
    else//为假关灯
    {
        cmd="a4b";
    }
    if(serial !=nullptr)
    {
        serial->write(cmd.toUtf8());
    }
}

void MainWindow::on_pushButton_beep_clicked(bool checked)
{
    QString cmd="";
    if(checked)//为真响
    {
        cmd="a5b";
    }
    else//为假不响
    {
        cmd="a6b";
    }
    if(serial !=nullptr)
    {
        serial->write(cmd.toUtf8());
    }
}

//刷新
void MainWindow::on_pushButton_refresh_clicked()
{
    ui->comboBox_com->clear();//清除combox 内容
    QList<QSerialPortInfo> serialPorts = QSerialPortInfo::availablePorts(); // 获取系统有效的串口
    foreach (QSerialPortInfo port, serialPorts)// 遍历所有的serialPorts 中所有的成员
    {
        qDebug()<<"port="<<port.portName();
        qDebug()<<"port="<<port.description();
        //ui->comboBox_com->addItem(port.portName()+"#"+port.description(),QVariant());
        ui->comboBox_com->addItem(port.portName()+" #"+port.description(),QVariant(port.portName()));
    }
}

