#include <stdio.h>
int main(int argc, char const *argv[])
{
    //这个二维数组一共有7行10列，一共70个元素，每一个元素都是char类型的，这个数组占用70个字节
    /*week[0]    把二维数组降维理解成一个一维数组，week中有7个元素，分别是week[0]到week[6]
     *m每一个元素又有10个子元素，也就是week[0]中有10个子元素，每一个元素都是char类型的
     *week[0]占用了10个字节*/
    char week[][10] = {"Mon", "Tue", "Wed", "Thur", "Fir", "Sat", "Sun"};
    printf("sizeof(week)=%d\n", sizeof(week));
    printf("sizeof(week[0])=%d\n", sizeof(week[0]));
    printf("week[0]=%s\n", week[0]);
    for (int i = 0; i < sizeof(week) / sizeof(week[0]); i++)
    {
        printf("今天是 %s\n", week[i]);
    }

    return 0;
}
