#include <stdio.h>

int main(int argc, char const *argv[])
{
    long long a[60]={0};
    for(int i=0;i<60;i++)
    {
        if(i<2)
        {
            a[i]=1;
        }
        else
        {
            a[i]=a[i-1]+a[i-2];
        }
        printf("第%d月的兔子数量为:%lld\n",i+1,a[i]/100000000);
      
    }
    return 0;
}
