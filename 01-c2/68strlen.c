#include <stdio.h>
#include <string.h>
int main(int argc, char const *argv[])
{
    char s1[] = {'A', '\0', 'B', 'C', '\0', 'D'};
    char s2[] = "\t\v\\\0will\n";
    char s3[] = "\x69\082\n"; //\o82先识别\0
    printf("strlen(s1)=%d\n", strlen(s1));
    printf("strlen(s2)=%d\n", strlen(s2));
    printf("strlen(s3)=%d\n", strlen(s3));

    return 0;
}
