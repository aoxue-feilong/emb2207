#include <stdio.h>
#define N 10
int main(int argc, char const *argv[])
{
    int a[N][N] = {0};

    for (int i = 0; i < N; i++) //控制显示行数
    {
        for (int j = 0; j < N - 1 - i; j++)
        {
            printf("  ");
        }
        for (int j = 0; j <= i; j++) //控制显示列数
        {
            if (j == 0) //最左边的都显示1，表示第0列都是1
            {
                a[i][0] = 1;
            }
            if (i == j) // i=j表示最后一列，也显示1
            {
                a[i][j] = 1;
            }
            if (i >= 2) //第二行以后,即第三行开始
            {
                a[i][j] = a[i - 1][j - 1] + a[i - 1][j]; //同一行不同列
            }
            printf("%3d ", a[i][j]); //最大数是3位数，用3d比较好 ，边试边调整即可
        }
        printf("\n");
    }
    return 0;
}
