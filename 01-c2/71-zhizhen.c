#include <stdio.h>
int main(int argc, char const *argv[])
{
    char a=10;
    short b=10;
    int c=10;
    long d=10;
    float e=10.1;
    double f=10.1;
    printf("&a =%p\n",&a);
    printf("&b =%p\n",&b);
    printf("&c =%p\n",&c);
    printf("&d =%p\n",&d);
    printf("&e =%p\n",&e);
    printf("&f =%p\n",&f);
    char str[10]={0};
    for (int  i = 0; i < 10; i++)
    {
      printf("&str[%d]=%p\n",i,&str[i]);  
    }
    
    return 0;
}
