#include <stdio.h>
#include <string.h>
int main(int argc, char const *argv[])
{
    char str1[100] = {0};
    char str2[50] = {0};
    printf("请输入第一个字符串>");
    scanf("%s", str1);
    printf("请输入第二个字符串>");
    scanf("%s", str2);
    printf("str1=%s\n", str1);
    printf("str2=%s\n", str2);
    //*strcpy(str1,str2);//把str2复制到str1,str1==str2,'\o'会被一同复制
    int i = 0, j = 0;
    while (1)
    {
        str1[i] = str2[j];
        if (str1[i] == '\0')
            break;
        i++;
        j++;
    }
    // while((str1[i++]=str2[j++]!='\0))优化后的程序
    printf("str1=%s\n", str1);
    printf("str2=%s\n", str2);
}