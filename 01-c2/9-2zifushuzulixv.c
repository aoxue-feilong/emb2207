#include <stdio.h>
int main(int argc, char const *argv[])
{
    char str1[100] = {0};

    printf("请输入一个字符串 >");
    scanf("%s", str1); //只有%s后面，地址符号不用，数组名本身就是地址
    printf("str1=%s\n", str1);
    int len = 0;
    while (str1[len] != '\0') //如果buf[len]=='0',是字符串结束的标志
    {
        len++;
    }
    printf("len=%d\n", len); //知道字符数组里面有多少个元素
    printf("逆序为>");
    char t;
    for (int i = 0; i < len / 2; i++)
    {
        t = str1[i];
        str1[i] = str1[len - 1 - i]; //第一个数和最后一个数交换
        str1[len - 1 - i] = t;
    }
    printf("str1=%s\n", str1);
    return 0;
}
