#include <iostream>
#include <string>
#include <vector>

//向量： vector 实现的是一个动态数组，即可以进行元素的插入和删除

using namespace std;

class student
{
private:
    int number;
    string name;
    int age;
    float score;

public:
    student()
    {
        number = 0;
        name = "";
        age = 0;
        score = 0;
    }
    student(int num, string n, int a, float s)
    {
        number = num;
        name = n;
        age = a;
        score = s;
    }
    ~student()
    {
    }
    // 重载 ostream 就是cout
    // cout<< student 会调用如下代码
    friend ostream &operator<<(ostream &os, const student s)
    {
        os << "|" << s.number << "|" << s.name << "|" << s.age << "|" << s.score << "|" << endl;
    }
    int operator==(const student s)
    {
        if (this->number == s.number)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    int operator!=(const student s)
    {
        if (this->number != s.number)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
};

int main(int argc, char const *argv[])
{

    vector<student> v1;
/************************插入一个类**********************************/
    for (int i = 0; i < 10; i++)
    {
        v1.push_back(student(i + 1, "student" + to_string(i + 1), 22 + i, 80 + i));
    }
    cout << "容器内容为:" << endl;
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i];
    }
/************************删除**********************************/
    v1.erase(v1.begin() + 9); // v1.begin()+9  是第10个元素的位置
    v1.erase(v1.begin() + 4); // v1.begin()+4  是第5 个元素的位置
    v1.erase(v1.begin() + 0); // v1.begin()+0  是第1 个元素的位置
    cout << "容器内容为:" << endl;
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i];
    }
/************************修改**********************************/
    v1[6] = student(66, "student66", 22, 88); //  访问第7个位置
    v1.at(0) = student(99, "student99", 24, 78);
    // 访问第1个位置
    cout << "容器内容为:" << endl;
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i];
    }
/************************查找**********************************/
    for (int i = 0; i < v1.size(); i++)
    {
        if (v1[i] == student(66, "student66", 22, 88))
        {
            cout << "student66 found" << endl;
            break;
        }
        // i == v1.size()-1 , i的值已经到了最后一个元素的位置
        // v1[i] !=99 最后一个元素不等于99
        if ((v1[i] != student(66, "student66", 22, 88)) && (i == v1.size() - 1))
        {
            cout << "student66 not found" << endl;
        }
    }
    for (int i = 0; i < v1.size(); i++)
    {
        if (v1[i] == student(100, "student100", 22, 88))
        {
            cout << "student100 found" << endl;
            break;
        }
        // i == v1.size()-1 , i的值已经到了最后一个元素的位置
        // v1[i] !=99 最后一个元素不等于99
        if ((v1[i] != student(100, "student6", 22, 88)) && (i == v1.size() - 1))
        {
            cout << "student100 not found" << endl;
        }
    }

    return 0;
}