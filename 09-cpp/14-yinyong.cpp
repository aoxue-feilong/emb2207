#include <iostream>

using namespace std;

// 交换a和b的值
// 赋值传参
// 形参不能修改实参的值
int swap_copy(int a, int b)
{
    int c;
    c = a;
    a = b;
    b = c;
    return 0;
}

// 交换a和b的值
// 地址传参
// 形参可以修改实参的值
int swap_addr(int *a, int *b)
{
    int c;
    c = *a;
    *a = *b;
    *b = c;
    return 0;
}

// 交换a和b的值
// 引用传参 , 目的是为了减少指针的使用, 让程序更简单
// 引用就是实参的别名, 操作形参等价于是操作实参
// 形参可以修改实参的值
int swap_ref(int &a, int &b)
{
    int c;
    c = a;
    a = b;
    b = c;
    return 0;
}

int main(int argc, char const *argv[])
{

    int x = 10, y = 20;
    swap_copy(x, y);
    cout << "1:x=" << x << " y=" << y << endl;

    swap_addr(&x, &y);
    cout << "2:x=" << x << " y=" << y << endl;

    swap_ref(x, y);
    cout << "3:x=" << x << " y=" << y << endl;
    return 0;
}
