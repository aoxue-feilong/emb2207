#include <iostream>
#include <string>

using namespace std;


class object
{
private:
    /* data */
public:
    object(/* args */);
    ~object();
    virtual int area() = 0  ; // 这个函数就是纯虚函数, 对应的类为抽象类, 也叫纯虚类
};

object::object(/* args */)
{
}

object::~object()
{
}


class circle : public object
{
    #define  PI  3.1415926
private:
    double r ; 
public:
    circle(double rr);
    ~circle();
    int area() // 这个函数是纯虚函数 , 因为基类有同名函数 
    {
        cout<<"圆的面积为:"<< PI *r*r <<endl;
    }
};

circle::circle(double rr)
{
    r = rr ; 
}

circle::~circle()
{
}


class rect:public object 
{
private:
    double height , width;
public:
    rect(double h, double w);
    ~rect();
    int area()   // 这个函数是纯虚函数 , 因为基类有同名函数 
    {
        cout<<"矩形的面积为:"<< height * width <<endl;
    }
};

rect::rect(double h, double w)
{
    height = h ; 
    width = w;
}

rect::~rect()
{
}


int main(int argc, char const *argv[])
{
    
    // new 等价于c语言的malloc
    // 创建一个circle 的对象, 虚函数同名函数子类会覆盖父类的方法 

    object *p = new circle(1) ;
    p->area();  //子类的 area 会覆盖父类的同名函数 

    p = new rect(3,4); 
    p->area(); //子类的 area 会覆盖父类的同名函数 

    return 0;
}