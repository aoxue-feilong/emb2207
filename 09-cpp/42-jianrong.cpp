#include <iostream>
#include <string>

using namespace std;

class B0
{
private:
public:
    B0(/* args */) {}
    ~B0() {}
    int display()
    {
        cout << "B0::display()" << endl;
    }
};

class B1 : public B0
{
private:
public:
    B1(/* args */) {}
    ~B1() {}
    int display()
    {
        cout << "B1::display()" << endl;
    }
};
class D1 : public B1
{
private:
public:
    D1(/* args */) {}
    ~D1() {}
    int display()
    {
        cout << "D1::display()" << endl;
    }
};



int main(int argc, char const *argv[])
{
    B0 b0;   //声明B0类对象
    B1 b1;   //声明B1类对象
    D1 d1;   //声明D1类对象
    B0 *p;   //声明B0类指针
    p = &b0; // B0类指针指向B0类对象
    p->display();   // 会输出 B0::display()

    // 派生对象可以当成基类对象使用, 派生类对象可以赋值给基类指针 , 同名函数, 子类屏蔽父类
    p = &b1; 
    p->display();   // 会输出 B0::display()

    // 派生对象可以当成基类对象使用, 派生类对象可以赋值给基类指针 , 同名函数, 子类屏蔽父类
    p = &d1;  
    p->display();  // B0::display()  


    // 程序运行输出 
    /*
    B0::display()
    B0::display()
    B0::display()   
    */

    B1 *ptr2 = &b1; 
    // 父类和子类同名函数, 子类会屏蔽父类
    ptr2->display(); //  会输出 B1::display()


    D1 *ptr3 = &d1; 
    // 父类和子类同名函数, 子类会屏蔽父类
    ptr3->display() ; // 会输出 D1::display()


    return 0;
}