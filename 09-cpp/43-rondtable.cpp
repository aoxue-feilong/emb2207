#include <iostream>
#include <string>

using namespace std;

class complex
{
private:
    double real, imagic;

public:
    complex(double r, double i);
    ~complex();
    int showComplex();

    // 重载为友元函数时 , 参数个数为 2个 , + 号是双目运算符, 需要2个参数 
    friend complex operator+(complex &com1,complex &com2) ; // 声明为友元函数 
    friend complex operator-(complex &com1,complex &com2) ; // 声明为友元函数 


};

complex::complex(double r, double i)
{
    real = r;
    imagic = i;
}

complex::~complex()
{
}
int complex::showComplex()
{
    cout << "(" << this->real << "," << this->imagic << ")" << endl;
    return 0; 
}

// 重载 + 运算符 为友元函数 
complex operator+(complex &com1,complex &com2)  // 声明为友元函数 
{
    complex com3(0,0)  ; 
    com3.real = com1.real + com2.real ; 
    com3.imagic  = com1.imagic + com2.imagic ; 
    return com3 ; 
}

// 重载 - 运算符 为友元函数 
complex operator-(complex &com1,complex &com2)  // 声明为友元函数 
{
    complex com3(0,0)  ; 
    com3.real = com1.real - com2.real ; 
    com3.imagic  = com1.imagic - com2.imagic ; 
    return com3 ; 
}

int main(int argc, char const *argv[])
{
    complex com1(1,-5),com2(3,8); 

    // 把 + 号理解为函数  , +(com1,com2)
    // com3 =  (operator+)(com1,com2)
    complex com3 = com1 + com2 ;  
    com1.showComplex(); 
    com2.showComplex(); 
    cout<<"com1 + com2 = " ; 
    com3.showComplex() ; 

    com3 = com1 - com2 ;  
    cout<<"com1 - com2 = " ; 
    com3.showComplex() ; 
    return 0;
}