#include <iostream>
#include <string>
#include <vector>

//向量： vector 实现的是一个动态数组，即可以进行元素的插入和删除

using namespace std;
template <class T>
bool search(vector<T> &v1, T value)
{
    for (auto it = v1.begin(); it != v1.end(); it++)
    {
        if (*it == value)
        {
            return true;
        }
    }
    return false;
}
int main(int argc, char const *argv[])
{
    string citys[] = {"Beijing", "Shanghai", "Shenzhen", "Guangzhou", "Hangzhou",
                      "Suzhou", "Tianjin", "Chengdu", "Chongqing", "Nanjing"};
    vector<string> v1;
/************************插入**********************************/
    for (int i = 0; i < 10; i++)
    {
        v1.push_back(citys[i]);//尾插法
    }
    cout << "插入顺序表内容为:";
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    cout << endl;
/************************删除**********************************/
    // v1.begin()  是第1个元素的位置
    v1.erase(v1.begin() + 9); // v1.begin()+9  是第10个元素的位置
    v1.erase(v1.begin() + 4); // v1.begin()+4  是第5 个元素的位置
    v1.erase(v1.begin() + 0); // v1.begin()+0  是第1 个元素的位置
    cout << "删除顺序表内容为:";
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    cout << endl;
/************************修改**********************************/
    v1[6] = "Wuhan";    //  访问第7个位置
    v1.at(0) = "Chongqing"; // 访问第1个位置
    cout << "修改顺序表内容为:";
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    cout << endl;
/************************查找**********************************/
  if (search(v1, string("Beijing")))
    {
        cout << "Beijing found\n";
    }
    else
    {
        cout << "Beijing not found\n";
    }
    if (search(v1, string("Shenzhen")))
    {
        cout << "Shenzhen found\n";
    }
    else
    {
        cout << "Shenzhen not found\n";
    }
    return 0;
}