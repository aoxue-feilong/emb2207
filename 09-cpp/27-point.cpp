#include <iostream>
#include <string>
using namespace std; 
// 这种情况下就需要前向引用声明 
class classA ; // 这就是类的声明 

class classB
{
private:
    // error: field ‘a’ has incomplete type ‘classA’
    // 使用前向引用声明虽然可以解决一些问题，但它并不是万能的。需要注意的是，
    // 尽管使用了前向引用声明，但是在提供一个完整的类声明之前，不能声明该类的对象
    // classA a; // 错误用法 
    classA *pa ; // 正确用法是定义指针类型的即可 
    
    /* data */
public:

};

class classA
{
private:
    classB b; // 未定义的引用 
    /* data */
public:

};

int main(int argc, char const *argv[])
{
    /* code */
    return 0;
}
