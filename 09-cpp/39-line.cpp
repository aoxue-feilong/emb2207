#include <iostream>
#include <string>
#include <math.h>

using namespace std;

class point
{
private:
    int x, y;
public:
    point(){}
    ~point(){}
    int getX()
    {
        return x;
    }
    int getY()
    {
        return y;
    }
    int setX(int xx)
    {
        x = xx;
    }
    int setY(int yy)
    {
        y = yy;
    }
    int showPoint()
    {
        cout << "(" << x << "," << y << ")" << endl;
    }
};
class line
    {
    private:
        point p1, p2;

    public:
        line(int x1, int y1, int x2, int y2)
        {
            p1.setX(x1);
            p1.setY(y1);
            p2.setX(x2);
            p2.setY(y2);
        }
        ~line(){}
        int showLine()
        {
            p1.showPoint();
            p2.showPoint();
        }
        int distance()
        {
            float lx=(p2.getX()-p1.getX())*(p2.getX()-p1.getX());
            float ly=(p2.getY()-p1.getY())*(p2.getY()-p1.getY());
            cout << "两点之间的距离为:" << sqrt(lx + ly) << endl;
        }
    };
int main(int argc, char const *argv[])
{
  line l1(1,1,5,5);
  l1.showLine();
  l1.distance();
    return 0;
}
