#include <iostream>
#include <string>
#include <deque>

using namespace std;

int main(int argc, char const *argv[])
{
    deque<int> d1; 
    cout<< "d1.size()="<<d1.size()<<endl;
/************************入栈**********************************/
    cout<<"入栈顺序:";
    for(int i=0;i<10;i++)
    {
        d1.push_back(i+1); 
        cout<<i+1<<" ";
    }
    cout<<endl;
    cout<< "d1.size()="<<d1.size()<<endl;
/************************出栈**********************************/
    cout<<"出栈顺序:";
    for(int i=0;i<10;i++)
    {
        cout<< d1.back() <<" "; // 获取最后一个元素的值 
        d1.pop_back();// 这个函数只是把最后的一个元素删除, 并不返回 
    }
    cout<<endl;
    cout<< "d1.size()="<<d1.size()<<endl;
    return 0;
}