#include <iostream>
#include <string>
#include <array>
#include <deque>
#include <list>

using namespace std;

int main(int argc, char const *argv[])
{
    list<int> l1; 
    cout<< "l1.size()="<<l1.size()<<endl;
/************************入队**********************************/
    cout<<"双链表入队顺序:";
    for(int i=0;i<10;i++)
    {
        l1.push_back(i+1); 
        cout<<i+1<<" ";
    }
    cout<<endl;
    cout<< "l1.size()="<<l1.size()<<endl;
/************************出队**********************************/ 
    cout<<"双链表出队顺序:";
    for(int i=0;i<10;i++)
    {
        cout<< l1.front() <<" "; // 获取第一个元素的值 
        l1.pop_front();// 这个函数只是把最前的一个元素删除, 并不返回 
    }
    cout<<endl;
    cout<< "l1.size()="<<l1.size()<<endl;




    return 0;
}