#include <iostream>
#include <string>
#include <vector>

//向量： vector 实现的是一个动态数组，即可以进行元素的插入和删除

using namespace std;

int main(int argc, char const *argv[])
{

    /****************************************/
    string citys[] = {"Beijing", "Shanghai", "Shenzhen", "Guangzhou", "Hangzhou",
                      "Suzhou", "Tianjin", "Chengdu", "Chongqing", "Nanjing"};
    vector<string> v1;
/************************插入**********************************/
    for (int i = 0; i < 10; i++)
    {
        v1.push_back(citys[i]);//尾插法
    }
    cout << "链表内容为:";
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    cout << endl;
/************************删除**********************************/
    // v1.begin()  是第1个元素的位置
    v1.erase(v1.begin() + 9); // v1.begin()+9  是第10个元素的位置
    v1.erase(v1.begin() + 4); // v1.begin()+4  是第5 个元素的位置
    v1.erase(v1.begin() + 0); // v1.begin()+0  是第1 个元素的位置
    cout << "链表内容为:";
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    cout << endl;
/************************修改**********************************/
    v1[6] = "Wuhan";    //  访问第7个位置
    v1.at(0) = "Chongqing"; // 访问第1个位置
/************************查找**********************************/
    for (int i = 0; i < v1.size(); i++)
    {
        if (v1[i] == "Shanghai")
        {
            cout << "Shanghai found" << endl;
            break;
        }
        // i == v1.size()-1 , i的值已经到了最后一个元素的位置
        // v1[i] !=99 最后一个元素不等于99
        if ((v1[i] != "Shanghai") && (i == v1.size() - 1))
        {
            cout << "Shanghai not found" << endl;
        }
    }
    for (int i = 0; i < v1.size(); i++)
    {
        if (v1[i] == "Harbin")
        {
            cout << "Harbin found" << endl;
            break;
        }
        // i == v1.size()-1 , i的值已经到了最后一个元素的位置
        // v1[i] !=99 最后一个元素不等于99
        // v1[v1.size()-1] != 99
        if ((v1[i] != "Harbin") && (i == v1.size() - 1))
        {
            cout << "c not found" << endl;
        }
    }

    return 0;
}