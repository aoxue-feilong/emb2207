#include <iostream>
#include <string>

using namespace std;

class B0
{
private:
public:
    B0(/* args */) {}
    ~B0() {}
    int display()
    {
        cout << "B0::display()" << endl;
    }
};

class B1 : public B0
{
private:
public:
    B1(/* args */) {}
    ~B1() {}
};
class B2 : public B0
{
private:
public:
    B2(/* args */) {}
    ~B2() {}
};

class D1 : public B1 , public B2
{
private:
    /* data */
public:
    D1()
    {

    }
    ~D1()
    {

    }
};


int main(int argc, char const *argv[])
{

    D1 d1; 
    d1.display(); 
    

    return 0;
}