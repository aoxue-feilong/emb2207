// C++ 中 可以省略.h 的书写方式 
// iostream input output stream 
#include <iostream>


int main(int argc, char const *argv[])
{
    bool flag  = true; // 设置flag 是一个布尔变量 , 可以为true 和false 
    // 真为1  
    std::cout <<"flag ="<<flag <<std::endl ; //输出值 flag = 1 , 和c语言保持一直
    flag = false ; 
    // 假为0 
    std::cout <<"flag ="<<flag <<std::endl ; //输出值 flag = 0 , 和c语言保持一直
    return 0;
}
