#include <iostream>
#include <string>

using namespace std;

#define PI 3.1415926
class circle
{
private:
    float r; // 圆的半径
public:
    circle()
    {
        r = 0;
    }
    circle(float R)
    {
        r = R; 
    }
    ~circle()
    {

    }
    float area()
    {
        return  (PI * r * r) ;
    }
};


class table
{
private:
    float height  ; // 桌子的高度 
    string color  ;// 桌子的颜色 
public:
    table()
    {
        this->height = 0 ; 
        this->color = "White"; 
    }
    table(float h,string col)
    {
        height = h ; 
        color = col ; 
    }
    ~table()
    {

    }
    float getH() // 获取桌子的高度 
    {
        return height ; 
    }
    string getColor()
    {
        return color; 
    }
    
};

// 多继承 
class roundtable : public circle , public table 
{
private:
   
public:
    roundtable()
    {

    }
    // 带参数的构造函数 
    // : 基类名(参数) 
    roundtable(float R,float h, string col):circle(R),table(h,col)
    {

    }
    ~roundtable()
    {

    }
    int showInfo()
    {
        cout<<"圆桌的面积为:"<<this->area()<<endl;
        cout<<"圆桌的高度为:"<<this->getH()<<endl;
        cout<<"圆桌的颜色为:"<<this->getColor()<<endl;
    }
};





int main(int argc, char const *argv[])
{

    roundtable rt1(1,0.75,"Yellow");
    rt1.showInfo();  
    

    return 0;
}