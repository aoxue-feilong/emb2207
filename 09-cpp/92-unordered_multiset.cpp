#include <iostream>
#include <string>
#include <map>
#include <set>
#include <unordered_set>

using namespace std;

int main(int argc, char const *argv[])
{
    unordered_multiset<string> s1 = {"语文", "数学", "英语", "地理", "历史", "政治"};
    s1.emplace("物理");
    s1.emplace("物理");
    s1.emplace("化学");
    s1.emplace("化学");
    s1.emplace("生物");
    s1.emplace("生物");
    cout << "set内容为:";
    for (auto it = s1.begin(); it != s1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << "\n";
    cout << "set内容为:";
    s1.erase("物理");
    s1.erase("历史");
    s1.erase("生物");
    for (auto it = s1.begin(); it != s1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << "\n";
    if (s1.find("语文") != s1.end())
    {
        cout << "语文找到了" << endl;
    }
    else
    {
        cout << "语文没找到了" << endl;
    }
    if (s1.find("物理") != s1.end())
    {
        cout << "物理找到了" << endl;
    }
    else
    {
        cout << "物理没找到了" << endl;
    }
    return 0;
}
