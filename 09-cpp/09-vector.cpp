#include <iostream>
#include <string>
#include <vector>

using namespace std;
int main(int argc, char const *argv[])
{


    vector<int> v1; 
    for(int i=0;i<100;i++)
    {
        v1.push_back(i); // 向链表中插入100个元素 
    }
    cout<<"v1.size()="<<v1.size() <<endl; // 向量表的大小 

    // 向量表 支持迭代(遍历)的功能
    for(int i:v1) 
    {
        cout<<i<<" "; 
    }
    cout<<endl;

    // for 循环控制 
    for(int i=0;i<v1.size();i++)
    {
        cout<<v1[i]<<" ";  
    }
    cout<<endl;

    // for 循环控制 
    for(int i=0;i<v1.size();i++)
    {
        cout<<v1.at(i)<<" "; // 可以使用索引去访问   
    }
    cout<<endl;
    



    return 0;
}
