#include <iostream>
#include <string>
#include <map>

using namespace std;

int main(int argc, char const *argv[])
{
    //默认表示从小到大排序
    map<int,string> m1;
    string name[]={"li","zhou","zhu","Tian","qing",
                    "yang","Nan","cui","Wu","mao"};
    for (int i = 0; i < 10; i++)
    {
        // m1.insert(quhao[i],citys[i]);//insert（）里面放键值对，这种不支持
        m1.emplace(i+1, name[i]);
    }
    //迭代器  遍历关联容器
    // it->first是key
    // it->second是value
    for (auto it = m1.begin(); it != m1.end(); it++)
    {
        cout << it->first << "\t:" << it->second << endl;
    }
    cout<<"-------------------------------------------\n";
    //std::greater<int>表示从大到小排序
    map<int,string,std::greater<int>> m2;
    for (int i = 0; i < 10; i++)
    {
         m2.insert(pair<int,string>(i+1,name[i]));//insert（）里面放键值对，这种不支持
        //m2.emplace(i+1, name[i]);
    }
    //迭代器  遍历关联容器
    // it->first是key
    // it->second是value
    for (auto it = m2.begin(); it != m2.end(); it++)
    {
        cout << it->first << "\t:" << it->second << endl;
    }
    return 0;
}
