#include <iostream>
#include <string>

using namespace std;

#define PI 3.1415926
class circle
{
private:
    float r; // 圆的半径
public:
    circle()
    {
        r = 0;
    }
    circle(float R)
    {
        r = R;
    }
    ~circle(){}
    float area()
    {
        return  (PI * r * r) ;
    }
    int show()
    {
        cout<<"圆的面积为"<<this->area()<<endl;
    }
};

class table
{
private:
    float height  ; // 桌子的高度 
    string color  ;// 桌子的颜色 
public:
    table()//无参的构造函数
    {
        this->height = 0 ; 
        this->color = "White"; 
    }
    table(float h,string col)//有参的构造函数
    {
        height=h;
        color=col; 
    }
    ~table(){}
    float getH() // 获取桌子的高度 
    {
        return height ; 
    }
    string getColor() // 获取桌子的颜色 
    {
        return color; 
    }
    int show()
    {
        cout<<"桌子的高度"<<this->height<<endl;
        cout<<"桌子的颜色"<<this->color<<endl;
    }
    
};
// 多继承 
class roundtable : public circle , public table 
{
private:
   
public:
    roundtable(){}
    roundtable(float R,float h,string col):circle(R),table(h,col)
    {
    }
    ~roundtable(){}
    int showInfo()
    {
        cout<<"圆桌的面积为:"<<this->area()<<endl;
        cout<<"圆桌的高度为:"<<this->getH()<<endl;
        cout<<"圆桌的颜色为:"<<this->getColor()<<endl;
    }
};
int main(int argc, char const *argv[])
{
    roundtable rt1(1,0.75,"yellow");   
    //rt1.showInfo();
    //rt1.show();//这就是二义性，两个父类中都show，编译器不知道是哪个show，所以报错
    rt1.circle::show();
    cout<<"-----------------------------------\n";
    rt1.table::show();
    return 0;
}