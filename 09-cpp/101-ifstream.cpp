#include <iostream> // in out stream
#include <string>
#include <fstream> // file stream

using namespace std;

int main(int argc, char const *argv[])
{

    // ofstream ios::out|ios::trunc
    // ifstream ios::in
    // fstream 无默认值

    if (argc != 3)
    {
        cout << "错误:运行程序时请带入参数 ./101-ifstream file1 file2" << endl;
        exit(-1);
    }

    // 读一个文件 , 不存在则报错 , 存在就打开
    ifstream file1(argv[1]); // 使用默认值 ios::in
    // 文件打开是否是失败的, 文件打开失败返回为真 , file1.fail(), 文件打开失败返回为真的
    if (file1.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }

    // 写一个文件 , 不存在则创建 , 存在就打开并清空
    ofstream file2(argv[2]); // 使用默认值 ios::out|ios::trunc
    // 文件打开是否是失败的, 文件打开失败返回为真 , file2.fail(), 文件打开失败返回为真的
    if (file2.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }

    char buf[128] = {0};
    // 一次读一行, 遇见'\n'结束输入, 等价于fgets
    // 遇见'\n'结束输入, '\n'不保存
    // 成功返回值读回来的字节数
    // 失败返回 空, 为0
    // while (file1.getline(buf, 128))
    // {
    //     // 必须手动加上 '\n'
    //     file2 << buf << endl; // 把buf的内容输出到file2
    // }

    while (!file1.eof())
    {
        file1.getline(buf, 128);
        if (file1.tellg() > 0) // 获取文件的位置
        {
            // 必须手动加上 '\n'
            file2 << buf << endl; // 把buf的内容输出到file2
        }
    }

    file1.close();
    file2.close();

    return 0;
}