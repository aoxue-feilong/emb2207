#include <iostream>
#include <string>
#include <array>
#include <deque>
#include <list>
#include <forward_list>
#include <vector>
#include <map>
#include <utility>
#include <set>
#include <unordered_set>
#include <stack>
#include <queue>

using namespace std;

int main(int argc, char const *argv[])
{

    queue<int> q1;
    cout << "队列的大小:q1.size()=" << q1.size() << endl;
    cout << "入队顺序:";
    for (int i = 0; i < 10; i++)
    {
        q1.push(i + 1); // 入队元素
        cout << i + 1 << " ";
    }
    cout << endl;
    cout << "队列的大小:q1.size()=" << q1.size() << endl;

    cout << "出队顺序:";
    for (int i = 0; i < 10; i++)
    {
        // 出队列时, 重队列的头部出 , 因此使用front 出队列
        // cout<<q1.back() <<" " ;// 得到队列的最后一个元素的值
        cout << q1.front() << " "; // 得到队列的第一个元素的值

        q1.pop(); // 出队, 会删除队列的第一个元素, 不返回值
    }
    cout << endl;
    cout << "队列的大小:q1.size()=" << q1.size() << endl;
    return 0;
}
