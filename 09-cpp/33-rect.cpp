#include <iostream>
#include <string>

using namespace std;

class point
{
private:
    float x, y;

public:
    point();                   // 无参构造函数
    point(float xx, float yy); // 有参构造函数
    ~point();
    float getX() // 获取点的x坐标
    {
        return x;
    }
    float getY() // 获取点的y坐标
    {
        return y;
    }
    float showPoint() // 显示这个点
    {
        cout << "(" << x << "," << y << ")" << endl;
    }
    float show() // 显示这个点
    {
        cout << "(" << x << "," << y << ")" << endl;
    }
    
};

point::point()
{
    cout<<"point 无参构造函数被调用\n";
    this->x = 0;
    this->y = 0;
}
point::point(float xx, float yy)
{
    cout<<"point 有参构造函数被调用\n";
    this->x = xx;
    this->y = yy;
}

point::~point()
{
}

class rect : public point
{
private:
    float width, height;

public:
    rect();                                     // 无参构造函数
    rect(float xx, float yy, float w, float h); // 有参构造函数
    ~rect();
    int showRect();
    float show() // 显示矩形 
    {
        cout << "(" << getX() << "," << getY() << "," << width << "," << height << ")"<<endl;;
    }
};

// 重点理解的地方 
// 构造派生类对象时, 要先构造基类对象, 无参构造函数不需要给基类传递参数
rect::rect()
{
    cout<<"rect 无参构造函数被调用\n";
    this->width = 0;
    this->height = 0;
}

// 构造派生类对象时, 要先构造基类对象, 
// 有参构造函数, 需要给基类传递参数
/*
派生类名::派生类名(基类所需的形参，本类成员所需的形参):基类名(参数表)
{
        本类成员初始化赋值语句；
};
*/
// : point(xx,yy)  给基类构造函数传递参数

rect::rect(float xx, float yy, float w, float h) :point(xx,yy)
{
    cout<<"rect 有参构造函数被调用\n";
    this->width = w;
    this->height = h;
}

rect::~rect()
{
}
int rect::showRect()
{
    cout << "(" << getX() << "," << getY() << "," << width << "," << height << ")"<<endl;;
}

int main(int argc, char const *argv[])
{

    rect r1(1,2,3,4); 
    r1.showPoint();  // 输出(1,2)
    r1.showRect();   // 输出(1,2,3,4)
    r1.show() ;     //  输出(1,2,3,4) 父类和子类都有show函数,这是子类会屏蔽父类的同名函数  
    return 0;
}
