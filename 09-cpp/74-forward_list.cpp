#include <iostream>
#include <string>
#include <list>
#include <forward_list>
using namespace std;
//单链表只能头部入，头部出
template <class T>
bool search(forward_list<T> &l1, T value)
{
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        if (*it == value)
        {
            return true;
        }
    }
    return false;
}
int main(int argc, char const *argv[])
{
    forward_list<int> l1;
    //cout << "l1.size()=" << l1.size() << endl;
/******************************插入**********************************/
    cout << "插入单链表的顺序：";
    for (int i = 0; i < 10; i++)
    {
        l1.push_front(i + 1);
    }

    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
   // cout << "l1.size()=" << l1.size() << endl;
/******************************删除**********************************/
    cout << "删除单链表的顺序：";
    l1.remove(10);
    l1.remove(4);
    l1.remove(1);
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    //cout << "l1.size()=" << l1.size() << endl;
/******************************修改**********************************/
    cout << "修改单链表的顺序：";
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        if (*it == 9)
        {
            *it = 99;
        }
        if (*it == 2)
        {
            *it = 22;
        }
    }
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
/******************************查找**********************************/
    if (search(l1, 99))
    {
        cout << "99 found\n";
    }
    else
    {
        cout << "99 not found\n";
    }
    if (search(l1, 100))
    {
        cout << "100 found\n";
    }
    else
    {
        cout << "100 not found\n";
    }
/******************************默认排序方式**********************************/
    cout << "单链表的逆序内容为：";
    l1.reverse(); //逆序
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
/******************************默认排序方式**********************************/
    cout << "单链表默认排序的内容为：";
    l1.sort(); //默认排序方式
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    return 0;
}
