#include <iostream>
#include <string>
#include <array>

//c++版本的数组：array 实现的是静态数组（容量固定的数组）：不能对数组里面的元素进行插入和删除
using namespace std;

int main(int argc, char const *argv[])
{
    array<int, 10> a1 = {};
/************************插入int**********************************/
    for (int i = 0; i < a1.size(); i++)
    {
        a1.at(i) = i + 1;
    }
    cout << "array的内容为:";
    for (int i = 0; i < a1.size(); i++)
    {
        cout << a1[i] << " ";
    }
    cout << endl;
/************************插入float**********************************/
    array<float, 10> a2 = {};
    for (int i = 0; i < a2.size(); i++)
    {
        a2.at(i) = i + 1.1;
    }
    cout << "array的内容为:";
    for (int i = 0; i < a2.size(); i++)
    {
        cout << a2[i] << " ";
    }
    cout << endl;
/************************插入string**********************************/
    string citys[] = {"Beijing", "Shanghai", "Shenzhen", "Guangzhou", "Hangzhou",
                      "Suzhou", "Tianjin", "Chengdu", "Chongqing", "Nanjing"};
    array<string, 10> a3 = {};
    for (int i = 0; i < a3.size(); i++)
    {
        a3.at(i) = citys[i];
    }
    cout << "array的内容为:";
    for (int i = 0; i < a3.size(); i++)
    {
        cout << a3[i] << " ";
    }
    cout << endl;

    return 0;
}