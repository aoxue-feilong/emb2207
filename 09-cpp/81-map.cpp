#include <iostream>
#include <string>
#include <map>
#include <utility>

using namespace std;

int main(int argc, char const *argv[])
{
    //STL库：pair有类模板      pair键值对
    //表示每一门的成绩
    pair<string ,float> p1("数学",99);
    pair<string ,float> p2("英语",78);
    pair<string ,float> p3("计算机",100);
    pair<string ,float> p4("单片机",80);
    //first第一个元素  ，   second第二个元素
    cout<<p1.first<<":"<<p1.second<<endl;
    cout<<p2.first<<":"<<p2.second<<endl;
    cout<<p3.first<<":"<<p3.second<<endl;
    cout<<p4.first<<":"<<p4.second<<endl;
    /**************************************/
    map<string,float> m1;
    cout<<"--------------------------------------\n";
    //把p1插进去
    m1.insert(p1);
    m1.insert(p2);
    m1.insert(p3);
    m1.insert(p4);
    //迭代器  遍历关联容器
    for(auto it=m1.begin();it!=m1.end();it++)
    {
        cout<<it->first<<"="<<it->second<<endl;
    }
    return 0;
}
