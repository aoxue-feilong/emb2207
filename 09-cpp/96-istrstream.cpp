#include <iostream>
#include <string>
#include <queue>
#include <strstream>
using namespace std;

int main(int argc, char const *argv[])
{
    int i, j;
    char buf[] = {"1234567890"};
    istrstream s1(buf, 4);
    s1 >> i; // s1当成cin的方式
    istrstream s2(buf, 3);
    s2 >> j; // s2当成cin的方式
    cout << "i=" << i << endl;
    cout << "j=" << j << endl;
    cout << "i+j=" << i + j << endl;
    return 0;
}
