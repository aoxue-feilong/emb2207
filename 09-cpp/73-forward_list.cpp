#include <iostream>
#include <string>
#include <deque>
#include <forward_list>
using namespace std;
//单链表只能头部入，头部出
int main(int argc, char const *argv[])
{
    forward_list<int> l1; 
    //cout<< "l1.size()="<<l1.size()<<endl;没有这个接口 
/************************入栈**********************************/
    cout<<"单链表入栈顺序:";
    for(int i=0;i<10;i++)
    {
        l1.push_front(i+1); 
        cout<<i+1<<" ";
    }
    cout<<endl;
    //cout<< "l1.size()="<<l1.size()<<endl;没有这个接口 
/************************出栈**********************************/
    cout<<"单链表出栈顺序:";
    for(int i=0;i<10;i++)
    {
        cout<< l1.front() <<" "; // 获取第一个元素的值
        l1.pop_front();// 这个函数只是把最前的一个元素删除, 并不返回（移除队头的元素）
    }
    cout<<endl;
    //cout<< "l1.size()="<<l1.size()<<endl;没有这个接口 
    return 0;
}