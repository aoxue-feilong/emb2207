#include <iostream>
#include <string>
#include <stack>

using namespace std;

int main(int argc, char const *argv[])
{
    
    stack<int> s1; 
    cout<<"栈的大小:s1.size()="<<s1.size()<<endl;
    cout<<"入栈顺序:";
    for(int i=0;i<10;i++)
    {
        s1.push(i+1); 
        cout<<i+1<<" " ; 
    }
    cout <<endl;
    cout<<"栈的大小:s1.size()="<<s1.size()<<endl;

    cout<<"出栈顺序:";
    for(int i=0;i<10;i++)
    {
        cout<<s1.top()<<" " ;// 得到栈顶的元素的值
        s1.pop();  // 出栈, 会删除栈顶的元素, 不返回值 
        
    }
    cout <<endl;
    cout<<"栈的大小:s1.size()="<<s1.size()<<endl;
    return 0;
}
