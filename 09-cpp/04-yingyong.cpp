#include <iostream>

int main(int argc, char const *argv[])
{
    int a = 10;
    int &b = a; // a的别名是b
    // int &c ;   // 报错, 声明引用必须被初始化, 否则报错
    std::cout << "1:a=" << a << std::endl;
    std::cout << "1:b=" << a << std::endl;
    a = 20;
    std::cout << "2:a=" << a << std::endl;
    std::cout << "2:b=" << a << std::endl;
    b = 30;
    std::cout << "3:a=" << a << std::endl;
    std::cout << "3:b=" << a << std::endl;

    std::cout << "1:&a=" << &a << std::endl; // 取变量a的地址
    std::cout << "1:&b=" << &b << std::endl; // 取变量b的地址

    return 0;
}
