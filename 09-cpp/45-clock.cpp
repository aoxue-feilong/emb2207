#include <iostream>
#include <string>
#include <unistd.h>

using namespace std;

class Clock
{
private:
    int hour, min, sec;

public:
    Clock()
    {
        hour = 0;
        min = 0;
        sec = 0;
    }
    Clock(int h, int m, int s)
    {
        hour = h;
        min = m;
        sec = s;
    }
    ~Clock()
    {
    }
    int showTime()
    {
        cout << "time " << hour << ":" << min << ":" << sec << endl;
    }

    // 前置单目运算符重载设计 , 无形参
    //  clk1.(operator++)()
    // ++clk1
    void operator++();
    void operator--();

    // 后置单目运算符重载设计 , 有形参
    //  clk1.(operator++)(int)
    // clk1++;
    void operator++(int );
    void operator--(int );


};

void Clock::operator++()
{
    this->sec++;
    if (sec >= 60)
    {
        min++;
        sec = 0;
        if (min >= 60)
        {
            hour++;
            min = 0;
            if (hour >= 24)
            {
                hour = 0;
            }
        }
    }
}

void Clock::operator--()
{
    this->sec--;
    if (sec <= 0)
    {
        min--;
        sec = 59;
        if (min <= 0)
        {
            hour--;
            min = 59;
            if (hour <= 0)
            {
                hour = 23;
            }
        }
    }
}

void Clock::operator++(int)
{
    this->sec++;
    if (sec >= 60)
    {
        min++;
        sec = 0;
        if (min >= 60)
        {
            hour++;
            min = 0;
            if (hour >= 24)
            {
                hour = 0;
            }
        }
    }
}
void Clock::operator--(int)
{
    this->sec--;
    if (sec <= 0)
    {
        min--;
        sec = 59;
        if (min <= 0)
        {
            hour--;
            min = 59;
            if (hour <= 0)
            {
                hour = 23;
            }
        }
    }
}
int main(int argc, char const *argv[])
{
    Clock clk1(9, 3, 10);
    Clock clk2(10,3,10);
    Clock clk3(0,0,0); // 23:59:59
    Clock clk4(0,0,0); // 23:59:59
    clk1.showTime();
    clk2.showTime(); 
    clk3.showTime(); 
    while (1)
    {
        ++clk1; // clk1.(operator++)()
        clk1.showTime();

        clk2++; 
        clk2.showTime(); 

        --clk3;
        clk3.showTime(); 

        clk4--; 
        clk4.showTime(); 
        sleep(1);
    }
    return 0;
}