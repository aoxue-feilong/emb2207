#include <iostream>  // in out stream 
#include <string>
#include <fstream>  // file stream 

using namespace std;

int main(int argc, char const *argv[])
{

    // ofstream ios::out|ios::trunc
    // ifstream ios::in
    // fstream 无默认值

    // 读一个文件 , 不存在则报错 , 存在就打开
    ifstream file1("test1.txt"); // 使用默认值 ios::in
    // file1 想读文件中的内容时, 可以把file1 当成cin  , cin >>  变量

    // 文件打开是否是失败的, 文件打开失败返回为真 , !file1.fail(), 文件打开失败返回为假的
    // !file1.fail(), 文件打开成功返回为真

    string buf,name;
    if (!file1.fail())
    {
        while(!file1.eof() ) // 文件是否到达了文件末尾 
        {
            file1 >> buf;   // 一次读取一个单词  
            
            cout <<buf <<endl;;
            buf=""; // 清空buf 
        }
        file1.close(); // 关闭文件
    }
    else
    {
        cout<<"file2 文件打开失败"<<endl;
    }
    return 0;
}
