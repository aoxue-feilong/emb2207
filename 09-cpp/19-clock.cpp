#include <iostream>
#include <string>

using namespace std; 

class Clock
{
private:
    int hour,min,sec;  // 成员数据 
public:
    // 构造函数 ,和类名一样的函数就是构造函数, 没有返回值, 系统自动调用 
    // 构造函数可以是默认参数的 
    Clock(int h=0,int m=0,int s=0);
    int set_time(int h=0,int m=0,int s=0); // 带默认参数的成员函数  函数声明 
    int show_time(); // 函数声明 

};

Clock::Clock(int h,int m,int s)
{
    cout<<"构造函数被调用\n"; 
    // 构造函数内, 一般设置类对象的初始化操作 
    hour = h ;
    min  = m ; 
    sec = s;  
}

// 带默认形参只能在声明时使用, 在定义时使用会报错 
int Clock::set_time(int h,int m,int s)
{
    hour = h ; //  在成员函数中可以直接访问类内成员数据和成员函数
    min = m ; //  在成员函数中可以直接访问类内成员数据和成员函数
    sec = s ; //  在成员函数中可以直接访问类内成员数据和成员函数
    return 0 ; 
}


int Clock::show_time()
{
    //  在成员函数中可以直接访问类内成员数据和成员函数
    cout<<"HH:MM:SS "<<hour<<":"<<min<<":"<<sec<<endl;
}
int main(int argc, char const *argv[])
{
    // 类定义一个类变量(对象)  , 是直接初始化的方式
    // 定义一个对象, 并用参数初始化这个对象
    Clock clk1(10,32,10); 
    // clk1.hour = 10; 错误用法, 对象.成员 不可以访问private和protected 的成员 
    clk1.show_time(); 

    Clock clk2 ; // 定义一个对象, 不赋初始值, 构造函数会使用默认参数 
    clk2.show_time();        
    return 0;
}
