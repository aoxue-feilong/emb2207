#include <iostream>
#include <string>
#include <vector>

//向量： vector 实现的是一个动态数组，即可以进行元素的插入和删除
using namespace std;

int main(int argc, char const *argv[])
{
    vector<int> v1;
/************************插入**********************************/
    for (int i = 0; i < 10; i++)
    {
        v1.push_back(i + 1);//尾插法
    }
    cout << "链表的内容为：";
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    cout << endl;
/************************删除**********************************/
    v1.erase(v1.begin() + 9);
    v1.erase(v1.begin() + 4);
    v1.erase(v1.begin() + 0);
/************************修改**********************************/
    v1[6] = 99;
    v1.at(0) = 22;
    cout << "链表的内容为：";
    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }
    cout << endl;
/************************查找**********************************/
    for (int i = 0; i < v1.size(); i++)
    {
        if (v1[i] == 99)
        {
            cout << "99 found" << endl;
        }
        if ((v1[i] != 99) && (i == v1.size() - 1))
        {
            cout << "99 not found" << endl;
        }
    }
    for (int i = 0; i < v1.size(); i++)
    {
        if (v1[i] == 5)
        {
            cout << "5 found" << endl;
        }
        if ((v1[i] != 5) && (i == v1.size() - 1))
        {
            cout << "5 not found" << endl;
        }
    }

    return 0;
}
