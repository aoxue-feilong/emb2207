#include <iostream>
#include <string>

using namespace std; 

class Clock
{
private:
    int hour,min,sec;  // 成员数据 
public:
    int set_time(int h=0,int m=0,int s=0); // 带默认参数的成员函数  函数声明 
    int show_time(); // 函数声明 
};

// 带默认形参只能在声明时使用, 在定义时使用会报错 
int Clock::set_time(int h,int m,int s)
{
    hour = h ; //  在成员函数中可以直接访问类内成员数据和成员函数
    min = m ; //  在成员函数中可以直接访问类内成员数据和成员函数
    sec = s ; //  在成员函数中可以直接访问类内成员数据和成员函数
    return 0 ; 
}

int Clock::show_time()
{
    //  在成员函数中可以直接访问类内成员数据和成员函数
    cout<<"HH:MM:SS "<<hour<<":"<<min<<":"<<sec<<endl;
}
int main(int argc, char const *argv[])
{
    // 这个类(clock) 和系统(std)的类重名了, 要改变类的名称
    Clock clk1; // 类定义一个类变量(对象) 
    clk1.set_time() ;// 对象.成员函数  , 可以访问对象内的public的成员
    // clk1.hour = 10; 错误用法, 对象.成员 不可以访问private和protected 的成员 
    clk1.show_time(); 

    clk1.set_time(9,49,10); 
    clk1.show_time(); 
    
    
    return 0;
}
