#include <iostream>
#include <string>
#include <queue>

using namespace std;

int main(int argc, char const *argv[])
{
    //默认排序是从大到小
    priority_queue<int> q1;
    cout << "队列的大小:q1.size()=" << q1.size() << endl;
    int array[] = {100, 20, 23, 2, 43, 65, 3, 456, 213, 78};
    cout << "入队顺序:";
    for (int i = 0; i < 10; i++)
    {
        q1.push(array[i]); // 入队元素
        cout << array[i] << " ";
    }
    cout << endl;
    cout << "队列的大小:q1.size()=" << q1.size() << endl;
    cout << "出队顺序:";
    for (int i = 0; i < 10; i++)
    {
        // 出队列时, 从队列的头部出 , 因此使用front 出队列
        // cout<<q1.back() <<" " ;// 得到队列的最后一个元素的值
        cout << q1.top() << " "; // 得到队列的第一个元素的值

        q1.pop(); // 出队, 会删除队列的第一个元素, 不返回值
    }
    cout << endl;
    cout << "队列的大小:q1.size()=" << q1.size() << endl;

    cout << "------------------------------------------\n";

    priority_queue<int, std::vector<int>, std::greater<int>> q2;

    cout << "队列的大小:q2.size()=" << q2.size() << endl;
    cout << "入队顺序:";
    for (int i = 0; i < 10; i++)
    {
        q2.push(array[i]); // 入队元素
        cout << array[i] << " ";
    }
    cout << endl;
    cout << "队列的大小:q2.size()=" << q2.size() << endl;
    cout << "出队顺序:";
    for (int i = 0; i < 10; i++)
    {
        // 出队列时, 从队列的头部出 , 因此使用front 出队列
        // cout<<q2.back() <<" " ;// 得到队列的最后一个元素的值
        cout << q2.top() << " "; // 得到队列的第一个元素的值

        q2.pop(); // 出队, 会删除队列的第一个元素, 不返回值
    }
    cout << endl;
    cout << "队列的大小:q2.size()=" << q2.size() << endl;
    return 0;
}
