#include <iostream> // in out stream
#include <string>
#include <fstream> // file stream
#include <string.h>

using namespace std;

int main(int argc, char const *argv[])
{

    // ofstream ios::out|ios::trunc
    // ifstream ios::in
    // fstream 无默认值

    if (argc != 3)
    {
        cout << "错误:运行程序时请带入参数 ./102-ifstream file1 file2" << endl;
        exit(-1);
    }

    // 读一个文件 , 不存在则报错 , 存在就打开
    ifstream file1(argv[1]); // 使用默认值 ios::in
    // 文件打开是否是失败的, 文件打开失败返回为真 , file1.fail(), 文件打开失败返回为真的
    if (file1.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }

    // 写一个文件 , 不存在则创建 , 存在就打开并清空
    ofstream file2(argv[2]); // 使用默认值 ios::out|ios::trunc
    // 文件打开是否是失败的, 文件打开失败返回为真 , file2.fail(), 文件打开失败返回为真的
    if (file2.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }

    char buf[128] = {0};
    int n;
    // 是否读到文件末尾
    while (!file1.eof())
    {

        file1.read(buf, 128); // 读指定的字节数
        n = file1.gcount();   // 获取读回来的字节数
        file2.write(buf, n);  // 写指定字节数
    }
    file1.close();
    file2.close();

    return 0;
}