#include <iostream>
#include <string>

using namespace std;

class B0
{
private:
public:
    B0(/* args */) {}
    ~B0() {}
    int display()
    {
        cout << "B0::display()" << endl;
    }
};

class B1 : public B0
{
private:
public:
    B1(/* args */) {}
    ~B1() {}
    int display()
    {
        cout << "B1::display()" << endl;
    }
};
class D1 : public B1
{
private:
public:
    D1(/* args */) {}
    ~D1() {}
    int display()
    {
        cout << "D1::display()" << endl;
    }
};



class X0
{
private:
public:
    X0(/* args */) {}
    ~X0() {}
    // 声明这个函数为虚函数 
    // 虚函数可以继承，当一个成员函数被声明为虚函数后，其派生类中的同名函数都自动成为虚函数
    virtual int display()
    {
        cout << "X0::display()" << endl;
    }
};

class X1 : public X0
{
private:
public:
    X1(/* args */) {}
    ~X1() {}
    int display()
    {
        cout << "X1::display()" << endl;
    }
};
class Z1 : public X1
{
private:
public:
    Z1(/* args */) {}
    ~Z1() {}
    int display()
    {
        cout << "Z1::display()" << endl;
    }
};


int main(int argc, char const *argv[])
{

    B0 b0 ; 
    b0.display() ; // B0::display()

    B1 b1; 
    // B1 继承于B0 , B1和B0 那种都有display函数, 同名函数子类屏蔽父类
    b1.display(); // B1::display()

    D1 d1 ; 
    //D1继承于B1, B1 继承于B0 , D1、B1和B0 都有display函数, 同名函数子类屏蔽父类
    d1.display(); // D1::display()



    X0 x0 ; 
    X0 *ptr = &x0 ; 
    x0.display(); // X0::display()
    ptr->display(); // X0::display()

    X1 x1; 
    // X1 继承于X0 , X1和X0 都有display函数, 因为X0中的display是虚函数, 同名函数自动为虚函数
    // 因此 X1中的display函数也会虚函数 , 同名虚函数子类覆盖父类, 是重写了父类, 
    // 用子类替换父类的代码
    x1.display() ; // X1::display()
    ptr = &x1; 
    ptr->display() ; // X1::display()


    Z1 z1 ; 
    //Z1 继承于X1,  X1 继承于X0 , Z1 , X1和X0 都有display函数, 因为X0中的display是虚函数, 同名函数自动为虚函数
    // 因此 Z1中的display函数也会虚函数 , 同名虚函数子类覆盖父类, 是重写了父类, 
    // 用子类的代码替换父类的代码
    z1.display(); // Z1::display()
    ptr = & z1 ; 
    ptr->display(); // Z1::display()
    return 0;
}