#include <iostream>
#include <string>

using namespace std;

/*
T  是一个可变类型
T 可以是 int , flaot , string
模版函数优先使用指定参数类型的同名函数, 如果没有指定类型的函数，使用模版函数去兼容
*/

template <class T>
T Max(T a, T b)
{
    cout << "T Max(T a, T b)" << endl;
    if (a > b)
        return a;
    else
        return b;
}

// 指定参数类型的同名函数
int Max(int a, int b)
{
    cout << "int Max(int a, int b)" << endl;
    if (a > b)
        return a;
    else
        return b;
}

// 指定参数类型的同名函数
float Max(float a, float b)
{
    cout << "float Max(float a, float b)" << endl;
    if (a > b)
        return a;
    else
        return b;
}



int main(int argc, char const *argv[])
{
    int x = 10, y = 20;
    // 模版函数优先使用指定参数类型的同名函数
    // int Max(int a, int b)
    cout << "1:Max=" << Max(x, y) << endl;

    // 模版函数优先使用指定参数类型的同名函数
    // float Max(float a, float b)
    float m = 10.5, n = 20.6;
    cout << "2:Max=" << Max(m, n) << endl;

    // 如果没有指定类型的函数，使用模版函数去兼容
    string s1 = "good", s2 = "bye";
    cout << "3:Max=" << Max(s1, s2) << endl;

    return 0;
}