#include <iostream>
#include <string>

using namespace std;

// T 是一个可变类型, 通常是int , float , string
// T 也可以可以是一个类

class point
{
private:
    int x, y;

public:
    point(int xx=0 , int yy=0)
    {
        x = xx;
        y = yy;
    }
    ~point()
    {
    }
    void display()
    {
        cout << "(" << x << "," << y << ")" << endl;
    }
    // 重载 ostream 就是cout 
    // cout<< point 会调用如下代码 
    friend ostream & operator <<(ostream &os,const point p)
    {
        cout<<"("<<p.x<<","<<p.y<<")";
    }
};

// set 就是一个集合,  也可以理解为一个数组
template <class T>
class set
{
private:
    T t;

public:
    set(T tt);
    ~set();
    void display();
};

template <class T>
set<T>::set(T tt)
{
    t = tt;
}

template <class T>
set<T>::~set()
{
}

template <class T>
void set<T>::display()
{

    // 如果 t 是一个point 类, 直接输出 cout << point 编译会报错 
    // 解决办法 就是重载 << 这个运算符
    cout << t << endl;
}

int main(int argc, char const *argv[])
{
    set<int> s1(1);
    s1.display();

    // 下面两种写法完全等价
    // set<point> s2 = point(3,4); 
    set<point> s2(point(3,4)); 
    s2.display(); 

    return 0;
}
