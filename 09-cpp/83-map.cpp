#include <iostream>
#include <string>
#include <map>

using namespace std;

int main(int argc, char const *argv[])
{
    //std::greater<string>表示从大到小排序
    map<string,string,std::greater<string>> m1;
    string quhao[]={"010","020","021","022","023","024","025"
                   ,"026","027","028","029"};
    string citys[]={"Beijing","Guangzhou","Shanghai","Tianjin","Chongqing",
                    "Shenyang","Nanjing","Tainei","Wuhan","Chengdu","Xian"};
    for (int i = 0; i < 11; i++)
    {
        // m1.insert(quhao[i],citys[i]);//insert（）里面放键值对，这种不支持
        m1.emplace(quhao[i], citys[i]);
    }
    //迭代器  遍历关联容器
    // it->first是key
    // it->second是value
    for (auto it = m1.begin(); it != m1.end(); it++)
    {
        cout << it->first << "\t:" << it->second << endl;
    }
    return 0;
}
