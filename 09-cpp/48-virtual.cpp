#include <iostream>
#include <string>

using namespace std;

class B0
{
private:
public:
    B0() {}
    ~B0() {}
    virtual int display()
    {
        cout << "B0::display()" << endl;
    }
};

class B1 : public B0
{
private:
public:
    B1() {}
    ~B1() {}
    int display()
    {
        cout << "B1::display()" << endl;
    }
};
class D1 : public B1
{
private:
public:
    D1() {}
    ~D1() {}
    int display()
    {
        cout << "D1::display()" << endl;
    }
};

int main(int argc, char const *argv[])
{
    B0 b0;   //声明B0类对象
    B0 *p;   //声明B0类指针
    p=&b0;
    b0.display();

    B1 b1;   //声明B1类对象
    p=&b1;
    b1.display();

    D1 d1;   //声明D1类对象
    p=&d1;
    d1.display();
    return 0;
}