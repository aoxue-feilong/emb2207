#include <iostream>
#include <string>
#include <math.h>

//类的组合:
using namespace std;
class complex
{
private:
    double real, imagic;

public:
    complex(double r, double i);
    ~complex();
    void showComplex();
    //重载为成员函数
    //参数个数=原操作数-1
    //实际调用的是 com1+com2 ,会变成 com1.(+)(com2)
    complex operator+(complex &c1);
    complex operator-(complex &c2);
};

complex::complex(double r, double i)
{
    real = r;
    imagic = i;
}

complex::~complex()
{
}
void complex::showComplex()
{
    cout << "(" << this->real << "," << this->imagic << ")" << endl;
}

complex complex::operator+(complex &c1)
{
    complex com3(0,0);
    com3.real=this->real+c1.real;
    com3.imagic=this->imagic+c1.imagic;
    return com3;
}
complex complex::operator-(complex &c2)
{
    complex com4(0,0);
    com4.real=this->real-c2.real;
    com4.imagic=this->imagic-c2.imagic;
    return com4;
}
int main(int argc, char const *argv[])
{
    complex com1(1, -5), com2(3, 7);
    // 把 + 号理解为函数  , com1.(operator+)(com2)
    // operator+  是一个函数 , com2 是函数的参数 
    // com3 =  com1.(opertor+)(com2)
    // com1 就变成了类内成员 
    complex com3 = com1 + com2;
    complex com4 = com1 - com2;
    com1.showComplex();
    com2.showComplex();
    cout << "com1 + com2 =" ; 
    com3.showComplex();
    cout << "com1 - com2 =" ;
    com4.showComplex();
    return 0;
}
