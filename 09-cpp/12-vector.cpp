#include <iostream>
#include <string>
#include <vector>

using namespace std;
int main(int argc, char const *argv[])
{

    vector<int> v1{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    vector<string> v2{"beijing", "shanghai", "harbin", "shenzhen", "hangzhou"};

    // 把迭代器当成c语言的指针即可
    // 不确定v1.begin() 的位置是什么类型的, 可以使用auto , 让编译器自动适配类型
    auto p = v1.begin(); // p指向v1链表中第一个元素的位置
    // cout << "p=" << p << endl; 先要打印p的值, 这种是错误用法, 不支持的操作
    cout << "p=" << *p << endl; // *p 取位置中的值

    // 使用迭代器(指针)的方式遍历链表中的所有元素
    for (auto it = v1.begin(); it != v1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;

    // 使用迭代器(指针)的方式遍历链表中的所有元素
    for (auto it = v2.begin(); it != v2.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;

    return 0;
}
