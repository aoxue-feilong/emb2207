#include <iostream>
#include <string>
#include <math.h>

using namespace std;

class point
{
private:
    float x, y;

public:
    // 这就是构造函数的重载 , 就是函数重载
    point();                   // 无参构造函数
    point(float xx, float yy); // 有参构造函数
    ~point();
    // 声明line 是友元类 , line 可以访问point中的私有成员
    friend class line;

    int showPoint()
    {
        cout << "(" << x << "," << y << ")" << endl;
    }
    int setPoint(float xx, float yy)
    {
        this->x = xx;
        this->y = yy;
    }

    // point这个类把 distence 函数当成朋友, 关系是单向的 , distence函数可以访问point中的私有成员
    friend int distance(point &p1, point &p2); // 声明distence 这个函数是一个友元函数
};

point::point()
{
    this->x = 0;
    this->y = 0;
}
point::point(float xx, float yy)
{
    this->x = xx;
    this->y = yy;
}

point::~point()
{
}

int distance(point &p1, point &p2)
{
    float lx = (p2.x - p1.x) * (p2.x - p1.x);
    float ly = (p2.y - p1.y) * (p2.y - p1.y);
    cout << "两点之间的距离为:" << sqrt(lx + ly) << endl;
}

class line
{
private:
    point p1, p2; // 一个线由2个点组成
public:
    line();
    line(float x1, float y1, float x2, float y2);
    ~line();
    int showLine()
    {
        cout << "(" << p1.x << "," << p1.y << ")" << endl;
        cout << "(" << p2.x << "," << p2.y << ")" << endl;
    }
    int distance()
    {
        float lx = (p2.x - p1.x) * (p2.x - p1.x);
        float ly = (p2.y - p1.y) * (p2.y - p1.y);
        cout << "两点之间的距离为:" << sqrt(lx + ly) << endl;
    }
};

line::line(/* args */)
{
    p1.x = 0; // 因为line 是point的友元类, 因此line中可以访问point的私有成员
    p1.y = 0;
    p2.x = 0;
    p2.y = 0;
}

line::line(float x1, float y1, float x2, float y2)
{
    p1.x = x1;
    p1.y = y1;
    p2.x = x2;
    p2.y = y2;
}

line::~line()
{
}

int main(int argc, char const *argv[])
{
    line l1(0,0,3,3); 
    l1.showLine(); 
    l1.distance(); 

    return 0;
}
