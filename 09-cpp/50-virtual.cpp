#include <iostream>
#include <string>

using namespace std;

class B0
{
private:
public:
    B0(/* args */) 
    {
        cout<<"B0 的构造函数被调用"<<endl; 
    }
    ~B0() {}
    int display()
    {
        cout << "B0::display()" << endl;
    }
};


// 解决多继承时,多个父类的问题使用虚继承的方式解决即可
class B1 : virtual public B0
{
private:
public:
    B1() 
    {
        cout<<"B1 的构造函数被调用"<<endl; 
    }
    ~B1() {}
};


// 解决多继承时,多个父类的问题使用虚继承的方式解决即可
class B2 : virtual public B0
{
private:
public:
    B2(/* args */) 
    {
        cout<<"B2 的构造函数被调用"<<endl; 

    }
    ~B2() {}
};

class D1 : public B1 , public B2
{
private:
    /* data */
public:
    D1()
    {
        cout<<"D1 的构造函数被调用"<<endl; 
    }
    ~D1()
    {

    }
};



int main(int argc, char const *argv[])
{

    D1 d1; 
    d1.display(); 
    

    return 0;
}