#include <iostream>
#include <string>

using namespace std;

/*
T1 可以是 int , flaot , string
T2 默认是double , 如果给定类型就使用给定的类型, 如果没有给定类型使用默认的类型 
可以定义一个变量给一个默认值
*/

template <class T1,class T2=double,int num=10>
class point
{
private:
    T1 x;
    T2 y;
public:
    point(T1 xx, T2 yy);
    ~point();
    T1 getX();
    T2 getY();
    void showPoint();
};

template <class T1,class T2,int num>
point<T1,T2,num>::point(T1 xx, T2 yy)
{
    x = xx+num;
    y = yy+num;
}

template <class T1,class T2,int num>
point<T1,T2,num>::~point()
{
}

template <class T1,class T2,int num>
T1 point<T1,T2,num>::getX()
{
    return x;
}

template <class T1,class T2,int num>
T2 point<T1,T2,num>::getY()
{
    return y;
}

template <class T1,class T2,int num>
void point<T1,T2,num>::showPoint()
{
    cout << "(" << x << "," << y << ")" << endl;
}

int main(int argc, char const *argv[])
{
    // T1 , T2 在实际参数是, 一定要传入具体的类型 
    // point<T1,T2> 带入实际类型 point<int,int>  
    // T1 的值为int , T2的值是int 
    // T2 给了具体的值, 就使用给定的值  
    // num 的默认值为10 , 
    point<int,int>  p1(1,2); 
    p1.showPoint(); 


    // T1 , T2 在实际参数是, 一定要传入具体的类型 
    // point<T1,T2> 带入实际类型 point<int>  
    // T1 的值为int , T2的值是double
    // T2 没有给定具体的值, 使用默认的类型double 
    point<int>  p2(1,2.9); 
    p2.showPoint(); 

    // T1 , T2 在实际参数是, 一定要传入具体的类型 
    // point<T1,T2> 带入实际类型 point<int>  
    // T1 的值为int , T2的值是double
    // T2 没有给定具体的值, 使用默认的类型double 
    // num = 20 , 使用给定的参数 
    point<int,int,20>  p3(1,2); 
    p3.showPoint(); 


    // T1 , T2 在实际参数是, 一定要传入具体的类型 
    // point<T1,T2> 带入实际类型 point<string,string>  
    // T1 的值为string , T2的值是string
    // T2 给了具体的值, 就使用给定的值  
    // 不支持字符串的操作了, 因此下面的代码编译会报错 
    //point<string,string>  p4("5","9"); 
    //p4.showPoint(); 

    return 0;
}

