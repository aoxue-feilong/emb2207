#include <iostream>
#include <string>
// string 是一个头文件, 这个头文件定义了string 类的实现
// string 是属于命名空间std的一部分, 因此在使用string 时要加上 std::string

// 使用这个句话的含义是 , 可以在程序中可以省略 std:: 前缀
// 把命名空间倒入到程序内, 程序内可以直接访问命名空间中的类名(结构体)
using namespace std;
int main(int argc, char const *argv[])
{

    // c语言的理解: 结构体定义了一个结构体变量
    // c++语言的理解: 类实例化了一个对象, 就是类定义了一个对象
    string s1, s2;

    // 字符串的输入与输出
    cout << "请输入第1个字符串的值>:";
    cin >> s1; // 会残留'\n' , cin 在读取时, 会自动跳过'\n'
    cout << "请输入第2个字符串的值>:";
    cin >> s2; // 会残留'\n' , cin 在读取时, 会自动跳过'\n'

    cout << "s1=" << s1 << endl;
    cout << "s2=" << s2 << endl;
    if (s1 == s2)
    {
        cout << "s1==s2" << endl;
    }
    /********************************************/
    if (s1 != s2)
    {
        cout << "s1!=s2" << endl;
    }
    /********************************************/

    if (s1 >= s2)
    {
        cout << "s1>=s2" << endl;
    }
    /********************************************/
    if (s1 <= s2)
    {
        cout << "s1<=s2" << endl;
    }
    /********************************************/
    if (s1 > s2)
    {
        cout << "s1>s2" << endl;
    }
    else if (s1 < s2)
    {
        cout << "s1<s2" << endl;
    }
    else
    {
        cout << "s1==s2" << endl;
    }

    return 0;
}
