#include <iostream>
#include <string>
#include <unistd.h>

using namespace std;

class Clock
{
private:
    int hour, min, sec;

public:
    Clock()
    {
        hour = 0;
        min = 0;
        sec = 0;
    }
    Clock(int h, int m, int s)
    {
        hour = h;
        min = m;
        sec = s;
    }
    ~Clock()
    {
    }
    int showTime()
    {
        cout << "time " << hour << ":" << min << ":" << sec << endl;
    }

    // 友元函数 前置单目运算符重载设计 , 有一个形参
    //  clk1.(operator++)(Clock &clk)
    // ++clk1
    friend void operator++(Clock &clk);
    friend void operator--(Clock &clk);

    // 后置单目运算符重载设计 , 有2个形参
    //  clk1.(operator++)(int)
    // clk1++;
    friend void operator++(Clock &clk, int );
    friend void operator--(Clock &clk, int );


};

void operator++(Clock &clk)
{
    clk.sec++;
    if (clk.sec >= 60)
    {
        clk.min++;
        clk.sec = 0;
        if (clk.min >= 60)
        {
            clk.hour++;
            clk.min = 0;
            if (clk.hour >= 24)
            {
                clk.hour = 0;
            }
        }
    }
}

void operator--(Clock &clk)
{
    clk.sec--;
    if (clk.sec <= 0)
    {
        clk.min--;
        clk.sec = 59;
        if (clk.min <= 0)
        {
            clk.hour--;
            clk.min = 59;
            if (clk.hour <= 0)
            {
                clk.hour = 23;
            }
        }
    }
}

void operator++(Clock &clk, int )
{
    clk.sec++;
    if (clk.sec >= 60)
    {
        clk.min++;
        clk.sec = 0;
        if (clk.min >= 60)
        {
            clk.hour++;
            clk.min = 0;
            if (clk.hour >= 24)
            {
                clk.hour = 0;
            }
        }
    }
}
void operator--(Clock &clk, int )
{
    clk.sec--;
    if (clk.sec <= 0)
    {
        clk.min--;
        clk.sec = 59;
        if (clk.min <= 0)
        {
            clk.hour--;
            clk.min = 59;
            if (clk.hour <= 0)
            {
                clk.hour = 23;
            }
        }
    }
}
int main(int argc, char const *argv[])
{
    Clock clk1(9, 3, 10);
    Clock clk2(10,3,10);
    Clock clk3(0,0,0); // 23:59:59
    Clock clk4(0,0,0); // 23:59:59
    clk1.showTime();
    clk2.showTime(); 
    clk3.showTime(); 
    while (1)
    {
        ++clk1; // clk1.(operator++)()
        clk1.showTime();

        clk2++; 
        clk2.showTime(); 

        --clk3;
        clk3.showTime(); 

        clk4--; 
        clk4.showTime(); 
        sleep(1);
    }
    return 0;
}