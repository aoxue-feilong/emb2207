#include <iostream>
#include <string>
// string 是一个头文件, 这个头文件定义了string 类的实现
// string 是属于命名空间std的一部分, 因此在使用string 时要加上 std::string


// 使用这个句话的含义是 , 可以在程序中可以省略 std:: 前缀 
// 把命名空间倒入到程序内, 程序内可以直接访问命名空间中的类名(结构体)
using namespace std;  
int main(int argc, char const *argv[])
{

    // c语言的理解: 结构体定义了一个结构体变量
    // c++语言的理解: 类实例化了一个对象, 就是类定义了一个对象
    string str1 = "hello world!!";
    cout << "str1=" << str1 << endl;

    return 0;
}
