#include <iostream>
#include <string>
#include <math.h>
using namespace std;
class point
{
private:
    float x, y;
public:
    // 这就是构造函数的重载 , 就是函数重载
    point();                   // 无参构造函数
    point(float xx, float yy); // 有参构造函数
    ~point();
    int showPoint()
    {
        cout << "(" << x << "," << y << ")" << endl;
    }
    int setPoint(float xx, float yy)
    {
        this->x = xx;
        this->y = yy;
    }
    // point这个类把 distence 函数当成朋友, 关系是单向的 , distence函数可以访问point中的私有成员 
    friend int distance(point &p1,point &p2) ;  // 声明distence 这个函数是一个友元函数 
};

point::point()// 无参调用这个函数
{
    this->x = 0;
    this->y = 0;
}
point::point(float xx, float yy)// 有参调用这个函数
{
    this->x = xx;
    this->y = yy;
}

point::~point()
{
}

int distance(point &p1,point &p2)  
{
    float lx = (p2.x-p1.x)*(p2.x-p1.x); 
    float ly = (p2.y-p1.y)*(p2.y-p1.y); 
    cout<<"两点之间的距离为:"<< sqrt(lx+ly) <<endl;
}

int main(int argc, char const *argv[])
{
    point p1; 
    p1.showPoint(); 
    point p2(3,3);
    p2.showPoint();

    // 计算两点之间的距离  
    distance(p1,p2);  

    return 0;
}
