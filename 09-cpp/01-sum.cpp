
// C++ 中 可以省略.h 的书写方式 
// iostream input output stream 
#include <iostream>


int main(int argc, char const *argv[])
{
    // std  standard 标准  , C++ 库的标准 , std 是一个容器,把容器当成一个数组或链表 
    // :: 是限定符, 表示属于谁的东西  , 类似于 结构体.成员名 
    // pthon 和 go 语言都已经把:: 换成了. 
    // 可以把这内容理解为结构体访问成员名 
    // << 表示输出内容   >> 表示输入内容  
    // std::endl  就是c语言的 '\n' 
    std::cout << "Enter two number:" << std::endl ; // 等价于 c语言的printf 函数的功能
    int v1 =  0 , v2 = 0  ; 

    // std::cin  输入语句, 等价于c语言的scanf 
    //  >> 表示输入内容 
    // 把标准输入的内容分别写入到v1和v2 当中 
    std::cin  >>  v1 >> v2 ;  // scanf("%d%d",&v1,&v2); 

    // printf("The sum of %d and %d  is %d\n",v1,v2,v1+v2); 
    std::cout <<"The sum of "<<v1 << " and " << v2 << " is " << v1+v2 <<std::endl; 

    return 0;
}


