#include <iostream>
#include <cmath>
#define PI 3.1415926

using namespace std;

class pool
{
private:
  float r, zhanlan_cost, guodao_cost, guodao_width;

public:
  pool(float zhanlan, float guodao_c, float width);//构造函数(有参数是调用)
  pool();//无参调用
  int input(); //输入半径
  int cost();  //计算造价
};
pool::pool(float zhanlan, float guodao_c, float width)//有对象创建后，系统自动调用构造函数
{
  this->zhanlan_cost = zhanlan;
  this->guodao_cost = guodao_c;
  this->guodao_width = width;
}
pool::pool()//无参调用
{
}
int pool::input()//类内的函数可以直接类的成员（数据和函数）
{
  cout << "请输入圆的半径>:";
  cin >> r;
}
int pool::cost()//类内的函数可以直接类的成员（数据和函数）
{
  cout << "过道的造价>:" << (PI*(r + guodao_width)*(r + guodao_width) - PI*r*r)*guodao_cost<<endl;
  cout << "栅栏的造价>:" << PI*2*(r + guodao_width)*zhanlan_cost<<endl;
}
int main(int argc, char const *argv[])
{
  pool p1(35, 20, 3);//对象，构造函数:有对象创建后，系统自动调用构造函数
  p1.input();//类内的函数
  p1.cost();//类内的函数
  return 0;
}
