#include <iostream>
#include <string>

using namespace std;

#define PI 3.1415926
class circle
{
private:
    float r; // 圆的半径
public:
    circle()
    {
        r = 0;
    }
    circle(float R)
    {
        r = R; 
    }
    ~circle()
    {

    }
    int setR(float R) // 设置圆的半径
    {
        r = R;
    }
    float area()
    {
        return  (PI * r * r) ;
    }
};


class table
{
private:
    float height  ; // 桌子的高度 
    string color  ;// 桌子的颜色 
public:
    table()
    {
        this->height = 0 ; 
        this->color = "White"; 
    }
    table(float h,string col)
    {
        height = h ; 
        color = col ; 
    }
    ~table()
    {

    }
    int setH(float h)
    {
        this->height = h ; 
    }
    float getH() // 获取桌子的高度 
    {
        return height ; 
    }
    int setColor(string col)
    {
        this->color = col;
    }
    string getColor()
    {
        return color; 
    }
    
};

class fruit
{
private:
    string  kind ; // 哪一种水果 
    int count; // 水果的个数 
public:
    fruit(string kin,int cou)
    {
        this->kind = kin; 
        this->count = cou; 
    }
    ~fruit()
    {
    }
    string getKind()
    {
        return kind ; 
    }
    int getCount()
    {
        return count ; 
    }
};




// 多继承 
class roundtable : public circle , public table 
{
private:
    fruit fr; // 水果的对象 
public:
    // 多继承且有内嵌对象时的构造函数实现
    roundtable(float r,float h , string col,string kind,int count)
    :circle(r),table(h,col),fr(kind,count)
    {

    }
    ~roundtable()
    {

    }
    int showInfo()
    {
        cout<<"圆桌的面积为:"<<this->area()<<endl;
        cout<<"圆桌的高度为:"<<this->getH()<<endl;
        cout<<"圆桌的颜色为:"<<this->getColor()<<endl;
        cout<<"圆桌的水果种类为:"<<fr.getKind()<<endl;
        cout<<"圆桌的水果数量为:"<<fr.getCount()<<endl;
    }
};
int main(int argc, char const *argv[])
{

    roundtable rt1(1,0.75,"Yellow","Apple",10);
    rt1.showInfo();  
    return 0;
}