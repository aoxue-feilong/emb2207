
// C++ 中 可以省略.h 的书写方式 
// iostream input output stream 
#include <iostream>
#include <string>
class book
{
private:  // 私有成员 
    std::string isbn;   // 字符串变量 ,图书编号 , string 属于std成员 , 需要加限定符 
    int number ;  // 数量 
    float price ; // 价格
    
public:   // 共有成员 
    book(/* args */);  // 构造函数声明
    ~book();           // 析构函数声明
    int input(); // 声明一个函数 
    int show();  // 声明一个函数

};

// 构造函数定义
book::book(/* args */)  
{
}

// 析构函数定义
book::~book()
{
}

int book::input()
{
    std::cout<<"请输入书的isbn号>:"; 
    std::cin>>isbn; 
    std::cout<<"请输入书的数量>:"; 
    std::cin>>number; 
    std::cout<<"请输入书的价格>:"; 
    std::cin>>price; 
    return 0 ; 
}

int book::show()
{
    std::cout<<"图书信息为:"<<isbn<<" "<<number <<" "<<price<<std::endl; 
    return 0; 
}


int main(int argc, char const *argv[])
{

    book bk1 ;  // 类定义一个对象, 结构体定义一个结构体变量
    bk1.input(); // 调用对象中的函数,  结构体调用成员名 
    bk1.show(); // 调用对象中的函数,  结构体调用成员名 

    return 0;
}


