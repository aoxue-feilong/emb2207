#include <iostream>
#include <string>

using namespace std;

class Clock
{
private:
    int hour, min, sec; // 成员数据
public:
    // 构造函数 ,和类名一样的函数就是构造函数, 没有返回值, 系统自动调用
    // 构造函数可以是默认参数的
    Clock(int h = 0, int m = 0, int s = 0);
    Clock(Clock &clk);                             // 拷贝构造函数, 其形参为本类的对象引用
    int set_time(int h = 0, int m = 0, int s = 0); // 带默认参数的成员函数  函数声明
    int show_time();                               // 函数声明
    Clock & setup(Clock clk);                          // 形参为本类的对象
};

Clock::Clock(int h, int m, int s)
{
    cout << "构造函数被调用\n";
    // 构造函数内, 一般设置类对象的初始化操作
    hour = h;
    min = m;
    sec = s;
}

Clock::Clock(Clock &clk)
{
    cout << "拷贝构造函数被调用\n";
    // 构造函数内, 一般设置类对象的初始化操作
    hour = clk.hour;
    min = clk.min;
    sec = clk.sec;
}

// 带默认形参只能在声明时使用, 在定义时使用会报错
int Clock::set_time(int h, int m, int s)
{
    hour = h; //  在成员函数中可以直接访问类内成员数据和成员函数
    min = m;  //  在成员函数中可以直接访问类内成员数据和成员函数
    sec = s;  //  在成员函数中可以直接访问类内成员数据和成员函数
    return 0;
}

Clock & Clock::setup(Clock clk)
{
    static Clock clk1;//这儿也会调用构造函数，因为创建了类的对象
    clk1.hour = clk.hour; 
    clk1.min = clk.min ; 
    clk1.sec = clk.sec; 
    return clk1; 
}

int Clock::show_time()
{
    //  在成员函数中可以直接访问类内成员数据和成员函数
    cout << "HH:MM:SS " << hour << ":" << min << ":" << sec << endl;
}

int main(int argc, char const *argv[])
{
    // 类定义一个类变量(对象)  , 是直接初始化的方式
    // 定义一个对象, 并用参数初始化这个对象
    Clock clk1(10, 32, 10);

    // 第1种情况
    // 当用类的一个对象去初始化该类的另一个对象时系统自动调用拷贝构造函数实现拷贝赋值。
    Clock clk2(clk1);  // 直接初始化 clk2 = clk1  , 会调用copy构造函数
    Clock clk3 = clk1; // 复制初始化 clk3 = clk1  , 会调用copy构造函数

    // 第2种情况
    // 若函数的形参为类对象，调用函数时，实参赋值给形参，系统自动调用拷贝构造函数。
    // 实参赋值给形参 clk(形参) = clk1 , 也会产生一个形参对象, 系统也会调动copy构造函数
    // clk3.setup(clk1); 

    // 第4种情况
    // 当函数的返回值是类对象引用时，系统自动调用拷贝构造函数
    //  clk4 = clk(形参对象)
    // 程序报错 , 出现段错误 , 是应为我们使用了clk(形参对象)是一个局部变量, 生存期在return clk时已经结束
    // 不能赋值给clk4 , 因此会出现这个问题, 解决办法, 延长生存期
    // 
    Clock clk4 =  clk3.setup(clk1);
    return 0;
}
