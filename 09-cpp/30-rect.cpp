#include <iostream>
#include <string>

using namespace std;
class point
{
private:
    float x, y;

public:
    point();                   // 无参构造函数
    point(float xx, float yy); // 有参构造函数
    ~point();
    float getX() // 获取点的x坐标
    {
        return x;
    }
    float getY() // 获取点的y坐标
    {
        return y;
    }
    int setPoint(float xx, float yy) // 设置这个点
    {
        this->x = xx;
        this->y = yy;
    }
    float showPoint() // 显示这个点
    {
        cout << "(" << x << "," << y << ")" << endl;
    }
};

point::point()
{
    this->x = 0;
    this->y = 0;
}
point::point(float xx, float yy)
{
    this->x = xx;
    this->y = yy;
}

point::~point()
{
}

class rect : private point
{
private:
    float width, height;

public:
    rect();                                     // 无参构造函数
    rect(float xx, float yy, float w, float h); // 有参构造函数
    ~rect();
    int setRect(float xx, float yy, float w, float h); // 设置矩形
    int showRect();
};

// 重点理解的地方 
// 构造派生类对象时, 要先构造基类对象, 无参构造函数不需要给基类传递参数
rect::rect()
{
    this->width = 0;
    this->height = 0;
}

// 构造派生类对象时, 要先构造基类对象, 
// 有参构造函数, 需要给基类传递参数
/*
派生类名::派生类名(基类所需的形参，本类成员所需的形参):基类名(参数表)
{
        本类成员初始化赋值语句；
};
*/
// : point(xx,yy)  给基类构造函数传递参数

rect::rect(float xx, float yy, float w, float h) :point(xx,yy)
{
    this->width = w;
    this->height = h;
}

rect::~rect()
{
}

int rect::setRect(float xx, float yy, float w, float h) // 设置矩形
{
    this->setPoint(xx, yy); // 设置点的x和y
    this->width = w;
    this->height = h;
}

int rect::showRect()
{
    cout << "(" << getX() << "," << getY() << "," << width << "," << height << ")"<<endl;;
}

int main(int argc, char const *argv[])
{

    rect r1 ; // 不给传递参数, 使用默认的构造函数 
    r1.showRect(); 

    rect r2(0,0,3,4);
    r2.showRect() ;

    // 通过派生类的对象不能直接访问基类中的任何成员。
    r1.showPoint();  // 通过子类调用父类的成员函数 , 私有继承不可以这样使用
    r2.showPoint();  // 通过子类调用父类的成员函数 , 私有继承不可以这样使用
    return 0;
}
