#include <iostream>
#include <string>
#include <vector>
using namespace std;

template <class T>
bool search(vector<T> &v1, T value)
{
    for (auto it = v1.begin(); it != v1.end(); it++)
    {
        if (*it == value)
        {
            return true;
        }
    }
    return false;
}
int main(int argc, char const *argv[])
{
    vector<int> v1;
    cout << "v1.size()=" << v1.size() << endl;
/******************************插入**********************************/
    cout << "插入顺序表的顺序：";
    for (int i = 0; i < 10; i++)
    {
        v1.push_back(i + 1);
    }

    for (auto it = v1.begin(); it != v1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    cout << "v1.size()=" << v1.size() << endl;
/******************************删除**********************************/
    cout << "删除顺序表的顺序：";
    v1.erase(v1.begin() + 9);
    v1.erase(v1.begin() + 4);
    v1.erase(v1.begin());
    for (auto it = v1.begin(); it != v1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    cout << "v1.size()=" << v1.size() << endl;
/******************************修改**********************************/
        cout << "修改顺序表的顺序：";
        for (auto it = v1.begin(); it != v1.end(); it++)
        {
            if (*it == 9)
            {
                *it = 99;
            }
            if (*it == 2)
            {
                *it = 22;
            }
        }
        for (auto it = v1.begin(); it != v1.end(); it++)
        {
            cout << *it << " ";
        }
        cout << endl;
/******************************查找**********************************/
        if (search(v1, 99))
        {
            cout << "99 found\n";
        }
        else
        {
            cout << "99 not found\n";
        }
        if (search(v1, 100))
        {
            cout << "100 found\n";
        }
        else
        {
            cout << "100 not found\n";
        }
    return 0;
}
