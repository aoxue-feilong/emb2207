#include <iostream>

using namespace std;

// 函数如果不传递参数, 就是使用默认值
// 函数如果传递指定的参数, 那就是用指定的值
// 
int show(int x = 1, int y = 2, int z = 3)
{
    cout << "x=" << x << " y=" << y << " z=" << z << endl;
}

int main(int argc, char const *argv[])
{
    // 第1种情况: 给指定的参数 , 使用指定的参数值 
    int  a=10,b=20,c=30 ; 
    show(a,b,c);

    // 第2种情况: 不传递参数, 会使用默认值
    show();

    // 第3种情况: 传递一部分参数,另外一部分不传递,
    // 传递的参数使用指定的值, 不传递的使用默认值 
    show(4);
    show(5,6); 
    return 0;
}
