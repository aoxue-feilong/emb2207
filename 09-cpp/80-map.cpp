#include <iostream>
#include <string>
#include <map>
using namespace std;
int main(int argc, char const *argv[])
{
    //创建一个空的map 关联式容器，该容器中存储键值对
    //key 为string ， value 为string
    map<string,string> m1;

    m1["C语言基础"]="https://xxx.com/Cbase";
    m1["Linux语言基础"]="https://xxx.com/Linuxbase";
    m1["C语言高级"]="https://xxx.com/Chigh";

    cout<<"C语言基础="<<m1["C语言基础"]<<endl;
    cout<<"Linux语言基础="<<m1["Linux语言基础"]<<endl;
    cout<<"C语言高级="<<m1["C语言高级"]<<endl;
    cout<<"--------------------------------------\n";
    //迭代器  遍历关联容器
    for(auto it=m1.begin();it!=m1.end();it++)
    {
        cout<<it->first<<"="<<it->second<<endl;
    }
    return 0;
}
