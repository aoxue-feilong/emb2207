#include <iostream>
#include <string>

using namespace std;

class Clock
{
private:
    int hour, min, sec; // 成员数据
    static string factory ; // 表的厂家  , 声明这个factory是一个静态成员 
public:
    // 构造函数 ,和类名一样的函数就是构造函数, 没有返回值, 系统自动调用
    // 构造函数可以是默认参数的
    Clock(int h = 0, int m = 0, int s = 0);
    Clock(Clock &clk); // 拷贝构造函数, 其形参为本类的对象引用
    ~Clock();          // 析构函数 , 构造函数前加上~ , 表示是析构函数 , 函数自动被调用

    int set_time(int h = 0, int m = 0, int s = 0); // 带默认参数的成员函数  函数声明
    int show_time();                               // 函数声明
    Clock &setup(Clock clk);                       // 形参为本类的对象
    int show_factory(); // 显示厂家信息 
    int set_factory(string fac) // 设置厂家信息
    {
        factory = fac;  
    }
};

// 静态成员数据, 在类外定义和初始化 
string Clock::factory = "Casio" ; 

Clock::Clock(int h, int m, int s)
{
    //cout << "构造函数被调用\n";
    // 构造函数内, 一般设置类对象的初始化操作
    this->hour = h; // 可以使用this指针提示的功能, 来访问成员函数或数据
    this->min = m;
    this->sec = s;
}

Clock::Clock(Clock &clk)
{
    //cout << "拷贝构造函数被调用\n";
    // 构造函数内, 一般设置类对象的初始化操作
    this->hour = clk.hour;
    this->min = clk.min;
    this->sec = clk.sec;
}

Clock::~Clock()
{
    //cout << "析构函数被调用\n";
}

// 带默认形参只能在声明时使用, 在定义时使用会报错
int Clock::set_time(int h, int m, int s)
{
    this->hour = h; //  在成员函数中可以直接访问类内成员数据和成员函数
    this->min = m;  //  在成员函数中可以直接访问类内成员数据和成员函数
    this->sec = s;  //  在成员函数中可以直接访问类内成员数据和成员函数
    return 0;
}

Clock &Clock::setup(Clock clk)
{
    static Clock clk1;
    clk1.hour = clk.hour;
    clk1.min = clk.min;
    clk1.sec = clk.sec;
    return clk1;
}

int Clock::show_time()
{
    //  在成员函数中可以直接访问类内成员数据和成员函数
    cout << "HH:MM:SS " << hour << ":" << min << ":" << sec << endl;
}

int Clock::show_factory()
{
    cout<<"厂家为:"<<factory<<endl; 
}

int main(int argc, char const *argv[])
{

    Clock clk1(15,05,50); // 使用指定参数的构造函数
    clk1.show_factory(); 
    Clock clk2;      //  带默认参数的构造函数
    clk2.show_factory();  
    Clock clk3 = clk1;  // copy 构造函数 
    clk3.show_factory();  
    clk3.set_factory("Shanghai");
    clk1.show_factory(); 
    clk2.show_factory(); 
    clk3.show_factory(); 
    // 该类的所有对象维护该成员的同一个拷贝
    // 所有的表对象共享一个factory 静态成员数据 
    return 0;
}
