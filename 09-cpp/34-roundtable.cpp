#include <iostream>
#include <string>

using namespace std;

#define PI 3.1415926
class circle
{
private:
    float r; // 圆的半径
public:
    circle()
    {
        r = 0;
    }
    ~circle(){}
    int setR(float R) // 设置圆的半径
    {
        r = R;
    }
    float area()
    {
        return  (PI * r * r) ;
    }
};

class table
{
private:
    float height  ; // 桌子的高度 
    string color  ;// 桌子的颜色 
public:
    table()
    {
        this->height = 0 ; 
        this->color = "White"; 
    }
    ~table(){}
    int setH(float h)
    {
        this->height = h ; 
    }
    float getH() // 获取桌子的高度 
    {
        return height ; 
    }
    int setColor(string col)
    {
        this->color = col;
    }
    string getColor()
    {
        return color; 
    }
};

// 多继承 
class roundtable : public circle , public table 
{
private:
   
public:
    roundtable(){}
    ~roundtable(){}
    int showInfo()
    {
        cout<<"圆桌的面积为:"<<this->area()<<endl;
        cout<<"圆桌的高度为:"<<this->getH()<<endl;
        cout<<"圆桌的颜色为:"<<this->getColor()<<endl;
    }
};
int main(int argc, char const *argv[])
{

    roundtable rt1;//对象 
    rt1.setR(1); // 设置圆桌的半径 
    rt1.setH(0.75); // 设置圆桌的高度 
    rt1.setColor("Red"); // 设置圆桌的颜色
    rt1.showInfo();  
    return 0;
}