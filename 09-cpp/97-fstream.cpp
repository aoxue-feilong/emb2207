#include <iostream>
#include <string>
#include <queue>
#include <fstream>
using namespace std;

int main(int argc, char const *argv[])
{
    //想往文件内写入内容时，可以把file1 当成cout，cout<< 写入文件
    //ios::in | ios::out | ios::trunc 对文件可读可写，不存在就创建
    fstream file1("text1.txt", ios::in | ios::out | ios::trunc);
    file1 << "student1 info" << endl;
    file1 << "student1 number:" << 1 << endl;
    file1 << "student1 name:" << "lcl" << endl;
    file1 << "student1 age:" << 22 << endl;
    file1 << "student1 score:" << 100 << endl;
    
    //想往文件内写入内容时，可以把file2 当成cout，cout<< 写入文件
    fstream file2;
    file2.open("text2.txt", ios::in | ios::out | ios::trunc);
    file2 << "student2 info" << endl;
    file2 << "student2 number:" << 2 << endl;
    file2 << "student2 name:" << "lcl" << endl;
    file2 << "student2 age:" << 22 << endl;
    file2 << "student2 score:" << 100 << endl;

    return 0;
}
