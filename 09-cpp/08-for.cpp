#include <iostream>
#include <string>

using namespace std;
int main(int argc, char const *argv[])
{

    string s1 = "goodbye";
    // s1中的每一个元素是一个字符, 要遍历s1中所有的元素, 需要一个字符变量i
    // 字符串对象支持迭代的功能 , 迭代器的意思是支持遍历所有成员 
    for (char i : s1)
    {
        cout << "i=" << i << endl;
    }
    return 0;
}
