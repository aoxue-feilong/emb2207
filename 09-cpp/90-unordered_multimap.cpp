#include <iostream>
#include <string>
#include <map>
#include <unordered_map>

using namespace std;

int main(int argc, char const *argv[])
{
    //默认表示从小到大排序
    unordered_map<string,float> m1;
    m1.emplace("数学",89);
    m1.emplace("数学",90);
    m1.emplace("数学",100);
    m1.emplace("语文",90);
    m1.emplace("英语",91);

    
    //迭代器  遍历关联容器
    // it->first是key
    // it->second是value
    for (auto it = m1.begin(); it != m1.end(); it++)
    {
        cout << it->first << "\t:" << it->second << endl;
    }
    cout<<"----------------------------"<<endl;
    unordered_multimap<string,float> m2;
    m2.emplace("数学",89);
    m2.emplace("数学",90);
    m2.emplace("数学",100);
    m2.emplace("语文",90);
    m2.emplace("英语",91);

    
    //迭代器  遍历关联容器
    // it->first是key
    // it->second是value
    for (auto it = m2.begin(); it != m2.end(); it++)
    {
        cout << it->first << "\t:" << it->second << endl;
    }
    return 0;
}
