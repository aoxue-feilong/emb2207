#include <iostream>
#include <string>

using namespace std;

/*
T  是一个可变类型
T 可以是 int , flaot , string

1. 第一种情况
如果T 是int 那这个模板函数编译器会解析成
int add(int a, int b)
{
    return a+b;
}


2. 第2种情况
如果T 是float 那这个模板函数编译器会解析成
float add(float a, float b)
{
    return a+b;
}

3. 第3种情况
如果T 是string 那这个模板函数编译器会解析成
string add(string a, strint b)
{
    return a+b;
}


*/

template <class T>
T add(T a, T b)
{
    return a + b;
}

int main(int argc, char const *argv[])
{
    int x = 10, y = 20;
    cout << "x+y=" << add(x, y) << endl;

    float m = 10.5, n = 20.6;
    cout << "m+n=" << add(m, n) << endl;

    string s1 = "good", s2 = "bye";
    cout << "s1+s2=" << add(s1, s2) << endl;

    return 0;
}