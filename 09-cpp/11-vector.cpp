#include <iostream>
#include <string>
#include <vector>

using namespace std;
int main(int argc, char const *argv[])
{

    vector<int> v1{1, 2, 3, 4, 5};
    vector<int> v2{1, 2, 3, 4, 5};
    /********************************/
    if (v1 == v2)
    {
        cout << "v1==v2" << endl;
    }
    /********************************/
    if (v1 != v2)
    {
        cout << "v1!=v2" << endl;
    }
    /********************************/
    if (v1 >= v2)
    {
        cout << "v1>=v2" << endl;
    }
    /********************************/
    if (v1 <= v2)
    {
        cout << "v1<=v2" << endl;
    }
    /********************************/
    if (v1 > v2)
    {
        cout << "v1>v2" << endl;
    }
    else if (v1 < v2)
    {
        cout << "v1<v2" << endl;
    }
    else
    {
        cout << "v1==v2" << endl;
    }

    return 0;
}
