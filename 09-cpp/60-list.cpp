#include <iostream>
#include <string>

using namespace std;

class student
{
private:
    int number;
    string name;
    int age;
    float score;

public:
    student()
    {
        number = 0;
        name = "";
        age = 0;
        score = 0;
    }
    student(int num, string n, int a, float s)
    {
        number = num;
        name = n;
        age = a;
        score = s;
    }
    ~student()
    {
    }
    // 重载 ostream 就是cout
    // cout<< student 会调用如下代码
    friend ostream &operator<<(ostream &os, const student s)
    {
        cout << "|" << s.number << "|" << s.name << "|" << s.age << "|" << s.score << "|" << endl;
    }
    int operator==(const student s)
    {
        if(this->number == s.number )
        {
            return true; 
        }
        else 
        {
            return false ;
        }
    }
};

template <class T>
class list
{
private:
    T data;     // 数据域部分
    list *next; // 指向下一个节点的指针
public:
    list(); // 构造函数, 创建头结点
    ~list();
    int insert(T data);                 // 插入链表
    int del(T data);                    // 链表的删除 防止和系统的delete关键字重名, 不用delete名称,用del
    int modify(T oldvalue, T newvalue); // 链表的修改
    bool search(T data);                // 链表的查找
    int reverse();                      // 链表的逆序
    int show();                         // 显示链表的中内容
};

template <class T>
list<T>::list()
{
    this->next = nullptr; // 头结点的下一个节点为空
}

template <class T>
list<T>::~list()
{
}

template <class T>
int list<T>::insert(T data)
{
    list *p = new list;
    p->data = data;
    p->next = this->next; // p->next =h->next
    this->next = p;       // h->next = p ;
    return 0;
}

//  要找到要删除节点的前一个节点
template <class T>
int list<T>::del(T data)
{
    list *p = this;    // p = h ;
    list *q = nullptr; // q 是要删除的节点
    while (p->next != nullptr)
    {
        // p->next->data 是student对象 
        // data 传递进来的也student 
        if (p->next->data == data)
        {
            q = p->next;       // q保存要删除的节点
            p->next = q->next; // 接上链表
            delete q;          // 删除节点
            return 0;          // 必须要退出循环 , 否则可能会出错(p = p->next)
        }
        p = p->next; // 移动p 节点
    }
    return 0;
}

template <class T>
int list<T>::modify(T oldvalue, T newvalue)
{
    list *p = this->next; // p指向头结点的下一个节点

    while (p != nullptr)
    {
        if (p->data == oldvalue)
        {
            p->data = newvalue;
            return 0;
        }
        p = p->next; // 移动节点
    }
    return 0;
}

template <class T>
bool list<T>::search(T value)
{
    list *p = this->next; // p指向头结点的下一个节点

    while (p != nullptr)
    {
        if (p->data == value)
        {
            return 1; // 返回真
        }
        p = p->next; // 移动节点
    }
    return 0; // 返回假
}

template <class T>
int list<T>::show()
{
    list *p = this->next; // p指向头结点的下一个节点
    cout << "list内容为:" << endl;
    while (p != nullptr)
    {
        cout << p->data;
        p = p->next; // 移动节点
    }
}

template <class T>
int list<T>::reverse()
{
    list *p = this->next; // p 指向头结点的下一个节点
    list *q;              // 用来保存p的下一个节点
    this->next = nullptr; // 设置一个新的头结点
    
    while (p != nullptr)
    {
        // 把p 节点重新插入到头结点后面
        q = p->next;          // q保存p->next 节点
        p->next = this->next; // p->next = h->next;
        this->next = p;       // h->next = p
        p = q;                // 把q的值赋值给p
    }
    return 0;
}

int main(int argc, char const *argv[])
{

    list<student> l1;
    for (int i = 0; i < 10; i++)
    {
        l1.insert(student(i + 1, "student" + to_string(i + 1), 22 + i, 80 + i));
    }
    l1.show();

    l1.del(student(10,"student10",31,89));
    l1.del(student(1 ,"student1" ,22,80));
    l1.del(student(5 ,"student5" ,26,84));
    l1.show();

    l1.modify(student(9,"student9",30,88), student(99,"student99",24,99));
    l1.modify(student(6,"student6",30,88), student(66,"student66",24,90));
    l1.show();

    if (l1.search(student(99,"student99",30,88)))
    {
        cout << "99 found" << endl;
    }
    else
    {
        cout << "99 not found" << endl;
    }
    if (l1.search(student(100,"student100",30,88)))
    {
        cout << "100 found" << endl;
    }
    else
    {
        cout << "100 not found" << endl;
    }

    l1.reverse();
    l1.show();
    return 0;
}
