#include <iostream>
#include <string>
// string 是一个头文件, 这个头文件定义了string 类的实现
// string 是属于命名空间std的一部分, 因此在使用string 时要加上 std::string

// 使用这个句话的含义是 , 可以在程序中可以省略 std:: 前缀
// 把命名空间倒入到程序内, 程序内可以直接访问命名空间中的类名(结构体)
using namespace std;
int main(int argc, char const *argv[])
{

    // c语言的理解: 结构体定义了一个结构体变量
    // c++语言的理解: 类实例化了一个对象, 就是类定义了一个对象
    string s1 = "hello world!!"; // 赋值初始化
    string s2("goodbye");        // 直接初始化 , 使用()的形式实现
    string s3(10, 'A');          // 直接初始化 , 使用10个A初始化这个字符串
    cout << "s1=" << s1 << endl;
    cout << "s2=" << s2 << endl;
    cout << "s3=" << s3 << endl;

    // 字符串的输入与输出
    cout << "请输入第1个字符串的值>:";
    cin >> s1; // 会残留'\n' , cin 在读取时, 会自动跳过'\n'
    cout << "请输入第2个字符串的值>:";
    cin >> s2; // 会残留'\n' , cin 在读取时, 会自动跳过'\n'
    cout << "请输入第3个字符串的值>:";
    getchar();        // 消除'\n';
    getline(cin, s3); // c++的内置函数, 一次读取一行, 等价于c语言的fgets 函数 , 遇见'\n'结束, '\n'会被读入
    cout << "s1=" << s1 << endl;
    cout << "s2=" << s2 << endl;
    cout << "s3=" << s3 << endl;

    // 计算字符串的大小
    cout << "s1 length = " << s1.size() << endl; // 获取字符串的长度 , 等价于c 的strlen
    cout << "s2 length = " << s2.size() << endl; // 获取字符串的长度 , 等价于c 的strlen
    cout << "s3 length = " << s3.size() << endl; // 获取字符串的长度 , 等价于c 的strlen

    // 判断字符串是否为空
    s1 = "";                                     // 这就是空串
    cout << "s1.empty()=" << s1.empty() << endl; // 为空为真(1)
    cout << "s1.size()=" << s1.size() << endl;
    s2 = "god";                                  // strcpy
    cout << "s2.emtpy()=" << s2.empty() << endl; // 不为空返回假(0)
    cout << "s2.size()=" << s2.size() << endl;

    // 可以按照数组的形式对字符串进行访问
    s1 = "hello";
    s1[0] = 'H';
    cout << "s1=" << s1 << endl;

    // 对字符串进行拼接 strcat
    s2 = "World!!";
    s3 = s1 + s2; // 就是strcat 的功能
    cout << "s3=s1+s2=" << s3 << endl;

    return 0;
}
