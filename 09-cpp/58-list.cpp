#include <iostream>
#include <string>

using namespace std;

class list
{
private:
    int data;   // 数据域部分
    list *next; // 指向下一个节点的指针
public:
    list(); // 构造函数, 创建头结点
    ~list();
    int insert(int data);                   // 插入链表
    int del(int data);                      // 链表的删除 防止和系统的delete关键字重名, 不用delete名称,用del
    int modify(int oldvalue, int newvalue); // 链表的修改
    int search(int data);                   // 链表的查找
    int reverse();                          // 链表的逆序
    int show();                             // 显示链表的中内容
};

list::list()
{
    this->data = 0;       // 数据域部分为0
    this->next = nullptr; // 头结点的下一个节点为空
}

list::~list()
{
}

int list::insert(int data)
{
    list *p = new list;
    p->data = data;
    p->next = this->next; // p->next =h->next
    this->next = p;       // h->next = p ;
}

//  要找到要删除节点的前一个节点
int list::del(int data)
{
    list *p = this;    // p = h ;
    list *q = nullptr; // q 是要删除的节点
    while (p->next != nullptr)
    {
        if (p->next->data == data)
        {
            q = p->next;       // q保存要删除的节点
            p->next = q->next; // 接上链表
            delete q;          // 删除节点
            return 0;          // 必须要退出循环 , 否则可能会出错(p = p->next)
        }
        p = p->next; // 移动p 节点
    }
    return 0;
}

int list::modify(int oldvalue, int newvalue)
{
    list *p = this->next; // p指向头结点的下一个节点

    while (p != nullptr)
    {
        if (p->data == oldvalue)
        {
            p->data = newvalue;
            return 0;
        }
        p = p->next; // 移动节点
    }
    return 0;
}

int list::search(int value)
{
    list *p = this->next; // p指向头结点的下一个节点

    while (p != nullptr)
    {
        if (p->data == value)
        {
            return 1; // 返回真
        }
        p = p->next; // 移动节点
    }
    return 0; // 返回假
}

int list::show()
{
    list *p = this->next; // p指向头结点的下一个节点
    cout << "list内容为:";
    while (p != nullptr)
    {
        cout << p->data << " ";
        p = p->next; // 移动节点
    }
    cout << endl;
}

int list::reverse()
{
    list *p = this->next; // p 指向头结点的下一个节点
    list *q;              // 用来保存p的下一个节点
    this->next = nullptr; // 设置一个新的头结点

    while (p != nullptr)
    {
        // 把p 节点重新插入到头结点后面
        q = p->next;          // q保存p->next 节点
        p->next = this->next; // p->next = h->next;
        this->next = p;       // h->next = p
        p = q;                // 把q的值赋值给p
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    list l1;
    for (int i = 0; i < 10; i++)
    {
        l1.insert(i + 1);
    }
    l1.show();
    l1.del(10);
    l1.del(1);
    l1.del(5);
    l1.show();
    l1.modify(9, 99);
    l1.modify(6, 66);
    l1.show();
    if (l1.search(99))
    {
        cout << "99 found" << endl;
    }
    else
    {
        cout << "99 not found" << endl;
    }
    if (l1.search(100))
    {
        cout << "100 found" << endl;
    }
    else
    {
        cout << "100 not found" << endl;
    }

    l1.reverse(); 
    l1.show(); 
    return 0;
}
