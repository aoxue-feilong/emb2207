#include <iostream>

using namespace std;

int add(int a, int b)
{
    cout << "重载函数:int add(int a,int b)\n";
    return a + b;
}

float add(float a, float b)
{
    cout << "重载函数:float add(float a , float b)\n";
    return a + b;
}

string add(string &s1, string &s2)
{
    cout << "重载函数:string add(string &s1,string &s2)\n";
    return s1 + s2;
}

int main(int argc, char const *argv[])
{

    int x = 10, y = 20;
    int ret = add(x, y); // x 和y 是int 类型的, 因此会调用int add(int a,int b)
    cout << "ret=" << ret << endl;

    float m=10.5, n=20.6; 
    float ret1 = add(m,n);  // x 和y 是float 类型的, 因此会调用float add(float a, float b)
    cout << "ret1=" << ret1 << endl;

    string s1="hello", s2="world"; 
    string s3 = add(s1,s2); // x 和y 是string 类型的, 因此会调用string add(string &s1, string &s2)
    cout << "s3=" << s3 << endl;



    return 0;
}
