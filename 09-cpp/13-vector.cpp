#include <iostream>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
    string s1, s2;
    while (1)
    {
        cout << "请输入2个字符串 >:";
        cin >> s1 >> s2;
        cout << "s1=" << s1 << endl;
        cout << "s2=" << s2 << endl;
        try // 检测错误
        {
            if (s1 != s2)
            {
                throw runtime_error("s1 和 s2 必须相等,请重新输入"); // 报告错误
            }
        }
        // 捕捉错误
        catch (const runtime_error &e) // e 保存了这个字符串 "s1 和 s2 必须相等,请重新输入"
        {
            cerr << e.what() << endl; // 输出错误信息 "s1 和 s2 必须相等,请重新输入
            cerr << "try again enter y or n >:";
            char c;
            cin >> c;
            if (c == 'n' || !cin)
            {
                break; // while 语句 
            }
            else
            {
                continue; // 退出当前语句, 继续执行下一次
            }
        }
        cout << "s1 == s2\n";
        break;
    }

    return 0;
}
