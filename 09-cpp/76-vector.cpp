#include <iostream>
#include <string>
#include <vector>

using namespace std;
int main(int argc, char const *argv[])
{
    vector<int> v1; 
    cout<<"v1.size()="<<v1.size() <<endl;
/************************入队**********************************/
    cout<<"顺序表的入队顺序\n";
    for(int i=0;i<10;i++)
    {
        v1.push_back(i);
        cout<<i+1<<" ";
    }
    cout<<endl;
    cout<<"v1.size()="<<v1.size() <<endl;
/************************出队**********************************/
    cout<<"顺序表的出队顺序\n";
    for(int i=0;i<10;i++)
    {
        cout<<v1.front()<<" ";
        v1.erase(v1.begin());
    }
    cout<<endl;
    cout<<"v1.size()="<<v1.size() <<endl;
    return 0;
}
