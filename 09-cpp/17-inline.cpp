#include <iostream>

using namespace std;


// 因为内联函数是就地展开代码，不再产生调用跳转指令， 也不需要保存与恢复，执行效率更高   
inline int show(int x = 1, int y = 2, int z = 3)
{
    cout << "x=" << x << " y=" << y << " z=" << z << endl;
}

int main(int argc, char const *argv[])
{
    int  a=10,b=20,c=30 ; 

    // 代码会在这里进行展开 , 相当于就是 cout << "x=" << x << " y=" << y << " z=" << z << endl;
    // show 函数被 cout << "x=" << x << " y=" << y << " z=" << z << endl; 替换 
    
    show(a,b,c); 

    return 0;
}
