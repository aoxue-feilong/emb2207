#include <iostream> // in out stream
#include <string>
#include <fstream> // file stream

using namespace std;

int main(int argc, char const *argv[])
{

    // ofstream ios::out|ios::trunc
    // ifstream ios::in
    // fstream 无默认值

    if (argc != 3)
    {
        cout << "错误:运行程序时请带入参数 ./100-ifstream file1 file2" << endl;
        exit(-1);
    }

    // 读一个文件 , 不存在则报错 , 存在就打开
    ifstream file1(argv[1]); // 使用默认值 ios::in
    // 文件打开是否是失败的, 文件打开失败返回为真 , file1.fail(), 文件打开失败返回为真的
    if (file1.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }

    // 写一个文件 , 不存在则创建 , 存在就打开并清空
    ofstream file2(argv[2]); // 使用默认值 ios::out|ios::trunc
    // 文件打开是否是失败的, 文件打开失败返回为真 , file2.fail(), 文件打开失败返回为真的
    if (file2.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }

    char t; 
    // 文件是否到达了文件末尾
    // 函数返回值为假 表示读到了文件末尾 
    // 函数返回值为真 表示没有读到文件末尾 
    // t是读出来的字符 
    while ( file1.get(t)  )  
    {
        file2.put( t ) ; // 一次读取一个字节并写入到file2 当中
    }
    file1.close(); 
    file2.close(); 

    return 0;
}