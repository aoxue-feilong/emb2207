#include <iostream>
#include <string>
#include <list>
#include <forward_list>

using namespace std;

//单链表只能头部入，头部出
template <class T>
bool search(forward_list<T> &l1, T value)
{
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        if (*it == value)
        {
            return true;
        }
    }
    return false;
}

int main(int argc, char const *argv[])
{
    string citys[] = {"Beijing", "Shanghai", "Shenzhen", "Guangzhou", "Hangzhou",
                      "Suzhou", "Tianjin", "Chengdu", "Chongqing", "Nanjing"};
    forward_list<string> l1;
    //cout << "l1.size()=" << l1.size() << endl;
/******************************插入**********************************/ 
    cout << "插入单链表的顺序：";
    for (int i = 0; i < 10; i++)
    {
        l1.push_front(citys[i]);
    }

    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    //cout << "l1.size()=" << l1.size() << endl;
/******************************删除**********************************/
    cout << "删除单链表的顺序：";
    l1.remove("Beijing");
    l1.remove("Hangzhou");
    l1.remove("Nanjing");
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    //cout << "l1.size()=" << l1.size() << endl;
/******************************修改**********************************/
    cout << "修改单链表的顺序：";
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        if (*it == "Chongqing")
        {
            *it = "Wuhan";
        }
        if (*it == "Chongqing")
        {
            *it = "Harbin";
        }
    }
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
/******************************查找**********************************/
    //if(search <string>(l1,"Beijing))这种方法也可以，类似于强转
    if(search (l1,string("Beijing")))
    {
        cout << "Beijing found\n";
    }
    else
    {
        cout << "Beijing not found\n";
    }
    if (search(l1, string("Shenzhen")))
    {
        cout << "Shenzhen found\n";
    }
    else
    {
        cout << "Shenzhen not found\n";
    }
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
/******************************逆序**********************************/
    cout << "单链表逆序的内容为：";
    l1.reverse(); //逆序
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
/******************************默认排序方式**********************************/
    l1.sort(); //默认排序方式
    cout << "单链表默认排序的内容为：";
    for (auto it = l1.begin(); it != l1.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    return 0;
}
