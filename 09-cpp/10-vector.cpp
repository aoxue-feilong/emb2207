#include <iostream>
#include <string>
#include <vector>

using namespace std;
int main(int argc, char const *argv[])
{

    vector<int> v1;
    for (int i = 0; i < 10; i++)
    {
        v1.push_back(i); // 向链表中插入10个元素
    }
    cout << "v1.size()=" << v1.size() << endl; // 向量表的大小

    for (int i = 0; i < 5; i++)
    {
        cout << v1.back() << " "; // 访问链表中最后一个元素
        v1.pop_back() ;    // 删除最后一个元素
    }
    cout << endl;

    cout << "v1.size()=" << v1.size() << endl; // 向量表的大小
    for (int i : v1)
    {
        cout << i << " ";
    }
    cout << endl;


    cout << "v1.empty()="<<v1.empty()<<endl; // 判断v1是否为空 , 不为空返回假(0)
    for (int i = 0; i < 5; i++)
    {
        cout << v1.back() << " "; // 访问链表中最后一个元素
        v1.pop_back() ;    // 删除最后一个元素
    }
    cout << endl;


    cout << "v1.empty()="<<v1.empty()<<endl; // 判断v1是否为空 , 为空返回真(1)
    cout << "v1.size()=" << v1.size() << endl; // 向量表的大小

    return 0;
}
