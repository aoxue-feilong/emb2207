#include <iostream>
#include <string>
#include <deque>

using namespace std;

int main(int argc, char const *argv[])
{
    deque<int> d1; 
    cout<< "d1.size()="<<d1.size()<<endl;
/************************插入**********************************/
    cout<<"插入对列顺序:";
    for(int i=0;i<10;i++)
    {
        d1.push_back(i+1); 
        cout<<i+1<<" ";
    }
    cout<<endl;
    cout<< "d1.size()="<<d1.size()<<endl;

    cout<<"对列的内容为:";
    for(int i=0;i<d1.size();i++)
    {
        cout<<d1.at(i)<<" "; 
    }
    cout<<endl;
/************************删除**********************************/
    d1.erase(d1.begin() + 9); // d1.begin()+9  是第10个元素的位置
    d1.erase(d1.begin() + 4); // d1.begin()+4  是第5 个元素的位置
    d1.erase(d1.begin() + 0); // d1.begin()+0  是第1 个元素的位置
    cout<<"表的内容为:";
    for(int i=0;i<d1.size();i++)
    {
        cout<<d1.at(i)<<" "; 
    }
    cout<<endl;
    cout<< "d1.size()="<<d1.size()<<endl;
/************************修改**********************************/
    d1[6] = 99; //  访问第7个位置
    d1.at(0) = 22; // 访问第1个位置
    cout<<"表的内容为:";
    for(int i=0;i<d1.size();i++)
    {
        cout<<d1.at(i)<<" "; 
    }
    cout<<endl;
/************************查找**********************************/
    for (int i = 0; i < d1.size(); i++)
    {
        if (d1[i] == 22)
        {
            cout << "22 found" << endl;
            break;
        }
        if ((d1[i] != 22) && (i == d1.size() - 1))
        {
            cout << "22 not found" << endl;
        }
    }
    for (int i = 0; i < d1.size(); i++)
    {
        if (d1[i] == 100)
        {
            cout << "100 found" << endl;
            break;
        }
        if ((d1[i] != 100) && (i == d1.size() - 1))
        {
            cout << "100 not found" << endl;
        }
    }
    return 0;
}