#include <iostream>  // in out stream 
#include <string>
#include <fstream>  // file stream 

using namespace std;

int main(int argc, char const *argv[])
{

    // ofstream ios::out|ios::trunc
    // ifstream ios::in
    // fstream 无默认值

    // 只写 , 不存在创建 , 存在清空
    ofstream file1("test1.txt"); // 使用默认值 ofstream ios::out|ios::trunc
    // file1 想往文件内写入内容时, 可以把file1 当成cout  , cout <<  写入文件

    // 文件打开是否是失败的, 文件打开失败返回为真 , !file1.fail(), 文件打开失败返回为假的
    // !file1.fail(), 文件打开成功返回为真
    if (!file1.fail())
    {
        file1 << "student1 info" << endl; // 把file1 当成cout 去使用
        file1 << "student1 number:" << 1 << endl;
        file1 << "student1 name  :" << "ysl" << endl;
        file1 << "student1 age   :" << 22 << endl;
        file1 << "student1 score :" << 88 << endl;
        file1.close(); // 关闭文件
    }
    else
    {
        cout<<"file2 文件打开失败"<<endl;
    }


    //无默认值 , 必须给指定的参数, 否在不能创建文件
    //fstream file2("test2.txt"); // 无默认值  , 这种用法无法正常读写文件, 创建问价时失败
    fstream file2("test2.txt",ios::in|ios::out|ios::trunc); 

    // file2 想往文件内写入内容时, 可以把file2 当成cout  , cout <<  写入文件

    // 文件打开是否是失败的, 文件打开失败返回为真 , !file2.fail(), 文件打开失败返回为假的
    // !file2.fail(), 文件打开成功返回为真
    if (!file2.fail())
    {
        // file2  想往文件内写入内容时, 可以把file2 当成cout  , cout <<  写入文件 
        file2 << "student1 info" <<endl; // 把file2 当成cout 去使用 
        file2 << "student1 number:"<<1 <<endl;
        file2 << "student1 name  :"<<"ysl" <<endl;
        file2 << "student1 age   :"<<22 <<endl;
        file2 << "student1 score :"<<88 <<endl;
        file2.close(); 
    }
    else
    {
        cout<<"file2 文件打开失败"<<endl;
    }
    return 0;
}
