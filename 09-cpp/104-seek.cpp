#include <iostream> // in out stream
#include <string>
#include <fstream> // file stream
#include <string.h>

using namespace std;

// ./103-seek   part1.cpp  part2.cpp file2.cpp 

int main(int argc, char const *argv[])
{

    // ofstream ios::out|ios::trunc
    // ifstream ios::in
    // fstream 无默认值

    if (argc != 4)
    {
        cout << "错误:运行程序时请带入参数 ./104-seek  part1.cpp  part2.cpp file2.cpp  " << endl;
        exit(-1);
    }

    // 读一个文件 , 不存在则报错 , 存在就打开
    ifstream file1(argv[1]); // 使用默认值 ios::in
    // 文件打开是否是失败的, 文件打开失败返回为真 , file1.fail(), 文件打开失败返回为真的
    if (file1.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }

    // 读一个文件 , 不存在则报错 , 存在就打开
    ifstream file2(argv[2]); // 使用默认值 ios::in
    // 文件打开是否是失败的, 文件打开失败返回为真 , file2.fail(), 文件打开失败返回为真的
    if (file2.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }

    // 写一个文件 , 不存在则创建 , 存在就打开并清空
    ofstream file3(argv[3]); // 使用默认值 ios::out|ios::trunc
    // 文件打开是否是失败的, 文件打开失败返回为真 , file3.fail(), 文件打开失败返回为真的
    if (file3.fail())
    {
        cerr << "文件打开失败" << endl;
        exit(-1);
    }
    char ch ;
    
    while(file1.get(ch))//从 file1 读一个字节 
    {
        file3.put(ch); 
    }
    while(file2.get(ch))
    {
        file3.put(ch); 
    }
    file1.close();
    file2.close();
    file3.close();
    return 0;
}