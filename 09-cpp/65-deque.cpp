#include <iostream>
#include <string>
#include <deque>

using namespace std;

int main(int argc, char const *argv[])
{
    deque<int> d1; 
    cout<< "d1.size()="<<d1.size()<<endl;
/************************入队**********************************/
    cout<<"入队顺序:";
    for(int i=0;i<10;i++)
    {
        d1.push_back(i+1); 
        cout<<i+1<<" ";
    }
    cout<<endl;
    cout<< "d1.size()="<<d1.size()<<endl;
/************************出队**********************************/
    cout<<"出队顺序:";
    for(int i=0;i<10;i++)
    {
        cout<< d1.front() <<" "; // 获取第一个元素的值
        d1.pop_front();// 这个函数只是把最前的一个元素删除, 并不返回（移除队头的元素）
    }
    cout<<endl;
    cout<< "d1.size()="<<d1.size()<<endl;
    return 0;
}