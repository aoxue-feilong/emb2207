#include <iostream>
#include <string>

using namespace std;

/*
T 可以是 int , flaot , string


*/

template <class T>
class point
{
private:
    T x, y;

public:
    point(T xx, T yy);
    ~point();
    T getX();
    T getY();
    void showPoint();
};

template <class T>
point<T>::point(T xx, T yy)
{
    x = xx;
    y = yy;
}

template <class T>
point<T>::~point()
{
}

template <class T>
T point<T>::getX()
{
    return x;
}

template <class T>
T point<T>::getY()
{
    return y;
}

template <class T>
void point<T>::showPoint()
{
    cout << "(" << x << "," << y << ")" << endl;
}

int main(int argc, char const *argv[])
{
    // T 在实际参数是, 一定要传入具体的类型 
    // point<T> 带入实际类型 point<int>  
    // T 的值为int 
    point<int>  p1(1,2); 
    p1.showPoint(); 

    // T 在实际参数是, 一定要传入具体的类型 
    // point<T> 带入实际类型 point<int>  
    // T 的值为float
    point<float>  p2(1.3,2.9); 
    p2.showPoint(); 

    // T 在实际参数是, 一定要传入具体的类型 
    // point<T> 带入实际类型 point<int>  
    // T 的值为string
    point<string>  p3("5","9"); 
    p3.showPoint(); 
    return 0;
}

