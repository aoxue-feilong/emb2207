#include <iostream>
#include <string>
#include <vector>

using namespace std;
int main(int argc, char const *argv[])
{
    vector<int> v1; 
    cout<<"v1.size()="<<v1.size() <<endl;
/************************入栈**********************************/
    cout<<"顺序表的入栈顺序\n";
    for(int i=10;i>0;i--)
    {
        v1.push_back(i);
        cout<<i<<" ";
    }
    cout<<endl;  
    cout<<"v1.size()="<<v1.size() <<endl;
/************************出栈**********************************/
    cout<<"顺序表的出栈顺序\n";
    for(int i=0;i<10;i++)
    {
        cout<<v1.back()<<" ";
        v1.pop_back();
    }
    cout<<endl;
    cout<<"v1.size()="<<v1.size() <<endl;
    return 0;
}
