#include <stdio.h>
#include "kbd.h"
#include "command.h"
#include "insert.h"
#include "display.h"

int main(int argc, const char *argv[])
{


    display_kbd();
    display_command();
    display_insert();
    display();
    printf("hello world!!\n");
    return 0;
}
