#! /bin/bash


echo -n "please input filename >:"
read  filename 
echo "filename=$filename"

if ! [ -e $filename ]  # 文件不存在则报错  
then 
    echo "file not existed"
    exit 1
fi 

if [ -b $filename ]   # 块设备文件 
then 
    echo "block"
elif test -c $filename  # 字符设备文件
then 
    echo "character"
elif [ -d $filename ] #目录 
then 
    echo "directory"
elif [ -h $filename ]  # 符号链接
then 
    echo "link"
elif [ -f $filename ] # 普通文件
then 
    echo "regular"
elif [ -S $filename ]  # socket文件 
then 
    echo "socket"
elif [ -p $filename ]  # pipe文件 
then 
    echo "pipe"
fi


if [ -r $filename ]  # 可读 
then 
    echo "read"
fi

if [ -w $filename ]  # 可写 
then 
    echo "write"
fi

if [ -x $filename ]  # 可执行 
then 
    echo "exec"
fi
