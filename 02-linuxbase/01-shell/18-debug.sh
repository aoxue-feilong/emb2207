#! /bin/bash

set -x 
add()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 + $2`
    return $ret 
}
sub()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 - $2`
    return $ret 
}

mul()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 \* $2`
    return $ret 
}
div()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 / $2`
    return $ret 
}
yu()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 % $2`
    return $ret 
}






echo -n "please input 2 number >:"
read n1 n2 
echo "n1=$n1 , n2=$n2"

if [ -z $n1 ] || [ -z $n2 ]
then 
    echo "n1 or n2 empty"
fi 

add $n1 $n2     
# 位置变量
# $0 = add 
# $1 = $n1 
# $2 = $n2
# 函数的返回值 通过$? 获取 
echo "$n1 + $n2 = $? "

sub $n1 $n2     
echo "$n1 - $n2 = $? "

mul $n1 $n2     
echo "$n1 * $n2 = $? "

div $n1 $n2     
echo "$n1 / $n2 = $? "

yu $n1 $n2     
echo "$n1 % $n2 = $? "


set +x 
