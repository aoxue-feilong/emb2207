#! /bin/bash


i=0
sum=0
until ! [ $i -lt 100 ] 
do 
    i=`expr $i + 1 `    # i = i + 1 
    echo "i=$i"
    sum=`expr $sum + $i `
done
echo "sum=$sum"
