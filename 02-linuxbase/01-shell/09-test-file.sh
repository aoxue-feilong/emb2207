#! /bin/bash


echo -n "please input filename >:"
read  filename 
echo "filename=$filename"

if ! test -e $filename   # 文件不存在则报错  
then 
    echo "file not existed"
    exit 1
fi 

if test -b $filename   # 块设备文件 
then 
    echo "block"
elif test -c $filename  # 字符设备文件
then 
    echo "character"
elif test -d $filename  #目录 
then 
    echo "directory"
elif test -h $filename   # 符号链接
then 
    echo "link"
elif test -f $filename   # 普通文件
then 
    echo "regular"
elif test -S $filename   # socket文件 
then 
    echo "socket"
elif test -p $filename   # pipe文件 
then 
    echo "pipe"
fi


if test -r $filename   # 可读 
then 
    echo "read"
fi

if test -w $filename   # 可写 
then 
    echo "write"
fi

if test -x $filename   # 可执行 
then 
    echo "exec"
fi
