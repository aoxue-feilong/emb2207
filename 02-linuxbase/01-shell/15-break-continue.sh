#! /bin/bash



sec=0
min=0
hour=0
count_sec=0
while  true 
do 
    echo "HH:MM:SS -> $hour:$min:$sec"
    count_sec=`expr $count_sec + 1`
    sec=`expr $sec + 1`
    if [ $sec -lt 60 ]
    then 
        continue 
    fi 
    sec=0
    min=`expr $min + 1`
    if [ $min -lt 60 ]
    then 
        continue 
    fi 
    min=0
    hour=`expr $hour + 1`
    if [ $hour -lt 24 ]
    then 
        continue 
    fi 
    hour=0
    break

done  
echo "count_sec=$count_sec"
