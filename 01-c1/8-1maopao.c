#include <stdio.h>
#define P 7
int main(int argc, char const *argv[])
{
    int a[P] = {0};
    int t;
    for (int i = 0; i < 7; i++)
    {
        printf("请输入第%d 个数>>", i + 1); //数组的输入
        scanf("%d", &a[i]);
    }
    printf("数组的内容为>>>>>");
    for (int i = 0; i < 7; i++)
    {
        printf("a[%d]=%d\n", i, a[i]);    //数组的输出
    }
    printf("\n");                         //以上为数组的输入和输出
    for (int i = 0; i < P - 1; i++)       //冒泡排序，控制需要多少次排序
    {
        for (int j = 0; j < P - 1 - i; j++) //控制每一次冒泡排序需要比较的次数
        {
            if (a[j] > a[j + 1])          //判断两个元素的大小，大则交换，即从小到大排序，如果大到小排序，即可改变这儿的大于号即可
            {
                t = a[j];                 //交换这两个数
                a[j] = a[j + 1];
                a[j + 1] = t;
            }
        }
    }
    printf("冒泡排序的内容为>>>>>>>>>>\n");
    for (int i = 0; i < P; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}