//分支语句if()   slse)//例子键盘输入2个整数，程序中使用=、>或<符号判断其大小，打印出来
#include <stdio.h>
int main()
{
  int a, b;
  printf("请输入两个整数");
  scanf("%d%d", &a, &b);
  printf("a=%d,b=%d\n", a, b);
  if (a > b)
  {
    printf("%d > %d\n", a, b);
  }
  else if (a < b)
  {
    printf("%d < %d\n", a, b);
  }
  else
  {
    printf("%d=%d\n", a, b);
  }
}