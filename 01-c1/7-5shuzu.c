#include <stdio.h>
int main(int argc, char const *argv[])
{
    int a1[6];             //
    printf("a1=%p\n", a1); //数组名是数组的起始地址，也是数组第一个元素的地址
    //计算数组占用内存的字节数，数组中一共有6个元素，每个元素都是int类型
    //一个int类型的变量占4个字节，4*6=24字节，sizeof(数组名)来计算数组占用的字节数
    printf("sizeof(a)=%d\n", sizeof(a1));
    int a[6];
    a[0] = 1;
    a[1] = 2;
    a[2] = 3;
    a[3] = 4;
    a[4] = 5;
    a[5] = 6;
    for (int i = 0; i < 6; i++)
    {
        printf("a[%d]=%d\n", i, a[i]);
    }

    return 0;
}
