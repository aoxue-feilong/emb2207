#include <stdio.h>
#define N 10
int main(int argc, char const *argv[])
{
    int a[N] = {0};
    for (int i = 0; i < N; i++)
    {
        printf("请输入第%d个数>", i + 1);
        scanf("%d", &a[i]);
    }
    for (int i = 0; i < N; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
    printf("选择排序从小到大排序>");
    for (int i = 0, t, k; i < N - 1; i++)
    {
        k=0;
        for (int j = 0; j < N - 1 - i; j++)
        {
           
            if (a[k] < a[j + 1])
            {
                k = j + 1;
            }
        }
        if (k != N - 1 - i)
        {
            t = a[k];
            a[k] = a[N - 1 - i];
            a[N - 1 - i] = t;
        }
    }

    for (int i = 0; i < N; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
    printf("选择排序从大到小排序>");
    for (int i = 0, t, k; i < N - 1; i++)
    {
        k=0;
        for (int j = 0; j < N - 1 - i; j++)
        {
           
            if (a[k] > a[j + 1])
            {
                k = j + 1;
            }
        }
        if (k != N - 1 - i)
        {
            t = a[k];
            a[k] = a[N - 1 - i];
            a[N - 1 - i] = t;
        }
    }
    for (int i = 0; i < N; i++)
    {
        printf("%d ", a[i]);
    }

    return 0;
}
