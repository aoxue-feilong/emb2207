#include <stdio.h>
int main(int argc, char const *argv[])
{
    printf("sizeof(char)=%d\n",sizeof(char));               //字符类型   计算char占用的字节数
    printf("sizeof(short)=%d\n",sizeof(short));             //短整型     计算short占用的字节数
    printf("sizeof(int)=%d\n",sizeof(int));                 //整型型     计算int占用的字节数
    printf("sizeof(long)=%d\n",sizeof(long));               //长整型     计算long占用的字节数
    printf("sizeof(long int)=%d\n",sizeof(long int));       //长整型     计算long int占用的字节数
    printf("sizeof(long long)=%d\n",sizeof(long long));     //长整型     计算long long占用的字节数
    printf("sizeof(float)=%d\n",sizeof(float));             //单精度浮点数     计算float占用的字节数
    printf("sizeof(double)=%d\n",sizeof(double));           //双精度浮点数     计算double占用的字节数
    printf("sizeof(long double)=%d\n",sizeof(long double)); //长双精度浮点数   计算long double占用的 字节数
    printf("sizeof(void)=%d\n",sizeof(void));               //           计算void占用的 字节数
    return 0;
}
