#include <stdio.h>
int main(int argc, char const *argv[])
{
    unsigned char x = 0x17;
    unsigned char y = ~x; //位逻辑反
    printf("y=%x\n", y);
    //位逻辑与
    unsigned char z;
    x = 0126, y = 0xac; // x:八进制 0101 0110 y；16进制1010 1100
    z = x & y;
    printf("z=%x\n", z);
    //位逻辑或
    x = 076, y = 0x89;   // oo11 0110
    z = x | y;           // 1010 1100
    printf("z=%x\n", z); // 0000 0100 =0x4
    //位逻辑异或
    x = 75, y = 0173; // x=0100 1011（相同为0，不同为1）
    z = x ^ y;        // y=0111 1011
    printf("z=%x\n", z);
    //左移运算符(高位舍弃，低位补0)
    x = 0xe4;   // 1110 0100
    z = x << 3; // 0010 0000
    printf("z=%x\n", z);
    //右移
    char a = -10 ;//-10的绝对值为10，按位取反 1111 0101再加1 1111 0110（这就是-10的补码）
    char b = a >> 3;//1111 0110 >>3,高位补符号位1 1111 1110，最高位为1，表示这个数是负数
    printf("b=%d\n", b);//原码=~（补码-1）
    printf("b=%x\n", b);// %x是unsigned int 是4个字节//原码=1000 0010 //-2
    return 0;
}
