#include <stdio.h>
int main(int argc, char const *argv[])
{
  int m = 0, n = 0;
  printf("请输入了两个数>");
  scanf("%d%d", &m, &n);
  printf("m=%d,n=%d\n", m, n);
  int max = m > n ? m : n;
  /*解题思路；让m是最大值，n是最小值，可以使用增加m去找最小公倍数，n减少去找最大公约数
  *计算最小公倍数，找到m和n,让最大值增加，分别整除m和n，同时整除时就是最小公倍数
  */
  for (; max > 0; max++)
  {
    if ((max % m == 0) && (max % n == 0))
    {
      printf("最小公倍数为：%d\n", max);
      break;
    }
  }
    int min = m < n ? m : n;
   for (; min > 0; min--)
   {
    if ((m % min == 0) && (n % min == 0))
    {
      printf("最大公约数为：%d\n", min);
      break;
    }
   }
   
}
