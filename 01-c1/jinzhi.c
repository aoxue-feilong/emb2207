#include <stdio.h>
int main(int argc, char const *argv[])
{
    unsigned int a=0b1010; //二进制以0b开头
    unsigned int b=01010;  //8进制以0开头
    unsigned int c=1010;   //10进制没有前缀
    unsigned int d=0x1010; //16进制以0x开头
    unsigned int e=0X1010; //16进制以0X开头
    printf("a=%d\n",a);    //按照十进制输出
    printf("b=%d\n",b);    //按照十进制输出
    printf("c=%d\n",c);    //按照十进制输出
    printf("d=%d\n",d);    //按照十进制输出
    printf("e=%d\n",e);    //按照十进制输出
    return 0;
}
