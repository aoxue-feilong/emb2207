#include <stdio.h>
int main(int argc, char const *argv[])
{
    int a = 15, b = 9, c;
    double x = 15, y = 8, z;
    c = a + b;
    printf("c=%d\n", c);
    c = a - b;
    printf("c=%d\n", c);
    c = a * b;
    printf("c=%d\n", c);
    c = a / b;
    printf("c=%d\n", c);
    c = a % b;
    printf("c=%d\n", c);
    z = x + y;
    printf("z=%f\n", z);
    z = x - y;
    printf("z=%f\n", z);
    z = x * y;
    printf("z=%f\n", z);
    z = x / y;
    printf("z=%f\n", z);
    //实数不能求余  错误用法z=x%y
    return 0;
}
