#include <stdio.h>
#include <math.h>
int main(int argc, char const *argv[])
{
    float a, b, c, x1, x2, p;
    printf("请输3个数值作为方程的值");
    scanf("%f%f%f", &a, &b, &c);
    printf("a=%f,b=%f,c=%f\n", a, b, c);

    if (((b * b) - 4 * a * c) > 0)
    {
        p = sqrt((b * b )- (4 * a * c));
        x1 = ((-b + p )/( 2 * a));
        x2 = ((-b - p )/(2 * a));
        printf("x1=%f\n", x1);
        printf("x2=%f\n", x2);
    }
    else
    {
        printf("表示方程无实根，\n");
    }
    return 0;
}
