#include <stdio.h>
int main(int argc, char const *argv[])
{
    int a = 10, b = 8;
    a += b;
    printf("a=%d\n", a);

    a = 10, b = 8;
    a -= b;
    printf("a=%d\n", a);

    a = 10, b = 8;
    a *= b;
    printf("a=%d\n", a);

    a = 10, b = 8;
    a /= b;
    printf("a=%d\n", a);

    a = 10, b = 8;
    a |= b;
    printf("a=%d\n", a);

    a = 10, b = 8;
    a ^= b;
    printf("a=%d\n", a);

    a = 10, b = 8;
    a <<= b;
    printf("a=%d\n", a);

    a = 10, b = 8;
    a >>= b;
    printf("a=%d\n", a);

    return 0;
}
