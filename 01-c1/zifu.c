#include <stdio.h>
int main(int argc, char const *argv[])
{
    char a,b,u,v;//说明a,b,u,v的值为字符变量
    a='F' ;      //'F'asciid的值为70
    b='A'+2 ;    //b存放的是c字符
    u=' '+'B';   //大小写转换，即值为b
    v='b'-32 ;   //v存放的是B字符
    printf("a=%d\n",a);
    printf("b=%d\n",b);
    printf("u=%d\n",u);
    printf("v=%d\n",v);
    return 0;
}
