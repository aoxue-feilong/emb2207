#include <stdio.h>
int main(int argc, char const *argv[])
{
    int a1[5];
    for (int i = 0; i < 5; i++)
    {
        printf("a1[%d]=%d\n", i, a1[i]); //定义一个数组，不对其初始化，其值是随机值
    }
    printf("***************\n");
    int a2[5] = {1, 2, 3};
    for (int i = 0; i < 5; i++)
    {
        printf("a2[%d]=%d\n", i, a2[i]); //只给部分元素赋初值，其余元素为0
    }
    printf("***************\n");
    static int a3[7];
    for (int i = 0; i < 7; i++)
    {
        printf("a3[%d]=%d\n", i, a3[i]); // static当数组元素不赋初始值，系统自动赋0
    }
    printf("***************\n");
    int a4[] = {3, 4, 5, 6, 7, 8, 9}; //当全部元素赋初始值时，可不指定数组宽度，会根据初始化的个数，自动确定长度
    for (int i = 0; i < 7; i++)       // static也是一样的
    {
        printf("a4[%d]=%d\n", i, a4[i]);
    }
    return 0;
}
