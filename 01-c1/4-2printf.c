#include <stdio.h>
int main(int argc, char const *argv[])
{
  // m 指定输出指定输出宽度
  int a = 567;
  printf("a=%d\n", a);
  printf("a=%6d\n", a); //按照10进制输出整数，实际输出<m，指定输出宽度6个位置，不够左补3个空格
  printf("a=%2d\n", a); //按照10进制输出整数，实际输出>m，指定输出宽度2个位置，超出设定值，按照实际输出
  //.n实数，指定小数点后后几位数；四舍五入
  //对字符串，指定实际输出位数
  float b = 456.789;
  printf("b=%f\n", b);             //输出一个实数，小数点后保留6位
  printf("b=%.2f\n", b);           //输出一个实数，小数点后保留2位
  printf("b=%8.2f\n", b);          //输出一个实数，小数点后保留2位，指定宽度8个位置，不够左补2个空格
  printf("ch=%s\n", "goodbye");    //输出一个字符串，输出全部字符
  printf("ch=%.3s\n", "goodbye");  //输出一个字符串，输出有效3字符
  printf("ch=%3.4s\n", "goodbye"); //输出一个字符串，输出有效4字符，输出3个有效字符
                                   //#在8进制和16进制前显示到0,0x
  printf("a=%o\n", a);
  printf("a=%#o\n", a);
  printf("a=%#e\n", a);
  printf("a=%#E\n", a);

  //默认右对齐，可以使用-来实现左对齐
  printf("ch=%8s\n", "goodbye");   //右对齐，左补1个空格
  printf("a=%8d\n", a);            //左对齐，右补一个空格
  printf("b=%-8.2s\n", "goodbye"); //右对齐，左补5个空格
  printf("a=%-8.2d\n", a);         //左对齐，右补5个空格

  //+ 指定在有符号符数的正数前显示正号
  printf("a=%+d\n", a);  //按照10进制输出，带上符号位，即+ -
  printf("a=%+2d\n", a); //按照10进制输出，指定输出宽度为2，带上符号位，即+ -
  int c = -567;
  printf("c=%+8d\n", c); //按照10进制输出，带上符号位，即+ -
  printf("b=%+8f\n", b); //按照小数形式输出，指定输出宽度为8个位置，带上符号位，即+ -
  // 0 不使用空格填充，使用0填充
  //补0都是左边补
  //字符串补0没有意义
  printf("a=%08d\n", a); //右对齐，左补5个0
  // l在d,0,x,u前，指定为精度  long类型
  // l在e,f,g面前，指定输出精度 double类型
  printf("a=%ld\n", a); // long 类型的数据
  printf("b=%lf\n", b); // double 类型的数据

  return 0;
}
