#include <stdio.h>
int main(int argc, char const *argv[])
{
    int x = 5, y = 6.,m;
    m=(x > 5) && (y++);//第一个表达式为假，结果为假，表达式二不运算a=5，
    printf("x=%d,y=%d\n", x,y);
    m=(x>=5) && (y++);
    printf("x=%d,y=%d\n", x,y);

    x = 5, y = 18;
    int ret;
    ret = (x >= 5) && (y < 20);
    printf("1:ret=%d\n", ret);
    ret = ((x + 1) >= 0) && (y < 17);
    printf("2:ret=%d\n", ret);
    ret = ((x - 8) >= 0) && (y == 18);
    printf("3:ret=%d\n", ret);
    ret = ((x - 5 >= 0)) && (y != 18);
    printf("4:ret=%d\n", ret);
    ret = ((x >= 5)) || (y < 20);
    printf("5:ret=%d\n", ret);
    ret = ((x + 1) >= 0) || (y < 17);
    printf("6:ret=%d\n", ret);
    ret = ((x - 8) >= 0) || (y == 18);
    printf("7:ret=%d\n", ret);
    ret = ((x - 5) > 0) || (y != 18);
    printf("8:ret=%d\n", ret);
    return 0;
}
