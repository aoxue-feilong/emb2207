#include <stdio.h>
int main(int argc, char const *argv[])
{
#if 0
    int i = 2, b = 0;
    for (int i = 2; i < 100; i++)
    {
        for (int j = 1; j <= i; j++)
        {
            if (i % j == 0)
                b++; //通过公因数判断是否为素数
        }
        if (b == 2)
        {
            printf("%d ", i);
            b = 0;
        }
        else
        {
            b = 0;
        }
    }
#endif
#if 1
    for (int i = 2; i < 100; i++)
    {
        if (i == 2)
            printf("%d ", i); //特殊情况判断，直接输出
        for (int j = 2; j < i; j++)//从2开始整除，一直到这个数本身
        {
            if (i % j == 0)//如果能够被整除，就不是素数
                break;
            if (i > j / 2)//如果j都循环到了i/2还没有整除了，就不可能整除了，这个数就是素数
            {
                printf("%d ", i);
                break;//找到后，立即退出循环，否则会出现重复的数据
            }
        }
    }
#endif
    return 0;
}
