#include <stdio.h>
int main(int argc, char const *argv[])
{
    printf("hello world!!\n");//输出表没有，普通字符原样输出

    int x = 10, y= 20,z=30;
    printf("x=%d\n",x);//输出表有一个
    printf("x=%d,y=%d\n,z=%d\n",x,y,z);//输出表有多个，用，隔开

    int a = 571;
    printf("a=%d\n", a);//输出带符号的10进制
    printf("a=%i\n", a);//输出带符号输出10进制整数
    printf("a=%u\n", a);//输出无符号10进制整数
    printf("a=%x\n", a);//输出无符号16进制整数
    printf("a=%X\n", a);//输出无符号16进制整数
    printf("a=%o\n", a);//输出无符号8进制整数

    a = 'Y';
    printf("a=%c\n", a);//输出单一字符，与putchar()的功能一样
    printf("%s\n", "helloworld");//输出字符串，字符串带用双引号 引起来
    float c = 567.789;
    printf("c=%e\n", c);//输出实数或者指数形式浮点小数，(e)
    printf("c=%E\n", c);//输出实数或者指数形式浮点小数,(E)
    printf("c=%f\n", c);//输出实数或者小数形式浮点小数
    printf("c=%g\n", c);//输出比e和f中较短的一种
    printf("&c=%p\n", &c);//输出C的内存地址
    printf("%%\n");//输出百分号本身
    return 0;
}
