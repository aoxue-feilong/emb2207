#include <stdio.h>
int main(int argc, char const *argv[])
{
    printf("1/2=%d\n",1/2);
    printf("1.0/2=%f\n",1.0/2);
    printf("1.0/2.0=%f\n",1.0/2.0);
    printf("1/2.0=%f\n",1/2.0);
    return 0;
}
