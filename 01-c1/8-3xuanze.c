#include <stdio.h>
#define N 10
int main(int argc, char const *argv[])
{
    int a[N] = {0};
    for (int i = 0; i < N; i++)
    {
        printf("请输入第%d个数", i + 1);
        scanf("%d", &a[i]);
    }
    printf("数组的内容为>");
    for (int i = 0; i < N; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
    for (int i = 0, t, k; i < N - 1; i++)//控制一共需要多少次排序
    {
        k = 0;//用来记录最大元素的下标
        for (int j = 0; j < N - 1-i; j++)//控制每一次选择排序需要比较的次数
        {
            if (a[k] < a[j + 1])//如果a[k]<a[j+1]则记录k=j+1
            {
                k = j + 1;
            }
        }
        //排序一次后，要进行一次交换a[k]和最后一个元素a[n-1-i]交换
        if (k != N - 1 - i)//如果k就是最后一个元素，就不需要交换了
        {
            t = a[k];
            a[k] = a[N - 1 - i];
            a[N - 1 - i] = t;
        }
    }

    printf("小到大排序的内容为>");
    for (int i = 0; i < N; i++)
    {
        printf("%d ", a[i]);
    }
     printf("\n");
    for (int i = 0, t, k; i < N - 1; i++)//控制一共需要多少次排序
    {
        k = 0;//用来记录最大元素的下标
        for (int j = 0; j < N - 1-i; j++)//控制每一次选择排序需要比较的次数
        {
            if (a[k] > a[j + 1])//如果a[k]<a[j+1]则记录k=j+1
            {
                k = j + 1;
            }
        }
        //排序一次后，要进行一次交换a[k]和最后一个元素a[n-1-i]交换
        if (k != N - 1 - i)//如果k就是最后一个元素，就不需要交换了
        {
            t = a[k];
            a[k] = a[N - 1 - i];
            a[N - 1 - i] = t;
        }
    }

    printf("大到小排序的内容为>");
    for (int i = 0; i < N; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
    return 0;
}
