//处理垃圾字符的第一种方法       //空格的思路
#include <stdio.h>
int main()
{
  int a;
  char b;
  printf("请输入一个整数>="); //从终端读取一个十进制的整数
  scanf("%d", &a);
  printf("请输入一个字符>=");
#if 0
  scanf(" %c",&b);                        //第一种处理垃圾字符的方法：利用空格，处理一个垃圾字符
#endif
#if 0 //第2种处理垃圾字符的方法：利用getchar()，处理一个垃圾字符
  getchar();                              //h获取一个字符不接受
  scanf("%c",&b);                         //从终端读取一个字符
#endif
#if 0 //第三种处理垃圾字符的方法：利用%*c，处理一个垃圾字符
  scanf("%*c%c",&b);                       //从终端读取一个字符
#endif

#if 1               //第四种处理垃圾字符的方法：利用%*[^\n]，处理多个垃圾字符
  scanf("%*[^\n]"); //抑制除了'\n'以外的所有的字符
  scanf("%*c");     //在抑制一个'\n'
  scanf("%c", &b);
#endif

  printf("b=%c\n", b);//# if 为 预处理命令，1即执行，0则不执行
  printf("a=%d\n", a);
  printf("a=%d\n", a);
}
