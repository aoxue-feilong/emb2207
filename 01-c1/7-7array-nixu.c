#include <stdio.h>
int main(int argc, char const *argv[])
{
    int a1[10] = {0};
    int i;
    int t;
    for (i = 0; i < 10; i++)
    {
        printf("请输入第%d 个元素>>>", i + 1);//数组的存入
        scanf("%d", &a1[i]);
    }
    printf("数组的内容为：\n");//数组的输出
    for (i = 0; i < 10; i++)
    {
        printf("a1[%d]=%d\n", i, a1[i]);
    }
    for (i = 0; i < 10 / 2; i++)//交换第一个数和最后一个数
    {
        t = a1[i];
        a1[i] = a1[10 - 1 - i];
        a1[10 - 1 - i] = t;
    }
    printf("逆序后的内容为>>>>>\n");
    for (i = 0; i < 10; i++)
    {
        printf("a1[%d]=%d\n", i, a1[i]);
    }

    return 0;
}
