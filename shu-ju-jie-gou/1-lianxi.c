#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// 1.*p在最空表头里面申请内存，后面直接用
// 2.总结错误：if里面的判断相等是==，老写成=
// 3.写链表逆序，思路跟不上
typedef struct node
{
    int data;
    struct node *next;
} linklist_t;

linklist_t *create_empty_linklist_head(void)
{
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->data;
    h->next = NULL;
    return h;
}
int linklist_insert(linklist_t *h, int value)
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->data = value;
    p->next = h->next;
    h->next = p;
    return 0;
}

int linklist_delete(linklist_t *h, int value)
{
    linklist_t *p = h;
    linklist_t *q;
    while (p->next != NULL)
    {
        if (p->next->data == value)
        {
            q = p->next;
            p->next = q->next;
            free(q);
            break;
        }
        p = p->next;
    }
    return 0;
}
int linklist_modify(linklist_t *h, int old, int new)
{
    linklist_t *p = h->next;
    while (p != NULL)
    {
        if (p->data == old)
        {
            p->data = new;
            break;
        }
        p = p->next;
    }
    return 0;
}
int linklist_search(linklist_t *h, int value)
{
    linklist_t *p = h->next;
    while (p != NULL)
    {
        if (p->data == value)
        {
            return 1;
        }
        p = p->next;
    }
    return 0;
}
int linklist_reverse(linklist_t *h)
{
//一.首先定义两个指针指向结构体
//  1.定义p指针指向头结点的下一个结点
//  2.定义q指针（用来保存删除的结点）
//二.把表头置位空头
//三.判断是否还有结点
//四.结构体指针q指向p的下一个结点
//五.连接链表的两步
//六.让p=q，相当于移动结点
    linklist_t *p = h->next;
    linklist_t *q;
    h->next=NULL;
    while (p!=NULL)
    {
       q=p->next;
       p->next=h->next;
       h->next=p;
       p=q;
    }
    return 0;
}
int linklist_show(linklist_t *h)
{
    linklist_t *p = h->next;
    printf("链表的内容为：");
    while (p != NULL)
    {
        printf("%d ", p->data);
        p = p->next;
    }
    printf("\n");
    return 0;
}

int main(int argc, char const *argv[])
{
    linklist_t *H = create_empty_linklist_head();
    for (int i = 0; i < 10; i++)
    {
        linklist_insert(H, i + 1);
    }
    linklist_show(H);
    linklist_delete(H, 5);
    linklist_delete(H, 1);
    linklist_delete(H, 10);
    linklist_show(H);
    linklist_modify(H, 3, 33);
    linklist_modify(H, 2, 22);
    linklist_modify(H, 9, 99);
    linklist_show(H);
    if (linklist_search(H, 2))
    {
        printf("2 is found\n");
    }
    else
    {
        printf("2 not found\n");
    }

    if (linklist_search(H, 99))
    {
        printf("99 is found\n");
    }
    else
    {
        printf("99 not found\n");
    }
    linklist_reverse(H);
    linklist_show(H);
    return 0;
}
