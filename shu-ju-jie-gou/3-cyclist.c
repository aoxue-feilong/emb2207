#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//删除的时候，删除函数实参的时候要接受函数的返回值
typedef struct node
{
    int data;
    struct node *next; // 指向本结构体的指针
} linklist_t;

linklist_t *create_empty_cyclist_head(void)
{
    // 一.申请一段结构体大小的内存，并用结构体指针来保存这段内存
    // 二.想象空表头的内存图，写出h->data=1; h->next=h;在循环链表中头结点参与运算
    // 三.返回申请内存的地址
    // 四.设置函数类型：即函数的返回值类型-结构体类型
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->data = 1; // 循环链表的特点是, 头结点也要参与运算
    h->next = h; // 让头节点的下一个节点指向头部
    return h;
}
// 插入循环链表
int cyclist_insert(linklist_t *h, int value)
{
    // 一.申请一段结构体大小的内存，并用结构体指针来保存这段内存
    // 二.想象插入结点时的内存图
    // 三.让新添加的结点 h->data=value;
    // 四.接链表的两步
    //    1. p->next=h->next;//接链表第一步
    //    2. h->next=p;      //接链表第二步
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->data = value;
    p->next = h->next; // 接链表第1步
    h->next = p;       //接链表的第2步
    return 0;
}
// 显示循环链表中的内容
int cyclist_show(linklist_t *h)
{
    // 一. 1.让指针p指向头结点的下一个结点
    //     2.打印单循环链表的内容:
    // 二. 1.判断p ！=h为循环了一遍并移动结点
    //     2.输出p->data中的内容
    // 三. 1.判断p == h ，表示最后一个元素
    //     2.输出p指向结点 p->data中的内容
    // 四. 打印换行符并且返回0
    linklist_t *p = h->next; // 让p指向头节点下一个节点, 让头节点作为最后一个节点使用
    printf("单循环链表的内容:");
    while (p != h) // p->next == h 是表示循环一圈
    {
        printf("%d ", p->data); // 输出p指向节点的内容
        p = p->next;            // 移动节点
    }
    // p == h
    if (p == h) // p指向的h了, 作为循环表的最后一个元素
    {
        printf("%d ", p->data); // 输出p指向节点的内容
    }
    printf("\n");
    return 0;
}
// 循环表删除节点时, 要定位到要删除节点的前一个节点
// 如果要删除的节点是头结点 , 必须要更新一个新的头结点
linklist_t *cyclist_delete(linklist_t *h, int value)
{
// 一. 1.让指针p指向h
//     2.定义q指针(用来保存删除的结点)
// 二. 1.判断循环一圈
//     2.移动p结点
// 三. 1.判断p结点的下一个结点是否等于value
//     2.q = p->next; //要删除的节点
//     3.p->next = q->next; // 连接链表
//     4.释放q 
//     5.返回h             
// 四. 1.判断是否删除的头结点
//     2.q = p->next; //要删除的节点
//     3.p->next = q->next; // 连接链表
//     4.释放q 
//     5.返回p              //删除的是头结点，要更新头结点             
    linklist_t *p = h; // P 指向h节点
    linklist_t *q;
    while (p->next != h) //循环一圈少一个
    {
        if (p->next->data == value)
        {
            q = p->next;       // 要删除的节点
            p->next = q->next; // 连接链表
            free(q);
            return h; //  函数要返回, 不返回在最后一个节点时, 会报错,
        //如果删除的是最后一个节点，没有返回地址时，将移动空指针，程序会自动崩溃
        }
        p = p->next; //p节点往后移动
    }
    if (p->next == h) // 表示要删除的是头结点
    {
        q = p->next;       // 要删除的节点
        p->next = q->next; // 连接链表
        free(q);           // 释放节点
        return p;          // 此时删除的是头结点, 要返回一个新的头
    }
}
//修改函数
int cyclist_modify(linklist_t *h, int old, int new)
{
// 一.让指针p指向头结点的下一个结点
// 二. 1.判断p！=h为循环了一遍并移动结点
//     2.判断p->data == old
//     3.让p->data = new;并返回值为值
// 三. 1.判断p == h ，表示最后一个元素
//     2.p->data=new;
//     3.返回0;
    linklist_t *p = h->next;
    while (p != h)
    {
        if (p->data == old)
        {
            p->data = new;
            return 0;
        }
        p=p->next;
    }
    if(p==h)
    {
        p->data=new;
        return 0;
    }
    return 0;
}
//查找函数
int cyclist_search(linklist_t *h, int value)
{

    linklist_t *p = h->next;
    while (p != h)
    {
        if (p->data == value)
        {
            return 1;
        }
        p = p->next;
    }
    if(p==h)
    {
        p->data=value;
        return 1;
    }
    return 0;
}
// 因为链表的头节点, 我们把他处理为最后一个节点, 要做逆序, 必须把头结点处理为
// 第一个节点, 最先输出, 只需要把头结点往前移动一个位置即可, 头节点变为第一个节点
linklist_t * cyclist_reverse(linklist_t * h)
{
//一.首先定义两个指针指向结构体
//   1.定义p指针指向头结点的下一个结点
//   2.定义q指针（用来保存p的下一个结点）
//二.把p指向的节点循环插入到h的后面
//   1.判断 p!= h
//   2.q = p->next ; // 保存下一个要插入的节点
//   3.接链表的第1步 接链表的第二步
//   4.p=q; 把p结点移动到q结点
//三.// 处理最后一个节点, 把头节点往后移动一个位置
//    要找到头节点的前一个位置
//    p = h ;  // 让p 指向h 
//四. 1.判断p->next != h
//    2.移动p结点
//五. 1.判断p->next == h
//    2.返回p的地址，作为一个新头
    linklist_t *p=h->next;//让指针p指向头结点的下一个结点
    h->next=h;            //把头结点设置为空结点
    linklist_t *q;
    // 把p指向的节点循环插入到h的后面
    while(p != h) 
    {
        q = p->next ; // 保存下一个要插入的节点
        p->next = h->next ; // 接链表的第1步
        h->next = p ; // 接链表的第2步
        p = q ; // 把p和q 保持一致 
    }
    //  处理最后一个节点, 把头节点往后移动一个位置
    // 要找到头节点的前一个位置
    p = h ;  // 让p 指向h 
    while(p->next != h)//执行完p=q，p和q在一处，移动p让 P!=q
    {
        p = p->next ; 
    }
    if(p->next == h)//更新一个新的头结点
    {
        return p ;//返回p的地址
    }
    return h ; //返回h的地址
}
int main(int argc, char const *argv[])
{
    linklist_t *H = create_empty_cyclist_head();
    for (int i = 2; i < 11; i++)
    {
        cyclist_insert(H, i);
    }
    cyclist_show(H);
    H = cyclist_delete(H, 10);
    H = cyclist_delete(H, 1);
    H = cyclist_delete(H, 5);
    cyclist_show(H);
    cyclist_modify(H,2,22);
    cyclist_modify(H,9,99);
    cyclist_show(H);
    if (cyclist_search(H, 99))
    {
        printf("99 found\n");
    }
    else
    {
        printf("99 not found\n");
    }
    if (cyclist_search(H, 88))
    {
        printf("88 found\n");
    }
    else
    {
        printf("88 not found\n");   
    }
    H=cyclist_reverse(H);
    cyclist_show(H);
    H=cyclist_reverse(H);
    cyclist_show(H);
    return 0;
}
