#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 32
//遍历数组的注意是小于等于数组的下标
typedef struct node
{
   int data[N];
   int index;//数组下标的索引
}seqlist_t;

//空数组表
seqlist_t * create_empty_seqlist_head()
{
    //申请一段结构体内存
    seqlist_t *h=(seqlist_t *)malloc(sizeof(seqlist_t));
    //数组类型的话，数组是空的话
    h->index=-1;
    memset(h->data,0,sizeof(h->data));//清空数组
    return h;
}
//判断是否已满
int seqlist_is_full(seqlist_t *h)
{
    return (h->index==N-1);
}
//判断是否已空
int seqlist_is_empty(seqlist_t *h)
{
    return (h->index==-1);
}
//插入数组中的元素(因为是数组，插入是考虑满的问题)
int seqlist_t_insert(seqlist_t *h,int value)
{
//1.先判断是否为空
//2.数组的下标加一
//3.把插入的数据写到数组内
    if(seqlist_is_full(h))
    {
        printf("顺序表已满\n");
        return -1;
    }
    h->index++;
    h->data[h->index]=value;
    return 0;
}
//删除数组中的元素
int seqlist_delete(seqlist_t *h,int value)
{
//一.判断顺序表是否空，空则立即退出，并提示已空
//二.思路：删除数组里面需要先遍历数组找到要删除的元素，并将后面的所有元素向前移动一个位置
//  1.遍历数组
//  2.找到要删除的元素，，并将数组的下标减1（因为删除了一个元素，数组里面少了一个元素），并且立即返回函数返回值
//  3.将数组删除元素后面的所有元素向前移动移动一个位置
//三.返回值为0，结束程序
    if(seqlist_is_empty(h))
    {
        printf("顺序表已空\n");
        return -1;
    }
    for (int i = 0; i <=h->index ; i++)
    {
        if(h->data[i]==value)
        {
            for (int j=i; j< h->index; j++)
            {
                h->data[j]=h->data[j+1];
            }
            h->index--;
            return 0;
        }
    }
    return 0;
}
int seqlist_modify(seqlist_t *h,int old,int new)
{
//思路：先遍历数组找到要修改的元素，并将旧的和新的替换
//一.遍历数组
//二.找到要修改的元素，并将旧的和新的替换
   for (int i = 0; i < h->index; i++)
   {
      if(h->data[i]==old)
      {
         h->data[i]=new;
         break; 
      }
   }
    return 0;
}
int seqlist_search(seqlist_t *h,int value)
{
//思路：先遍历数组找到要搜索的元素，并返回1，没找到返回为0
//一.遍历数组
//二.找到要搜索的元素，返回1，退出循环  
    for (int i = 0; i < h->index; i++)
    {
        if(h->data[i]==value)
        {
            return 1;
        }
    }
    return 0;
}
int seqlist_reverse(seqlist_t *h)
{
//数组的逆序
    for (int i = 0,t=0; i <(h->index+1)/2 ; i++)
    {
        t=h->data[i];
        h->data[i]=h->data[h->index-i];
        h->data[h->index-i]=t;
    }
    return 0;
}
//采用冒泡排序
//先写出冒泡排序的思路，然后进行改写
int seqlist_sort1(seqlist_t *h)
{
    for (int i = 0; i < h->index; i++)
    {
        for (int j = 0, t = 0; j < h->index - i; j++)
        {
            if (h->data[j] > h->data[j + 1])
            {
                t = h->data[j];
                h->data[j] = h->data[j + 1];
                h->data[j + 1] = t;
            }
        }
    }
    return 0;
}
//采用冒泡排序
//先写出冒泡排序的思路，然后进行改写
int seqlist_sort2(seqlist_t *h)
{
    for (int i = 0; i < h->index; i++)
    {
        for (int j = 0, t = 0; j < h->index - i; j++)
        {
            if (h->data[j] < h->data[j + 1])
            {
                t = h->data[j];
                h->data[j] = h->data[j + 1];
                h->data[j + 1] = t;
            }
        }
    }
    return 0;
}
int seqlist_show(seqlist_t *h)
{
//一.打印顺序表
//二.遍历数组中的内容(这儿一定是<=，小于的话少一个元素)
    printf("顺序表的内容");
    for (int i = 0; i <= h->index; i++)
    {
        printf("%d ",h->data[i]);
    }
    printf("\n");
    return 0;
}

int main(int argc, char const *argv[])
{
    seqlist_t *H=create_empty_seqlist_head();
    for (int i = 0; i < 33; i++)
    {
        printf("%d ",i+1);
        seqlist_t_insert(H,i+1);
    }
    seqlist_show(H);
    seqlist_delete(H,1);
    seqlist_delete(H,32);
    seqlist_delete(H,15);
    seqlist_show(H);
    seqlist_modify(H, 2, 222);
    seqlist_modify(H, 16, 166);
    seqlist_modify(H, 31, 311);
    seqlist_show(H);
    if (seqlist_search(H, 3))
    {
        printf("3 is found\n");
    }
    else
    {
        printf("3 is not found\n");
    }
    if (seqlist_search(H, 2))
    {
        printf("2 is found\n");
    }
    else
    {
        printf("2 is not found\n");
    }
    seqlist_reverse(H);
    seqlist_show(H);
    seqlist_sort1(H);
    seqlist_show(H);
    seqlist_sort2(H);
    seqlist_show(H);
    return 0;
}
