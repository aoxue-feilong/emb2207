#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//定义一个结构体来描述单链表的结点
typedef struct node
{
    int data;
    struct node *next; //指向本结构体的指针
} linklist_t;

linklist_t *create_empty_linklist_head(void)//表头
{
//一.申请一段结构体大小的内存，并结构体指针来保存申请内存的地址
//二.想象空表头的图，写出h->data;h->next = NULL;
//三.返回申请内存的地址
//四.设置函数类型：即函数的返回值类型-结构体类型
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->data;
    h->next = NULL;
    return h;
    //首先申请一段内存来保存这个结构体
    // linklist_t *h并用这个结构体指针来保存申请内存的地址
    // 就可以用一个结构体指针变量保存这个地址 , 指针运算时, 要求类型必须一致
}
int linklist_insert(linklist_t *h, int value) //插入
{
//一.申请一段结构体大小的内存，并结构体指针来保存申请内存的地址
//二.想象插入第一个结点时的图形
//三.并让新建的data=value
//四.接链表的两步
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->data = value;
    p->next = h->next;
    h->next = p;
    return 0;
}
int linklist_delete(linklist_t *h, int value)//删除
{
//一.首先定义两个指针指向结构体
//  1.定义p指针指向表头
//  2.定义q指针（用来保存删除的结点） 
    linklist_t *p = h; //指向表头
    linklist_t *q;     //用来保存删除的结点
//二.如何删除
//  1.判断是否有结点可删while() | 移动结点
//  2.找到要删除的结点
//  3.q来指向要删除的结点，
//  4.连接链表的
//  5.释放要删除的结点q
//  6.删除后立即退出循环，否则万一删除的最后一个节点，不退出循环将移动空结点，系统直接会报错
    while (p->next != NULL) //判断是否有结点可删
    {
        if (p->next->data == value)//找到要删除的结点
        {
            q = p->next;       //删除的结点
            p->next = q->next; //连接链表
            free(q);           //释放要删除的结点
//删除结点后，要退出循环。如果删除的是最后一个结点，不退出循环将移动空结点，系统直接会报错
            break;
        }
        p = p->next; //移动结点
    }
    return 0;
}
int linklist_modify(linklist_t *h, int old, int new) //修改
{
//一.定义一个结构体指针p，指向头结点的下一个结点
//二.判断有结点没（头结点不参与运算）| 移动结点
//三.找到要修改的结点,修改后退出循环
//四.将修改结点的data赋值为新值
    linklist_t *p = h->next; //指向头结点的下一个结点
    while (p != NULL)
    {
        if (p->data == old)
        {
            p->data = new;
            break;
        }
        p = p->next;
    }
    return 0;
}
int linklist_search(linklist_t *h, int value)//搜索
{
//一.定义一个结构体指针p，指向头结点的下一个结点
//二.判断有结点没（头结点不参与运算）| 移动结点
//三.找到要搜索的结点,找到返回值为1，找不到返回值为0
    linklist_t *p = h->next; //指向头结点的下一个结点
    while (p != NULL)
    {
        if (p->data == value)
        {
            return 1;
        }
     p = p->next;
    }
    return 0;
}
//总体思路：用指针p指向第一个节点, 然后把头设计成空头, 然后把p节点插入头的后面, 以此类推
//即就是：头 9 8 7 6 5 4 3 2 1 变成 头 1 2 3 4 5 6 7 8 9 
// 即可实现链表的逆序
int linklist_reverse(linklist_t *h)
{
//一.首先定义两个指针指向结构体
//  1.定义p指针指向头结点的下一个结点
//  2.定义q指针（用来保存删除的结点）
//二.把表头置位空头
//三.判断是否还有结点
//四.结构体指针q指向p的下一个结点
//五.连接链表的两步
//六.让p=q，相当于移动结点
    linklist_t *p = h->next; //指向头结点的下一个结点
    linklist_t *q;
    h->next=NULL;
    while (p!=NULL)//p 指向的是链表的节点 , 不为空表示还有节点
    {
        q=p->next;
        p->next=h->next;
        h->next=p;
        p=q;
    }
    
    return 0;
}
int linklist_show(linklist_t *h)
{
//一.定义p指针指向头结点的下一个结点
//二.打印链表的内容为
//三.判断是否还有结点 | 移动结点
//四.输出data里面的内容
    linklist_t *p = h->next; //指向头结点的下一个结点
    printf("链表的内容为：");
    while (p != NULL)
    {
        printf("%d ", p->data);
        p = p->next; //移动结点
    }
    printf("\n");
    return 0;
}
int main(int argc, char const *argv[])
{
    linklist_t *H = create_empty_linklist_head();
    for (int i = 1; i < 10; i++)
    {
        linklist_insert(H, i+1);
    }
    linklist_show(H);
    
    linklist_delete(H, 1);
    linklist_delete(H, 5);
    linklist_delete(H, 10);
    linklist_show(H);
    linklist_modify(H,2,22);
    linklist_modify(H,9,99);
    linklist_show(H);
    if(linklist_search(H,99))
    {
        printf("99 找到了\n");
    }
    else
    {
        printf("99 没找到了\n");
    }
    if(linklist_search(H,100))
    {
        printf("100 找到了\n");
    }
    else
    {
        printf("100 没找到了\n");
    }
    linklist_reverse(H);
    linklist_show(H);
    linklist_reverse(H);
    linklist_show(H);
    return 0;
}
