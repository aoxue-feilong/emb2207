#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// linkquence queue
typedef struct node
{
    int data;
    struct node *next;
} linklist_t;

typedef struct
{
    linklist_t *front; // 队头
    linklist_t *rear;  // 队尾
} linkqueue_t;

// 空队列的概念是 : 队头等于队尾
// 为了区分空队 和满队 , 队列元素个数要比数组元素少一个
// 满队+1 等于空队
// 1 2 3 4 5
// f       f
// r

linkqueue_t *create_empty_linkqueue()
{
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    linkqueue_t *q = (linkqueue_t *)malloc(sizeof(linkqueue_t));
    q->front = q->rear = h;
    q->front->data = 0;
    return q;
}

int linkqueue_is_empty(linkqueue_t *q)
{
    return (q->front == q->rear);
}

// 链式队列没有满的概念
// int linkqueue_is_full(linkqueue_t *q)
// {

// }

// 入队 队尾加



int linkqueue_is_size(linkqueue_t *q)
{
    int size = 0 ;
    linklist_t *p=q->front->next;
    while (p!=NULL)
    {
        size ++;
        p=p->next;
    }
    return size;
}

int linkqueue_enqueue(linkqueue_t *q, int value)
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->data = value;
    p->next = q->rear->next; // 尾部插入第1步
    q->rear->next = p;       // 尾部插入第2步
    q->rear = p;             // 尾部插入第3步
    return 0;
}

// 头部出队列 , 头部删除法
int linkqueue_dequeue(linkqueue_t *q, int *pvalue)
{
    linklist_t *p;
    if (linkqueue_is_empty(q))
    {
        printf("队列为空");
        return -1;
    }
    p = q->front->next;           // 头节点的下一个节点  , p是要删除的节点
    q->front->next = p->next;     // 接上链表
    if (q->front->next == NULL) // 此时队列为空
        q->rear = q->front; // 
    
    *pvalue = p->data;
    free(p);
    return 0;
}
int seqstack_push(linkqueue_t *q1, linkqueue_t *q2, int value)
{
    linkqueue_enqueue(q1, value); // 队列不满
    return 0;
}
int seqstack_pop(linkqueue_t *q1, linkqueue_t *q2, int *pvalue)
{
    int value;
    // q1 出队列只剩下一个元素
    // printf("sequeue_is_size(q1)=%d\n",sequeue_is_size(q1));
    while (linkqueue_is_size(q1) > 1)
    {
        linkqueue_dequeue(q1, &value) ; 
        linkqueue_enqueue(q2, value);
    }

    // 把q1中的元素出队
    linkqueue_dequeue(q1, pvalue);

    // 把q2中的所有元素出队到q1
    while (linkqueue_is_size(q2) > 0)
    {
        linkqueue_dequeue(q2, &value);
        linkqueue_enqueue(q1, value);
    }
    return 0;
}
int main(int argc, char const *argv[])
{
    linkqueue_t *Q1 = create_empty_linkqueue();
    linkqueue_t *Q2 = create_empty_linkqueue();
    printf("入栈顺序:");
    for (int i = 0; i < 32; i++)
    {
        if (seqstack_push(Q1, Q2, i + 1) == 0) // 表示成功
        {
            printf("%d ", i + 1);
        }
    }
    printf("\n");

    printf("出栈顺序:");
    for (int i = 0, value; i < 32; i++)
    {
        if (seqstack_pop(Q1, Q2, &value) == 0)
        {
            printf("%d ", value);
        }
    }
    printf("\n");
    return 0;
}
