#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// sequence queue

#define N 128
typedef struct
{
    int data[N];
    int front; // 队列的头  , 数组的下标
    int rear;  // 队列的尾  , 数组的下标
} sequeue_t;

// 空队列的概念是 : 队头等于队尾
// 为了区分空队 和满队 , 队列元素个数要比数组元素少一个
// 满队+1 等于空队
// 1 2 3 4 5
// f       f
// r

sequeue_t *create_empty_sequeue()
{
    sequeue_t *q = (sequeue_t *)malloc(sizeof(sequeue_t));
    memset(q->data, 0, sizeof(q->data));
    q->front = q->rear = N - 1; //指向数组最后一个元素
    return q;
}

int sequeue_is_empty(sequeue_t *q)
{
    return (q->front == q->rear);
}

int sequeue_is_full(sequeue_t *q)
{
    // 队尾 +1 == 队头 , 就是满队
    // 任何一个一个数对N求余 , 这个数的范围是  0 - N-1
    return ((q->rear + 1) % N == q->front);
}

int sequeue_is_size(sequeue_t *q)
{
    // 队尾减去队头就是元素的个数
    if (q->rear >= q->front) //
        return (q->rear - q->front);
    else
        return (N - q->front + q->rear);
}

int sequeue_enqueue(sequeue_t *q, int value)
{
    if (sequeue_is_full(q))
    {
        printf("队列已满");
        return -1;
    }
    q->rear = (q->rear + 1) % N; // 防止数组越界
    q->data[q->rear] = value;
    return 0;
}
int sequeue_dequeue(sequeue_t *q, int *pvalue)
{
    if (sequeue_is_empty(q))
    {
        printf("队列已空");
        return -1;
    }
    q->front = (q->front + 1) % N; // 防止数组越界
    *pvalue = q->data[q->front];
    return 0;
}

// sequeue stack
typedef struct
{
    int data[N]; // 数组的空间
    int index;   // 是数组下标的索引

} seqstack_t;

seqstack_t *create_empty_seqstack()
{
    seqstack_t *s = (seqstack_t *)malloc(sizeof(seqstack_t));
    memset(s->data, 0, sizeof(s->data)); // 把数组中的内容清为0
    s->index = -1;                       // 表示栈为空
    return s;
}

int seqstack_is_empty(seqstack_t *s)
{
    return (s->index == -1);
}

int seqstack_is_full(seqstack_t *s)
{
    return (s->index == N - 1);
}

int seqstack_is_size(seqstack_t *s)
{
    return (s->index+1);
}

// 入栈 , 压栈
int seqstack_push(seqstack_t *s, int value)
{
    if (seqstack_is_full(s))
    {
        printf("栈已满");
        return -1;
    }
    s->index++;
    s->data[s->index] = value;
    return 0;
}
int seqstack_pop(seqstack_t *s, int *pvalue)
{
    if (seqstack_is_empty(s))
    {
        printf("栈已空");
        return -1;
    }
    *pvalue = s->data[s->index];
    s->index--;
    return 0;
}

// 检查队列是否是 是否是1,2,3...27 这个顺序
// 是这个顺序 返回真 
// 不是这个顺序 返回假
int check_sequeue(sequeue_t * q)
{
    for(int i=1;i<27;i++)
    {
        // 正常顺序 1,2,3,4,...27 
        if(q->data[(q->front+i)%N] > q->data[(q->front+i+1)%N] )
        {
            return 0; // 这个顺序不对 
        }
    }
    return 1; // 所有顺序都对 , 返回真 
}

int main(int argc, char const *argv[])
{
    sequeue_t *Q = create_empty_sequeue(); // 创建一个空队列
    // 往队列中装入27个元素
    for (int i = 0; i < 27; i++)
    {
        sequeue_enqueue(Q, i + 1);
    }

    seqstack_t *S1 = create_empty_seqstack();  // S1 分钟指示器 
    seqstack_t *S5 = create_empty_seqstack();  // S5 5分钟指示器 
    seqstack_t *S60 = create_empty_seqstack(); // S60 5小时指示器 

    int count =0 ; //  用来记录工作的分钟数 
    int value = 0 ,t12;
    while(1)
    {
      count++;                    // 计算累计时间的
      sequeue_dequeue(Q, &value); // 从球队列中取出一个球
      seqstack_push(S1, value);   // 放入分钟指示器中

      /*************************************************************/
      // 分钟指示器的工作原理

      if (seqstack_is_size(S1) < 5)
        continue;
      // 第五个球就会进入五分钟指示器
      seqstack_pop(S1, &value); // 从s1里面出栈第五个球
      seqstack_push(S5, value); //入到s5分钟指示器里面

      //在分钟指示器的4个球就会按照他们被放入时的相反顺序加入球队列的队尾
      for (int i = 0; i < 4; i++)
      {
        seqstack_pop(S1, &value);  //从s1里面出栈出来
        sequeue_enqueue(Q, value); //1~4个球入到队列里面
      }
        /*************************************************************/
        // 5分钟指示器的工作原理 
        if(seqstack_is_size(S5) < 12)  continue;
        // 第12个球就会进入小时指示器
        seqstack_pop(S5, &value);  //从s5出栈第12个球
        seqstack_push(S60, value); // 1~11个球入到s60分钟指示器里面

        //在5分钟指示器的11个球就会按照他们被放入时的相反顺序加入球队列的队尾
        for (int i = 0; i < 11; i++)
        {
            seqstack_pop(S5, &value);  //从s5里面出栈出来
            sequeue_enqueue(Q, value); // 1~11个球入到队列里面
        }

        /*************************************************************/
        // 小时指示器的工作原理 
        if(seqstack_is_size(S60) < 12)  continue;
        
        seqstack_pop(S60,&t12);  // 第12个球拿出来

        //小时指示器的11个球就会按照他们被放入时的相反顺序加入球队列的队尾
        for (int i = 0; i < 11; i++)
        {
            seqstack_pop(S60, &value); //从s60出栈第12个球
            sequeue_enqueue(Q, value); // 1~11个球入到队列里面
        }
        sequeue_enqueue(Q, t12); // 然后第12个球也回到队尾

        /*************************************************************/
        // 结束条件 
        if(check_sequeue(Q)) // 为真, 返回 , 为假继续循环
        {
            break; 
        }
        else
        {
            continue;
        }
    }
    // 33120 是标准答案 
    printf("count = %d\n",count); 

    return 0;
}