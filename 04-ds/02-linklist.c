#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    int date;//结构体成员
    struct node *next;//结构体指针，和本结构体类型一样的结构体指针
} linklist_t;
// 这个函数的功能是在内存中申请了一块结构体大小的内存
// 并把申请后的地址返回 
linklist_t *create_empty_linklist_head(void)
{
    //malloc(sizeof(linklist_t)在栈区申请一个结构体大小的内存 16字节
    //(linklist_t *)malloc把malloc返回的地址强转成结构体指针类型的
    // 就可以用一个结构体指针变量保存这个地址 , 指针运算时, 要求类型必须一致
    // *h 就是一个结构体了 , 
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->date;// (*h).data = 0  这两种用法等价
    h->next = NULL;
    return h; // 在堆区申请的内存, 是静态生存期, 内存不会释放, 用户使用free释放, 操作系统不会去释放
}
// h  是一个结构体指针, 保存结构体的地址 
// value 要写入的值 
int linklist_insert(linklist_t *h, int value)//插入
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));//申请一段内存
    p->date = value;

    p->next = h->next;//(1)
    h->next = p;//(2)
    return 0;
}
int linklist_show(linklist_t *h)//显示
{
    linklist_t *p = h->next;
    printf("链表的内容");
    while (p != NULL)
    {
        printf("%d ", p->date);//访问节点的内容5 4 3 2 1
        p = p->next;//节点往后移动
    }
    printf("\n");
    return 0;
}

int main(int argc, char const *argv[])
{
    linklist_t *H = create_empty_linklist_head();
    for (int i = 0; i < 10; i++)
    {
        linklist_insert(H, i + 1);
    }
    linklist_show(H);

    return 0;
}
