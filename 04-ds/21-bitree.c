#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    int data;
    struct node *lchild; // left child 左孩子指针
    struct node *rchild; // right child 右孩子指针
} bitree_t;

// i :根的起始编号, n :要创建节点的个
// 根节点从1 开始
// n 表示的一共的节点个数
bitree_t *create_bitree(int i, int n)
{
    bitree_t *r = malloc(sizeof(bitree_t));
    r->data = i; // 创建根节点
    printf("i=%d\n", i);
    if (2 * i <= n) // 有左孩子
    {
        r->lchild = create_bitree(2 * i, n);
    }
    else
    {
        r->lchild = NULL; // 左孩子为空
    }

    if (2 * i + 1 <= n) // 有右孩子
    {
        r->rchild = create_bitree(2 * i + 1, n);
    }
    else
    {
        r->rchild = NULL;
    }

    return r;
}


int main(int argc, char const *argv[])
{
    bitree_t *R = create_bitree(1, 15);
    return 0;
}