#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    int date;
    struct node *next;
} linklist_t;

linklist_t *create_empty_linklist_head(void)
{
    // malloc(sizeof(linklist_t)在栈区申请一个结构体大小的内存 16字节
    //(linklist_t *)malloc把malloc返回的地址强转成结构体指针类型的
    // 就可以用一个结构体指针变量保存这个地址 , 指针运算时, 要求类型必须一致
    // *h 就是一个结构体了 , 
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->date;
    h->next = NULL;
    return h;
}

int linklist_insert(linklist_t *h, int value)//插入
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->date = value;
    p->next = h->next;
    h->next = p;
    return 0;
}
//
int linklist_show(linklist_t *h)
{
    linklist_t *p = h->next;//指向表头的下一个结点
    printf("链表的内容");
    while (p != NULL)
    {
        printf("%d ", p->date);
        p = p->next;
    }
    printf("\n");
    return 0;
}

int main(int argc, char const *argv[])
{
    linklist_t *H = create_empty_linklist_head();
    linklist_insert(H, 1);
    linklist_insert(H, 2);
    linklist_show(H);

    return 0;
}
