#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct node
{
  int data;
  struct node *lchild; // left child 左孩子指针
  struct node *rchild; // right child 右孩子指针
} bitree_t;
// i :根的起始编号, n :要创建节点的个
// 根节点从1 开始
// n 表示的一共的节点个数
bitree_t *create_bitree(int i, int n)
{
  bitree_t *r = malloc(sizeof(bitree_t));
  r->data = i; // 创建根节点
  printf("i=%d\n", i);
  if (2 * i <= n) // 有左孩子
  {
    r->lchild = create_bitree(2 * i, n);
  }
  else
  {
    r->lchild = NULL; // 左孩子为空
  }
  if (2 * i + 1 <= n) //有右孩子
  {
    r->rchild = create_bitree(2 * i + 1, n);
  }
  else
  {
    r->lchild = NULL;
  }
}
//递归函数一定要有结束条件
//根左右

int preorder(bitree_t *b) //先序
{
  if (b == NULL) //结束条件，空树返回
    return 0;
  //先访问根
  printf("%d ", b->data);
  //再访问左
  preorder(b->lchild);
  //最后访问右
  preorder(b->rchild);
  return 0;
}
//中序访问
//
int inorder(bitree_t *b) //中序
{
  if (b == NULL)
    return 0;
  //先访问左
  inorder(b->lchild);
  //再访问根
  printf("%d ", b->data);
  //最后访问右
  inorder(b->rchild);
  return 0;
}
int postorder(bitree_t *b) //后序
{
  if (b == NULL)
    return 0;
  //先访问左
  postorder(b->lchild);
  //再访问右
  postorder(b->rchild);
  //最后访问根
  printf("%d ", b->data);
}

int main(int argc, char const *argv[])
{
  bitree_t *R = create_bitree(1, 15);
  printf("先序顺序:");
  preorder(R);
  printf("\n");

  printf("中序顺序:");
  inorder(R);
  printf("\n");

  printf("后序顺序:");
  postorder(R);
  printf("\n");
  return 0;
}
