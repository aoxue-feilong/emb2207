#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    int data;
    struct node *prior;
    struct node *next;
} linklist_t;

linklist_t *create_empty_double_linklist_head()
{
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->data = 0;
    h->next = NULL;
    h->prior = NULL;
    return h;
}

int double_linklist_insert(linklist_t *h, int value)
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->data = value;
    p->next = h->next; // 双链插入第1步
    h->next = p;       // 双链插入第2步
    if (p->next != NULL)
        p->next->prior = p; // 双链插入第3步
    p->prior = h;           // 双链插入第4步
    return 0;
}
int double_linklist_show(linklist_t *h)
{
    linklist_t *p = h->next;
    printf("链表的内容为:");
    while (p != NULL)
    {
        printf("%d ", p->data);
        p = p->next;
    }
    printf("\n");
    return 0;
}

int double_linklist_delete(linklist_t *h, int value)
{
    linklist_t *p = h->next;
    while (p != NULL)
    {
        if (p->data == value)
        {
            if (p->prior != NULL)
                p->prior->next = p->next; // 接后链
            if (p->next != NULL)
                p->next->prior = p->prior; // 接前链
            free(p); 
            return 0;
        }
        p = p->next;
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    linklist_t *H = create_empty_double_linklist_head();
    for (int i = 0; i < 10; i++)
    {
        double_linklist_insert(H, i + 1);
    }
    double_linklist_show(H);
    double_linklist_delete(H,10); 
    double_linklist_delete(H,1);
    double_linklist_delete(H,5);
    double_linklist_show(H); 
     


    return 0;
}