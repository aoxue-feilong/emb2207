#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    int data;
    struct node *next;//指向本结构体的指针
} linklist_t;

linklist_t *create_empty_cyclist_head(void)
{
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->data = 1; // 循环链表的特点是, 头结点也要参与运算
    h->next = h; // 让头节点的下一个节点指向头部
    return h;
}
int cyclist_insert(linklist_t *h, int value)
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->data = value;
    p->next = h->next; // 接链表第1步
    h->next = p;       //接链表的第2步
    return 0;
}

int cyclist_show(linklist_t *h)
{
    linklist_t *p = h->next; // 让p指向头节点下一个节点, 让头节点作为最后一个节点使用
    printf("单循环链表的内容:");
    while (p != h) // p->next == h 是表示循环一圈
    {
        printf("%d ", p->data); // 输出p指向节点的内容
        p = p->next;            // 移动节点
    }
    // p == h
    if (p == h) // p指向的h了, 作为循环表的最后一个元素
    {
        printf("%d ", p->data); // 输出p指向节点的内容
    }
    printf("\n");
    return 0;
}

// 循环表删除节点时, 要定位到要删除节点的前一个节点
// 如果要删除的节点是头结点 , 必须要更新一个新的头结点
linklist_t *cyclist_delete(linklist_t *h, int value)
{
    linklist_t *p = h; // P 指向h节点
    linklist_t *q;
    while (p->next != h) //循环一圈少一个
    {
        if (p->next->data == value)
        {
            q = p->next;       // 要删除的节点
            p->next = q->next; // 连接链表
            free(q);
            return h; //  函数要返回, 不返回在最后一个节点时, 会报错,
        //如果删除的是最后一个节点，没有返回地址时，将移动空指针，程序会自动崩溃
        }
        p = p->next; //p节点往后移动
    }
    if (p->next == h) // 表示要删除的是头结点
    {
        q = p->next;       // 要删除的节点
        p->next = q->next; // 连接链表
        free(q);           // 释放节点
        return p;          // 此时删除的是头结点, 要返回一个新的头
    }
}

//头节点时链表的最后一个节点
int cyclist_modify(linklist_t *h, int old, int new)
{
    linklist_t *p = h->next; // 指向链表的第一个节点
    while (p != h)
    {
        if (p->data == old)
        {
            p->data = new;
            return 0;
        }
        p = p->next; // 移动节点
    }
    if (p == h)
    {
        if (p->data == old)
        {
            p->data = new;
            return 0;
        }
    }
    return 0;
}
// 找到返回为真
// 没找到返回为假
int cyclist_search(linklist_t *h, int value)
{
    linklist_t *p = h->next; // 指向链表的第一个节点
    while (p != h)
    {
        if (p->data == value)
        {
            return 1;
        }
        p = p->next; // 移动节点
    }
    if (p == h)
    {
        if (p->data == value)
        {
            return 1;
        }
    }
    return 0;
}

// 因为链表的头节点, 我们把他处理为最后一个节点, 要做逆序, 必须把头结点处理为
// 第一个节点, 最先输出, 只需要把头结点往前移动一个位置即可, 头节点变为第一个节点
linklist_t * cyclist_reverse(linklist_t * h)
{
    linklist_t * p  = h->next ; // 让p 指向第一个节点
    h->next = h  ; // 把头设置位一个空节点 
    linklist_t * q; 
    
    // 把p指向的节点循环插入到h的后面
    while(p != h) 
    {
        q = p->next ; // 保存下一个要插入的节点
        p->next = h->next ; // 接链表的第1步
        h->next = p ; // 接链表的第2步
        p = q ; // 把p和q 保持一致 
    }
    //  处理最后一个节点, 把头节点往后移动一个位置
    // 要找到头节点的前一个位置
    p = h ;  // 让p 指向h 
    while(p->next != h)//执行完p=q，p和q在一处，移动p让 P!=q
    {
        p = p->next ; 
    }
    if(p->next == h)
    {
        return p ; 
    }
    return h ; 
}
int main(int argc, char const *argv[])
{
    linklist_t *H = create_empty_cyclist_head();
    cyclist_insert(H, 2);
    cyclist_insert(H, 3);
    cyclist_insert(H, 4);
    cyclist_insert(H, 5);
    cyclist_insert(H, 6);
    cyclist_insert(H, 7);
    cyclist_insert(H, 8);
    cyclist_insert(H, 9);
    cyclist_insert(H, 10);
    cyclist_show(H);
    H = cyclist_delete(H, 10);
    H = cyclist_delete(H, 1);
    H = cyclist_delete(H, 2);
    H = cyclist_delete(H, 3);
    H = cyclist_delete(H, 4);
    H = cyclist_delete(H, 5);
    cyclist_show(H);
    cyclist_modify(H, 9, 99);
    cyclist_modify(H, 7, 77);
    cyclist_modify(H, 6, 66);
    cyclist_show(H);
    if (cyclist_search(H, 99))
    {
        printf("99 found\n");
    }
    else
    {
        printf("99 not found\n");
    }
    if (cyclist_search(H, 88))
    {
        printf("88 found\n");
    }
    else
    {
        printf("88 not found\n");
    }
    H = cyclist_reverse(H); 
    cyclist_show(H); 
    H = cyclist_reverse(H); 
    cyclist_show(H); 


    return 0;
}
