#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>

// sequeue stack

#define N 32
typedef struct
{
    int data[N]; // 数组的空间
    int index;   // 是数组下标的索引

} seqstack_t;

seqstack_t *create_empty_seqstack()
{
    seqstack_t *s = (seqstack_t *)malloc(sizeof(seqstack_t));
    memset(s->data, 0, sizeof(s->data)); // 把数组中的内容清为0
    s->index = -1;                       // 表示栈为空
    return s;
}

int seqstack_is_empty(seqstack_t *s)
{
    return (s->index == -1);
}

int seqstack_is_full(seqstack_t *s)
{
    return (s->index == N - 1);
}

// 入栈 , 压栈
int seqstack_push(seqstack_t *s, int value)
{
    if (seqstack_is_full(s))
    {
        printf("栈已满");
        return -1;
    }
    s->index++;
    s->data[s->index] = value;
    return 0;
}
int seqstack_pop(seqstack_t *s, int *pvalue)
{
    if (seqstack_is_empty(s))
    {
        printf("栈已空");
        return -1;
    }
    *pvalue = s->data[s->index];
    s->index--;
    return 0;
}

int sequeue_enqueue(seqstack_t *s1, seqstack_t *s2, int value)//入队
{
    seqstack_push(s1, value);

    return 0;
}
int sequeue_dequeue(seqstack_t *s1, seqstack_t *s2, int *pvalue)//出队
{
    int value;
    while (!seqstack_is_empty(s1))
    {

        seqstack_pop(s1, &value);
        seqstack_push(s2, value);
    }
    seqstack_pop(s2, pvalue);
    while (!seqstack_is_empty(s2))
    {
        seqstack_pop(s2, &value);
        seqstack_push(s1, value);
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    seqstack_t *S1 = create_empty_seqstack();
    seqstack_t *S2 = create_empty_seqstack();
    //  这个队列, 最多可以存放 31 个元素
    printf("入队顺序为:");
    for (int i = 0; i < 32; i++)
    {
        if (sequeue_enqueue(S1, S2, i + 1) == 0)
        {
            printf("%d ", i + 1);
        }
    }
    printf("\n");

    printf("出队顺序为:");
    for (int i = 0, value; i < 32; i++)
    {
        if (sequeue_dequeue(S1, S2, &value) == 0)
        {
            printf("%d ", value);
        }
    }
    printf("\n");

    return 0;
}