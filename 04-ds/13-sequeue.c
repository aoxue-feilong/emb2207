#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 32

typedef struct
{
    int data[N];//一维数组
    int front;
    int rear;
} sequeue_t;
sequeue_t *create_empty_sequeue()
{
    sequeue_t *q = (sequeue_t *)malloc(sizeof(sequeue_t));//申请一段内存
    memset(q->data, 0, sizeof(q->data));//内存置位（内存都变成0）
    q->front = q->rear = N - 1;//表示空的队列（0~31)
    return q;
}
int sequeue_is_empty(sequeue_t *q)//看这个队列是否为空
{
    return (q->front == q->rear);//为空的判断条件
}
int sequeue_is_full(sequeue_t *q)//看这个队列是否为满
{
    return ((q->rear + 1) % N == q->front);//为满的判断条件(队尾加1等于对头，就是满队)
    //(q->rear + 1) % N  的意思是，防止超出数组的下标范围
}
//任何一个数对N求余，结果是0~N-1
int sequeue_enqueue(sequeue_t *q, int value)//入队
{
    if (sequeue_is_full(q))//先判断是否为满
    {
        printf("队列已满");
        return -1;
    }
    q->rear = (q->rear + 1) % N;     //防止数组越界（防止超出范围0~31）
    q->data[q->rear] = value;//在队尾插入
    return 0;
}
int sequeue_dequeue(sequeue_t *q, int *pvalue)//出队
{
    if (sequeue_is_empty(q))//判断是否为空
    {
        printf("队列已空");
        return -1;
    }
    q->front = (q->front + 1) % N;//防止数组越界
    *pvalue = q->data[q->front];//在队头删除
    return 0;
}
int main(int argc, char const *argv[])
{
    sequeue_t *Q = create_empty_sequeue();
    //  这个队列, 最多可以存放 31 个元素
    printf("入队顺序");
    for (int i = 0; i < 32; i++)
    {
        if (sequeue_enqueue(Q, i + 1) == 0)//入队
        {
            printf("%d ", i + 1);
        }
    }
    printf("\n");
    printf("出队顺序");
    for (int i = 0, value; i < 32; i++)
    {
        if (sequeue_dequeue(Q, &value) == 0)//出队
        {
            printf("%d ", value);
        }
    }
    printf("\n");
    return 0;
}
