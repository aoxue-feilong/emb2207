#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 32

typedef struct node
{
    int data[N]; // 用来存放数据的数组
    int index;   //数组下标的索引
} seqlist_t;

seqlist_t *create_empty_seqlist_head()
{
    seqlist_t *h = (seqlist_t *)malloc(sizeof(seqlist_t));
    h->index = -1; // 数组的下标从-1 开始 , 表示数组内没有元素
    memset(h->data, 0, sizeof(h->data));
    return h;
}
// 为空 为真
// 不为空 为假
int seqlist_is_empty(seqlist_t *h)
{
    return (h->index == -1);
}

// 为空 为真
// 不为空 为假
int seqlist_is_full(seqlist_t *h)
{
    return (h->index >= N - 1);
}

int seqlist_insert(seqlist_t *h, int value)
{
    if (seqlist_is_full(h))//判断是否满，看函数的返回值
    {
        printf("顺序表已满\n");
        return -1;
    }
    h->index++;
    h->data[h->index] = value;//写到数组里面
    return 0;
}

int seqlist_delete(seqlist_t *h, int value)
{
    if (seqlist_is_empty(h))
    {
        printf("顺序表已空\n");
        return -1;
    }
    for (int i = 0; i <= h->index; i++)
    {
        if (h->data[i] == value) // 找到要删除的元素
        {
            // 后面的所有元素往前移动一个位置（思路：覆盖住就可以，不像链表一样需要断链）
            for (int j = i; j < h->index; j++)
            {
                h->data[j] = h->data[j + 1];
            }
            h->index--; // 顺序表中的元素个数减1
            return 0;   // 直接返回
        }
    }

    return 0;
}
int seqlist_show(seqlist_t *h)
{
    printf("顺序表的内容:");
    for (int i = 0; i <= h->index; i++)
    {
        printf("%d ", h->data[i]);
    }
    printf("\n");
}

int seqlist_modify(seqlist_t *h, int old, int new)
{
    for (int i = 0; i <= h->index; i++)
    {
        if (h->data[i] == old) //  找到要修改的数据
        {
            h->data[i] = new;
            return 0;
        }
    }
    return 0;
}
// 找到返回真 , 真是1
// 没找到返回假, 假是0
int seqlist_search(seqlist_t *h, int value)
{
    for (int i = 0; i <= h->index; i++)
    {
        if (h->data[i] == value) //  找到要修改的数据
        {
            return 1; // 找到
        }
    }
    return 0; // 没找到
}
int seqlist_reverse(seqlist_t *h)//倒序
{
    for (int i = 0, t = 0; i < (h->index + 1) / 2; i++)
    {
        t = h->data[i];
        h->data[i] = h->data[h->index - i];
        h->data[h->index - i] = t;
    }
    return 0;
}
int seqlist_sort1(seqlist_t *h)//冒泡排序（先写冒泡的结构在进行改参数）
{
    for (int i = 0,t=0; i < h->index; i++)//这儿不需要减1，本身需要多循环一次
    {
        for (int j = 0; j < h->index - i; j++)//这儿不需要减1，本身需要多循环一次
        {
            // 交换
            if( h->data[j] > h->data[j+1])
            {
                t = h->data[j]; 
                h->data[j] = h->data[j+1]; 
                h->data[j+1] = t;
            }
        }
    }
    return 0;
}
int seqlist_sort2(seqlist_t *h)
{
    for (int i = 0,t=0; i < h->index; i++)
    {
        for (int j = 0; j < h->index - i; j++)
        {
            // 交换
            if( h->data[j] < h->data[j+1])
            {
                t = h->data[j]; 
                h->data[j] = h->data[j+1]; 
                h->data[j+1] = t;
            }
        }
    }
    return 0;
}
int main(int argc, char const *argv[])
{
    seqlist_t *H = create_empty_seqlist_head();
    printf("顺序表的插入:");
    for (int i = 0; i < 33; i++)
    {
        printf("%d ", i + 1);
        seqlist_insert(H, i + 1);
    }
    seqlist_show(H);
    seqlist_delete(H, 1);
    seqlist_delete(H, 15);
    seqlist_delete(H, 32);
    seqlist_show(H);
    seqlist_modify(H, 2, 102);
    seqlist_modify(H, 31, 131);
    seqlist_modify(H, 5, 55);
    seqlist_modify(H, 10, 100);
    seqlist_show(H);
    if (seqlist_search(H, 55))
    {
        printf("55 found\n");
    }
    else
    {
        printf("55 not found\n");
    }
    if (seqlist_search(H, 31))
    {
        printf("31 found\n");
    }
    else
    {
        printf("31 not found\n");
    }
    seqlist_reverse(H);
    seqlist_show(H);
    seqlist_reverse(H);
    seqlist_show(H);
    seqlist_sort1(H); //从小到大排序
    seqlist_show(H);
    seqlist_sort2(H); //从大到小排序
    seqlist_show(H);

    return 0;
}