#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// sequence queue

#define N 32
typedef struct
{
    int data[N];
    int front; // 队列的头  , 数组的下标
    int rear;  // 队列的尾  , 数组的下标
} sequeue_t;

// 空队列的概念是 : 队头等于队尾
// 为了区分空队 和满队 , 队列元素个数要比数组元素少一个
// 满队+1 等于空队
// 1 2 3 4 5
// f       f
// r

sequeue_t *create_empty_sequeue()
{
    sequeue_t *q = (sequeue_t *)malloc(sizeof(sequeue_t));
    memset(q->data, 0, sizeof(q->data));
    q->front = q->rear = N - 1; //指向数组最后一个元素
    return q;
}

int sequeue_is_empty(sequeue_t *q)
{
    return (q->front == q->rear);
}

int sequeue_is_full(sequeue_t *q)
{
    // 队尾 +1 == 队头 , 就是满队
    // 任何一个一个数对N求余 , 这个数的范围是  0 - N-1
    return ((q->rear + 1) % N == q->front);
}

int sequeue_is_size(sequeue_t *q)
{
    // 队尾减去队头就是元素的个数
    if (q->rear >= q->front) //
        return (q->rear - q->front);
    else
        return (N - q->front + q->rear);
}

int sequeue_enqueue(sequeue_t *q, int value)
{
    if (sequeue_is_full(q))
    {
        printf("队列已满");
        return -1;
    }
    q->rear = (q->rear + 1) % N; // 防止数组越界
    q->data[q->rear] = value;
    return 0;
}
int sequeue_dequeue(sequeue_t *q, int *pvalue)
{
    if (sequeue_is_empty(q))
    {
        printf("队列已空");
        return -1;
    }
    q->front = (q->front + 1) % N; // 防止数组越界
    *pvalue = q->data[q->front];
    return 0;
}

int seqstack_push(sequeue_t *q1, sequeue_t *q2, int value)
{
    sequeue_enqueue(q1, value); // 队列不满
    return 0;
}
int seqstack_pop(sequeue_t *q1, sequeue_t *q2, int *pvalue)
{
    int value;
    // q1 出队列只剩下一个元素
    // printf("sequeue_is_size(q1)=%d\n",sequeue_is_size(q1));
    while (sequeue_is_size(q1) > 1)
    {
        sequeue_dequeue(q1, &value) ; 
        sequeue_enqueue(q2, value);
    }

    // 把q1中的元素出队
    sequeue_dequeue(q1, pvalue);

    // 把q2中的所有元素出队到q1
    while (sequeue_is_size(q2) > 0)
    {
        sequeue_dequeue(q2, &value);
        sequeue_enqueue(q1, value);
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    sequeue_t *Q1 = create_empty_sequeue();
    sequeue_t *Q2 = create_empty_sequeue();
    printf("入栈顺序:");
    for (int i = 0; i < 32; i++)
    {
        if (seqstack_push(Q1, Q2, i + 1) == 0) // 表示成功
        {
            printf("%d ", i + 1);
        }
    }
    printf("\n");

    printf("出栈顺序:");
    for (int i = 0, value; i < 32; i++)
    {
        if (seqstack_pop(Q1, Q2, &value) == 0)
        {
            printf("%d ", value);
        }
    }
    printf("\n");
    return 0;
}
