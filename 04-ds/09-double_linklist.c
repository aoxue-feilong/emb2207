#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    int data;
    struct node *prior;
    struct node *next;
} linklist_t;

linklist_t *create_empty_double_linklist_head()
{
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->data = 0;
    h->next = NULL;
    h->prior = NULL;
    return h;
}

int double_linklist_insert(linklist_t *h, int value)
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->data = value;
    p->next = h->next; // 双链插入第1步
    h->next = p;       // 双链插入第2步
    if (p->next != NULL)
        p->next->prior = p; // 双链插入第3步
    p->prior = h;           // 双链插入第4步
    return 0;
}
int double_linklist_show(linklist_t *h)
{
    linklist_t *p = h->next;
    printf("链表的内容为:");
    while (p != NULL)
    {
        printf("%d ", p->data);
        p = p->next;
    }
    printf("\n");
    return 0;
}

int double_linklist_delete(linklist_t *h, int value)
{
    linklist_t *p = h->next;
    while (p != NULL)
    {
        if (p->data == value)
        {
            if (p->prior != NULL)
                p->prior->next = p->next; // 接后链
            if (p->next != NULL)
                p->next->prior = p->prior; // 接前链
            free(p);
        }
        p = p->next;
    }
    return 0;
}
int double_linklist_modify(linklist_t *h, int old, int new)
{
    linklist_t *p = h->next;
    while (p != NULL)
    {
        if (p->data == old)
        {
            p->data = new;
            return 0;
        }
        p = p->next;
    }
    return 0;
}
int doublelinklist_search(linklist_t *h, int value)
{
    
    linklist_t *p = h->next;
    while (p != NULL)
    {
        if (p->data == value)
        {
            return 1;
        }
        p=p->next;
    }
    return 0;
}
//思路：构建一个空头，把所有的再插入一遍
linklist_t * doublelinklist_reverse(linklist_t * h)
{
    linklist_t * p = h->next ; // p 指向第一个节点
    linklist_t * q; 
    h->next = NULL ; // 把表头置位空头 
    h->prior=NULL;
    while(p != NULL) // p 指向的是链表的节点 , 不为空表示还有节点
    {
       q=p->next;
       p->next=h->next;
       h->next=p;
       if(p->next!=NULL)
        p->next->prior=p;
        p->prior=h;
        p=q;
       
    }
    return 0;

}
int main(int argc, char const *argv[])
{
    linklist_t *H = create_empty_double_linklist_head();
    for (int i = 0; i < 10; i++)
    {
        double_linklist_insert(H, i + 1);
    }
    double_linklist_show(H);
    double_linklist_delete(H, 10);
    double_linklist_delete(H, 1);
    double_linklist_delete(H, 5);
    double_linklist_show(H);
    double_linklist_modify(H, 2, 22);
    double_linklist_modify(H, 8, 88);
    double_linklist_modify(H, 6,66);
    double_linklist_show(H);
    if (doublelinklist_search(H, 22))
    {
        printf("22 found\n");
    }
    else
    {
        printf("22 not found\n");
    }
    if (doublelinklist_search(H, 99))
    {
        printf("99 found\n");
    }
    else
    {
        printf("88 not found\n");
    }
    doublelinklist_reverse(H);
    double_linklist_show(H);
    doublelinklist_reverse(H);
    double_linklist_show(H);
    return 0;
}