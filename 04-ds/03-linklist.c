#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    int date;
    struct node *next;
} linklist_t;
linklist_t *create_empty_cyclist_head(void)
{
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->next = h; // 让头节点的下一个节点指向本省
    return h;
}
linklist_t *create_empty_linklist_head(void)
{
    // malloc(sizeof(linklist_t)在栈区申请一个结构体大小的内存 16字节
    //(linklist_t *)malloc 把malloc返回的地址强转成结构体指针类型的（地址可以强转成任何类型）
    // 就可以用一个结构体指针变量保存这个地址 , 指针运算时, 要求类型必须一致
    // *h 就是一个结构体了 ,
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    h->date;
    h->next = NULL;
    return h; // 在堆区申请的内存, 是静态生存期, 内存不会释放, 用户使用free释放, 操作系统不会去释放
}
// h 是一个结构体指针，保存结构体的地址
// value 要写入的数据
int linklist_insert(linklist_t *h, int value)
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->date = value;
    p->next = h->next;
    h->next = p;
    return 0;
}
int linklist_show(linklist_t *h)
{
    linklist_t *p = h->next;
    printf("链表的内容");
    while (p != NULL)
    {
        printf("%d ", p->date);
        p = p->next;
    }
    printf("\n");
    return 0;
}
// 正常删除链表中的1个节点, 需要把要删除链表的前一个节点和后一个节点连接
// 需要找到要删除节点的前一个节点才可以删除
int linklist_delete(linklist_t *h, int value) //功能是删除
{
    // 链表中第一个元素的前1个节点就是链表的head
    linklist_t *p = h; // p要指向删除节点的前一个节点
    linklist_t *q;
    // p->next 指向链表的第1个节点 5
    while (p->next != NULL)
    {
        if (p->next->date == value)
        {
            // 需要1个新的变量保存要删除的链表节点, 使用q
            q = p->next;       // p->next 是要删除的节点, q去保存要删除的节点
            p->next = q->next; // 链表的前1个节点 与 后1个节点连接
            free(q);           //释放内存
            // 释放完内存节点后, 要立即退出循环
            break;
        }
        p = p->next;
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    // H保存申请内存的起始地址
    linklist_t *H = create_empty_linklist_head();
    for (int i = 0; i < 5; i++)
    {
        linklist_insert(H, i + 1);
    }
    linklist_show(H);
    linklist_delete(H, 3);
    linklist_show(H);
    return 0;
}
