#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>

// link stack顺序栈

typedef struct node
{
    int data;
    struct node *next;
} linklist_t;

linklist_t *create_empty_linkstack()
{
    linklist_t *s = (linklist_t *)malloc(sizeof(linklist_t));
    s->data = 0;
    s->next = NULL;
    return s;
}

// 链表没有满  , 有空的概念
int linkstack_is_empty(linklist_t *s)
{
    return (s->next == NULL);
}

// 头部插入法
int linkstack_push(linklist_t *s, int value)//压入弹夹
{
    linklist_t *p = (linklist_t *)malloc(sizeof(linklist_t));
    p->data = value;
    p->next = s->next;
    s->next = p;
    return 0;
}

// 头部删除法
int linkstack_pop(linklist_t *s, int *pvalue)//pop弹的意思，弹出弹夹
{
    linklist_t *p = s->next; // p 指向s的下一个节点
    if(linkstack_is_empty(s))
    {
        printf("栈已空"); 
        return -1 ; 
    }
    // s->next 不为空 
    s->next = p->next; // 接链表 
    *pvalue = p->data ; // 读取数据
    free(p); 
    return 0;
}

int main(int argc, char const *argv[])
{
    linklist_t *S = create_empty_linkstack();
    printf("入栈顺序:");
    for(int i=0;i<33;i++)
    {
        if( linkstack_push(S,i+1) ==  0 )  // 表示成功（入栈后才进行打印）
        {
            printf("%d ",i+1);
        }
    }
    printf("\n");

    printf("出栈顺序:");
    for(int i=0,value;i<34;i++)
    {
        if(linkstack_pop(S,&value) == 0 ) //出栈后才进行打印
        {
            printf("%d ",value);
        }

    }
    printf("\n");

    return 0;
}