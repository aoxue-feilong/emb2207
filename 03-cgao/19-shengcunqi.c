#include <stdio.h>

int *func(void)
{
    int a = 100;
    return &a;
}

int main(int argc, char const *argv[])
{
    int *p = func();
    printf("*p=%d\n", *p);
    return 0;
}
