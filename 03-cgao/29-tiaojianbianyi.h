#ifndef TIAOJIANBIANYI_H
#define TIAOJIANBIANYI_H
// 思路解析 
// 第一次包含头文件 #include "29-tiaojianbianyi.h"
// 因为 _TIAOJIANBIANYI_H 没有定义, #ifndef   _TIAOJIANBIANYI_H 
// 就会#define   _TIAOJIANBIANYI_H , 会定义这个宏  , 后面的内容被预处理 
// 定义宏 和结构体 
// 预处理结束 

// 第二次包含头文件时   #include "29-tiaojianbianyi.h"
// #ifndef   _TIAOJIANBIANYI_H  , 已经定义过_TIAOJIANBIANYI_H, 这个条件不成立, 后面的预处理不编译
// 后面的代码就不再解析了
// struct student 只能被处理一次 

// 以上就是防止头文件被重复包含的解决办法 


#define  LED1ON     1  
#define  LED1OFF    2  
#define  LED2ON     3  
#define  LED2OFF    4  
#define  N         20 
     
typedef struct student
{
    int number;   // 学号
    char name[N]; // 保存姓名的数组
    char sex[N];  // male female
    int age;      // 年龄
    float score;  // 成绩
}student_t ;
#endif   // _TIAOJIANBIANYI_H