#include <stdio.h>
#include <string.h>

//结构体数组指针
struct student
{
    int number;    //学号
    char name[10]; //保存姓名的数组
    char sex[10];  // male female
    int age;       //年龄
    float score;   //成绩
};

int main(int argc, char const *argv[])
{
    struct student stu1={1,"li"  ,"male"  ,20,100};
    struct student stu2={2,"liu" ,"female",20,100};
    struct student stu3={3,"zhao","male"  ,20,100};
    struct student *pstu; // pstu 是结构体类型的指针 , 简称结构体指针
    // *pstu 是就是这个变量,这个变量就是stu1 , *pstu 和stu1 是等价的
    // *pstu.number 这样写的话, 会有一个优先级的问题 , .的优先级比* 高, 因此先计算了. , 这样写会报错
    // 解决办法就是控制优先级, 使用()
    // (*pstu).number
    pstu = &stu1;
    printf("stu1的信息为:\n");
    printf("stu1.number:     %d\n", (*pstu).number);
    printf("stu1.name  :     %s\n", (*pstu).name);
    printf("stu1.sex   :     %s\n", (*pstu).sex);
    printf("stu1.age   :     %d\n", (*pstu).age);
    printf("stu1.score :     %f\n", (*pstu).score);
    pstu = &stu2;
    printf("stu2的信息为:\n");
    printf("stu2.number:     %d\n", (*pstu).number);
    printf("stu2.name  :     %s\n", (*pstu).name);
    printf("stu2.sex   :     %s\n", (*pstu).sex);
    printf("stu2.age   :     %d\n", (*pstu).age);
    printf("stu2.score :     %f\n", (*pstu).score);
    pstu = &stu3;
    printf("stu3的信息为:\n");
    printf("stu3.number:     %d\n", (*pstu).number);
    printf("stu3.name  :     %s\n", (*pstu).name);
    printf("stu3.sex   :     %s\n", (*pstu).sex);
    printf("stu3.age   :     %d\n", (*pstu).age);
    printf("stu3.score :     %f\n", (*pstu).score);

    return 0;
}
