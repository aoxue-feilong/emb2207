#ifndef _BEEP_H
#define _BEEP_H

char * beep_on(void);
int beep_off(void);
#endif