#include <stdio.h>
enum WEEK
{
    Mon,  //这里就是一个宏值  , 默认为0 , 默认这个值是一个整数
    Tue,  // 1
    Wed,  // 2
    Thu,  // 3
    Fri,  // 4
    Sat,  // 5
    Sun   // 6 
};

int main(int argc, char const *argv[])
{

    enum WEEK day; // day 是一个整型变量 , 理解为 int day 
    for(day = Mon ; day <= Sun; day++)
    {
        printf("%d ",day); 
    }
    printf("\n");
    day = Thu ; 
    printf("Thu=%d\n",day); 
    return 0;
}