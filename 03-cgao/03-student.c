#include <stdio.h>
#include <string.h>

#define N 20

// 1. 结构体的定义
struct student
{
    int number;   // 学号
    char name[N]; // 保存姓名的数组
    char sex[N];  // male female
    int age;      // 年龄
    float score;  // 成绩
} stu1 = {1, "zhao", "female", 22, 90};

int main(int argc, char const *argv[])
{
    // 2. 定义一个结构体变量
    struct student stu2 = {2, "liu", "male", 20, 80}; // 定义一个结构体变量stu2

    printf("stu1 的信息:\n");
    printf("stu1.number:       %d\n", stu1.number);
    printf("stu1.name  :       %s\n", stu1.name);
    printf("stu1.sex   :       %s\n", stu1.sex);
    printf("stu1.age   :       %d\n", stu1.age);
    printf("stu1.score :       %f\n", stu1.score);

    printf("stu2 的信息:\n");
    printf("stu2.number:       %d\n", stu2.number);
    printf("stu2.name  :       %s\n", stu2.name);
    printf("stu2.sex   :       %s\n", stu2.sex);
    printf("stu2.age   :       %d\n", stu2.age);
    printf("stu2.score :       %f\n", stu2.score);

    return 0;
}
