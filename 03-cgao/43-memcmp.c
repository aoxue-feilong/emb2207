#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char buf[] = {"U.S. News & World Report is an American media company that publishes news, \
                consumer advice, rankings, and analysis. It was launched in 1948 as the merger \
                of domestic-focused weekly newspaper U.S. News and international-focused weekly \
                magazine World Report. In 1995, the company launched 'usnews.com' and in 2010, \
                the magazine ceased printing."};
    char ch;
    int ret;
    char *startp = buf; // 内存的起始位置
    int count = 0;
    while (1)
    {
        //  startp 从内存起始位置开始找
        // strlen(buf) , 这样写会有bug产生, 原因是这是一个固定长度的字节数, 实际搜索中, 随着查找的继续
        // 字符串的长度在减少 , 应该用 strlen(startp)计算
        ret = memcmp(startp, "news", strlen("news"));
        if (ret == 0) // 找到单词 news
        {
            count++; // 累计
            //找到以后, 要更新startp的位置
            printf("startp =%s\n", startp);
            startp ++; //找到单词的下一个位置开始继续搜索
        }
        else
        {
            startp++; // 往后移动
        }

        // buf[strlen(buf)] 这是数组元素的最后一个位置, 是'\0'
        if (startp == &buf[strlen(buf)])
        {
            break;
        }
    }
    printf("news 有 %d 个单词\n", count);
    return 0;
}