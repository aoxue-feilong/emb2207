#include <stdio.h>
//不在块作用域内，不在函数原型的作用域内，这种作用域就是文件作用域，就是全局变量
//从变量定义的开始，到文件的结束
int a = 100;
int func(int x); // x是声明作用域
int main(int argc, char const *argv[])
{
    int b = 20;

    a = 200;
    printf("1:a=%d\n", a);
    func(1);

    return 0;
}
int func(int x) // x=1
{
    a = 300;
    printf("2:a=%d\n", a); // a是块作用域
}
