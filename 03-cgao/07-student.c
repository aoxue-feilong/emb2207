#include <stdio.h>
#include <string.h>


//第三种结构体数组的用法
struct             //省略结构体名
{
    int number;    //学号
    char name[10]; //保存姓名的数组
    char sex[10];  // male female
    int age;       //年龄
    float score;   //成绩
}stu[3];

int main(int argc, char const *argv[])
{
   
    stu[0].number = 1;
    strcpy(stu[0].name, "zhao");
    strcpy(stu[0].sex, "male");
    stu[0].age = 22;
    stu[0].score = 100;

    stu[1].number = 2;
    strcpy(stu[1].name, "li");
    strcpy(stu[1].sex, "male");
    stu[1].age = 22;
    stu[1].score = 100;

    stu[2].number = 3;
    strcpy(stu[2].name, "wang");
    strcpy(stu[2].sex, "female");
    stu[2].age = 22;
    stu[2].score = 100;
    for (int i = 0; i < 3; i++)
    {
        printf("stu[%d]的信息为:\n", i);
        printf("stu[%d].number:     %d\n", i, stu[i].number);
        printf("stu[%d].name  :     %s\n", i, stu[i].name);
        printf("stu[%d].sex   :     %s\n", i, stu[i].sex);
        printf("stu[%d].age   :     %d\n", i, stu[i].age);
        printf("stu[%d].score :     %f\n", i, stu[i].score);
    }

    return 0;
}
