#include <stdio.h>

int func(int a)
{
    int b = a;
    {
        int c = 10;
        printf("1:c=%d\n", c);
    } // c作用域结束位置
    // printf("2:c=%d\n", c);

    printf("1:a=%d\n", a);
    printf("1:b=%d\n", b);
} // a,b作用域结束位置
int main(int argc, char const *argv[])
{
    func(1); //调用函数，传递参数1
    // printf("2:a=%d\n", a);  //a在这个范围没有定义
    // printf("2:b=%d\n",b);   //b在这个范围没有定义
    return 0;
}
