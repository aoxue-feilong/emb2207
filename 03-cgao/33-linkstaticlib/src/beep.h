#ifndef _BEEP_H
#define _BEEP_H

int beep_on(void);
int beep_off(void);
#endif