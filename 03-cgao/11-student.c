#include <stdio.h>
#include <string.h>

//共用体的理解
union student
{
    int number;    //学号
    char name[10]; //保存姓名的数组
    char sex[10];  // male female
    int age;       //年龄
    float score;   //成绩
};

int main(int argc, char const *argv[])
{
    union student stu1;
    stu1.number = 1; // . 是一个运算符，用来访问结构体的成员
    printf("stu1.number:     %d\n",stu1.number);
    printf("stu1.name  :     %s\n",stu1.name);
    printf("stu1.sex   :     %s\n",stu1.sex);
    printf("stu1.age   :     %d\n",stu1.age);
    printf("stu1.score :     %f\n",stu1.score);
    printf("************************\n");
    strcpy(stu1.name, "zhao");
     printf("stu1.number:     %d\n",stu1.number);
    printf("stu1.name  :     %s\n",stu1.name);
    printf("stu1.sex   :     %s\n",stu1.sex);
    printf("stu1.age   :     %d\n",stu1.age);
    printf("stu1.score :     %f\n",stu1.score);
    printf("************************\n");
    strcpy(stu1.sex, "male");
     printf("stu1.number:     %d\n",stu1.number);
    printf("stu1.name  :     %s\n",stu1.name);
    printf("stu1.sex   :     %s\n",stu1.sex);
    printf("stu1.age   :     %d\n",stu1.age);
    printf("stu1.score :     %f\n",stu1.score);
    printf("************************\n");
    stu1.age = 22;
    printf("stu1.number:     %d\n",stu1.number);
    printf("stu1.name  :     %s\n",stu1.name);
    printf("stu1.sex   :     %s\n",stu1.sex);
    printf("stu1.age   :     %d\n",stu1.age);
    printf("stu1.score :     %f\n",stu1.score);
    printf("************************\n");
    stu1.score = 89.5;
    printf("stu1.number:     %d\n",stu1.number);
    printf("stu1.name  :     %s\n",stu1.name);
    printf("stu1.sex   :     %s\n",stu1.sex);
    printf("stu1.age   :     %d\n",stu1.age);
    printf("stu1.score :     %f\n",stu1.score);
   
    return 0;
}
