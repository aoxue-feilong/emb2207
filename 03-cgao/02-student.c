#include <stdio.h> 
#include <string.h> 


// 1. 结构体的定义 
struct student 
{
    int   number ;  // 学号  
    char  name[20];  // 保存姓名的数组 
    char  sex[10] ;  // male female
    int   age    ;  // 年龄 
    float score  ;  // 成绩 
}; 


int main(int argc, char const *argv[])
{
    // 2. 定义一个结构体变量 
    struct student stu1,stu2;  // 定义一个结构体变量stu1 , stu2 
    printf("sizeof(stu1.number)=%ld\n",sizeof(stu1.number)) ;  // sizeof(int) = 4

    // 计算数组占用内存的字节数,20个元素, 每个元素占用1个字节, 一共占用20个字节
    printf("sizeof(stu1.name  )=%ld\n",sizeof(stu1.name)) ;  

    // 计算数组占用内存的字节数,10个元素, 每个元素占用1个字节, 一共占用10个字节
    printf("sizeof(stu1.sex   )=%ld\n",sizeof(stu1.sex)) ;  

    // 计算age的字节数,int类型, 占用4个字节
    printf("sizeof(stu1.age   )=%ld\n",sizeof(stu1.age)) ;  

    // 计算score的字节数,float类型, 占用4个字节
    printf("sizeof(stu1.score )=%ld\n",sizeof(stu1.score)) ;  


    // 计算结构体的大小  4+20+10+4+4 = 42 , 还必须满足4字节对齐 , 因此要补2个字节 = 44 
    printf("sizeof(stu1       )=%ld\n",sizeof(stu1)) ;  
    printf("sizeof(struct student  )=%ld\n",sizeof(struct student)) ;  
    


    return 0;
}
