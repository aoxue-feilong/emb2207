#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char buf[] = {"U.S. News & World Report is an American media company that publishes news, \
                  advice, rankings, and analysis. It was launched in 1948 as the merger \
                  of domestic-focused weekly newspaper U.S. News and international-focused weekly \
                  magazine World Report. In 1995, the company launched 'usnews.com' and in 2010, \
                  the magazine ceased printing."};
    char ch;
    char *retp = NULL;
    char *startp = buf;//内存的起始位置
    int count = 0;
    while (1)
    {
        retp =strstr(startp,"news");
        if (retp != NULL)
        {
            count++;
            //找到字符后，更新startp的位置
            printf("startp=%s\n",retp);
            startp = retp + 1;
        }
        else 
        {
            startp++;
        }
        if(startp == &buf[strlen(buf)])
        {
            break;
        }
    }
    printf("news有%d次\n",count);
    return 0;
}
