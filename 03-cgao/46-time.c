#include <stdio.h>
#include <string.h>
#include <time.h>

char *week[]={"星期一","星期二","星期三","星期四","星期五","星期六","星期日"};
int main(int argc, char const *argv[])
{
    // long int seconds ;
    time_t seconds = time(NULL); // 或许系统时间的秒数
    printf("seconds=%ld\n", seconds);
    printf("time:%s", ctime(&seconds)); // 直接获取时间字符串, 西方的时间表示法 

    // localtime 的功能是把seconds 这个变量解析成一个结构体 struct tm 
    struct tm * tmp =  localtime(&seconds); // 
    printf("%04d-%02d-%02d %02d:%02d:%02d %s\n",tmp->tm_year+1900,tmp->tm_mon+1,tmp->tm_mday,
                                            tmp->tm_hour,tmp->tm_min,tmp->tm_sec,week[tmp->tm_wday-1]);
    return 0;
}