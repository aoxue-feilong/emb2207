#include <stdio.h>

#include <string.h>

typedef struct student
{
    int number;   //学号
    char name[10]; //保存姓名的数组
    char sex[10];  // male female
    int age;      //年龄
    float score;  //成绩
}student_t;
int main(int argc, char const *argv[])
{
    int buf[10];
    student_t stu1;
    memset(&stu1,0,sizeof(student_t));
    printf("stu1的信息为:\n");
    printf("stu1.number:     %d\n", stu1.number);
    printf("stu1.name  :     %s\n", stu1.name);
    printf("stu1.sex   :     %s\n", stu1.sex);
    printf("stu1.age   :     %d\n", stu1.age);
    printf("stu1.score :     %f\n", stu1.score);
    for (int i = 0; i < 10; i++)
    {
        printf("buf[%d]=%d\n",i,buf[i]);
    }
    memset(buf,0,sizeof(buf));
    for (int i = 0; i < 10; i++)
    {
        printf("buf[%d]=%d\n",i,buf[i]);
    }
    
    return 0;
}




