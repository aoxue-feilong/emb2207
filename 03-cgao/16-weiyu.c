#include <stdio.h>

typedef struct
{
    unsigned int led1 : 1;
    unsigned int led2 : 1;
    unsigned int beep : 1;
    unsigned int keyval : 3;
} cmd_t;
int main(int argc, char const *argv[])
{
    printf("sizeof(cmd_t)=%ld\n", sizeof(cmd_t));
    cmd_t cmd;      //结构体定义了一个结构体变量
    cmd.led1 = 1;   // led1赋值为1
    cmd.led2 = 0;   // led2赋值为0
    cmd.beep = 1;   // beep赋值为1
    cmd.keyval = 5; // keyval赋值为5
    printf("cmd.led1  =%d\n", cmd.led1);
    printf("cmd.led2  =%d\n", cmd.led2);
    printf("cmd.beep  =%d\n", cmd.beep);
    printf("cmd.keyval=%d\n", cmd.keyval);
    printf("***********************\n");
    //错误用法，提示溢出
    cmd.led1 = 10;
    cmd.led2 = 7;
    cmd.beep = 2;
    cmd.keyval = 10;
    printf("cmd.led1  =%d\n", cmd.led1);   //提示溢出，会产生逻辑问题，10=1010，取最低位，即0
    printf("cmd.led2  =%d\n", cmd.led2);   //提示溢出，会产生逻辑问题,7=0111，取最低位，即1
    printf("cmd.beep  =%d\n", cmd.beep);   //提示溢出，会产生逻辑问题，2=02，取最低位，即2
    printf("cmd.keyval=%d\n", cmd.keyval); //提示溢出，会产生逻辑问题，10=1010 取最低3位010，即2
    return 0;
}
