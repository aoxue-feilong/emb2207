cmake_minimum_required (VERSION 2.8)      

project (main)

aux_source_directory(src SRC_LIST)    # 获取目录下的源文件 

# TestFunc  是库的名称 
# STATIC    是静态库 
# ${SRC_LIST} 是源文件的列表  , 要除取main.c 文件
add_library (testFunc_Static STATIC ${SRC_LIST})  # 把beep.c 和 led.c 制作成静态库

# testFunc_Static 库的名称 
# testFunc  : 最终库输出的名称 
set_target_properties (testFunc_Static PROPERTIES OUTPUT_NAME "testFunc")


# 把生成的静态库 写入到原码目录下的lib目录内 
set (LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)
