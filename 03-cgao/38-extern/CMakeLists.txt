cmake_minimum_required (VERSION 2.8)      

project (main)

include_directories(. src)    # 添加标准头文件的搜索路径 , 程序中可以使用 include <>
  
aux_source_directory(.   SRC_LIST1)    # 获取目录下的源文件 
aux_source_directory(src SRC_LIST2)    # 获取目录下的源文件 


add_executable(main ${SRC_LIST1} ${SRC_LIST2})
