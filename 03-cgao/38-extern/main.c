#include <stdio.h>

#include "led.h"
#include "beep.h"

// 这里不是定义, 因此不能赋初始值, 只是声明, 告诉编译器这个变量在别的文件中定义了, 这里只是声明
// 编译不要报未找到变量
extern int led_count; // 不能赋初始值, 一旦赋值就变成定义变量了，extern在书写时，可以省略
extern int display_beep(void); // 函数的声明, 可以不写在.h 文件中 
int main(int argc, char const *argv[])
{
    led_on();
    led_off();
    char *p = beep_on();
    printf("*p=%s\n", p);

    p = beep_on();
    printf("*p=%s\n", p);

    p = beep_on();
    printf("*p=%s\n", p);

    led_count++;
    printf("led_count=%d\n", led_count);

    display_beep();

    return 0;
}