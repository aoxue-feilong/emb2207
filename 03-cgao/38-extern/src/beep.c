#include <stdio.h>

int led_count = 1; // 定义一个全局变量 , 这个全局变量想在main.c中使用

// 全局变量在工程中是唯一的变量, 如果出现重名就报错
static int temp = 100; // temp 的作用域就是在文件内 , 不会超出本文件, 不会出现工程中变量重名了
char *beep_on(void)
{

    static int a = 10; // static 修饰局部变量, 只能被初始化一次  , 值可以保持
    static int b;      // static 修饰的局部变量 没有赋初始值, 初始值为0
    static char buf[100] = {"hello world!!"};
    a++;
    b++;
    printf("beep_on:a=%d\n", a);
    printf("beep_on:b=%d\n", b);
    printf("beep_on\n");

    return buf;
}

int beep_off(void)
{
    printf("beep_off\n");

    return 0;
}

static int display(void)
{
    printf("display\n");
    return 0;
}

int display_beep(void)
{
    printf("display_beep\n");
    return 0;
}