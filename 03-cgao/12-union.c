#include <stdio.h>

struct mystruct
{
    unsigned char a;  // 1字节
    unsigned short b; // 2字节
    unsigned int c;   // 4字节
    unsigned long d;  // 8字节
};// 1+2+4+8 = 15 , 4字节对齐, 占用16字节
union myunion
{
    unsigned char a;
    unsigned short b;
    unsigned int c;
    unsigned long d;
};// 所有成员公用一块内存, 以最大字节成员的内存作为共用体的内存 , 占用8字节

int main(int argc, char const *argv[])
{
    struct mystruct st1 = {0};
    union myunion un1 = {0};
    printf("sizeof(st1)=%ld\n", sizeof(st1));
    printf("sizeof(un1)=%ld\n", sizeof(un1));
    un1.a = 0x11;
    printf("un1.a=%#x\n", un1.a);// 总大小8字节, 可以访问前1个字节的大小空间
    printf("un1.b=%#x\n", un1.b);// 总大小8字节, 可以访问前2个字节的大小空间
    printf("un1.c=%#x\n", un1.c);// 总大小8字节, 可以访问前4个字节的大小空间
    printf("un1.d=%#lx\n", un1.d);// 总大小8字节, 可以访问前8个字节的大小空间
    printf("***************\n");
    un1.b = 0x2211;
    printf("un1.a=%#x\n", un1.a);
    printf("un1.b=%#x\n", un1.b);
    printf("un1.c=%#x\n", un1.c);
    printf("un1.d=%#lx\n", un1.d);
    printf("***************\n");
    un1.c = 0x44332211;
    printf("un1.a=%#x\n", un1.a);
    printf("un1.b=%#x\n", un1.b);
    printf("un1.c=%#x\n", un1.c);
    printf("un1.d=%#lx\n", un1.d);
    printf("***************\n");
    un1.d = 0x665544332211;
    printf("un1.a=%#x\n", un1.a);
    printf("un1.b=%#x\n", un1.b);
    printf("un1.c=%#x\n", un1.c);
    printf("un1.d=%#lx\n", un1.d);

    printf("***********************\n");
    un1.a = 0x11; 
    un1.b = 0x22; 
    un1.c = 0x33; 
    un1.d = 0x44; 
    printf("un1.a=%#x\n",un1.a);   // 总大小8字节, 可以访问前1个字节的大小空间
    printf("un1.b=%#x\n",un1.b);   // 总大小8字节, 可以访问前2个字节的大小空间
    printf("un1.c=%#x\n",un1.c);   // 总大小8字节, 可以访问前4个字节的大小空间
    printf("un1.d=%#lx\n",un1.d);  // 总大小8字节, 可以访问前8个字节的大小空间
    //在使用共用体类型变量的数据时要注意：在共用体类型变量中起作用的成员是最后一次存放的成员，
    //在存入一个新的成员后原有的成员就失去作用
    return 0;
}
