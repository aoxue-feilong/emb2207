#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char *p;
    p=malloc(128);
    if(p==NULL)    //如果申请内存失败
    {
        printf("内存申请失败\n");
        exit(-1);
    }
    memset(p,0,128);
    memset(p,'A',127);
    printf("1:p=%s\n",p);
    free(p);//申请释放内存
    printf("2:p=%s\n",p);
    return 0;
}

