#include <stdio.h>

// 全局变量在工程中是唯一的变量, 如果出现重名就报错
static int temp = 100; // temp 的作用域就是在文件内 , 不会超出本文件, 不会出现工程中变量重名了

int led_on(void)
{
    printf("led_on\n");
    return 0;
}
int led_off(void)
{
    printf("led_off\n");
    return 0;
}
static int display(void)
{
    printf("dispaly\n");
    return 0;
}