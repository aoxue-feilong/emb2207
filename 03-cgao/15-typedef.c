#include <stdio.h>

typedef int func_t(int a, int b);

int add(int a, int b)
{
    return a + b;
}
int sub(int a, int b)
{
    return a - b;
}
int mul(int a, int b)
{
    return a * b;
}
int div(int a, int b)
{
    return a / b;
}

int main(int argc, char const *argv[])
{
    func_t *pfunc;
    pfunc = add;
    int ret1 = pfunc(10, 20);
    printf("a+b=%d\n", ret1);

    pfunc = sub;
    int ret2 = pfunc(10, 20);
    printf("a-b=%d\n", ret2);
    pfunc = mul;

    int ret3 = pfunc(10, 20);
    printf("a*b=%d\n", ret3);
    pfunc = div;

    int ret4 = pfunc(10, 20);
    printf("a/b=%d\n", ret4);
    return 0;
}
