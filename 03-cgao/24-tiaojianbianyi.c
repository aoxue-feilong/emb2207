#include <stdio.h>
int main(int argc, char const *argv[])
{

#if 1 //条件为真执行if后的代码，否则不执行
    printf("hello world!!\n");
#endif
#if 0
    printf("goodbye!!\n");
#endif

#if 0 //条件为真，执行if分支的代码，条件为假，执行else分支的代码
    printf("hello world!!\n");

#else
    printf("goodbye!!\n");
#endif

    return 0;
}
