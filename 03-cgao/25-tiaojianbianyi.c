#include <stdio.h>
int main(int argc, char const *argv[])
{
// 表达式1为真, 编译 #if 1  分支的代码
// 表达式1为假, 编译 #elif  分支的代码
#if 1
    printf("hello world!!\n");
#elif 0
    printf("goodbye\n");
#else
    printf("sorry\n");
#endif

// 表达式1为真, 编译 #if 0  分支的代码
// 表达式1为假, 编译 #elif  分支的代码
#if 0   // 表达式1   
    printf("hello world!!\n");
// 表达式2为真, 编译 #elif 1  分支的代码   
// 表达式2为假, 编译 #else   分支的代码
#elif 1 // 表达式2
    printf("goodbye\n");
#else
    printf("sorry\n");
#endif

// 表达式1为真, 编译 #if 0  分支的代码
// 表达式1为假, 编译 #elif  分支的代码
#if 0   // 表达式1   
    printf("hello world!!\n");
// 表达式2为真, 编译 #elif 1  分支的代码   
// 表达式2为假, 编译 #else   分支的代码
#elif 0 // 表达式2
    printf("goodbye\n");
#else
    printf("sorry\n");
#endif

    return 0;
}
