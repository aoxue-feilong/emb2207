#include <stdio.h>
#include <string.h>

struct
{
    int number;   // 学号
    char name[20]; // 保存姓名的数组
    char sex[10];  // male female
    int age;      // 年龄
    float score;  // 成绩
} stu1 = {1, "zhao", "female", 22, 90},stu2;

int main(int argc, char const *argv[])
{
    stu2.number = 2; // . 是一个运算符，用来访问结构体的成员
    strcpy(stu2.name, "liu");
    strcpy(stu2.sex, "male");
    stu2.age = 22;
    stu2.score = 89.5;

    printf("stu1 的信息:\n");
    printf("stu1.number:       %d\n", stu1.number);
    printf("stu1.name  :       %s\n", stu1.name);
    printf("stu1.sex   :       %s\n", stu1.sex);
    printf("stu1.age   :       %d\n", stu1.age);
    printf("stu1.score :       %f\n", stu1.score);

    printf("stu2 的信息:\n");
    printf("stu2.number:       %d\n", stu2.number);
    printf("stu2.name  :       %s\n", stu2.name);
    printf("stu2.sex   :       %s\n", stu2.sex);
    printf("stu2.age   :       %d\n", stu2.age);
    printf("stu2.score :       %f\n", stu2.score);
    return 0;
}
