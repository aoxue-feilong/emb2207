#include <stdio.h>
#include <string.h>

//结构体数组的初始化
struct student
{
    int number;    //学号
    char name[10]; //保存姓名的数组
    char sex[10];  // male female
    int age;       //年龄
    float score;   //成绩
};

int main(int argc, char const *argv[])
{
    struct student stu[3]={ {1,"li"  ,"male"  ,20,100},
                            {2,"chen","female",22,100},
                            {3,"liu" ,"male"  ,22,100},
                          };

    
    for (int i = 0; i < 3; i++)
    {
        printf("stu[%d]的信息为:\n", i);
        printf("stu[%d].number:     %d\n", i, stu[i].number);
        printf("stu[%d].name  :     %s\n", i, stu[i].name);
        printf("stu[%d].sex   :     %s\n", i, stu[i].sex);
        printf("stu[%d].age   :     %d\n", i, stu[i].age);
        printf("stu[%d].score :     %f\n", i, stu[i].score);
    }

    return 0;
}
