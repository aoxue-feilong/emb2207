#include <stdio.h>
#include <led.h>
#include <beep.h>

int main(int argc, char const *argv[])
{
    led_on();
    led_off();
    char *p = beep_on();
    printf("*p=%s\n", p);

    p = beep_on();
    printf("*p=%s\n", p);

    p = beep_on();
    printf("*p=%s\n", p);
    return 0;
}
