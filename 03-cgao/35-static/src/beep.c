#include <stdio.h>




char * beep_on(void)
{
    static int a = 10;// static 修饰局部变量, 只能被初始化一次  , 值可以保持 
    static int b ;// static 修饰的局部变量 没有赋初始值, 初始值为0 
    static char buf[100]={"hello world!!"};
    a++;
    b++;
    printf("beep_onn:a=%d\n",a);
    printf("beep_onn:b=%d\n",b);
    printf("beep_on\n");
    return buf;
}
int beep_off(void)
{
    printf("beep_off\n");
    return 0;
}