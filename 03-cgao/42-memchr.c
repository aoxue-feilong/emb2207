
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char buf[] = {"U.S. News & World Report is an American media company that publishes news, \
                consumer advice, rankings, and analysis. It was launched in 1948 as the merger \
                of domestic-focused weekly newspaper U.S. News and international-focused weekly \
                magazine World Report. In 1995, the company launched 'usnews.com' and in 2010, \
                the magazine ceased printing."};
    char ch;
    char *retp = NULL;
    char *startp = buf;// 内存的起始位置
    int count = 0;
    printf("请输入你要找的字符 >:");
    scanf("%c", &ch);
    while (1)
    {
        //  startp 从内存起始位置开始找
        // strlen(buf) , 这样写会有bug产生, 原因是这是一个固定长度的字节数, 实际搜索中, 随着查找的继续
        // 字符串的长度在减少 , 应该用 strlen(startp)计算
        retp = (char *)memchr(startp, ch, strlen(startp)); 
        if (retp != NULL) // 不为NULL 表示找到了字符, 我们计数
        {
            count++; // 累计
            //找到以后, 要更新startp的位置
            startp = retp+1; // 要跳过找到的字符, 找到字符的下一个位置开始继续搜索
            printf("retp =%s\n",retp);
        }
        else if (retp == NULL)
        {
            break;
        }
    }
    printf("%c 有 %d 个字符\n", ch, count);
    return 0;
}
