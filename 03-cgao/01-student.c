#include <stdio.h>
#include <string.h>

#define N 20

struct student
{
    int number;   //学号
    char name[N]; //保存姓名的数组
    char sex[N];  // male female
    int age;      //年龄
    float score;  //成绩
};

int main(int argc, char const *argv[])
{
    struct student stu1;
    stu1.number = 1; // . 是一个运算符，用来访问结构体的成员
    strcpy(stu1.name, "zhao");
    strcpy(stu1.sex, "male");
    stu1.age = 22;
    stu1.score = 89.5;

    printf("stu1的信息为:\n");
    printf("stu1.number:     %d\n", stu1.number);
    printf("stu1.name  :     %s\n", stu1.name);
    printf("stu1.sex   :     %s\n", stu1.sex);
    printf("stu1.age   :     %d\n", stu1.age);
    printf("stu1.score :     %f\n", stu1.score);

    return 0;
}
