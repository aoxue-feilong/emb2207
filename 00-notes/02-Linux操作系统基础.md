

## 第01章 linux系统的发展

### 1.1 什么是linux系统

##### 1.什么是Linux

需要从贝尔实验室的UNIX说起1969年，AT&T公司的贝尔实验室与MIT合作开发的Unix，旨在于创建一个用于大型、并行、多用户的操作系统。Unix的推广：从学校走进企业Unix的版本主要两个： AT&T System V ——就是俗称的“系统5”, Berkley Software Distribution (BSD)。

##### 2. Linux是一种操作系统

​	1991年，芬兰赫尔辛基大学的学生Linus Torvals为了能在家里的PC机上使用与学校一样的操作系统，开始编写了类UNIX。1991.8.25，Linux就在comp.os.minix新闻组中首次发布了一个Linux内核的公共版本。最初Linus编写的程序只适用于Intel386处理器，且不能移植。

​	由于人们的鼓励，继续编写可移植的Linux系统。之后，就有越来越多的计算机爱好者、程序员通过网络参与到Linux系统的不断完善之中。

##### 3. Linux的系统发展史

- 第一阶段：

  - ​	1969年，Ken.Thompson和Dennis Ritchie 开发UNIX 操作系统的原型，原先是为了运行星际旅行（Space 	travel）游戏。

  - ​	1972 年，Dennis Ritchie ，用C语言改写，使得UNIX 系统在大专院校得到了推广。

  - ​	总结：诞生后，在C语言改编下，在学校得到了推广。


- 第二阶段：

  - ​	1984年，Andrew S.Tanenbaum开发了用于教学的UNIX系统，命名为Mininx。为了方便教学，保持着小型化。


  - ​	1989年，Andrew S.Tanenbaum将Minix系统运行于x86的PC平台。


  - ​	总结：为了教学，进行了优化，缺乏实用操作系统。


- 第三阶段：

  - ​	1990年，芬兰赫尔辛基大学学生Linux Torvalds首次接触Minix系统。


  - ​	1991年，Linux Torvalds开始在Minix上编写各种驱动程序等操作系统内核组件。


  - ​	1991年年底，Linux Torvalds公开了Linux内核源码0.02版，此版本仅仅是部分代码。


  - ​	1993年，Linux 1.0版本发行，Linux转向GPL版本协议。


  - ​	1994年，Linux的第一个商业发行版Slackware问世。


  - ​	1996年，美国国家标准技术局的计算机系统实验室确认Linux版本1.2.13符合POSIX标准。


  - ​	1999年，Linux的简体中文发行版问世。


  - ​	linux是在unix中延伸出来的，也被称为类unix系统。


### 1.2 为什么要学习linux系统

​	在讲课时，我经常会问同学们一个问题：“为什么学习Linux系统？”很多学生会脱口而出：“因为Linux系统是开源的，所以要去学。”其实这个想法是完全错误的！开源的操作系统少说有100个，开源的软件至少也有10万个，为什么不去逐个学习？所以上面谈到的开源特性只是一部分优势，并不足以成为您付出精力去努力学习的理由。

​	对普通用户来讲，开源共享精神仅具备锦上添花的效果，我们更加看重的是，Linux系统是一款优秀的软件产品，具备类似UNIX系统的程序界面，并继承了其良好的稳定性。而且开源社区也在源源不断地提供高品质代码以及丰富的第三方软件支持，能够在高可用性、高性能等方面较好地满足工作需求。

​	还有一个应用场景—全球超级计算机竞赛。每年全球会评选出计算峰值速度最快的500台超级计算机，其中包括美国的Summit、Sierra和中国神威·太湖之光、天河二号等超级计算机。截至本书写作时，这些超级计算机无一例外采用的都是Linux操作系统。

​	开源社区更是与全球用户唇齿相依，任何人都可以贡献自己的代码与灵感，任何人也都可以从开源社区中受益。如此良性循环下来，开源软件便具备了4大关键性优势。

##### 1. 低风险

​	使用闭源软件无疑把命运交付给他人，一旦封闭的源代码没有人来维护，你将进退维谷；而且相较于商业软件公司，开源社区很少存在倒闭的问题。

##### 2. 低成本

​	开源工作者都是在幕后默默且无偿地付出劳动成果，为美好的世界贡献一份力量，因此使用开源社区推动的软件项目可以节省大量的人力、物力和财力。

##### 3.高品质

​	相较于闭源软件产品，开源项目通常是由开源社区来研发及维护的，参与编写、维护、测试的用户数量众多，一般的bug还没有等暴发就已经被修补。另外，在灵感不断碰撞、代码不断迭代的交流氛围中，程序员也不可能将“半成品”上传到开源社区中。

##### 4.更透明

​	没有哪个笨蛋会把木马或后门代码放到开源项目中，这样无疑是把自己的罪行暴露在阳光之下，很容易被他人发现。

### 1.3 开源精神及常见的Linux系统

​	一般情况下，软件的源代码只由编写者拥有，而开源（即开放源代码，Open Source Code）是指一种更自由的软件发布模式。简单来说，开源软件的特点就是把软件程序和源代码文件一起打包提供给用户，让用户在不受限制地使用某个软件功能的基础上还可以对代码按需修改，让软件更贴合硬件环境，让功能更符合工作需求。用户还可以将其编制成衍生产品再发布出去。用户一般享有使用自由、复制自由、修改自由、创建衍生品自由，以及收费自由。但是，如果开源软件只单纯追求“自由”而牺牲了程序员的利益，这肯定会影响程序员的创作热情。为了平衡两者的关系，截至目前，世界上已经有100多种被开源促进组织（OSI，Open Source Initiative）确认的开源许可证，用于保护开源工作者的权益。

##### 1. Red Hat

​	红帽企业版Linux最初于2002年3月面世，当年Dell、HP、Oracle以及IBM公司便纷纷表示支持该系统平台的硬件开发，因此红帽企业版Linux系统的市场份额在近20年时间内不断猛增。红帽企业版Linux当时是全世界使用最广泛的Linux系统之一，在世界500强企业中，所有的航空公司、电信服务提供商、商业银行、医疗保健公司均无一例外地通过该系统向外提供服务。红帽企业版Linux当前的最新版本是RHEL 8，该系统具有极强的稳定性，在全球范围内都可以获得完善的技术支持。

##### 2. CentOS

​	顾名思义，CentOS是由开源社区研发和维护的一款企业级Linux操作系统，在2014年1月被红帽公司正式收购。CentOS系统最为别人广泛熟悉的标签就是“免费”。如果您问一个运维“老鸟”选择CentOS系统的理由，他绝对不会跟你说更安全或更稳定，而只是说两个字—免费！由于红帽企业版Linux是开源软件，任何人都有修改和创建衍生品的权利，因此CentOS便是将红帽企业版Linux中的收费功能通通去掉，然后将新系统重新编译后发布给用户免费使用的Linux系统。也正因为其免费的特性，CentOS拥有了广泛的用户。

​	从本质上来说，由于CentOS是针对红帽企业版Linux进行修改后再发布的版本，因此不会针对它单独开发新功能，CentOS的版本号也是随红帽企业版Linux而变更。例如，CentOS 8.0对应的就是RHEL 8.0，CentOS 8.1对应的就是RHEL 8.1；以此类推。

##### 3. Fedora

​	Fedora翻译为中文是“浅顶软呢男帽”的意思，翻译之后跟Linux系统很不搭界，所以更多人干脆将其音译为“费多拉”系统。Fedora Linux是正正经经的红帽公司自己的产品，最初是为了给红帽企业版Linux制作和测试第三方软件而构建的产品，孕育了最早的开源社群，固定每6个月发布一个新版本，当前在全球已经有几百万的用户。

##### 4. Debian

一款基于GNU开源许可证的Linux系统，历史久远，最初发布于1993年9月。Debian的名字取自创始人Ian Murdock和他女朋友Debra的姓氏组合。在维基百科中，Debian被翻译为“蝶变”系统，多么浪漫而富有诗意的名字。但可惜国内的用户不买账，看着Logo一圈一圈的形状，硬生生地将经念歪了。这么多年下来，现在反而很少有人听说过蝶变系统这个名字了。

Debian系统具有很强的稳定性和安全性，并且提供了免费的基础支持，可以良好地适应各种硬件架构，以及提供近十万种不同的开源软件，在国外拥有很高的认可度和使用率。

##### 5. Ubuntu

​	ubuntu是一款桌面版Linux系统，以Debian为蓝本进行修改和衍生而来，发布周期为6个月。Ubuntu的中文音译为“乌班图”，这个词最初来自于非洲南部部落使用的祖鲁语，意思是“我的存在是因为大家的存在”，体现了一种谦卑、感恩的价值观，寓意非常好。

​	Ubuntu系统的第一个版本发布于2004年10月。2005年7月，Ubuntu基金会成立，Ubuntu后续不断增加开发分支，有了桌面版系统、服务器版系统和手机版系统。据调查，Ubuntu最高峰时的用户达到了10亿人。尽管Ubuntu基于Debian系统衍生而来，但会对系统进行深度化定制，因此两者之间的软件并不一定完全兼容。Ubuntu系统现在由Canonical公司提供商业技术支持，只要购买付费技术支持服务就能获得帮助，桌面版系统最长时间3年，服务器版系统最长时间5年。

##### 6. openSUSE Linux

一款源自德国的Linux系统，在全球范围内有着不错的声誉及市场占有率。openSUSE的桌面版系统简洁轻快易于使用，而服务器版本则功能丰富极具稳定性，而且即便是“菜鸟”也能轻松上手。虽然openSUSE在技术上颇具优势，而且大大的绿色蜥蜴Logo人见人爱，只可惜命途多舛，赞助和研发该系统的SuSE Linux AG公司由于效益不佳，于2003年被Novell公司收购，而Novell公司又因经营不佳而在2011年被Attachmate公司收购。而到了2014年，Attachmate公司又被Micro Focus公司收购，后者仍然只把维护openSUSE系统的团队当作公司内的一个部门来运营。

##### 7. Kali Linux

跟上面的呆萌大蜥蜴相比，Kali Linux的Logo似乎有点凶巴巴，一副不好惹的样子。这款系统一般是供黑客或安全人员使用的，能够以此为平台对网站进行渗透测试，通俗来讲就是能“攻击”网站。Kali Linux系统的前身名为BackTrack，其设计用途就是进行数字鉴识和渗透测试，内置有600多款网站及系统的渗透测试软件，包括大名鼎鼎的Nmap、Wireshark、sqlmap等。Kali Linux能够被安装到个人电脑、公司服务器，甚至手掌大小的树莓派（一款微型电脑）上，可以让人有一种随身携带了一个武器库的感觉，有机会真应该单独写本书聊聊它。

##### 8. 深度操作系统（deepin）

​	在过去的十多年，基于开源系统二次定制开发的“国产操作系统”陆续出现过一些，但大多发展不好，深度操作系统却是少数能够将技术研发与商业运作结合起来的成功案例。据Deepin的官网介绍，该系统是由武汉深之度科技有限公司于2011年基于Debian系统衍生而来的，提供32种语言版本，目前累计下载量已近1亿次，用户遍布100余个国家/地区。	

### 1.4 Ubuntu系统介绍

##### 1.ubuntu的由来

Ubuntu Linux是由南非人马克·沙特尔沃思(Mark Shuttleworth)创办的基于Debian Linux的操作系统，于2004年10月公布Ubuntu的第一个版本(Ubuntu 4.10“Warty Warthog”)。Ubuntu适用于笔记本电脑、桌面电脑和服务器，特别是为桌面用户提供尽善尽美的使用体验。Ubuntu几乎包含了所有常用的应用软件：文字处理、电子邮件、软件开发工具和Web服务等。用户下载、使用、分享未修改的原版Ubuntu系统，以及到社区获得技术支持，无需支付任何许可费用。

Ubuntu提供了一个健壮、功能丰富的计算环境，既适合家庭使用又适用于商业环境。Ubuntu社区承诺每6个月发布一个新版本，以提供最新最强大的软件。

##### 2.ubuntu的特点

Ubuntu在桌面办公、服务器方面有着不俗的表现，总能够将最新的应用特性囊括其中，主要包括以下几方面：

1、桌面系统使用最新的Gnome、KDE、Xfce等桌面环境组件。

2、集成搜索工具Tracker，为用户提供方便、智能的桌面资源搜索。

3、抛弃繁琐的X桌面配置流程，可以轻松使用图形化界面完成复杂的配置。

4、集成最新的Compiz稳定版本，让用户体验酷炫的3D桌面。

5、“语言选择”程序提供了常用语言支持的安装功能，让用户可以在系统安装后，方便地安装多语言支持软件包。

6、提供了全套的多媒体应用软件工具，包括处理音频、视频、图形、图像的工具。

7、集成了Libreoffice办公套件，帮助用户完成文字处理、电子表格、幻灯片播放等日常办公任务。

8、含有辅助功能，为残障人士提供辅助性服务。

9、支持蓝牙(Bluetooth)输入设备，如蓝牙鼠标、蓝牙键盘。

10、拥有成熟的网络应用工具，从网络配置工具到Firefox网页浏览器、Gaim即时聊天工具、电子邮件工具、BT下载工具等。

11、加入更多的打印机驱动，包括对HP的一体机(打印机、扫描仪集成)的支持。

12、进一步加强系统对笔记本电脑的支持，包括系统热键以及更多型号笔记本电脑的休眠与唤醒功能。

13、内置了Linux终端服务器功能，提供对以瘦客户机作为图形终端的支持，大大提高老式PC机的利用率。

14、Ubuntu 20.04 LTS提供对配备指纹识别功能笔记本的支持。可录制指纹和进行登陆认证。



## 第02章 linux系统的安装与配置

### 2.1 下载ubuntu18.04镜像(iso)

​	1. 官网下载地址: [Ubuntu 18.04.6 LTS (Bionic Beaver)](https://releases.ubuntu.com/18.04.6/)

​	Bionic Beaver :  仿生的河狸

​	LTS :  long team support  长期团队支持 

```c
amd64 表示是x86的64位系统
i386  表示的x86的32位系统
     
ubuntu-18.04.6-desktop-amd64.iso    //  desktop  表示桌面版 
ubuntu-18.04.6-live-server-amd64.iso // live  表示在线安装, 联网安装  server :表示服务器版本
```

2. 国内下载方式 

   [Index of /ubuntu-releases/18.04.6/ | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/ubuntu-releases/18.04.6/)

### 2.2 VMWare 16虚拟机安装

1. VMWare Workstation 16 Pro 介绍 

Workstation 16 Pro 基于行业定义的技术，在以下方面实现了改进：DirectX 11 和 OpenGL 4.1 3D 加速图形支持、全新的“暗黑模式”用户界面、在 Windows 10 版本 2004 和更高版本的主机上对 Windows Hyper-V 模式的支持、一个用于支持容器和 Kubernetes 集群的新 CLI“vctl”，以及对最新 Windows 和 Linux 操作系统的支持等。

2. VMWare虚拟机安装软件下载

   官方下载地址: [下载 VMware Workstation Pro | CN](https://www.vmware.com/cn/products/workstation-pro/workstation-pro-evaluation.html)

   下载地址官网 : https://download3.vmware.com/software/WKST-1624-WIN/VMware-workstation-full-16.2.4-20089737.exe

3. VMware Workstation 安装

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/7NKpKLY2lhQXCcf7Uaw5tw.png)        

接受许可条款

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/tDe12sem4qeN3yzE9fPpMQ.png)        

选择虚拟机软件的安装路径

自定义虚拟机软件的安装路径。一般情况下无须修改安装路径，但如果您担心C盘容量不足，则可以考虑修改安装路径，将其安装到其他位置。

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/9K2DGkxSN2PAf-FoYYbhyg.png)        

用户体验设置

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/A9OeFdwsJbvC3fpLLO8t2w.png)        

创建快捷方式

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/0R7PJDR9SnaNjXpxHVHu1w.png)        

 准备开始安装虚拟机

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/X7OVCwAKUmLSI7KyNGJOsQ.png)        

等待安装完成

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/m1UBtysZHk9WSatGnCGV-Q.png)        

安装向导完成界面

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/n36_PkIwlZTUrCt7vkQT3Q.png)        

双击桌面上生成的虚拟机快捷图标，在弹出的如图所示的界面中，输入许可证密钥（如果已经购买了的话）。大多数同学此时应该是没有许可证密钥，所以我们当前选中“我希望试用VMware Worksatation 16 30天”单选按钮，然后单击“继续”按钮。

或者是"ZF3R0-FHED2-M80TY-8QYGC-NPKYF" , 仅限于学习使用。

许可证密钥验证界面

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/m2PkNqEtfdBE3goqJQrsvQ.png)        

虚拟机软件的感谢页面

​                 ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/ax9QIDmm19iCKc_eNsVpRw.png)        

双击桌面的图标， 运行软件如下： 

![img](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816100109207.png)

### 2.3 安装ubuntu18.04系统

```c
-> 创建新的虚拟机 
->  典型(推荐)
->  安装程序光盘镜像文件 
-> 点击浏览   
-> 选择 ubuntu-18.04.6-desktop-amd64.iso
-> 点击下一步 
-> 个性linux名称 :计算机名称 例如:ubuntu
   用户名        : linux 
   密码          : 1 
   确认密码       : 1 
->  点击下一步 
->  虚拟机名称    :  Ubuntu18.04
    路径         :  E:\YShengli\06-VirtualMachines\Ubuntu18.04
-> 下一步  
-> 最大磁盘大小   : 50G 
   选择 "将虚拟磁盘存储位单个文件"
-> 点击下一步 
-> 点击完成
-> 开启此虚拟机 
->系统开始自动安装
    
```

<img src="https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816103054008.png" alt="image-20220816103054008"  />



![image-20220816103406798](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816103406798.png)



![image-20220816103503069](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816103503069.png)

![image-20220816103503069](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816103742612.png)

![image-20220816103817922](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816103817922.png)

![image-20220816104050967](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816104050967.png)

![image-20220816104215935](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816104215935.png)

![image-20220816104644315](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816104644315.png)

![image-20220816104855228](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816104855228.png)

![image-20220816110618134](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816110618134.png)





### 2.4 安装Ubuntu18.04 错误汇总

##### 1. 虚拟化没开启的错误解决

- 在很多电脑出厂时, 虚拟化技术默认没有开启, 需要手动的在bios中开启 

  ![image-20220816110618134](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816111417852.png)

![image-20220816111445459](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816111445459.png)

- 以联想电脑为例 ， 首先重启电脑，连续按下“ F2 ”进入bios设置。

   ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/14968248_20220424131413_60790_thumb.jpg)

- 打开后，找到并进入“ **security** ”设置。

   ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/14968248_20220424131437_12005_thumb.jpg)

- 然后在下面打开“ **virtualization** ”虚拟化。

   ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/14968248_20220424131502_74478_thumb.jpg)

- 随后选中它右边的选项。

   ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/14968248_20220424131523_19879_thumb.jpg)

- 打开后，将它改成“ **enabled** ”就能打开vt了。

   ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/14968248_20220424131543_55058_thumb.jpg)

- 最后按下“ **F10** ”并选择“ **yes** ”保存即可。

   ![img](https://gitee.com/embmaker/cloudimage/raw/master/img/14968248_20220424131601_78308_thumb-166062013812536.jpg)



##### 2. 丢失vcruntime140_1.dll的解决

![image-20220816142321887](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816142321887.png)

- VCRUNTIME140.dll丢失”是Windows计算机上经常可能发生的错误。通常，在安装Windows更新或特定程序安装失败后，会出现此问题。
- win10小伙是不是经常出现打开软件或游戏的时候出现启动弹出缺少VCRUNTIME140_1.DLL

```c
->window图标 
-> 设置  
-> 应用 
-> 选择 MicroSoft Visual C++ 2015-2019 Red... 
-> 选择卸载
-> 弹窗选择 "修复" 
-> 即可解决 
```

![image-20220816142922068](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816142922068.png)

![image-20220816143204987](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816143204987.png)





##### 3. vmnet0上的网桥没有运行

​	出现这个错误的大部分都是vmware软件安装的问题, 都是因为安装路径设置不合适造成的。

使用 geek 这个卸载软件去卸载VMware 软件  

geek的下载地址如下: 

链接：https://pan.baidu.com/s/1EihSdoEAJ3yHCZa4JScffg 
提取码：1234

```shell
-> 运行geek 软件 
-> 右击 VMware Workstation 
-> 卸载 
-> 重启系统 
-> 重新安装 VMware , 选择默认安装路径即可解决这个问题, 注意 不要修改路径
```

![image-20220816161851454](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816161851454.png)



### 2.5 Ubuntu系统配置

#### 1. Ubuntu设置镜像源服务器

```c
-> 点击左下角图标 
-> ALL 
-> Software & Update 
-> Download From , 单机下拉列表 
-> Select Best Server 
-> Choose Server 
->  输入密码:1  
    点击 :Authentic (认证)
-> 点击 CLose 
-> Reload . 等待加载完成后,自动退出 
-> 此后在安装软件是, 已经切换到国内的服务了 
```

- 安装Ubuntu后, 默认的服务器是美国的服务器, 会比较慢, 需要把服务器切换到国内的服务器。 

  ![image-20220816143919602](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816143919602.png)

![image-20220816144040411](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816144040411.png)

![image-20220816144118305](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816144118305.png)



![image-20220816144619966](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816144619966.png)



![image-20220816144720542](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816144720542.png)

![image-20220816144746209](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816144746209.png)



![image-20220816144827471](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816144827471.png)

![image-20220816144857200](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816144857200.png)

#### 2. Ubuntu网络设置 

```shell
-> 双击网络连接图标 
-> 桥接模式 
-> 点击确定
-> 查看右上角的网络连接图标 
-> ... 出现三个点, 表示正在连接
-> 没有出现问号, 就表示网络连接正常
```



![image-20220816150248196](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816150248196.png)

![image-20220816150429563](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816150429563.png)

![image-20220816150547328](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816150547328.png)



#### 3. 把系统设置成中文

```shell
-> 左下角图标 
-> ALL 
-> Language Support 
-> 中文包没有安装完全, 需要继续安装  选择 install 
-> 单机 汉语(中文) 向上拖动到最上边 
-> Apply System-wide
-> 点击 Close
-> 重启系统
-> 重启后, 需要选择是否更新文件夹名称  , 选择"选择保留旧的名称"


```

- 操作步骤:


![image-20220816151344141](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816151344141.png)

![image-20220816151506033](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816151506033.png)



![image-20220816151634053](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816151634053.png)



![image-20220816151811155](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816151811155.png)



![image-20220816152243235](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816152243235.png)



![image-20220816152352495](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816152352495.png)



![image-20220816152748495](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220816152748495.png)



####   4. 安装open-vmware-tools 

```shell
1. VMTool 这个工具可以实现ubuntu窗口最大化, 还支持复制, 粘贴, 拖拽等扩展功能 

2. 启动一个终端 
    -> 左下角的图标 
    -> ALL 
    -> 终端 
    -> 在运行的终端上右击 
    -> 添加到收藏夹 
    -> 在终端中 输入一个安装命令 
3. 安装VMTool
    sudo apt update      # 注释内容  更新服务器的软件清单 
	sudo apt install open-vm-tools-desktop
	-> 提示输入密码: 1 
    -> 即可实现安装

```

![image-20220817085916836](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817085916836.png)





#### 5. 无法获得锁 /var/lib/dpkg/lock-frontend - open (11: 资源暂时不可用)

```shell
linux@ubuntu:~$ sudo apt-get install open-vm-tools-desktop
[sudo] linux 的密码： 
E: 无法获得锁 /var/lib/dpkg/lock-frontend - open (11: 资源暂时不可用)
E: 无法获取 dpkg 前端锁 (/var/lib/dpkg/lock-frontend)，是否有其他进程正占用它？

#是因为 后台有进程在更新资源, 因此可以关闭更新即可 
# 主要是因为系统在开机时, 要进行自动检查更新, 会占用服务 

```

解决办法: 

```shell
-> 左下角图标 
-> ALL  
-> 软件和更新 
-> 更新, 设置如下:
自动检查更新: 从不
当有安全更新: 立即显示 
当有其他更新: 每两周一次
有新版本时通知我: 从不 
-> 点击关闭 
-> 重启系统
```

![image-20220817090806584](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817090806584.png)



#### 6. Ubuntu系统时间设置 

```shell
-> 左下角的图标
-> 全部应用 
-> 设置 
-> 详细信息 
-> 日期和时间 
-> 选择时区 
-> 选择上海时区
```

![image-20220817094252888](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817094252888.png)

![image-20220817094421248](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817094421248.png)

![image-20220817094457381](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817094457381.png)

![image-20220817094655613](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817094655613.png)

![image-20220817094854743](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817094854743.png)

#### 7. 自动登录Ubuntu系统不输入密码

```shell
-> 左下角的图标 
-> 全部应用 
-> 设置 
-> 详细信息 
-> 用户 
-> 单机logo 可以给用户名更换登录图标 
-> 点击右上角的解锁 
-> 修改用户名为:linux 
   自动登录    : 打开

->重启系统后, 发现可以直接进入系统 , 不需要输入密码 
```

![image-20220817095230927](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817095230927.png)



![image-20220817095343194](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817095343194.png)





#### 8. 更换壁纸 

```shell
-> 右击桌面 
-> 更换背景 
-> 背景 : 是做面的背景 
   锁定屏幕 : 在系统锁屏时显示的图片
```

#### 9. 任务栏(Dock)的设置

```shell
-> 左下角图标 
-> ALL 
-> 设置 
-> Dock (任务栏)
  自动隐藏 : 开启程序时, 会自动隐藏任务栏 
  图标大小 : 可以改变任务栏的图标大小 
  屏幕上的位置: 可以左侧 , 右侧 , 底部 
```

![image-20220817100330455](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817100330455.png)

#### 10. 区域与语言

```shell
-> 左下角图标 
-> ALL 
-> 设置 
-> 区域和语言
  格式 : 中国  
```

![image-20220817100812253](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817100812253.png)

![image-20220817100931231](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817100931231.png)



#### 11. Ubuntu不待机的设置

```shell
-> 左下角图标 
-> ALL 
-> 设置 
-> 电源
  空白屏幕 : 从不  # 表示是永不待机, 屏幕常亮, 不待机, 不息屏 , 适合编译程序使用
```

![image-20220817101425481](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817101425481.png)



#### 12. 把系统升级到最新 

```shell
-> 左下角图标 
-> 全部
-> 软件更新器 
-> 设置 
	选中 :重要安全更新 
	选中 :推荐更新
	选中 :不支持的更新 
	自动检查更新:从不 
	当有安全更新时 :立即显示 
	当有其他更新时 :每两周显示一次
    当有新版本时通知我 :从不 
-> 点击关闭 
-> 立即安装
-> 重启系统

```

![image-20220817102332865](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817102332865.png)

![image-20220817102534237](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817102534237.png)

![image-20220817102559849](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817102559849.png)

![image-20220817102631063](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817102631063.png)

![image-20220817102936823](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817102936823.png)

![image-20220817103130725](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817103130725.png)

#### 13. 终端字体设置

```shell
-> 左下角图标 
-> 全部
-> 终端
-> 编辑  
-> 首选项 
-> 选中 :自定义字体  # 可以修改字体和字体的大小
   光标形状 : I形   # 方块, I形 , 下划线
-> 关闭即可设置完成 

```

![image-20220817103431404](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817103431404.png)





![image-20220817103353869](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817103353869.png)

![image-20220817103624059](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817103624059.png)



#### 14. 放大终端和缩小终端的快剪键

```shell 
ctrl + shift + '+' # 放大终端字体
ctrl + '-'         # 缩小终端字体
```



#### 15. Ubuntu系统的CPU和内存配置

```shell
1. 因为系统使用的默认的配置, 可以更具自己的需求调整

2. 关闭Ubuntu18.04 , 在VMware中进行编辑 

内存配置依据 :  看自己系统的内存, 至少2G , 我的配置:2G
CPU个数依据 :  查看设备管理器, 确认自己电脑cpu的个数, 一般设置一半的处理器即可 , 我的配置: 4
USB控制器   :  兼容 3.1  #可以兼容3.1的usb设备 

```

![image-20220817104349901](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817104349901.png)



![image-20220817104719652](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817104719652.png)



![image-20220817104859873](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817104859873.png)

![image-20220817105212614](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817105212614.png)



![image-20220817105241129](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817105241129.png)



#### 16. Ubuntu配置好的系统压缩包

以上是Ubuntu配置的镜像, 已经做好并压缩 , 使用时解压后使用VMware的打开虚拟机 , 即可打开使用 

```shell
-> VMware 
-> 文件 
-> 打开 
-> 把Ubuntu18.04.7z 解压到当前文件夹 
-> 在弹窗选择 "Ubuntu18.04.vmx"
-> 开启虚拟机
```

![image-20220818085621792](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220818085621792.png)

Ubuntu18.04.7z的百度云盘连接地址: 

```shell
链接  ：https://pan.baidu.com/s/1fZ8SIOLHJXYUFPpN7Pa_Vg 
提取码：1234
```

### 2.6 VMware 的配置 

#### 1. 虚拟网络编辑器

```shell
VMnet0 : 桥接模式   #  Ubuntu 有一个独立的ip 和window 是独立的两个系统
VMnet1 : 尽主机模式 #  在专用的网络内连接虚拟机
VMnet8 : 共享主机的IP #  Ubuntu 和window 公用一个IP , 适合需要使用账号和密码上网是用这个比较好

# 如果以上的网卡乱了, 或者被删除了, 可以使用 "还原默认配置" 进行恢复  

```

![image-20220817141038840](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817141038840.png)

![image-20220817141542235](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817141542235.png)



![image-20220817141601826](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817141601826.png)





#### 2. VMware的首选项 



![image-20220817142107520](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817142107520.png)



![image-20220817142206871](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817142206871.png)





## 第03章 Shell基础

### 3.1 Shell的理解

随着各式Linux系统的图形化程度的不断提高，用户在桌面环境下，通过点击、拖拽等操作就可以完成大部分的工作。

然而，许多Ubuntu Linux功能使用Shell命令来实现，要比使用图形界面交互，完成的**更快、更直接**。 

​	英文单词Shell可直译为“贝壳”。“贝壳”是动物作为外在保护的一种工具。

​	可以这样认为，Linux中的Shell就是Linux内核的一个外层保护工具，并负责完成用户与内核之间的交互

![image-20220817151001355](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220817151001355.png)

**命令**是用户向系统内核发出控制请求，与之交互的文本流。

Shell是一个命令行解释器，将用户命令解析为操作系统所能理解的指令，实现用户与操作系统的交互。

同时，Shell为操作系统提供了内核之上的功能，直接用来管理和运行系统。

当需要重复执行若干命令，可以将这些命令集合起来，加入一定的控制语句，编辑成为Shell脚本文件，交给Shell批量执行。

### 3.2 命令提示符格式

```shell
linux@ubuntu:~$ 
root@ubuntu:/home/linux# 

linux  , root  :是用户名 
root 用户是管理员权限的用户, 等价于 window 的 admin 用户 
@      : 是一个分隔符号 
ubuntu : 计算机名 
~      : 家目录  , 就是/home/linux , 家目录是根目录的一个子目录 
/      : 根目录  , 目录的起始位置 

$      : 表示用户是普通用户 
#      : 表示用户是超级用户 
```

### 3.3 命令的格式

```shell
#命令格式  Shell命令格式 
通常一条命令包含三个要素：命令名称、选项、参数。命令名称是必须的，选项和参数都可能是可选项。
命令格式如下所示：
Command [-Options] Argument1 Argument2  …
指令      选项      参数1      参数2      …

#Command：命令名称，Shell命令或程序，严格区分大小写，例如设置日期指令为date等；
#Options：命令选项，用于改变命令执行动作的类型，由“-”引导，可以同时带有多个选项；
#Argument：命令参数，指出命令作用的对象或目标，有的命令允许带多个参数。

#一条命令的三要素之间用空格隔开；   注意 注意 
#若将多个命令在一行书写，用分号（;）将各命令隔开；
#如果一条命令不能在一行写完，在行尾使用反斜杠（\）标明该条命令未结束。
#命令不带选项或参数，通常意为使用默认选项或参数。
```

### 3.4 命令的回溯 

```shell
使用键盘的上下键 来进行显示命令历史 
↑ : 用来向前回溯命令
↓ : 用来向后回溯命令

window下的powershell 也支持这种操作 

```

### 3.5 移动光标

```shell
1. 可借助上箭头按键，来获得上次输入的命令。
2. 使用左右箭头按键。定位到命令行的任意位置 , 编辑命令
```

### 3.6 鼠标和光标  

```shell
1. 可以在终端仿真器里使用鼠标。 X 窗口系统支持快速拷贝和粘贴技巧。按下鼠标左键，沿着文本拖动鼠标（或者双击一个单词）高亮了一些文本，那么这些高亮的文本就被拷贝到了一个由 X管理的缓冲区里面。然后按下鼠标中键，这些文本就被粘贴到光标所在的位置

2. 不要在一个终端窗口里使用 Ctrl-c 和 Ctrl-v 快捷键来执行拷贝和粘贴操作。它们不起作用。
    复制的快捷键:  ctrl + shift + c
    粘贴的快捷键:  ctrl + shift + v
    
3. ctrl + c :作用是结束一个正在运行的程序 

4. ctrl + l  : 清除屏幕显示的快捷键 
```

### 3.7 测试命令 

```shell
date :显示系统的时间
cal  :显示系统的日历 
df   :查看磁盘使用情况
free :显示系统的空闲内存   
```

```shell
linux@ubuntu:~/Desktop$ date
2022年 08月 17日 星期三 14:57:47 CST
linux@ubuntu:~/Desktop$ cal
      八月 2022         
日 一 二 三 四 五 六  
    1  2  3  4  5  6  
 7  8  9 10 11 12 13  
14 15 16 17 18 19 20  
21 22 23 24 25 26 27  
28 29 30 31           
                      
linux@ubuntu:~/Desktop$ df
文件系统          1K-块    已用     可用 已用% 挂载点
udev             974532       0   974532    0% /dev
tmpfs            199940    1612   198328    1% /run
/dev/sda1      51288544 8959400 39691424   19% /
tmpfs            999684       0   999684    0% /dev/shm
tmpfs              5120       4     5116    1% /run/lock
tmpfs            999684       0   999684    0% /sys/fs/cgroup
/dev/loop1         2560    2560        0  100% /snap/gnome-calculator/884
/dev/loop0        56832   56832        0  100% /snap/core18/2128
/dev/loop2       410496  410496        0  100% /snap/gnome-3-38-2004/112
/dev/loop3          640     640        0  100% /snap/gnome-logs/112
/dev/loop4          640     640        0  100% /snap/gnome-logs/106
/dev/loop5          768     768        0  100% /snap/gnome-characters/726
/dev/loop6        56960   56960        0  100% /snap/core18/2538
/dev/loop7         2688    2688        0  100% /snap/gnome-calculator/920
/dev/loop8       224256  224256        0  100% /snap/gnome-3-34-1804/72
/dev/loop9       247168  247168        0  100% /snap/gnome-3-38-2004/70
/dev/loop10        2560    2560        0  100% /snap/gnome-system-monitor/163
/dev/loop11       63488   63488        0  100% /snap/core20/1611
/dev/loop12        2688    2688        0  100% /snap/gnome-system-monitor/178
/dev/loop13         128     128        0  100% /snap/bare/5
/dev/loop14       63488   63488        0  100% /snap/core20/1593
/dev/loop15       93952   93952        0  100% /snap/gtk-common-themes/1535
/dev/loop16       48128   48128        0  100% /snap/snapd/16292
/dev/loop17       66688   66688        0  100% /snap/gtk-common-themes/1515
/dev/loop18         768     768        0  100% /snap/gnome-characters/741
/dev/loop19      224256  224256        0  100% /snap/gnome-3-34-1804/77
tmpfs            199936      28   199908    1% /run/user/1000
linux@ubuntu:~/Desktop$ free
              总计         已用        空闲      共享    缓冲/缓存    可用
内存：     1999368     1020104      153640       17880      825624      804584
交换：     2097148        1036     2096112
linux@ubuntu:~/Desktop$ 
	
```

### 3.8 Shell补齐命令与文件名

在shell解释器的快捷键中，Tab键绝对是使用频率最高的，它能够实现对命令、参数或文件的内容补全。例如，如果想执行reboot重启命令，但一时想不起来该命令的完整拼写，则可以这样输入：

1. tab 实现命令的补全 

```shell
linux@ubuntu:~/Desktop$ re<Tab键><Tab键>
read                     readonly                 recode-sr-latin          regdbdump               
remove-shell             reset                    return
readarray                readprofile              recountdiff              
reject                   rename.ul                resize2fs                rev
readelf                  realpath                 red                      
remove-default-ispell    rendercheck              resizecons               
readlink                 reboot                   rediff                  
remove-default-wordlist  renice                   resizepart      
linux@ubuntu:~/Desktop$ reb<Tab键> 
linux@ubuntu:~/Desktop$ reboot 
```

2. tab 实现文件名的补全  

```shell
linux@ubuntu:~$ touch hello.c
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  hello.c  Music  Pictures  Public  snap  Templates  Videos
linux@ubuntu:~$ vi he<Tab键>   #文件名补全
linux@ubuntu:~$ vi hello.c    # 按下两次 shift +z 退出 
```

### 3.8 Shell查询命令历史

history命令用于显示执行过的命令历史，语法格式为  “history [-c]”。

history命令应该是运维人员最喜欢的命令。执行history命令能显示出当前用户在本地计算机中执行过的最近1000条命令记录。如果觉得1000不够用，可以自定义/etc/profile文件中的HISTSIZE变量值。

在使用history命令时，可以使用-c参数清空所有的命令历史记录。还可以使用“!编码数字”的方式来重复执行某一次的命令。

```shell
linux@ubuntu:~$ history 
    1  ls
    2  sudo apt-get install open-vm-tools-desktop
    3  exit
    4  sudo apt install open-vm-tools-desktop
    5  sudo apt autoremove 
    6  ls
    7  sudo apt install open-vm-tools-desktop
    8  ls
    9  shell
   10  ls
   11  sudo su 
   12  pwd
   13  ls
   14  cd Desktop/
   15  ls

linux@ubuntu:~$ !1    # 执行命令历史的第一个命令 
ls
Desktop  Documents  Downloads  hello.c  Music  Pictures  Public  snap  Templates  Videos
linux@ubuntu:~$ history -c   # 清除命令的记录 
linux@ubuntu:~$ history 
    1  history 
```

### 3.9 Shell命令别名

- 命令别名通常是其他命令的缩写，用来减少键盘输入。该命令的一般格式如下所示。


```shell
alias   alias-name='original-command'    # 添加命令的别名 
unalias  alias-name                      # 删除命令的别名 
```

- 例如 : 


```shell
linux@ubuntu:~$ alias myls='ls -l'   # 单引号, 不是反引号 ,别弄错, 中间不能有空格  
linux@ubuntu:~$ myls 
总用量 36
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Downloads
-rw-rw-r-- 1 linux linux    0 8月  17 15:18 hello.c
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Music
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Pictures
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Public
drwx------ 4 linux linux 4096 8月  17 10:19 snap
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Templates
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Videos
linux@ubuntu:~$ ls -l
总用量 36
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Downloads
-rw-rw-r-- 1 linux linux    0 8月  17 15:18 hello.c
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Music
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Pictures
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Public
drwx------ 4 linux linux 4096 8月  17 10:19 snap
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Templates
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Videos
linux@ubuntu:~$ unalias myls    # 删除命令的别名 
linux@ubuntu:~$ myls

Command 'myls' not found, did you mean:

  command 'tyls' from deb terminology
  command 'mmls' from deb sleuthkit

Try: sudo apt install <deb name>

linux@ubuntu:~$ 
```

### 3.10 Shell中的特殊字符

#### 1. 通配符 

当需要用命令处理一组文件，例如file1.txt、file2.txt、file3.txt……，用户不必一一输入文件名，可以使用Shell通配符

```shell
1. 星号（*）: 匹配任意长度的字符串  用file_*.txt，匹配file_wang.txt、file_zhang.txt、file_liu.txt


2. 问号（?）: 匹配一个长度的字符 用flie_?.txt，匹配file_1.txt、file1_2.txt、file_3.txt

3. 方括号（[…]） :匹配其中指定的一个字符 用file_[otr].txt，匹配file_o.txt、file_r.txt和file_t.txt

4. 方括号（[ - ]）:匹配指定的一个字符范围 用file_[a-z].txt，匹配file_a.txt、file_b.txt，直到file_z.txt

5. 方括号（[^…]） : 除了其中指定的字符，均可匹配 用file_[^otr].txt，除了file_o.txt、file_r.txt和file_t.txt的其他文件
   在c语言清除垃圾字符时, 使用过这个表达 scanf("%*[^\n]")
```

- 操作结果

```shell
# 通配符* 的理解 
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos
linux@ubuntu:~$ mkdir work
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cd work/
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ touch file_wang.c file_liu.c file_zhang.c
linux@ubuntu:~/work$ ls
file_liu.c  file_wang.c  file_zhang.c
linux@ubuntu:~/work$ ls file_*
file_liu.c  file_wang.c  file_zhang.c
linux@ubuntu:~/work$ rm file_*
linux@ubuntu:~/work$ ls
# 通配符 ? 的理解 
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ touch file_1.c file_2.c file_3.c
linux@ubuntu:~/work$ ls
file_1.c  file_2.c  file_3.c
linux@ubuntu:~/work$ ls file?
ls: 无法访问'file?': 没有那个文件或目录
linux@ubuntu:~/work$ ls file*
file_1.c  file_2.c  file_3.c
linux@ubuntu:~/work$ ls file_?
ls: 无法访问'file_?': 没有那个文件或目录
linux@ubuntu:~/work$ ls file_?.c
file_1.c  file_2.c  file_3.c
linux@ubuntu:~/work$ rm file_?.c
linux@ubuntu:~/work$ ls 

# [] 通配符的理解 
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ touch file_o.c file_t.c file_r.c file_1.c file_2.c file_3.c
linux@ubuntu:~/work$ ls
file_1.c  file_2.c  file_3.c  file_o.c  file_r.c  file_t.c
linux@ubuntu:~/work$ ls file_[otr].c
file_o.c  file_r.c  file_t.c
linux@ubuntu:~/work$ ls file_[otr12].c
file_1.c  file_2.c  file_o.c  file_r.c  file_t.c
linux@ubuntu:~/work$ ls file_[ot].c
file_o.c  file_t.c
linux@ubuntu:~/work$ rm  file_[otr123].c
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ 

# [-] 通配符的理解
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ touch file_a.c file_b.c file_x.c file_z.c 
linux@ubuntu:~/work$ ls
file_a.c  file_b.c  file_x.c  file_z.c
linux@ubuntu:~/work$ ls file_[a-n].c
file_a.c  file_b.c
linux@ubuntu:~/work$ ls file_[a-x].c
file_a.c  file_b.c  file_x.c
linux@ubuntu:~/work$ ls file_[a-z].c
file_a.c  file_b.c  file_x.c  file_z.c
linux@ubuntu:~/work$ rm  file_[a-z].c
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ 

# [^]  通配符的理解
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ touch file_1.c file_2.c file_a.c file_b.c file_x.c file_z.c
linux@ubuntu:~/work$ ls
file_1.c  file_2.c  file_a.c  file_b.c  file_x.c  file_z.c
linux@ubuntu:~/work$ ls file[^\n].c
ls: 无法访问'file[^n].c': 没有那个文件或目录
linux@ubuntu:~/work$ ls file_[^\n].c
file_1.c  file_2.c  file_a.c  file_b.c  file_x.c  file_z.c
linux@ubuntu:~/work$ ls file_[^1].c
file_2.c  file_a.c  file_b.c  file_x.c  file_z.c
linux@ubuntu:~/work$ ls file_[^12].c
file_a.c  file_b.c  file_x.c  file_z.c
linux@ubuntu:~/work$ ls file_[^12a].c
file_b.c  file_x.c  file_z.c
linux@ubuntu:~/work$ ls file_[^\n].c
file_1.c  file_2.c  file_a.c  file_b.c  file_x.c  file_z.c
linux@ubuntu:~/work$ rm file_[^\n].c
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ 
```

#### 2. 管道 

管道可以把一系列命令连接起来，意味着第一个命令的输出将作为第二个命令的输入，通过管道传递给第二个命令，第二个命令的输出又将作为第三个命令的输入，以此类推。就像通过使用“|”符连成了一个管道。

- 例如：


```shell
linux@ubuntu:~/work$ cd ..   # 返回上一级目录 
linux@ubuntu:~$ ls -l        # 查看目录的详细信息
总用量 40
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Downloads
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Music
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Pictures
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Public
drwx------ 4 linux linux 4096 8月  17 10:19 snap
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Templates
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Videos
drwxrwxr-x 2 linux linux 4096 8月  18 09:53 work
linux@ubuntu:~$ ls -l | grep work    # grep work 搜索关键字 work , 输出搜索的结果 
drwxrwxr-x 2 linux linux 4096 8月  18 09:53 work
linux@ubuntu:~$ ls -l | grep Music
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Music
linux@ubuntu:~$ ls -l | grep Desk
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
linux@ubuntu:~$ ls -l | grep D
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Downloads
linux@ubuntu:~$ 

```

#### 3. 输入/输出重定向

- 输入/输出重定向是改变Shell命令或程序默认的标准输入/输出目标，重新定向到新的目标。

- Linux中默认的标准输入定义为键盘，标准输出定义为终端窗口 
- 输出重定向 > 

```shell
# 输出重定向 
>  file   #  把输出的内容 定向到文件内 
ls > log 
# 解释 如果有同名文件log ， 同名文件会被删除 ，如果文件不存在则创建这个文件 
```

- 例如： 


```shell
linux@ubuntu:~$ ls 
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work

#如果有同名文件log ， 同名文件会被清空后再写入新的内容 ，如果文件不存在则创建这个文件 
linux@ubuntu:~$ ls > log   # 把输出到终端的内, 写到文件内  
linux@ubuntu:~$ cat log    # 显示文件中的内容 
Desktop
Documents
Downloads
log
Music
Pictures
Public
snap
Templates
Videos
work
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  log  Music  Pictures  Public  snap  Templates  Videos  work
```

- 输出重定向  >>

```shell
 >> file  :将file文件重定向为输出源，追加模式
```

- 例如： 


```shell
linux@ubuntu:~$ cat log 
Desktop
Documents
Downloads
log
Music
Pictures
Public
snap
Templates
Videos
work
linux@ubuntu:~$ ls -l >>log   # 如果log 不存在则创建, 如果存在则追加写入, 文件内容不清空
linux@ubuntu:~$ cat log 
Desktop
Documents
Downloads
log
Music
Pictures
Public
snap
Templates
Videos
work
总用量 44
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Downloads
-rw-rw-r-- 1 linux linux   81 8月  18 10:22 log
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Music
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Pictures
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Public
drwx------ 4 linux linux 4096 8月  17 10:19 snap
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Templates
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Videos
drwxrwxr-x 2 linux linux 4096 8月  18 09:53 work
linux@ubuntu:~$ 
```

- 输入重定向 

```shell
 < file  ： 将file文件重定向为输入源
```

- 例如： 


```shell
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  log  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ grep work <log
work
drwxrwxr-x 2 linux linux 4096 8月  18 09:53 work
linux@ubuntu:~$ cat log 
Desktop
Documents
Downloads
log
Music
Pictures
Public
snap
Templates
Videos
work
总用量 44
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Downloads
-rw-rw-r-- 1 linux linux   81 8月  18 10:22 log
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Music
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Pictures
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Public
drwx------ 4 linux linux 4096 8月  17 10:19 snap
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Templates
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Videos
drwxrwxr-x 2 linux linux 4096 8月  18 09:53 work
linux@ubuntu:~$ 
```

### 3.11 Shell命令置换

- 命令替换是将一个命令的输出作为另一个命令的参数。命令格式如下所示。

```shell
command1  `command2`   # `  是反引号
```

其中，命令command2的输出将作为命令command1的参数。需要注意，命令置换的单引号为ESC键下方的“`”键 

- 例如： 


```shell
linux@ubuntu:~$ pwd    # 显示当先的目录 
/home/linux
linux@ubuntu:~$ ls `pwd`     # 等价于 ls /home/linux
Desktop  Documents  Downloads  log  Music  Pictures  Public  snap  Templates  Videos  work
```

### 3.12 Shell的常用命令

#### 1. ls 命令

- ls命令用于显示目录中的文件信息，英文全称为“list”，语法格式为 “ls [参数] [文件名称] ”。


- 所处的工作目录不同，当前工作目录下能看到的文件肯定也不同。使用ls命令的-a参数可以看到全部文件（包括隐藏文件），使用-l参数可以查看文件的属性、大小等详细信息。将这两个参数整合之后，再执行ls命令即可查看当前目录中的所有文件并输出这些文件的属性信息：
- 命令格式

```shell
ls 参数  文件名/目录
# ls  目录     
# ls  /home/linux   : 显示制定目录下的内容
# ls                : 默认显示当前目录下的内容 
# ls .              : 显示当前目录下的内容 , 等价于 ls 
# ls ..             : 显示上一级目录中的内容 
# ls ~              : 显示家目录中的内容  ~目录就是/home/linux
# ls /              : 显示根目录下的内容 

# ls -l  文件/目录   :  查看文件的属性、大小等详细信息 
# ls -l  work  
# ls -l  hello.c   

# ls -la 文件/目录   :  查看文件的属性、大小等详细信息 , 包括隐藏文件, linux下以点开头的文件都是隐藏文件 
# ls -la  # 缩写形式 ll 
```

- 操作结果

```shell
linux@ubuntu:~$ ls /home/linux/    # 指定目录
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ ls           #默认是当前目录 
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ ls .        # . 是当前目录 
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ ls ..       # .. 是上一级目录 
linux   
linux@ubuntu:~$ ls ~        #  ~ 是家目录 /home/linux
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ ls -l .     # 详细显示 
总用量 40
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Downloads
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Music
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Pictures
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Public
drwx------ 4 linux linux 4096 8月  17 10:19 snap
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Templates
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Videos
drwxrwxr-x 2 linux linux 4096 8月  18 09:53 work
linux@ubuntu:~$ ls -la     # 详细显示 包括隐藏文件
总用量 104
drwxr-xr-x 18 linux linux 4096 8月  18 11:02 .
drwxr-xr-x  3 root  root  4096 8月  16 10:57 ..
-rw-------  1 linux linux  165 8月  17 14:39 .bash_history
-rw-r--r--  1 linux linux  220 8月  16 10:57 .bash_logout
-rw-r--r--  1 linux linux 3771 8月  16 10:57 .bashrc
drwx------ 17 linux linux 4096 8月  17 09:59 .cache
drwx------ 12 linux linux 4096 8月  17 09:59 .config
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Downloads
drwx------  3 linux linux 4096 8月  16 14:37 .gnupg
-rw-------  1 linux linux 4444 8月  18 08:58 .ICEauthority
drwx------  3 linux linux 4096 8月  16 11:03 .local
drwx------  5 linux linux 4096 8月  16 15:11 .mozilla
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Music
-rw-r--r--  1 linux linux  363 8月  17 10:09 .pam_environment
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Pictures
-rw-r--r--  1 linux linux  807 8月  16 10:57 .profile
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Public
drwx------  4 linux linux 4096 8月  17 10:19 snap
drwx------  2 linux linux 4096 8月  16 14:37 .ssh
-rw-r--r--  1 linux linux    0 8月  17 09:03 .sudo_as_admin_successful
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Templates
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Videos
drwxrwxr-x  2 linux linux 4096 8月  18 09:53 work
-rw-rw-r--  1 linux linux  131 8月  17 10:19 .xinputrc
linux@ubuntu:~$ ll    # 是 ls -la 的缩写 
总用量 104
drwxr-xr-x 18 linux linux 4096 8月  18 11:02 ./
drwxr-xr-x  3 root  root  4096 8月  16 10:57 ../
-rw-------  1 linux linux  165 8月  17 14:39 .bash_history
-rw-r--r--  1 linux linux  220 8月  16 10:57 .bash_logout
-rw-r--r--  1 linux linux 3771 8月  16 10:57 .bashrc
drwx------ 17 linux linux 4096 8月  17 09:59 .cache/
drwx------ 12 linux linux 4096 8月  17 09:59 .config/
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Desktop/
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Documents/
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Downloads/
drwx------  3 linux linux 4096 8月  16 14:37 .gnupg/
-rw-------  1 linux linux 4444 8月  18 08:58 .ICEauthority
drwx------  3 linux linux 4096 8月  16 11:03 .local/
drwx------  5 linux linux 4096 8月  16 15:11 .mozilla/
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Music/
-rw-r--r--  1 linux linux  363 8月  17 10:09 .pam_environment
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Pictures/
-rw-r--r--  1 linux linux  807 8月  16 10:57 .profile
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Public/
drwx------  4 linux linux 4096 8月  17 10:19 snap/
drwx------  2 linux linux 4096 8月  16 14:37 .ssh/
-rw-r--r--  1 linux linux    0 8月  17 09:03 .sudo_as_admin_successful
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Templates/
drwxr-xr-x  2 linux linux 4096 8月  16 11:03 Videos/
drwxrwxr-x  2 linux linux 4096 8月  18 09:53 work/
-rw-rw-r--  1 linux linux  131 8月  17 10:19 .xinputrc
linux@ubuntu:~$ 
```

#### 2. cd 命令 

- cd命令用于切换当前的工作路径，英文全称为“change directory”，语法格式为 “cd [参数] [目录]”。
- 这个命令应该是最常用的一个Linux命令了。可以通过cd命令迅速、灵活地切换到不同的工作目录。

```shell
命令格式  :   cd [参数] [目录]
cd          :  等价于 cd ~   进入家目录,  等价于 cd /home/linux 
cd   ~      :  进入家目录 , 等价于 cd  
cd   -      :  返回上一次所在的目录 , 退回上一步 
cd  .       :  进入当前目录, 已经在当前目录了, 相当于原地踏步 
cd  ..      :  返回上一级目录 
cd  ../../  :  返回上一级的上一级 , 依次类推 
cd  /       :  进入更目录内 
cd ~/work    :  进入家目录下的work目录内 
```

- 操作结果


```shell
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cd    # 进入到家目录 
linux@ubuntu:~$ cd /  # 进入到根目录
linux@ubuntu:/$ ls
bin   cdrom  etc   initrd.img      lib    lost+found  mnt  proc  run   snap  swapfile  tmp  var      vmlinuz.old
boot  dev    home  initrd.img.old  lib64  media       opt  root  sbin  srv   sys       usr  vmlinuz
linux@ubuntu:/$ cd ~  # 进入到家目录  , 可以缩写为cd 
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cd -   # 返回上一次的目录 
/
linux@ubuntu:/$ cd -   # 返回上一次的目录 
/home/linux
linux@ubuntu:~$ cd .   # 原地踏步 
linux@ubuntu:~$ cd ..  # 返回上一级 
linux@ubuntu:/home$ cd ..  # 返回上一级 
linux@ubuntu:/$ cd 
linux@ubuntu:~$ cd /     #  进入到根目录
linux@ubuntu:/$ cd ~/work   #  进入到家目录的work子目录内 
linux@ubuntu:~/work$ 
```

#### 3. 绝对路径与相对路径

- 绝对路径 : 开始于根目录(/)，紧跟着目录树的一个个分支，一直到达所期望的目录或文件
- 相对路径 : 以当前的目录为参考而表示出的目录 , 不带绝对路径的 '/' 
- .               : 表示的当前的工作目录 
  ..              : 表示的当前工作目录的上一级目录 
- 实例操作

```shell
linux@ubuntu:~/work$ cd /     # 绝对路径进入根目录 
linux@ubuntu:/$ ls
bin   cdrom  etc   initrd.img      lib    lost+found  mnt  proc  run   snap  swapfile  tmp  var      vmlinuz.old
boot  dev    home  initrd.img.old  lib64  media       opt  root  sbin  srv   sys       usr  vmlinuz
linux@ubuntu:/$ cd home/linux/work/     # 在根目录下, 以相对路径进入work目录
linux@ubuntu:~/work$ cd /home/linux/    # 以绝对路径进入家目录 
linux@ubuntu:~$ cd work/                # 以相对路径进入家目录 
linux@ubuntu:~/work$ ls                 
linux@ubuntu:~/work$ cd ..       # 以相对路径,进入上一级目录
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cd /home/linux/work/     # 绝对路径 
linux@ubuntu:~/work$ cd ..       # 相对路径 
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cd work/     # 相对路径 

# 根目录 和家目录的区别 
linux@ubuntu:~$ pwd      
/home/linux
linux@ubuntu:~$ cd /
linux@ubuntu:/$ pwd
/
linux@ubuntu:/$ 
```

#### 4. touch 命令 

- 用于创建一个或多个空文件。
- 命令格式:  

```shell
touch [OPTION]  FILE 
```

- 例如 

```shell
touch  file1    #  创建换一个文件 
touch  file1 file2 file3 file4    #  创建换多个文件 ， 参数之间要用空格隔开 
```

- 操作运行

```shell
linux@ubuntu:~/work$ touch 1.c 2.c 3.c 4.c 
linux@ubuntu:~/work$ ls -l 
总用量 0
-rw-rw-r-- 1 linux linux 0 8月  18 14:20 1.c
-rw-rw-r-- 1 linux linux 0 8月  18 14:20 2.c
-rw-rw-r-- 1 linux linux 0 8月  18 14:20 3.c
-rw-rw-r-- 1 linux linux 0 8月  18 14:20 4.c
linux@ubuntu:~/work$ touch 5.c 6.c 7.c 8.c 
linux@ubuntu:~/work$ ls
1.c  2.c  3.c  4.c  5.c  6.c  7.c  8.c
linux@ubuntu:~/work$ 
```

#### 5. cat命令

- 把文件的内容显示到终端上  
- 命令格式:

```shell
cat [OPTION]  [FILE] 
```

- 例如

```shell
cat file1        # 把文件的内容显示到终端上来 
cat file1 file2  # 把多个文件的内容显示到终端上来 
```

- 运行结果

```shell
linux@ubuntu:~/work$ echo "hello world">1.c
linux@ubuntu:~/work$ echo "goodbye">2.c
linux@ubuntu:~/work$ cat 1.c 
hello world
linux@ubuntu:~/work$ cat 2.c 
goodbye
linux@ubuntu:~/work$ cat 1.c 2.c 
hello world
goodbye
linux@ubuntu:~/work$ echo "hello world"
hello world
linux@ubuntu:~/work$ echo "goodbye"
goodbye
linux@ubuntu:~/work$ 
```

#### 6. mkdir 命令 

- 用户创建一个目录 
- 命令格式

```shell
mkdir [OPTION]   DIRECTORY  
```

- 例如

```shell
mkdir   目录                 #创建目录   
mkdir   -p 目录              #创建目录 , 如果目录存在不报错  , 允许创建子目录 
mkdir dir1 dir2 dir3 dir4   #一次创建多个目录
```

- 运行结果

```shell
linux@ubuntu:~/work$ ls
1.c  2.c  3.c  4.c  5.c  6.c  7.c  8.c  dir1
linux@ubuntu:~/work$ mkdir dir2
linux@ubuntu:~/work$ ls
1.c  2.c  3.c  4.c  5.c  6.c  7.c  8.c  dir1  dir2
linux@ubuntu:~/work$ mkdir dir1     # 如果目录已经存在会报错 
mkdir: 无法创建目录"dir1": 文件已存在
linux@ubuntu:~/work$ mkdir -p dir1  # 如果目录已经存在不会报错 
linux@ubuntu:~/work$ ls 
1.c  2.c  3.c  4.c  5.c  6.c  7.c  8.c  dir1  dir2
linux@ubuntu:~/work$ mkdir -p dir1   # 如果目录已经存在不会报错 
linux@ubuntu:~/work$ mkdir  dir1/dir2/dir3/dir4  # 不允许创建子目录 
mkdir: 无法创建目录"dir1/dir2/dir3/dir4": 没有那个文件或目录
linux@ubuntu:~/work$ mkdir -p dir1/dir2/dir3/dir4  # mkdir -p 允许创建子目录 
linux@ubuntu:~/work$ ls dir<TAB><TAB>
dir1/ dir2/ 
linux@ubuntu:~/work$ ls dir1/dir2/dir3/dir4/ 
linux@ubuntu:~/work$ ls dir1/dir2/dir3/dir4/ -l
总用量 0
linux@ubuntu:~/work$ ls
1.c  2.c  3.c  4.c  5.c  6.c  7.c  8.c  dir1  dir2
linux@ubuntu:~/work$ mkdir -p dir1 dir2 dir3 dir4
linux@ubuntu:~/work$ ls
1.c  2.c  3.c  4.c  5.c  6.c  7.c  8.c  dir1  dir2  dir3  dir4
linux@ubuntu:~/work$ 
```

#### 7. rmdir 命令(不建议使用)

- 删除一个空目录 
- 命令格式

```shell
rmdir [OPTION]  DIRECTORY
```

- 例如

```shell
rmdir  目录   #目录内不为空 , 不为空的话, 删除错误 
rmdir  -p  dir1/dir2/dir2/dir4     # 可以删除层次的目录 
```



- 操作结果

```shell
linux@ubuntu:~/work$ rmdir dir1     # 目录内不为空, 不能删除 
rmdir: 删除 'dir1' 失败: 目录非空
linux@ubuntu:~/work$ 
linux@ubuntu:~/work$ rmdir -p dir1
rmdir: 删除 'dir1' 失败: 目录非空
linux@ubuntu:~/work$ rmdir -p dir1/dir2/dir3/dir4/
linux@ubuntu:~/work$ ls 
1.c  2.c  3.c  4.c  5.c  6.c  7.c  8.c  dir2  dir3  dir4
linux@ubuntu:~/work$ 

```

#### 8. rm 命令(替代rmdir)

- rm命令用于删除文件或目录，英文全称为“remove”
- 语法格式为 

```shell
rm [参数] 文件名称 
```

- 在Linux系统中删除文件时，系统会默认向您询问是否要执行删除操作，如果不想总是看到这种反复的确认信息，可在rm命令后跟上-f参数来强制删除。要想删除一个目录，需要在rm命令后面加一个-r参数才可以，否则删除不掉。

- rm命令的参数及其作用如表所示。

| 参数 | 作用       |
| ---- | ---------- |
| -f   | 强制执行   |
| -i   | 删除前询问 |
| -r   | 删除目录   |
| -v   | 显示过程   |

- 例如

```shell
rm  file1    #删除file1 文件
rm -r dir1   #删除目录dir1 
rm -rf  文件或目录     # 强制删除目录, 不报错误提示
```

- 操作过程

```shell
linux@ubuntu:~/work$ ls
1.c  2.c  3.c  4.c  5.c  6.c  7.c  8.c  dir2  dir3  dir4
linux@ubuntu:~/work$ rm 1.c
linux@ubuntu:~/work$ rm dir2/
rm: 无法删除'dir2/': 是一个目录
linux@ubuntu:~/work$ rm -r dir2/
linux@ubuntu:~/work$ rm -rf dir3 1.c 4.c 
linux@ubuntu:~/work$ ls
2.c  3.c  5.c  6.c  7.c  8.c  dir4
linux@ubuntu:~/work$ 
```

#### 9. pwd 命令

- 显示当前的工作目录
- 命令格式

```shell
pwd [OPTION]
```

- 操作运行

```shell
linux@ubuntu:~/work$ pwd
/home/linux/work
```

#### 10. clear 命令

- 清除终端屏幕显示的内容
- 命令格式

```shell
clear [-Ttype] [-V] [-x]      #  快捷键为  : ctrl + l  
```

- 操作运行

```shell
linux@ubuntu:~/work$ clear   # 命令执行后, 屏幕显示内容清除 , 并不是把现实的内容清除, 只是翻页显示新的页
```

#### 11. man 命令

- 在线参考手册的接口, 可以给出命令的使用帮助 
- 命令格式 

```shell
man  命令/函数名
```

- 例如

```shell
man 是系统的手册分页程序。指定给 man 的 页 选项通常是程序、工具或函数名。程序将显示每一个找到的相关 手册页。如果指定了 章节，man  将只在手册的指定  章节  搜索。默认将按预定的顺序查找所有可用的  章节  (默认是“1 n l 8 3 2 3posix 3pm 3perl 3am 5 4 9 6 7”，除非被 /etc/manpath.config 中的SECTION 指令覆盖)，并只显示找到的第一个 页，即使多个 章节 中都有这个 页面。

下表显示了手册的 章节 号及其包含的手册页类型。
1   可执行程序或 shell 命令
2   系统调用(内核提供的函数)
3   库调用(程序库中的函数)
4   特殊文件(通常位于 /dev)
5   文件格式和规范，如 /etc/passwd
6   游戏
7   杂项(包括宏包和规范，如 man(7)，groff(7))
8   系统管理命令(通常只针对 root 用户)
9   内核例程 [非标准

# 使用方式 

man  章节 命令    
man  ls     #查找ls 命令的帮助
man  rmdir  #查找rmdir 命令的帮助
man 1 read  # 在第1章(可执行程序)中去查找read 命令 
man 2 read  # 在第2章(可执行程序)中去查找read 命令 
man  man    # 给出man命令本省的帮助 
man 命令的退出使用 q 退出 
```

- 操作结果

```shell
linux@ubuntu:~/work$ man 1 stat     # 在第1章中去找stat  
linux@ubuntu:~/work$ man 2 stat     # 在第2章中去找stat  
在第 2 节中没有关于 stat 的手册页条目。
当没有手册页时，可以用 man 7 undocumented 命令来寻求帮助。
linux@ubuntu:~/work$ sudo apt install gcc   # 安装gcc 编译器 
[sudo] linux 的密码： 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
将会同时安装下列软件：
  gcc-7 libasan4 libatomic1 libc-dev-bin libc6-dev libcilkrts5 libgcc-7-dev libitm1 liblsan0 libmpx2 libquadmath0 libtsan0 libubsan0 linux-libc-dev
  manpages-dev
建议安装：
  gcc-multilib make autoconf automake libtool flex bison gcc-doc gcc-7-multilib gcc-7-doc gcc-7-locales libgcc1-dbg libgomp1-dbg libitm1-dbg
  libatomic1-dbg libasan4-dbg liblsan0-dbg libtsan0-dbg libubsan0-dbg libcilkrts5-dbg libmpx2-dbg libquadmath0-dbg glibc-doc
下列【新】软件包将被安装：
  gcc gcc-7 libasan4 libatomic1 libc-dev-bin libc6-dev libcilkrts5 libgcc-7-dev libitm1 liblsan0 libmpx2 libquadmath0 libtsan0 libubsan0 linux-libc-dev
  manpages-dev
升级了 0 个软件包，新安装了 16 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
需要下载 18.8 MB 的归档。
解压缩后会消耗 75.2 MB 的额外空间。
您希望继续执行吗？ [Y/n] y
获取:1 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libitm1 amd64 8.4.0-1ubuntu1~18.04 [27.9 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libatomic1 amd64 8.4.0-1ubuntu1~18.04 [9,192 B]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libasan4 amd64 7.5.0-3ubuntu1~18.04 [358 kB]
获取:4 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 liblsan0 amd64 8.4.0-1ubuntu1~18.04 [133 kB]
获取:5 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libtsan0 amd64 8.4.0-1ubuntu1~18.04 [288 kB]
获取:6 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libubsan0 amd64 7.5.0-3ubuntu1~18.04 [126 kB]
获取:7 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libcilkrts5 amd64 7.5.0-3ubuntu1~18.04 [42.5 kB]
获取:8 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libmpx2 amd64 8.4.0-1ubuntu1~18.04 [11.6 kB]
获取:9 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libquadmath0 amd64 8.4.0-1ubuntu1~18.04 [134 kB]
获取:10 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libgcc-7-dev amd64 7.5.0-3ubuntu1~18.04 [2,378 kB]
获取:11 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 gcc-7 amd64 7.5.0-3ubuntu1~18.04 [9,381 kB]
获取:12 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 gcc amd64 4:7.4.0-1ubuntu2.3 [5,184 B]
获取:13 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 libc-dev-bin amd64 2.27-3ubuntu1.6 [71.9 kB]
获取:14 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 linux-libc-dev amd64 4.15.0-191.202 [984 kB]
获取:15 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 libc6-dev amd64 2.27-3ubuntu1.6 [2,587 kB]
获取:16 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 manpages-dev all 4.15-1 [2,217 kB]
已下载 18.8 MB，耗时 5秒 (3,644 kB/s)      
正在选中未选择的软件包 libitm1:amd64。
(正在读取数据库 ... 系统当前共安装有 151844 个文件和目录。)
正准备解包 .../00-libitm1_8.4.0-1ubuntu1~18.04_amd64.deb  ...
正在解包 libitm1:amd64 (8.4.0-1ubuntu1~18.04) ...
正在选中未选择的软件包 libatomic1:amd64。
正准备解包 .../01-libatomic1_8.4.0-1ubuntu1~18.04_amd64.deb  ...
正在解包 libatomic1:amd64 (8.4.0-1ubuntu1~18.04) ...
正在选中未选择的软件包 libasan4:amd64。
正准备解包 .../02-libasan4_7.5.0-3ubuntu1~18.04_amd64.deb  ...
正在解包 libasan4:amd64 (7.5.0-3ubuntu1~18.04) ...
正在选中未选择的软件包 liblsan0:amd64。
正准备解包 .../03-liblsan0_8.4.0-1ubuntu1~18.04_amd64.deb  ...
正在解包 liblsan0:amd64 (8.4.0-1ubuntu1~18.04) ...
正在选中未选择的软件包 libtsan0:amd64。
正准备解包 .../04-libtsan0_8.4.0-1ubuntu1~18.04_amd64.deb  ...
正在解包 libtsan0:amd64 (8.4.0-1ubuntu1~18.04) ...
正在选中未选择的软件包 libubsan0:amd64。
正准备解包 .../05-libubsan0_7.5.0-3ubuntu1~18.04_amd64.deb  ...
正在解包 libubsan0:amd64 (7.5.0-3ubuntu1~18.04) ...
正在选中未选择的软件包 libcilkrts5:amd64。
正准备解包 .../06-libcilkrts5_7.5.0-3ubuntu1~18.04_amd64.deb  ...
正在解包 libcilkrts5:amd64 (7.5.0-3ubuntu1~18.04) ...
正在选中未选择的软件包 libmpx2:amd64。
正准备解包 .../07-libmpx2_8.4.0-1ubuntu1~18.04_amd64.deb  ...
正在解包 libmpx2:amd64 (8.4.0-1ubuntu1~18.04) ...
正在选中未选择的软件包 libquadmath0:amd64。
正准备解包 .../08-libquadmath0_8.4.0-1ubuntu1~18.04_amd64.deb  ...
正在解包 libquadmath0:amd64 (8.4.0-1ubuntu1~18.04) ...
正在选中未选择的软件包 libgcc-7-dev:amd64。
正准备解包 .../09-libgcc-7-dev_7.5.0-3ubuntu1~18.04_amd64.deb  ...
正在解包 libgcc-7-dev:amd64 (7.5.0-3ubuntu1~18.04) ...
正在选中未选择的软件包 gcc-7。
正准备解包 .../10-gcc-7_7.5.0-3ubuntu1~18.04_amd64.deb  ...
正在解包 gcc-7 (7.5.0-3ubuntu1~18.04) ...
正在选中未选择的软件包 gcc。
正准备解包 .../11-gcc_4%3a7.4.0-1ubuntu2.3_amd64.deb  ...
正在解包 gcc (4:7.4.0-1ubuntu2.3) ...
正在选中未选择的软件包 libc-dev-bin。
正准备解包 .../12-libc-dev-bin_2.27-3ubuntu1.6_amd64.deb  ...
正在解包 libc-dev-bin (2.27-3ubuntu1.6) ...
正在选中未选择的软件包 linux-libc-dev:amd64。
正准备解包 .../13-linux-libc-dev_4.15.0-191.202_amd64.deb  ...
正在解包 linux-libc-dev:amd64 (4.15.0-191.202) ...
正在选中未选择的软件包 libc6-dev:amd64。
正准备解包 .../14-libc6-dev_2.27-3ubuntu1.6_amd64.deb  ...
正在解包 libc6-dev:amd64 (2.27-3ubuntu1.6) ...
正在选中未选择的软件包 manpages-dev。
正准备解包 .../15-manpages-dev_4.15-1_all.deb  ...
正在解包 manpages-dev (4.15-1) ...
正在设置 libquadmath0:amd64 (8.4.0-1ubuntu1~18.04) ...
正在设置 libatomic1:amd64 (8.4.0-1ubuntu1~18.04) ...
正在设置 libasan4:amd64 (7.5.0-3ubuntu1~18.04) ...
正在设置 libcilkrts5:amd64 (7.5.0-3ubuntu1~18.04) ...
正在设置 libubsan0:amd64 (7.5.0-3ubuntu1~18.04) ...
正在设置 libtsan0:amd64 (8.4.0-1ubuntu1~18.04) ...
正在设置 linux-libc-dev:amd64 (4.15.0-191.202) ...
正在设置 liblsan0:amd64 (8.4.0-1ubuntu1~18.04) ...
正在设置 libmpx2:amd64 (8.4.0-1ubuntu1~18.04) ...
正在设置 libc-dev-bin (2.27-3ubuntu1.6) ...
正在设置 manpages-dev (4.15-1) ...
正在设置 libc6-dev:amd64 (2.27-3ubuntu1.6) ...
正在设置 libitm1:amd64 (8.4.0-1ubuntu1~18.04) ...
正在设置 libgcc-7-dev:amd64 (7.5.0-3ubuntu1~18.04) ...
正在设置 gcc-7 (7.5.0-3ubuntu1~18.04) ...
正在设置 gcc (4:7.4.0-1ubuntu2.3) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在处理用于 libc-bin (2.27-3ubuntu1.6) 的触发器 ...
linux@ubuntu:~/work$ man 1 stat    # 第1章查找stat   , 可以找到帮助  
linux@ubuntu:~/work$ man 2 stat    # 第2章查找stat   , 也可以找到函数使用帮助 

```



## 第04章 vim编辑器

### **4.1 ** vim的介绍

Vim 是从 vi 发展出来的一个文本编辑器。代码补全、编译及错误跳转等方便编程的功能特别丰富，在程序员中被广泛使用。
简单的来说， vi 是老式的字处理器，不过功能已经很齐全了，但是还是有可以进步的地方。 vim 则可以说是程序开发者的一项很好用的工具。连 vim 的官方网站 (https://www.vim.org/) 自己也说 vim 是一个程序开发工具而不是文字处理软件

### 4.2 **vi与vim 之间的关系** 

​	本质上是一个程序  

### 4.3 安装vim 工具

```shell
linux@ubuntu:~$ sudo apt install vim  # 安装vim 工具 
[sudo] linux 的密码： 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
将会同时安装下列软件：
  vim-runtime
建议安装：
  ctags vim-doc vim-scripts
下列【新】软件包将被安装：
  vim vim-runtime
升级了 0 个软件包，新安装了 2 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
需要下载 6,589 kB 的归档。
解压缩后会消耗 32.0 MB 的额外空间。
您希望继续执行吗？ [Y/n] y
获取:1 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 vim-runtime all 2:8.0.1453-1ubuntu1.8 [5,435 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 vim amd64 2:8.0.1453-1ubuntu1.8 [1,154 kB]
已下载 6,589 kB，耗时 1秒 (8,736 kB/s)
正在选中未选择的软件包 vim-runtime。
(正在读取数据库 ... 系统当前共安装有 155719 个文件和目录。)
正准备解包 .../vim-runtime_2%3a8.0.1453-1ubuntu1.8_all.deb  ...
正在添加 vim-runtime 导致 /usr/share/vim/vim80/doc/help.txt 转移到 /usr/share/vim/vim80/doc/help.txt.vim-tiny
正在添加 vim-runtime 导致 /usr/share/vim/vim80/doc/tags 转移到 /usr/share/vim/vim80/doc/tags.vim-tiny
正在解包 vim-runtime (2:8.0.1453-1ubuntu1.8) ...
正在选中未选择的软件包 vim。
正准备解包 .../vim_2%3a8.0.1453-1ubuntu1.8_amd64.deb  ...
正在解包 vim (2:8.0.1453-1ubuntu1.8) ...
正在设置 vim-runtime (2:8.0.1453-1ubuntu1.8) ...
正在设置 vim (2:8.0.1453-1ubuntu1.8) ...
update-alternatives: 使用 /usr/bin/vim.basic 来在自动模式中提供 /usr/bin/vim (vim)
update-alternatives: 使用 /usr/bin/vim.basic 来在自动模式中提供 /usr/bin/vimdiff (vimdiff)
update-alternatives: 使用 /usr/bin/vim.basic 来在自动模式中提供 /usr/bin/rvim (rvim)
update-alternatives: 使用 /usr/bin/vim.basic 来在自动模式中提供 /usr/bin/rview (rview)
update-alternatives: 使用 /usr/bin/vim.basic 来在自动模式中提供 /usr/bin/vi (vi)
update-alternatives: 使用 /usr/bin/vim.basic 来在自动模式中提供 /usr/bin/view (view)
update-alternatives: 使用 /usr/bin/vim.basic 来在自动模式中提供 /usr/bin/ex (ex)
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
linux@ubuntu:~$ 

```

### 4.4 vim的操作

#### 1. vi的三种工作模式

  视图模式   : 打开一个文件后的默认模式 , 在这个模式下不可以输入内容 

  编辑模式   : 编辑文本 

  命令模式   : 在最下方输入操作命令 

#### 2. 三种模式的切换 

  视图模式 -> 编辑模式  , 在键盘上输入i 

  编辑模式 -> 视图模式  , 在键盘上输入esc  

  视图模式 -> 命令模式  , 在键盘上输入 : (或则是空格) 

  命令模式 -> 视图模式  , 在键盘上输入esc 

#### **3. 保存退出文件** 

   视图模式  -> 命令模式  

​		          -> :wq    # 保存退出 

​				 -> : w      #只保存

​                  ->  :q     #  不保存退出 

​                  ->  :q!    # 强制退出 

​                  ->  :x     # 保存退出, 等价于wq 

​                  ->  :X    # 加密文件, 对文件设置访问密码 , 解密文件时, 把密码设置为空即可 

​    **问题补充**:  如果在vim 是按下了ctrl + s , 会发生vim锁屏 

​                   :  ctrl + q 退出vim 的锁屏 

#### **4. 视图模式下的快捷键**  

- 移动光标 :   h 向左移动光标 

​             	 	      l  向右移动光标 

​      			          j  向下移动光标 

​     			           k  向上移动光标 

- 进入编辑模式 :

​     	 i  : 进入编辑模式 

​      	o  : 新起一行,进入编辑模式, 光标移动到开始 

​     	 a : 光标移动到下一个位置, 进入编辑模式 

​     	 s : 删除一个字符, 并进入编辑模式 



- 删除字符:         

​         x  : 删除光标所在的字符, 不进入编辑模式 

- 复制内容 :

​		yy   :  复制一行  

​		 nyy  :  复制n行 

​		 5yy  :  复制5行 

​		 shift + v : 选中一行  

​		 y         : 复制选中的内容 

- 剪切内容 :

​        d  :剪切选中的内容

​       dd :剪切一行 

​       ndd :剪切n行  

​       3dd :剪切3行 

- 粘贴内容: 

​       p :粘贴粘贴板的内容 

- 操作的撤销与反撤销 :

​       u  : 撤销 

​       ctrl + r :反撤销 

- 多行选中操作 :

​       shift + v  :选中一行  , 配合 : k 和 j 选中多行 或则是上下键 

​       再使用 y 复制 , 或则d 剪切 ,  最后在使用 p 去粘贴  

- 文本自动对齐 

​       选中所有 : ggVGG

​       使用  =  来对齐文本 

- 光标移动:

​       gg :光标移动到文件开始 

​       GG :光标移动到文件末尾 

- 搜索关键字  :   /

​        /keyword   

​        使用n(next) 来进行多个的查看 

### 4.5 vim的使用

- 使用vim编辑一个文件, 保存一个文件, gcc 编译一个文件, 再运行一个文件 

```shell
linux@ubuntu:~/work$ vi 01-hello.c    # 打开这个文件, 不存在就创建, 存在就打开 
linux@ubuntu:~/work$ gcc 01-hello.c   # gcc 编译这个文件  
linux@ubuntu:~/work$ ./a.out          # 执行这个可执行程序 
hello
linux@ubuntu:~/work$ 
```

### 4.6 vim的增强插件 

在vim的基础上, 加上自己的配置,形成的增强插件 

```shell
-> 把插件jeffy-vim-sl-v5.0.tar.xz复制到Ubuntu 中的 /home/linux/work 目录内
-> 右击 jeffy-vim-sl-v5.0.tar.xz 
-> 选择 "提取到此处" 
-> 开启终端
-> 进入到 cd work/ 
-> cd jeffy-vim-sl-v5.0/
-> 运行 ./install.sh
-> 选择时  输入 y 即可 
```

![image-20220819101105558](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220819101105558.png)

![image-20220819101211449](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220819101211449.png)

![image-20220819101249083](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220819101249083.png)

![image-20220819101550164](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220819101550164.png)

- 安装过程:

```shell
linux@ubuntu:~/work$ ls
01-hello.c  02-str.c  a.out  jeffy-vim-sl-v5.0  jeffy-vim-sl-v5.0.tar.xz
linux@ubuntu:~/work$ cd jeffy-vim-sl-v5.0/
linux@ubuntu:~/work/jeffy-vim-sl-v5.0$ ls
exefiles  images  install.sh  vimfiles  vimrc
linux@ubuntu:~/work/jeffy-vim-sl-v5.0$ 
linux@ubuntu:~/work/jeffy-vim-sl-v5.0$ ls
exefiles  images  install.sh  vimfiles  vimrc
linux@ubuntu:~/work/jeffy-vim-sl-v5.0$ ./install.sh 
[sudo] linux 的密码： 
命中:1 http://mirrors.yun-idc.com/ubuntu bionic InRelease
获取:2 http://mirrors.yun-idc.com/ubuntu bionic-backports InRelease [74.6 kB]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic-security InRelease [88.7 kB]
获取:4 http://mirrors.yun-idc.com/ubuntu bionic-updates InRelease [88.7 kB]
获取:5 http://mirrors.yun-idc.com/ubuntu bionic-backports/universe amd64 DEP-11 Metadata [9,260 B]
获取:6 http://mirrors.yun-idc.com/ubuntu bionic-security/main i386 Packages [1,225 kB]
获取:7 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 Packages [2,365 kB]
获取:8 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 DEP-11 Metadata [55.2 kB]
获取:9 http://mirrors.yun-idc.com/ubuntu bionic-security/universe amd64 Packages [1,222 kB]
获取:10 http://mirrors.yun-idc.com/ubuntu bionic-security/universe i386 Packages [1,032 kB]
获取:11 http://mirrors.yun-idc.com/ubuntu bionic-security/universe amd64 DEP-11 Metadata [61.0 kB]
获取:12 http://mirrors.yun-idc.com/ubuntu bionic-security/multiverse amd64 DEP-11 Metadata [2,464 B]
获取:13 http://mirrors.yun-idc.com/ubuntu bionic-updates/multiverse amd64 DEP-11 Metadata [2,468 B]
获取:14 http://mirrors.yun-idc.com/ubuntu bionic-updates/universe i386 Packages [1,622 kB]
获取:15 http://mirrors.yun-idc.com/ubuntu bionic-updates/universe amd64 Packages [1,836 kB]
获取:16 http://mirrors.yun-idc.com/ubuntu bionic-updates/universe amd64 DEP-11 Metadata [302 kB]
获取:17 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 Packages [2,706 kB]
获取:18 http://mirrors.yun-idc.com/ubuntu bionic-updates/main i386 Packages [1,524 kB]
获取:19 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 DEP-11 Metadata [298 kB]
已下载 14.5 MB，耗时 3秒 (4,291 kB/s)                              
正在读取软件包列表... 完成
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
注意，选中 'exuberant-ctags' 而非 'ctags'
建议安装：
  cscope-el
下列【新】软件包将被安装：
  cscope exuberant-ctags
升级了 0 个软件包，新安装了 2 个软件包，要卸载 0 个软件包，有 1 个软件包未被升级。
需要下载 335 kB 的归档。
解压缩后会消耗 1,588 kB 的额外空间。
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 cscope amd64 15.8b-3 [209 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 exuberant-ctags amd64 1:5.9~svn20110310-11 [126 kB]
已下载 335 kB，耗时 0秒 (1,803 kB/s) 
正在选中未选择的软件包 cscope。
(正在读取数据库 ... 系统当前共安装有 157486 个文件和目录。)
正准备解包 .../cscope_15.8b-3_amd64.deb  ...
正在解包 cscope (15.8b-3) ...
正在选中未选择的软件包 exuberant-ctags。
正准备解包 .../exuberant-ctags_1%3a5.9~svn20110310-11_amd64.deb  ...
正在解包 exuberant-ctags (1:5.9~svn20110310-11) ...
正在设置 exuberant-ctags (1:5.9~svn20110310-11) ...
update-alternatives: 使用 /usr/bin/ctags-exuberant 来在自动模式中提供 /usr/bin/ctags (ctags)
update-alternatives: 使用 /usr/bin/ctags-exuberant 来在自动模式中提供 /usr/bin/etags (etags)
正在设置 cscope (15.8b-3) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
将会同时安装下列软件：
  fonts-lato javascript-common libjs-jquery liblua5.2-0 libruby2.5 libtcl8.6 rake ruby ruby-did-you-mean ruby-minitest ruby-net-telnet
  ruby-power-assert ruby-test-unit ruby2.5 rubygems-integration vim-gui-common
建议安装：
  apache2 | lighttpd | httpd tcl8.6 ri ruby-dev bundler fonts-dejavu gnome-icon-theme vim-doc
下列【新】软件包将被安装：
  fonts-lato javascript-common libjs-jquery liblua5.2-0 libruby2.5 libtcl8.6 rake ruby ruby-did-you-mean ruby-minitest ruby-net-telnet
  ruby-power-assert ruby-test-unit ruby2.5 rubygems-integration vim-gtk vim-gui-common
升级了 0 个软件包，新安装了 17 个软件包，要卸载 0 个软件包，有 1 个软件包未被升级。
需要下载 8,573 kB 的归档。
解压缩后会消耗 35.9 MB 的额外空间。
您希望继续执行吗？ [Y/n] y   # 选择y 
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 fonts-lato all 2.0-2 [2,698 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 javascript-common all 11 [6,066 B]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 libjs-jquery all 3.2.1-1 [152 kB]
获取:4 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 liblua5.2-0 amd64 5.2.4-1.1build1 [108 kB]
获取:5 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 rubygems-integration all 1.11 [4,994 B]
获取:6 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 ruby2.5 amd64 2.5.1-1ubuntu1.12 [48.6 kB]
获取:7 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 ruby amd64 1:2.5.1 [5,712 B]
获取:8 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 rake all 12.3.1-1ubuntu0.1 [44.9 kB]
获取:9 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 ruby-did-you-mean all 1.2.0-2 [9,700 B]
获取:10 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 ruby-minitest all 5.10.3-1 [38.6 kB]
获取:11 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 ruby-net-telnet all 0.1.1-2 [12.6 kB]
获取:12 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 ruby-power-assert all 0.3.0-1 [7,952 B]
获取:13 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 ruby-test-unit all 3.2.5-1 [61.1 kB]
获取:14 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libruby2.5 amd64 2.5.1-1ubuntu1.12 [3,073 kB]
获取:15 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 libtcl8.6 amd64 8.6.8+dfsg-3 [881 kB]
获取:16 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 vim-gui-common all 2:8.0.1453-1ubuntu1.8 [71.8 kB]
获取:17 http://mirrors.yun-idc.com/ubuntu bionic-security/universe amd64 vim-gtk amd64 2:8.0.1453-1ubuntu1.8 [1,348 kB]
已下载 8,573 kB，耗时 1秒 (7,522 kB/s)
正在选中未选择的软件包 fonts-lato。
(正在读取数据库 ... 系统当前共安装有 157523 个文件和目录。)
正准备解包 .../00-fonts-lato_2.0-2_all.deb  ...
正在解包 fonts-lato (2.0-2) ...
正在选中未选择的软件包 javascript-common。
正准备解包 .../01-javascript-common_11_all.deb  ...
正在解包 javascript-common (11) ...
正在选中未选择的软件包 libjs-jquery。
正准备解包 .../02-libjs-jquery_3.2.1-1_all.deb  ...
正在解包 libjs-jquery (3.2.1-1) ...
正在选中未选择的软件包 liblua5.2-0:amd64。
正准备解包 .../03-liblua5.2-0_5.2.4-1.1build1_amd64.deb  ...
正在解包 liblua5.2-0:amd64 (5.2.4-1.1build1) ...
正在选中未选择的软件包 rubygems-integration。
正准备解包 .../04-rubygems-integration_1.11_all.deb  ...
正在解包 rubygems-integration (1.11) ...
正在选中未选择的软件包 ruby2.5。
正准备解包 .../05-ruby2.5_2.5.1-1ubuntu1.12_amd64.deb  ...
正在解包 ruby2.5 (2.5.1-1ubuntu1.12) ...
正在选中未选择的软件包 ruby。
正准备解包 .../06-ruby_1%3a2.5.1_amd64.deb  ...
正在解包 ruby (1:2.5.1) ...
正在选中未选择的软件包 rake。
正准备解包 .../07-rake_12.3.1-1ubuntu0.1_all.deb  ...
正在解包 rake (12.3.1-1ubuntu0.1) ...
正在选中未选择的软件包 ruby-did-you-mean。
正准备解包 .../08-ruby-did-you-mean_1.2.0-2_all.deb  ...
正在解包 ruby-did-you-mean (1.2.0-2) ...
正在选中未选择的软件包 ruby-minitest。
正准备解包 .../09-ruby-minitest_5.10.3-1_all.deb  ...
正在解包 ruby-minitest (5.10.3-1) ...
正在选中未选择的软件包 ruby-net-telnet。
正准备解包 .../10-ruby-net-telnet_0.1.1-2_all.deb  ...
正在解包 ruby-net-telnet (0.1.1-2) ...
正在选中未选择的软件包 ruby-power-assert。
正准备解包 .../11-ruby-power-assert_0.3.0-1_all.deb  ...
正在解包 ruby-power-assert (0.3.0-1) ...
正在选中未选择的软件包 ruby-test-unit。
正准备解包 .../12-ruby-test-unit_3.2.5-1_all.deb  ...
正在解包 ruby-test-unit (3.2.5-1) ...
正在选中未选择的软件包 libruby2.5:amd64。
正准备解包 .../13-libruby2.5_2.5.1-1ubuntu1.12_amd64.deb  ...
正在解包 libruby2.5:amd64 (2.5.1-1ubuntu1.12) ...
正在选中未选择的软件包 libtcl8.6:amd64。
正准备解包 .../14-libtcl8.6_8.6.8+dfsg-3_amd64.deb  ...
正在解包 libtcl8.6:amd64 (8.6.8+dfsg-3) ...
正在选中未选择的软件包 vim-gui-common。
正准备解包 .../15-vim-gui-common_2%3a8.0.1453-1ubuntu1.8_all.deb  ...
正在解包 vim-gui-common (2:8.0.1453-1ubuntu1.8) ...
正在选中未选择的软件包 vim-gtk。
正准备解包 .../16-vim-gtk_2%3a8.0.1453-1ubuntu1.8_amd64.deb  ...
正在解包 vim-gtk (2:8.0.1453-1ubuntu1.8) ...
正在设置 vim-gui-common (2:8.0.1453-1ubuntu1.8) ...
正在设置 libjs-jquery (3.2.1-1) ...
正在设置 fonts-lato (2.0-2) ...
正在设置 ruby-did-you-mean (1.2.0-2) ...
正在设置 ruby-net-telnet (0.1.1-2) ...
正在设置 rubygems-integration (1.11) ...
正在设置 liblua5.2-0:amd64 (5.2.4-1.1build1) ...
正在设置 libtcl8.6:amd64 (8.6.8+dfsg-3) ...
正在设置 javascript-common (11) ...
正在设置 ruby-minitest (5.10.3-1) ...
正在设置 ruby-power-assert (0.3.0-1) ...
正在设置 ruby2.5 (2.5.1-1ubuntu1.12) ...
正在设置 ruby (1:2.5.1) ...
正在设置 ruby-test-unit (3.2.5-1) ...
正在设置 rake (12.3.1-1ubuntu0.1) ...
正在设置 libruby2.5:amd64 (2.5.1-1ubuntu1.12) ...
正在设置 vim-gtk (2:8.0.1453-1ubuntu1.8) ...
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/vim (vim)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/vimdiff (vimdiff)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/rvim (rvim)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/rview (rview)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/vi (vi)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/view (view)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/ex (ex)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/editor (editor)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/gvim (gvim)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/gview (gview)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/rgview (rgview)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/rgvim (rgvim)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/evim (evim)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/eview (eview)
update-alternatives: 使用 /usr/bin/vim.gtk 来在自动模式中提供 /usr/bin/gvimdiff (gvimdiff)
正在处理用于 mime-support (3.60ubuntu1) 的触发器 ...
正在处理用于 desktop-file-utils (0.23-1ubuntu3.18.04.2) 的触发器 ...
正在处理用于 libc-bin (2.27-3ubuntu1.6) 的触发器 ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在处理用于 gnome-menus (3.13.3-11ubuntu1.1) 的触发器 ...
正在处理用于 fontconfig (2.12.6-0ubuntu2) 的触发器 ...
This install will NOT backup your old vim files.
Did you backup your old vim files. [y,n]? y     # 输入选择 y 
Install new vim files...done!
install finished, enjoy yourself!
linux@ubuntu:~/work/jeffy-vim-sl-v5.0$ 

```

### 4.7 vim的增强插件的功能

```shell
# 以下是做的一些常用快捷键映射

" 显示找到的结果，用列表形式
nmap <c-]> g<c-]>        # ctrl + ]  来查找函数定义 , 结构体定义 

" 映射快捷键，ctrl+c 增加 "+y，进行复制  
map <c-c> "+y            # 可以把vim 中的内容复制到window内的粘贴板

" 映射快捷键，ctrl+v 增加 "+p，进行复制
map <c-v> "+p            # 可以把window 中的内容复制到vim内

" 映射快捷键，ctrl+d 增加 "+d，进行剪切
map <c-x> "+d            # 可以把vim中的内容剪切到window内  

" 映射快捷键，ctrl+z 增加 u，进行复制
map <c-z> u              # 使用window的快捷键ctrl + z  , 增加一个实现的方式, 

" 映射快捷键，ctrl+y 增加 ctrl+r，进行撤销
map <c-y> <c-r>

" 映射快捷键，ctrl + a 增加 ggvGG   
map <c-a> ggvGG       # ctrl +a 进行全选 

" map : -> <space>
map <Space> :        # 把: 使用空格进行了一次映射 , 既可以使用 : , 空格也可以进入命令模式
#空格 等价于 冒号 
```

- 练习, 使用vim 实现7个数组元素的输入并进行冒泡排序后 , 从大到小的排序并输出。

```c
#include <stdio.h>

int maopao(int *pa,int n)
{
    for(int i=0;i<n-1;i++)
    {
        for(int j=0,t=0;j<n-1-i;j++)
        {
            if(pa[j] < pa[j+1])
            {
                t = pa[j]; 
                pa[j] = pa[j+1]; 
                pa[j+1] = t; 
            }
        }
    }
    
    return 0;
}

int display_array(int *pa,int n)
{
    printf("array:");
    for(int i=0;i<7;i++)
    {
        printf("%d ",pa[i]);
    }
    printf("\n");
}
int main(int argc, const char *argv[])
{
    int a[7]={0};
    for(int i=0;i<7;i++)
    {
        printf("please input %d number >:",i+1);
        scanf("%d",&a[i]); 
    }
    display_array(a,7);
    maopao(a,7);
    display_array(a,7);
    return 0;
}
```

- 运行结果

```c
linux@ubuntu:~/work$ vi maopao.c
linux@ubuntu:~/work$ gcc maopao.c 
linux@ubuntu:~/work$ ./a.out 
please input 1 number >:23
please input 2 number >:55
please input 3 number >:9
please input 4 number >:12
please input 5 number >:642
please input 6 number >:87
please input 7 number >:29
array:23 55 9 12 642 87 29 
array:642 87 55 29 23 12 9 
linux@ubuntu:~/work$ 

```



## 第05章 环境变量

### **5.1 环境变量的概念**

1、环境变量的含义

程序（操作系统命令和应用程序）的执行都需要运行环境，这个环境是由多个环境变量组成的。

2、环境变量的分类

1）按生效的范围分类。

系统环境变量：公共的，对全部的用户都生效。

用户环境变量：用户私有的、自定义的个性化设置，只对该用户生效。

2）按生存周期分类。

永久环境变量：在环境变量脚本文件中配置，用户每次登录时会自动执行这些脚本，相当于永久生效。

临时环境变量：使用时在Shell中临时定义，退出Shell后失效。

3、Linux环境变量

Linux环境变量也称之为Shell环境量变，以下划线和字母打头，由下划线、字母（区分大小写）和数字组成，习惯上使用大写字母，例如PATH、HOSTNAME、LANG等。

### **5.2 常用的环境变量**

1、查看环境变量

1）env命令或者printenv 命令 

在Shell下，用env命令查看当前用户全部的环境变量。

```shell
linux@ubuntu:~/work$ env     #  显示系统所有的环境变量 
CLUTTER_IM_MODULE=xim 
LC_MEASUREMENT=zh_CN.UTF-8   # 中文的， utf-8 编码  , 系统编码 
LESSCLOSE=/usr/bin/lesspipe %s %s
LC_PAPER=zh_CN.UTF-8
LC_MONETARY=zh_CN.UTF-8
XDG_MENU_PREFIX=gnome-   # ide 桌面的前缀 
LANG=zh_CN.UTF-8        # 表示系统的语言   
DISPLAY=:0     #  显示器默认为0 
GNOME_SHELL_SESSION_MODE=ubuntu  # 计算机名称 
COLORTERM=truecolor   # 终端颜色 真彩 
USERNAME=linux      # 用户名 linux 
XDG_VTNR=1
SSH_AUTH_SOCK=/run/user/1000/keyring/ssh
LC_NAME=zh_CN.UTF-8    # 语言名称 
XDG_SESSION_ID=1
USER=linux    # 用户linux 
DESKTOP_SESSION=ubuntu
QT4_IM_MODULE=xim
TEXTDOMAINDIR=/usr/share/locale/
GNOME_TERMINAL_SCREEN=/org/gnome/Terminal/screen/75a981ec_d9ee_47f0_95f7_8a04a33b1ad2
PWD=/home/linux/work   # 当前所在的路径
HOME=/home/linux       # 家目录的绝对路径 
TEXTDOMAIN=im-config
SSH_AGENT_PID=1437
QT_ACCESSIBILITY=1
XDG_SESSION_TYPE=x11    # 窗口类型 x11 
XDG_DATA_DIRS=/usr/share/ubuntu:/usr/local/share/:/usr/share/:/var/lib/snapd/desktop
XDG_SESSION_DESKTOP=ubuntu
LC_ADDRESS=zh_CN.UTF-8
GJS_DEBUG_OUTPUT=stderr
LC_NUMERIC=zh_CN.UTF-8
GTK_MODULES=gail:atk-bridge
PAPERSIZE=a4
WINDOWPATH=1
TERM=xterm-256color
SHELL=/bin/bash
VTE_VERSION=5202
QT_IM_MODULE=xim
XMODIFIERS=@im=ibus
IM_CONFIG_PHASE=2
XDG_CURRENT_DESKTOP=ubuntu:GNOME
GPG_AGENT_INFO=/run/user/1000/gnupg/S.gpg-agent:0:1
GNOME_TERMINAL_SERVICE=:1.65
XDG_SEAT=seat0
SHLVL=1
LANGUAGE=zh_CN:en_US:en
LC_TELEPHONE=zh_CN.UTF-8
GDMSESSION=ubuntu
GNOME_DESKTOP_SESSION_ID=this-is-deprecated
LOGNAME=linux
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
XDG_RUNTIME_DIR=/run/user/1000
XAUTHORITY=/run/user/1000/gdm/Xauthority
XDG_CONFIG_DIRS=/etc/xdg/xdg-ubuntu:/etc/xdg
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
LC_IDENTIFICATION=zh_CN.UTF-8
GJS_DEBUG_TOPICS=JS ERROR;JS LOG
SESSION_MANAGER=local/ubuntu:@/tmp/.ICE-unix/1259,unix/ubuntu:/tmp/.ICE-unix/1259
LESSOPEN=| /usr/bin/lesspipe %s
GTK_IM_MODULE=ibus
LC_TIME=zh_CN.UTF-8
_=/usr/bin/env
OLDPWD=/home/linux    
```

上图只截取了部分环境变量，并非全部。

用env命令的时候，满屏显示了很多环境变量，不方便查看，可以用grep筛选。

env|grep 环境变量名

例如查看环境变量名中包含PATH的环境变量。

env|grep PATH

```shell
linux@ubuntu:~/work$ env | grep PATH
WINDOWPATH=1
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
linux@ubuntu:~/work$ 
    
```

2）echo命令

echo $环境变量

```shell
linux@ubuntu:~/work$ echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
linux@ubuntu:~/work$ echo $LANG
zh_CN.UTF-8
linux@ubuntu:~/work$ 
```

注意，符号$不能缺少，这是语法规定。

2、常用的环境变量

1）PATH

可执行程序的搜索目录，可执行程序包括Linux系统命令和用户的应用程序，PATH变量的具体用法本文后面的章节中有详细的介绍.      

2）LANG

Linux系统的语言、地区、字符集，LANG变量的具体用法本文后面的章节中有详细的介绍。           

3）HOSTNAME

服务器的主机名。

4）SHELL

用户当前使用的Shell解析器。

5）HISTSIZE

保存历史命令的数目。

6）USER

当前登录用户的用户名。

7）HOME

当前登录用户的主目录。

8）PWD

当前工作目录。

9）LD_LIBRARY_PATH

C/C++语言动态链接库文件搜索的目录，它不是Linux缺省的环境变量，但对C/C++程序员来说非常重要，具体用法本文后面的章节中有详细的介绍。

10）CLASSPATH

JAVA语言库文件搜索的目录，它也不是Linux缺省的环境变量，但对JAVA程序员来说非常重要，具体用法本文后面的章节中有详细的介绍。

```shell
linux@ubuntu:~/work$ env | grep PATH
WINDOWPATH=1
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
linux@ubuntu:~/work$ echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
linux@ubuntu:~/work$ echo $LANG
zh_CN.UTF-8
linux@ubuntu:~/work$ echo $HOSTNAME
ubuntu
linux@ubuntu:~/work$ echo $SHELL
/bin/bash
linux@ubuntu:~/work$ echo $HISTSIZE
1000
linux@ubuntu:~/work$ echo $USER
linux
linux@ubuntu:~/work$ echo $HOME
/home/linux
linux@ubuntu:~/work$ echo $PWD
/home/linux/work
linux@ubuntu:~/work$ echo $LD_LIBRARY_PATH

linux@ubuntu:~/work$ 
```



### **5.3 设置环境量**

```shell
变量名='值'   # 等号之间不能有空格 
export 变量名='值'     # export 导出的意思 ,  告诉shell , 有一个变量被定义了, 
```

- 如果环境变量的值没有空格等特殊符号，可以不用单引号包含。


- 示例：

```shell
export ORACLE_HOME=/oracle/home                          # 值没有特殊的, 不用单引号 
export ORACLE_BASE=/oracle/base
export ORACLE_SID=snorcl11g
export NLS_LANG='Simplified Chinese_China.ZHS16GBK'      # 如果值有空格等特殊字符需要使用 单引号包含
export PATH=$PATH:$HOME/bin:$ORACLE_HOME/bin:            # 把更多的bin目录加入到PATH中 
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME/lib:
```

采用export设置的环境变量，在退出Shell后就会失效，下次登录时需要重新设置。如果希望环境变量永久生效，需要在登录脚本文件中配置。

#### 1. 系统环境变量

系统环境变量对全部的用户生效，设置系统环境变量有三种方法。

1）在/etc/profile文件中设置。

用户登录时执行/etc/profile文件中设置系统的环境变量。但是，Linux不建议在/etc/profile文件中设置系统环境变量。

2）在/etc/profile.d目录中增加环境变量脚本文件，这是Linux推荐的方法。

/etc/profile在每次启动时会执行 /etc/profile.d下全部的脚本文件。/etc/profile.d比/etc/profile好维护，不想要什么变量直接删除 /etc/profile.d下对应的 shell 脚本即可。

/etc/profile.d目录下有很多脚本文件，例如

```shell
linux@ubuntu:~/work$ ls /etc/profile.d/
01-locale-fix.sh  apps-bin-path.sh  bash_completion.sh  cedilla-portuguese.sh  input-method-config.sh  vte-2.91.sh  xdg_dirs_desktop_session.sh
linux@ubuntu:~/work$ 
```

在以上示例中，/etc/profile.d目录中的bash_completion.sh是bash环境变量配置文件，内容如下：

```shell
# Check for interactive bash and that we haven't already been sourced.
if [ -n "${BASH_VERSION-}" -a -n "${PS1-}" -a -z "${BASH_COMPLETION_VERSINFO-}" ]; then

    # Check for recent enough version of bash.
    if [ ${BASH_VERSINFO[0]} -gt 4 ] || \
       [ ${BASH_VERSINFO[0]} -eq 4 -a ${BASH_VERSINFO[1]} -ge 1 ]; then
        [ -r "${XDG_CONFIG_HOME:-$HOME/.config}/bash_completion" ] && \
            . "${XDG_CONFIG_HOME:-$HOME/.config}/bash_completion"
        if shopt -q progcomp && [ -r /usr/share/bash-completion/bash_completion ]; then
            # Source completion code.
            . /usr/share/bash-completion/bash_completion
        fi
    fi

fi
```

  3）在/etc/bash.bashrc文件中设置环境变量。

该文件配置的环境变量将会影响全部用户使用的bash shell。但是，Linux也不建议在/etc/bashrc文件中设置系统环境变量。

```shell
linux@ubuntu:~/work$ vi /etc/bash.bashrc 
linux@ubuntu:~/work$ cat /etc/bash.bashrc 
# System-wide .bashrc file for interactive bash(1) shells.

# To enable the settings / commands in this file for login shells as well,
# this file has to be sourced in /etc/profile.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, overwrite the one in /etc/profile)
# but only if not SUDOing and have SUDO_PS1 set; then assume smart user.
if ! [ -n "${SUDO_USER}" -a -n "${SUDO_PS1}" ]; then
  PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi

# Commented out, don't overwrite xterm -T "title" -n "icontitle" by default.
# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
#    ;;
#*)
#    ;;
#esac

```



#### 2. 用户环境变量

用户环境变量只对当前用户生效，设置用户环境变量也有多种方法。

在用户的主目录，有几个特别的文件，用ls是看不见的，用ls .bash_*可以看见。

```shell
linux@ubuntu:~$ ls -la 
总用量 132
drwxr-xr-x 19 linux linux  4096 8月  19 14:57 .
drwxr-xr-x  3 root  root   4096 8月  18 15:26 ..
-rw-------  1 linux linux  1971 8月  18 15:21 .bash_history           # 命令的历史记录 
-rw-r--r--  1 linux linux   220 8月  16 10:57 .bash_logout            # 当每次退出bash时执行该文件。
-rw-r--r--  1 linux linux  3771 8月  16 10:57 .bashrc       #当用户登录时以及每次打开新的Shell时
drwx------ 17 linux linux  4096 8月  17 09:59 .cache
drwx------ 12 linux linux  4096 8月  17 09:59 .config
drwx------  3 linux linux  4096 8月  16 14:37 .gnupg
-rw-------  1 linux linux  6988 8月  19 08:51 .ICEauthority
drwx------  3 linux linux  4096 8月  16 11:03 .local
drwx------  5 linux linux  4096 8月  16 15:11 .mozilla
-rw-r--r--  1 linux linux   363 8月  17 10:09 .pam_environment
-rw-r--r--  1 linux linux   807 8月  16 10:57 .profile #当用户登录时执行，每个用户都可以使用该文件来配置自己的环境变量
drwx------  2 linux linux  4096 8月  16 14:37 .ssh
-rw-r--r--  1 linux linux     0 8月  17 09:03 .sudo_as_admin_successful
drwxrwxr-x  4 linux linux  4096 8月  19 10:15 .vim
-rw-------  1 linux linux 10662 8月  19 14:57 .viminfo
-rw-rw-r--  1 linux linux   185 8月  19 14:56 .vim_mru_files
-rw-rw-r--  1 linux linux  7329 8月  19 10:15 .vimrc
-rw-rw-r--  1 linux linux   131 8月  17 10:19 .xinputrc
linux@ubuntu:~$
```

1）.profile（推荐首选）

当用户登录时执行，每个用户都可以使用该文件来配置专属于自己的环境变量。

2）.bashrc

当用户登录时以及每次打开新的Shell时该文件都将被读取，不推荐在里面配置用户专用的环境变量，因为每开一个Shell，该文件都会被读取一次，效率肯定受影响。

3）.bash_logout

当每次退出系统（退出bash shell）时执行该文件。

4）.bash_history

保存了当前用户使用过的历史命令。

3、环境变量脚本文件的执行顺序

环境变量脚本文件的执行顺序如下：

```shell
/etc/profile -> /etc/profile.d -> /etc/bashrc ->用户的.profile -> 用户的.bashrc
```

同名的环境变量，如果在多个脚本中有配置，以最后执行的脚本中的配置为准。

### **5.4 重要环境变量的详解**

#### 1. PATH环境变量

可执行程序的搜索目录，可执行程序包括Linux系统命令和用户的应用程序。如果可执行程序的目录不在PATH指定的目录中，执行时需要指定目录。

1）PATH环境变量存放的是目录列表，目录之间用冒号:分隔，最后的圆点.表示当前目录。

export PATH=目录1:目录2:目录3:......目录n:.

2）PATH缺省包含了Linux系统命令所在的目录（/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin），如果不包含这些目录，Linux的常用命令也无法执行（要输入绝对路径才能执行）。

示例：

```shell
linux@ubuntu:~$ ls 
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
linux@ubuntu:~$ export PATH=
linux@ubuntu:~$ ls
bash: ls: 没有那个文件或目录
linux@ubuntu:~$ /bin/ls
Desktop  Documents  Downloads  Music  Pictures	Public	snap  Templates  Videos  work
linux@ubuntu:~$ 
```

3）在用户的.profile文件中，会对PATH进行扩充，如下：

export PATH=$PATH:$HOME/bin

4）如果PATH变量中没有包含圆点.，执行当前目录下的程序需要加./或使用绝对路径。      

#### 2. LD_LIBRARY_PATH环境变量

C/C++语言动态链接库文件搜索的目录，它不是Linux缺省的环境变量，但对C/C++程序员来说非常重要。

LD_LIBRARY_PATH环境变量存放的也是目录列表，目录之间用冒号:分隔，最后的圆点.表示当前目录，与PATH的格式相同。

export LD_LIBRARY_PATH=目录1:目录2:目录3:......目录n:.

### **5.5 环境变量的生效**

1）在Shell下，用export设置的环境变量对当前Shell立即生效，Shell退出后失效。

2）在脚本文件中设置的环境变量不会立即生效，退出Shell后重新登录时才生效，或者用source命令让它立即生效，

例如：

```shell
source /etc/profile
```



## 第06章 软件包管理

这一章主要是教大家如何安装软件与卸载软件。

### **6.1 rpm与deb的软件包理解**

最初，基于Linux系统的开发者在完成应用程序开发后，将很多二进制文件发给用户，用户使用之前需要将很多程序逐个安装。

因此，**Debian** **Linux**首先提出软件包的管理机制——**Deb**软件包，将应用程序的二进制文件、配置文档、man/info帮助页面等文件合并打包在一个文件中，用户使用软件包管理器直接操作软件包，完成获取、安装、卸载、查询等操作。

随即，**Redhat** **Linux**基于这个理念推出了自己的软件包管理机制——**Rpm**软件包。当然，Redhat采用了自己的打包格式生成Rpm包文件，由Rpm包管理器负责安装、维护、查询，甚至是软件包版本管理。

随着Linux操作系统规模的不断扩大，系统中软件包间复杂的依赖关系，导致Linux用户麻烦不断。

Debian Linux开发出了APT软件包管理器。

**检查和修复软件包依赖关系**

利用Internet网络帮助用户**主动获取软件包**

APT工具再次促进了Deb软件包更为广泛地使用，成为Debian Linux的一个无法替代的亮点。

### **6.2 dpkg命令行管理工具**

离线软件包安装工具

**dpkg**是最早的Deb包管理工具，它在Debian一提出包管理模式后就诞生了。使用dpkg可以实现软件包的安装、编译、卸载、查询，以及应用程序打包等功能。但是由于当时Linux系统规模和Internet网络条件的限制，没有考虑到操作系统中软件包存在如此复杂的依赖关系，以及帮助用户获取软件包（获取存在依赖关系的软件包）。因而，为了解决软件包依赖性问题和获取问题，就出现了APT工具。

dpkg是Ubuntu Linux中最基本的命令行软件包管理工具，用于安装、编译、卸载和查询Deb软件包

缺陷：

- 第一，不能主动从镜像站点获取软件包；

- 第二，安装软件包时，无法检查软件包的依赖关系。

因此，在对一个软件组件的依赖关系不清楚的情况下，建议使用APT软件包管理器。除非用户对软件包的依赖关系非常清楚，再使用dpkg

```shell
dpkg相关命令
dpkg -i <package>    # install 安装一个在本地文件系统上存在的Debian软件包
dpkg -r <package>    # remove  移除一个已经安装的软件包
dpkg -P <package>    # purge   移除已安装软件包及配置文件
dpkg -L <package>    # 列出安装的软件包清单
dpkg -s <package>    # status  显出软件包的安装状态
dpkg-reconfigure <package>    #重新配置一个已经安装的软件包
```

- 例如

```shell
#检查gcc 是否安装 
linux@ubuntu:~$ dpkg -s gcc   # 检查gcc 软件是否安装 
Package: gcc
Status: install ok installed
Priority: optional
Section: devel
Installed-Size: 50
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Architecture: amd64
Source: gcc-defaults (1.176ubuntu2.3)
Version: 4:7.4.0-1ubuntu2.3
Provides: c-compiler
Depends: cpp (>= 4:7.4.0-1ubuntu2.3), gcc-7 (>= 7.4.0-1~)
Recommends: libc6-dev | libc-dev
Suggests: gcc-multilib, make, manpages-dev, autoconf, automake, libtool, flex, bison, gdb, gcc-doc
Conflicts: gcc-doc (<< 1:2.95.3)
Description: GNU C compiler
 This is the GNU C compiler, a fairly portable optimizing compiler for C.
 .
 This is a dependency package providing the default GNU C compiler.
Original-Maintainer: Debian GCC Maintainers <debian-gcc@lists.debian.org>
linux@ubuntu:~$ dpkg -s g++   # 检查g++ 是否安装 
dpkg-query: 系统没有安装软件包 g++，因而没有相关的信息
使用 dpkg --info (= dpkg-deb --info) 来检测打包好的文件，
还可以通过 dpkg --contents (= dpkg-deb --contents) 来列出它们的内容。
linux@ubuntu:~$ dpkg -L gcc   # 检查gcc 软件包清单 
/.
/usr
/usr/bin
/usr/bin/c89-gcc
/usr/bin/c99-gcc
/usr/lib
/usr/lib/bfd-plugins
/usr/share
/usr/share/doc
/usr/share/doc/cpp
/usr/share/man
/usr/share/man/man1
/usr/share/man/man1/c89-gcc.1.gz
/usr/share/man/man1/c99-gcc.1.gz
/usr/bin/gcc
/usr/bin/gcc-ar
/usr/bin/gcc-nm
/usr/bin/gcc-ranlib
/usr/bin/gcov
/usr/bin/gcov-dump
/usr/bin/gcov-tool
/usr/bin/x86_64-linux-gnu-gcc
/usr/bin/x86_64-linux-gnu-gcc-ar
/usr/bin/x86_64-linux-gnu-gcc-nm
/usr/bin/x86_64-linux-gnu-gcc-ranlib
/usr/bin/x86_64-linux-gnu-gcov
/usr/bin/x86_64-linux-gnu-gcov-dump
/usr/bin/x86_64-linux-gnu-gcov-tool
/usr/lib/bfd-plugins/liblto_plugin.so
/usr/share/doc/cpp/README.Bugs
/usr/share/doc/gcc
/usr/share/man/man1/gcc-ar.1.gz
/usr/share/man/man1/gcc-nm.1.gz
/usr/share/man/man1/gcc-ranlib.1.gz
/usr/share/man/man1/gcc.1.gz
/usr/share/man/man1/gcov-dump.1.gz
/usr/share/man/man1/gcov-tool.1.gz
/usr/share/man/man1/gcov.1.gz
/usr/share/man/man1/x86_64-linux-gnu-gcc-ar.1.gz
/usr/share/man/man1/x86_64-linux-gnu-gcc-nm.1.gz
/usr/share/man/man1/x86_64-linux-gnu-gcc-ranlib.1.gz
/usr/share/man/man1/x86_64-linux-gnu-gcc.1.gz
/usr/share/man/man1/x86_64-linux-gnu-gcov-dump.1.gz
/usr/share/man/man1/x86_64-linux-gnu-gcov-tool.1.gz
/usr/share/man/man1/x86_64-linux-gnu-gcov.1.gz
linux@ubuntu:~$ 

```



### 6.3 Ubuntu搜狗输入法安装指南

1. 输入法的支持情况

   搜狗输入法已支持Ubuntu1604、1804、1910、2004、2010

2. 搜狗输入法的下载:  [搜狗输入法linux-首页 (sogou.com)](https://shurufa.sogou.com/linux)

3. 添加中文语言支持

​	打开 系统设置——区域和语言——管理已安装的语言——在“语言”tab下——点击“添加或删除语言”

![打开 系统设置——区域和语言——管理已安装的语言——在“语言”tab下——点击“添加或删除语言”](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fstep-1.8106d423.jpg&w=828&q=75)

弹出“已安装语言”窗口，勾选中文（简体），点击应用

![弹出“已安装语言”窗口，勾选中文（简体），点击应用](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fstep-2.4afc85d1.jpg&w=1200&q=75)

回到“语言支持”窗口，在键盘输入法系统中，选择“fcitx”

![回到“语言支持”窗口，在键盘输入法系统中，选择“fcitx”](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fstep-3.79400b61.jpg&w=828&q=75)

注:

如果在键盘输入法系统中，没有“fcitx”选项时，建议先打开终端手动安装

```shell
sudo apt install fcitx
```

等安装成功之后再执行上述步骤

点击“应用到整个系统”，关闭窗口，重启电脑

![如果在键盘输入法系统中，没有“fcitx”选项时，建议先打开终端手动安装fcitx：sudo apt-get install fcitx等安装成功之后再执行上述步骤点击“应用到整个系统”，关闭窗口，重启电脑](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fstep-4.c5301a59.jpg&w=828&q=75)

4. 通过命令行安装搜狗输入法

```shell
sudo dpkg -i sogoupinyin_4.0.1.2800_x86_64.deb

#注:如果安装过程中提示缺少相关依赖，则执行如下命令解决：
sudo apt -f install
```

5. 注销或重启计算机即可正常使用搜狗输入法

6. 问题解决

   Ubuntu18.04 正常安装后, 发现输入法不能使用, 解决问题的办法如下: 

```shell
# 安装依赖的库, 可能的原因是ubuntu18.04系统进行了升级原因造成的
sudo apt install libqt5qml5 libqt5quick5 libqt5quickwidgets5 qml-module-qtquick2 libgsettings-qt1
```

7. 搜狗输入法的使用

```shell
ctrl + 空格   #  激活输入法 与关闭输入法 
shift        #  中英文切换  
```

### **6.4 apt命令行管理工具**

**APT**系列工具可能是Deb软件包管理工具中功能最强大的。Ubuntu将所有的开发软件包存放在Internet上的许许多多镜像站点上。用户可以选择其中最适合自己的站点作为软件源。然后，在APT工具的帮助下，就可以完成所有的软件包的管理工作，包括维护系统中的软件包数据库、自动检查软件包依赖关系、安装和升级软件包、从软件源镜像站点主动获取相关软件包等。

常用的APT实用程序有：apt。

#### 1. apt install  安装软件

```shell
sudo apt install <软件名称>
sudo apt -f install   #修复系统的软件安装错误

#例如 
sudo apt install sl   # 小火车 安装软件, 可以使用LS 去执行这个可执行程序 
sudo apt install  snake4 # 贪吃蛇游戏 , 可以使用snake4 启动程序 
```

#### 2. apt remove 卸载软件

```shell
sudo apt remove  <软件名称>
sudo apt autoremove  #  自动卸载系统不再需要的软件包 

# 例如 
sudo apt remove sl   # 卸载火车软件 
sudo apt remove snake4   # 卸载贪吃蛇游戏
```

- 例如:

```shell
linux@ubuntu:~$ sudo apt install sl   # 安装软件 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列【新】软件包将被安装：
  sl
升级了 0 个软件包，新安装了 1 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
需要下载 26.4 kB 的归档。
解压缩后会消耗 98.3 kB 的额外空间。
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 sl amd64 3.03-17build2 [26.4 kB]
已下载 26.4 kB，耗时 0秒 (330 kB/s)
正在选中未选择的软件包 sl。
(正在读取数据库 ... 系统当前共安装有 162585 个文件和目录。)
正准备解包 .../sl_3.03-17build2_amd64.deb  ...
正在解包 sl (3.03-17build2) ...
正在设置 sl (3.03-17build2) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
linux@ubuntu:~$ LS    # 运行程序 

linux@ubuntu:~$ sudo apt remove sl   # 卸载软件 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列软件包将被【卸载】：
  sl
升级了 0 个软件包，新安装了 0 个软件包，要卸载 1 个软件包，有 0 个软件包未被升级。
解压缩后将会空出 98.3 kB 的空间。
您希望继续执行吗？ [Y/n] y
(正在读取数据库 ... 系统当前共安装有 162643 个文件和目录。)
正在卸载 sl (3.03-17build2) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
Llinux@ubuntu:~$ LS    # 运行程序失败 

Command 'LS' not found, but can be installed with:

sudo apt install sl

linux@ubuntu:~$ 
linux@ubuntu:~$ sudo apt remove snake4   # 卸载贪吃蛇游戏
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列软件包是自动安装的并且现在不需要了：
  libshhmsg1 libshhopt1
使用'sudo apt autoremove'来卸载它(它们)。
下列软件包将被【卸载】：
  snake4
升级了 0 个软件包，新安装了 0 个软件包，要卸载 1 个软件包，有 0 个软件包未被升级。
解压缩后将会空出 272 kB 的空间。
您希望继续执行吗？ [Y/n] y
(正在读取数据库 ... 系统当前共安装有 162612 个文件和目录。)
正在卸载 snake4 (1.0.14-1build1) ...
正在处理用于 desktop-file-utils (0.23-1ubuntu3.18.04.2) 的触发器 ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在处理用于 gnome-menus (3.13.3-11ubuntu1.1) 的触发器 ...
正在处理用于 mime-support (3.60ubuntu1) 的触发器 ...
linux@ubuntu:~$ 

```

#### 3. apt policy 查看软件安装状态 

```shell
#policy  策略, 
sudo apt policy <软件名称>

sudo apt policy gcc     # 检查gcc 的安装状态 
sudo apt policy sl      # 检查sl 软件安装状态
sudo apt policy snake4  # 检查snake4 软件安装状态
```

例如:

```shell
linux@ubuntu:~$ sudo apt policy gcc
gcc:
  已安装：4:7.4.0-1ubuntu2.3
  候选： 4:7.4.0-1ubuntu2.3
  版本列表：
 *** 4:7.4.0-1ubuntu2.3 500
        500 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 Packages
        500 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 Packages
        100 /var/lib/dpkg/status
     4:7.3.0-3ubuntu2 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 Packages
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo apt policy sl
sl:
  已安装：(无)
  候选： 3.03-17build2
  版本列表：
     3.03-17build2 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 Packages
linux@ubuntu:~$ sudo apt policy snake4
snake4:
  已安装：(无)
  候选： 1.0.14-1build1
  版本列表：
     1.0.14-1build1 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 Packages
        100 /var/lib/dpkg/status
linux@ubuntu:~$ 
```

#### 4. apt list 软件清单 

```shell
sudo apt list  # 列出系统所有的软件包清单 
sudo apt list <软件名称> # 列出这个软甲的清单 
sudo apt list <软件名称>  -a  # 列出这个软件的所有版本清单 

# 例如
sudo apt list gcc
sudo apt list gcc  -a
sudo apt list sl -a 
sudo apt list snake4  
```

例如

```shell
linux@ubuntu:~$ sudo apt list gcc  
正在列表... 完成
gcc/bionic-security,bionic-updates,now 4:7.4.0-1ubuntu2.3 amd64 [已安装]
N: 还有 1 个版本。请使用 -a 选项来查看它(他们)。
linux@ubuntu:~$ sudo apt list gcc  -a
正在列表... 完成
gcc/bionic-security,bionic-updates,now 4:7.4.0-1ubuntu2.3 amd64 [已安装]
gcc/bionic 4:7.3.0-3ubuntu2 amd64

linux@ubuntu:~$ sudo apt list sl -a 
正在列表... 完成
sl/bionic 3.03-17build2 amd64

linux@ubuntu:~$ sudo apt list snake4
正在列表... 完成
snake4/bionic,now 1.0.14-1build1 amd64 [配置文件残留]    # 这个软件在卸载后, 有残留, 需要解决 
linux@ubuntu:~$ 
```

#### 5. apt purge 卸载软件和配置文件 

```shell
sudo apt purge  <软件名称>

sudo apt purge snake4  # 卸载软件和清除配置文件
```

例如:

```shell
linux@ubuntu:~$ sudo apt purge snake4
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列软件包是自动安装的并且现在不需要了：
  libshhmsg1 libshhopt1
使用'sudo apt autoremove'来卸载它(它们)。
下列软件包将被【卸载】：
  snake4*
升级了 0 个软件包，新安装了 0 个软件包，要卸载 1 个软件包，有 0 个软件包未被升级。
解压缩后会消耗 0 B 的额外空间。
您希望继续执行吗？ [Y/n] y
(正在读取数据库 ... 系统当前共安装有 162597 个文件和目录。)
正在清除 snake4 (1.0.14-1build1) 的配置文件 ...    # 残留的配置文件被清除 
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo apt list snake4    # 检查是否把配置文件清除 
正在列表... 完成
snake4/bionic 1.0.14-1build1 amd64
linux@ubuntu:~$ 

```

#### 6. apt search 搜索软件包

在可以用的软件包中搜索制定的包 , 如果找到，该命令将返回名称与搜索词匹配的包。

```shell
sudo apt search <软件包名称>

# 例如 
sudo apt search snake4

```

- 例如

```shell


linux@ubuntu:~$ sudo apt search snake4
正在排序... 完成
全文搜索... 完成  
snake4/bionic 1.0.14-1build1 amd64
  Snake game


```

#### 7. apt update 更新软件目录

从服务器更新软件信息到本地 ，保持本地的软件信息和服务器一致 。

```shell
sudo apt update    # 更软软件源信息 

sudo apt list --upgradable   # 查看可升级的软件信息
```

例如：

```shell
linux@ubuntu:~$ sudo apt update 
命中:1 http://mirrors.yun-idc.com/ubuntu bionic InRelease
获取:2 http://mirrors.yun-idc.com/ubuntu bionic-backports InRelease [74.6 kB]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic-security InRelease [88.7 kB]
获取:4 http://mirrors.yun-idc.com/ubuntu bionic-updates InRelease [88.7 kB]
获取:5 http://mirrors.yun-idc.com/ubuntu bionic-backports/universe amd64 DEP-11 Metadata [9,264 B]
获取:6 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 Packages [2,365 kB]
获取:7 http://mirrors.yun-idc.com/ubuntu bionic-security/main i386 Packages [1,225 kB]
获取:8 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 DEP-11 Metadata [55.3 kB]
获取:9 http://mirrors.yun-idc.com/ubuntu bionic-security/universe i386 Packages [1,032 kB]
获取:10 http://mirrors.yun-idc.com/ubuntu bionic-security/universe amd64 Packages [1,222 kB]
获取:11 http://mirrors.yun-idc.com/ubuntu bionic-security/universe Translation-en [282 kB]
获取:12 http://mirrors.yun-idc.com/ubuntu bionic-security/universe amd64 DEP-11 Metadata [61.1 kB]
获取:13 http://mirrors.yun-idc.com/ubuntu bionic-security/multiverse amd64 DEP-11 Metadata [2,464 B]
获取:14 http://mirrors.yun-idc.com/ubuntu bionic-updates/multiverse amd64 DEP-11 Metadata [2,464 B]
获取:15 http://mirrors.yun-idc.com/ubuntu bionic-updates/universe amd64 Packages [1,837 kB]
获取:16 http://mirrors.yun-idc.com/ubuntu bionic-updates/universe i386 Packages [1,623 kB]
获取:17 http://mirrors.yun-idc.com/ubuntu bionic-updates/universe Translation-en [398 kB]
获取:18 http://mirrors.yun-idc.com/ubuntu bionic-updates/universe amd64 DEP-11 Metadata [302 kB]
获取:19 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 Packages [2,706 kB]
获取:20 http://mirrors.yun-idc.com/ubuntu bionic-updates/main i386 Packages [1,523 kB]
获取:21 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 DEP-11 Metadata [297 kB]
已下载 15.2 MB，耗时 3秒 (5,401 kB/s)                              
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
有 1 个软件包可以升级。请执行 ‘apt list --upgradable’ 来查看它们。
linux@ubuntu:~$ 
linux@ubuntu:~$ apt list --upgradable   # 查看可升级的软件信息
正在列表... 完成
rsync/bionic-security,bionic-updates 3.1.2-2.1ubuntu1.5 amd64 [可从该版本升级：3.1.2-2.1ubuntu1.4]
N: 还有 2 个版本。请使用 -a 选项来查看它(他们)。
linux@ubuntu:~$ 
```

#### 8. apt upgrade 更新软件到最新

把系统升级到最新 

![image-20220822141609480](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220822141609480.png)

```shell
sudo apt upgrade   # 把系统所有的软件升级到最新 ， 类似windows的更新 
```

- 例如

```shell
linux@ubuntu:~$ sudo apt upgrade 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
正在计算更新... 完成
下列软件包是自动安装的并且现在不需要了：
  libshhmsg1 libshhopt1
使用'sudo apt autoremove'来卸载它(它们)。
下列软件包将被升级：
  rsync
升级了 1 个软件包，新安装了 0 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
1 standard security update
需要下载 335 kB 的归档。
解压缩后会消耗 0 B 的额外空间。
您希望继续执行吗？ [Y/n] y
获取:1 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 rsync amd64 3.1.2-2.1ubuntu1.5 [335 kB]
已下载 335 kB，耗时 1秒 (311 kB/s)
(正在读取数据库 ... 系统当前共安装有 162598 个文件和目录。)
正准备解包 .../rsync_3.1.2-2.1ubuntu1.5_amd64.deb  ...
正在将 rsync (3.1.2-2.1ubuntu1.5) 解包到 (3.1.2-2.1ubuntu1.4) 上 ...
正在设置 rsync (3.1.2-2.1ubuntu1.5) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在处理用于 ureadahead (0.100.0-21) 的触发器 ...
正在处理用于 systemd (237-3ubuntu10.53) 的触发器 ...
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo apt autoremove
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列软件包将被【卸载】：
  libshhmsg1 libshhopt1
升级了 0 个软件包，新安装了 0 个软件包，要卸载 2 个软件包，有 0 个软件包未被升级。
解压缩后将会空出 85.0 kB 的空间。
您希望继续执行吗？ [Y/n] y
(正在读取数据库 ... 系统当前共安装有 162597 个文件和目录。)
正在卸载 libshhmsg1 (1.4.2-1) ...
正在卸载 libshhopt1 (1.1.7-3) ...
正在处理用于 libc-bin (2.27-3ubuntu1.6) 的触发器 ...
linux@ubuntu:~$ 

linux@ubuntu:~$ sudo apt upgrade 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
正在计算更新... 完成
升级了 0 个软件包，新安装了 0 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
linux@ubuntu:~$ sudo apt autoremove
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
升级了 0 个软件包，新安装了 0 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
linux@ubuntu:~$ 

```

#### 9. apt clean 删除下载的软件包

在安装软件时 ， 需要先下载软件包到本地， 之后才可以安装软件包

软件包被下载在： /var/cache/apt/archives/

```shell
sudo apt autoclean   # 清除老版本的软件包 ， 保留新版本的软件包 
sudo apt clean       # 把所有下载的软件包删除掉 
```



```shell
linux@ubuntu:~$ ls /var/cache/apt/archives/
apport_2.20.9-0ubuntu7.28_all.deb                                 liblwres160_1%3a9.11.3+dfsg-1ubuntu1.17_amd64.deb
apport-gtk_2.20.9-0ubuntu7.28_all.deb                             liblzma5_5.2.2-1.3ubuntu0.1_amd64.deb
libklibc_2.0.4-9ubuntu2.2_amd64.deb                               xwayland_2%3a1.19.6-1ubuntu4.11_amd64.deb
libldap-2.4-2_2.4.45+dfsg-1ubuntu1.11_amd64.deb                   xxd_2%3a8.0.1453-1ubuntu1.8_amd64.deb
libldap-common_2.4.45+dfsg-1ubuntu1.11_all.deb                    xz-utils_5.2.2-1.3ubuntu0.1_amd64.deb
liblouis14_3.5.0-1ubuntu0.4_amd64.deb                             zlib1g_1%3a1.2.11.dfsg-0ubuntu2.1_amd64.deb
liblouis-data_3.5.0-1ubuntu0.4_all.deb                            zlib1g_1%3a1.2.11.dfsg-0ubuntu2.2_amd64.deb
liblua5.2-0_5.2.4-1.1build1_amd64.deb
linux@ubuntu:~$ du -mh /var/cache/apt/archives/   # 查看目录的大小 
du: 无法读取目录'/var/cache/apt/archives/partial': 权限不够
4.0K	/var/cache/apt/archives/partial
396M	/var/cache/apt/archives/ 
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo apt autoclean 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
Del rsync 3.1.2-2.1ubuntu1.4 [334 kB]
Del zlib1g 1:1.2.11.dfsg-0ubuntu2.1 [56.4 kB]
linux@ubuntu:~$  
linux@ubuntu:~$ sudo apt clean 
linux@ubuntu:~$ du -mh /var/cache/apt/archives/
du: 无法读取目录'/var/cache/apt/archives/partial': 权限不够
4.0K	/var/cache/apt/archives/partial
28K	/var/cache/apt/archives/
linux@ubuntu:~$ 
```

#### 10. apt show 软件包的详细信息

查看软件包的详细信息 

```shell
sudo apt show <软件的名称>

# 例如
sudo apt show gcc     # 查看gcc 软件包的详细信息
sudo apt show snake4  # 查看snake4 软件包的详细信息
```

- 例如

```shell
linux@ubuntu:~$ sudo apt show gcc
Package: gcc
Version: 4:7.4.0-1ubuntu2.3
Priority: optional
Build-Essential: yes
Section: devel
Source: gcc-defaults (1.176ubuntu2.3)
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Debian GCC Maintainers <debian-gcc@lists.debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 51.2 kB
Provides: c-compiler
Depends: cpp (>= 4:7.4.0-1ubuntu2.3), gcc-7 (>= 7.4.0-1~)
Recommends: libc6-dev | libc-dev
Suggests: gcc-multilib, make, manpages-dev, autoconf, automake, libtool, flex, bison, gdb, gcc-doc
Conflicts: gcc-doc (<< 1:2.95.3)
Supported: 5y
Download-Size: 5,184 B
APT-Manual-Installed: yes
APT-Sources: http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 Packages
Description: GNU C 编译器
 这是 GNU C 编译器，一个高度精炼和优化的 C 编译器。
 .
 这是一个提供默认 GNU C 编译器的依赖包。

N: 有 1 条附加记录。请加上 ‘-a’ 参数来查看它们
linux@ubuntu:~$ sudo apt show snake4
Package: snake4
Version: 1.0.14-1build1
Priority: optional
Section: universe/games
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Alexandre Dantas <eu@alexdantas.net>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 272 kB
Depends: libc6 (>= 2.14), libshhmsg1 (>= 1.4.2), libshhopt1 (>= 1.1.7), libx11-6, libxaw7, libxpm4, libxt6
Homepage: http://shh.thathost.com/pub-unix/#snake4
Download-Size: 36.2 kB
APT-Sources: http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 Packages
Description: Snake game
 This is a basic but nice implementation of the snake game. The objective
 is to "snake around" and eat fruit, while avoiding the evil headbanger
 and not crashing into your tail.
 .
 Features five levels of difficulty and a site-wid
```



## 第07章 用户管理

### 7.1 用户管理命令

**管理员UID为0**：系统的管理员用户。

**系统用户UID为1～999**：Linux系统为了避免因某个服务程序出现漏洞而被黑客提权至整台服务器，默认服务程序会由独立的系统用户负责运行，进而有效控制被破坏范围。

**普通用户UID从1000开始**：是由管理员创建的用于日常工作的用户。

#### 1. id命令

id命令用于显示用户的详细信息，语法格式为“id 用户名”。

这个id命令是一个在创建用户前需要仔细学习的命令，它能够简单轻松地查看用户的基本信息，例如用户ID、基本组与扩展组GID，以便于我们判别某个用户是否已经存在，以及查看相关信息。

```shell
linux@ubuntu:~$ id linux
uid=1000(linux) gid=1000(linux) 组=1000(linux),
4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),116(lpadmin),126(sambashare)
linux@ubuntu:~$ id root
uid=0(root) gid=0(root) 组=0(root)
linux@ubuntu:~$ 
```

#### 2. useradd与adduser命令

在**Ubuntu**中创建新用户，通常会用到两个命令：**useradd**和**adduser**。虽然作用一样，但用法却不尽相同：

- 使用useradd时，如果后面不添加任何参数选项，例如：#sudo useradd test创建出来的用户将是默认“三无”用户：

  一无Home Directory，二无密码，三无系统Shell。

- 使用adduser时，创建用户的过程更像是一种人机对话，系统会提示你输入各种信息，然后会根据这些信息帮你创建新用户。

下面使用adduser命令创建一个名称为ysl的用户，并使用id命令确认信息：

```shell
linux@ubuntu:~$ adduser ysl
adduser：只有 root 才能将用户或组添加到系统。
linux@ubuntu:~$ sudo adduser ysl
adduser：用户"ysl"已经存在。
linux@ubuntu:~$ sudo userdel ysl
linux@ubuntu:~$ id ysl
id: "ysl": no such user
linux@ubuntu:~$ sudo adduser ysl
正在添加用户"ysl"...
正在添加新组"ysl" (1001)...
正在添加新用户"ysl" (1001) 到组"ysl"...
创建主目录"/home/ysl"...
正在从"/etc/skel"复制文件...
输入新的 UNIX 密码： 
重新输入新的 UNIX 密码： 
passwd：已成功更新密码
正在改变 ysl 的用户信息
请输入新值，或直接敲回车键以使用默认值
	全名 []: 
	房间号码 []: 
	工作电话 []: 
	家庭电话 []: 
	其它 []: 
这些信息是否正确？ [Y/n] y
linux@ubuntu:~$ ls /home/
linux  ysl
linux@ubuntu:~$ su ysl   # 切换用户 
密码： 
ysl@ubuntu:/home/linux$  
```

#### 3. userdel 删除用户

userdel命令用于删除已有的用户账户，英文全称为“user delete”，语法格式为“userdel [参数] 用户名”。

如果确认某位用户后续不会再登录到系统中，则可以通过userdel命令删除该用户的所有信息。在执行删除操作时，该用户的家目录默认会保留下来，此时可以使用-r参数将其删除。userdel命令的参数以及作用如表所示。

表5-4                       userdel命令中的参数以及作用

| 参数 | 作用                     |
| ---- | ------------------------ |
| -f   | 强制删除用户             |
| -r   | 同时删除用户及用户家目录 |

```shell
linux@ubuntu:~$ userdel ysl   # 删除用户, 权限不足 
userdel: Permission denied.
userdel：无法锁定 /etc/passwd，请稍后再试。
linux@ubuntu:~$ sudo userdel ysl  # 给一段时间的临时权限
[sudo] linux 的密码： 
userdel: user ysl is currently used by process 1880
linux@ubuntu:~$ kill 1880   # 杀死这个进程 , 才可以删除用户, 因为这个用户在登录状态 
bash: kill: (1880) - 不允许的操作
linux@ubuntu:~$ sudo kill 1880   # 杀死这个进程  (某写原因杀不到的时候,可以重启系统)
linux@ubuntu:~$ sudo userdel ysl  # 删除这个用户 
linux@ubuntu:~$ ls /home/
linux  ysl
linux@ubuntu:~$ rm -rf /home/ysl/ 
rm: 无法删除'/home/ysl/公共的': 权限不够
rm: 无法删除'/home/ysl/.presage': 权限不够
rm: 无法删除'/home/ysl/图片': 权限不够
rm: 无法删除'/home/ysl/.dbus': 权限不够
rm: 无法删除'/home/ysl/文档': 权限不够
rm: 无法删除'/home/ysl/模板': 权限不够
rm: 无法删除'/home/ysl/下载': 权限不够
rm: 无法删除'/home/ysl/视频': 权限不够
rm: 无法删除'/home/ysl/.cache': 权限不够
linux@ubuntu:~$ sudo rm -rf /home/ysl/ 
linux@ubuntu:~$ id ysl   # 查看用户id , 表示已经彻底删除掉了 
id: "ysl": no such user
linux@ubuntu:~$ 
```

#### 4. passwd修改密码

passwd命令用于修改用户的密码、过期时间等信息，英文全称为“password”，语法格式为“passwd [参数] 用户名”。

普通用户只能使用passwd命令修改自己的系统密码，而root管理员则有权限修改其他所有人的密码。更酷的是，root管理员在Linux系统中修改自己或他人的密码时不需要验证旧密码，这一点特别方便。既然root管理员能够修改其他用户的密码，就表示其完全拥有该用户的管理权限。

```shell
passwd   # 这个密码必须6位以上的长度 

# ubuntu中, root用户默认没有设置密码 , 如果需要可以手动设置密码 

```

- 例如

```shell
linux@ubuntu:~$ sudo adduser ysl
[sudo] linux 的密码： 
正在添加用户"ysl"...
正在添加新组"ysl" (1001)...
正在添加新用户"ysl" (1001) 到组"ysl"...
创建主目录"/home/ysl"...
正在从"/etc/skel"复制文件...
输入新的 UNIX 密码：    # 1
重新输入新的 UNIX 密码： #1
passwd：已成功更新密码
正在改变 ysl 的用户信息
请输入新值，或直接敲回车键以使用默认值
	全名 []: 
	房间号码 []: 
	工作电话 []: 
	家庭电话 []: 
	其它 []: 
这些信息是否正确？ [Y/n] 
linux@ubuntu:~$ su ysl   # 切换用户 
密码：  
ysl@ubuntu:/home/linux$ passwd      # 更改密码 
更改 ysl 的密码。
（当前）UNIX 密码： 
输入新的 UNIX 密码：     # 123456
重新输入新的 UNIX 密码：  # 123456
Bad: new and old password are too similar
输入新的 UNIX 密码：     #1234567890
重新输入新的 UNIX 密码：  #1234567890
passwd：已成功更新密码
ysl@ubuntu:/home/linux$ exit  # 退出ysl用户, 退回到linux用户
exit
linux@ubuntu:~$ sudo su root  # 切换到root用户 
root@ubuntu:/home/linux# passwd   # 修改root用户的密码 
输入新的 UNIX 密码：    # 1
重新输入新的 UNIX 密码： # 1
passwd：已成功更新密码
root@ubuntu:/home/linux# exit   # 退出root 用户
exit
linux@ubuntu:~$ 
```

#### 5. su  切换用户命令

切换用户  

```shell
su ysl   # 切换到ysl用户下 
su root  #
```

- 例如

```shell
linux@ubuntu:~$ su root  # 切换到root 用户 , 会失败, 原因ubuntu是root用户默认没有密码, 不能通过这种方式进入
密码： 
su：认证失败
linux@ubuntu:~$ sudo su root  # 临时获取超级用户权限, 此时使用超级用户进入root , 需要使用的是linux的密码 
[sudo] linux 的密码：   # 输入linux用户的密码 
root@ubuntu:/home/linux#   # 成功进入root用户 
root@ubuntu:/home/linux# passwd  #更新密码 
输入新的 UNIX 密码：   # 1
重新输入新的 UNIX 密码：# 1
passwd：已成功更新密码
root@ubuntu:/home/linux# exit
linux@ubuntu:~$ 
linux@ubuntu:~$ su root  # 切换到root用户 , 可以切换成功, 是应为root用户已经有密码了 
密码：   # 1
root@ubuntu:/home/linux# 
```

#### 6. sudo  临时获取权限

普通用户临时获取超级用户的权限一段时间, 这段时间内不需要验证 。

```shell
sudo apt install gcc # 安装gcc 

# 如果当前用户已经是超级用户了， 就不需要在命令前加sudo了 
apt install g++       #即可实现安装 
sudo apt install g++  #如果写了sudo , 对结果没有影响 
```













































## 第08章 文件管理

### 8.1 linux文件系统介绍

 ubuntu的根目录如下: 

![image-20220823091318939](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823091318939.png)

####  1.  /bin和/sbin目录

​	bin 是 Binary 的缩写，存放着可执行文件或可执行文件的链接（类似快捷方式），你可以看到 ls, cat, mkdir 等常用命令都在这里，/bin 目录中的文件都是可执行的二进制文件，而不是文本文件。

![image-20220823085853653](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823085853653.png)

​	与 /bin 类似的是 /sbin 目录，System Binary 的缩写，这里存放的命令可以对系统配置进行操作。普通用户可能可以使用这里的命令查看某些系统状态，但是如果想更改配置，就需要 sudo 授权或者切换成超级用户。

可以看到一些熟悉的命令，比如 ifconfig, iptables。普通用户可以使用 ifconfig 查看网卡状态，但是想配置网卡信息，就需要授权了。

![image-20220823085951679](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823085951679.png)



#### 2. /boot 目录

​	这里是系统启动需要的文件，你可以看到 grub 文件夹，它是常见的开机引导程序。我们不应该乱动这里的文件。

![image-20220823090551468](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823090551468.png)

#### 3. /dev 目录

​	dev 是 device 的缩写，这里存放这所有的设备文件。在 Linux 中，所有东西都是以文件的形式存在的，包括硬件设备。

比如说，sda, sdb 就是我电脑上的两块硬盘，后面的数字是硬盘分区 ， 鼠标、键盘等等设备也都可以在这里找到。

![image-20220823090709661](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823090709661.png)

#### 4. /etc 目录

这个目录经常使用，存放很多程序的配置信息。

![image-20220823090951434](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823090951434.png)

#### 5.  /lib 目录

​	lib 是 Library 的缩写，类似于 Windows 系统中存放 dll 文件的库，包含 bin 和 sbin 中可执行文件的依赖。也可能出现 lib32 或 lib64 这样的目录，和 lib 差不多，只是操作系统位数不同而已。

![image-20220823091244977](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823091244977.png)

#### 6.  /media 目录

​	这里会有一个以你用户名命名的文件夹，里面是自动挂载的设备，比如 U 盘，移动硬盘，网络设备等。

比如说我在电脑上插入一个 U 盘，系统会把 U 盘自动给我挂载到 /media/linux 这个文件夹里（我的用户名是 linux），如果我要访问 U 盘的内容，就可以在那里找到。

![image-20220823092037408](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823092037408.png)

#### 7.  /mnt 目录

​	这也是和设备挂载相关的一个文件夹，一般是空文件夹。media 文件夹是系统自动挂载设备的地方，这里是你手动挂载设备的地方。

比如说，刚才我们在 dev 中看到了一大堆设备，你想打开某些设备看看里面的内容，就可以通过命令把设备挂载到 mnt 目录进行操作。

不过一般来说，现在的操作系统已经很聪明了，像挂载设备的操作几乎都不用你手动做，都被自动挂载到 media 目录了。

![image-20220823092211223](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823092211223.png)



#### 8. /opt 目录

​	opt 是 Option 的缩写，这个文件夹的使用比较随意，一般来说我们自己在浏览器上下载的软件，安装在这里比较好。当然，包管理工具下载的软件也可能被存放在这里。

 比如我在这里存放了 搜狗输入法等等软件的文件夹。

![image-20220823092322868](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823092322868.png)



#### 9.  /proc 目录

proc 是 process 的缩写，这里存放的是全部正在运行程序的状态信息。

![image-20220823092459756](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823092459756.png)

你会发现 /proc 里面有一大堆数字命名的文件夹，这个数字其实是 Process ID（PID），文件夹里又有很多文件。

前面说过，Linux 中一切都以文件形式储存，类似 /dev，这里的文件也不是真正的文件，而是程序和内核交流的一些信息。比如说我们可以查看当前操作系统的版本

![image-20220823092659085](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823092659085.png)

或者查看 CPU 的状态：

![image-20220823092913381](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823092913381.png)

![image-20220823093029842](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823093029842.png)

如果你需要调试应用程序，proc 目录中的信息也许会帮上忙。

#### 10.  /root 目录

这是 root 用户的家目录，普通用户需要授权才能访问。

![image-20220823094055800](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823094055800.png)

#### 11.  /run 和 /sys 目录

用来存储某些程序的运行时信息和系统需要的一些信息。

![image-20220823094223684](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823094223684.png)

需要注意的是，这两个位置的数据都存储在内存中，所以一旦重启，/run 和 /sys 目录的信息就会丢失，所以不要试图在这里存放任何文件。

#### 12.  /srv 目录

​	srv 是 service 的缩写，主要用来存放服务数据。

​	对于桌面版 Linux 系统，这个文件夹一般是空的，但是对于 Linux 服务器，Web 服务或者 FTP 文件服务的资源可以存放在这里。

![image-20220823094342439](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823094342439.png)

#### 13.  /tmp 目录

tmp 是 temporary 的缩写，存储一些程序的临时文件。

![image-20220823094432341](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823094432341.png)

临时文件可能起到很重要的作用。比如经常听说某同学的 Word 文档崩溃了，好不容易写的东西全没了，Linux 的很多文本编辑器都会在 /tmp 放一份当前文本的 copy 作为临时文件，如果你的编辑器意外崩溃，还有机会在 /tmp 找一找临时文件抢救一下。

当然，tmp 文件夹在系统重启之后会自动被清空，如果没有被清空，说明系统删除某些文件失败，也许需要你手动删除一下

#### 14. **/usr** 目录

usr 是 Universal System Resource 的缩写，这里存放的是一些非系统必须的资源，比如用户安装的应用程序。)

![image-20220823094745999](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823094745999.png)

/usr 和 /usr/local 目录中又含有 bin 和 sbin 目录，也是存放可执行文件（命令），但和根目录的 bin 和 sbin 不同的是，这里大都是用户使用的工具，而非系统必须使用的。

值得一提的是，如果使用 Linux 桌面版，有时候在桌面找不到应用程序的快捷方式，就需要在 /usr/share/applications 中手动配置桌面图标文件：

![image-20220823095036392](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823095036392.png)

#### 15.  /var 目录

var 是 variable 的缩写，这个名字是历史遗留的，现在该目录最主要的作用是存储日志（log）信息，比如说程序崩溃，防火墙检测到异常等等信息都会记录在这里。

日志文件不会自动删除，也就是说随着系统使用时间的增长，你的 var 目录占用的磁盘空间会越来越大，也许需要适时清理一下。

![image-20220823093331021](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823093331021.png)

#### 16. /home 目录

​	home 目录，这是普通用户的家目录。在桌面版的 Linux 系统中，用户的家目录会有下载、视频、音乐、桌面等文件夹，这些没啥可说的，我们说一些隐藏的比较重要的文件夹（Linux 中名称以 . 开头就是隐藏文件）。

这是我的家目录的部分文件：

![image-20220823093641980](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823093641980.png)

![image-20220823093724829](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823093724829.png)

其中 .cache 文件夹存储应用缓存数据，.config 文件夹存储了一部分应用程序的配置，比如说我的 Chrome 浏览器配置就是那里面。但是还有一部分应用程序并不把配置储存在 .config 文件夹，而是自己创建一个隐藏文件夹，存放自己的配置文件等等信息，比如你可以看到 .sougouinput的配置文件就不在 .config 中。

最后说 .local 文件夹，有点像 /usr/local，里面也有 bin 文件夹，也是存放可执行文件的。比如说我的 python pip 以及 pip 安装的一些工具，都存放在 ～/.local/bin 目录中。但是，存在这里的文件，只有该用户才能使用。

这就是为什么，有时候普通用户可以使用的命令，用 sudo 或者超级用户却被告知找不到该命令。因为有的命令是特定用户家目录里的，被添加到了该用户的 PATH 环境变量里，他可以直接用。你超级用户想用当然可以，但是得写全绝对路径才行。

#### 17. cdrom 目录

光盘驱动器的目录 , 一般为空

![image-20220823100159803](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823100159803.png)

#### 18. snap 目录

Snaps是跨发行版、无依赖关系且易于安装的应用程序，这些应用程序与所有依赖项打包在一起，可以在所有主要的 Linux 发行版上运行。从单个构建开始，一个 snap（应用程序）将在桌面、云和 IoT 上的所有受支持的 Linux 发行版上运行。支持的发行版包括Ubuntu、Debian、Fedora、Arch Linux、Manjaro 和 CentOS/RHEL。

存放snap软件的目录

![image-20220823100330922](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823100330922.png)

 

#### 19. swapfile 文件

交换文件允许 Linux 将磁盘空间模拟为内存。当你的系统开始耗尽内存时，它会使用交换空间将内存的一些内容交换到磁盘空间上。这样释放了内存，为更重要的进程服务。当内存再次空闲时，它会从磁盘交换回数据。

传统上，交换空间是磁盘上的一个独立分区。安装 Linux 时，只需创建一个单独的分区进行交换。但是这种趋势在最近几年发生了变化。

使用交换文件，你不再需要单独的分区。你会根目录下创建一个文件，并告诉你的系统将其用作交换空间就行了。

使用专用的交换分区，在许多情况下，调整交换空间的大小是一个可怕而不可能的任务。但是有了交换文件，你可以随意调整它们的大小。

最新版本的 Ubuntu 和其他一些 Linux 发行版已经开始 默认使用交换文件。甚至如果你没有创建交换分区，Ubuntu 也会自己创建一个 1GB 左右的交换文件。

![image-20220823100910054](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823100910054.png)

### 8.2 文件类型(bcd-lsp)

linux把文件类型分为7种 ,  简写为 "BCD-LSP"

```
B :  block  , 块设备  , 驱动相关
C :  char   , 字符设备, 驱动相关
D :  dir    , 目录
- :  regular , 普通文件
L :  link    , 软连接, 快捷方式
S :  socket  , 套接字, 网络通信
P :  pipe    , 管道, 进程通信
```

```shell
# 文件属性的第一个字符, 就是文件类型
brw-rw----  1 root  disk      8,   0 8月  22 15:38 sda
crw-rw----  1 root  tty       7,  70 8月  22 15:38 vcsu6    # 字符设备 
drwxr-xr-x  2 root  root          60 8月  22 15:38 vfio     # 目录
-rw------- 1 linux linux      447 8月  19 16:25 01-hello.c  #普通文件
lrwxrwxrwx 1 root root       8 8月  16 10:51 ypdomainname -> hostname  # 软连接, 快捷方式
srwxrwxr-x 1 linux linux        0 8月  23 10:32 socket      #socket 文件
prw-rw-r-- 1 linux linux        0 8月  23 10:30 1           # 管道文件
```



#### 1. 普通文件 (-)

- 像文本文件、二进制文件，我们编写的源代码文件这些都是普通的文件。

文本文件：

- 譬如常见的.c、.h、.sh、.txt 等这些都是文本文件，文本文件的好处就是方便人阅读、浏览以及编写

```shell
linux@ubuntu:~/work$ stat 01-hello.c 
  文件：01-hello.c
  大小：447       	块：8          IO 块：4096   普通文件
设备：801h/2049d	Inode：2755157     硬链接：1
权限：(0600/-rw-------)  Uid：( 1000/   linux)   Gid：( 1000/   linux)
最近访问：2022-08-19 16:25:32.158855594 +0800
最近更改：2022-08-19 16:25:32.158855594 +0800
最近改动：2022-08-19 16:25:32.158855594 +0800
创建时间：-
linux@ubuntu:~/work$       
```

- 二进制文件：

linux 系统下的可执行文件、C 代码编译之后得到的.o 文件、.bin文件等都是二进制文件

```shell
linux@ubuntu:~/work$ stat a.out 
  文件：a.out
  大小：8520      	块：24         IO 块：4096   普通文件
设备：801h/2049d	Inode：2753336     硬链接：1
权限：(0775/-rwxrwxr-x)  Uid：( 1000/   linux)   Gid：( 1000/   linux)
最近访问：2022-08-19 11:31:01.599042574 +0800
最近更改：2022-08-19 11:30:58.614827271 +0800
最近改动：2022-08-19 11:30:58.614827271 +0800
创建时间：-
linux@ubuntu:~/work$     
```

#### 2. 目录文件(d)

- 目录文件就是文件夹，在linux下面文件夹也是一种文件（特殊的文件）。可以使用vim打开显示。在Linux系统下，会有一些专门的系统调用用于读写文件夹。

```
linux@ubuntu:~$ ls -l 
总用量 40
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Desktop
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Documents
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Downloads
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Music
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Pictures
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Public
drwx------ 4 linux linux 4096 8月  17 10:19 snap
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Templates
drwxr-xr-x 2 linux linux 4096 8月  16 11:03 Videos
drwxrwxr-x 3 linux linux 4096 8月  23 10:32 work
linux@ubuntu:~$ 
       
```

#### 3. 字符设备文件(c)

#### 4. 块设备文件(b)

- 关于设备的驱动开发，对字符设备文件（character）、块设备文件（block）这些文件类型应该都是熟悉的 。
- Linux 系统下，一切皆文件，也包括各种硬件设备。设备文件（字符设备文件、块设备文件）对应的是硬件设备，在Linux 系统中，硬件设备会对应到一个设备文件，应用程序通过对设备文件的读写来操控、使用硬件设备。
- 虽然有设备文件，但是设备文件并不对应磁盘上的一个文件，也就是说设备文件并不存在于磁盘中，而是由文件系统虚拟出来的。一般是由内存来维护，当系统关机时，设备文件都会消失。
- 字符设备文件一般存放在 Linux系统/dev/目录下，所以/dev也称为虚拟文件系统 devfs。

```shell
linux@ubuntu:~$ ls /dev/ -l 
总用量 0
crw-------  1 root  root     10, 175 8月  22 15:38 agpgart
crw-r--r--  1 root  root     10, 235 8月  22 15:38 autofs
drwxr-xr-x  2 root  root         520 8月  23 09:21 block
drwxr-xr-x  2 root  root          80 8月  23 09:21 bsg
crw-------  1 root  root     10, 234 8月  22 15:38 btrfs-control
drwxr-xr-x  3 root  root          60 8月  22 15:38 bus
lrwxrwxrwx  1 root  root           3 8月  22 15:38 cdrom -> sr0
lrwxrwxrwx  1 root  root           3 8月  22 15:38 cdrw -> sr0
drwxr-xr-x  2 root  root        3760 8月  23 09:21 char
crw-------  1 root  root      5,   1 8月  22 15:38 console
lrwxrwxrwx  1 root  root          11 8月  22 15:38 core -> /proc/kcore
crw-------  1 root  root     10,  60 8月  22 15:38 cpu_dma_latency
crw-------  1 root  root     10, 203 8月  22 15:38 cuse
drwxr-xr-x  6 root  root         120 8月  22 15:38 disk
crw-rw----+ 1 root  audio    14,   9 8月  22 15:38 dmmidi
drwxr-xr-x  3 root  root         100 8月  22 15:38 dri
lrwxrwxrwx  1 root  root           3 8月  22 15:38 dvd -> sr0
crw-------  1 root  root     10,  62 8月  22 15:38 ecryptfs
crw-rw----  1 root  video    29,   0 8月  22 15:38 fb0
                     
```

#### 5. 符号链接文件(l)

- 符号链接文件（link）类似于 Windows系统中的快捷方式文件，是一种特殊文件，它的内容指向的是另一个文件路径，当对符号链接文件进行操作时，系统根据情况会对这个操作转移到它指向的文件上去，而不是对它本身进行操作。
- 符号链接分为两种。软连接和硬链接
- 建立软硬链接

```shell
软连接  : ln -s  源文件 目标文件 
硬链接  : ln  源文件  目标文件
源文件  : 是要对谁创建快捷方式      
```

```shell
linux@ubuntu:~/work$ ln -s a.out  a.exe
linux@ubuntu:~/work$ ls -l 
总用量 81176
prw-rw-r-- 1 linux linux        0 8月  23 10:30 1
lrwxrwxrwx 1 linux linux        5 8月  23 10:46 a.exe -> a.out
-rwxrwxr-x 1 linux linux     8520 8月  19 11:30 a.out
linux@ubuntu:~/work$ 
```

- 二者的区别
- 软连接可以理解为window的快捷方式 , 它和window的快捷方式的作用是一样的 。
- 硬链接是用两个文件名指向同一块磁盘空间   , 两个文件名有相同的inode号   
- 磁盘存放文件的原理

![inode](https://gitee.com/embmaker/cloudimage/raw/master/img/inode.png)      

```shell
linux@ubuntu:~/work$ ln  01-hello.c  02-hello.c
linux@ubuntu:~/work$ stat 01-hello.c     # inode 号 在linux系统是唯一的, 除非是硬链接
  文件：01-hello.c
  大小：447       	块：8          IO 块：4096   普通文件
设备：801h/2049d	Inode：2755157     硬链接：2
权限：(0600/-rw-------)  Uid：( 1000/   linux)   Gid：( 1000/   linux)
最近访问：2022-08-19 16:25:32.158855594 +0800
最近更改：2022-08-19 16:25:32.158855594 +0800
最近改动：2022-08-23 10:53:46.559329619 +0800
创建时间：-
linux@ubuntu:~/work$ stat 02-hello.c 
  文件：02-hello.c
  大小：447       	块：8          IO 块：4096   普通文件
设备：801h/2049d	Inode：2755157     硬链接：2
权限：(0600/-rw-------)  Uid：( 1000/   linux)   Gid：( 1000/   linux)
最近访问：2022-08-19 16:25:32.158855594 +0800
最近更改：2022-08-19 16:25:32.158855594 +0800
最近改动：2022-08-23 10:53:46.559329619 +0800
创建时间：-
linux@ubuntu:~/work$ 

```

简单的说下：

软连接：很简单类比一下windows的快捷方式。

硬链接： 硬链接文件和源文件inode节点号相同，并且一个inode节点可以对应多个文件名。

#### 6. 管道文件（p）

- 主要用于进程间通信

- 管道分为无名管道和有名管道两种管道，管道文件是建立在内存之上可以同时被两个进程访问的文件

#### 7. 套接字文件(s)

- 套接字文件（socket）也是一种进程间通信的方式，与管道文件不同的是，它们可以在不同主机上的进程间通信，实际上就是网络通信。  

​      

### 8.3 文件权限与归属

#### 1. 文件的权限

- ​	在Linux系统中，每个文件都有归属的所有者和所属组，并且规定了文件的所有者、所属组以及其他人对文件所拥有的可读（r）、可写（w）、可执行（x）等权限。

- 对于一般文件来说，权限比较容易理解：“可读”表示能够读取文件的实际内容；“可写”表示能够编辑、新增、修改、删除文件的实际内容；“可执行”则表示能够运行一个脚本程序。但是，对于目录文件来说，理解其权限设置就不那么容易了。很多资深Linux用户其实也没有真正搞明白。对于目录文件来说，“可读”表示能够读取目录内的文件列表；“可写”表示能够在目录内新增、删除、重命名文件；而“可执行”则表示能够进入该目录。

- 可读、可写、可执行权限对应的命令在文件和目录上是有区别的

![image-20220823140630899](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823140630899.png)

- 例如： 

```shell
linux@ubuntu:~/work$ ls
linux@ubuntu:~/work$ mkdir hello
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:14 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
drwxrwxr-x  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ chmod a-x hello/    # 给目录去掉可执行全选， 之后目录不能进入
linux@ubuntu:~/work$ cd hello/           # 进入目录失败， 因为没有写权限
bash: cd: hello/: 权限不够
linux@ubuntu:~/work$ chmod a+x-w hello/  # 给目录一个执行权限， 取消写权限， 此时目录中不能创建子目录和文件
linux@ubuntu:~/work$ cd hello/
linux@ubuntu:~/work/hello$ touch 1.c     #权限不足 
touch: 无法创建'1.c': 权限不够
linux@ubuntu:~/work/hello$ cd ..
linux@ubuntu:~/work$ ls
hello
linux@ubuntu:~/work$ chmod a+xw-r hello/  #给目录可执行，可写， 去掉可读权限 ， 目录不能访问
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:14 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
d-wx-wx-wx  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ ls hello/    # 目录不能访问 
ls: 无法打开目录'hello/': 权限不够
linux@ubuntu:~/work$ 
```

文件权限表 

```shell
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
d        | -wx     | -wx      | -wx      | 2       | linux      |linux    | 4096  |8月  23 14:14 | hello/
文件类型  | 文件所有者| 文件所属组 | 其他用户  |硬链接数量 | 文件所有者  | 文件所属组|文件大小|文件修改时间    | 文件名
#  第1个字符 ：文件的类型 
#  第2-4字符 ：文件的所有者权限
#  第5-7字符 ：文件的所属组 
#  第8-10字符：其他用户的权限， 其他组的成员访问这个文件的权限 
#  r ：可读 
#  w ： 可写 
#  x ： 可执行
#  - ： 没有这个权限

```

![image-20220823141914075](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823141914075.png)

文件权限的数字表示法基于字符（rwx）的权限计算而来，其目的是简化权限的表示方式。

例如，若某个文件的权限为7，则代表可读、可写、可执行（4+2+1）；若权限为6，则代表可读、可写（4+2）。

我们来看一个例子。现在有这样一个文件，其所有者拥有可读、可写、可执行的权限，其文件所属组拥有可读、可写的权限；其他人只有可读的权限。那么，这个文件的权限就是rwxrw-r--，数字法表示即为764。

​	这里以rw-r-x-w-权限为例来介绍如何将字符表示的权限转换为数字表示的权限。首先，要将各个位上的字符替换为数字，如图所示。

![image-20220823142554325](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823142554325.png)



减号是占位符，代表这里没有权限，在数字表示法中用0表示。也就是说，rw-转换后是420，r-x转换后是401，-w-转换后是020。然后，将这3组数字之间的每组数字进行相加，得出652，这便是转换后的数字表示权限。

#### 2. chmod 修改文件的权限

- 修改文件的权限位 

```
chmod [OPTION]... MODE[,MODE]... FILE...
```

- 第一种表达:数字表示法 

```shell
chmod 0777  hello   # 把hello 目录改为777 , 这个777 是八进制的数, 要用0开头 
```

- 例如

```shell
linux@ubuntu:~/work$ chmod 0777 hello/
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
drwxrwxrwx  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ chmod 0000 hello/
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
d---------  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ chmod 0664 hello/
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
drw-rw-r--  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ chmod 0775 hello/
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
drwxrwxr-x  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ 
```

- 第2种表示方法: 字符表示法 

  语法：chmod [who] [+ | - | =] [mode] 文件名

  命令中各选项的含义为：

  操作对象who可是下述字母中的任一个或者它们的组合：

  　　u 表示“用户（user）”，即文件或目录的所有者。

  　　g 表示“同组（group）用户”，即与文件属主有相同组ID的所有用户。

  　　o 表示“其他（others）用户”。

  　　a 表示“所有（all）用户”。它是系统默认值。

  操作符号可以是：

  　　+ 添加某个权限。

  　　- 取消某个权限。

  　　= 赋予给定权限并取消其他所有权限（如果有的话）。

  设置 mode 所表示的权限可用下述字母的任意组合：

  　　r 可读。

  　　w 可写。

   　   x 可执行。

```shell
chmod a+x-w hello   # a 表示所有用户 +x , 给所有用户一个可执行权限 , -w 去掉所有用户的可写权限 
chmod a+x-w hello/
chmod a-rwx hello/
chmod a+rwx hello/
```

- 例如

```shell
linux@ubuntu:~/work$ chmod a+x-w hello/
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
dr-xr-xr-x  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ chmod a-rwx hello/
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
d---------  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ chmod a+rwx hello/
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
drwxrwxrwx  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ 

# chmod 0664 使用字符表示法实现
linux@ubuntu:~/work$ chmod ug+rw-x,o+r-wx 1.txt   # 0664 字符表示方法 
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
drwxrwxrwx  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ 

```

#### 3. chown 修改文件的所有者 

​	所有的文件皆有其拥有者（Owner）。利用 chown命令 可以将文件的拥有者加以改变。一般来说，这个命令只能由系统管理者(root)使用，一般用户没有权限来改变别人的文件的拥有者，也没有权限可以将自己的文件的拥有者改设为别人。只有系统管理者(root)才有这样的权限。

```shell
chown [OPTION]... [OWNER][:[GROUP]] FILE...

# 例如
chown  linux:linux 1.c   # 把1.c的文件所有者和所属组改为linux:linux 
```

- 例如

```shell
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
drwxrwxrwx  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ sudo chown root:root 1.txt 
[sudo] linux 的密码： 
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 root  root     0 8月  23 14:19 1.txt
drwxrwxrwx  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ sudo chown linux:linux 1.txt 
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 linux linux    0 8月  23 14:19 1.txt
drwxrwxrwx  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ sudo chown root 1.txt 
linux@ubuntu:~/work$ ll
总用量 12
drwxrwxr-x  3 linux linux 4096 8月  23 14:19 ./
drwxr-xr-x 22 linux linux 4096 8月  23 10:42 ../
-rw-rw-r--  1 root  linux    0 8月  23 14:19 1.txt
drwxrwxrwx  2 linux linux 4096 8月  23 14:14 hello/
linux@ubuntu:~/work$ 

```

### 8.4 文件操作

#### 1. cp 命令

​	cp命令来自于英文单词copy的缩写，用于将一个或多个文件或目录复制到指定位置，亦常用于文件的备份工作。-r参数用于递归操作，复制目录时若忘记加则会直接报错，而-f参数则用于当目标文件已存在时会直接覆盖不再询问，这两个参数尤为常用。

**语法格式：**cp [参数] 源文件 目标文件

**常用参数：**

| -f   | 若目标文件已存在，则会直接覆盖原文件                         |
| ---- | ------------------------------------------------------------ |
| -i   | 若目标文件已存在，则会询问是否覆盖                           |
| -p   | 保留源文件或目录的所有属性                                   |
| -r   | 递归复制文件和目录                                           |
| -d   | 当复制符号连接时，把目标文件或目录也建立为符号连接，并指向与源文件或目录连接的原始文件或目录 |
| -l   | 对源文件建立硬连接，而非复制文件                             |
| -s   | 对源文件建立符号连接，而非复制文件                           |
| -b   | 覆盖已存在的文件目标前将目标文件备份                         |
| -v   | 详细显示cp命令执行的操作过程                                 |
| -a   | 等价于“pdr”选项                                              |

```shell
1. 复制文件
cp   源文件    目标文件 
# 目标文件不存在, 则会自动创建目标文件 
# 目标文件存在, 则会覆盖目标文件的内容

2. 把文件复制到目录内 
cp   源文件    目录 
# 目录不存在 , 会把目录当做一个新的文件名
# 目录存在  , 会把文件复制到目录内 

3. 把目录复制到目录 
cp  -rf  源目录  目的目录 
# 目的目录不存在, 则创目的目录
# 目的目录存在, 则会把源目录复制到目的目录内
```

**参考实例**

```shell
# cp 文件 文件 
linux@ubuntu:~/work$ cp 01-C语言基础.pdf 02-C语言基础.pdf   # 目的文件不存在则创建 
linux@ubuntu:~/work$ ls
01-C语言基础.pdf  02-C语言基础.pdf
linux@ubuntu:~/work$ cp 01-C语言基础.pdf 02-C语言基础.pdf  # 目的文件存在 则覆盖 
linux@ubuntu:~/work$ ls
01-C语言基础.pdf  02-C语言基础.pdf
linux@ubuntu:~/work$ mkdir hello
linux@ubuntu:~/work$ ls
01-C语言基础.pdf  02-C语言基础.pdf  hello
linux@ubuntu:~/work$ cp 01-C语言基础.pdf hello/   # 目录存在, 把问价复制到目录内
linux@ubuntu:~/work$ ls hello/
01-C语言基础.pdf
linux@ubuntu:~/work$ rm -rf hello/
linux@ubuntu:~/work$ cp 01-C语言基础.pdf hello   # 目录不存在, 会把目录当做一个新的文件名
linux@ubuntu:~/work$ ll
总用量 28592
drwxrwxr-x  2 linux linux    4096 8月  24 15:59 ./
drwxr-xr-x 23 linux linux    4096 8月  24 16:02 ../
-rwxrw-r--  1 linux linux 9756631 8月  12 16:11 01-C语言基础.pdf*
-rwxrw-r--  1 linux linux 9756631 8月  24 15:58 02-C语言基础.pdf*
-rwxrw-r--  1 linux linux 9756631 8月  24 15:59 hello*
linux@ubuntu:~/work$ 

# cp -rf  目录 目录
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cp -rf work/ hello
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  hello  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cp -rf work/ hello
linux@ubuntu:~$ ls hello/
01-C语言基础.pdf  02-C语言基础.pdf  hello  work
linux@ubuntu:~$ 

```

#### 2. mv 命令

​	mv命令来自于英文单词move的缩写，其功能与英文含义相同，用于对**文件进行剪切和重命名**。

这是一个高频使用的文件管理命令，我们需要留意它与复制命令的区别。cp命令是用于文件的复制操作，文件个数是增加的，而mv则为剪切操作，也就是对文件进行移动（搬家）操作，文件位置发生变化，但总个数并无增加。

在同一个目录内对文件进行剪切的操作，实际应理解成重命名操作。

**语法格式：**mv [参数] 源文件 目标文件

**常用参数：**

| -i   | 若存在同名文件，则向用户询问是否覆盖                         |
| ---- | ------------------------------------------------------------ |
| -f   | 覆盖已有文件时，不进行任何提示                               |
| -b   | 当文件存在时，覆盖前为其创建一个备份                         |
| -u   | 当源文件比目标文件新，或者目标文件不存在时，才执行移动此操作 |

```shell
1. 重命名文件
mv   源文件    目标文件 
# 目标文件不存在, 则会自动创建目标文件 , 就是重命名文件
# 目标文件存在, 则会报错

2. 把文件移动到目录内 
mv   源文件    目录 
# 目录不存在 , 会把目录当做一个新的文件名, 进行重命名文件
# 目录存在  , 会把文件移动到目录内 

3. 把目录复制到目录 
mv   源目录  目的目录 
# 目的目录不存在, 则重命名目录
# 目的目录存在, 则会把源目录移动到目的目录内
```

**参考实例**

```shell
linux@ubuntu:~/work$ ll
总用量 28592
drwxrwxr-x  2 linux linux    4096 8月  24 15:59 ./
drwxr-xr-x 23 linux linux    4096 8月  24 16:02 ../
-rwxrw-r--  1 linux linux 9756631 8月  12 16:11 01-C语言基础.pdf*
-rwxrw-r--  1 linux linux 9756631 8月  24 15:58 02-C语言基础.pdf*
-rwxrw-r--  1 linux linux 9756631 8月  24 15:59 hello*
linux@ubuntu:~/work$ mv hello 03-C语言基础.pdf   # 同一个目录使用mv 就是重命名文件
linux@ubuntu:~/work$ ls
01-C语言基础.pdf  02-C语言基础.pdf  03-C语言基础.pdf
linux@ubuntu:~/work$ ll
总用量 28592
drwxrwxr-x  2 linux linux    4096 8月  24 16:11 ./
drwxr-xr-x 23 linux linux    4096 8月  24 16:02 ../
-rwxrw-r--  1 linux linux 9756631 8月  12 16:11 01-C语言基础.pdf*
-rwxrw-r--  1 linux linux 9756631 8月  24 15:58 02-C语言基础.pdf*
-rwxrw-r--  1 linux linux 9756631 8月  24 15:59 03-C语言基础.pdf*
linux@ubuntu:~/work$ 
linux@ubuntu:~/work$ mv 03-C语言基础.pdf 03-C语言基础.pdf 
mv: '03-C语言基础.pdf' 与'03-C语言基础.pdf' 为同一文件
linux@ubuntu:~/work$ 

linux@ubuntu:~/work$ ls
01-C语言基础.pdf  02-C语言基础.pdf  03-C语言基础.pdf
linux@ubuntu:~/work$ mkdir hello
linux@ubuntu:~/work$ mv 01-C语言基础.pdf hello/   # 如果目录存在, 则移动到目录内
linux@ubuntu:~/work$ ls hello/
01-C语言基础.pdf
linux@ubuntu:~/work$ rm -rf hello/   
linux@ubuntu:~/work$ ls
02-C语言基础.pdf  03-C语言基础.pdf
linux@ubuntu:~/work$ mv 02-C语言基础.pdf hello   # 如果目录不存在则把文件重命名 
linux@ubuntu:~/work$ ls
03-C语言基础.pdf  hello

linux@ubuntu:~$ mv work/ work1  # 目的目录不存在, 重命名
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work1
linux@ubuntu:~$ mv work1/ work 
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ mv work/ Desktop/  # 目的目录存在, 移动到目的目录内
linux@ubuntu:~$ ls 
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos
linux@ubuntu:~$ ls Desktop/
work
linux@ubuntu:~$ 
linux@ubuntu:~$ mv Desktop/work/ . 
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ 

```



### 8.5 归档与压缩

​	用户在进行数据备份时，需要把若干文件整合为一个文件以便保存。尽管整合为一个文件进行管理，但文件大小仍然没变。若需要网络传输文件时，就希望将其压缩成较小的文件，以节省在网络传输的时间。因此本节介绍文件的归档与压缩。

#### 1. **文件压缩和归档**

首先需要明确两个概念：

**归档文件**是将一组文件或目录保存在一个文件中。

**压缩文件**也是将一组文件或目录保存一个文件中，并按照某种存储格式保存在磁盘上，所占磁盘空间比其中所有文件总和要少。

因此，归档文件仍是没有经过压缩的，它所使用的磁盘空间仍等于其所有文件的总和。因而，用户可以将归档文件再进行压缩，使其容量更小。

gzip是Linux中最流行的压缩工具，具有很好的移植性，可在很多不同架构的系统中使用。bzip2在性能上优于gzip，提供了最大限度的压缩比率。如果用户需要经常在Linux和微软Windows间交换文件，建议使用zip。 

常用的压缩方式有 : zip , rar , tgz , tbz , txz , 7z 

#### 2. zip 压缩与解压

​	zip命令的功能是用于压缩文件，解压命令为unzip。通过zip命令可以将文件打包成.zip格式的压缩包，里面会附含文件的名称、路径、创建时间、上次修改时间等等信息，与tar命令相似。

**语法格式：**zip 参数 文件

**常用参数：**

| -q             | 不显示指令执行过程                               |
| -------------- | ------------------------------------------------ |
| -r             | 递归处理，将指定目录下的所有文件和子目录一并处理 |
| -z             | 替压缩文件加上注释                               |
| -v             | 显示指令执行过程或显示版本信息                   |
| -d             | 更新压缩包内文件                                 |
| -n<字尾字符串> | 不压缩具有特定字尾字符串的文件                   |

```shell
zip -r work.zip work/      # 压缩文件 
unzip work.zip             # 解压文件
```

**参考实例**

将指定目录及其内全部文件都打包成zip格式压缩包文件：

```shell
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ zip -r work.zip work/      # 压缩文件 
  adding: work/ (stored 0%)
  adding: work/hello/ (stored 0%)
  adding: work/1.txt (stored 0%)
  adding: work/01-C语言基础.pdf (deflated 4%)
linux@ubuntu:~$ 
linux@ubuntu:~$ du -h work     # 计算目录的大小 
4.0K	work/hello
9.4M	work
linux@ubuntu:~$ du -h work.zip    # 查看压缩文件的大小 
9.0M	work.zip
linux@ubuntu:~$ ls -l work.zip    # 详细查看压缩文件的大小
-rw-rw-r-- 1 linux linux 9374693 8月  24 14:53 work.zip
linux@ubuntu:~$ rm -rf work 
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work.zip
linux@ubuntu:~$ unzip work.zip 
Archive:  work.zip
   creating: work/
   creating: work/hello/
 extracting: work/1.txt              
  inflating: work/01-C语言基础.pdf  
linux@ubuntu:~$ 


```

#### 3. tar 压缩与解压

​	tar命令的功能是用于压缩和解压缩文件，能够制作出Linux系统中常见的.tar、.tar.gz、.tar.bz2等格式的压缩包文件。

把要传输的文件先进行压缩再进行传输，能够很好的提高工作效率，方便分享。

**语法格式：**tar 参数 文件或目录

**常用参数：**

| -c   | 建立新的备份文件                                             |
| ---- | ------------------------------------------------------------ |
| -x   | 从归档文件中提取文件                                         |
| -t   | 列出备份文件的内容                                           |
| -z   | 通过gzip指令压缩/解压缩文件，文件名最好为*.tgz               |
| -v   | 显示指令执行过程                                             |
| -r   | 添加文件到已经压缩的文件                                     |
| -j   | 通过bzip2指令压缩/解压缩文件，文件名最好为*.tbz              |
| -J   | 通过xz指令压缩/解压缩文件，文件名最好为*.xz, 目前最流行的方式 |

```shell
tar cvzf work.tgz work   # 安装gzip的压缩方式压缩文件 
tar cvjf work.tbz work   # 按照bzip的压缩方式进行压缩
tar cvJf work.xz work    # xz 的压缩方式 
tar xvf work.tgz   # 统一使用 tar xvf 解压
tar xvf work.tbz   # 统一使用 tar xvf 解压 
tar xvf work.xz  # 统一使用 tar xvf 解压 
```

**实例**

使用gzip压缩格式对某个目录进行打包操作，显示压缩过程，压缩包规范后缀为.tgz：

```shell
linux@ubuntu:~$ tar cvzf work.tgz work   # 安装gzip的压缩方式压缩文件 
work/
work/01-C语言基础.pdf
linux@ubuntu:~$ ls -l work.tgz  work.zip 
-rw-rw-r-- 1 linux linux 9374322 8月  24 15:07 work.tgz
-rw-rw-r-- 1 linux linux 9374395 8月  24 15:07 work.zip
linux@ubuntu:~$ 
linux@ubuntu:~$ ls -l work.tgz  work.zip 
-rw-rw-r-- 1 linux linux 9374322 8月  24 15:07 work.tgz
-rw-rw-r-- 1 linux linux 9374395 8月  24 15:07 work.zip
linux@ubuntu:~$ tar cvjf work.tbz work   # 按照bzip的压缩方式进行压缩
work/
work/01-C语言基础.pdf
linux@ubuntu:~$ ll work.* 
-rw-rw-r-- 1 linux linux 9471113 8月  24 15:09 work.tbz
-rw-rw-r-- 1 linux linux 9374322 8月  24 15:07 work.tgz
-rw-rw-r-- 1 linux linux 9374395 8月  24 15:07 work.zip
linux@ubuntu:~$ 
linux@ubuntu:~$ tar cvJf work.xz work    # xz 的压缩方式 
work/
work/01-C语言基础.pdf
linux@ubuntu:~$ ll work.*
-rw-rw-r-- 1 linux linux 9471113 8月  24 15:09 work.tbz  # bzip 压缩最差
-rw-rw-r-- 1 linux linux 9374322 8月  24 15:07 work.tgz
-rw-rw-r-- 1 linux linux 9342076 8月  24 15:13 work.xz   # xz压缩最好, 也最慢
-rw-rw-r-- 1 linux linux 9374395 8月  24 15:07 work.zip  # zip 压缩最快, 压缩率一般 
linux@ubuntu:~$ 
linux@ubuntu:~$ tar xvf work.tgz   # 统一使用 tar xvf 解压 
work/
work/01-C语言基础.pdf
linux@ubuntu:~$ rm -rf work
linux@ubuntu:~$ tar xvf work.tbz   # 统一使用 tar xvf 解压 
work/
work/01-C语言基础.pdf
linux@ubuntu:~$ rm -rf work
linux@ubuntu:~$ tar xvf work.xz  # 统一使用 tar xvf 解压 
work/
work/01-C语言基础.pdf
linux@ubuntu:~$ 
```

#### 4. 7z 压缩与解压

​	7z命令来自于英文词组“7-zip”的缩写，其功能是用于文件解压缩工作。7-zip应该是Linux系统中常用的解压缩工具了，7z是一种压缩格式，具备较高的压缩比率，对文本文件尤其有效。

**语法格式：**7z [参数] 文件

**常用参数：**

| a    | 添加文件到压缩包   |
| ---- | ------------------ |
| d    | 从压缩包中删除文件 |
| e    | 从压缩包中提取文件 |
| t    | 测试压缩包是否完整 |
| u    | 更新压缩包中的文件 |

```shell
7z a work.7z work   # 压缩命令
7z x work.7z        # 解压文件
```

**参考实例**

```shell
linux@ubuntu:~$ 7z a work.7z work 

Command '7z' not found, but can be installed with:

sudo apt install p7zip-full

linux@ubuntu:~$ sudo apt install p7zip-full    # 默认系统没有安装7z压缩, 需要额外安装 
[sudo] linux 的密码： 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
将会同时安装下列软件：
  p7zip
建议安装：
  p7zip-rar
下列【新】软件包将被安装：
  p7zip p7zip-full
升级了 0 个软件包，新安装了 2 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
需要下载 1,522 kB 的归档。
解压缩后会消耗 5,755 kB 的额外空间。
您希望继续执行吗？ [Y/n] y
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 p7zip amd64 16.02+dfsg-6 [358 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 p7zip-full amd64 16.02+dfsg-6 [1,164 kB]
已下载 1,522 kB，耗时 0秒 (3,103 kB/s)
正在选中未选择的软件包 p7zip。
(正在读取数据库 ... 系统当前共安装有 162585 个文件和目录。)
正准备解包 .../p7zip_16.02+dfsg-6_amd64.deb  ...
正在解包 p7zip (16.02+dfsg-6) ...
正在选中未选择的软件包 p7zip-full。
正准备解包 .../p7zip-full_16.02+dfsg-6_amd64.deb  ...
正在解包 p7zip-full (16.02+dfsg-6) ...
正在设置 p7zip (16.02+dfsg-6) ...
正在设置 p7zip-full (16.02+dfsg-6) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
linux@ubuntu:~$ 7z a work.7z work 

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=zh_CN.UTF-8,Utf16=on,HugeFiles=on,64 bits,4 CPUs 11th Gen Intel(R) Core(TM) i5-1135G7 @ 2.40GHz (806C1),ASM,AES-NI)

Scanning the drive:
1 folder, 1 file, 9756631 bytes (9528 KiB)

Creating archive: work.7z

Items to compress: 2

                                  
Files read from disk: 1
Archive size: 9341587 bytes (9123 KiB)
Everything is Ok
linux@ubuntu:~$ 
linux@ubuntu:~$ ll work.*
-rw-rw-r-- 1 linux linux 9341587 8月  24 15:20 work.7z    # 7z 压缩率最高, 也最慢 
-rw-rw-r-- 1 linux linux 9471113 8月  24 15:09 work.tbz
-rw-rw-r-- 1 linux linux 9374322 8月  24 15:07 work.tgz
-rw-rw-r-- 1 linux linux 9342076 8月  24 15:13 work.xz
-rw-rw-r-- 1 linux linux 9374395 8月  24 15:07 work.zip
linux@ubuntu:~$ 
linux@ubuntu:~$ 7z x work.7z    # 解压文件

7-Zip [64] 16.02 : Copyright (c) 1999-2016 Igor Pavlov : 2016-05-21
p7zip Version 16.02 (locale=zh_CN.UTF-8,Utf16=on,HugeFiles=on,64 bits,4 CPUs 11th Gen Intel(R) Core(TM) i5-1135G7 @ 2.40GHz (806C1),ASM,AES-NI)

Scanning the drive for archives:
1 file, 9341587 bytes (9123 KiB)

Extracting archive: work.7z
--
Path = work.7z
Type = 7z
Physical Size = 9341587
Headers Size = 195
Method = LZMA2:12m
Solid = -
Blocks = 1

Everything is Ok                  

Folders: 1
Files: 1
Size:       9756631
Compressed: 9341587
linux@ubuntu:~$ 
```

### 8.6 文件比较与补丁

#### 1. diff 比较文件

​	diff命令来自于英文单词different的缩写，其功能是用于比较文件内容差异。如果有多个内容相近的文件，如何快速定位到不同内容所在位置？此时用diff命令就再合适不过了！

**语法格式：**diff [参数] 文件1 文件2

**常用参数：**

| -a                      | 逐行比较文本文件                                       |
| ----------------------- | ------------------------------------------------------ |
| -b                      | 不检查空格字符的不同                                   |
| -W                      | 指定栏宽                                               |
| -x                      | 不比较选项中所指定的文件或目录                         |
| -X                      | 将文件或目录类型存成文本文件                           |
| -y                      | 以并列的方式显示文件的异同之处                         |
| --brief                 | 仅判断两个文件是否不同                                 |
| --help                  | 查看帮助信息                                           |
| --left-column           | 若两个文件某一行内容相同，则仅在左侧的栏位显示该行内容 |
| --suppress-common-lines | 在使用-y参数时，仅显示不同之处                         |

**参考实例**

```shell
linux@ubuntu:~/work$ ls
99.c
linux@ubuntu:~/work$ cp 99.c 100.c
linux@ubuntu:~/work$ ls
100.c  99.c
linux@ubuntu:~/work$ diff 99.c 100.c   #判断2个文件是否相同, 相同没有输出, 不同会告诉不同之处 
linux@ubuntu:~/work$ vi 100.c 
linux@ubuntu:~/work$ diff 99.c 100.c   # 后面的文件比前面的文件 多(>) 还是少(<)
11a12
>         printf("\n");  # 100.c 比 99.c 多的内容
12a14
>     return 0;
linux@ubuntu:~/work$ diff 100.c 99.c
12d11
<         printf("\n");   # 99.c 比 100.c 少的内容
14d12
<     return 0;
linux@ubuntu:~/work$ diff -c 99.c 100.c    # 100.c 比 99.c 多(+) 少(-)
*** 99.c	2022-08-02 11:02:02.482386000 +0800
--- 100.c	2022-08-25 08:53:35.968734345 +0800
***************
*** 9,14 ****
--- 9,16 ----
              printf("%d x %d = %-2d  ",j,i,i*j);
          }
          printf("\n");
+         printf("\n");
      }
      return 0;
+     return 0;
  }
linux@ubuntu:~/work$ 
linux@ubuntu:~/work$ diff -c 100.c 99.c   # 99.c 比 100.c 少(-)
*** 100.c	2022-08-25 08:53:35.968734345 +0800
--- 99.c	2022-08-02 11:02:02.482386000 +0800
***************
*** 9,16 ****
              printf("%d x %d = %-2d  ",j,i,i*j);
          }
          printf("\n");
-         printf("\n");
      }
      return 0;
-     return 0;
  }
--- 9,14 ----
linux@ubuntu:~/work$ 

```

#### 2. diff 生成补丁

​	diff 除了可以比较文件外, 也可以用于生成补丁文件 

```shell
diff  -uNr 1.1.c 1.2.c  >1.1_1.2.patch   # 生成补丁, 1.2.c是升级的版本, 1.1.c要升级的版本 
```

例如

```shell
linux@ubuntu:~/work$ ls
100.c  99.c
linux@ubuntu:~/work$ vi 100.c 
linux@ubuntu:~/work$ mv 99.c 1.1.c
linux@ubuntu:~/work$ mv 100.c 1.2.c
linux@ubuntu:~/work$ diff -uNr 1.1.c 1.2.c >1.1_1.2.patch
linux@ubuntu:~/work$ cat 1.1_1.2.patch 
--- 1.1.c	2022-08-02 11:02:02.482386000 +0800
+++ 1.2.c	2022-08-25 09:05:51.270640423 +0800
@@ -7,8 +7,11 @@
         for(int j=1;j<i+1;j++) // 控制列数 
         {
             printf("%d x %d = %-2d  ",j,i,i*j);
+            printf("%d x %d = %-2d  ",j,i,i*j);
         }
         printf("\n");
+        printf("\n");
     }
     return 0;
+    return 0;
 }
linux@ubuntu:~/work$ 
```

#### 3. patch 打补丁

​	patch命令让用户利用设置修补文件的方式，修改，更新原始文件。倘若一次仅修改一个文件，可直接在指令列中下达指令依序执行。如果配合修补文件的方式则能一次修补大批文件，这也是Linux系统核心的升级方法之一 。

**语法格式：**patch [参数]

```shell
patch -p0 <1.1_1.2.patch  # 1.1_1.2.patch 补丁包会自动找当前目录下的文件进行打补丁 
# -p0 : 是在同级目录进行 , 主要是确认补丁的源文件在哪一个目录下 
# -p1 : 是在子1级目录进行, 以此类推 
```

**参考实例**

```shell
linux@ubuntu:~/work$ ls
1.1_1.2.patch  1.1.c  1.2.c
linux@ubuntu:~/work$ diff -c 1.1.c 1.2.c 
*** 1.1.c	2022-08-25 09:14:04.399767049 +0800
--- 1.2.c	2022-08-25 09:05:51.270640423 +0800
***************
*** 7,14 ****
--- 7,17 ----
          for(int j=1;j<i+1;j++) // 控制列数 
          {
              printf("%d x %d = %-2d  ",j,i,i*j);
+             printf("%d x %d = %-2d  ",j,i,i*j);
          }
          printf("\n");
+         printf("\n");
      }
      return 0;
+     return 0;
  }
linux@ubuntu:~/work$ patch -p0 <1.1_1.2.patch 
patching file 1.1.c
linux@ubuntu:~/work$ diff -c 1.1.c 1.2.c 
linux@ubuntu:~/work$ 
```



## 第09章 磁盘管理

### 9.1 文件系统分类

Linux系统中颇具特色的文件存储结构常常搞得新手头昏脑涨，本章将从Linux系统中的文件存储结构开始，讲述文件系统层次标准（Filesystem Hierarchy Standard，FHS）、udev硬件命名规则以及硬盘设备的原理。

为了让读者更好地理解文件系统的作用，将在本章详细地分析Linux系统中最常见的Ext3、Ext4与XFS文件系统的不同之处，练习硬盘设备分区、格式化以及挂载等常用的硬盘管理操作，以便熟练掌握文件系统的使用方法。

Linux系统支持数十种文件系统，而最常见的文件系统如下所示。

**Ext2**：最早可追溯到1993年，是Linux系统的第一个商业级文件系统，它基本沿袭了UNIX文件系统的设计标准。但由于不包含日志读写功能，数据丢失的可能性很大，因此大家能不用就不用，或者顶多建议用于SD存储卡或U盘。

**Ext3**：是一款日志文件系统，它会把整个硬盘的每个写入动作的细节都预先记录下来，然后再进行实际操作，以便在发生异常宕机后能回溯追踪到被中断的部分。Ext3能够在系统异常宕机时避免文件系统资料丢失，并能自动修复数据的不一致与错误。然而，当硬盘容量较大时，所需的修复时间也会很长，而且也不能100%地保证资料不会丢失。

**Ext4**：Ext3的改进版本，作为linux系统中默认的文件管理系统，它支持的存储容量高达1EB（1EB=1,073,741,824GB），且能够有无限多的子目录。另外，Ext4文件系统能够批量分配block（块），从而极大地提高了读写效率。现在很多主流服务器也会使用Ext4文件系统。



### 9.2 文件系统的根(/)

​	在Linux系统中，目录、字符设备、套接字、硬盘、光驱、打印机等都被抽象成文件形式，一直强调的“Linux系统中一切都是文件”。既然平时我们打交道的都是文件，那么又应该如何找到它们呢？在Windows操作系统中，想要找到一个文件，要依次进入该文件所在的磁盘分区（也叫盘符），然后再进入该分区下的具体目录，最终找到这个文件。但是在Linux系统中并不存在C、D、E、F等盘符，Linux系统中的一切文件都是从“根”目录（/）开始的，并按照文件系统层次标准（FHS）采用倒树状结构来存放文件，以及定义了常见目录的用途。

另外，Linux系统中的文件和目录名称是严格区分大小写的。例如，root、rOOt、Root、rooT均代表不同的目录，并且文件名称中不得包含斜杠（/）。

![image-20220823152342269](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823152342269.png)

### 9.3 物理设备命名规则

​	在Linux系统中一切都是文件，硬件设备也不例外。既然是文件，就必须有文件名称。系统内核中的udev设备管理器会自动把硬件名称规范起来，目的是让用户通过设备文件的名字可以猜出设备大致的属性以及分区信息等。Linux系统中常见的硬件设备及其文件名称如图所示。

![image-20220823152945835](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823152945835.png)

​	IDE : 老式的硬盘 

   SATA :目前流行的硬盘  

​	第一个知识点是设备名称的理解错误。比如/dev/sda表示主板上第一个插槽上的存储设备，读者在实践操作的时候会发现果然如此，因此也就对这条理论知识更加深信不疑。但真相不是这样的，/dev目录中sda设备之所以是a，并不是由插槽决定的，而是由系统内核的识别顺序来决定的，而恰巧很多主板的插槽顺序就是系统内核的识别顺序，因此才会被命名为/dev/sda。大家以后在使用SCSI网络存储设备时就会发现，明明主板上第二个插槽是空着的，但系统却能识别到/dev/sdb这个设备—就是这个道理。

​	第二个知识点是对分区名称的理解错误。分区的编号代表分区的个数。比如sda3表示这是设备上的第3个分区，而学员在做实验的时候确实也会得出这样的结果。但是这个理论知识是错误的，因为分区的数字编码不一定是强制顺延下来的，也有可能是手工指定的。因此sda3只能表示是编号为3的分区，而不能判断sda设备上已经存在了3个分区。

![image-20220823153535955](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823153535955.png)

首先，/dev/目录中保存的应当是硬件设备文件；其次，sd表示的是存储设备；然后，a表示系统中同类接口中第一个被识别到的设备；最后，5表示这个设备是一个逻辑分区。一言以蔽之，“/dev/sda5”表示的就是“这是系统中第一块被识别到的硬件设备中分区编号为5的逻辑分区的设备文件”。

- 例如 :手动添加一个磁盘, 然后对磁盘分区 

```shell
->关闭虚拟机 
->编辑虚拟机设置 
->添加 
->硬盘
->SCSI(推荐)
->创建虚拟磁盘
-> 磁盘大小: 10G 
   立即分配所有磁盘空间
   将虚拟磁盘存储位单一文件
-> 磁盘命名: Ubuntu18.04-ext.vmdk  (VMware disk)
-> 确定
-> 开启虚拟机 
-> 左下角开始图标
-> 工具
-> 磁盘
-> 即可看到 我们新加的磁盘: /dev/sdb 
```

![image-20220823153846504](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823153846504.png)

![image-20220823153935920](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823153935920.png)

![image-20220823154054585](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823154054585.png)

![image-20220823154112819](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823154112819.png)

![image-20220823154146136](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823154146136.png)

![image-20220823154238970](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823154238970.png)

![image-20220823154317761](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823154317761.png)



![image-20220823154509010](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220823154509010.png)

### 9.4 fdisk 分区磁盘

#### 1. 图形化工具分区磁盘

```shell
->把u盘插入到电脑
->打开虚拟机的右下角图标
->右击u盘图标
->连接
->出现弹窗, 提示"某个USB设备将要重主机拔出并连接到该虚拟机"
->点击确定
->Ubuntu资源管理窗口会有U盘信息弹出来
->打开Ubuntu的左下角开始图标 
->工具
->磁盘
->选中 U盘 硬盘
->单击 '-' 按钮  , 进行删除分区
->弹窗中选择删除
->点击 '+' 图标, 进行添加分区
->分区大小 设置为: 2G 
->输入 分区名称 : "boot"
->选中剩余的磁盘 
->点击 "+"
->剩余空间即可 
->输入分区名称 : "rootfs"
->至此分区完成
```

![image-20220824090332384](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824090332384.png)

![image-20220824090515028](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824090515028.png)

![image-20220824091309010](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824091309010.png)

![image-20220824090935268](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824090935268.png)

![image-20220824091405019](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824091405019.png)

![image-20220824091458397](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824091458397.png)

![image-20220824091609721](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824091609721.png)

![image-20220824091647823](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824091647823.png)

![image-20220824091745706](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824091745706.png)

![image-20220824091822005](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824091822005.png)

![image-20220824091929859](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824091929859.png)

![image-20220824092108200](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824092108200.png)

![image-20220824092157840](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824092157840.png)

#### 2.  命令分区磁盘

- 同样也可以使用fdisk命令进行分区 
- 查看磁盘信息命令格式:  fdisk -l [device...]
2. 对磁盘进行分区格式化操作: fdisk device 

- 例如

```shell
linux@ubuntu:~$ fdisk -l  /dev/sda
fdisk: 打不开 /dev/sda: 权限不够
linux@ubuntu:~$ sudo fdisk -l  /dev/sda
[sudo] linux 的密码： 
Disk /dev/sda：50 GiB，53687091200 字节，104857600 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xe29ed0d4

设备       启动  起点      末尾      扇区 大小 Id 类型
/dev/sda1  *     2048 104855551 104853504  50G 83 Linux
linux@ubuntu:~$ sudo fdisk -l  /dev/sdb
Disk /dev/sdb：10 GiB，10737418240 字节，20971520 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x00000000
linux@ubuntu:~$

linux@ubuntu:~$ sudo fdisk  /dev/sdb 

欢迎使用 fdisk (util-linux 2.31.1)。
更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。


命令(输入 m 获取帮助)： m

帮助：

  DOS (MBR)
   a   开关 可启动 标志
   b   编辑嵌套的 BSD 磁盘标签
   c   开关 dos 兼容性标志

  常规
   d   删除分区
   F   列出未分区的空闲区
   l   列出已知分区类型
   n   添加新分区
   p   打印分区表
   t   更改分区类型
   v   检查分区表
   i   打印某个分区的相关信息

  杂项
   m   打印此菜单
   u   更改 显示/记录 单位
   x   更多功能(仅限专业人员)

  脚本
   I   从 sfdisk 脚本文件加载磁盘布局
   O   将磁盘布局转储为 sfdisk 脚本文件

  保存并退出
   w   将分区表写入磁盘并退出
   q   退出而不保存更改

  新建空磁盘标签
   g   新建一份 GPT 分区表
   G   新建一份空 GPT (IRIX) 分区表
   o   新建一份的空 DOS 分区表
   s   新建一份空 Sun 分区表


命令(输入 m 获取帮助)： l

 0  空              24  NEC DOS         81  Minix / 旧 Linu bf  Solaris        
 1  FAT12           27  隐藏的 NTFS Win 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 隐藏 或 In c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux 扩展      c7  Syrinx         
 5  扩展            41  PPC PReP Boot   86  NTFS 卷集       da  非文件系统数据 
 6  FAT16           42  SFS             87  NTFS 卷集       db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux 纯文本    de  Dell 工具      
 8  AIX             4e  QNX4.x 第2部分  8e  Linux LVM       df  BootIt         
 9  AIX 可启动      4f  QNX4.x 第3部分  93  Amoeba          e1  DOS 访问       
 a  OS/2 启动管理器 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O        
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor      
 c  W95 FAT32 (LBA) 52  CP/M            a0  IBM Thinkpad 休 ea  Rufus 对齐     
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         eb  BeOS fs        
 f  W95 扩展 (LBA)  54  OnTrackDM6      a6  OpenBSD         ee  GPT            
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        ef  EFI (FAT-12/16/
11  隐藏的 FAT12    56  Golden Bow      a8  Darwin UFS      f0  Linux/PA-RISC  
12  Compaq 诊断     5c  Priam Edisk     a9  NetBSD          f1  SpeedStor      
14  隐藏的 FAT16 <3 61  SpeedStor       ab  Darwin 启动     f4  SpeedStor      
16  隐藏的 FAT16    63  GNU HURD 或 Sys af  HFS / HFS+      f2  DOS 次要       
17  隐藏的 HPFS/NTF 64  Novell Netware  b7  BSDI fs         fb  VMware VMFS    
18  AST 智能睡眠    65  Novell Netware  b8  BSDI swap       fc  VMware VMKCORE 
1b  隐藏的 W95 FAT3 70  DiskSecure 多启 bb  Boot Wizard 隐  fd  Linux raid 自动
1c  隐藏的 W95 FAT3 75  PC/IX           bc  Acronis FAT32 L fe  LANstep        
1e  隐藏的 W95 FAT1 80  旧 Minix        be  Solaris 启动    ff  BBT            

命令(输入 m 获取帮助)： F
未分区的空间 /dev/sdb：10 GiB，10736369664 个字节，20969472 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节

 起点     末尾     扇区 大小
 2048 20971519 20969472  10G

命令(输入 m 获取帮助)： p
Disk /dev/sdb：10 GiB，10737418240 字节，20971520 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x00000000

命令(输入 m 获取帮助)： n
分区类型
   p   主分区 (0个主分区，0个扩展分区，4空闲)
   e   扩展分区 (逻辑分区容器)
选择 (默认 p)： p
分区号 (1-4, 默认  1): 1
第一个扇区 (2048-20971519, 默认 2048): 
上个扇区，+sectors 或 +size{K,M,G,T,P} (2048-20971519, 默认 20971519): 2048*2048          
上个扇区，+sectors 或 +size{K,M,G,T,P} (2048-20971519, 默认 20971519): 4194304

创建了一个新分区 1，类型为“Linux”，大小为 2 GiB。

命令(输入 m 获取帮助)： t
已选择分区 1
Hex 代码(输入 L 列出所有代码)： b
已将分区“Linux”的类型更改为“W95 FAT32”。

命令(输入 m 获取帮助)： p
Disk /dev/sdb：10 GiB，10737418240 字节，20971520 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x00000000

设备       启动  起点    末尾    扇区 大小 Id 类型
/dev/sdb1        2048 4194304 4192257   2G  b W95 FAT32

命令(输入 m 获取帮助)： n
分区类型
   p   主分区 (1个主分区，0个扩展分区，3空闲)
   e   扩展分区 (逻辑分区容器)
选择 (默认 p)： p
分区号 (2-4, 默认  2): 2
第一个扇区 (4194305-20971519, 默认 4196352): 
上个扇区，+sectors 或 +size{K,M,G,T,P} (4196352-20971519, 默认 20971519): 

创建了一个新分区 2，类型为“Linux”，大小为 8 GiB。

命令(输入 m 获取帮助)： p
Disk /dev/sdb：10 GiB，10737418240 字节，20971520 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x00000000

设备       启动    起点     末尾     扇区 大小 Id 类型
/dev/sdb1          2048  4194304  4192257   2G  b W95 FAT32
/dev/sdb2       4196352 20971519 16775168   8G 83 Linux

命令(输入 m 获取帮助)： m

帮助：

  DOS (MBR)
   a   开关 可启动 标志
   b   编辑嵌套的 BSD 磁盘标签
   c   开关 dos 兼容性标志

  常规
   d   删除分区
   F   列出未分区的空闲区
   l   列出已知分区类型
   n   添加新分区
   p   打印分区表
   t   更改分区类型
   v   检查分区表
   i   打印某个分区的相关信息

  杂项
   m   打印此菜单
   u   更改 显示/记录 单位
   x   更多功能(仅限专业人员)

  脚本
   I   从 sfdisk 脚本文件加载磁盘布局
   O   将磁盘布局转储为 sfdisk 脚本文件

  保存并退出
   w   将分区表写入磁盘并退出
   q   退出而不保存更改

  新建空磁盘标签
   g   新建一份 GPT 分区表
   G   新建一份空 GPT (IRIX) 分区表
   o   新建一份的空 DOS 分区表
   s   新建一份空 Sun 分区表


命令(输入 m 获取帮助)： w
分区表已调整。
将调用 ioctl() 来重新读分区表。
正在同步磁盘。

linux@ubuntu:~$ sudo fdisk  -l /dev/sdb 
Disk /dev/sdb：10 GiB，10737418240 字节，20971520 个扇区
单元：扇区 / 1 * 512 = 512 字节
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x00000000

设备       启动    起点     末尾     扇区 大小 Id 类型
/dev/sdb1          2048  4194304  4192257   2G  b W95 FAT32
/dev/sdb2       4196352 20971519 16775168   8G 83 Linux
```

### 9.5 mkfs  格式化磁盘

​	mkfs命令来自于英文词组“make file system”的缩写，其功能是用于对设备进行格式化文件系统操作。在挂载使用硬盘空间前的最后一步，运维人员需要对整块硬盘或指定分区进行格式化文件系统操作，Linux系统支持的文件系统包含ext2、ext3、ext4、xfs、fat、msdos、vfat、minix等多种格式。

**语法格式：** mkfs [参数] 设备名

**常用参数：**

| -V   | 详细显示模式         |
| ---- | -------------------- |
| -t   | 给定档案系统的型式   |
| -c   | 检查该设备是否有损坏 |

**实例**

```shell
linux@ubuntu:~$ mkfs<TAB><TAB>
mkfs         mkfs.bfs     mkfs.cramfs  mkfs.ext2    mkfs.ext3    mkfs.ext4    mkfs.fat     mkfs.minix   mkfs.msdos   mkfs.ntfs    mkfs.vfat
linux@ubuntu:~$ mkfs.fat /dev/sd<TAB>
sda   sda1  sdb   sdb1  sdb2  sdc   sdc1  sdc2  
linux@ubuntu:~$ mkfs.fat /dev/sdb
sdb   sdb1  sdb2  
linux@ubuntu:~$ mkfs.fat /dev/sdb1
mkfs.fat 4.1 (2017-01-24)
mkfs.fat: unable to open /dev/sdb1: Permission denied
linux@ubuntu:~$ sudo mkfs.fat /dev/sdb1
[sudo] linux 的密码： 
mkfs.fat 4.1 (2017-01-24)
linux@ubuntu:~$ sudo mkfs.ext4 /dev/sdb2
mke2fs 1.44.1 (24-Mar-2018)
创建含有 2096896 个块（每块 4k）和 524288 个inode的文件系统
文件系统UUID：7b6ac31f-7a8c-4bb6-ba2b-fdda970bce63
超级块的备份存储于下列块： 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

正在分配组表： 完成                            
正在写入inode表： 完成                            
创建日志（16384 个块） 完成
写入超级块和文件系统账户统计信息： 已完成

linux@ubuntu:~$ 
```

格式化完成后，通过磁盘工具查看

![image-20220824103512731](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824103512731.png)

### 9.6 mount 挂载与umount卸载设备 

​	mount命令的功能是用于把文件系统挂载到目录，文件系统指的是被格式化过的硬盘或分区设备，进行挂载操作后，用户便可以在挂载目录中使用硬盘资源了。

默认情况下Linux系统并不会像Windows系统那样自动的挂载光盘和U盘设备，需要自行完成。

**挂载设备语法格式**：mount [参数] [设备] [挂载点]﻿

**卸载设备语法格式**：umount  [挂载点]﻿

**挂载点**：就是一个目录 

**参考实例**

```shell
linux@ubuntu:~$ ls /mnt/
linux@ubuntu:~$ sudo mkdir /mnt/linux
linux@ubuntu:~$ ls /mnt/
linux
linux@ubuntu:~$ mount /dev/sdb1  /mnt/linux/
mount: 只有 root 能执行该操作
linux@ubuntu:~$ sudo mount /dev/sdb1  /mnt/linux/
linux@ubuntu:~$ ls /mnt/linux/
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cp work/
1.txt  hello/ 
linux@ubuntu:~$ touch /mnt/linux/1.txt
touch: 无法创建'/mnt/linux/1.txt': 权限不够
linux@ubuntu:~$ sudo touch /mnt/linux/1.txt
linux@ubuntu:~$ ls /mnt/linux/
1.txt
linux@ubuntu:~$ sudo mkdir /mnt/rootfs
linux@ubuntu:~$ sudo mount /dev/sdb2 /mnt/rootfs/
linux@ubuntu:~$ ls /mnt/rootfs/
lost+found
linux@ubuntu:~$ umount /dev/sdb1
umount: /mnt/linux: umount failed: 不允许的操作.
linux@ubuntu:~$ umount /mnt/linux 
umount: /mnt/linux: umount failed: 不允许的操作.
linux@ubuntu:~$ sudo umount /mnt/linux 
linux@ubuntu:~$ sudo umount /mnt/rootfs 
linux@ubuntu:~$ sudo mount /dev/sdb2 /mnt/rootfs/
linux@ubuntu:~$ sudo mount /dev/sdb1  /mnt/linux/
linux@ubuntu:~$ sudo umount /mnt/rootfs 
linux@ubuntu:~$ sudo umount /mnt/linux 
linux@ubuntu:~$ 
```

### 9.7 fatlabel e2label 修改盘符 

e2label 命令用来设置ext2，ext3，ext4文件系统的卷标， 卷标就是盘符。

fatlabel 命令用来设置fat文件系统的卷标

ntfslabel 命令用来设置ntfs文件系统的卷标

实例

```shell
linux@ubuntu:~$ fatlabel /dev/sdb1 hello    # fat 文件系统要求盘符是大写字符， 如果小写会有兼容问题
fatlabel: warning - lowercase labels might not work properly with DOS or Windows
open: Permission denied
linux@ubuntu:~$ sudo fatlabel /dev/sdb1 hello # fat 文件系统要求盘符是大写字符， 如果小写会有兼容问题
fatlabel: warning - lowercase labels might not work properly with DOS or Windows
linux@ubuntu:~$ sudo fatlabel /dev/sdb1 HELLO
linux@ubuntu:~$ sudo e2label /dev/sdb2 kernel
linux@ubuntu:~$ sudo fatlabel /dev/sdb1 BOOT
linux@ubuntu:~$ sudo e2label /dev/sdb2 rootfs
linux@ubuntu:~$ sudo e2label /dev/sdb2 
rootfs
linux@ubuntu:~$ sudo fatlabel /dev/sdb1 
BOOT       
linux@ubuntu:~$ sudo e2label /dev/sdb1
e2label: 超级块中的幻数有错 尝试打开 /dev/sdb1 时
/dev/sdb1 有一个标签为“BOOT”的 vfat 文件系统
linux@ubuntu:~$ sudo fatlabel /dev/sdb1
BOOT       
linux@ubuntu:~$ 

```

### 9.8 df 查看磁盘 

​	df命令来自于英文词组”Disk Free“的缩写，其功能是用于显示系统上磁盘空间的使用量情况。df命令显示的磁盘使用量情况含可用、已有及使用率等信息，默认单位为Kb，建议使用-h参数进行单位换算，毕竟135M比138240Kb更利于阅读对吧。

**语法格式：** df [参数] [对象磁盘/分区]

**常用参数：**

| -a          | 显示所有系统文件     |
| ----------- | -------------------- |
| -B <块大小> | 指定显示时的块大小   |
| -h          | 以容易阅读的方式显示 |

**参考实例**

```shell
linux@ubuntu:~$ df
文件系统          1K-块    已用     可用 已用% 挂载点
udev             974532       0   974532    0% /dev
tmpfs            199940    1680   198260    1% /run
/dev/sda1      51288544 9294812 39356012   20% /
tmpfs            999684       0   999684    0% /dev/shm
tmpfs              5120       4     5116    1% /run/lock
tmpfs            999684       0   999684    0% /sys/fs/cgroup
/dev/loop1        56832   56832        0  100% /snap/core18/2128
/dev/loop2         2560    2560        0  100% /snap/gnome-calculator/884
/dev/loop0          128     128        0  100% /snap/bare/5
/dev/loop3         2688    2688        0  100% /snap/gnome-system-monitor/178
/dev/loop4       224256  224256        0  100% /snap/gnome-3-34-1804/72
/dev/loop5         2560    2560        0  100% /snap/gnome-system-monitor/163
/dev/loop6          768     768        0  100% /snap/gnome-characters/726
/dev/loop7          768     768        0  100% /snap/gnome-characters/741
/dev/loop8         2688    2688        0  100% /snap/gnome-calculator/920
/dev/loop9        63488   63488        0  100% /snap/core20/1611
/dev/loop10         640     640        0  100% /snap/gnome-logs/112
/dev/loop11       48128   48128        0  100% /snap/snapd/16292
/dev/loop12       93952   93952        0  100% /snap/gtk-common-themes/1535
/dev/loop13       66688   66688        0  100% /snap/gtk-common-themes/1515
/dev/loop14      247168  247168        0  100% /snap/gnome-3-38-2004/70
/dev/loop15       63488   63488        0  100% /snap/core20/1593
/dev/loop16       56960   56960        0  100% /snap/core18/2538
/dev/loop17      410496  410496        0  100% /snap/gnome-3-38-2004/112
/dev/loop18         640     640        0  100% /snap/gnome-logs/106
/dev/loop19      224256  224256        0  100% /snap/gnome-3-34-1804/77
tmpfs            199936      40   199896    1% /run/user/1000
linux@ubuntu:~$ df -h    # 便于阅读的方式查看磁盘使用情况
文件系统        容量  已用  可用 已用% 挂载点
udev            952M     0  952M    0% /dev
tmpfs           196M  1.7M  194M    1% /run
/dev/sda1        49G  8.9G   38G   20% /
tmpfs           977M     0  977M    0% /dev/shm
tmpfs           5.0M  4.0K  5.0M    1% /run/lock
tmpfs           977M     0  977M    0% /sys/fs/cgroup
/dev/loop1       56M   56M     0  100% /snap/core18/2128
/dev/loop2      2.5M  2.5M     0  100% /snap/gnome-calculator/884
/dev/loop0      128K  128K     0  100% /snap/bare/5
/dev/loop3      2.7M  2.7M     0  100% /snap/gnome-system-monitor/178
/dev/loop4      219M  219M     0  100% /snap/gnome-3-34-1804/72
/dev/loop5      2.5M  2.5M     0  100% /snap/gnome-system-monitor/163
/dev/loop6      768K  768K     0  100% /snap/gnome-characters/726
/dev/loop7      768K  768K     0  100% /snap/gnome-characters/741
/dev/loop8      2.7M  2.7M     0  100% /snap/gnome-calculator/920
/dev/loop9       62M   62M     0  100% /snap/core20/1611
/dev/loop10     640K  640K     0  100% /snap/gnome-logs/112
/dev/loop11      47M   47M     0  100% /snap/snapd/16292
/dev/loop12      92M   92M     0  100% /snap/gtk-common-themes/1535
/dev/loop13      66M   66M     0  100% /snap/gtk-common-themes/1515
/dev/loop14     242M  242M     0  100% /snap/gnome-3-38-2004/70
/dev/loop15      62M   62M     0  100% /snap/core20/1593
/dev/loop16      56M   56M     0  100% /snap/core18/2538
/dev/loop17     401M  401M     0  100% /snap/gnome-3-38-2004/112
/dev/loop18     640K  640K     0  100% /snap/gnome-logs/106
/dev/loop19     219M  219M     0  100% /snap/gnome-3-34-1804/77
tmpfs           196M   40K  196M    1% /run/user/1000
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo mount /dev/sdb1 /mnt/linux/   # 挂载 sdb1分区
[sudo] linux 的密码： 
linux@ubuntu:~$ sudo mount /dev/sdb2  /mnt/rootfs/    # 挂载 sdb2分区
linux@ubuntu:~$ df -h
文件系统        容量  已用  可用 已用% 挂载点
udev            952M     0  952M    0% /dev
tmpfs           196M  1.7M  194M    1% /run
/dev/sda1        49G  8.9G   38G   20% /
tmpfs           977M     0  977M    0% /dev/shm
tmpfs           5.0M  4.0K  5.0M    1% /run/lock
tmpfs           977M     0  977M    0% /sys/fs/cgroup
/dev/loop1       56M   56M     0  100% /snap/core18/2128
/dev/loop2      2.5M  2.5M     0  100% /snap/gnome-calculator/884
/dev/loop0      128K  128K     0  100% /snap/bare/5
/dev/loop3      2.7M  2.7M     0  100% /snap/gnome-system-monitor/178
/dev/loop4      219M  219M     0  100% /snap/gnome-3-34-1804/72
/dev/loop5      2.5M  2.5M     0  100% /snap/gnome-system-monitor/163
/dev/loop6      768K  768K     0  100% /snap/gnome-characters/726
/dev/loop7      768K  768K     0  100% /snap/gnome-characters/741
/dev/loop8      2.7M  2.7M     0  100% /snap/gnome-calculator/920
/dev/loop9       62M   62M     0  100% /snap/core20/1611
/dev/loop10     640K  640K     0  100% /snap/gnome-logs/112
/dev/loop11      47M   47M     0  100% /snap/snapd/16292
/dev/loop12      92M   92M     0  100% /snap/gtk-common-themes/1535
/dev/loop13      66M   66M     0  100% /snap/gtk-common-themes/1515
/dev/loop14     242M  242M     0  100% /snap/gnome-3-38-2004/70
/dev/loop15      62M   62M     0  100% /snap/core20/1593
/dev/loop16      56M   56M     0  100% /snap/core18/2538
/dev/loop17     401M  401M     0  100% /snap/gnome-3-38-2004/112
/dev/loop18     640K  640K     0  100% /snap/gnome-logs/106
/dev/loop19     219M  219M     0  100% /snap/gnome-3-34-1804/77
tmpfs           196M   40K  196M    1% /run/user/1000
/dev/sdb1       2.0G  4.0K  2.0G    1% /mnt/linux
/dev/sdb2       7.8G   24K  7.4G    1% /mnt/rootfs
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo umount  /mnt/linux 
linux@ubuntu:~$ sudo umount  /mnt/rootfs 
linux@ubuntu:~$ df -h
文件系统        容量  已用  可用 已用% 挂载点
udev            952M     0  952M    0% /dev
tmpfs           196M  1.7M  194M    1% /run
/dev/sda1        49G  8.9G   38G   20% /
tmpfs           977M     0  977M    0% /dev/shm
tmpfs           5.0M  4.0K  5.0M    1% /run/lock
tmpfs           977M     0  977M    0% /sys/fs/cgroup
/dev/loop1       56M   56M     0  100% /snap/core18/2128
/dev/loop2      2.5M  2.5M     0  100% /snap/gnome-calculator/884
/dev/loop0      128K  128K     0  100% /snap/bare/5
/dev/loop3      2.7M  2.7M     0  100% /snap/gnome-system-monitor/178
/dev/loop4      219M  219M     0  100% /snap/gnome-3-34-1804/72
/dev/loop5      2.5M  2.5M     0  100% /snap/gnome-system-monitor/163
/dev/loop6      768K  768K     0  100% /snap/gnome-characters/726
/dev/loop7      768K  768K     0  100% /snap/gnome-characters/741
/dev/loop8      2.7M  2.7M     0  100% /snap/gnome-calculator/920
/dev/loop9       62M   62M     0  100% /snap/core20/1611
/dev/loop10     640K  640K     0  100% /snap/gnome-logs/112
/dev/loop11      47M   47M     0  100% /snap/snapd/16292
/dev/loop12      92M   92M     0  100% /snap/gtk-common-themes/1535
/dev/loop13      66M   66M     0  100% /snap/gtk-common-themes/1515
/dev/loop14     242M  242M     0  100% /snap/gnome-3-38-2004/70
/dev/loop15      62M   62M     0  100% /snap/core20/1593
/dev/loop16      56M   56M     0  100% /snap/core18/2538
/dev/loop17     401M  401M     0  100% /snap/gnome-3-38-2004/112
/dev/loop18     640K  640K     0  100% /snap/gnome-logs/106
/dev/loop19     219M  219M     0  100% /snap/gnome-3-34-1804/77
tmpfs           196M   40K  196M    1% /run/user/1000
linux@ubuntu:~$ 

```

### 9.9 du 查看文件或目录的大小

​	du命令来自于英文词组“Disk Usage”的缩写，其功能是用于查看文件或目录的大小。人们经常会把df和du命令混淆，df是用于查看磁盘或分区使用情况的命令，而du命令则是用于按照指定容量单位来查看文件或目录在磁盘中的占用情况。

**语法格式：**du [参数] 文件

**常用参数：**﻿

| -a   | 显示目录中所有文件大小 |
| ---- | ---------------------- |
| -k   | 以KB为单位显示文件大小 |
| -m   | 以MB为单位显示文件大小 |
| -g   | 以GB为单位显示文件大小 |
| -h   | 以易读方式显示文件大小 |
| -s   | 仅显示总计             |

**参考实例**

```shell
linux@ubuntu:~$ du -h .
32K	./.presage
4.0K	./Downloads
12K	./.dbus/session-bus
16K	./.dbus
4.0K	./Templates
4.0K	./Videos
4.0K	./.config/en
24K	./.vim/bundle/echofunc/plugin
28K	./.vim/bundle/echofunc
8.0K	./.vim/bundle/nerdtree/syntax
140K	./.vim/bundle/nerdtree/plugin
16K	./.vim/bundle/nerdtree/nerdtree_plugin
60K	./.vim/bundle/nerdtree/doc
236K	./.vim/bundle/nerdtree
16K	./.vim/bundle/CmdlineComplete/plugin
24K	./.vim/bundle/CmdlineComplete
1.4M	./.vim/bundle
1.4M	./.vim
4.0K	./.ssh
249M	.
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo du -h /
... 
4.0K	/root/snap/gnome-system-monitor/178
16K	/root/snap/gnome-system-monitor
4.0K	/root/snap/gnome-characters/common
4.0K	/root/snap/gnome-characters/741
4.0K	/root/snap/gnome-characters/726
16K	/root/snap/gnome-characters
68K	/root/snap
4.0K	/root/.gnupg/private-keys-v1.d
8.0K	/root/.gnupg
4.0K	/root/.cache
96K	/root
15G	/
linux@ubuntu:~$ 
```

## 第10章 进程管理

### **10.1 进程的理解与分类**

- 进程是一个独立的可调度的任务

​		进程是一个程序的一次执行的过程

- 进程和程序的区别


​		程序是静态的，它是一些保存在磁盘上的指令的有序集合，没有任何执行的概念

​		进程是一个动态的概念，它是程序执行的过程，包括创建、调度和消亡

​	   进程是程序执行和资源管理的最小单位

- 进程的分类


​		交互进程：该类进程是由shell控制和运行的。交互进程既可以在前台运行，也可以在后台运行。

​		批处理进程：该类进程不属于某个终端，它被提交到一个队列中以便顺序执行。

​		守护进程：该类进程在后台运行。它一般在Linux启动时开始执行，系统关闭时才结束。

- 主要的进程标识

​		进程号(Process Identity Number，PID)  , PID唯一地标识一个进程

​		父进程号(Parent Process ID，PPID)	

- 进程的状态

  - 运行态：此时进程或者正在运行，或者准备运行。
  
  
    - 等待态(挂起)：此时进程在等待一个事件的发生或某种系统资源。
  
      - v可中断
      -  v不可中断
  
  
    - 停止态：此时进程被中止。
  
  
    - 死亡态：这是一个已终止的进程，但还在进程向量数组中占有一个task_struct结构。
  




![image-20220824135103099](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220824135103099.png)

### **10.2 进程管理命令**

#### 1. **ps 查看进程命令**

​	ps命令来自于英文词组”process status“的缩写，其功能是用于显示当前系统的进程状态。使用ps命令可以查看到进程的所有信息，例如进程的号码、发起者、系统资源使用占比（处理器与内存）、运行状态等等。帮助我们及时的发现哪些进程出现”僵死“或”不可中断“等异常情况。

经常会与kill命令搭配使用来中断和删除不必要的服务进程，避免服务器的资源浪费。

**语法格式：**ps [参数]

**常用参数：**﻿

| a    | 显示现行终端机下的所有程序，包括其他用户的程序 |
| ---- | ---------------------------------------------- |
| u    | 以用户为主的格式来显示程序状况                 |
| x    | 显示所有程序，不以终端机来区分                 |
| e    | 列出程序时，显示每个程序所使用的环境变量       |
| -f   | 显示UID,PPIP,C与STIME栏位                      |

**常用命令**

```shell
ps aux   # 显示进程 
ps -ef   # 查看进程, 显示pid ppid 

#精确查找进程
ps aux |grep sogou  #精确查找sogou 输入法进程信息
ps -ef |grep sogou  #精确查找sogou 输入法进程信息
# 可以使用kill 进程号 可以杀死进程 
kill   进程号 
```

**参考实例**

```shell
# USER : 进程的所有者, 哪个用户运行的进程 
# PID  : 进程的进程号,1是系统的守护进程, 是系统的超级进程
# %CPU : 占用cpu的百分比 
# %MEM : 占用内存的百分比
# VSZ  : 虚拟内存的大小 
# RSS  : 常驻内存集（Resident Set Size），表示该进程分配的内存大小
# TTY  : 所属的终端 
# STAT : 进程的状态 
# START: 启动时间
# TIME : Time是该进程所占用的处理器时间
# COMMAND : 执行的程序 
linux@ubuntu:~/work$ ps aux
USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root          1  0.0  0.3 225488  7616 ?        Ss   8月24   0:06 /sbin/init auto noprompt
root          2  0.0  0.0      0     0 ?        S    8月24   0:00 [kthreadd]
root          3  0.0  0.0      0     0 ?        I<   8月24   0:00 [rcu_gp]
root          4  0.0  0.0      0     0 ?        I<   8月24   0:00 [rcu_par_gp]
root          6  0.0  0.0      0     0 ?        I<   8月24   0:00 [kworker/0:0H-kb]
root          9  0.0  0.0      0     0 ?        I<   8月24   0:00 [mm_percpu_wq]
root         10  0.0  0.0      0     0 ?        S    8月24   0:00 [ksoftirqd/0]
root         11  0.0  0.0      0     0 ?        I    8月24   0:06 [rcu_sched]
root         12  0.0  0.0      0     0 ?        S    8月24   0:00 [migration/0]
root         13  0.0  0.0      0     0 ?        S    8月24   0:00 [idle_inject/0]
root         14  0.0  0.0      0     0 ?        S    8月24   0:00 [cpuhp/0]
root         15  0.0  0.0      0     0 ?        S    8月24   0:00 [cpuhp/1]
root         16  0.0  0.0      0     0 ?        S    8月24   0:00 [idle_inject/1]
linux      1420  0.0  0.3 356328  6828 tty1     Sl   8月24   0:00 ibus-daemon --xim --panel disable
linux      1424  0.0  0.2 275628  5744 tty1     Sl   8月24   0:00 /usr/lib/ibus/ibus-dconf
linux      1426  0.0  0.7 339336 14868 tty1     Sl   8月24   0:00 /usr/lib/ibus/ibus-x11 --kill-daemon
linux      1429  0.0  0.2 273568  5116 ?        Sl   8月24   0:00 /usr/lib/ibus/ibus-portal
linux      1436  0.0  0.2 266436  4888 ?        Ssl  8月24   0:00 /usr/libexec/xdg-permission-store
linux      1443  0.0  0.5 689896 10860 ?        Sl   8月24   0:00 /usr/lib/gnome-shell/gnome-shell-calendar-server
linux      1447  0.0  0.6 1367104 13536 ?       Ssl  8月24   0:00 /usr/lib/evolution/evolution-source-registry
linux      1455  0.0  0.7 785840 14508 ?        Sl   8月24   0:00 /usr/lib/gnome-online-accounts

# ps -ef 
# UID  : 用户名 
# PID  : 进程号 
# PPID : 父进程号 
# TTY  : 终端 
# STIME : Stime指进程启动时间
# TIME : Time是该进程所占用的处理器时间
# CMD  : 执行的程序 
linux@ubuntu:~/work$ ps -ef 
UID         PID   PPID  C STIME TTY          TIME CMD
root          1      0  0 8月24 ?       00:00:07 /sbin/init auto noprompt
root          2      0  0 8月24 ?       00:00:00 [kthreadd]
root          3      2  0 8月24 ?       00:00:00 [rcu_gp]
root          4      2  0 8月24 ?       00:00:00 [rcu_par_gp]
root          6      2  0 8月24 ?       00:00:00 [kworker/0:0H-kb]
root          9      2  0 8月24 ?       00:00:00 [mm_percpu_wq]
root         10      2  0 8月24 ?       00:00:00 [ksoftirqd/0]
root         11      2  0 8月24 ?       00:00:06 [rcu_sched]
linux      1762   1144  0 8月24 tty1    00:00:04 /usr/bin/gnome-software --gapplication-service
linux      1764   1144  0 8月24 tty1    00:00:01 update-notifier
linux      1901   1380  0 8月24 ?       00:00:00 /usr/lib/gvfs/gvfsd-network --spawner :1.24 /org/gtk/gvfs/exec_spaw/1
linux      1942      1  0 8月24 ?       00:00:17 /opt/sogoupinyin/files/bin/sogoupinyin-watchdog
linux      1949      1  0 8月24 ?       00:00:04 /opt/sogoupinyin/files/bin/sogoupinyin-service
linux      1964   1380  0 8月24 ?       00:00:00 /usr/lib/gvfs/gvfsd-dnssd --spawner :1.24 /org/gtk/gvfs/exec_spaw/7

# 精确查找程序运行的进程信息
linux@ubuntu:~/work$ ps aux |grep sogou
linux      1269  0.0  0.0  11312    40 ?        Ss   8月24   0:00 /usr/bin/ssh-agent /opt/sogoupinyin/files/bin/sogouimebs-session /usr/bin/im-launch env GNOME_SHELL_SESSION_MODE=ubuntu gnome-session --session=ubuntu
linux      1942  0.0  0.8 612732 16708 ?        Sl   8月24   0:17 /opt/sogoupinyin/files/bin/sogoupinyin-watchdog
linux      1949  0.0  3.3 1496596 66832 ?       Sl   8月24   0:04 /opt/sogoupinyin/files/bin/sogoupinyin-service
linux      6827  0.0  0.0  16180  1032 pts/0    S+   10:26   0:00 grep --color=auto sogou
linux@ubuntu:~/work$ ps -ef|grep sogou
linux      1269   1144  0 8月24 ?       00:00:00 /usr/bin/ssh-agent /opt/sogoupinyin/files/bin/sogouimebs-session /usr/bin/im-launch env GNOME_SHELL_SESSION_MODE=ubuntu gnome-session --session=ubuntu
linux      1942      1  0 8月24 ?       00:00:17 /opt/sogoupinyin/files/bin/sogoupinyin-watchdog
linux      1949      1  0 8月24 ?       00:00:04 /opt/sogoupinyin/files/bin/sogoupinyin-service
linux      6829   3371  0 10:27 pts/0    00:00:00 grep --color=auto sogou
linux@ubuntu:~/work$ 

# 可以使用kill 进程号 可以杀死进程 
linux@ubuntu:~/work$ ps -ef|grep sogou
linux      6860      1  0 10:30 ?        00:00:00 /opt/sogoupinyin/files/bin/sogoupinyin-watchdog
linux      6869      1  3 10:30 ?        00:00:00 /opt/sogoupinyin/files/bin/sogoupinyin-service
linux      6884   3371  0 10:30 pts/0    00:00:00 grep --color=auto sogou
linux@ubuntu:~/work$ sudo kill 6860 6869 
linux@ubuntu:~/work$ ps -ef|grep sogou
linux      6888   3371  0 10:31 pts/0    00:00:00 grep --color=auto sogou
linux@ubuntu:~/work$ 
```

#### 2. **kill 杀死进程命令**

​	kill命令的功能是用于杀死（结束）进程，与英文单词的含义相同。Linux系统中如需结束某个进程，既可以使用如service或systemctl的管理命令来结束服务，也可以使用kill命令直接结束进程信息。

如使用kill命令后进程并没有被结束，则可以使用信号9进行强制杀死动作。

**语法格式：**kill [参数] 进程号

**常用参数：**

| -l   | 列出系统支持的信号             |
| ---- | ------------------------------ |
| -s   | 指定向进程发送的信号           |
| -a   | 不限制命令名和进程号的对应关系 |
| -p   | 不发送任何信号                 |

**参考实例**

列出系统支持的全部信号列表：

```shell
linux@ubuntu:~/work$ kill -l
 1) SIGHUP	 2) SIGINT	 3) SIGQUIT	 4) SIGILL	 5) SIGTRAP
 6) SIGABRT	 7) SIGBUS	 8) SIGFPE	 9) SIGKILL	10) SIGUSR1
11) SIGSEGV	12) SIGUSR2	13) SIGPIPE	14) SIGALRM	15) SIGTERM
16) SIGSTKFLT	17) SIGCHLD	18) SIGCONT	19) SIGSTOP	20) SIGTSTP
21) SIGTTIN	22) SIGTTOU	23) SIGURG	24) SIGXCPU	25) SIGXFSZ
26) SIGVTALRM	27) SIGPROF	28) SIGWINCH	29) SIGIO	30) SIGPWR
31) SIGSYS	34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
63) SIGRTMAX-1	64) SIGRTMAX	
linux@ubuntu:~/work$ 
```

常用方法:

```shell
kill -9 -1     # 9 是强制杀死进程 Kill all processes you can kill.  系统立即崩溃 

kill -l 11     #  Translate number 11 into a signal name. 

kill -L        # List the available signal choices in a nice table.

kill 123 543 2341 3453 # Send the default signal, SIGTERM, to all those processes.  使用默认信号15 去杀死进程


kill 123      # 是用15信号去杀死进程 ,这个信号可以被忽略,有可能杀不死 
kill -9 123   # 是用9信号去杀死进程 , 9信号是强制杀死, 不能被忽略, 9 这个信号一定可以杀死
```

- 例如

```shell
linux@ubuntu:~/work$ ps -ef|grep sogou
linux      6936   3371  0 11:04 pts/0    00:00:00 grep --color=auto sogou
linux@ubuntu:~/work$ ps -ef|grep sogou
linux      6949      1 18 11:04 ?        00:00:00 /opt/sogoupinyin/files/bin/sogoupinyin-service
linux      6962   3371  0 11:04 pts/0    00:00:00 grep --color=auto sogou
linux@ubuntu:~/work$ kill -9 6949
linux@ubuntu:~/work$ ps -ef|grep sogou
linux      6965   3371  0 11:05 pts/0    00:00:00 grep --color=auto sogou
linux@ubuntu:~/work$ 

# 把系统的进程能杀死的全部杀死 , 自杀 
linux@ubuntu:~/work$ kill -9 -1  # 执行后, 立即注销用户了

root@ubuntu:~/work# kill -9 -1  # 执行后, 立即宕机了

```

#### 3. **pstree命令** 

​	Linux系统中pstree命令的英文全称是“process tree”，即将所有行程以树状图显示，树状图将会以 pid (如果有指定) 或是以 init 这个基本行程为根 (root)，如果有指定使用者 id，则树状图会只显示该使用者所拥有的行程。

**语法格式：** pstree [参数]

**常用参数：**

| -a   | 显示每个程序的完整指令，包含路径，参数或是常驻服务的标示 |
| ---- | -------------------------------------------------------- |

```shell
pstree         # 显示所有进程的详细信息
pstree -a      #显示所有进程的所有详细信息，遇到相同的进程名可以压缩显示
```

**参考实例**

```shell
linux@ubuntu:~$ pstree
systemd─┬─ModemManager───2*[{ModemManager}]
        ├─NetworkManager─┬─dhclient
        │                └─2*[{NetworkManager}]
        ├─VGAuthService
        ├─accounts-daemon───2*[{accounts-daemon}]
        ├─acpid
        ├─avahi-daemon───avahi-daemon
        ├─bluetoothd
        ├─boltd───2*[{boltd}]
        ├─colord───2*[{colord}]
        ├─cron
        ├─cups-browsed───2*[{cups-browsed}]
        ├─cupsd───dbus
        ├─2*[dbus-daemon]
        ├─fcitx───2*[{fcitx}]
        ├─fcitx-dbus-watc
        ├─fwupd───4*[{fwupd}]
        ├─rsyslogd───3*[{rsyslogd}]
        ├─rtkit-daemon───2*[{rtkit-daemon}]
        ├─snapd───13*[{snapd}]
        ├─sogoupinyin-ser───8*[{sogoupinyin-ser}]      # 这就是sougou输入法的进程图
        ├─sogoupinyin-wat───4*[{sogoupinyin-wat}]
        ├─systemd─┬─(sd-pam)
        │         ├─at-spi-bus-laun─┬─dbus-daemon
        │         │                 └─3*[{at-spi-bus-laun}]
        │         ├─at-spi2-registr───2*[{at-spi2-registr}]
        │         ├─dbus-daemon
        │         ├─ibus-portal───2*[{ibus-portal}]
        │         ├─pulseaudio───3*[{pulseaudio}]
        │         └─xdg-permission-───2*[{xdg-permission-}]
        ├─systemd─┬─(sd-pam)
        │         ├─at-spi-bus-laun─┬─dbus-daemon
        │         │                 └─3*[{at-spi-bus-laun}]
        │         ├─at-spi2-registr───2*[{at-spi2-registr}]
        │         ├─dbus-daemon
        │         ├─dconf-service───2*[{dconf-service}]
        │         ├─evolution-addre─┬─evolution-addre───5*[{evolution-addre}]
        │         │                 └─4*[{evolution-addre}]
        │         ├─evolution-calen─┬─evolution-calen───8*[{evolution-calen}]
        │         │                 └─4*[{evolution-calen}]
        │         ├─evolution-sourc───3*[{evolution-sourc}]
        │         ├─gnome-shell-cal───5*[{gnome-shell-cal}]
        │         ├─gnome-terminal-─┬─bash───pstree
        │         │                 └─3*[{gnome-terminal-}]
        │         ├─goa-daemon───3*[{goa-daemon}]
        │         ├─goa-identity-se───3*[{goa-identity-se}]
        │         ├─gvfs-afc-volume───3*[{gvfs-afc-volume}]
        │         ├─gvfs-goa-volume───2*[{gvfs-goa-volume}]
        │         ├─gvfs-gphoto2-vo───2*[{gvfs-gphoto2-vo}]
        │         ├─gvfs-mtp-volume───2*[{gvfs-mtp-volume}]
        │         ├─gvfs-udisks2-vo───2*[{gvfs-udisks2-vo}]
        │         ├─gvfsd─┬─gvfsd-trash───2*[{gvfsd-trash}]
        │         │       └─2*[{gvfsd}]
        │         ├─gvfsd-fuse───5*[{gvfsd-fuse}]
        │         ├─ibus-portal───2*[{ibus-portal}]
        │         └─xdg-permission-───2*[{xdg-permission-}]
        ├─systemd-journal
        ├─systemd-logind
        ├─systemd-resolve
        ├─systemd-timesyn───{systemd-timesyn}
        ├─systemd-udevd
        ├─udisksd───4*[{udisksd}]
        ├─unattended-upgr───{unattended-upgr}
        ├─upowerd───2*[{upowerd}]
        ├─vmtoolsd───2*[{vmtoolsd}]
        ├─vmtoolsd───3*[{vmtoolsd}]
        ├─vmware-vmblock-───2*[{vmware-vmblock-}]
        ├─whoopsie───2*[{whoopsie}]
        └─wpa_supplicant
linux@ubuntu:~$ 
linux@ubuntu:~$ pstree -a      #显示所有进程的所有详细信息，遇到相同的进程名可以压缩显示
systemd auto noprompt
  ├─ModemManager --filter-policy=strict
  │   └─2*[{ModemManager}]
  ├─NetworkManager --no-daemon
  │   ├─dhclient -d -q -sf /usr/lib/NetworkManager/nm-dhcp-helper -pf /run/dhclient-ens33.pid -lf...
  │   └─2*[{NetworkManager}]
  ├─VGAuthService
  ├─accounts-daemon
  │   └─2*[{accounts-daemon}]
  ├─acpid
  ├─avahi-daemon
  │   └─avahi-daemon
  ├─bluetoothd
  ├─boltd
  │   └─2*[{boltd}]
  ├─colord
  │   └─2*[{colord}]
  ├─cron -f
  ├─cups-browsed
  │   └─2*[{cups-browsed}]
  ├─cupsd -l
  │   └─dbus dbus:// 
  ├─gnome-keyring-d --daemonize --login
  │   └─3*[{gnome-keyring-d}]
  ├─gsd-printer
  │   └─2*[{gsd-printer}]
  ├─ibus-x11 --kill-daemon
  │   └─2*[{ibus-x11}]
  ├─ibus-x11 --kill-daemon
  │   └─2*[{ibus-x11}]
  ├─irqbalance --foreground
  │   └─{irqbalance}
  ├─kerneloops --test
  ├─kerneloops
  ├─networkd-dispat /usr/bin/networkd-dispatcher --run-startup-triggers
  │   └─{networkd-dispat}
  ├─packagekitd
  │   └─2*[{packagekitd}]
  ├─polkitd --no-debug
  │   └─2*[{polkitd}]
  ├─pulseaudio --start --log-target=syslog
  │   └─3*[{pulseaudio}]
  ├─rsyslogd -n
  │   └─3*[{rsyslogd}]
  ├─rtkit-daemon
  │   └─2*[{rtkit-daemon}]
  ├─snapd
  │   └─13*[{snapd}]
  ├─sogoupinyin-ser
  │   └─8*[{sogoupinyin-ser}]
  ├─sogoupinyin-wat
  │   └─4*[{sogoupinyin-wat}]
  ├─systemd --user
  │   ├─(sd-pam)
  │   ├─at-spi-bus-laun
  │   │   ├─dbus-daemon --config-file=/usr/share/defaults/at-spi2/accessibility.conf --nofork --print-address 3
  │   │   └─3*[{at-spi-bus-laun}]
  │   ├─at-spi2-registr --use-gnome-session
  │   │   └─2*[{at-spi2-registr}]
  │   ├─dbus-daemon --session --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
  │   ├─ibus-portal
  │   │   └─2*[{ibus-portal}]
  │   ├─pulseaudio --daemonize=no
  │   │   └─3*[{pulseaudio}]
  │   └─xdg-permission-
  │       └─2*[{xdg-permission-}]
  ├─systemd-udevd
  ├─udisksd
  │   └─4*[{udisksd}]
  ├─unattended-upgr /usr/share/unattended-upgrades/unattended-upgrade-shutdown --wait-for-signal
  │   └─{unattended-upgr}
  ├─upowerd
  │   └─2*[{upowerd}]
  ├─vmtoolsd
  │   └─2*[{vmtoolsd}]
  ├─vmtoolsd -n vmusr --blockFd 3
  │   └─3*[{vmtoolsd}]
  ├─vmware-vmblock- /run/vmblock-fuse -o rw,subtype=vmware-vmblock,default_permissions,allow_other,dev,suid
  │   └─2*[{vmware-vmblock-}]
  ├─whoopsie -f
  │   └─2*[{whoopsie}]
  └─wpa_supplicant -u -s -O /run/wpa_supplicant
linux@ubuntu:~$ 

```

#### 4. **top 任务管理器命令**

​	top命令的功能是用于实时显示系统运行状态，包含处理器、内存、服务、进程等重要资源信息。工程师们常常会把top命令比作是“加强版的Windows任务管理器”，因为除了能看到常规的服务进程信息以外，还能够对处理器和内存的负载情况一目了然，实时感知系统全局的运行状态，非常适合作为接手服务器后执行的第一条命令。

**语法格式：**top [参数]

**常用参数：**

| -d <秒> | 改变显示的更新速度                   |
| ------- | ------------------------------------ |
| -c      | 切换显示模式                         |
| -s      | 安全模式，不允许交互式指令           |
| -i      | 不显示任何闲置或僵死的行程           |
| -n      | 设定显示的总次数，完成后将会自动退出 |
| -b      | 批处理模式，不进行交互式显示         |

**参考实例**

```shell

top - 14:21:26 up  3:11,  1 user,  load average: 0.07, 0.02, 0.00
任务: 337 total,   1 running, 257 sleeping,   0 stopped,   0 zombie
%Cpu(s):  1.2 us,  1.9 sy,  0.0 ni, 96.7 id,  0.0 wa,  0.0 hi,  0.2 si,  0.0 st
KiB Mem :  1999368 total,    99572 free,  1333920 used,   565876 buff/cache
KiB Swap:  2097148 total,  2089968 free,     7180 used.   501016 avail Mem 
进程 USER      PR  NI    VIRT    RES    SHR �  %CPU %MEM     TIME+ COMMAND                                   1604 linux     20   0  489756  85000  40080 S   5.0  4.3   0:11.92 Xorg                                     1732 linux     20   0 3556352 210304  94628 S   4.0 10.5   0:18.12 gnome-shell                               2086 linux     20   0  693544  47552  35588 S   3.0  2.4   0:05.61 gnome-terminal-                           1 root      20   0  225328   8528   6612 S   0.3  0.4   0:03.92 systemd                                     7 root      20   0       0      0      0 I   0.3  0.0   0:01.63 kworker/0:1-eve                             410 root     -51   0       0      0      0 S   0.3  0.0   0:00.33 irq/16-vmwgfx                             605 root       0 -20  230284   7232   6596 S   0.3  0.4   0:15.11 vmtoolsd                                   2 root      20   0       0      0      0 S   0.0  0.0   0:00.02 kthreadd                                     3 root       0 -20       0      0      0 I   0.0  0.0   0:00.00 rcu_gp                                       4 root       0 -20       0      0      0 I   0.0  0.0   0:00.00 rcu_par_gp   


# top -c  显示执行程序完整的路径名 
top - 14:23:21 up  3:12,  1 user,  load average: 0.01, 0.01, 0.00
任务: 337 total,   1 running, 257 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.9 us,  1.3 sy,  0.0 ni, 97.6 id,  0.0 wa,  0.0 hi,  0.2 si,  0.0 st
KiB Mem :  1999368 total,    99048 free,  1334244 used,   566076 buff/cache
KiB Swap:  2097148 total,  2089968 free,     7180 used.   500612 avail Mem 
进程 USER      PR  NI    VIRT    RES    SHR �  %CPU %MEM     TIME+ COMMAND                                   1604 linux     20   0  489756  85000  40080 S   4.3  4.3   0:13.07 /usr/lib/xorg/Xorg vt1 -displayfd 3 -auth /run/user/1000/gdm/Xauthority -backgroun+ 
1732 linux     20   0 3556268 210308  94628 S   4.3 10.5   0:19.06 /usr/bin/gnome-shell                     2086 linux     20   0  693544  47552  35588 S   2.7  2.4   0:06.24 /usr/lib/gnome-terminal/gnome-terminal-server                                       
605 root       0 -20  230284   7232   6596 S   1.3  0.4   0:15.27 /usr/bin/vmtoolsd                         1 root      20   0  225328   8528   6612 S   0.3  0.4   0:03.98 /sbin/init auto noprompt                     
```

#### 5. **fg 切换到后台命令**

 	fg命令是""用于将后台作业（在后台运行的或者在后台挂起的作业）放到前台终端运行。与bg命令一样，若后台任务中只有一个，则使用该命令时，可以省略任务号。

**语法格式：**fg [参数]

**参考实例**

test_fg.c实现 

```c
#include <stdio.h>  
#include <unistd.h>

int main(int argc, char const *argv[])
{

    while(1)
    {
        sleep(1);
    }
    return 0;
}
```



```shell
linux@ubuntu:~/work$ gcc test_fg.c -o test_fg
linux@ubuntu:~/work$ ./test_fg &     # 可以把当前正运行的进程放到后台去执行
[2] 2513
linux@ubuntu:~/work$ ps -ef|grep test_fg
linux      2502   2093  0 14:31 pts/0    00:00:00 ./test_fg
linux      2513   2093  0 14:31 pts/0    00:00:00 ./test_fg
linux      2517   2093  0 14:32 pts/0    00:00:00 grep --color=auto test_fg
linux@ubuntu:~/work$ kill 2502 2513
linux@ubuntu:~/work$ ps -ef|grep test_fg
linux      2519   2093  0 14:32 pts/0    00:00:00 grep --color=auto test_fg
[1]-  已终止               ./test_fg
[2]+  已终止               ./test_fg
linux@ubuntu:~/work$ ps -ef|grep test_fg
linux      2521   2093  0 14:32 pts/0    00:00:00 grep --color=auto test_fg
linux@ubuntu:~/work$ ./test_fg &
[1] 2522     # [1] 表示任务的编号 
linux@ubuntu:~/work$ fg 2522  # 这里不能使用pid , 只能有任务编号 [1]里面的数字
bash: fg: 2522: 无此任务
linux@ubuntu:~/work$ fg 1   # 1 是任务编号 , 把任务1 切换到前台 
./test_fg
^Z
[1]+  已停止               ./test_fg    # 把这个任务切换到后台并停止运行 

```

#### 6. bg 切换到前台命令

​	bg命令用于将作业放到后台运行，使前台可以执行其他任务。该命令的运行效果与在指令后面添加符号&的效果是相同的，都是将其放到系统后台执行。

**语法格式：**bg [参数]

**参考实例**

```shell
linux@ubuntu:~/work$ ps -ef|grep test_fg
linux      2532   2093  0 14:39 pts/0    00:00:00 grep --color=auto test_fg
linux@ubuntu:~/work$ ./test_fg   # 在前台运行这个程序 
^Z
[1]+  已停止               ./test_fg   # 使用ctrl +z 把进程切换到后台并停止 
linux@ubuntu:~/work$ bg 1  # 在使用 bg 1 把这个程序运行起来  
[1]+ ./test_fg &
linux@ubuntu:~/work$ fg 1  # 把1 任务切换到前台 
./test_fg
^Z
[1]+  已停止               ./test_fg
linux@ubuntu:~/work$ bg 1
[1]+ ./test_fg &
linux@ubuntu:~/work$ ls
1.1_1.2.patch  1.1.c  test_fg  test_fg.c
linux@ubuntu:~/work$ fg 1
./test_fg
^C
linux@ubuntu:~/work$ 
```



## 第11章 网络管理

### **11.1 查看网络设置**

#### 1. **ifconfig 显示与设置网络命令**

​	ifconfig命令来自于英文词组”network interfaces configuring“的缩写，其功能是用于显示或设置网络设备参数信息。在Windows系统中与之类似的命令叫做ipconfig，同样的功能可以使用ifconfig去完成。

通常不建议使用ifconfig命令配置网络设备的参数信息，因为一旦服务器重启，配置过的参数会自动失效，还是编写到配置文件中更稳妥。

**语法格式：**ifconfig [参数] [网卡设备]

**常用参数：**

| add<地址> | 设置网络设备IPv6的IP地址 |
| --------- | ------------------------ |
| del<地址> | 删除网络设备IPv6的IP地址 |
| down      | 关闭指定的网络设备       |
| up        | 启动指定的网络设备       |



```shell
首先说明下eth0与ens33的关系:
目前的主流网卡为使用以太网络协定所开发出来的以太网卡 （Ethernet），因此我们 Linux 就称呼这种网络接口为 ethN （N 为数字）。 举例来说，主机上面有一张以太网卡，因此主机的网络接口就是 eth0 (第一张为 0 号开始）。新的 CentOS 7 开始对于网卡的编号有另一套规则，网卡的界面代号与网卡的来源有关,网卡名称会是这样分类的：

eno1 ：代表由主板 BIOS 内置的网卡
ens1 ：代表由主板 BIOS 内置的 PCI-E 界面的网卡
enp2s0 ：代表 PCI-E 界面的独立网卡，可能有多个插孔，因此会有 s0, s1... 的编号～
eth0 ：如果上述的名称都不适用，就回到原本的默认网卡编号
所以会有ens33这种网卡表现形式。
```

常用命令

```shell
ifconfig  # 查看系统的网卡信息 
ifconfig ens33   #查看具体某一个网卡的信息 
sudo ifconfig ens33 192.168.1.200 netmask 255.255.255.0  # 设置ip地址和子网掩码 , 只设置ip地址 , 重启后消失 
sudo ifconfig ens33 192.168.1.199  # 只设置ip地址 , 重启后消失 
sudo ifconfig ens33 down   # 关闭网卡 
sudo ifconfig ens33 up  # 启动网卡 , 启动网卡后, 原来ifconfig设置的ip会丢失
```

实例

```shell
linux@ubuntu:~/work$ ifconfig   # ubuntu 默认没有集成ifconfig ,需要额外的安装命令

Command 'ifconfig' not found, but can be installed with:

sudo apt install net-tools

linux@ubuntu:~/work$ 
linux@ubuntu:~/work$ sudo apt install net-tools    # 安装ifconfig 命令 
[sudo] linux 的密码： 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列【新】软件包将被安装：
  net-tools
升级了 0 个软件包，新安装了 1 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
需要下载 194 kB 的归档。
解压缩后会消耗 803 kB 的额外空间。
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 net-tools amd64 1.60+git20161116.90da8a0-1ubuntu1 [194 kB]
已下载 194 kB，耗时 0秒 (1,058 kB/s)
正在选中未选择的软件包 net-tools。
(正在读取数据库 ... 系统当前共安装有 162683 个文件和目录。)
正准备解包 .../net-tools_1.60+git20161116.90da8a0-1ubuntu1_amd64.deb  ...
正在解包 net-tools (1.60+git20161116.90da8a0-1ubuntu1) ...
正在设置 net-tools (1.60+git20161116.90da8a0-1ubuntu1) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
linux@ubuntu:~/work$
linux@ubuntu:~$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.103  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::fb81:9220:45a:8e  prefixlen 64  scopeid 0x20<link>
        inet6 2409:8a02:7826:b7f0:c027:1733:cfe1:1684  prefixlen 64  scopeid 0x0<global>
        inet6 2409:8a02:7826:b7f0:c575:a926:1076:de45  prefixlen 64  scopeid 0x0<global>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 4599  bytes 870936 (870.9 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 3423  bytes 406766 (406.7 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 24241  bytes 1730212 (1.7 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 24241  bytes 1730212 (1.7 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~$ 
linux@ubuntu:~/work$ ifconfig ens33 
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.103  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::3901:d959:9bc4:b96  prefixlen 64  scopeid 0x20<link>
        inet6 2409:8a02:782e:72d0:e147:85ad:1f7a:666d  prefixlen 64  scopeid 0x0<global>
        inet6 2409:8a02:782e:72d0:d89c:64ba:7cd2:a002  prefixlen 64  scopeid 0x0<global>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 2639  bytes 980891 (980.8 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1368  bytes 154990 (154.9 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


linux@ubuntu:~/work$ ifconfig ens33 192.168.1.200 netmask 255.255.255.0  # 设置ip地址和子网掩码
SIOCSIFADDR: 不允许的操作
SIOCSIFFLAGS: 不允许的操作
SIOCSIFNETMASK: 不允许的操作
linux@ubuntu:~/work$ sudo ifconfig ens33 192.168.1.200 netmask 255.255.255.0 
linux@ubuntu:~/work$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.200  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::3901:d959:9bc4:b96  prefixlen 64  scopeid 0x20<link>
        inet6 2409:8a02:782e:72d0:e147:85ad:1f7a:666d  prefixlen 64  scopeid 0x0<global>
        inet6 2409:8a02:782e:72d0:d89c:64ba:7cd2:a002  prefixlen 64  scopeid 0x0<global>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 2653  bytes 982155 (982.1 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1400  bytes 159009 (159.0 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 465  bytes 41122 (41.1 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 465  bytes 41122 (41.1 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~/work$ sudo ifconfig ens33 192.168.1.199  # 只设置ip地址 , 重启后消失 
linux@ubuntu:~/work$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.199  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::3901:d959:9bc4:b96  prefixlen 64  scopeid 0x20<link>
        inet6 2409:8a02:782e:72d0:e147:85ad:1f7a:666d  prefixlen 64  scopeid 0x0<global>
        inet6 2409:8a02:782e:72d0:d89c:64ba:7cd2:a002  prefixlen 64  scopeid 0x0<global>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 2664  bytes 983137 (983.1 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1432  bytes 163622 (163.6 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 473  bytes 41770 (41.7 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 473  bytes 41770 (41.7 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~/work$ sudo ifconfig ens33 down   # 关闭网卡 
linux@ubuntu:~/work$ ifconfig
lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 483  bytes 42690 (42.6 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 483  bytes 42690 (42.6 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~/work$ 
linux@ubuntu:~/work$ sudo ifconfig ens33 up  # 启动网卡 , 启动网卡后, 原来ifconfig设置的ip会丢失
linux@ubuntu:~/work$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.103  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::3901:d959:9bc4:b96  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 2698  bytes 986832 (986.8 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1509  bytes 171492 (171.4 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 511  bytes 44732 (44.7 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 511  bytes 44732 (44.7 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~/work$ 


```

#### 2. **route 显示与设置路由命令**

​	route命令的功能是用于显示与设置路由信息，是Linux系统中常用的静态路由配置工具。要想让两台处在不同子网之间的服务器能够通信，需要有一个跨网段的路由器来连接它们，并用route命令为其设置路由信息，提供软硬件的支撑。

**语法格式：**route [参数]

**常用参数：**

| -A     | 设置地址类型（ 默认IPv4）         |
| ------ | --------------------------------- |
| -C     | 打印核心的路由缓存                |
| -v     | 详细信息模式                      |
| -n     | 直接显示数字形式的ip地址          |
| -e     | netstat格式显示路由表             |
| -net   | 到一个网络的路由表                |
| -host  | 到一个主机的路由表                |
| Add    | 增加指定的路由记录                |
| Del    | 删除指定的路由记录                |
| Target | 目的网络或目的主机                |
| gw     | 设置默认网关                      |
| mss    | 设置TCP的最大区块长度（单位MB）   |
| window | 指定通过路由表的TCP连接的窗口大小 |
| dev    | 路由记录所表示的网络接口          |

**常用命令**

```shell
route   # 查看默认的路由信息 
route add -net 192.168.10.0 netmask 255.255.255.0 dev ens33  # 添加一个路由连接
route del -net 192.168.10.0 netmask 255.255.255.0 dev ens33  # 删除一个路由连接
```

**实例**

```shell
linux@ubuntu:~/work$ route
内核 IP 路由表
目标            网关            子网掩码        标志  跃点   引用  使用 接口
default         192.168.1.1     0.0.0.0         UG    20100  0        0 ens33
link-local      0.0.0.0         255.255.0.0     U     1000   0        0 ens33
192.168.1.0     0.0.0.0         255.255.255.0   U     100    0        0 ens33
linux@ubuntu:~/work$ route add -net 192.168.10.0 netmask 255.255.255.0 dev ens33   # 添加路由信息
SIOCADDRT: 不允许的操作
linux@ubuntu:~/work$ sudo route add -net 192.168.10.0 netmask 255.255.255.0 dev ens33  #
[sudo] linux 的密码： 
linux@ubuntu:~/work$ route
内核 IP 路由表
目标            网关            子网掩码        标志  跃点   引用  使用 接口
default         192.168.1.1     0.0.0.0         UG    100    0        0 ens33
link-local      0.0.0.0         255.255.0.0     U     1000   0        0 ens33
192.168.1.0     0.0.0.0         255.255.255.0   U     100    0        0 ens33
192.168.10.0    0.0.0.0         255.255.255.0   U     0      0        0 ens33
linux@ubuntu:~/work$ route del -net 192.168.10.0 netmask 255.255.255.0 dev ens33  # 删除路由信息
SIOCDELRT: 不允许的操作
linux@ubuntu:~/work$ sudo route del -net 192.168.10.0 netmask 255.255.255.0 dev ens33
linux@ubuntu:~/work$ route
内核 IP 路由表
目标            网关            子网掩码        标志  跃点   引用  使用 接口
default         192.168.1.1     0.0.0.0         UG    100    0        0 ens33
link-local      0.0.0.0         255.255.0.0     U     1000   0        0 ens33
192.168.1.0     0.0.0.0         255.255.255.0   U     100    0        0 ens33
linux@ubuntu:~/work$ 

```



#### 3. **hostname命令**

​	hostname命令的功能是用于显示和设置系统的主机名，Linux系统中的HOSTNAME环境变量对应保存了当前的主机名称，使用hostname命令能够查看和设置此环境变量的值，而要想永久修改主机名称则需要使用hostnamectl命令或直接编辑配置文件/etc/hostname才行。

**语法格式：**hostname [参数]

**常用参数：**

| -a   | 显示主机别名     |
| ---- | ---------------- |
| -d   | 显示DNS域名      |
| -f   | 显示FQDN名称     |
| -i   | 显示主机的ip地址 |
| -s   | 显示短主机名称   |
| -y   | 显示NIS域名      |

**参考实例**

```shell
linux@ubuntu:~/work$ cat /etc/hostname    
ubuntu
linux@ubuntu:~/work$ hostname
ubuntu
linux@ubuntu:~/work$ hostname -i
127.0.1.1
linux@ubuntu:~/work$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.103  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::3901:d959:9bc4:b96  prefixlen 64  scopeid 0x20<link>
        inet6 2409:8a02:782e:72d0:e147:85ad:1f7a:666d  prefixlen 64  scopeid 0x0<global>
        inet6 2409:8a02:782e:72d0:d89c:64ba:7cd2:a002  prefixlen 64  scopeid 0x0<global>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 2953  bytes 1020453 (1.0 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1690  bytes 191286 (191.2 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 593  bytes 51789 (51.7 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 593  bytes 51789 (51.7 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~/work$ hostname -y
hostname: Local domain name not set
linux@ubuntu:~/work$ man hostname
linux@ubuntu:~/work$ hostname ubuntu18.04   # 临时修改主机名, 重启后消失 
hostname: you must be root to change the host name
linux@ubuntu:~/work$ sudo hostname ubuntu18.04
linux@ubuntu:~/work$ hostname
ubuntu18.04
linux@ubuntu:~/work$ cat /etc/hostname 
ubuntu
linux@ubuntu:~/work$ 
linux@ubuntu1804:~$ sudo vi /etc/hostname   #修改为ubuntu ,
[sudo] linux 的密码： 
linux@ubuntu1804:~$ reboot 
```

#### 3. **netstat命令** 

​	netstat命令来自于英文词组”network statistics“的缩写，其功能是用于显示各种网络相关信息，例如网络连接状态、路由表信息、接口状态、NAT、多播成员等等。

netstat命令不仅应用于Linux系统，而且在Windows XP、Windows 7、Windows 10及Windows 11中均已默认支持，并且可用参数也相同，有经验的运维人员可以直接上手。

**语法格式：**netstat [参数]

**常用参数：**

| -a   | 显示所有连线中的Socket                   |
| ---- | ---------------------------------------- |
| -p   | 显示正在使用Socket的程序识别码和程序名称 |
| -l   | 仅列出在监听的服务状态                   |
| -t   | 显示TCP传输协议的连线状况                |
| -u   | 显示UDP传输协议的连线状况                |
| -i   | 显示网络界面信息表单                     |
| -r   | 显示路由表信息                           |
| -n   | 直接使用IP地址，不通过域名服务器         |

**常用命令**

```shell
netstat -a   # 显示系统网络中的所有连接信息 
netstat -nu  #显示系统网络状态中的UDP连接信息
netstat -apu #显示系统网络状态中的UDP连接端口号使用信息
netstat -i   #显示网卡当前状态信息
netstat -r   #显示网络路由表状态信息
netstat -ap | grep sogou   #找到某个服务所对应的连接信息
```

**参考实例**

```shell
linux@ubuntu:~$ netstat -a
激活Internet连接 (服务器和已建立连接的)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      0 localhost:ipp           0.0.0.0:*               LISTEN     
tcp        0      0 localhost:domain        0.0.0.0:*               LISTEN     
tcp6       0      0 ip6-localhost:ipp       [::]:*                  LISTEN     
udp        0      0 0.0.0.0:ipp             0.0.0.0:*                          
udp        0      0 0.0.0.0:43772           0.0.0.0:*                          
udp        0      0 localhost:domain        0.0.0.0:*                          
udp        0      0 0.0.0.0:bootpc          0.0.0.0:*                          
udp        0      0 0.0.0.0:mdns            0.0.0.0:*                          
udp6       0      0 [::]:56761              [::]:*                             
udp6       0      0 [::]:mdns               [::]:*                             
raw6       0      0 [::]:ipv6-icmp          [::]:*                  7          
活跃的UNIX域套接字 (服务器和已建立连接的)
Proto RefCnt Flags       Type       State         I-Node   路径
unix  2      [ ACC ]     流        LISTENING     43699    /tmp/fcitx-socket-:0
unix  2      [ ACC ]     流        LISTENING     49634    @/tmp/.ICE-unix/1197

linux@ubuntu:~$ netstat -nu
激活Internet连接 (w/o 服务器)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo netstat -apu
[sudo] linux 的密码： 
激活Internet连接 (服务器和已建立连接的)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
udp        0      0 0.0.0.0:ipp             0.0.0.0:*                           759/cups-browsed    
udp        0      0 0.0.0.0:43772           0.0.0.0:*                           665/avahi-daemon: r 
udp        0      0 localhost:domain        0.0.0.0:*                           560/systemd-resolve 
udp        0      0 0.0.0.0:bootpc          0.0.0.0:*                           818/dhclient        
udp        0      0 0.0.0.0:mdns            0.0.0.0:*                           665/avahi-daemon: r 
udp6       0      0 [::]:56761              [::]:*                              665/avahi-daemon: r 
udp6       0      0 [::]:mdns               [::]:*                              665/avahi-daemon: r 
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo netstat -i
Kernel Interface table
Iface      MTU    RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
ens33     1500      547      0      0 0           411      0      0      0 BMRU
lo       65536      201      0      0 0           201      0      0      0 LRU
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo netstat -r
内核 IP 路由表
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
default         192.168.1.1     0.0.0.0         UG        0 0          0 ens33
link-local      0.0.0.0         255.255.0.0     U         0 0          0 ens33
192.168.1.0     0.0.0.0         255.255.255.0   U         0 0          0 ens33
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo netstat -ap |grep sogou
unix  3      [ ]         流        已连接     55226    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     56677    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     53587    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     53573    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     53574    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     53579    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     55227    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     56676    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     55220    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     53582    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     56690    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     53577    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     55447    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     55441    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     55453    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     55451    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     55444    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     55454    1806/sogoupinyin-se  
unix  3      [ ]         流        已连接     55449    1800/sogoupinyin-wa  
unix  3      [ ]         流        已连接     55456    1806/sogoupinyin-se  
linux@ubuntu:~$ 
```



### **11.2 测试网络连接**

#### 1. **ping 命令**

​	ping命令的功能是用于测试主机间网络连通性，发送出基于ICMP传输协议的数据包，要求对方主机予以回复，若对方主机的网络功能没有问题且防火墙放行流量，则就会回复该信息，我们也就可得知对方主机系统在线并运行正常了。

不过值得我们注意的是Linux与Windows相比有一定差异，Windows系统下的ping命令会发送出去4个请求后自动结束该命令；而Linux系统则不会自动终止，需要用户手动按下组合键“Ctrl+c”才能结束，或是发起命令时加入-c参数限定发送个数。

**语法格式：**ping [参数] 目标主机(ip地址或域名)

**常用参数：**

| -d   | 使用Socket的SO_DEBUG功能                 |
| ---- | ---------------------------------------- |
| -c   | 指定发送报文的次数                       |
| -i   | 指定收发信息的间隔时间                   |
| -I   | 使用指定的网络接口送出数据包             |
| -l   | 设置在送出要求信息之前，先行发出的数据包 |
| -n   | 只输出数值                               |
| -p   | 设置填满数据包的范本样式                 |
| -q   | 不显示指令执行过程                       |
| -R   | 记录路由过程                             |
| -s   | 设置数据包的大小                         |
| -t   | 设置存活数值TTL的大小                    |
| -v   | 详细显示指令的执行过程                   |

```shell
ping 192.168.1.1        # 可以测试 电脑和路由器之间是否连接可以通信  Ctrl+c结束测试
ping www.baidu.com      # 测试外网的联通
ping -c 4 www.baidu.com # 发送4个数据包
```

**参考实例**

测试与指定网站服务器之间的网络连通性（需手动按下“Ctrl+c”组合键结束命令）：

```shell
linux@ubuntu:~$ ping 192.168.1.1    # 可以测试 电脑和路由器之间是否连接可以通信  Ctrl+c结束测试
PING 192.168.1.1 (192.168.1.1) 56(84) bytes of data.
64 bytes from 192.168.1.1: icmp_seq=1 ttl=64 time=2.18 ms
64 bytes from 192.168.1.1: icmp_seq=2 ttl=64 time=4.27 ms
64 bytes from 192.168.1.1: icmp_seq=3 ttl=64 time=4.19 ms
64 bytes from 192.168.1.1: icmp_seq=4 ttl=64 time=2.82 ms
64 bytes from 192.168.1.1: icmp_seq=5 ttl=64 time=2.67 ms
64 bytes from 192.168.1.1: icmp_seq=6 ttl=64 time=4.11 ms
64 bytes from 192.168.1.1: icmp_seq=7 ttl=64 time=2.48 ms
64 bytes from 192.168.1.1: icmp_seq=8 ttl=64 time=2.84 ms
64 bytes from 192.168.1.1: icmp_seq=9 ttl=64 time=2.38 ms
^C
--- 192.168.1.1 ping statistics ---
9 packets transmitted, 9 received, 0% packet loss, time 8016ms
rtt min/avg/max/mdev = 2.181/3.109/4.278/0.795 ms
linux@ubuntu:~$ ping www.baidu.com         # 测试外网的联通
PING www.a.shifen.com (39.156.66.18) 56(84) bytes of data.
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=1 ttl=53 time=11.1 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=2 ttl=53 time=11.9 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=3 ttl=53 time=11.7 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=4 ttl=53 time=11.7 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=5 ttl=53 time=12.8 ms
^C
--- www.a.shifen.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4007ms
rtt min/avg/max/mdev = 11.142/11.885/12.808/0.550 ms
linux@ubuntu:~$ ping www.github.com
PING github.com (20.205.243.166) 56(84) bytes of data.
^C
--- github.com ping statistics ---
11 packets transmitted, 0 received, 100% packet loss, time 10239ms

linux@ubuntu:~$ ^C
linux@ubuntu:~$ ping www.ubuntu.com.cn   # 测试ubuntu的网速 
PING www.ubuntu.com.cn (104.160.18.24) 56(84) bytes of data.
64 bytes from 104.160.18.24 (104.160.18.24): icmp_seq=1 ttl=51 time=276 ms
64 bytes from 104.160.18.24 (104.160.18.24): icmp_seq=2 ttl=51 time=203 ms
64 bytes from 104.160.18.24 (104.160.18.24): icmp_seq=3 ttl=51 time=203 ms
64 bytes from 104.160.18.24 (104.160.18.24): icmp_seq=4 ttl=51 time=204 ms
64 bytes from 104.160.18.24 (104.160.18.24): icmp_seq=5 ttl=51 time=203 ms
^C
--- www.ubuntu.com.cn ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 7738ms
rtt min/avg/max/mdev = 203.182/218.121/276.323/29.102 ms
linux@ubuntu:~$ ping mirrors.yun-idc.com
PING mirrors.yun-idc.com (114.112.43.174) 56(84) bytes of data.
64 bytes from 114.112.43.174 (114.112.43.174): icmp_seq=1 ttl=51 time=16.6 ms
64 bytes from 114.112.43.174 (114.112.43.174): icmp_seq=2 ttl=51 time=14.8 ms
64 bytes from 114.112.43.174 (114.112.43.174): icmp_seq=3 ttl=51 time=14.7 ms
64 bytes from 114.112.43.174 (114.112.43.174): icmp_seq=4 ttl=51 time=15.2 ms
^C
--- mirrors.yun-idc.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 12028ms
rtt min/avg/max/mdev = 14.789/15.403/16.682/0.768 ms
linux@ubuntu:~$ ping mirrors.cn99.com
PING mirrors.s.3322.net (118.184.169.32) 56(84) bytes of data.
64 bytes from h118-184-169-32.pubyun.com (118.184.169.32): icmp_seq=1 ttl=53 time=27.1 ms
64 bytes from h118-184-169-32.pubyun.com (118.184.169.32): icmp_seq=2 ttl=53 time=29.8 ms
64 bytes from h118-184-169-32.pubyun.com (118.184.169.32): icmp_seq=3 ttl=53 time=29.4 ms
64 bytes from h118-184-169-32.pubyun.com (118.184.169.32): icmp_seq=4 ttl=53 time=27.6 ms
^C
--- mirrors.s.3322.net ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 27.176/28.512/29.836/1.146 ms
linux@ubuntu:~$ 
linux@ubuntu:~$ ping -c 4 www.baidu.com
PING www.baidu.com (39.156.66.18) 56(84) bytes of data.
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=1 ttl=53 time=94.0 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=2 ttl=53 time=11.5 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=3 ttl=53 time=14.9 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=4 ttl=53 time=13.5 ms

--- www.baidu.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 11.570/33.537/94.097/34.984 ms
linux@ubuntu:~$ 

```

#### 2. **traceroute命令**

​	traceroute命令用于追踪数据包在网络上的传输时的全部路径，它默认发送的数据包大小是40字节。通过traceroute我们可以知道信息从你的计算机到互联网另一端的主机是走的什么路径。当然每次数据包由某一同样的出发点（source）到达某一同样的目的地(destination)走的路径可能会不一样，但基本上来说大部分时候所走的路由是相同的。

traceroute通过发送小的数据包到目的设备直到其返回，来测量其需要多长时间。一条路径上的每个设备traceroute要测3次。输出结果中包括每次测试的时间(ms)和设备的名称（如有的话）及其ip地址。

**语法格式：**traceroute [参数] [域名或者IP]

**参考实例**

```shell
linux@ubuntu:~$ traceroute

Command 'traceroute' not found, but can be installed with:

sudo apt install inetutils-traceroute
sudo apt install traceroute          

linux@ubuntu:~$ traceroute
linux@ubuntu:~$ traceroute www.baidu.com
traceroute to www.a.shifen.com (39.156.66.18), 64 hops max
  1   192.168.1.1  2.693ms  1.952ms  3.894ms 
  2   10.218.0.1  7.793ms  6.179ms  5.989ms 
  3   117.131.130.109  6.204ms  6.087ms  4.574ms 
  4   221.183.63.133  4.546ms  4.183ms  * 
  5   221.183.37.185  13.826ms  6.143ms  7.969ms 
  6   221.183.49.126  14.798ms  16.952ms  21.972ms 
  7   *  39.156.27.1  10.678ms  9.957ms 
  8   39.156.67.33  27.424ms  23.590ms  13.295ms 
  9   39.156.67.41  17.232ms  16.121ms  15.431ms 
 10   *  *  * 
 11   *  *  * 
 12   *  *  * 
 13   *  *  * 
 14   *  *  * 
 15   *  *  * 
 16   * ^C
linux@ubuntu:~$ ^C
linux@ubuntu:~$ ping www.baidu.com
PING www.a.shifen.com (39.156.66.18) 56(84) bytes of data.
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=1 ttl=53 time=11.6 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=2 ttl=53 time=17.7 ms
^C
--- www.a.shifen.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 11.630/14.676/17.722/3.046 ms
linux@ubuntu:~$ 

```

### **11.3 修改网络配置**

#### 1. **图形化配置**(network-manager)

​	Ubuntu有Server版和Desktop版，又有直接安装在处理器上和装在虚拟机上。
​	interfaces和 nm（network-manager）之间的关系，当系统内没有第三方网络管理工具（比如nm）时，系统默认使用interfaces文件内的参数进行网络配置。接着，当系统内安装了 nm之后，nm默认接管了系统的网络配置，使用nm 自己的网络配置参数来进行配置。
但是，如果用户在安装nm之后（Desktop版本默认安装了nm），自己手动修改了interfaces 文件，那nm 就自动停止对系统网络的管理，系统改使用interfaces 文件内的参数进行网络配置。此时，再去修改nm 内的参数，不影响系统实际的网络配置。若要让nm 内的配置生效，必须重新启用nm 接管系统的网络配置。

**设置静态ip上网**

```shell
-> 开始图标(显示应用程序)
-> 设置
-> 网络 
-> 有线连接
-> 齿轮工具 
-> 点击IPV4 选项卡
-> 选择"手动"
-> 通过"详细信息"查看, 可以知道主机的ip 是 192.168.1.103, 路由器的信息就是192.168.1.1
-> 地址栏 手动输入ip信息:
	地址   : 192.168.1.10
	子网掩码: 255.255.255.0
	网关    : 192.168.1.1 
	DNS   :自动关闭 , 输入 114.114.114.114
	路由   :自动
-> 应用 
-> 在网络配置界面中, 有线连接选项中 关闭网络 
-> 在网络配置界面中, 有线连接选项中 开启网络 (修改完ip后要重启网络)
```

```shell
linux@ubuntu:~$ ping www.baidu.com
PING www.a.shifen.com (39.156.66.18) 56(84) bytes of data.
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=1 ttl=53 time=11.6 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=2 ttl=53 time=17.7 ms
^C
--- www.a.shifen.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 11.630/14.676/17.722/3.046 ms
linux@ubuntu:~$ 
linux@ubuntu:~$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.10  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::3901:d959:9bc4:b96  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 8999  bytes 4139491 (4.1 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 3645  bytes 368562 (368.5 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 1206  bytes 104288 (104.2 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1206  bytes 104288 (104.2 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~$ ping www.baidu.com
PING www.baidu.com (39.156.66.18) 56(84) bytes of data.
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=1 ttl=53 time=11.5 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=2 ttl=53 time=13.0 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=3 ttl=53 time=11.9 ms
^C
--- www.baidu.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 11025ms
rtt min/avg/max/mdev = 11.596/12.216/13.072/0.637 ms
linux@ubuntu:~$
```

**设置动态ip上网**

```shell
-> 开始图标(显示应用程序)
-> 设置
-> 网络 
-> 有线连接
-> 齿轮工具 
-> 点击IPV4 选项卡
-> 选择"自动",  让路由器自动分配一个ip地址
	DNS   :自动
	路由   :自动
-> 应用 
-> 在网络配置界面中, 有线连接选项中 关闭网络 
-> 在网络配置界面中, 有线连接选项中 开启网络 (修改完ip后要重启网络)
```

```shell
linux@ubuntu:~$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.103  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::3901:d959:9bc4:b96  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 9588  bytes 4263373 (4.2 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 4111  bytes 427023 (427.0 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 1386  bytes 119391 (119.3 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1386  bytes 119391 (119.3 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~$ ping www.baidu.com
PING www.baidu.com (39.156.66.18) 56(84) bytes of data.
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=1 ttl=53 time=11.6 ms
64 bytes from 39.156.66.18 (39.156.66.18): icmp_seq=2 ttl=53 time=11.3 ms
^C
--- www.baidu.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 11.377/11.505/11.633/0.128 ms
linux@ubuntu:~$ 

```

#### 2. **命令行配置**(不建议使用,建议使用图形化)

​	关于Ubuntu网络配置文件说明：ubuntu从17.10开始，已放弃在/etc/network/interfaces里固定IP的配置，即使配置也不会生效，而是改成netplan方式 ，网卡配置文件路径在：/etc/netplan/文件下，一般后缀名为.yaml文件；可自行切换到/etc/netplan文件下自行查看，我的是：01-network-manager-all.yaml（如果没有.yaml后缀文件可以使用gedit 01-network-manager-all.yaml自己创建（非root账号，命令前加sudo））。
在Ubuntu系的Linux系统里，与网络相关的主要设置文件

```shell
# 
# Let NetworkManager manage all devices on this system
network:
  version: 2
  renderer: NetworkManager
  ethernets:
     ens33:         #配置的网卡名称
       addresses: [192.168.1.20/24]   #设置IP掩码 , 24 是掩码的位数 255.255.255.0 , 3个255 正好是三个字节,就是24位
       gateway4: 192.168.1.1       #设置网关
       nameservers:
         addresses: [114.114.114.114]  #设置dns
 
 
#注意点：
1.以上配置文件共11行，其中第2，3，6，7四行可以不写，测试过没有这四行，网络也能工作正常，第5行的ens33为虚拟网卡，可以使用ifconfig -a查看本机的网卡。
2.配置文件里在冒号：号出现的后面一定要空一格，不空格则在运行netplan apply时提示出错。
3.关键之关键是看清配置总共分为五个层次，逐层向后至少空一格，
第一层－network:
第二层－－ ethernets:
第三层－－－ ens33:
第四层－－－－addresses: [192.168.1.55/24]
第四层－－－－gateway4: 192.168.1.1
第四层－－－－nameservers:
第五层－－－－－addresses: [114.114.114.114, 8.8.8.8]
```

修改ip和网关信息

配置文件内容如下:  /etc/netplan/01-network-manager-all.yaml 

```shell
# Let NetworkManager manage all devices on this system
network:
  version: 2
  renderer: NetworkManager
  ethernets:
   ens33:                     
      addresses: [192.168.1.20/24]   
      gateway4: 192.168.1.1         
      nameservers:
          addresses: [114.114.114.114]  
```



```shell
linux@ubuntu:~$ sudo vi /etc/netplan/01-network-manager-all.yaml 
linux@ubuntu:~$ sudo netplan apply 
linux@ubuntu:~$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.20  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::20c:29ff:fed1:306c  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 1110  bytes 145373 (145.3 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1507  bytes 198438 (198.4 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 23621  bytes 1682426 (1.6 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 23621  bytes 1682426 (1.6 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~$ 


# 在图形界面下, 把netplay-ens33 这个名称改为 static , 这个配置表示是静态ip 
# 在图形界面下, 把ens33 这个名称改为 auto  ,这个配置表示是静态ip 
# 切换到auto 
linux@ubuntu:~$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.103  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::fb81:9220:45a:8e  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 1134  bytes 147930 (147.9 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1575  bytes 207866 (207.8 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 23655  bytes 1684912 (1.6 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 23655  bytes 1684912 (1.6 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~$ 
```

#### 3. 网络图标消失的解决办法

```shell
# 手动的让网络图标消失 
linux@ubuntu:~$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.103  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::fb81:9220:45a:8e  prefixlen 64  scopeid 0x20<link>
        inet6 2409:8a02:7826:b7f0:c027:1733:cfe1:1684  prefixlen 64  scopeid 0x0<global>
        inet6 2409:8a02:7826:b7f0:c575:a926:1076:de45  prefixlen 64  scopeid 0x0<global>
        ether 00:0c:29:d1:30:6c  txqueuelen 1000  (以太网)
        RX packets 745  bytes 104026 (104.0 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 558  bytes 75292 (75.2 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (本地环回)
        RX packets 22970  bytes 1634602 (1.6 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 22970  bytes 1634602 (1.6 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

linux@ubuntu:~$ sudo ifconfig ens33 down  # 关闭网卡 ens33 
[sudo] linux 的密码： 

# 启动网卡ens33 , 启动后, 网络图标并不会出现, 这个时候可以, 可以在设置中找到网络进行配置
linux@ubuntu:~$ sudo ifconfig ens33 up   


# 如果以上方法解决不了, 可以使用下面的解决办法 
sudo service network-manager stop     # 把网络管理服务关闭  
sudo rm /var/lib/NetworkManager/NetworkManager.state  # 删除这个状态文件, 重启服务器后, 会自动生成这个文件
sudo service network-manager start    # 再次启动服务 

sudo vi  /etc/NetworkManager/NetworkManager.conf   #把false改成true
sudo service network-manager restart   # 去设置-> 网络 再进行操作

```

## 第12章 时间管理

### **12.1 Linux时间介绍** 

​	Linux 时钟分为系统时钟（System Clock）和硬件（Real Time Clock，简称 RTC）时钟。系统时钟是指当前 Linux Kernel 的时钟，而硬件时钟则是主板上由电池供电的时钟，这个硬件时钟可以在 BIOS 中进行设置。当 Linux 启动时，硬件时钟会去读取系统时钟的 设置，然后系统时钟就会独立 于硬件运作。  Linux 中的所有命令（包括函数）都是采用的系统时钟设置。在 Linux 中，用于时钟查看和 设置的命令主要有 date、hwclock 和 clock。其中，clock 和 hwclock 用法相近，只用一个就行，只不 过 clock 命令除了支持 x86 硬 件体系外，还支持 Alpha 硬件体系。

### **12.2 Linux时间设置命令**

#### 1. date 显示与设置时间命令

​	date命令来自于英文单词的时间、时钟，其功能是用于显示或设置系统日期与时间信息。运维人员可以根据想要的格式来输出系统时间信息，时间格式`MMDDhhmm[CC][YY][.ss]`，其中 MM 为月份，DD 为日，hh 为小时，mm 为分钟，CC 为年份前两位数字，YY 为年份后两位数字，ss 为秒数。

**语法格式：**date [选项] [+输出形式]

**常用参数：**

| -d datestr | 显示 datestr 中所设定的时间 (非系统时间) |
| ---------- | ---------------------------------------- |
| -s datestr | 将系统时间设为 datestr 中所设定的时间    |
| -u         | 显示目前的格林威治时间                   |
| --help     | 显示帮助信息                             |
| --version  | 显示版本编号                             |

**参考实例**

```shell
linux@ubuntu:~$ date
2022年 08月 26日 星期五 15:41:15 CST
linux@ubuntu:~$ date "+%Y-%m-%d"
2022-08-26
linux@ubuntu:~$ date "+%Y-%m-%d %h:%M:%s"
2022-08-26 8月:42:1661499766
linux@ubuntu:~$ date "+%Y-%m-%d %hh:%M:%s"
2022-08-26 8月h:43:1661499780
linux@ubuntu:~$ date "+%Y-%m-%d %H:%M:%s"
2022-08-26 15:43:1661499784
linux@ubuntu:~$ date "+%Y-%m-%d %H:%M:%S"
2022-08-26 15:43:11
linux@ubuntu:~$ date -s "20220827 18:10:30"
date: 无法设置日期: 不允许的操作
2022年 08月 27日 星期六 18:10:30 CST
linux@ubuntu:~$ sudo date -s "20220827 18:10:30"
[sudo] linux 的密码： 
2022年 08月 27日 星期六 18:10:30 CST
linux@ubuntu:~$ date
2022年 08月 26日 星期五 15:45:23 CST
linux@ubuntu:~$ 
```

#### 2. hwclock命令

​	hwclock命令是一个硬件时钟访问工具，它可以显示当前硬件时间、设置硬件时钟的时间和设置硬件时钟为系统时间，也可设置系统时间为硬件时钟的时间。

在Linux中有硬件时钟与系统时钟等两种时钟。硬件时钟是指主机板上的时钟设备，也就是通常可在BIOS画面设定的时钟。系统时钟则是指kernel中的时钟。当Linux启动时，系统时钟会去读取硬件时钟的设定，之后系统时钟即独立运作。所有Linux相关指令与函数都是读取系统时钟的设定。

**语法格式：**hwclock [参数]

**常用参数：**

| --debug                   | 显示hwclock执行时详细的信息。                                |
| ------------------------- | ------------------------------------------------------------ |
| --directisa               | hwclock预设从/dev/rtc设备来存取硬件时钟。若无法存取时，可用此参数直接以I/O指令来存取硬件时钟。 |
| --hctosys                 | 将系统时钟调整为与目前的硬件时钟一致。                       |
| --set --date=<日期与时间> | 设定硬件时钟。                                               |
| --show                    | 显示硬件时钟的时间与日期。                                   |
| --systohc                 | 将硬件时钟调整为与目前的系统时钟一致。                       |
| --test                    | 仅测试程序，而不会实际更改硬件时钟。                         |
| --utc                     | 若要使用格林威治时间，请加入此参数，hwclock会执行转换的工作。 |
| --version                 | 显示版本信息。                                               |

**参考实例**

```shell
linux@ubuntu:~$ sudo hwclock    # 查看硬件时间
2023-08-10 12:00:07.760036+0800    
linux@ubuntu:~$ sudo hwclock --hctosys   # 把硬件时间更新到系统时间去 
linux@ubuntu:~$ date
2022年 08月 26日 星期五 16:02:24 CST   # 没有更新成功, 是因为我们系统有设置联网校正时间 
linux@ubuntu:~$ date
2022年 08月 26日 星期五 16:02:31 CST
linux@ubuntu:~$ hwclock 
hwclock: 无法通过已知的任何方式访问硬件时钟。
hwclock: 请使用 --debug 选项来观察程序搜索访问方法的详情。
linux@ubuntu:~$ sudo hwclock 
2022-08-26 16:02:40.743598+0800
linux@ubuntu:~$ sudo hwclock --set --date="20230810 12:00:00"   # 关闭网络和自动校正时间选项 , 并设置时间
linux@ubuntu:~$ sudo hwclock 
2023-08-10 12:00:05.009811+0800
linux@ubuntu:~$ sudo hwclock --hctosys  # hclock  to system , 硬件时间同步到系统时间
linux@ubuntu:~$ date                    # 系统时间改变
2023年 08月 10日 星期四 12:00:19 CST
linux@ubuntu:~$ date -s "20220827 18:10:30"  # 再次修改系统时间 
date: 无法设置日期: 不允许的操作
2022年 08月 27日 星期六 18:10:30 CST
linux@ubuntu:~$ sudo date -s "20220827 18:10:30"  #需要使用sudo 
[sudo] linux 的密码： 
2022年 08月 27日 星期六 18:10:30 CST
linux@ubuntu:~$ sudo hwclock 
2023-08-10 12:01:11.103429+0800
linux@ubuntu:~$ sudo hwclock --systohc    # system to  hwclock 把系统时间同步到硬件时间内
linux@ubuntu:~$ sudo hwclock 
2022-08-27 18:11:01.400677+0800
linux@ubuntu:~$ 
```

#### 3. tzslect时区的设置命令

​	tzselect命令在调用时不需要任何参数，它显示了一个由十几个地理区域组成的列表，人们可以粗略地将其视为大陆。按编号选择一个地理区域后，会显示这个区域的国家和城市列表。

**语法格式：**tzselect [参数]

**常用参数：**

| -c COORD | 根据自定义数值设置时区                      |
| -------- | ------------------------------------------- |
| -n LIMIT | 当使用-c时，显示大多数LIMIT位置（默认为10） |

**参考实例**

```shell
linux@ubuntu:~$ tzselect 
Please identify a location so that time zone rules can be set correctly.
Please select a continent, ocean, "coord", or "TZ".
 1) Africa
 2) Americas
 3) Antarctica
 4) Asia
 5) Atlantic Ocean
 6) Australia
 7) Europe
 8) Indian Ocean
 9) Pacific Ocean
10) coord - I want to use geographical coordinates.
11) TZ - I want to specify the time zone using the Posix TZ format.
#? 4
Please select a country whose clocks agree with yours.
 1) Afghanistan		  18) Iraq		    35) Pakistan
 2) Antarctica		  19) Israel		    36) Palestine
 3) Armenia		  20) Japan		    37) Philippines
 4) Azerbaijan		  21) Jordan		    38) Qatar
 5) Bahrain		  22) Kazakhstan	    39) Russia
 6) Bangladesh		  23) Korea (North)	    40) Saudi Arabia
 7) Bhutan		  24) Korea (South)	    41) Singapore
 8) Brunei		  25) Kuwait		    42) Sri Lanka
 9) Cambodia		  26) Kyrgyzstan	    43) Syria
10) China		  27) Laos		    44) Taiwan
11) Cyprus		  28) Lebanon		    45) Tajikistan
12) East Timor		  29) Macau		    46) Thailand
13) Georgia		  30) Malaysia		    47) Turkmenistan
14) Hong Kong		  31) Mongolia		    48) United Arab Emirates
15) India		  32) Myanmar (Burma)	    49) Uzbekistan
16) Indonesia		  33) Nepal		    50) Vietnam
17) Iran		  34) Oman		    51) Yemen
#? 10
Please select one of the following time zone regions.
1) Beijing Time
2) Xinjiang Time
#? 1

The following information has been given:

	China
	Beijing Time

Therefore TZ='Asia/Shanghai' will be used.
Selected time is now:	2022年 08月 26日 星期五 16:20:08 CST.
Universal Time is now:	2022年 08月 26日 星期五 08:20:08 UTC.
Is the above information OK?
1) Yes
2) No
#? 1

You can make this change permanent for yourself by appending the line
	TZ='Asia/Shanghai'; export TZ
to the file '.profile' in your home directory; then log out and log in again.

Here is that TZ value again, this time on standard output so that you
can use the /usr/bin/tzselect command in shell scripts:
Asia/Shanghai
linux@ubuntu:~$   # 重启

```

#### 4. ntpdate时间同步命令

​	ntpdate命令是用来设置本地日期和时间。它从指定的每个服务器获得了一些样本，并应用标准 NTP 时钟过滤器和选择算法来选择最好的样本。

使用很多服务器可以大幅度改善 ntpdate 命令的可靠性与精度。尽管能使用单一服务器，但您能通过提供至少三个或四个服务器以获得更好的性能。

**语法格式：**ntpdate [参数]

**常用参数：**

| -aKeyid | 使用 Keyid 来认证全部数据包                  |
| ------- | -------------------------------------------- |
| -b      | 通过调用 settimeofday 子例程来增加时钟的时间 |

**参考实例**

```shell
#中国科学院国家授时中心NTP授时服务器地址：
# ntp.ntsc.ac.cn
linux@ubuntu:~$ sudo ntpdate ntp.tencent.com
{"time":"2022-08-26T16:33:01.731045-0800","offset":0.010365,"precision":0.024938,"host":"ntp.tencent.com",ip:"139.199.215.251","stratum":2,"leap":"no-leap","adjusted":false}
linux@ubuntu:~$ sudo ntpdate ntp.tencent.com
{"time":"2022-08-26T16:33:02.704815-0800","offset":0.009713,"precision":0.025955,"host":"ntp.tencent.com",ip:"139.199.215.251","stratum":2,"leap":"no-leap","adjusted":false}
linux@ubuntu:~$ sudo ntpdate ntp.tencent.com
{"time":"2022-08-26T16:33:03.540055-0800","offset":0.002589,"precision":0.025091,"host":"ntp.tencent.com",ip:"139.199.215.251","stratum":2,"leap":"no-leap","adjusted":false}
linux@ubuntu:~$ 
```



## 第13章 系统服务管理

### **13.1 由来**

历史上Linux的启动一直采用init进程，下面的命令用来启动服务。

```
$ sudo /etc/init.d/apache2 start
#或者
$ service apache2 start
```

这种方法有两个缺点:

一是启动时间长。init进程是串行启动，只有前一个进程启动完，才会启动下一个进程。
二是启动脚本复杂。init进程只是执行启动脚本，不管其他事情。脚本需要自己处理各种情况，这往往使得脚本变得很长。

### **13.2 Systemd概述**

Systemd就是为了解决这些问题而诞生的。它的设计目标是，为系统的启动和管理提供一套完整的解决方案，根据Linux惯例，字母d是守护进程（daemon）的缩写，Systemd这个名字的含义，就是它要守护整个系统。

![Systemd](https://gitee.com/embmaker/cloudimage/raw/master/img/systemd_author_Lennart-Poettering.jpg)

​																		（上图为Systemd作者Lennart Poettering）

使用了Systemd，就不需要再用init了。Systemd取代了initd，成为系统的第一个进程（PID 等于 1），其他进程都是它的子进程。

```shell
# systemctl --version  #命令查看Systemd的版本。
linux@ubuntu:~$ systemctl --version 
systemd 237
+PAM +AUDIT +SELINUX +IMA +APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD -IDN2 +IDN -PCRE2 default-hierarchy=hybrid
linux@ubuntu:~$ 

```

Systemd的优点是功能强大，使用方便，缺点是体系庞大，非常复杂。事实上，现在还有很多人反对使用Systemd，理由就是它过于复杂，与操作系统的其他部分强耦合，违反”keep simple, keep stupid”的Unix哲学。

![Systemd入门教程：命令篇Systemd入门教程：命令篇](https://www.linuxprobe.com/wp-content/uploads/2016/05/systemd_framework.png)

​																					（上图为Systemd架构图）

### **13.3  系统管理**

Systemd并不是一个命令，而是一组命令，涉及到系统管理的方方面面。

#### **1 systemctl 是Systemd的主命令，用于管理系统**

```shell
#重启系统
$ sudo systemctl reboot

#关闭系统，切断电源
$ sudo systemctl poweroff

#CPU停止工作
$ sudo systemctl halt

#暂停系统
$ sudo systemctl suspend

#让系统进入冬眠状态
$ sudo systemctl hibernate

#让系统进入交互式休眠状态
$ sudo systemctl hybrid-sleep

#启动进入救援状态（单用户状态）
$ sudo systemctl rescue
```

#### **2 systemd-analyze 命令用于查看启动耗时**

```shell
#查看启动耗时
$ systemd-analyze                                                                                       

#查看每个服务的启动耗时
$ systemd-analyze blame

#显示瀑布状的启动过程流
$ systemd-analyze critical-chain

#显示指定服务的启动流
$ systemd-analyze critical-chain atd.service
```

#### **3 hostnamectl 命令用于查看当前主机的信息**

```shell
#显示当前主机的信息
$ hostnamectl

#设置主机名。
$ sudo hostnamectl set-hostname rhel7
```

#### **4 localectl 命令用于查看本地化设置**

```shell
#查看本地化设置
$ localectl

#设置本地化参数。
$ sudo localectl set-locale LANG=en_GB.utf8
$ sudo localectl set-keymap en_GB
```

#### **5 timedatectl 命令用于查看当前时区设置**

```shell
#查看当前时区设置
$ timedatectl

#显示所有可用的时区
$ timedatectl list-timezones                                                                                   

#设置当前时区
$ sudo timedatectl set-timezone America/New_York
$ sudo timedatectl set-time YYYY-MM-DD
$ sudo timedatectl set-time HH:MM:SS
```

**3.6 loginctl 命令用于查看当前登录的用户**

```shell
#列出当前session
$ loginctl list-sessions

#列出当前登录用户
$ loginctl list-users

#列出显示指定用户的信息
$ loginctl show-user ruanyf
```

### **13.4  Unit**

#### **1 含义**

Systemd可以管理所有系统资源，不同的资源统称为 Unit（单位）,Unit一共分成以下12种。

```shell
Service unit：系统服务
Target unit：多个Unit构成的一个组
Device Unit：硬件设备
Mount Unit：文件系统的挂载点
Automount Unit：自动挂载点
Path Unit：文件或路径
Scope Unit：不是由Systemd启动的外部进程
Slice Unit：进程组
Snapshot Unit：Systemd快照，可以切回某个快照
Socket Unit：进程间通信的socket
Swap Unit：swap文件
Timer Unit：定时器
```

systemctl list-units命令可以查看当前系统的所有Unit。

```shell
#列出正在运行的Unit
$ systemctl list-units

#列出所有Unit，包括没有找到配置文件的或者启动失败的
$ systemctl list-units --all

#列出所有没有运行的Unit
$ systemctl list-units --all --state=inactive

#列出所有加载失败的Unit
$ systemctl list-units --failed

#列出所有正在运行的、类型为service的Unit
$ systemctl list-units --type=service
```

#### **2 Unit的状态**

systemctl status命令用于查看系统状态和单个Unit的状态。

```shell
#显示系统状态
$ systemctl status

#显示单个Unit的状态
$ sysystemctl status bluetooth.service
```

实例

```shell
linux@ubuntu:~$ sudo systemctl status network-manager.service   # 查看网络管理服务状态 
● NetworkManager.service - Network Manager
   Loaded: loaded (/lib/systemd/system/NetworkManager.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-08-29 09:05:32 CST; 8min ago
     Docs: man:NetworkManager(8)
 Main PID: 667 (NetworkManager)
    Tasks: 3 (limit: 2283)
   CGroup: /system.slice/NetworkManager.service
           └─667 /usr/sbin/NetworkManager --no-daemon

8月 29 09:05:32 ubuntu NetworkManager[667]: <info>  [1661735132.8552] device (ens33): Activation: successful, device activated.
8月 29 09:05:32 ubuntu NetworkManager[667]: <info>  [1661735132.8562] devices added (path: /sys/devices/pci0000:00/0000:00:11.0/0000:02:01.0/net/ens33, i
8月 29 09:05:32 ubuntu NetworkManager[667]: <info>  [1661735132.8562] device added (path: /sys/devices/pci0000:00/0000:00:11.0/0000:02:01.0/net/ens33, if
8月 29 09:05:32 ubuntu NetworkManager[667]: <info>  [1661735132.8569] manager: startup complete
8月 29 09:05:33 ubuntu NetworkManager[667]: <info>  [1661735133.3406] devices added (path: /sys/devices/virtual/net/lo, iface: lo)
8月 29 09:05:33 ubuntu NetworkManager[667]: <info>  [1661735133.3406] device added (path: /sys/devices/virtual/net/lo, iface: lo): no ifupdown configurat
8月 29 09:05:33 ubuntu NetworkManager[667]: <info>  [1661735133.4685] bluez: use BlueZ version 5
8月 29 09:05:33 ubuntu NetworkManager[667]: <info>  [1661735133.4723] bluez5: NAP: added interface 18:CC:18:9F:7F:B7
8月 29 09:05:34 ubuntu NetworkManager[667]: <info>  [1661735134.9764] policy: set 'netplan-ens33' (ens33) as default for IPv6 routing and DNS
8月 29 09:10:34 ubuntu NetworkManager[667]: <info>  [1661735434.5635] manager: NetworkManager state is now CONNECTED_GLOBAL
linux@ubuntu:~$ 

```

除了status命令，systemctl还提供了三个查询状态的简单方法，主要供脚本内部的判断语句使用。

```shell
#显示某个Unit是否正在运行
$ systemctl is-active application.service

#显示某个Unit是否处于启动失败状态
$ systemctl is-failed application.service

#显示某个Unit服务是否建立了启动链接
$ systemctl is-enabled application.service
```

#### **3 Unit管理**

对于用户来说，最常用的是下面这些命令，用于启动和停止Unit（主要是service）。

```shell
#立即启动一个服务
$ sudo systemctl start apache.service

#立即停止一个服务
$ sudo systemctl stop apache.service

#重启一个服务
$ sudo systemctl restart apache.service

#杀死一个服务的所有子进程
$ sudo systemctl kill apache.service

#重新加载一个服务的配置文件
$ sudo systemctl reload apache.service

#重载所有修改过的配置文件
$ sudo systemctl daemon-reload

#显示某个 Unit 的所有底层参数
$ systemctl show httpd.service

#显示某个 Unit 的指定属性的值
$ systemctl show -p CPUShares httpd.service

#设置某个 Unit 的指定属性
$ sudo systemctl set-property httpd.service CPUShares=500
```

#### **4 依赖关系**

Unit之间存在依赖关系：A依赖于B，就意味着Systemd在启动A的时候，同时会去启动B。
systemctl list-dependencies命令列出一个Unit的所有依赖。

```
$ systemctl list-dependencies nginx.service
```

上面命令的输出结果之中，有些依赖是Target类型（详见下文），默认不会展开显示。如果要展开Target，就需要使用--all参数。

```
$ systemctl list-dependencies --all nginx.service
```



### 13.4 服务管理实例 ssh 服务管理实例 

```shell
1. 查看系统是否安装ssh服务 
linux@ubuntu:~$ sudo apt policy  ssh
ssh:
  已安装：(无)
  候选： 1:7.6p1-4ubuntu0.7
  版本列表：
     1:7.6p1-4ubuntu0.7 500
        500 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 Packages
        500 http://mirrors.yun-idc.com/ubuntu bionic-updates/main i386 Packages
     1:7.6p1-4ubuntu0.5 500
        500 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 Packages
        500 http://mirrors.yun-idc.com/ubuntu bionic-security/main i386 Packages
     1:7.6p1-4 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 Packages
        500 http://mirrors.yun-idc.com/ubuntu bionic/main i386 Packages
linux@ubuntu:~$ 

2. 安装ssh服务
linux@ubuntu:~$ sudo apt install openssh-client openssh-server 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
openssh-client 已经是最新版 (1:7.6p1-4ubuntu0.7)。
openssh-client 已设置为手动安装。
将会同时安装下列软件：
  ncurses-term openssh-sftp-server ssh-import-id
建议安装：
  molly-guard monkeysphere rssh ssh-askpass
下列【新】软件包将被安装：
  ncurses-term openssh-server openssh-sftp-server ssh-import-id
升级了 0 个软件包，新安装了 4 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
需要下载 637 kB 的归档。
解压缩后会消耗 5,320 kB 的额外空间。
您希望继续执行吗？ [Y/n] y
获取:1 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 ncurses-term all 6.1-1ubuntu1.18.04 [248 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 openssh-sftp-server amd64 1:7.6p1-4ubuntu0.7 [45.5 kB]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 openssh-server amd64 1:7.6p1-4ubuntu0.7 [332 kB]
获取:4 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 ssh-import-id all 5.7-0ubuntu1.1 [10.9 kB]
已下载 637 kB，耗时 0秒 (1,745 kB/s)      
正在预设定软件包 ...
正在选中未选择的软件包 ncurses-term。
(正在读取数据库 ... 系统当前共安装有 162789 个文件和目录。)
正准备解包 .../ncurses-term_6.1-1ubuntu1.18.04_all.deb  ...
正在解包 ncurses-term (6.1-1ubuntu1.18.04) ...
正在选中未选择的软件包 openssh-sftp-server。
正准备解包 .../openssh-sftp-server_1%3a7.6p1-4ubuntu0.7_amd64.deb  ...
正在解包 openssh-sftp-server (1:7.6p1-4ubuntu0.7) ...
正在选中未选择的软件包 openssh-server。
正准备解包 .../openssh-server_1%3a7.6p1-4ubuntu0.7_amd64.deb  ...
正在解包 openssh-server (1:7.6p1-4ubuntu0.7) ...
正在选中未选择的软件包 ssh-import-id。
正准备解包 .../ssh-import-id_5.7-0ubuntu1.1_all.deb  ...
正在解包 ssh-import-id (5.7-0ubuntu1.1) ...
正在设置 ncurses-term (6.1-1ubuntu1.18.04) ...
正在设置 openssh-sftp-server (1:7.6p1-4ubuntu0.7) ...
正在设置 ssh-import-id (5.7-0ubuntu1.1) ...
正在设置 openssh-server (1:7.6p1-4ubuntu0.7) ...

Creating config file /etc/ssh/sshd_config with new version
Creating SSH2 RSA key; this may take some time ...
2048 SHA256:jUn3b6PtMmeIM+/6wTvqQ3j3xBSKxDamhK0EchpYBvg root@ubuntu (RSA)
Creating SSH2 ECDSA key; this may take some time ...
256 SHA256:g+rxrkFtBCIwTyzY4xIPvtftDh8f04+ydYM6SFGdmBI root@ubuntu (ECDSA)
Creating SSH2 ED25519 key; this may take some time ...
256 SHA256:HhvgYDtn7L5Ay0wBbY4G/ekp608AEF1nRfOZF8VJc3g root@ubuntu (ED25519)
Created symlink /etc/systemd/system/sshd.service → /lib/systemd/system/ssh.service.
Created symlink /etc/systemd/system/multi-user.target.wants/ssh.service → /lib/systemd/system/ssh.service.
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在处理用于 ufw (0.36-0ubuntu0.18.04.2) 的触发器 ...
正在处理用于 ureadahead (0.100.0-21) 的触发器 ...
正在处理用于 systemd (237-3ubuntu10.53) 的触发器 ...
linux@ubuntu:~$ 

3. 查看sshd 服务是否启动
linux@ubuntu:~$ sudo systemctl status  sshd
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-08-29 09:22:52 CST; 57s ago
 Main PID: 2815 (sshd)
    Tasks: 1 (limit: 2283)
   CGroup: /system.slice/ssh.service
           └─2815 /usr/sbin/sshd -D

8月 29 09:22:52 ubuntu systemd[1]: Starting OpenBSD Secure Shell server...
8月 29 09:22:52 ubuntu sshd[2815]: Server listening on 0.0.0.0 port 22.
8月 29 09:22:52 ubuntu sshd[2815]: Server listening on :: port 22.
8月 29 09:22:52 ubuntu systemd[1]: Started OpenBSD Secure Shell server.

4. 关闭服务 
linux@ubuntu:~$ sudo systemctl stop sshd.service  
linux@ubuntu:~$ sudo systemctl status  sshd
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
   Active: inactive (dead) since Mon 2022-08-29 09:25:36 CST; 3s ago
  Process: 2815 ExecStart=/usr/sbin/sshd -D $SSHD_OPTS (code=exited, status=0/SUCCESS)
 Main PID: 2815 (code=exited, status=0/SUCCESS)

8月 29 09:22:52 ubuntu systemd[1]: Starting OpenBSD Secure Shell server...
8月 29 09:22:52 ubuntu sshd[2815]: Server listening on 0.0.0.0 port 22.
8月 29 09:22:52 ubuntu sshd[2815]: Server listening on :: port 22.
8月 29 09:22:52 ubuntu systemd[1]: Started OpenBSD Secure Shell server.
8月 29 09:25:36 ubuntu systemd[1]: Stopping OpenBSD Secure Shell server...
8月 29 09:25:36 ubuntu sshd[2815]: Received signal 15; terminating.
8月 29 09:25:36 ubuntu systemd[1]: Stopped OpenBSD Secure Shell server.
linux@ubuntu:~$ 
5. 关闭自动运行服务 
linux@ubuntu:~$ sudo systemctl  disable ssh.service 
Synchronizing state of ssh.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install disable ssh
Removed /etc/systemd/system/sshd.service.
linux@ubuntu:~$ 
linux@ubuntu:~$ sudo systemctl status  ssh.service 
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; disabled; vendor preset: enabled)
   Active: inactive (dead)

8月 29 09:22:52 ubuntu systemd[1]: Starting OpenBSD Secure Shell server...
8月 29 09:22:52 ubuntu sshd[2815]: Server listening on 0.0.0.0 port 22.
8月 29 09:22:52 ubuntu sshd[2815]: Server listening on :: port 22.
8月 29 09:22:52 ubuntu systemd[1]: Started OpenBSD Secure Shell server.
8月 29 09:25:36 ubuntu systemd[1]: Stopping OpenBSD Secure Shell server...
8月 29 09:25:36 ubuntu sshd[2815]: Received signal 15; terminating.
8月 29 09:25:36 ubuntu systemd[1]: Stopped OpenBSD Secure Shell server.
linux@ubuntu:~$ sudo systemctl reboot #系统重启后, 查看ssh服务是否启动
linux@ubuntu:~$ sudo systemctl status  ssh.service 
[sudo] linux 的密码： 
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; disabled; vendor preset: enabled)
   Active: inactive (dead)
linux@ubuntu:~$ 

6. 设置开机自动运行服务
linux@ubuntu:~$ sudo systemctl enable  ssh.service  # 设置开机自动运行服务
Synchronizing state of ssh.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable ssh
Created symlink /etc/systemd/system/sshd.service → /lib/systemd/system/ssh.service.
linux@ubuntu:~$ sudo systemctl status  ssh.service 
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
   Active: inactive (dead)
linux@ubuntu:~$ sudo systemctl reboot #系统重启后, 查看ssh服务是否启动
linux@ubuntu:~$ sudo systemctl status  ssh.service 
[sudo] linux 的密码： 
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-08-29 09:36:30 CST; 31s ago
  Process: 967 ExecReload=/bin/kill -HUP $MAINPID (code=exited, status=0/SUCCESS)
  Process: 961 ExecReload=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
  Process: 762 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
 Main PID: 780 (sshd)
    Tasks: 1 (limit: 2283)
   CGroup: /system.slice/ssh.service
           └─780 /usr/sbin/sshd -D

8月 29 09:36:30 ubuntu systemd[1]: Starting OpenBSD Secure Shell server...
8月 29 09:36:30 ubuntu sshd[780]: Server listening on 0.0.0.0 port 22.
8月 29 09:36:30 ubuntu sshd[780]: Server listening on :: port 22.
8月 29 09:36:30 ubuntu systemd[1]: Started OpenBSD Secure Shell server.
8月 29 09:36:30 ubuntu systemd[1]: Reloading OpenBSD Secure Shell server.
8月 29 09:36:30 ubuntu systemd[1]: Reloaded OpenBSD Secure Shell server.
8月 29 09:36:30 ubuntu sshd[780]: Received SIGHUP; restarting.
8月 29 09:36:30 ubuntu sshd[780]: Server listening on 0.0.0.0 port 22.
8月 29 09:36:30 ubuntu sshd[780]: Server listening on :: port 22.
linux@ubuntu:~$ 

```

## 第14章 Shell编程

### 14.1 Shell简介

随着各式Linux系统的图形化程度的不断提高，用户在桌面环境下，通过点击、拖拽等操作就可以完成大部分的工作。

然而，许多Ubuntu Linux功能使用Shell命令来实现，要比使用图形界面交互，完成的**更快、更直接**。 

英文单词Shell可直译为“贝壳”。“贝壳”是动物作为外在保护的一种工具。

可以这样认为，Linux中的Shell就是Linux内核的一个外层保护工具，并负责完成用户与内核之间的交互

![](https://gitee.com/embmaker/cloudimage/raw/master/img/02-linuxbase-01.png)

**命令**是用户向系统内核发出控制请求，与之交互的文本流。

**Shell是一个命令行解释器**，将用户命令解析为操作系统所能理解的指令，实现用户与操作系统的交互。

同时，Shell为操作系统提供了内核之上的功能，直接用来管理和运行系统。

当需要重复执行若干命令，可以将这些命令集合起来，加入一定的控制语句，编辑成为**Shell**脚本文件，交给Shell批量执行。

#### 1. **Shell交互过程**

1. 用户在命令行提示符下键入命令文本，开始与Shell进行交互。

2. 接着，Shell将用户的命令或按键转化成内核所能够理解的指令

3. 控制操作系统做出响应，直到控制相关硬件设备。

4. 然后，Shell将输出结果通过Shell提交给用户。

![image-20220829102905379](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220829102905379.png) 

#### **2. 选择Shell**

最初的UNIX Shell经过多年的发展，由不同的机构、针对不同的目的，开发出许多不同类型的Shell程序。目前流行的Shell主要有几种 :

**Bourne Shell**（简称**sh**）：Bourne Shell由AT&T贝尔实验室的S.R.Bourne开发，也因开发者的姓名而得名。它是Unix的第一个Shell程序，早已成为工业标准。目前几乎所有的Linux系统都支持它。不过Bourne Shell的作业控制功能薄弱，且不支持别名与历史记录等功能。目前大多操作系统是将其作为应急Shell使用。

**C Shell**（简称**csh**）：C Shell由加利福尼亚大学伯克利分校开发。最初开发的目的是改进Bourne Shell的一些缺点，并使Shell脚本的编程风格类似于C语言，因而受到广大C程序员的拥护。不过C Shell的健壮性不如Bourne Shell。

**Korn** **Shell**（简称**ksh**）：Korn Shell由David Korn开发，解决了Bourne Shell的用户交互问题，并克服了C Shell的脚本编程怪癖的缺点。Korn Shell的缺点是需要许可证，这导致它应用范围不如Bourne Shell广泛。

**Bourne Again Shell**（简称**bash**)：Bourne Again Shell由AT&T贝尔实验室开发，是Bourne Shell的增强版。随着几年的不断完善，已经成为最流行的Shell。它包括了早期的Bourne Shell和Korn Shell的原始功能，以及某些C Shell脚本语言的特性。此外，它还具有以下特点：能够提供环境变量以配置用户Shell环境，支持历史记录，内置算术功能，支持通配符表达式，将常用命令内置简化。

#### 2.  编译型语言与解释型语言

**编译型语言**:需要使用编译器进行编译,之后才可以执行的语言 , 源文件是不能直接执行的 

常用的编译型语言有: c , c++,java,c# ,vb , asm, go 

**解释型语言**:不需要编译, 直接可以运行的语言, 源文件可以直接运行的语言 

常用的解释性语言有: python , javascript ,PHP, sql , shell, html  

Shell脚本（角色扮演的剧本， 角本，脚本）语言是解释型语言。

Shell脚本的本质：Shell命令的有序集合。

### **14.2  shell 编程的基本过程**

1. 第一步 

建立 shell 文件, shell 文件的后缀名是.sh  , 或则不写 

包含任意多行操作系统命令或shell命令的文本文件

2. 赋予shell文件执行权限 (x)

  用chmod命令修改权限;

3. 执行shell文件

直接在命令行上调用shell程序

 例如:

- 源文件

```shell
02-linuxbase/01-hello.sh
```

- 源代码

```shell
#! /bin/bash

# 第一句话 表示那个可执行程序来解析shell 命令 

echo "hello world!!"

```

- 运行结果

```shell
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cd work/
linux@ubuntu:~/work$ ls
02-linuxbase
linux@ubuntu:~/work$ cd 02-linuxbase/
linux@ubuntu:~/work/02-linuxbase$ ls
linux@ubuntu:~/work/02-linuxbase$ vi 01-hello.sh
linux@ubuntu:~/work/02-linuxbase$ ls -l 01-hello.sh 
-rw-rw-r-- 1 linux linux 103 8月  29 10:49 01-hello.sh
linux@ubuntu:~/work/02-linuxbase$ ./01-hello.sh
bash: ./01-hello.sh: 权限不够
linux@ubuntu:~/work/02-linuxbase$ chmod a+x 01-hello.sh 
linux@ubuntu:~/work/02-linuxbase$ ls -l 01-hello.sh 
-rwxrwxr-x 1 linux linux 103 8月  29 10:49 01-hello.sh
linux@ubuntu:~/work/02-linuxbase$ ./01-hello.sh 
hello world!!
linux@ubuntu:~/work/02-linuxbase$ 
```

### **14.3 Shell变量**

Shell允许用户建立变量存储数据，但不支持数据类型（整型、字符、浮点型），将任何赋给变量的值都解释为一串字符

```shell
Variable=value     # 等号的左边和右边不能有空格
```

命名规则同C语言中的命名规则

```shell
count=1
echo $count    # $ 访问变量中的值 
DATE=`date`    #  反引号 是取变量的输出
echo $DATE
```

运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ DATE=`date`
linux@ubuntu:~/work/02-linuxbase$ $DATE
2022年：未找到命令
linux@ubuntu:~/work/02-linuxbase$ echo $DATE
2022年 08月 29日 星期一 11:12:17 CST
linux@ubuntu:~/work/02-linuxbase$ 
```

**Bourne Shell有如下四种变量**：

- 用户自定义变量 


- 位置变量即 命令行参数 


- 预定义变量 


- 环境变量



#### 1.  用户自定义变量 

在shell编程中通常使用全大写变量，方便识别 

```
$ COUNT=1
```

变量的调用：在变量前加$ 

```
$ echo $COUNT 
```

Linux Shell/bash从右向左赋值

```shell
$Y=y
$ X=$Y
$ echo $X 
y 
```

使用unset命令删除变量的赋值 

```shell
$ Z=hello 
$ echo $Z 
hello 
$ unset Z 
$ echo $Z 
```

#### 2. 位置变量

```shell
$0  与键入的命令行一样，包含脚本文件名
$1,$2,……$9  分别包含第一个到第九个命令行参数
$#   包含命令行参数的个数
$@   包含所有命令行参数：“$1,$2,……$9”
$?   包含前一个命令的退出状态
$*   包含所有命令行参数：“$1,$2,……$9”
$$   包含正在执行进程的ID号
```

例如:

- 源文件

```
02-weizhi.sh 
```

- 源代码

```shell
#! /bin/bash

echo "0=$0" 
echo "1=$1" 
echo "2=$2" 
echo "3=$3" 
echo "4=$4" 
echo "5=$5" 
echo "6=$6" 
echo "7=$7" 
echo "8=$8" 
echo "9=$9" 
echo "10=$10" 
echo "11=$11" 

echo "#=$#"
echo "@=$@"
echo "*=$*"

ls 
echo "ls -> ?=$?"   # 上一条命令执行的结果 0：成功 ， 1 失败 

cd /haha
echo "cd /haha -> ?=$?"   # 上一条命令执行的结果 0：成功 ， 1 失败 

echo "$ =$$"   # 程序的进程号 
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./weizhi.sh
0=./weizhi.sh
1=
2=
3=
4=
5=
6=
7=
8=
9=
linux@ubuntu:~/work/02-linuxbase$ ./weizhi.sh s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
0=./weizhi.sh
1=s1
2=s2
3=s3
4=s4
5=s5
6=s6
7=s7
8=s8
9=s9
linux@ubuntu:~/work/02-linuxbase$ ./weizhi.sh s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
0=./weizhi.sh
1=s1
2=s2
3=s3
4=s4
5=s5
6=s6
7=s7
8=s8
9=s9
10=s10
11=s11
linux@ubuntu:~/work/02-linuxbase$ ./weizhi.sh s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
0=./weizhi.sh
1=s1
2=s2
3=s3
4=s4
5=s5
6=s6
7=s7
8=s8
9=s9
10=s10
11=s11
#=11
linux@ubuntu:~/work/02-linuxbase$ vi weizhi.sh
linux@ubuntu:~/work/02-linuxbase$ ./weizhi.sh s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
0=./weizhi.sh
1=s1
2=s2
3=s3
4=s4
5=s5
6=s6
7=s7
8=s8
9=s9
10=s10
11=s11
#=11
@=s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
linux@ubuntu:~/work/02-linuxbase$ 
linux@ubuntu:~/work/02-linuxbase$ ./weizhi.sh 
0=./weizhi.sh
1=
2=
3=
4=
5=
6=
7=
8=
9=
10=0
11=1
#=0
@=
01-hello.sh  weizhi.sh
ls -> ?=0
./weizhi.sh: 第 23 行: cd: /haha: 没有那个文件或目录
cd /haha -> ?=1
linux@ubuntu:~/work/02-linuxbase$ ./weizhi.sh s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
0=./weizhi.sh
1=s1
2=s2
3=s3
4=s4
5=s5
6=s6
7=s7
8=s8
9=s9
10=s10
11=s11
#=11
@=s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
*=s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
01-hello.sh  weizhi.sh
ls -> ?=0
./weizhi.sh: 第 24 行: cd: /haha: 没有那个文件或目录
cd /haha -> ?=1
$ =2732
linux@ubuntu:~/work/02-linuxbase$ 

```

#### 3. 常用shell环境变量 

```shell
HOME： /etc/passwd文件中列出的用户主目录 
IFS：Internal Field Separator, 默认为空格，tab及换行符
PATH ：shell搜索路径
PS1，PS2：默认提示符($)及换行提示符(>) 
TERM：终端类型，常用的有vt100,ansi,vt200,xterm等 
```

实例23

- 输出环境变量的值

- 源程序

```
03-huanjing.sh 
```

- 源代码

```shell
#! /bin/bash

echo "HOME=$HOME"
echo "PATH=$PATH"
echo "PS1=$PS1"
echo "PS2=$PS2"
echo "TERM=$TERM"
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./03-huanjing.sh 
HOME=/home/linux
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
PS1=
PS2=
TERM=xterm-256color
linux@ubuntu:~/work/02-linuxbase$ 
```



### **14.4 shell 控制语句**

#### 1. Shell程序

shell 程序由零或多条shell语句构成。 shell语句包括三类:说明性语句、功能性语句和结构性语句。

**说明性语句**： 以#号开始到该行结束，不被解释执行

**功能性语句**:   任意的shell命令、用户程序或其它shell程序。

**结构性语句**：条件测试语句、多路分支语句、循环语句、循环控制语句等。       

**说明性语句 必不可少的:**       #! /bin/bash 

#### 2. 输入与输出

**输出**: echo 

**输入**: read 

```shell
read 命令
read 从标准输入读入一行, 并赋值给后面的变量, 等价于c语言的scanf 
其语法为:
  	read  var
把读入的数据全部赋给var
	read  var1  var2  var3
把读入行中的第一个单词(word)赋给var1, 第二个单词赋给var2， ……把其余所有的词赋给最后一个变量.
如果执行read语句时标准输入无数据, 则程序在此停留等侯, 直到数据的到来或被终止运行。
```

实例24 

- 源文件

```shell
04-read.sh 
```

- 源代码

```shell
#! /bin/bash


echo -n "please input 1 number >:"  # -n 的意思是， 去除\n， echo 不输出\n
read var1 

echo "var1 = $var1"

echo -n "please input 3 number >:"
read var1 var2 var3 

echo "var1 = $var1"
echo "var2 = $var2"
echo "var3 = $var3"

```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./04-read.sh 
please input 1 number >:123
var1 = 123
please input 3 number >:89
var1 = 89
var2 = 
var3 = 
linux@ubuntu:~/work/02-linuxbase$ ./04-read.sh 
please input 1 number >:1
var1 = 1
please input 3 number >:123 9 7 
var1 = 123
var2 = 9
var3 = 7
linux@ubuntu:~/work/02-linuxbase$ ./04-read.sh 
please input 1 number >:123
var1 = 123
please input 3 number >:1 2 3 4 5 5 6 76 7 
var1 = 1
var2 = 2
var3 = 3 4 5 5 6 76 7
linux@ubuntu:~/work/02-linuxbase$ 

```

#### 3. expr  命令 

算术运算命令expr主要用于进行简单的整数运算，包括加(+)、减（-）、乘（`\*`)、整除（/）和求模（%）等操作。

实例25:

- 计算2个数的加减乘除求余
- 源文件

```
05-expr.sh
```

- 源代码

```shell
#! /bin/bash

echo -n "please input 2 number >:"
read num1 num2
echo "num1=$num1 , num2=$num2"

ret=`expr $num1 + $num2`
echo "$num1 + $num2 = $ret"

ret=`expr $num1 - $num2`
echo "$num1 - $num2 = $ret"

ret=`expr $num1 \* $num2`
echo "$num1 * $num2 = $ret"

ret=`expr $num1 / $num2`
echo "$num1 / $num2 = $ret"

ret=`expr $num1 % $num2`
echo "$num1 % $num2 = $ret"
```



- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./05-expr.sh 
please input 2 number >:10 15
num1=10 , num2=15
10 + 15 = 25
10 - 15 = -5
10 * 15 = 150
10 / 15 = 0
10 % 15 = 10
linux@ubuntu:~/work/02-linuxbase$ 

```

#### **4. test命令** 

test语句可测试三种对象:

​      字符串   整数   文件属性

1. 字符串测试

```shell
s1 = s2     测试两个字符串的内容是否完全一样
s1 != s2    测试两个字符串的内容是否有差异
-z s1       测试s1 字符串的长度是否为0
-n s1       测试s1 字符串的长度是否不为0
```

2. 整数测试

```shell
a -eq b         测试a 与b 是否相等    # ==
a -ne b         测试a 与b 是否不相等  #  != 
a -gt b         测试a 是否大于b       # > 
a -ge b         测试a 是否大于等于b   # >=
a -lt b         测试a 是否小于b      # <
a -le b         测试a 是否小于等于b   # <=
```

3. 测试文件 

```shell
-e FILE    测试一个文件或目录时候存在 
-d name    测试name 是否为一个目录
-f name     测试name 是否为普通文件
-L name    测试name 是否为符号链接
-r name     测试name 文件是否存在且为可读
-w name     测试name 文件是否存在且为可写
-x name     测试name 文件是否存在且为可执行
-s name     测试name 文件是否存在且其长度不为0
f1 -nt f2   测试文件f1 是否比文件f2 更新
f1 -ot f2   测试文件f1 是否比文件f2 更旧
```



实例26:

- 测试2个字符串的关系
- 源文件

```
06-test-str.sh
```

- 源代码

```shell
#! /bin/bash


echo -n "please input 2 string >:"
read s1 s2
echo "s1=$s1 , s2=$s2"

if test -z $s1    # 判断s1字符串的长度不为0  
then 
    echo "s1 is empty"
    exit 1
fi 

if test -z $s2    # 判断s1字符串的长度不为0  
then 
    echo "s2 is empty"
    exit 1
fi 

if test $s1 = $s2 
then 
    echo "$s1 = $s2"
else 
    echo "$s1 != $s2"
fi 

```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./06-test-str.sh 
please input 2 string >:goodbye hello
s1=goodbye , s2=hello
goodbye != hello
linux@ubuntu:~/work/02-linuxbase$ ./06-test-str.sh 
please input 2 string >:goodbye goodbye
s1=goodbye , s2=goodbye
goodbye = goodbye
linux@ubuntu:~/work/02-linuxbase$ ./06-test-str.sh
please input 2 string >:
s1= , s2=
s1 is empty
```

实例27:

- 从终端输入的y/n , 读取用户输入的是y还是n   
- 源文件

```shell
07-test-yes-no.sh 
```

- 源代码

```shell
#! /bin/bash


echo -n "please input [y/n] >:"
read s1 
echo "s1=$s1"

if test -z $s1    # 判断s1字符串的长度不为0  
then 
    echo "s1 is empty"
    exit 1
fi 

if test $s1 = "y" 
then 
    echo "yes"
fi 
if test $s1 = "n" 
then 
    echo "no"
fi 


```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./07-test-yes-no.sh 
please input [y/n] >:y
s1=y
yes
linux@ubuntu:~/work/02-linuxbase$ ./07-test-yes-no.sh 
please input [y/n] >:n
s1=n
no
linux@ubuntu:~/work/02-linuxbase$ ./07-test-yes-no.sh 
please input [y/n] >:
s1=
s1 is empty
linux@ubuntu:~/work/02-linuxbase$ 
```

**实例28** 

- 输入2个数, 比较2个数之间的关系(> , >= , <, <=, ==, != )

- 源文件

```shell
08-test-numer.sh 
```

- 源代码

```shell
#! /bin/bash

echo -n "please input 2 number >:"
read n1  n2 
echo "n1=$n1 , n2=$n2"


if test $n1 -gt $n2  # >  
then 
    echo "$n1 > $n2"
fi 

if test $n1 -ge $n2   # >= 
then 
    echo "$n1 >= $n2"
fi 

if test $n1 -lt $n2   # < 
then 
    echo "$n1 < $n2"
fi 

if test $n1 -le $n2   # <= 
then 
    echo "$n1 <= $n2"
fi 

if test $n1 -eq $n2   # == 
then 
    echo "$n1 == $n2"
fi 

if test $n1 -ne $n2   # != 
then 
    echo "$n1 != $n2"
fi 
```



- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./08-test-numer.sh 
please input 2 number >:10 20
n1=10 , n2=20
10 < 20
10 <= 20
10 != 20
linux@ubuntu:~/work/02-linuxbase$ ./08-test-numer.sh 
please input 2 number >:10 10 
n1=10 , n2=10
10 >= 10
10 <= 10
10 == 10
linux@ubuntu:~/work/02-linuxbase$ ./08-test-numer.sh 
please input 2 number >:10 8
n1=10 , n2=8
10 > 8
10 >= 8
10 != 8
linux@ubuntu:~/work/02-linuxbase$
```

实例29 

- 测试文件的属性 , 文价是否存在 ,  文件的类型(BCD-LSP), 文件的读写权限(rwx)
- 源文件

```shell
09-test-file.sh 
```



- 源代码

```shell
#! /bin/bash


echo -n "please input filename >:"
read  filename 
echo "filename=$filename"

if ! test -e $filename   # 文件不存在则报错  
then 
    echo "file not existed"
    exit 1
fi 

if test -b $filename   # 块设备文件 
then 
    echo "block"
elif test -c $filename  # 字符设备文件
then 
    echo "character"
elif test -d $filename  #目录 
then 
    echo "directory"
elif test -h $filename   # 符号链接
then 
    echo "link"
elif test -f $filename   # 普通文件
then 
    echo "regular"
elif test -S $filename   # socket文件 
then 
    echo "socket"
elif test -p $filename   # pipe文件 
then 
    echo "pipe"
fi


if test -r $filename   # 可读 
then 
    echo "read"
fi

if test -w $filename   # 可写 
then 
    echo "write"
fi

if test -x $filename   # 可执行 
then 
    echo "exec"
fi
```



- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:/
filename=/
directory
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:/dev/sda
filename=/dev/sda
block
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:/dev/vca
filename=/dev/vca
file not existed
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:/dev/vcs
filename=/dev/vcs
character
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:09-test-file.sh
filename=09-test-file.sh
regular
linux@ubuntu:~/work/02-linuxbase$ ln -s 09 09-test-file.sh 
ln: 无法创建符号链接'09-test-file.sh': 文件已存在
linux@ubuntu:~/work/02-linuxbase$ ln -s 09-test-file.sh  09
linux@ubuntu:~/work/02-linuxbase$ ls -l 09
lrwxrwxrwx 1 linux linux 15 8月  30 10:41 09 -> 09-test-file.sh
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:09
filename=09
regular
linux@ubuntu:~/work/02-linuxbase$ vi 09-test-file.sh 
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:09
filename=09
link
linux@ubuntu:~/work/02-linuxbase$ mkfifo myfifo
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:myfifo
filename=myfifo
pipe
linux@ubuntu:~/work/02-linuxbase$ 
linux@ubuntu:~/work/02-linuxbase$ ./09-test-file.sh 
please input filename >:09-test-file.sh
filename=09-test-file.sh
regular
read
write
exec
linux@ubuntu:~/work/02-linuxbase$ 
linux@ubuntu:~/work/02-linuxbase$ ls -l 09-test-file.sh 
-rwxrwxr-x 1 linux linux 792 8月  30 10:46 09-test-file.sh
linux@ubuntu:~/work/02-linuxbase$ 
```



#### 5. test命令的替代写法[] 

在程序中可以 使用 [ ] 替代test 命令 , 这种用法就是 test 的缩写 

实例30 

- 把实例29的test命令用[ ] 替代 
- 源文件

```shell
10-test-file.sh 
```

- 源代码

```shell
#! /bin/bash


echo -n "please input filename >:"
read  filename 
echo "filename=$filename"

if ! [ -e $filename ]  # 文件不存在则报错  
then 
    echo "file not existed"
    exit 1
fi 

if [ -b $filename ]   # 块设备文件 
then 
    echo "block"
elif test -c $filename  # 字符设备文件
then 
    echo "character"
elif [ -d $filename ] #目录 
then 
    echo "directory"
elif [ -h $filename ]  # 符号链接
then 
    echo "link"
elif [ -f $filename ] # 普通文件
then 
    echo "regular"
elif [ -S $filename ]  # socket文件 
then 
    echo "socket"
elif [ -p $filename ]  # pipe文件 
then 
    echo "pipe"
fi


if [ -r $filename ]  # 可读 
then 
    echo "read"
fi

if [ -w $filename ]  # 可写 
then 
    echo "write"
fi

if [ -x $filename ]  # 可执行 
then 
    echo "exec"
fi
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./10-test-file.sh 
please input filename >:/
filename=/
directory
read
exec
linux@ubuntu:~/work/02-linuxbase$ ./10-test-file.sh 
please input filename >:09-test-file.sh
filename=09-test-file.sh
regular
read
write
exec
linux@ubuntu:~/work/02-linuxbase$ 
```



#### 6. 条件语句 if ... then ...fi  

语法结构:

```shell
if    表达式
then  
		命令表
fi     
```

如果表达式为真, 则执行命令表中的命令; 否则退出if语句, 即执行fi后面的语句。 

if和fi是条件语句的语句括号, 必须成对使用;

命令表中的命令可以是一条, 也可以是若干条。

#### 7. 条件语句  if…then…else…fi

语法结构为:

```shell
if      表达式
then 
	命令表1
else  
	命令表2
fi
```

如果表达式为真, 则执行命令表1中的命令; 

否则执行命令表2中的语句.

#### 8. 条件语句  if…then…elif ... then ... else…fi

语法结构为:



```shell
if     表达式1
then   
	命令表1
elif   表达式2
then   
	命令表2 
else   
	命令表3 
fi
```

如果表达式1为真, 则执行命令表1中的命令, 再退出if语句; 

否则执行命令表2中的语句, 再退出if语句.

#### 9. 多路分支语句  case…esac 

   多路分支语句case用于多重条件测试, 语法结构清晰自然.  其语法为:

```shell
 case   字符串变量   in
            模式1)
                       命令表1
                        ;;   # 这个分号不能少 
            模式2)
                       命令表2
                        ;;
             ……
            模式n)
                       命令表n
                        ;;
             *)
            ;;

 esac
```

实例31 

- 输入成绩的水平(A/B/C/D/E), 转换成成绩的范围 
- 源文件

```shell
11-case.sh 
```

- 源代码

```shell
#! /bin/bash



echo -n "please input your level(A/B/C/D/E) >:"
read level 
echo "level = $level"


case  $level  in 
    "A")
        echo "90 =< score <=100"
        ;;
    "B")
        echo "80 =< score < 90"
        ;;
    "C")
        echo "70 =< score < 80"
        ;;
    "D")
        echo "60 =< score < 70"
        ;;
    "E")
        echo " 0 =< score < 60"
        ;;
    *)
        ;;
esac


```



- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./11-case.sh 
please input your level(A/B/C/D/E) >:A
level = A
90 =< score <=100
linux@ubuntu:~/work/02-linuxbase$ ./11-case.sh 
please input your level(A/B/C/D/E) >:B
level = B
80 =< score < 90
linux@ubuntu:~/work/02-linuxbase$ ./11-case.sh 
please input your level(A/B/C/D/E) >:C
level = C
70 =< score < 80
linux@ubuntu:~/work/02-linuxbase$ ./11-case.sh 
please input your level(A/B/C/D/E) >:D
level = D
60 =< score < 70
linux@ubuntu:~/work/02-linuxbase$ ./11-case.sh 
please input your level(A/B/C/D/E) >:E
level = E
 0 =< score < 60
linux@ubuntu:~/work/02-linuxbase$ ./11-case.sh 
please input your level(A/B/C/D/E) >:8
level = 8
linux@ubuntu:~/work/02-linuxbase$ 
```

####  10. for…do…done

  当循环次数已知或确定时, 使用for循环语句来多次执行一条或一组命令。 循环体由语句括号do和done来限定。

 格式为:     

```shell
for  变量名  in  单词表
do
  命令表
done
```

变量依次取单词表中的各个单词,  每取一次单词, 就执行一次循环体中的命令.  循环次数由单词表中的单词数确定. 命令表中的命令可以是一条, 也可以是由分号或换行符分开的多条。

实例32 

- 源文件

```shell
12-for.sh 
```

- 源代码

```shell
#! /bin/bash


mylist=`ls`

if ! [ -e hello ]  # 判断当前目录下， hello目录是否存在
then 
    mkdir hello 
fi 

for file in $mylist 
do 
    if [ $file != hello ]    # 如果文件名不是hello ， 就复制， 是就不复制了
    then 
        cp $file hello 
    fi 

done 
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./12-for.sh 
linux@ubuntu:~/work/02-linuxbase$ ls
01-hello.sh   03-huanjing.sh  05-expr.sh      07-test-yes-no.sh  09-test-file.sh  11-case.sh  hello
02-weizhi.sh  04-read.sh      06-test-str.sh  08-test-numer.sh   10-test-file.sh  12-for.sh
linux@ubuntu:~/work/02-linuxbase$ ls hello/
01-hello.sh   03-huanjing.sh  05-expr.sh      07-test-yes-no.sh  09-test-file.sh  11-case.sh
02-weizhi.sh  04-read.sh      06-test-str.sh  08-test-numer.sh   10-test-file.sh  12-for.sh
linux@ubuntu:~/work/02-linuxbase$ 
```

#### 11. 循环语句 while…do…done

语法结构为： 

```shell
while     命令或表达式
do
	命令表
done
```

while语句首先测试其后的命令或表达式的值，如果为真，就执行一次循环体中的命令，然后再测试该命令或表达式的值，执行循环体，直到该命令或表达式为假时退出循环。

while语句的退出状态为命令表中被执行的最后一条命令的退出状态。

实例32:

- 计算1+2+3+4+...+100 之和 
- 源文件

```shell
13-while.sh 
```

- 源代码

```shell
#! /bin/bash


i=0
sum=0
while  [ $i -lt 100 ] 
do 
    i=`expr $i + 1 `    # i = i + 1 
    echo "i=$i"
    sum=`expr $sum + $i `
done
echo "sum=$sum"
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./13-while.sh 
i=1
i=2
i=3
i=4
i=5
i=6
...
i=89
i=90
i=91
i=92
i=93
i=94
i=95
i=96
i=97
i=98
i=99
i=100
sum=5050
linux@ubuntu:~/work/02-linuxbase$
```

#### 12. 循环语句 until…do…done

语法结构为:

```shell
until   命令或表达式
do
	命令表
done
```

until循环与while循环的功能相似,  所不同的是只有当测试的命令或表达式的值是假时, 才执行循环体中的命令表, 否则退出循环.  这一点与while命令正好相反.

实例34

- 计算1+2+3+4+...+100 之和 
- 源文件

```
14-until.sh
```

- 源代码

```shell
#! /bin/bash

i=0
sum=0
until ! [ $i -lt 100 ] 
do 
    i=`expr $i + 1 `    # i = i + 1 
    echo "i=$i"
    sum=`expr $sum + $i `
done
echo "sum=$sum"
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./14-until.sh 
i=1
i=2
i=3
i=4
i=5
i=6
...
i=90
i=91
i=92
i=93
i=94
i=95
i=96
i=97
i=98
i=99
i=100
sum=5050
linux@ubuntu:~/work/02-linuxbase$ 
```

#### 13. break 和 continue 

break n 则跳出n层; 

 continue语句则马上转到最近一层循环语句的*下一轮*循环上, 

continue n则转到最近n层循环语句的下一轮循环上.

实例35 

- 计算一天有多少秒 
- 源文件

```shell
15-break-continue.sh
```

- 源代码

```shell
#! /bin/bash

sec=0
min=0
hour=0
count_sec=0
while  true 
do 
    echo "HH:MM:SS -> $hour:$min:$sec"
    count_sec=`expr $count_sec + 1`
    sec=`expr $sec + 1`
    if [ $sec -lt 60 ]
    then 
        continue 
    fi 
    sec=0
    min=`expr $min + 1`
    if [ $min -lt 60 ]
    then 
        continue 
    fi 
    min=0
    hour=`expr $hour + 1`
    if [ $hour -lt 24 ]
    then 
        continue 
    fi 
    hour=0
    break

done  
echo "count_sec=$count_sec"
```

- 运行结果

```shell
...
HH:MM:SS -> 23:59:46
HH:MM:SS -> 23:59:47
HH:MM:SS -> 23:59:48
HH:MM:SS -> 23:59:49
HH:MM:SS -> 23:59:50
HH:MM:SS -> 23:59:51
HH:MM:SS -> 23:59:52
HH:MM:SS -> 23:59:53
HH:MM:SS -> 23:59:54
HH:MM:SS -> 23:59:55
HH:MM:SS -> 23:59:56
HH:MM:SS -> 23:59:57
HH:MM:SS -> 23:59:58
HH:MM:SS -> 23:59:59
count_sec=86400
linux@ubuntu:~/work/02-linuxbase$ 
```

### 14.5 shell 函数

#### 1. 函数

​    在shell程序中, 常常把完成固定功能、且多次使用的一组命令（语句）封装在一个函数里，每当要使用该功能时只需调用该函数名即可。

​    函数在调用前必须先定义，即在顺序上函数说明必须放在调用程序的前面。

​    调用程序可传递参数给函数, 函数可用return语句把运行结果返回给调用程序。

​    函数只在当前shell中起作用, 不能输出到子Shell中。

函数定义格式:

方式一:

```shell
function_name ( )
{
            command1
            ……
            commandn
}
```

方式二 :

```shell
function   function_name ( )
{
            command1
            ……
            commandn
}
```

实例35 

- 使用函数计算2个数的 加减乘除求余
- 源文件

```shell
16-func.sh 
```



- 源代码

```shell
#! /bin/bash


add()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 + $2`
    return $ret 
}
sub()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 - $2`
    return $ret 
}

mul()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 \* $2`
    return $ret 
}
div()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 / $2`
    return $ret 
}
yu()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 % $2`
    return $ret 
}



echo -n "please input 2 number >:"
read n1 n2 
echo "n1=$n1 , n2=$n2"

if [ -z $n1 ] || [ -z $n2 ]
then 
    echo "n1 or n2 empty"
fi 

add $n1 $n2     
# 位置变量
# $0 = add 
# $1 = $n1 
# $2 = $n2
# 函数的返回值 通过$? 获取 
echo "$n1 + $n2 = $? "

sub $n1 $n2     
echo "$n1 - $n2 = $? "

mul $n1 $n2     
echo "$n1 * $n2 = $? "

div $n1 $n2     
echo "$n1 / $n2 = $? "

yu $n1 $n2     
echo "$n1 % $n2 = $? "

```



- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./16-func.sh 
please input 2 number >:10 4
n1=10 , n2=4
1=10
2=4
10 + 4 = 14 
1=10
2=4
10 - 4 = 6 
1=10
2=4
10 * 4 = 40 
1=10
2=4
10 / 4 = 2 
1=10
2=4
10 % 4 = 2 
linux@ubuntu:~/work/02-linuxbase$ 
```

#### 2. 局部变量与全局变量

声明局部变量的格式：
local   variable_name =value
全局作用域：在脚本的其他任何地方都能够访问该变量。
variable_name =value

实例36

- 全局变量和局部变量的使用
- 源文件

```
17-global.sh 
```

- 源代码

```shell
#! /bin/bash

number1=200    # 全局变量

add()
{
    local number2=100   # 局部变量 
    echo "add:number1=$number1"
    echo "add:number2=$number2"

    ret=`expr $1 + $2`
    return $ret 
}



echo "number1=$number1"
echo "number2=$number2"

add  1 2 

```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./17-global.sh 
number1=200
number2=
add:number1=200
add:number2=100
linux@ubuntu:~/work/02-linuxbase$ 

```



### 14.6 shell 调试方法

分析输出的错误信息。

通过在脚本中加入调试语句，输出调试信息来辅助诊断错误。

功能:set -x , set +x 跟踪程序的每一步的执行结果

实例37

- 源文件

```shell
18-debug.sh 
```



- 源代码

```shell
#! /bin/bash

set -x 
add()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 + $2`
    return $ret 
}
sub()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 - $2`
    return $ret 
}

mul()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 \* $2`
    return $ret 
}
div()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 / $2`
    return $ret 
}
yu()
{
    echo "1=$1"
    echo "2=$2"
    ret=`expr $1 % $2`
    return $ret 
}






echo -n "please input 2 number >:"
read n1 n2 
echo "n1=$n1 , n2=$n2"

if [ -z $n1 ] || [ -z $n2 ]
then 
    echo "n1 or n2 empty"
fi 

add $n1 $n2     
# 位置变量
# $0 = add 
# $1 = $n1 
# $2 = $n2
# 函数的返回值 通过$? 获取 
echo "$n1 + $n2 = $? "

sub $n1 $n2     
echo "$n1 - $n2 = $? "

mul $n1 $n2     
echo "$n1 * $n2 = $? "

div $n1 $n2     
echo "$n1 / $n2 = $? "

yu $n1 $n2     
echo "$n1 % $n2 = $? "


set +x 
```



- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase$ ./18-debug.sh 
+ echo -n 'please input 2 number >:'
please input 2 number >:+ read n1 n2
1 4
+ echo 'n1=1 , n2=4'
n1=1 , n2=4
+ '[' -z 1 ']'
+ '[' -z 4 ']'
+ add 1 4
+ echo 1=1
1=1
+ echo 2=4
2=4
++ expr 1 + 4
+ ret=5
+ return 5
+ echo '1 + 4 = 5 '
1 + 4 = 5 
+ sub 1 4
+ echo 1=1
1=1
+ echo 2=4
2=4
++ expr 1 - 4
+ ret=-3
+ return -3
+ echo '1 - 4 = 253 '
1 - 4 = 253 
+ mul 1 4
+ echo 1=1
1=1
+ echo 2=4
2=4
++ expr 1 '*' 4
+ ret=4
+ return 4
+ echo '1 * 4 = 4 '
1 * 4 = 4 
+ div 1 4
+ echo 1=1
1=1
+ echo 2=4
2=4
++ expr 1 / 4
+ ret=0
+ return 0
+ echo '1 / 4 = 0 '
1 / 4 = 0 
+ yu 1 4
+ echo 1=1
1=1
+ echo 2=4
2=4
++ expr 1 % 4
+ ret=1
+ return 1
+ echo '1 % 4 = 1 '
1 % 4 = 1 
+ set +x
linux@ubuntu:~/work/02-linuxbase$ 

```



## 第15章 Makefile与Cmake编程

### 15.1 Makefile

#### 1. 什么是Makefile

   什么是makefile？或许很多Winodws的程序员都不知道这个东西，因为那些Windows的IDE都为你做了这个工作，但我觉得要作一个好的和professional的程序员，makefile还是要懂。特别在linux下的软件编译，你就不能不自己写makefile了，**会不会写makefile，从一个侧面说明了一个人是否具备完成大型工程的能力**。因为，makefile关系到了整个工程的编译规则。一个工程中的源文件不计数，其按**类型、功能、模块**分别放在若干个目录中，makefile定义了一系列的规则来指定，哪些文件需要先编译，哪些文件需要后编译，哪些文件需要重新编译，甚至于进行更复杂的功能操作，因为makefile就像一个Shell脚本一样，其中也可以执行操作系统的命令。

​	makefile带来的好处就是——“自动化编译”，一旦写好，只需要一个make命令，整个工程完全自动编译，极大的提高了软件开发的效率。

**关于程序的编译和链接**

  在此，我想多说关于程序编译的一些规范和方法，一般来说，无论是C、C++，首先要把源文件编译成***中间代码文件**，在Windows下也就是 .obj 文件，linux下是 .o 文件，即 Object File，这个动作叫做**编译（compile）**。然后再把大量的Object File合成执行文件，这个动作叫作链接（link）。  

​	**编译时**，编译器需要的是语法的正确，函数与变量的声明的正确。对于后者，通常是你需要告诉编译器头文件的所在位置（头文件中应该只是声明，而定义应该放在C/C++文件中），只要所有的语法正确，编译器就可以编译出中间目标文件。一般来说，每个源文件都应该对应于一个中间目标文件（O文件或是OBJ文件）。 
​    **链接时**，主要是链接函数和全局变量，所以，我们可以使用这些中间目标文件（O文件或是OBJ文件）来链接我们的应用程序。链接器并不管函数所在的源文件，只管函数的中间目标文件（Object File），在大多数时候，由于源文件太多，编译生成的中间目标文件太多，而在链接时需要明显地指出中间目标文件名，这对于编译很不方便，所以，我们要给中间目标文件打个包，在Windows下这种包叫“**库文件”（Library File)**，也就是dll或者 .lib 文件，在linux下，是Archive File，也就是 .a 文件。

#### 2.  Makefile 介绍

   make命令执行时，需要一个 Makefile 文件，以告诉make命令需要怎么样的去编译和链接程序。

   首先，我们用一个示例来说明Makefile的书写规则。以便给大家一个感兴认识。这个示例来源于GNU的make使用手册，在这个示例中，我们的工程有5个C文件，和4个头文件，我们要写一个Makefile来告诉make命令如何编译和链接这几个文件。我们的规则是：

​      1.如果这个工程没有编译过，那么我们的所有C文件都要编译并被链接。

​      2.如果这个工程的某几个C文件被修改，那么我们只编译被修改的C文件，并链接目标程序。

​      3.如果这个工程的头文件被改变了，那么我们需要编译引用了这几个头文件的C文件，并链接目标程序。

   只要我们的Makefile写得够好，所有的这一切，我们只用一个make命令就可以完成，make命令会自动智能地根据当前的文件修改的情况来确定哪些文件需要重编译，从而自己编译所需要的文件和链接目标程序。

#### 3. Makefile的规则

  在讲述这个Makefile之前，还是让我们先来粗略地看一看Makefile的规则。

```makefile
target : prerequisites
	command1
	...
	...
```

​    **target**也就是一个目标文件，可以是**Object File**，也可以是执行文件。还可以是一个标签（Label）。

​    **prerequisites**就是，要生成那个target所需要的文件或是目标。

​    **command**也就是make需要执行的命令。（任意的Shell命令）

​    这是一个文件的依赖关系，也就是说，target这一个或多个的目标文件依赖于prerequisites中的文件，其生成规则定义在command中。说白一点就是说，prerequisites中如果有一个以上的文件比target文件要新的话，command所定义的命令就会被执行。这就是Makefile的规则。也就是Makefile中最核心的内容。

#### 4.  一个示例

正如前面所说的，如果一个工程有4个头文件，和5个C文件，我们为了完成前面所述的那三个规则，我们的Makefile应该是下面的这个样子的。

实例38 

- 源文件 

```shell
command.c  command.h  display.c  display.h  insert.c  insert.h  kbd.c  kbd.h  main.c  Makefile
```

- 源代码

main.c

```shell
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat main.c 
#include <stdio.h> 
#include "display.h"
#include "command.h"
#include "insert.h"
#include "kbd.h"

int main(int argc, const char *argv[])
{
    
    display(); 
    display_kbd(); 
    display_command();
    display_insert();
    printf("hello world!!\n");
    return 0;
}

```

display.h 与 display.c 

```shell
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat display.h


#ifndef  __DISPLAY__
#define  __DISPLAY__


int display(void);

#endif 
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat display.c 
#include <stdio.h> 

int display()
{
    printf("display\n");
    return 0; 
}
```

kbd.c 与 kbd.h 

```shell
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat kbd.c 
#include <stdio.h> 


int display_kbd()
{
    printf("display_kdb\n");
    return 0; 
}
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat kbd.h


#ifndef  __KDB__
#define  __KDB__


int display_kbd(void);

#endif 
```

command.h 与 command.c

```shell
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat command.h


#ifndef  __COMMAND__
#define  __COMMAND__


int display_command(void);

#endif 
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat command.c 
#include <stdio.h> 


int display_command()
{
    printf("display_command\n");
    return 0; 
}

```

insert.c 与 insert.h 

```shell
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat insert.c 
#include <stdio.h> 


int display_insert()
{
    printf("display_insert\n");
    return 0; 
}
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ cat insert.h 


#ifndef  __KDB__
#define  __KDB__


int display_insert(void);

#endif
```

makefile

```makefile
main : main.o kbd.o command.o display.o insert.o
	cc -o main main.o kbd.o command.o display.o insert.o 

main.o : main.c 
	cc -c main.c   # cc -c main.c -o main.o

kbd.o : kbd.c  kbd.h
	cc -c kbd.c

command.o : command.c command.h
	cc -c command.c

display.o : display.c display.h
	cc -c display.c

insert.o : insert.c insert.h
	cc -c insert.c

clean :
	rm main main.o kbd.o command.o display.o insert.o
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ ls
command.c  command.h  display.c  display.h  insert.c  insert.h  kbd.c  kbd.h  main.c  Makefile
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ make 

Command 'make' not found, but can be installed with:

sudo apt install make      
sudo apt install make-guile

linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ sudo apt install make
[sudo] linux 的密码： 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
建议安装：
  make-doc
下列【新】软件包将被安装：
  make
升级了 0 个软件包，新安装了 1 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
需要下载 154 kB 的归档。
解压缩后会消耗 381 kB 的额外空间。
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 make amd64 4.1-9.1ubuntu1 [154 kB]
已下载 154 kB，耗时 0秒 (715 kB/s)
正在选中未选择的软件包 make。
(正在读取数据库 ... 系统当前共安装有 165601 个文件和目录。)
正准备解包 .../make_4.1-9.1ubuntu1_amd64.deb  ...
正在解包 make (4.1-9.1ubuntu1) ...
正在设置 make (4.1-9.1ubuntu1) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ sudo apt install make-guile
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
将会同时安装下列软件：
  guile-2.0-libs libgc1c2
建议安装：
  make-doc
下列软件包将被【卸载】：
  make
下列【新】软件包将被安装：
  guile-2.0-libs libgc1c2 make-guile
升级了 0 个软件包，新安装了 3 个软件包，要卸载 1 个软件包，有 0 个软件包未被升级。
需要下载 2,455 kB 的归档。
解压缩后会消耗 12.1 MB 的额外空间。
您希望继续执行吗？ [Y/n] y
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 libgc1c2 amd64 1:7.4.2-8ubuntu1 [81.8 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 guile-2.0-libs amd64 2.0.13+1-5ubuntu0.1 [2,218 kB]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 make-guile amd64 4.1-9.1ubuntu1 [155 kB]
已下载 2,455 kB，耗时 1秒 (4,838 kB/s)
(正在读取数据库 ... 系统当前共安装有 165616 个文件和目录。)
正在卸载 make (4.1-9.1ubuntu1) ...
正在选中未选择的软件包 libgc1c2:amd64。
(正在读取数据库 ... 系统当前共安装有 165601 个文件和目录。)
正准备解包 .../libgc1c2_1%3a7.4.2-8ubuntu1_amd64.deb  ...
正在解包 libgc1c2:amd64 (1:7.4.2-8ubuntu1) ...
正在选中未选择的软件包 guile-2.0-libs:amd64。
正准备解包 .../guile-2.0-libs_2.0.13+1-5ubuntu0.1_amd64.deb  ...
正在解包 guile-2.0-libs:amd64 (2.0.13+1-5ubuntu0.1) ...
正在选中未选择的软件包 make-guile。
正准备解包 .../make-guile_4.1-9.1ubuntu1_amd64.deb  ...
正在解包 make-guile (4.1-9.1ubuntu1) ...
正在设置 libgc1c2:amd64 (1:7.4.2-8ubuntu1) ...
正在设置 guile-2.0-libs:amd64 (2.0.13+1-5ubuntu0.1) ...
正在设置 make-guile (4.1-9.1ubuntu1) ...
正在处理用于 libc-bin (2.27-3ubuntu1.6) 的触发器 ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ make 
cc -c main.c   # cc -c main.c -o main.o
cc -c kbd.c
cc -c command.c
cc -c display.c
cc -c insert.c
cc -o main main.o kbd.o command.o display.o insert.o 
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ 
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ ls
command.c  command.h  command.o  display.c  display.h  display.o  insert.c  insert.h  insert.o  kbd.c  kbd.h  kbd.o  main  main.c  main.o  Makefile
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ ./main 
display
display_kdb
display_command
display_insert
hello world!!
linux@ubuntu:~/work/02-linuxbase/02-makefile/01-exam$ 

```

​    **反斜杠（\）**是换行符的意思。这样比较便于Makefile的易读。我们可以把这个内容保存在文件为“Makefile”或“makefile”的文件中，然后在该目录下直接输入命令“make”就可以生成执行文件main。如果要删除执行文件和所有的中间目标文件，那么，只要简单地执行一下“make clean”就可以了。

​    在这个makefile中，目标文件（target）包含：执行文件main和中间目标文件（*.o），依赖文件（prerequisites）就是冒号后面的那些 .c 文件和 .h文件。每一个 .o 文件都有一组依赖文件，而这些 .o 文件又是执行文件 main 的依赖文件。依赖关系的实质上就是说明了目标文件是由哪些文件生成的，换言之，目标文件是哪些文件更新的。

​    在定义好依赖关系后，后续的那一行定义了如何生成目标文件的**操作系统命令**，一定要以一个**Tab**键作为开头。记住，make并不管命令是怎么工作的，他只管执行所定义的命令。make会比较targets文件和prerequisites文件的修改日期，如果prerequisites文件的日期要比targets文件的日期要新，或者target不存在的话，那么，make就会执行后续定义的命令。

​    这里要说明一点的是，clean不是一个文件，它只不过是一个动作名字，有点像C语言中的lable一样，其冒号后什么也没有，那么，make就不会自动去找文件的依赖性，也就不会自动执行其后所定义的命令。要执行其后的命令，就要在make命令后明显得指出这个lable的名字。这样的方法非常有用，我们可以在一个makefile中定义不用的编译或是和编译无关的命令，比如程序的打包，程序的备份，等等。



#### 5.  make是如何工作的

在默认的方式下，也就是我们只输入make命令。那么，

1.  make会在当前目录下找名字叫“Makefile”或“makefile”的文件。
2.  如果找到，它会找文件中的第一个目标文件（target），在上面的例子中，他会找到“main”这个文件，并把这个文件作为最终的目标文件。
3.  如果main文件不存在，或是main所依赖的后面的 .o 文件的文件修改时间要比main这个文件新，那么，他就会执行后面所定义的命令来生成main这个文件。
4.  如果main所依赖的.o文件也存在，那么make会在当前文件中找目标为.o文件的依赖性，如果找到则再根据那一个规则生成.o文件。（这有点像一个堆栈的过程）
5.  当然，你的C文件和H文件是存在的啦，于是make会生成 .o 文件，然后再用 .o 文件声明make的终极任务，也就是执行文件main了。

 	这就是整个make的依赖性，make会一层又一层地去找文件的依赖关系，直到最终编译出第一个目标文件。在找寻的过程中，如果出现错误，比如最后被依赖的文件找不到，那么make就会直接退出，并报错，而对于所定义的命令的错误，或是编译不成功，make根本不理。make只管文件的依赖性，即如果在我找了依赖关系之后，冒号后面的文件还是不在，那么对不起，我就不工作啦。

​    通过上述分析，我们知道，像clean这种，没有被第一个目标文件直接或间接关联，那么它后面所定义的命令将不会被自动执行，不过，我们可以显示要make执行。即命令——**make clean，以此来清除所有的目标文件，以便重编译**。

   于是在我们编程中，如果这个工程已被编译过了，当我们修改了其中一个源文件，比如display.c，那么根据我们的依赖性，我们的目标display.o会被重编译（也就是在这个依性关系后面所定义的命令），于是display.o的文件也是最新的啦，于是display.o的文件修改时间要比main要新，所以main也会被重新链接了(重新生成)。

#### 6. makefile中使用变量

在上面的例子中，先让我们看看main的规则：

```makefile
main: main.o kbd.o command.o display.o insert.o
	cc -o main main.o kbd.o command.o display.o  insert.o 
```

  我们可以看到[.o]文件的字符串被重复了两次，如果我们的工程需要加入一个新的[.o]文件，那么我们需要在两个地方加（应该是三个地方，还有一个地方在clean中）。当然，我们的makefile并不复杂，所以在两个地方加也不累，但如果makefile变得复杂，那么我们就有可能会忘掉一个需要加入的地方，而导致编译失败。所以，为了makefile的易维护，在makefile中我们可以使用变量。makefile的变量也就是一个字符串，理解成C语言中的宏可能会更好。

比如，我们声明一个变量，叫objects, OBJECTS, objs, OBJS, obj, 或是 OBJ，反正不管什么啦，只要能够表示obj文件就行了。我们在makefile一开始就这样定义：

```makefile
objects = main.o kbd.o command.o display.o insert.o 
```

于是，我们就可以很方便地在我们的makefile中以**$(objects)**的方式来使用这个变量了，于是我们的改良版makefile就变成下面这个样子：

```makefile
objects=main.o kbd.o command.o insert.o display.o

main: $(objects) 
	cc -o main  $(objects)

main.o : main.c 
	cc -c main.c 

kbd.o : kbd.c kbd.h 
	cc -c kbd.c

command.o : command.c command.h
	cc -c command.c

insert.o : insert.c insert.h
	cc -c insert.c 

display.o : display.c display.h
	cc -c display.c

clean:
	rm -rf main $(objects)
```

于是如果有新的 .o 文件加入，我们只需简单地修改一下 objects 变量就可以了。

#### 7. 让make自动推导

​	GNU的make很强大，它可以自动推导文件以及文件依赖关系后面的命令，于是我们就没必要去在每一个[.o]文件后都写上类似的命令，因为，我们的make会自动识别，并自己推导命令。

只要make看到一个[.o]文件，它就会自动的把[.c]文件加在依赖关系中，如果make找到一个whatever.o，那么whatever.c，就会是whatever.o的依赖文件。并且 cc -c whatever.c 也会被推导出来，于是，我们的makefile再也不用写得这么复杂。我们的是新的makefile又出炉了。

```makefile
objects=main.o kbd.o command.o insert.o display.o

main: $(objects) 
 
kbd.o : kbd.h 

command.o : command.h

insert.o : insert.h

display.o : display.h

.PHONY : clean 
clean:
	rm -rf main $(objects)
```

运行结果

```shell
linux@ubuntu:~/work/02-linuxbase/02-makefile/03-exam$ make 
cc    -c -o main.o main.c
cc    -c -o kbd.o kbd.c
cc    -c -o command.o command.c
cc    -c -o insert.o insert.c
cc    -c -o display.o display.c
cc   main.o kbd.o command.o insert.o display.o   -o main
linux@ubuntu:~/work/02-linuxbase/02-makefile/03-exam$ 
```

这种方法，也就是make的“隐晦规则”。上面文件内容中，“.PHONY”表示，clean是个伪目标文件。

#### 8. 另类风格的makefile

  即然我们的make可以自动推导命令，那么我看到那堆[.o]和[.h]的依赖就有点不爽，那么多的重复的[.h]，能不能把其收拢起来，好吧，没有问题，这个对于make来说很容易，谁叫它提供了自动推导命令和文件的功能呢？来看看最新风格的makefile吧。

```makefile
objects=main.o kbd.o command.o insert.o display.o

main: $(objects) 
 
.PHONY : clean 
clean:
	rm -rf main $(objects)
```

```shell
linux@ubuntu:~/work/02-linuxbase/02-makefile/04-exam$ make 
cc    -c -o main.o main.c
cc    -c -o kbd.o kbd.c
cc    -c -o command.o command.c
cc    -c -o insert.o insert.c
cc    -c -o display.o display.c
cc   main.o kbd.o command.o insert.o display.o   -o main
linux@ubuntu:~/work/02-linuxbase/02-makefile/04-exam$ 
```

这种风格，让我们的makefile变得很简单，但我们的文件依赖关系就显得有点凌乱了。鱼和熊掌不可兼得。还看你的喜好了。我是不喜欢这种风格的，一是文件的依赖关系看不清楚，二是如果文件一多，要加入几个新的.o文件，那就理不清楚了。

#### 9. 清空目标文件的规则

   每个Makefile中都应该写一个清空目标文件（.o和执行文件）的规则，这不仅便于重编译，也很利于保持文件的清洁。这是一个“修养”。一般的风格都是：

```makefile
clean:
	rm -rf main $(objects)
```

更为稳健的做法是：

```makefile
.PHONY : clean 
clean:
	rm -rf main $(objects)
```

前面说过，.PHONY意思表示clean是一个“伪目标”，。而在rm -rf 命令，也许某些文件出现问题，但不要管，继续做后面的事。当然，clean的规则不要放在文件的开头，不然，这就会变成make的默认目标，相信谁也不愿意这样。不成文的规矩是——“clean从来都是放在文件的最后”。

#### 10. 要深入的了解makefile工作原理

请参考一下一篇blog :[Makefile教程（绝对经典，所有问题看这一篇足够了](https://blog.csdn.net/weixin_38391755/article/details/80380786?ops_request_misc=%7B%22request%5Fid%22%3A%22166199686616781683943598%22%2C%22scm%22%3A%2220140713.130102334..%22%7D&request_id=166199686616781683943598&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-80380786-null-null.142^v44^pc_rank_34_default_2&utm_term=makefile&spm=1018.2226.3001.4187)



### 15.2 CMake

#### 1. CMake的介绍

![查看源图像](https://gitee.com/embmaker/cloudimage/raw/master/img/cmake100.png)

CMake是开源、跨平台的构建工具，可以让我们通过编写简单的配置文件去生成本地的Makefile，这个配置文件是独立于运行平台和编译器的，这样就不用亲自去编写Makefile了，而且配置文件可以直接拿到其它平台上使用，无需修改，非常方便。

本文主要讲述在Linux下如何使用CMake来编译我们的程序。

#### 2. 安装CMake

本文使用ubuntu18.04，安装cmake使用如下命令，

```shell
sudo apt install cmake
```


安装完成后，在终端下输入cmake -version查看cmake版本，这样cmake就安装好了。

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ sudo apt install cmake  -y    # -y 是默认选择y 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
将会同时安装下列软件：
  cmake-data libcurl4 libjsoncpp1 librhash0 libuv1
建议安装：
  cmake-doc ninja-build
下列【新】软件包将被安装：
  cmake cmake-data libcurl4 libjsoncpp1 librhash0 libuv1
升级了 0 个软件包，新安装了 6 个软件包，要卸载 0 个软件包，有 15 个软件包未被升级。
需要下载 4,919 kB 的归档。
解压缩后会消耗 25.5 MB 的额外空间。
获取:1 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 cmake-data all 3.10.2-1ubuntu2.18.04.2 [1,332 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libcurl4 amd64 7.58.0-2ubuntu3.19 [220 kB]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 libjsoncpp1 amd64 1.7.4-3 [73.6 kB]
获取:4 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 librhash0 amd64 1.3.6-2 [78.1 kB]
获取:5 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 libuv1 amd64 1.18.0-3 [64.4 kB]
获取:6 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 cmake amd64 3.10.2-1ubuntu2.18.04.2 [3,152 kB]
已下载 4,919 kB，耗时 1秒 (4,236 kB/s)
正在选中未选择的软件包 cmake-data。
(正在读取数据库 ... 系统当前共安装有 166232 个文件和目录。)
正准备解包 .../0-cmake-data_3.10.2-1ubuntu2.18.04.2_all.deb  ...
正在解包 cmake-data (3.10.2-1ubuntu2.18.04.2) ...
正在选中未选择的软件包 libcurl4:amd64。
正准备解包 .../1-libcurl4_7.58.0-2ubuntu3.19_amd64.deb  ...
正在解包 libcurl4:amd64 (7.58.0-2ubuntu3.19) ...
正在选中未选择的软件包 libjsoncpp1:amd64。
正准备解包 .../2-libjsoncpp1_1.7.4-3_amd64.deb  ...
正在解包 libjsoncpp1:amd64 (1.7.4-3) ...
正在选中未选择的软件包 librhash0:amd64。
正准备解包 .../3-librhash0_1.3.6-2_amd64.deb  ...
正在解包 librhash0:amd64 (1.3.6-2) ...
正在选中未选择的软件包 libuv1:amd64。
正准备解包 .../4-libuv1_1.18.0-3_amd64.deb  ...
正在解包 libuv1:amd64 (1.18.0-3) ...
正在选中未选择的软件包 cmake。
正准备解包 .../5-cmake_3.10.2-1ubuntu2.18.04.2_amd64.deb  ...
正在解包 cmake (3.10.2-1ubuntu2.18.04.2) ...
正在设置 libuv1:amd64 (1.18.0-3) ...
正在设置 libcurl4:amd64 (7.58.0-2ubuntu3.19) ...
正在设置 cmake-data (3.10.2-1ubuntu2.18.04.2) ...
正在设置 librhash0:amd64 (1.3.6-2) ...
正在设置 libjsoncpp1:amd64 (1.7.4-3) ...
正在设置 cmake (3.10.2-1ubuntu2.18.04.2) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在处理用于 libc-bin (2.27-3ubuntu1.6) 的触发器 ...
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ cmake -version
cmake version 3.10.2

CMake suite maintained and supported by Kitware (kitware.com/cmake).
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ 
```

#### 3. 简单样例

首先让我们从最简单的代码入手，先来体验下cmake是如何操作的。使用Makefile中的例子，在main.c相同目录下编写CMakeLists.txt，内容如下:

main.c 

```c
#include <stdio.h>

int main(int argc, const char *argv[])
{
    printf("hello world!!\n");
    return 0;
}
```

CMakeLists.txt

```cmake
cmake_minimum_required (VERSION 2.8)      

project (demo)

add_executable(main main.c)

```

第一行意思是表示cmake的最低版本要求是2.8，我们安装的是3.10.2；第二行是表示本工程信息，也就是工程名叫demo；第三行比较关键，表示最终要生成的elf文件的名字叫main，使用的源文件是main.c
在终端下切到main.c所在的目录下，然后输入以下命令运行cmake，

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ ls
CMakeList.txt  main.c
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ vi CMakeList.txt 
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ cmake .   # 文件名错误, CMakeLists.txt不能是CMakeList.txt
CMake Error: The source directory "/home/linux/work/02-linuxbase/03-cmake/01-exam" does not appear to contain CMakeLists.txt.
Specify --help for usage, or press the help button on the CMake GUI.
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ ls
CMakeList.txt  main.c
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ mv CMakeList.txt CMakeLists.txt 
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ cmake .
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is unknown
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
CMake Error at CMakeLists.txt:3 (project):    # 错误信息 , 文件的第三行 
  No CMAKE_CXX_COMPILER could be found.       # 没有c++ 的编译器, 需要安装

  Tell CMake where to find the compiler by setting either the environment
  variable "CXX" or the CMake cache entry CMAKE_CXX_COMPILER to the full path
  to the compiler, or to the compiler name if it is in the PATH.


-- Configuring incomplete, errors occurred!
See also "/home/linux/work/02-linuxbase/03-cmake/01-exam/CMakeFiles/CMakeOutput.log".
See also "/home/linux/work/02-linuxbase/03-cmake/01-exam/CMakeFiles/CMakeError.log".
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ sudo apt install g++  # 安装c++ 编译器
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
将会同时安装下列软件：
  g++-7 libstdc++-7-dev
建议安装：
  g++-multilib g++-7-multilib gcc-7-doc libstdc++6-7-dbg libstdc++-7-doc
下列【新】软件包将被安装：
  g++ g++-7 libstdc++-7-dev
升级了 0 个软件包，新安装了 3 个软件包，要卸载 0 个软件包，有 15 个软件包未被升级。
需要下载 11.2 MB 的归档。
解压缩后会消耗 42.4 MB 的额外空间。
您希望继续执行吗？ [Y/n] y
获取:1 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 libstdc++-7-dev amd64 7.5.0-3ubuntu1~18.04 [1,471 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 g++-7 amd64 7.5.0-3ubuntu1~18.04 [9,697 kB]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 g++ amd64 4:7.4.0-1ubuntu2.3 [1,568 B]
已下载 11.2 MB，耗时 2秒 (4,489 kB/s)
正在选中未选择的软件包 libstdc++-7-dev:amd64。
(正在读取数据库 ... 系统当前共安装有 168632 个文件和目录。)
正准备解包 .../libstdc++-7-dev_7.5.0-3ubuntu1~18.04_amd64.deb  ...
正在解包 libstdc++-7-dev:amd64 (7.5.0-3ubuntu1~18.04) ...
正在选中未选择的软件包 g++-7。
正准备解包 .../g++-7_7.5.0-3ubuntu1~18.04_amd64.deb  ...
正在解包 g++-7 (7.5.0-3ubuntu1~18.04) ...
正在选中未选择的软件包 g++。
正准备解包 .../g++_4%3a7.4.0-1ubuntu2.3_amd64.deb  ...
正在解包 g++ (4:7.4.0-1ubuntu2.3) ...
正在设置 libstdc++-7-dev:amd64 (7.5.0-3ubuntu1~18.04) ...
正在设置 g++-7 (7.5.0-3ubuntu1~18.04) ...
正在设置 g++ (4:7.4.0-1ubuntu2.3) ...
update-alternatives: 使用 /usr/bin/g++ 来在自动模式中提供 /usr/bin/c++ (c++)
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ cmake .   # 再次运行程序
-- The CXX compiler identification is GNU 7.5.0
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/01-exam
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  CMakeLists.txt  main.c  Makefile
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ vi Makefile 
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ make 
Scanning dependencies of target main
[ 50%] Building C object CMakeFiles/main.dir/main.c.o
[100%] Linking C executable main
[100%] Built target main
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  CMakeLists.txt  main  main.c  Makefile
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ ./main 
hello world!!
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ make clean
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  CMakeLists.txt  main.c  Makefile
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ make 
[ 50%] Building C object CMakeFiles/main.dir/main.c.o
[100%] Linking C executable main
[100%] Built target main
linux@ubuntu:~/work/02-linuxbase/03-cmake/01-exam$ 
```

再来看看目录下的文件，可以看到成功生成了Makefile，还有一些cmake运行时自动生成的文件。
然后在终端下输入make并回车，可以看到执行cmake生成的Makefile可以显示进度，并带颜色。再看下目录下的文件，

可以看到我们需要的elf文件main也成功生成了，然后运行main，运行成功！

PS: 如果想重新生成main，输入make clean就可以删除main这个elf文件。

#### 4. MakeCache.txt引发的错误

在使用cmake . 的时候, 出现了错误 , 提示如下:

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ cmake .
CMake Error: The current CMakeCache.txt directory /home/linux/work/02-linuxbase/03-cmake/02-exam/CMakeCache.txt is different than the directory /home/linux/work/02-linuxbase/03-cmake/01-exam where CMakeCache.txt was created. This may result in binaries being created in the wrong place. If you are not sure, reedit the CMakeCache.txt
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ ls
```

cmake并没有提供类似于
`cmake clean`
这样的方式来让我们清除产生的缓存，但是它编译的缓存(*.cmake, Makefile,CmakeCache.txt,CMakeFiles目录)会遍布各个目录。

解决方法：在根部目录下建立一个build目录，然后在build目录中编译即可。

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  CMakeLists.txt  display.c  display.h  main.c  Makefile
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ rm -rf CMakeCache.txt CMakeFiles/ cmake_install.cmake Makefile 
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ cmake .
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/02-exam
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ 
```



#### 5. 同一目录下多个源文件

接下来进入稍微复杂的例子：在同一个目录下有多个源文件。
在之前的目录下添加2个文件，display.c和display.h。添加完后整体文件结构如下，

display.c , display.h , main.c  , CMakeLists.txt 

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ cat display.c  
#include <stdio.h>


int display()
{
    printf("display\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ cat display.h 
#ifndef __DISPLAY__
#define __DISPLAY__


int display();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ cat main.c 
#include <stdio.h>
#include "display.h"

int main(int argc, const char *argv[])
{
    display();
    printf("hello world!!\n");
    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ cat CMakeLists.txt 
cmake_minimum_required (VERSION 2.8)      

project (demo)

add_executable(main main.c display.c)

```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  CMakeLists.txt  display.c  display.h  main.c  Makefile
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ rm -rf CMakeCache.txt CMakeFiles/ cmake_install.cmake Makefile 
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ cmake .
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/02-exam
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  CMakeLists.txt  display.c  display.h  main.c  Makefile
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ make 
Scanning dependencies of target main
[ 33%] Building C object CMakeFiles/main.dir/main.c.o
[ 66%] Building C object CMakeFiles/main.dir/display.c.o
[100%] Linking C executable main
[100%] Built target main
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ ./main 
display
hello world!!
linux@ubuntu:~/work/02-linuxbase/03-cmake/02-exam$ 
```

​	修改CMakeLists.txt，在add_executable的参数里把display.c加进来，然后重新执行cmake生成Makefile并运行make，然后运行重新生成的elf文件main，可以类推，如果在同一目录下有多个源文件，那么只要在add_executable里把所有源文件都添加进去就可以了。

​	如果有一百个源文件，再这样做就有点坑了，无法体现cmake的优越性，cmake提供了一个命令可以把指定目录下所有的源文件存储在一个变量中，这个命令就是 aux_source_directory(dir var)。第一个参数dir是指定目录，第二个参数var是用于存放源文件列表的变量。

​	我们在main.c所在目录下再添加4个文件。添加完后整体文件结构如下：

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ ls
CMakeLists.txt  command.c  command.h  display.c  display.h  insert.c  insert.h  main.c
```

使用aux_source_directory把当前目录下的源文件存列表存放到变量SRC_LIST里，然后在add_executable里调用SRC_LIST（注意调用变量时的写法）。再次执行cmake和make，并运行main，可以看到运行成功了。

**实例42**

- 源文件

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ ls
CMakeLists.txt  command.c  command.h  display.c  display.h  insert.c  insert.h  main.c
```



- 源代码

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cat main.c 
#include <stdio.h>
#include "display.h"
#include "command.h"
#include "insert.h"

int main(int argc, const char *argv[])
{
    display();
    display_insert(); 
    display_command();
    printf("hello world!!\n");
    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cat insert.c
#include <stdio.h>


int display_insert()
{
    printf("display_insert\n");

    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cat insert.h
#ifndef __INSERT__
#define __INSERT__


int display_insert();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cat display.c
#include <stdio.h>


int display()
{
    printf("display\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cat display.h
#ifndef __DISPLAY__
#define __DISPLAY__


int display();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cat command.c
#include <stdio.h>


int display_command()
{
    printf("display_command\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cat command.h 
#ifndef __COMMAND__
#define __COMMAND__


int display_command();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cat CMakeLists.txt 
cmake_minimum_required (VERSION 2.8)      

project (demo)

aux_source_directory(. SRC_LIST)

add_executable(main ${SRC_LIST})

linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ 
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ ls
CMakeLists.txt  command.c  command.h  display.c  display.h  insert.c  insert.h  main.c
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ vi CMakeLists.txt 
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ ls
CMakeLists.txt  command.c  command.h  display.c  display.h  insert.c  insert.h  main.c
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ vi CMakeLists.txt 
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cmake .
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
CMake Error at CMakeLists.txt:7 (add_executable):
  Cannot find source file:

    $(SRC_LIST)     # 错误是因为， 们使用了  $(SRC_LIST)， 应该使用 ${SRC_LIST}  

  Tried extensions .c .C .c++ .cc .cpp .cxx .m .M .mm .h .hh .h++ .hm .hpp
  .hxx .in .txx


CMake Error: CMake can not determine linker language for target: main
CMake Error: Cannot determine link language for target "main".
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/03-exam
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ vi CMakeLists.txt 
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ cmake .
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/03-exam
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ make 
Scanning dependencies of target main
[ 20%] Building C object CMakeFiles/main.dir/command.c.o
[ 40%] Building C object CMakeFiles/main.dir/display.c.o
[ 60%] Building C object CMakeFiles/main.dir/insert.c.o
[ 80%] Building C object CMakeFiles/main.dir/main.c.o
[100%] Linking C executable main
[100%] Built target main
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ ./main 
display
display_insert
display_command
hello world!!
linux@ubuntu:~/work/02-linuxbase/03-cmake/03-exam$ 
```

#### 6. 不同目录下多个源文件

一般来说，当程序文件比较多时，我们会进行分类管理，把代码根据功能放在不同的目录下，这样方便查找。那么这种情况下如何编写CMakeLists.txt呢？

**实例43** 

- 源文件

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ tree
.
├── bsp
│   ├── insert.c
│   └── insert.h
├── CMakeLists.txt
├── common
│   ├── command.c
│   ├── command.h
│   ├── display.c
│   └── display.h
└── main.c

2 directories, 8 files
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ 
```

- 源代码

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat CMakeLists.txt 
cmake_minimum_required (VERSION 2.8)      

project (demo)

include_directories(. bsp common) 

aux_source_directory(. SRC_LIST1)
aux_source_directory(bsp SRC_LIST2) 
aux_source_directory(common SRC_LIST3) 

add_executable(main ${SRC_LIST1} ${SRC_LIST2} ${SRC_LIST3})

linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat main.c 
#include <stdio.h>
#include "display.h"
#include "command.h"
#include "insert.h"

int main(int argc, const char *argv[])
{
    display();
    display_insert(); 
    display_command();
    printf("hello world!!\n");
    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat bsp/insert.c 
#include <stdio.h>


int display_insert()
{
    printf("display_insert\n");

    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat bsp/insert.h 
#ifndef __INSERT__
#define __INSERT__


int display_insert();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat common/display.c 
#include <stdio.h>


int display()
{
    printf("display\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat common/display.h
#ifndef __DISPLAY__
#define __DISPLAY__


int display();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat common/
command.c  command.h  display.c  display.h  
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat common/command.c 
#include <stdio.h>


int display_command()
{
    printf("display_command\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cat common/command.h
#ifndef __COMMAND__
#define __COMMAND__


int display_command();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ 
```



- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ ls
app  bsp  CMakeLists.txt  common
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ tree

Command 'tree' not found, but can be installed with:

sudo snap install tree  # version 1.8.0+pkg-3fd6, or
sudo apt  install tree

See 'snap info tree' for additional versions.

linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ sudo apt install tree
[sudo] linux 的密码： 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列【新】软件包将被安装：
  tree
升级了 0 个软件包，新安装了 1 个软件包，要卸载 0 个软件包，有 15 个软件包未被升级。
需要下载 40.7 kB 的归档。
解压缩后会消耗 105 kB 的额外空间。
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 tree amd64 1.7.0-5 [40.7 kB]
已下载 40.7 kB，耗时 0秒 (166 kB/s)
正在选中未选择的软件包 tree。
(正在读取数据库 ... 系统当前共安装有 169446 个文件和目录。)
正准备解包 .../tree_1.7.0-5_amd64.deb  ...
正在解包 tree (1.7.0-5) ...
正在设置 tree (1.7.0-5) ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ tree
.
├── bsp
│   ├── insert.c
│   └── insert.h
├── CMakeLists.txt
├── common
│   ├── command.c
│   ├── command.h
│   ├── display.c
│   └── display.h
└── main.c

2 directories, 8 files

linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cmake .
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/04-exam
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ make 
Scanning dependencies of target main
[ 20%] Building C object CMakeFiles/main.dir/main.c.o
/home/linux/work/02-linuxbase/03-cmake/04-exam/main.c:2:10: fatal error: display.h: 没有那个文件或目录
 #include "display.h"              #  是因为CMakeLists.txt中没有指定头文件的搜索路径 
          ^~~~~~~~~~~
compilation terminated.
CMakeFiles/main.dir/build.make:62: recipe for target 'CMakeFiles/main.dir/main.c.o' failed
make[2]: *** [CMakeFiles/main.dir/main.c.o] Error 1
CMakeFiles/Makefile2:67: recipe for target 'CMakeFiles/main.dir/all' failed
make[1]: *** [CMakeFiles/main.dir/all] Error 2
Makefile:83: recipe for target 'all' failed
make: *** [all] Error 2
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ vi main.c 
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ vi CMake
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ vi CMakeLists.txt 
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ cmake .
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/04-exam
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ make 
Scanning dependencies of target main
[ 20%] Building C object CMakeFiles/main.dir/main.c.o
[ 40%] Building C object CMakeFiles/main.dir/bsp/insert.c.o
[ 60%] Building C object CMakeFiles/main.dir/common/command.c.o
[ 80%] Building C object CMakeFiles/main.dir/common/display.c.o
[100%] Linking C executable main
[100%] Built target main
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ ./main 
display
display_insert
display_command
hello world!!
linux@ubuntu:~/work/02-linuxbase/03-cmake/04-exam$ 
```

CMakeList内容修改成如下所示:

```shell
cmake_minimum_required (VERSION 2.8)      

project (demo)

include_directories(. bsp common) 

aux_source_directory(. SRC_LIST1)
aux_source_directory(bsp SRC_LIST2) 
aux_source_directory(common SRC_LIST3) 

add_executable(main ${SRC_LIST1} ${SRC_LIST2} ${SRC_LIST3})
```

这里出现了一个新的命令：include_directories。该命令是用来向工程添加多个指定头文件的搜索路径，路径之间用空格分隔。
因为main.c里include了display.h和command.h，如果没有这个命令来指定头文件所在位置，就会无法编译。

#### 7. 正规一点的组织结构

​	正规一点来说，一般会把源文件放到src目录下 ，生成的对象文件放入到build目录下，最终输出的elf文件会放到bin目录下，这样整个结构更加清晰。让我们把前面的文件再次重新组织下:

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ tree
.
├── bin
├── build
├── CMakeLists.txt
└── src
    ├── bsp
    │   ├── insert.c
    │   └── insert.h
    ├── common
    │   ├── command.c
    │   ├── command.h
    │   ├── display.c
    │   └── display.h
    └── main.c

5 directories, 8 files
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ 
```

我们在最外层目录下新建一个CMakeLists.txt，内容如下:

```cmake
cmake_minimum_required (VERSION 2.8)

project (demo)

add_subdirectory (src)
```

这里出现一个新的命令add_subdirectory()，这个命令可以向当前工程添加存放源文件的子目录，并可以指定中间二进制和目标二进制的存放位置。这里指定src目录下存放了源文件，当执行cmake时，就会进入src目录下去找src目录下的CMakeLists.txt，所以在src目录下也建立一个CMakeLists.txt，内容如下:

```cmake
aux_source_directory (. SRC_LIST)

include_directories (../include)

add_executable (main ${SRC_LIST})

set (EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
```

这里又出现一个新的命令set，是用于定义变量的，EXECUTABLE_OUT_PATH和PROJECT_SOURCE_DIR是CMake自带的预定义变量，其意义如下，

EXECUTABLE_OUTPUT_PATH ：目标二进制可执行文件的存放位置
PROJECT_SOURCE_DIR：工程的根目录

所以，这里set的意思是把存放elf文件的位置设置为工程根目录下的bin目录。

添加好以上这2个CMakeLists.txt后，整体文件结构如下

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ tree
.
├── bin
├── build
├── CMakeLists.txt
└── src
    ├── bsp
    │   ├── insert.c
    │   └── insert.h
    ├── CMakeLists.txt
    ├── common
    │   ├── command.c
    │   ├── command.h
    │   ├── display.c
    │   └── display.h
    └── main.c

5 directories, 9 files
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ 
```

下面来运行cmake，不过这次先让我们切到build目录下，然后输入以下命令，
`cmake ..`
Makefile会在build目录下生成，然后在build目录下运行make，

运行ok，我们再切到bin目录下，发现main已经生成，并运行测试，

这里解释一下为什么在build目录下运行cmake？从前面几个case中可以看到，如果不这样做，cmake运行时生成的附带文件就会跟源码文件混在一起，这样会对程序的目录结构造成污染，而在build目录下运行cmake，生成的附带文件就只会待在build目录下，如果我们不想要这些文件了就可以直接清空build目录，非常方便。

**实例44**

- 源文件

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ tree
.
├── bin
├── build
├── CMakeLists.txt
└── src
    ├── bsp
    │   ├── insert.c
    │   └── insert.h
    ├── CMakeLists.txt
    ├── common
    │   ├── command.c
    │   ├── command.h
    │   ├── display.c
    │   └── display.h
    └── main.c

5 directories, 9 files
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ 
```

- 源代码

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat CMakeLists.txt 
cmake_minimum_required (VERSION 2.8)

project (demo)

add_subdirectory (src)
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat src/CMakeLists.txt 
include_directories(. bsp common) 

aux_source_directory(. SRC_LIST1)
aux_source_directory(bsp SRC_LIST2) 
aux_source_directory(common SRC_LIST3) 

add_executable(main ${SRC_LIST1} ${SRC_LIST2} ${SRC_LIST3})

set (EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat src/main.c 
#include <stdio.h>
#include "display.h"
#include "command.h"
#include "insert.h"

int main(int argc, const char *argv[])
{
    display();
    display_insert(); 
    display_command();
    printf("hello world!!\n");
    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat src/common/command.c 
#include <stdio.h>


int display_command()
{
    printf("display_command\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat src/common/command.h
#ifndef __COMMAND__
#define __COMMAND__


int display_command();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat src/common/display.c 
#include <stdio.h>


int display()
{
    printf("display\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat src/common/display.h
#ifndef __DISPLAY__
#define __DISPLAY__


int display();
#endif  
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat src/bsp/insert.c
#include <stdio.h>


int display_insert()
{
    printf("display_insert\n");

    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cat src/bsp/insert.h
#ifndef __INSERT__
#define __INSERT__


int display_insert();
#endif 
```

- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ ls
bin  build  CMakeLists.txt  src
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam$ cd build/
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ cmake ..
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/05-exam/build
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  Makefile  src
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ make 
Scanning dependencies of target main
[ 20%] Building C object src/CMakeFiles/main.dir/main.c.o
[ 40%] Building C object src/CMakeFiles/main.dir/bsp/insert.c.o
[ 60%] Building C object src/CMakeFiles/main.dir/common/command.c.o
[ 80%] Building C object src/CMakeFiles/main.dir/common/display.c.o
[100%] Linking C executable ../../bin/main
[100%] Built target main
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ ls
CMakeCache.txt  CMakeFiles  cmake_install.cmake  Makefile  src
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ ../bin/main 
display
display_insert
display_command
hello world!!
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ 
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ rm -rf * 
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ ls
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ cmake ..
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/05-exam/build
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ make 
Scanning dependencies of target main
[ 20%] Building C object src/CMakeFiles/main.dir/main.c.o
[ 40%] Building C object src/CMakeFiles/main.dir/bsp/insert.c.o
[ 60%] Building C object src/CMakeFiles/main.dir/common/command.c.o
[ 80%] Building C object src/CMakeFiles/main.dir/common/display.c.o
[100%] Linking C executable ../../bin/main
[100%] Built target main
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ ../bin/main 
display
display_insert
display_command
hello world!!
linux@ubuntu:~/work/02-linuxbase/03-cmake/05-exam/build$ 
```

#### 8. 另外一种写法

前面的工程使用了2个CMakeLists.txt，最外层的CMakeLists.txt用于掌控全局，使用add_subdirectory来控制其它目录下的CMakeLists.txt的运行。

上面的例子也可以只使用一个CMakeLists.txt，把最外层的CMakeLists.txt内容改成如下，

```cmake
cmake_minimum_required (VERSION 2.8)

project (demo)

set (EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

aux_source_directory (src SRC_LIST)

include_directories (include)

add_executable (main ${SRC_LIST})
```


同时，还要把src目录下的CMakeLists.txt删除。

**实例45**

- 源文件

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ tree
.
├── bin
├── build
├── CMakeLists.txt
└── src
    ├── bsp
    │   ├── insert.c
    │   └── insert.h
    ├── common
    │   ├── command.c
    │   ├── command.h
    │   ├── display.c
    │   └── display.h
    └── main.c

5 directories, 8 files

```

- 源代码

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cat CMakeLists.txt 
cmake_minimum_required (VERSION 2.8)

project (demo)


include_directories(src src/bsp src/common) 

aux_source_directory(src        SRC_LIST1)
aux_source_directory(src/bsp    SRC_LIST2) 
aux_source_directory(src/common SRC_LIST3) 

add_executable(main ${SRC_LIST1} ${SRC_LIST2} ${SRC_LIST3})

set (EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cat src/main.c 
#include <stdio.h>
#include "display.h"
#include "command.h"
#include "insert.h"

int main(int argc, const char *argv[])
{
    display();
    display_insert(); 
    display_command();
    printf("hello world!!\n");
    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cat src/common/command.c
#include <stdio.h>


int display_command()
{
    printf("display_command\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cat src/common/command.h
#ifndef __COMMAND__
#define __COMMAND__


int display_command();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cat src/common/display.c
#include <stdio.h>


int display()
{
    printf("display\n"); 
    return 0;
}

linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cat src/common/display.h
#ifndef __DISPLAY__
#define __DISPLAY__


int display();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cat src/bsp/insert.c 
#include <stdio.h>


int display_insert()
{
    printf("display_insert\n");

    return 0;
}
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cat src/bsp/insert.h
#ifndef __INSERT__
#define __INSERT__


int display_insert();
#endif 
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ 
```



- 运行结果

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ ls
bin  build  CMakeLists.txt  src
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ tree
.
├── bin
├── build
├── CMakeLists.txt
└── src
    ├── bsp
    │   ├── insert.c
    │   └── insert.h
    ├── common
    │   ├── command.c
    │   ├── command.h
    │   ├── display.c
    │   └── display.h
    └── main.c

5 directories, 8 files
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ cd build/
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam/build$ ls
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam/build$ cmake ..
-- The C compiler identification is GNU 7.5.0
-- The CXX compiler identification is GNU 7.5.0
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/linux/work/02-linuxbase/03-cmake/06-exam/build
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam/build$ make 
Scanning dependencies of target main
[ 20%] Building C object CMakeFiles/main.dir/src/main.c.o
[ 40%] Building C object CMakeFiles/main.dir/src/bsp/insert.c.o
[ 60%] Building C object CMakeFiles/main.dir/src/common/command.c.o
[ 80%] Building C object CMakeFiles/main.dir/src/common/display.c.o
[100%] Linking C executable ../bin/main
[100%] Built target main
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam/build$ ../bin/main 
display
display_insert
display_command
hello world!!
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam/build$ make clean
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam/build$ rm -rf * 
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam/build$ tree ..
..
├── bin
├── build
├── CMakeLists.txt
└── src
    ├── bsp
    │   ├── insert.c
    │   └── insert.h
    ├── common
    │   ├── command.c
    │   ├── command.h
    │   ├── display.c
    │   └── display.h
    └── main.c

5 directories, 8 files
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam/build$ 
```



## **第16章 常用服务搭建**

### **16.1 TFTP服务器搭建**

1. TFTP（Trial File Transfer Protocol）是一种网络协议，主要用于文件的传输。在嵌入式交叉开发环境中被广泛使用。
2. 查看服务(软件)是否安装

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo apt policy tftpd xinetd
tftpd:
  已安装：(无)
  候选： 0.17-18ubuntu3
  版本列表：
     0.17-18ubuntu3 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 Packages
xinetd:
  已安装：(无)
  候选： 1:2.3.15.3-1
  版本列表：
     1:2.3.15.3-1 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 Packages
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ 
```

3. 安装tftp 服务器和客户端 

```shell
# tftp :下载文件的客户端 
# tftpd :下载文件的服务器 
# xinetd :系统后台服务 
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo apt install tftpd tftp xinetd
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
下列【新】软件包将被安装：
  tftp tftpd xinetd
升级了 0 个软件包，新安装了 3 个软件包，要卸载 0 个软件包，有 15 个软件包未被升级。
需要下载 138 kB 的归档。
解压缩后会消耗 404 kB 的额外空间。
获取:1 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 tftp amd64 0.17-18ubuntu3 [16.5 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 xinetd amd64 1:2.3.15.3-1 [108 kB]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic/universe amd64 tftpd amd64 0.17-18ubuntu3 [14.3 kB]
已下载 138 kB，耗时 0秒 (487 kB/s)
正在选中未选择的软件包 tftp。
(正在读取数据库 ... 系统当前共安装有 169453 个文件和目录。)
正准备解包 .../tftp_0.17-18ubuntu3_amd64.deb  ...
正在解包 tftp (0.17-18ubuntu3) ...
正在选中未选择的软件包 xinetd。
正准备解包 .../xinetd_1%3a2.3.15.3-1_amd64.deb  ...
正在解包 xinetd (1:2.3.15.3-1) ...
正在选中未选择的软件包 tftpd。
正准备解包 .../tftpd_0.17-18ubuntu3_amd64.deb  ...
正在解包 tftpd (0.17-18ubuntu3) ...
正在设置 xinetd (1:2.3.15.3-1) ...
正在设置 tftpd (0.17-18ubuntu3) ...
Note: xinetd currently is not fully supported by update-inetd.
      Please consult /usr/share/doc/xinetd/README.Debian and itox(8).
正在设置 tftp (0.17-18ubuntu3) ...
正在处理用于 systemd (237-3ubuntu10.53) 的触发器 ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在处理用于 ureadahead (0.100.0-21) 的触发器 ...
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ 
```

4. 启动服务是否启动 

```shell
sudo systemctl status  xinetd #查看后台服务器是否启动
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo systemctl status  tftpd
Unit tftpd.service could not be found.
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo systemctl status  xinetd
● xinetd.service - LSB: Starts or stops the xinetd daemon.
   Loaded: loaded (/etc/init.d/xinetd; generated)
   Active: active (running) since Mon 2022-09-05 08:45:43 CST; 2min 37s ago
     Docs: man:systemd-sysv-generator(8)
    Tasks: 1 (limit: 2283)
   CGroup: /system.slice/xinetd.service
           └─27702 /usr/sbin/xinetd -pidfile /run/xinetd.pid -stayalive -inetd_compat -inetd_ipv6

9月 05 08:45:43 ubuntu xinetd[27702]: Reading included configuration file: /etc/xinetd.d/discard [file=/etc/xinetd.d/discard] [line=14]
9月 05 08:45:43 ubuntu xinetd[27702]: Reading included configuration file: /etc/xinetd.d/discard-udp [file=/etc/xinetd.d/discard-udp] [line=25]
9月 05 08:45:43 ubuntu xinetd[27702]: Reading included configuration file: /etc/xinetd.d/echo [file=/etc/xinetd.d/echo] [line=14]
9月 05 08:45:43 ubuntu xinetd[27702]: Reading included configuration file: /etc/xinetd.d/echo-udp [file=/etc/xinetd.d/echo-udp] [line=26]
9月 05 08:45:43 ubuntu xinetd[27702]: Reading included configuration file: /etc/xinetd.d/servers [file=/etc/xinetd.d/servers] [line=14]
9月 05 08:45:43 ubuntu xinetd[27702]: Reading included configuration file: /etc/xinetd.d/services [file=/etc/xinetd.d/services] [line=13]
9月 05 08:45:43 ubuntu xinetd[27702]: Reading included configuration file: /etc/xinetd.d/time [file=/etc/xinetd.d/time] [line=13]
9月 05 08:45:43 ubuntu xinetd[27702]: Reading included configuration file: /etc/xinetd.d/time-udp [file=/etc/xinetd.d/time-udp] [line=28]
9月 05 08:45:43 ubuntu xinetd[27702]: 2.3.15.3 started with libwrap loadavg labeled-networking options compiled in.
9月 05 08:45:43 ubuntu xinetd[27702]: Started working: 0 available services
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ 

```

5. 设置下载和目录 

```shell
sudo vi /etc/xinetd.d/tftp # 这个文件是, 配置tftp下载的目录信息 

service tftp
{
    socket_type        = dgram 
    protocol           = udp 
    wait               = yes
    user               = linux
    server             = /usr/sbin/in.tftpd  # 要启动的程序
    server_args        = -s /var/tftpboot   # 这个就是服务器的目录 
    disable            = no 
    per_source         = 11
    cps                = 100 2
    flags              = IPv4
}

#server_args设置的/var/tftpboot目录是tftp服务器的目录，TFTP客户端就是从这个目录里获取文件的。
#使用命令"mkdir /var/tftpboot"建立TFTP服务器的目录。然后设置/var/tftpboot的访问权限为775.
sudo mkdir /var/tftpboot
sudo chmod 0777 /var/tftpboot


# 执行过程:
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo vi /etc/xinetd.d/tftp
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ ls /var/
backups  cache  crash  lib  local  lock  log  mail  metrics  opt  run  snap  spool  tmp
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo mkdir /var/tftpboot
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ ls /var/
backups  cache  crash  lib  local  lock  log  mail  metrics  opt  run  snap  spool  tftpboot  tmp
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo chmod 0777 /var/tftpboot
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ ls /var/
backups  cache  crash  lib  local  lock  log  mail  metrics  opt  run  snap  spool  tftpboot  tmp
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ ls -l /var/
总用量 52
drwxrwxrwx  2 root root     4096 9月   5 08:54 tftpboot
drwxrwxrwt  9 root root     4096 9月   5 08:45 tmp
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$
```

6. 重启xinetd服务 

```shell
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo systemctl restart xinetd.service 
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ sudo systemctl status xinetd
● xinetd.service - LSB: Starts or stops the xinetd daemon.
   Loaded: loaded (/etc/init.d/xinetd; generated)
   Active: active (running) since Mon 2022-09-05 08:56:42 CST; 12s ago
     Docs: man:systemd-sysv-generator(8)
  Process: 29097 ExecStop=/etc/init.d/xinetd stop (code=exited, status=0/SUCCESS)
  Process: 29103 ExecStart=/etc/init.d/xinetd start (code=exited, status=0/SUCCESS)
    Tasks: 1 (limit: 2283)
   CGroup: /system.slice/xinetd.service
           └─29113 /usr/sbin/xinetd -pidfile /run/xinetd.pid -stayalive -inetd_compat -inetd_ipv6

9月 05 08:56:42 ubuntu xinetd[29113]: Reading included configuration file: /etc/xinetd.d/discard-udp [file=/etc/xinetd.d/discard-udp] [line=25]
9月 05 08:56:42 ubuntu xinetd[29113]: Reading included configuration file: /etc/xinetd.d/echo [file=/etc/xinetd.d/echo] [line=14]
9月 05 08:56:42 ubuntu xinetd[29113]: Reading included configuration file: /etc/xinetd.d/echo-udp [file=/etc/xinetd.d/echo-udp] [line=26]
9月 05 08:56:42 ubuntu xinetd[29113]: Reading included configuration file: /etc/xinetd.d/servers [file=/etc/xinetd.d/servers] [line=14]
9月 05 08:56:42 ubuntu xinetd[29113]: Reading included configuration file: /etc/xinetd.d/services [file=/etc/xinetd.d/services] [line=13]
9月 05 08:56:42 ubuntu xinetd[29113]: Reading included configuration file: /etc/xinetd.d/tftp [file=/etc/xinetd.d/tftp] [line=13]
9月 05 08:56:42 ubuntu xinetd[29113]: Reading included configuration file: /etc/xinetd.d/time [file=/etc/xinetd.d/time] [line=13]
9月 05 08:56:42 ubuntu xinetd[29113]: Reading included configuration file: /etc/xinetd.d/time-udp [file=/etc/xinetd.d/time-udp] [line=28]
9月 05 08:56:42 ubuntu xinetd[29113]: 2.3.15.3 started with libwrap loadavg labeled-networking options compiled in.
9月 05 08:56:42 ubuntu xinetd[29113]: Started working: 1 available service
linux@ubuntu:~/work/02-linuxbase/03-cmake/06-exam$ 

```

7. 测试文件的下载

```shell
linux@ubuntu:~/work/02-linuxbase/01-shell$ cp 01-hello.sh /var/tftpboot/
cp: 无法创建普通文件'/var/tftpboot/01-hello.sh': 权限不够
linux@ubuntu:~/work/02-linuxbase/01-shell$ sudo chmod 0777 /var/tftpboot/
linux@ubuntu:~/work/02-linuxbase/01-shell$ cp 01-hello.sh /var/tftpboot/
linux@ubuntu:~/work/02-linuxbase/01-shell$ cp 02-weizhi.sh  /var/tftpboot/
linux@ubuntu:~/work/02-linuxbase/01-shell$ cp 03-huanjing.sh /var/tftpboot/
linux@ubuntu:~/work/02-linuxbase/01-shell$ ls /var/tftpboot/
01-hello.sh  02-weizhi.sh  03-huanjing.sh


linux@ubuntu:~/Desktop$ tftp 192.168.1.20   # 自己计算机的ip 
tftp> get 01-hello.sh    # 下载/var/tftpboot目录中的文件 , 把这些文件下载到Destktop目录下 
Received 110 bytes in 0.0 seconds
tftp> get 02-weizhi.sh
Received 416 bytes in 0.0 seconds
tftp> get 03-huanjing.sh
Received 109 bytes in 0.0 seconds
tftp> q    # 退出程序 
linux@ubuntu:~/Desktop$ 
```



### **16.2 Samba服务搭建**

1.  Samba是在Linux和UNIX系统上实现SMB协议的一个免费软件，由服务器及客户端程序构成。SMB（Server Messages Block，信息服务块）是一种在局域网上共享文件和打印机的一种通信协议，它为局域网内的不同计算机之间提供文件及打印机等资源的共享服务。
2. 检查samba服务是否安装

```shell
linux@ubuntu:~$ sudo apt policy samba
[sudo] linux 的密码： 
samba:
  已安装：(无)
  候选： 2:4.7.6+dfsg~ubuntu-0ubuntu2.28
  版本列表：
     2:4.7.6+dfsg~ubuntu-0ubuntu2.28 500
        500 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 Packages
        500 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 Packages
     2:4.7.6+dfsg~ubuntu-0ubuntu2 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 Packages
linux@ubuntu:~$ 
```

3. 安装samba软件

```shell
linux@ubuntu:~$ sudo apt install samba
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
将会同时安装下列软件：
  attr ibverbs-providers libcephfs2 libibverbs1 libnl-route-3-200 libpython-stdlib librados2 python python-crypto python-dnspython python-ldb
  python-minimal python-samba python-tdb python2.7 python2.7-minimal samba-common samba-common-bin samba-dsdb-modules samba-vfs-modules tdb-tools
建议安装：
  python-doc python-tk python-crypto-doc python-gpgme python2.7-doc binfmt-support bind9 bind9utils ctdb ldb-tools ntp | chrony smbldap-tools winbind
  heimdal-clients
下列【新】软件包将被安装：
  attr ibverbs-providers libcephfs2 libibverbs1 libnl-route-3-200 libpython-stdlib librados2 python python-crypto python-dnspython python-ldb
  python-minimal python-samba python-tdb python2.7 python2.7-minimal samba samba-common samba-common-bin samba-dsdb-modules samba-vfs-modules tdb-tools
升级了 0 个软件包，新安装了 22 个软件包，要卸载 0 个软件包，有 15 个软件包未被升级。
需要下载 9,533 kB 的归档。
解压缩后会消耗 52.8 MB 的额外空间。
您希望继续执行吗？ [Y/n] y
获取:1 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 python2.7-minimal amd64 2.7.17-1~18.04ubuntu1.8 [1,289 kB]
获取:2 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 python-minimal amd64 2.7.15~rc1-1 [28.1 kB]
获取:3 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 python2.7 amd64 2.7.17-1~18.04ubuntu1.8 [248 kB]
...
获取:22 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 samba-vfs-modules amd64 2:4.7.6+dfsg~ubuntu-0ubuntu2.28 [298 kB]
已下载 9,533 kB，耗时 1秒 (8,214 kB/s)      
正在预设定软件包 ...
正在选中未选择的软件包 python2.7-minimal。
(正在读取数据库 ... 系统当前共安装有 169505 个文件和目录。)
正准备解包 .../python2.7-minimal_2.7.17-1~18.04ubuntu1.8_amd64.deb  ...
正在解包 python2.7-minimal (2.7.17-1~18.04ubuntu1.8) ...
正在选中未选择的软件包 python-minimal。
正准备解包 .../python-minimal_2.7.15~rc1-1_amd64.deb  ...
...
Samba is not being run as an AD Domain Controller, masking samba-ad-dc.service.
Please ignore the following error about deb-systemd-helper not finding samba-ad-dc.service.
Created symlink /etc/systemd/system/multi-user.target.wants/nmbd.service → /lib/systemd/system/nmbd.service.
Failed to preset unit: Unit file /etc/systemd/system/samba-ad-dc.service is masked.
/usr/bin/deb-systemd-helper: error: systemctl preset failed on samba-ad-dc.service: No such file or directory
Created symlink /etc/systemd/system/multi-user.target.wants/smbd.service → /lib/systemd/system/smbd.service.
正在处理用于 mime-support (3.60ubuntu1) 的触发器 ...
正在处理用于 ureadahead (0.100.0-21) 的触发器 ...
正在处理用于 desktop-file-utils (0.23-1ubuntu3.18.04.2) 的触发器 ...
正在处理用于 libc-bin (2.27-3ubuntu1.6) 的触发器 ...
正在处理用于 systemd (237-3ubuntu10.53) 的触发器 ...
正在处理用于 man-db (2.8.3-2ubuntu0.1) 的触发器 ...
正在处理用于 gnome-menus (3.13.3-11ubuntu1.1) 的触发器 ...
正在处理用于 ufw (0.36-0ubuntu0.18.04.2) 的触发器 ...
linux@ubuntu:~$ 
```

4. 查看samba服务是否启动

```shell
linux@ubuntu:~$ sudo systemctl status smbd.service 
● smbd.service - Samba SMB Daemon
   Loaded: loaded (/lib/systemd/system/smbd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-09-05 10:24:58 CST; 2min 19s ago
     Docs: man:smbd(8)
           man:samba(7)
           man:smb.conf(5)
 Main PID: 31250 (smbd)
   Status: "smbd: ready to serve connections..."
    Tasks: 4 (limit: 2283)
   CGroup: /system.slice/smbd.service
           ├─31250 /usr/sbin/smbd --foreground --no-process-group
           ├─31252 /usr/sbin/smbd --foreground --no-process-group
           ├─31253 /usr/sbin/smbd --foreground --no-process-group
           └─31254 /usr/sbin/smbd --foreground --no-process-group

9月 05 10:24:58 ubuntu systemd[1]: Starting Samba SMB Daemon...
9月 05 10:24:58 ubuntu systemd[1]: Started Samba SMB Daemon.
linux@ubuntu:~$ 
```

5. 配置共享目录  /etc/samba/smb.conf 

```shell
sudo vi /etc/samba/smb.conf 
#在文件末尾追加： 
[work]   # 表示共享目录的名称 , 只是一个标号 
   comment = work folder  # 表述信息 
   browseable = yes    # 可浏览 
   path = /home/linux/work  #共享的路径 
   create mask = 0777  # 可读 , 可写 , 可执行 
   directory mask = 0777  # 不屏蔽权限 
   valid users = linux   #有效的用户 linux 
   force user = linux   # 指定linux
   force group = linux  # 指定linux 
   public = yes          # 不是私有的
   writeable = yes      # 可写 
   available = yes     # 有效 
```

6. 设置访问密码

```shell
sudo smbpasswd -a linux  // 密码 ，设置用户(liunux)的密码 
#根据提示深入用户密码(登陆Samba共享目录的时候需要)。


linux@ubuntu:~$ sudo vi /etc/samba/smb.conf 
linux@ubuntu:~$ sudo smbpasswd -a linux
New SMB password:     # 1 
Retype new SMB password:  # 1 
Added user linux.
linux@ubuntu:~$ 
```

7. 重启samba服务 

```shell
sudo systemctl  restart smbd.service

linux@ubuntu:~$ sudo systemctl restart smbd.service 
linux@ubuntu:~$ sudo systemctl status smbd.service 
● smbd.service - Samba SMB Daemon
   Loaded: loaded (/lib/systemd/system/smbd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-09-05 10:35:40 CST; 8s ago
     Docs: man:smbd(8)
           man:samba(7)
           man:smb.conf(5)
 Main PID: 33097 (smbd)
   Status: "smbd: ready to serve connections..."
    Tasks: 4 (limit: 2283)
   CGroup: /system.slice/smbd.service
           ├─33097 /usr/sbin/smbd --foreground --no-process-group
           ├─33099 /usr/sbin/smbd --foreground --no-process-group
           ├─33100 /usr/sbin/smbd --foreground --no-process-group
           └─33101 /usr/sbin/smbd --foreground --no-process-group

9月 05 10:35:40 ubuntu systemd[1]: Starting Samba SMB Daemon...
9月 05 10:35:40 ubuntu systemd[1]: Started Samba SMB Daemon.
linux@ubuntu:~$ 
```

8. 测试samba服务

```shell
#Ubuntu测试： 
->在Ubuntu中点击文件图标 -> 其他位置 -> 底部的连接到服务器中输入服务器地址:
smb://192.168.1.20/work       # ip 是自己电脑的ip地址 
然后点击右下角的Connect按钮. 
此时会提示输入密码(在第6步中创建)，输入密码后即可进入共享目录。
```

9. window下测试samba服务

```shell
打开window的资源管理器(文件夹) -> 在地址栏输入:  \\192.168.1.20\work 
-> 用户名: linux 
     密码: 1 
```

![image-20220905105237411](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905105237411.png)

![image-20220905105542393](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905105542393.png)

点击此电脑 , 添加一个网络驱动器 

![image-20220905105805411](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905105805411.png)

![image-20220905105854467](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905105854467.png)

![image-20220905140603813](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905140603813.png)

10. 知识点的补充 

    使用以上配置 , 发现不能写文件夹和创建文件, 因为没有设置force user 和   force group, 才会导致权限的问题 , 重新配置如下:

```shell
[work]  
   comment = work folder   
   browseable = yes    
   path = /home/linux/work 
   create mask = 0777  
   directory mask = 0777 
   valid users = linux  
   force user = linux   #  用户
   force group = linux  #  组 都这是都linux 
   public = yes      
   writeable = yes    
   available = yes   
   
  # 重启服务
  linux@ubuntu:~$ sudo systemctl status smbd.service 
● smbd.service - Samba SMB Daemon
   Loaded: loaded (/lib/systemd/system/smbd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-09-05 11:13:41 CST; 6min ago
     Docs: man:smbd(8)
           man:samba(7)
           man:smb.conf(5)
 Main PID: 33423 (smbd)
   Status: "smbd: ready to serve connections..."
    Tasks: 5 (limit: 2283)
   CGroup: /system.slice/smbd.service
           ├─33423 /usr/sbin/smbd --foreground --no-process-group
           ├─33425 /usr/sbin/smbd --foreground --no-process-group
           ├─33426 /usr/sbin/smbd --foreground --no-process-group
           ├─33427 /usr/sbin/smbd --foreground --no-process-group
           └─33434 /usr/sbin/smbd --foreground --no-process-group

9月 05 11:13:41 ubuntu systemd[1]: Starting Samba SMB Daemon...
9月 05 11:13:41 ubuntu systemd[1]: Started Samba SMB Daemon.
9月 05 11:14:47 ubuntu smbd[33434]: pam_unix(samba:session): session opened for user linux by (uid=0)
linux@ubuntu:~$ 
```



### **16.3 SSH服务搭建**

1. 什么是SSH

​	SSH 为 Secure Shell 的缩写，由 IETF 的网络小组（Network Working Group）所制定；SSH 为建立在应用层基础上的安全协议。SSH 是较可靠，专为远程登录会话和其他网络服务提供安全性的协议。利用 SSH 协议可以有效防止远程管理过程中的信息泄露问题。SSH最初是UNIX系统上的一个程序，后来又迅速扩展到其他操作平台。SSH在正确使用时可弥补网络中的漏洞。SSH客户端适用于多种平台。几乎所有UNIX平台—包括HP-UX、Linux、AIX、Solaris、Digital UNIX、Irix，以及其他平台，都可运行SSH。

![image-20220905142117852](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905142117852.png)

2. 查看系统是否安装SSH服务

```shell
linux@ubuntu:~$ sudo apt policy openssh-client openssh-server 
openssh-client:
  已安装：1:7.6p1-4ubuntu0.7
  候选： 1:7.6p1-4ubuntu0.7
  版本列表：
 *** 1:7.6p1-4ubuntu0.7 500
        500 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 Packages
        100 /var/lib/dpkg/status
     1:7.6p1-4ubuntu0.5 500
        500 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 Packages
     1:7.6p1-4 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 Packages
openssh-server:
  已安装：1:7.6p1-4ubuntu0.7
  候选： 1:7.6p1-4ubuntu0.7
  版本列表：
 *** 1:7.6p1-4ubuntu0.7 500
        500 http://mirrors.yun-idc.com/ubuntu bionic-updates/main amd64 Packages
        100 /var/lib/dpkg/status
     1:7.6p1-4ubuntu0.5 500
        500 http://mirrors.yun-idc.com/ubuntu bionic-security/main amd64 Packages
     1:7.6p1-4 500
        500 http://mirrors.yun-idc.com/ubuntu bionic/main amd64 Packages
linux@ubuntu:~$ 
```

3. 安装ssh服务   

```shell
sudo apt install openssh-client openssh-server 
linux@ubuntu:~$ sudo apt install openssh-client openssh-server 
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
openssh-client 已经是最新版 (1:7.6p1-4ubuntu0.7)。
openssh-server 已经是最新版 (1:7.6p1-4ubuntu0.7)。
升级了 0 个软件包，新安装了 0 个软件包，要卸载 0 个软件包，有 15 个软件包未被升级。
linux@ubuntu:~$ 
```

4. 检查ssh服务是否启动

```shell
linux@ubuntu:~$ sudo systemctl status sshd.service 
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-08-29 09:36:30 CST; 1 weeks 0 days ago
 Main PID: 780 (sshd)
    Tasks: 1 (limit: 2283)
   CGroup: /system.slice/ssh.service
           └─780 /usr/sbin/sshd -D

9月 05 10:09:17 ubuntu systemd[1]: Reloading OpenBSD Secure Shell server.
9月 05 10:09:17 ubuntu systemd[1]: Reloaded OpenBSD Secure Shell server.
9月 05 10:09:17 ubuntu sshd[780]: Received SIGHUP; restarting.
9月 05 10:09:17 ubuntu sshd[780]: Server listening on 0.0.0.0 port 22.
9月 05 10:09:17 ubuntu sshd[780]: Server listening on :: port 22.
9月 05 10:09:18 ubuntu systemd[1]: Reloading OpenBSD Secure Shell server.
9月 05 10:09:18 ubuntu sshd[780]: Received SIGHUP; restarting.
9月 05 10:09:18 ubuntu systemd[1]: Reloaded OpenBSD Secure Shell server.
9月 05 10:09:18 ubuntu sshd[780]: Server listening on 0.0.0.0 port 22.
9月 05 10:09:18 ubuntu sshd[780]: Server listening on :: port 22.
linux@ubuntu:~$ sudo systemctl status ssh
sshd.service  ssh.service   ssh.socket    
linux@ubuntu:~$ sudo systemctl status ssh.service   # sshd.server 和 ssh.server 是一样的服务
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-08-29 09:36:30 CST; 1 weeks 0 days ago
 Main PID: 780 (sshd)
    Tasks: 1 (limit: 2283)
   CGroup: /system.slice/ssh.service
           └─780 /usr/sbin/sshd -D

9月 05 10:09:17 ubuntu systemd[1]: Reloading OpenBSD Secure Shell server.
9月 05 10:09:17 ubuntu systemd[1]: Reloaded OpenBSD Secure Shell server.
9月 05 10:09:17 ubuntu sshd[780]: Received SIGHUP; restarting.
9月 05 10:09:17 ubuntu sshd[780]: Server listening on 0.0.0.0 port 22.
9月 05 10:09:17 ubuntu sshd[780]: Server listening on :: port 22.
9月 05 10:09:18 ubuntu systemd[1]: Reloading OpenBSD Secure Shell server.
9月 05 10:09:18 ubuntu sshd[780]: Received SIGHUP; restarting.
9月 05 10:09:18 ubuntu systemd[1]: Reloaded OpenBSD Secure Shell server.
9月 05 10:09:18 ubuntu sshd[780]: Server listening on 0.0.0.0 port 22.
9月 05 10:09:18 ubuntu sshd[780]: Server listening on :: port 22.
linux@ubuntu:~$ 
```

4. 在Window下进行测试 ，运行 cmd 或者 PowerShell ，输入链接命令：

```shell
ssh linux@192.168.1.20     # ssh   用户名@ip地址    , 是自己ubuntu的ip地址


PS C:\Users\sheng> ssh linux@192.168.1.20  # 在window下登录ubuntu 
The authenticity of host '192.168.1.20 (192.168.1.20)' can't be established.
ECDSA key fingerprint is SHA256:g+rxrkFtBCIwTyzY4xIPvtftDh8f04+ydYM6SFGdmBI.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '192.168.1.20' (ECDSA) to the list of known hosts.
linux@192.168.1.20's password:     # 输入密码 1 
Welcome to Ubuntu 18.04.6 LTS (GNU/Linux 5.4.0-124-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

15 updates can be applied immediately.
14 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2023.

The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
```

### 16.4 xshell登录ubuntu 

安装软件并选择免费为家庭/学校 

![image-20220905150243085](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905150243085.png)

安装完成后, 运行软件是会弹出新建连接对话框  

![image-20220905150444287](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905150444287.png)



```shell
->在弹出的对话框中点击新建
-> 名称: ubuntu18.04
   协议: ssh 
   主机: 192.168.1.20   # ubuntu主机的ip地址 
   说明信息: 用户名:linux 
            密码  :1 
-> 点击确定 
```

![image-20220905151019781](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905151019781.png)

选中ubuntu18.04 之后点击连接

![image-20220905151051629](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905151051629.png)

会提示, 是否保存秘钥 ，可以保存用户名和密码 ， 下一次连接时可以不输入密码， 选择接收并保存

![image-20220905151130745](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905151130745.png)

输入用户名：linux ， 选择记住用户名 ， 点击确定

![image-20220905151336807](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905151336807.png)

正产输入密码后，即可连接， 如果没有连接上 ， 可以点击工具栏中的重新连接

![image-20220905151536743](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905151536743.png)

点击重新连接后， 输入密码， 选择记住密码， 下次可以不在输入密码 ， 之后点击确定。

![image-20220905151559021](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905151559021.png)

连接成功如下： 

![image-20220905151735559](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905151735559.png)



### 16.5  VSCode通过SSH登录ubuntu

1. 安装vscode的ssh插件

打开vscode软件 ， 点击“扩展” ， 在搜索框中输入： ssh , 之后点击安装即可。

![image-20220905155604392](C:/Users/sheng/AppData/Roaming/Typora/typora-user-images/image-20220905155605455.png)

安装成功后 ， 在侧边栏会有一个 远程资源管理器， 单击如下图：

![image-20220905155742710](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905155742710.png)

2. 使用ssh 连接ubuntu18.04 

如下图， 点击 “+” ， 新建一个连接， 在弹出的输入框中输入：

```shell
ssh linux@192.168.1.20 
```



![image-20220905155948736](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905155948736.png)

![image-20220905160135162](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905160135162.png)

![image-20220905160206464](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905160206464.png)

```shell
-> 选择 C:\user\sheng\.ssh\config 文件 
-> 在弹出的对话中选择 , OpenConfig 选项 
-> 添加如下信息:

Host ubuntu18.04-2207
  HostName 192.168.1.20
  User linux
  ForwardAgent yes
 
# 使用ctrl + s 保存文件
```

在远程资源管理窗口中点击 刷新后, 会出现我们配置的连接信息，之后右击“ubuntu18.04-2207”

![image-20220905160635573](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905160635573.png)

![image-20220905160814045](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905160814045.png)

选择 “Connect to Host in Current Window” , 开始连接

平台选择: linux 

密码:  1 

之后按下回车键即可完成连接  ， 出现如下图， 表示成功连接。

![image-20220905161037520](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905161037520.png)

打开“资源管理器” ， 选择打开文件夹 ， 选择 /home/linux/work 目录， 点击确定。

![image-20220905161210802](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905161210802.png)

![image-20220905161245420](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905161245420.png)

之后再次输入密码： 1

![image-20220905161403953](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905161403953.png)

选择 “是， 我信任此作者 信任文件夹并启动所有功能”

![image-20220905161433814](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905161433814.png)

成功打开后， 如下图 

![image-20220905161713612](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905161713612.png)

开启终端： ctrl + ~  开启终端， 再按一次关闭终端 

3. vscode 安装runner code 插件 

单击扩展 , 在搜索框中输入: code , 选择Code Runner  , 打开目录有, 右击代码选择 "Run Code" 即可完成运行代码 

![image-20220905162455999](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220905162455999.png)



### 16.6 "Bad owner or permissions on C:\\Users\\\##/.ssh"

![image-20220906083741025](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220906083741025.png)

1. 第一种可能的解决办法 

```shell
vscode 中 "Bad owner or permissions on C:\\Users\\\##/.ssh" 
解决办法:
-> 文件 
-> 首选项 
-> 设置 
-> 扩展 
-> Remote-SSH 
-> Remote.SSH: Config File
   The absolute file path to a custom SSH config file.
   "C:\Users\YShengli\.ssh\config"
-> 再次连接, 以上问题即可解决 
```

2. 第二种可能的解决办法

在ip地址改变以后, 同样也会发生这样的问题 , 解决办法:  直接删除windonw下的known_hosts

```shell
PS C:\Users\sheng> cd .\.ssh\
PS C:\Users\sheng\.ssh> ls


    目录: C:\Users\sheng\.ssh


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----            22/9/5     16:05            301 config
-a----            22/9/6     08:57           2602 id_rsa
-a----            22/9/6     08:57            569 id_rsa.pub
-a----            22/9/6     09:43            877 known_hosts


PS C:\Users\sheng\.ssh> rm .\known_hosts  # 也可以打开这个文件，删除指定的秘钥  
```

再次连接一次即可。



### 16.7 SSH免密登录 

xshell 可以自动保存秘钥, 因此可以免密登录 

window下的powershell没有免密登录的功能，vscode的ssh插件也没有这个功能， 我们需要手动的生成秘钥， 来进行免密登录

1.  在window 下 进入cmd 或则 powershell

```shell
进入目录：C:\Users\sheng\.ssh> 
输入命令：ssh-keygen -t rsa
目录里面会生成两个文件： id_rsa  id_rsa.pub

# 执行过程： 
PS C:\Users\sheng> cd .\.ssh\
PS C:\Users\sheng\.ssh> ls


    目录: C:\Users\sheng\.ssh


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----            22/9/5     16:05            301 config
-a----            22/9/5     14:17            701 known_hosts


PS C:\Users\sheng\.ssh> pwd

Path
----
C:\Users\sheng\.ssh

PS C:\Users\sheng\.ssh> ssh-keygen -t rsa   # 生成密码文件 
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\sheng/.ssh/id_rsa):    # 回车
Enter passphrase (empty for no passphrase):   # 回车
Enter same passphrase again:  # 回车
Your identification has been saved in C:\Users\sheng/.ssh/id_rsa.
Your public key has been saved in C:\Users\sheng/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:S/fj/sL5AKxi4bvu08kQhDnkrv03UGR+4Pmn3KN51CM sheng@YShengli
The key's randomart image is:
+---[RSA 3072]----+
|   ..o           |
|   .+ . +        |
|    .o = o       |
|   .  . =..      |
|    . .oSo+  .   |
|   o .oo +.oE o  |
|  . . +=oo *+o . |
|     o.o* ooBo   |
|     o*+ .o+o=o  |
+----[SHA256]-----+
PS C:\Users\sheng\.ssh>
PS C:\Users\sheng\.ssh> ls


    目录: C:\Users\sheng\.ssh


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----            22/9/5     16:05            301 config
-a----            22/9/6     08:57           2602 id_rsa
-a----            22/9/6     08:57            569 id_rsa.pub
-a----            22/9/5     14:17            701 known_hosts


PS C:\Users\sheng\.ssh>
```

2. 在ubuntu中也生成一个密钥

```shell
# 执行过程
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ ssh-keygen -t rsa    # 生成秘钥 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/linux/.ssh/id_rsa):   # 回车 
Enter passphrase (empty for no passphrase):   # 回车 
Enter same passphrase again:    # 回车 
Your identification has been saved in /home/linux/.ssh/id_rsa.
Your public key has been saved in /home/linux/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:GDj+F21ZALB1NGuWaAm8+sNXKTfNBN1CWARa0EqOHvM linux@ubuntu
The key's randomart image is:
+---[RSA 2048]----+
|     .o.+=BB+.   |
|     ..+.*+=o .  |
|    o o==.=...   |
|   . .=+o+ o.    |
|    .o.+S +=     |
|    ... Eo= o    |
|     o. .+ .     |
|      +..        |
|       o         |
+----[SHA256]-----+
linux@ubuntu:~$ 

linux@ubuntu:~$ ls .ssh/     # 生成了2个文件   id_rsa  id_rsa.pub
id_rsa  id_rsa.pub
linux@ubuntu:~$ 
```

3.  把window下生成的秘钥放到ubuntu中去， 即可实现免密登录 

把window下的 `C:\Users\YShengli\.ssh>id_rsa.pub`  复制到ubuntu的/home/linux 目录下

```shell
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  id_rsa.pub  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ cat id_rsa.pub >>.ssh/authorized_keys  #把id_rsa.pub 加入到信任密钥列表中去
linux@ubuntu:~$ rm id_rsa.pub   #删除id_rsa.pub
linux@ubuntu:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos  work
linux@ubuntu:~$ 
```

4. powershell测试免密连接

```shell
PS C:\Users\sheng> ssh linux@192.168.1.20  # 测试连接命令 
Welcome to Ubuntu 18.04.6 LTS (GNU/Linux 5.4.0-124-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

15 updates can be applied immediately.
14 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2023.
Last login: Tue Sep  6 08:54:16 2022 from 192.168.1.103
linux@ubuntu:~$
linux@ubuntu:~$ exit  # 断开连接 
注销
Connection to 192.168.1.20 closed.
PS C:\Users\sheng>
```

5. vscode 测试免密连接

直接使用vscode的ssh连接ubuntu即可, 中途不需要输入密码即可ssh连接ubuntu18.04

![image-20220906091102158](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220906091102158.png)

### 16.8 github和gitee的使用 

1. GitHub是一个面向开源及私有软件项目的托管平台，因为只支持Git作为唯一的版本库格式进行托管，故名GitHub。

由于github国内访问速度过慢，使用很不方便， 使用国内的替代网站， 就是gitee 。

2. Gitee是开源中国（OSChina）推出的基于Git的代码托管服务
3. 使用gitee 来创建一个组织和仓库 

```shell
-> 注册账号并登陆账号
-> 登陆成功后,点击右上角的"+" 
-> 创建组织
-> 输入组织的名称 :iotemb
-> 再次点击右上角的"+" 
-> 新建仓库
-> 输入: 创库名称 : emb2207
-> 根据提示 完成仓库的创建 
```

![image-20220920085421202](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220920085421202.png)





### 16.9  https模式下使用vscode+ubuntu克隆仓库并上传代码 

1. **ubuntu18.04 中安装git** 

```shell
linux@ubuntu:~$ sudo apt install git
正在读取软件包列表... 完成
正在分析软件包的依赖关系树       
正在读取状态信息... 完成       
git 已经是最新版 (1:2.17.1-1ubuntu0.12)。
升级了 0 个软件包，新安装了 0 个软件包，要卸载 0 个软件包，有 15 个软件包未被升级。
linux@ubuntu:~$ 
```

2. 使用git克隆代码

```shell
-> gitee中找到自己的创库 
-> 选择 "克隆/下载" 选项
-> 选择 Https 
-> 选择 复制
-> 即可得到仓库的地址  https://gitee.com/iotemb/emb2207.git
-> vscode 使用ssh 登录ubuntu18.04 
-> vscode 中的代码管理器 
-> 克隆仓库 
-> 在弹出的输入框中输入仓库地址: https://gitee.com/iotemb/emb2207.git
-> 选择存放的路径, emb2207.git将来会创建一个目录,这个目录是emb2207 , 这个注意一下 
-> 提示输入账号和密码
	账号: 18612251094
	密码: *********  # 自己账户的密码 
-> 在提示是否打开创库时
-> 选择打开仓库 
-> 把本地的代码复制到 这个目录内 
-> 点击vscode中的"代码管理器"
-> 在消息输入框中输入:一般是说明信息, 哪一个用户
-> 提交
-> 即可完成本地数据上传代码托管平台
```

![image-20220920090841909](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220920090841909.png)

https的模式下每次都需要输入账号和密码 , 这种方式使用起来很麻烦  ， 可以使用ssh的方式来实现免密登录

### 16.10 SSH模式来实现Gitee的密码提交代码

1. 在ubuntu18.04 中生成密码 

   请参考16.7 章节去实现

2. 把密码保存到gitee服务器中 

```shell
-> 登录gitee账号 
-> 设置 
-> 安全设置 
-> SSH 公钥 
-> 使用命令 获取ubuntu18.04中秘钥的内容 
-> 把秘钥中的内容复制到下图的文本框中
-> 确定
-> 输gitee 的密码 
-> 添加秘钥成功
```

```shell
linux@ubuntu:~$ cd .ssh/
linux@ubuntu:~/.ssh$ cat id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDCn8gt9bl9rbxB7BMfTuTrVCnA5LfQLC0O9l5geFooLaPKrU3G0OIIPYy/lhxoPbryONuHlmJZr+Ur901iKLtncdwsWhQt7bhVZJsV//vsSAF4yXIy97r6nuQSie8pGWVNGQqIOs0LvNVMwlF4XBtY/hNHN4mCLf8BCvvVdHbm7fOOAqBwnnwT42lOmWg7N4QvOaflqaoGFE+c1f6K5w78cimW0uTWL9a+DD85cGCwLT+vmQP7qhJjCxyjaNgbaDDkU6/xRDH6YrG6t8Kb95ZmhRRQWZfuHUWwaoY3FMVT7jYQrISqk22J2Ghj3ZoM3yw86TEoySe8l+Sb/1zmXGeZ linux@ubuntu
linux@ubuntu:~/.ssh$ 
```

![image-20220915144735894](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220915144735894.png)



![image-20220915145027915](https://gitee.com/embmaker/cloudimage/raw/master/img/image-20220915145027915.png)



3. 重新获取git的ssh地址 

   需要把emb2207 这个目录改一个名称, 因为会发生重名的问题, 所以需要改名。

```

```



```shell

-> 找到自己的创库 
-> 选择 "克隆/下载" 选项
-> 选择 ssh
-> 选择 复制
-> 即可得到仓库的地址  
git@gitee.com:iotemb/emb2207.git
-> 在家目录下克隆代码 
```

```shell
linux@ubuntu:~$ git clone git@gitee.com:iotemb/emb2207.git
正克隆到 'emb2207'...
The authenticity of host 'gitee.com (212.64.63.190)' can't be established.
ECDSA key fingerprint is SHA256:FQGC9Kn/eye1W8icdBgrQp+KkGYoFgbVr17bmjey0Wc.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitee.com,212.64.63.190' (ECDSA) to the list of known hosts.
remote: Enumerating objects: 184, done.
remote: Counting objects: 100% (184/184), done.
remote: Compressing objects: 100% (169/169), done.
remote: Total 184 (delta 55), reused 113 (delta 14), pack-reused 0
接收对象中: 100% (184/184), 120.76 MiB | 336.00 KiB/s, 完成.
处理 delta 中: 100% (55/55), 完成.
```









