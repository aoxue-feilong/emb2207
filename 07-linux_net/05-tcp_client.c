#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#define N 128

int process_list(struct sockaddr_in *addr, char *buf)
{
    //创建一个socket
    int sockfd, ret;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    ret = connect(sockfd, (struct sockaddr *)addr, sizeof(*addr));
    if (ret < 0)
    {
        perror("connect");
        exit(-1);
    }

    ret = send(sockfd, buf, N, 0); // 发送给服务器
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }
    // recv 函数 , 服务器使用close(newsockfd)关闭socket时, recv函数返回值为0
    // recv 函数发生错误时, 返回值为-1
    // 返回读回来的字节数
    while (recv(sockfd, buf, N, 0) > 0) // 表示数据还没有接收完毕
    {
        printf("%s\n", buf); // 打印接收的内容
    }
    close(sockfd); // 关闭和服务器通信的socket
    return 0;
}
// 从服务器下载文件 , 本地创建文件, 服务器读取文件
int process_get(struct sockaddr_in *addr, char *buf)
{
    //创建一个socket
    int sockfd, ret, fd, nbytes;

    // 本地要创建一个新的文件
    // get filename
    fd = open(buf + 4, O_RDWR | O_CREAT | O_TRUNC, 0664);
    if (fd < 0)
    {
        perror("open");
        exit(-1);
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    ret = connect(sockfd, (struct sockaddr *)addr, sizeof(*addr));
    if (ret < 0)
    {
        perror("connect");
        exit(-1);
    }

    ret = send(sockfd, buf, N, 0); // 发送给服务器
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }

    // recv 函数 , 服务器使用close(newsockfd)关闭socket时, recv函数返回值为0
    // recv 函数发生错误时, 返回值为-1
    // 返回读回来的字节数
    // 读回来多少字节写入到文件内多少字节
    while ((nbytes = recv(sockfd, buf, N, 0)) > 0) // 表示数据还没有接收完毕
    {
        // printf("nbytes=%d\n",nbytes);
        // printf("buf=%s\n",buf);
        ret = write(fd, buf, nbytes); // 把读回来的内容写入到文件内
        if (ret < 0)
        {
            perror("write");
            exit(-1);
        }
    }
    // printf("nbytes = %d\n",nbytes);
    close(fd);
    close(sockfd); // 关闭和服务器通信的socket
    return 0;
}

// 上传文件到服务器 , 本地读文件, 服务器创建文件
int process_put(struct sockaddr_in *addr, char *buf)
{
    //创建一个socket
    int sockfd, ret, fd, nbytes;

    // 本地要创建一个新的文件
    // get filename
    fd = open(buf + 4, O_RDONLY | O_EXCL);
    if (fd < 0)
    {
        perror("open");
        exit(-1);
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    ret = connect(sockfd, (struct sockaddr *)addr, sizeof(*addr));
    if (ret < 0)
    {
        perror("connect");
        exit(-1);
    }

    ret = send(sockfd, buf, N, 0); // 发送给服务器
    if (ret < 0)
    {
        perror("send");
        exit(-1);
    }

    // 读本地的文件 , 把文件的内容读到buf内 , 达到文件末尾返回0
    // read 读错误时, 返回-1
    while ((nbytes = read(fd, buf, N)) > 0) // 表示文件还没有读完
    {
        ret = send(sockfd, buf, nbytes, 0); // 把读回来的数据发送给服务器
        if (ret < 0)
        {
            perror("send");
            exit(-1);
        }
    }

    // printf("nbytes = %d\n",nbytes);
    close(fd);
    close(sockfd); // 关闭和服务器通信的socket
    return 0;
}

// 01-socket 192.168.1.2 8000
// argv[0]   arv[1]      argv[2]
int main(int argc, char const *argv[])
{

    int sockfd, ret;
    struct sockaddr_in server_addr;
    socklen_t addrlen;
    char buf[N] = {0};

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./03-tcp_client ip port)\n");
        exit(-1);
    }
    memset(&server_addr, 0, sizeof(server_addr));     // 清空结构体
    server_addr.sin_family = AF_INET;                 // 什么类型的通信
    server_addr.sin_port = htons(atoi(argv[2]));      // 设置socket 的固定端口
    server_addr.sin_addr.s_addr = inet_addr(argv[1]); // 把字符串ip转换成网络二进制的ip地址

    while (1)
    {
        printf(">:list 查看目录\n");
        printf(">:get filename\n");
        printf(">:put filename\n");
        printf(">:quit \n");
        printf(">:");
        fgets(buf, N, stdin);
        buf[strlen(buf) - 1] = 0; // 清除'\n'
        if (strncmp(buf, "list", 4) == 0)
        {
            process_list(&server_addr, buf);
        }
        else if (strncmp(buf, "get", 3) == 0)
        {
            process_get(&server_addr, buf);
        }
        else if (strncmp(buf, "put", 3) == 0)
        {
            process_put(&server_addr, buf);
        }
        else if (strncmp(buf, "quit", 4) == 0)
        {
            exit(0);
        }
    }
    close(sockfd);

    return 0;
}