#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>


#define   N    128 

// 01-socket 192.168.1.2 8000
// argv[0]   arv[1]      argv[2]
int main(int argc, char const *argv[])
{

    int sockfd, ret;
    struct sockaddr_in server_addr;
    socklen_t addrlen;//客户端地址的大小
    char buf[N]= {0}; 

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./03-tcp_client ip port)\n");
        exit(-1);
    }

    // 创建一个socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&server_addr, 0, sizeof(server_addr));     // 清空结构体
    server_addr.sin_family = AF_INET;                 // 什么类型的通信
    server_addr.sin_port = htons(atoi(argv[2]));      // 设置socket 的固定端口
    server_addr.sin_addr.s_addr = inet_addr(argv[1]); // 把字符串ip转换成网络二进制的ip地址
    ret = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));//connect 发起TCP的连接
    if (ret < 0)
    {
        perror("connect");
        exit(-1);
    }

    while (1)
    {
        printf(">:");
        fgets(buf, N, stdin);//从屏幕读取
        buf[strlen(buf) - 1] = 0;      // 清除'\n'
        ret = send(sockfd, buf, N, 0); // 发送给服务器
        if (ret < 0)
        {
            perror("send");
            exit(-1);
        }
        if (strncmp(buf, "quit", 4) == 0)//判断退出，一般都是客户端退出
            break;
        memset(buf, 0, sizeof(buf));   // buf 内容清空
        ret = recv(sockfd, buf, N, 0); // 接收服务器发来的数据
        if (ret < 0)
        {
            perror("read");
            exit(-1);
        }
        //inet_ntoa ip地址
        //ntohs 端口转换函数
        //buf里面的内容
        printf("%s & %d said :%s\n", inet_ntoa(server_addr.sin_addr),
               ntohs(server_addr.sin_port), buf);
    }
    close(sockfd);

    return 0;
}
