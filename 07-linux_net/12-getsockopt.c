#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

// 01-socket 192.168.100.128 8000
// argv[0]   arv[1]          argv[2]
int main(int argc, char const *argv[])
{

    int sockfd, ret, newsockfd;
    struct sockaddr_in myaddr, client_addr;
    socklen_t addrlen;

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./12-getsockopt ip port)\n");
        exit(-1);
    }

    // 创建一个socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    // 获取socket 接收缓存区的大小
    int recvsize = 0;
    socklen_t len = sizeof(recvsize);
    ret = getsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &recvsize, &len);
    if (ret < 0)
    {
        perror("getsockopt");
        exit(-1);
    }
    printf("1:recvsize=%d/%d KB\n", recvsize, recvsize / 1024);

    recvsize = 75 * 1024;
    // 设置接收缓存器为150K
    ret = setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &recvsize, sizeof(recvsize));
    if (ret < 0)
    {
        perror("getsockopt");
        exit(-1);
    }
    //  再次获取缓存器的值
    ret = getsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &recvsize, &len);
    if (ret < 0)
    {
        perror("getsockopt");
        exit(-1);
    }
    printf("2:recvsize=%d/%d KB\n", recvsize, recvsize / 1024);

    memset(&myaddr, 0, sizeof(myaddr));           // 清空结构体
    memset(&client_addr, 0, sizeof(client_addr)); // 清空结构体
    myaddr.sin_family = AF_INET;                  // 什么类型的通信
    myaddr.sin_port = htons(atoi(argv[2]));       // 设置socket 的固定端口
    myaddr.sin_addr.s_addr = inet_addr(argv[1]);  // 把字符串ip转换成网络二进制的ip地址
    ret = bind(sockfd, (struct sockaddr *)&myaddr, sizeof(myaddr));
    if (ret < 0)
    {
        perror("bind");
        exit(-1);
    }
    ret = listen(sockfd, 10);
    if (ret < 0)
    {
        perror("listen");
        exit(-1);
    }
    // system("netstat -ant");
    addrlen = sizeof(client_addr); //这个赋值如果缺少, 可能会引发程序错误
    while (1)
    {
        newsockfd = accept(sockfd, (struct sockaddr *)&client_addr, &addrlen);
        if (newsockfd < 0)
        {
            perror("accept");
            exit(-1);
        }
        printf("IP & Port:%s %d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
        close(newsockfd); // 关闭客户端连接
    }

    close(sockfd);

    return 0;
}
