#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

// 01-socket 192.168.100.128 8000
// argv[0]   arv[1]          argv[2]
int main(int argc, char const *argv[])
{

    int sockfd, ret, newsockfd;
    struct sockaddr_in myaddr,client_addr;
    socklen_t addrlen ; //客户端地址的大小

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./02-tcp_server ip port)\n");
        exit(-1);
    }

    // 创建一个socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&myaddr, 0, sizeof(myaddr));          // 清空结构体
    memset(&client_addr, 0, sizeof(client_addr));          // 清空结构体
    myaddr.sin_family = AF_INET;                 // 什么类型的通信
    myaddr.sin_port = htons(atoi(argv[2]));      // 设置socket 的固定端口
    myaddr.sin_addr.s_addr = inet_addr(argv[1]); // 把字符串ip转换成网络二进制的ip地址
    ret = bind(sockfd, (struct sockaddr *)&myaddr, sizeof(myaddr));//绑定主机地址和端口，（绑定套接字）
    if (ret < 0)
    {
        perror("bind");
        exit(-1);
    }
    ret = listen(sockfd, 10);
    if (ret < 0)
    {
        perror("listen");
        exit(-1);
    }
    //system("netstat -ant"); 
    while(1)
    {
        //accept接受接收tcp的连接请求
        newsockfd =  accept(sockfd,(struct sockaddr *)&client_addr,&addrlen) ;//addrlen就是客户端地址的大小
        if(newsockfd < 0)
        {
            perror("accept");
            exit(-1);
        }
        //inet_ntoa 把网络二进制的ip地址转成点分十进制的字符串（IP）
        //ntohs端口转换函数                                           （port端口）
        printf("IP & Port:%s %d\n",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));
        close(newsockfd); // 关闭客户端连接
    }

    close(sockfd);

    return 0;
}
