#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// ./11-gethostbyname name
int main(int argc, char const *argv[])
{
    struct sockaddr_in addr;  //定义一个结构体用来复制
    if (argc != 2)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./11-gethostbyname domain )\n");
        exit(-1);
    }
    //定义一个结构体 变量名是hostp
    // gethostbyname 就是struct hostent *类型的
    struct hostent *hostp = gethostbyname(argv[1]);
    if (hostp == NULL)
    {
        printf("%s\n", hstrerror(h_errno));
        exit(-1);
    }
    printf("office name:%s\n", hostp->h_name); // 域名的官方名称

    // 把h_aliases 当成一个字符指针数组, 
    //  char * h_aliases[] ={...} , 这个数组内每一个成员都是 字符指针, 每一个字符指针指向一个字符串
    // 这个数组的最后一个成员是 NULL, 表示是最后一个成员 
    for (int i = 0; hostp->h_aliases[i] != NULL; i++)
    {
        printf("alias name:%s\n", hostp->h_aliases[i]); //别名
    }

    // h_addr_list 当成一个字符指针数组, 
    //  char *h_addr_list[] ={...} , 这个数组内每一个成员都是 字符指针, 每一个字符指针指向一个字符串
    // 这个数组的最后一个成员是 NULL, 表示是最后一个成员 
    for(int i=0;hostp->h_addr_list[i] != NULL;i++)
    {
        memcpy(&addr.sin_addr.s_addr,hostp->h_addr_list[i],hostp->h_length);//把地址复制到地址结构体内
        printf("addresses :%s\n",inet_ntoa(addr.sin_addr));//地址名  
    }

    return 0;
}
