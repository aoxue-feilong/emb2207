#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/un.h>

#define N 128

int main(int argc, char const *argv[])
{
    int ret, sockfd;
    struct sockaddr_un server_addr, client_addr;
    char buf[N] = {0};
    socklen_t addrlen = sizeof(client_addr);

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./19-unix_server send recv)\n");
        exit(-1);
    }
    remove(argv[2]); 

    // 用户数据包 , 本地通信
    sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&server_addr, 0, sizeof(server_addr)); // 清空结构体
    memset(&client_addr, 0, sizeof(client_addr)); // 清空结构体

    server_addr.sun_family = AF_UNIX; // 什么类型的通信
    strcpy(server_addr.sun_path, argv[1]); // send  服务器绑定 

    client_addr.sun_family = AF_UNIX; // 什么类型的通信
    strcpy(client_addr.sun_path, argv[2]);  // recv 客户端绑定 

    ret = bind(sockfd, (struct sockaddr *)&client_addr, sizeof(client_addr));
    if (ret < 0)
    {
        perror("bind");
        exit(-1);
    }

    while (1)
    {
        printf(">:");
        fgets(buf, N, stdin);
        buf[strlen(buf) - 1] = 0; // 消除 '\n'
        ret = sendto(sockfd, buf, N, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
        if (ret < 0)
        {
            perror("sendto");
            exit(-1);
        }
        // 因为接收的消息是服务发送回来的, 客户端已经知道服务器的地址信息, 因此不需要再次保存
        // 可以使用 NULL 来不保存客户端的地址
        ret = recvfrom(sockfd, buf, N, 0, NULL, NULL);
        if (ret < 0)
        {
            perror("recvfrom");
            exit(-1);
        }
        printf("buf=%s\n", buf);
    }

    return 0;
}
