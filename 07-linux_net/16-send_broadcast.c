#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define N 128

int main(int argc, char const *argv[])
{
    int ret, sockfd;
    struct sockaddr_in server_addr, client_addr;
    char buf[N] = {0};
    socklen_t addrlen = sizeof(client_addr);

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./06-udp_server ip port)\n");
        exit(-1);
    }

    // 用户数据包 , 使用udp协议
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    int on = 1;//为真开启
    ret = setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));
    if (ret < 0)
    {
        perror("setsockopt");
        exit(-1);
    }
    memset(&server_addr, 0, sizeof(server_addr));     // 清空结构体
    memset(&client_addr, 0, sizeof(client_addr));     // 清空结构体
    server_addr.sin_family = AF_INET;                 // 什么类型的通信
    server_addr.sin_port = htons(atoi(argv[2]));      // 设置socket 的固定端口
    server_addr.sin_addr.s_addr = inet_addr(argv[1]); // 把字符串ip转换成网络二进制的ip地址
    while (1)
    {
        printf(">:");
        fgets(buf, N, stdin);
        buf[strlen(buf) - 1] = 0; // 消除 '\n'
        ret = sendto(sockfd, buf, N, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
        if (ret < 0)
        {
            perror("sendto");
            exit(-1);
        }
        // 因为接收的消息是服务发送回来的, 客户端已经知道服务器的地址信息, 因此不需要再次保存
        // 可以使用 NULL 来不保存客户端的地址
        ret = recvfrom(sockfd, buf, N, 0, NULL, NULL);
        if (ret < 0)
        {
            perror("recvfrom");
            exit(-1);
        }
        printf("%s&%d:%s\n", inet_ntoa(server_addr.sin_addr), ntohs(server_addr.sin_port), buf);
    }

    return 0;
}
