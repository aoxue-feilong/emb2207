#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define N 128

int main(int argc, char const *argv[])
{
    int ret, sockfd;
    struct sockaddr_in server_addr, client_addr;
    char buf[N] = {0};
    socklen_t addrlen = sizeof(client_addr);

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./06-udp_server ip port)\n");
        exit(-1);
    }
     

    // 用户数据包 , 使用udp协议
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&server_addr, 0, sizeof(server_addr));     // 清空结构体
    memset(&client_addr, 0, sizeof(client_addr));     // 清空结构体
    server_addr.sin_family = AF_INET;                 // 什么类型的通信
    server_addr.sin_port = htons(atoi(argv[2]));      // 设置socket 的固定端口
    server_addr.sin_addr.s_addr = inet_addr(argv[1]); // 把字符串ip转换成网络二进制的ip地址
    ret = bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (ret < 0)
    {
        perror("bind");
        exit(-1);
    }

    while (1)
    {
        ret = recvfrom(sockfd, buf, N, 0, (struct sockaddr *)&client_addr, &addrlen);
        if (ret < 0)
        {
            perror("recvfrom");
            exit(-1);
        }
        printf("%s&%d:%s\n",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port),buf);
        strcpy(buf, "server is received!!");
        ret = sendto(sockfd, buf, N, 0, (struct sockaddr *)&client_addr, sizeof(client_addr));
        if (ret < 0)
        {
            perror("sendto");
            exit(-1);
        }
    }

    return 0;
}
