#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>

#define N 128

int process_list(int newsockfd, char *buf)
{
    struct dirent *dt;
    int ret;
    DIR *dirp = opendir(".");//.表示当前目录，打开当前的目录
    if (dirp == NULL)
    {
        perror("opendir");
        exit(-1);
    }
    while ((dt = readdir(dirp)) != NULL)//用dt这个结构体指针接受这个返回值，为空代表目录已经读完
    {
        if (strncmp(dt->d_name, ".", 1) == 0)//带.的是隐藏文件，需要忽略
            continue;
        strcpy(buf, dt->d_name);//字符串赋值函数，将后面的写到前面
        ret = send(newsockfd, buf, N, 0); // 发送给客户端
        if (ret < 0)
        {
            perror("send");
            exit(-1);
        }
    }
    closedir(dirp);//关闭目录指针
    close(newsockfd); // 传输完成后, 关闭和客户端通信的socket , 一旦关闭后, 客户端的recv函数就返回为0
    return 0;
}

int process_get(int newsockfd, char *buf)//下载一个文件
{

    //创建一个socket
    int ret, fd, nbytes;

    // 本地要打开一个已存在的文件
    // get filename
    //buf+4:get加个空格
    fd = open(buf + 4, O_RDONLY|O_EXCL);//打开这个文件，并设置权限
    if (fd < 0)
    {
        perror("open");
        exit(-1);
    }
    // 读本地的文件 , 把文件的内容读到buf内 , 到达文件末尾返回0 
    // read 读错误时, 返回-1 
    while ( (nbytes = read(fd, buf, N)) > 0) // 表示文件还没有读完 
    {
        //printf("buf=%s\n",buf); 
        //printf("nbytes=%d\n",nbytes);
        ret = send(newsockfd, buf, nbytes,0); // 把读回来的数据发送给客户端
        if (ret < 0)
        {
            perror("send");
            exit(-1);
        }
    }
    //printf("nbytes = %d\n",nbytes); 
    close(newsockfd); // 传输完成后, 关闭和客户端通信的socket , 一旦关闭后, 客户端的recv函数就返回为0
    close(fd); 
    return 0;
}

int process_put(int newsockfd, char *buf)//上传服务器
{
    //创建一个socket
    int ret, fd, nbytes;
    // 本地要创建一个文件
    // put filename

    fd = open(buf + 4, O_RDWR | O_CREAT | O_TRUNC, 0664);//打开这个文件并设置文件权限
    if (fd < 0)
    {
        perror("open");
        exit(-1);
    }

    // recv 函数 , 服务器使用close(newsockfd)关闭socket时, recv函数返回值为0
    // recv 函数发生错误时, 返回值为-1
    // 返回读回来的字节数
    // 读回来多少字节写入到文件内多少字节
    while ( (nbytes = recv(newsockfd, buf, N, 0)) > 0) // 表示数据还没有接收完毕
    {
        ret = write(fd, buf, nbytes); // 把读回来的内容写入到文件内
        if (ret < 0)
        {
            perror("write");
            exit(-1);
        }
    }

    close(newsockfd); // 传输完成后, 关闭和客户端通信的socket , 一旦关闭后, 客户端的recv函数就返回为0
    close(fd); 

    return 0;
}

// 01-socket 192.168.1.2  8000
// argv[0]   arv[1]       argv[2]
int main(int argc, char const *argv[])
{

    int sockfd, ret, newsockfd;
    struct sockaddr_in myaddr, client_addr;
    //客户端地址的大小
    socklen_t addrlen=addrlen = sizeof(client_addr); //这个赋值如果缺少, 可能会引发程序错误
    char buf[N] = {0};

    if (argc != 3)//判断参数
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./02-tcp_server ip port)\n");
        exit(-1);
    }

    // 创建一个socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&myaddr, 0, sizeof(myaddr));           // 清空结构体
    memset(&client_addr, 0, sizeof(client_addr)); // 清空结构体
    myaddr.sin_family = AF_INET;                  // 什么类型的通信
    myaddr.sin_port = htons(atoi(argv[2]));       // 设置socket 的固定端口
    myaddr.sin_addr.s_addr = inet_addr(argv[1]);  // 把字符串ip转换成网络二进制的ip地址
    ret = bind(sockfd, (struct sockaddr *)&myaddr, sizeof(myaddr));//绑定主机的地址和端口
    if (ret < 0)
    {
        perror("bind");
        exit(-1);
    }
    ret = listen(sockfd, 10);//设置这个socket 为监听socket , 让这个socket变成服务器的socket。
    //可以接收客户端发送的请求，10:等待队列的长度 ， 客户端请求队列的长度
    if (ret < 0)
    {
        perror("listen");
        exit(-1);
    }
    // system("netstat -ant");
    
    while (1)
    {
        newsockfd = accept(sockfd, (struct sockaddr *)&client_addr, &addrlen); // 接收客户端tcp连接请求
        if (newsockfd < 0)
        {
            perror("accept");
            exit(-1);
        }
        printf("newsockfd=%d\n", newsockfd);
        ret = recv(newsockfd, buf, N, 0); // 接收客户端发来的数据
        if (ret < 0)
        {
            perror("read");
            exit(-1);
        }
//输出IP地址和端口与客户端发来的消息
        printf("%s & %d said :%s\n", inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port), buf);
//判断客户端发来的消息，进而调用相对应的函数
        if (strncmp(buf, "list", 4) == 0)
        {
            process_list(newsockfd, buf);
        }
        else if (strncmp(buf, "get", 3) == 0)
        {
            process_get(newsockfd, buf);
        }
        else if (strncmp(buf, "put", 3) == 0)
        {
            process_put(newsockfd, buf);
        }
        close(newsockfd); // 关闭客户端连接
    }

    close(sockfd);//服务一般不自动退出

    return 0;
}
