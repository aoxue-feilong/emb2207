#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/un.h>



#define   N    128 

// 01-socket 192.168.1.2 8000
// argv[0]   arv[1]      argv[2]
int main(int argc, char const *argv[])
{

    int sockfd, ret;
    struct sockaddr_un server_addr;
    socklen_t addrlen;
    char buf[N]= {0}; 

    if (argc != 2)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./18-tcp_client mysocket)\n");
        exit(-1);
    }
    // 创建一个socket
    sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&server_addr, 0, sizeof(server_addr));     // 清空结构体
    server_addr.sun_family = AF_UNIX;                 // 什么类型的通信
    strcpy(server_addr.sun_path,argv[1]);
    ret = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (ret < 0)
    {
        perror("connect");
        exit(-1);
    }

    while (1)
    {
        printf(">:");
        fgets(buf, N, stdin);
        buf[strlen(buf) - 1] = 0;      // 清除'\n'
        ret = send(sockfd, buf, N, 0); // 发送给服务器
        if (ret < 0)
        {
            perror("send");
            exit(-1);
        }
        if (strncmp(buf, "quit", 4) == 0)
            break;
        memset(buf, 0, sizeof(buf));   // buf 内容清空
        ret = recv(sockfd, buf, N, 0); // 接收服务器发来的数据

        if (ret < 0)
        {
            perror("read");
            exit(-1);
        }

        printf("%s\n",  buf);
    }
    close(sockfd);

    return 0;
}