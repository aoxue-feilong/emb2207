#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define N 128

typedef struct node
{
    struct sockaddr_in addr;
    struct node *next; //结构体指针，和本结构体类型一样的结构体指针
} linklist_t;

typedef struct
{
    char mtype;    //类型
    char mname[N]; //名字
    char mtext[N]; //正文
} msg_t;
// 这个函数的功能是在内存中申请了一块结构体大小的内存
// 并把申请后的地址返回
linklist_t *create_empty_linklinst_head() //创建头结点
{
    linklist_t *h = (linklist_t *)malloc(sizeof(linklist_t));
    memset(&h->addr, 0, sizeof(h->addr)); // 清空结构体 addr
    h->next = NULL;
    return h;
}

int process_login(msg_t *msgp, linklist_t *h, struct sockaddr_in *addr, int sockfd) //登录函数
{
    int ret;
    strcpy(msgp->mtext, "login"); //将 login赋值 到msgp->mtext里面
    linklist_t *p = h->next;      // p指向头结点的下一个节点(头结点不参与运算)
    while (p != NULL)
    {
        // memcmp：内存比较函数
        if (memcmp(&p->addr, addr, sizeof(*addr)) != 0) // 除了自己(addr)以外的所有地址
        {
            // sendto:发送数据到指定的地址上,倒数第二个数:发送的目的地址，倒数第一个数：地址结构体的大小
            ret = sendto(sockfd, msgp, sizeof(msg_t), 0, (struct sockaddr *)&p->addr, sizeof(p->addr));
            if (ret < 0)
            {
                perror("sendto");
                exit(-1);
            }
        }
        p = p->next; // 移动节点
    }

    // 把新登录的成员 加入到链表
    p = (linklist_t *)malloc(sizeof(linklist_t)); // 申请一个节点
    p->addr = *addr;                              // 赋值
    p->next = h->next;                            //第一步
    h->next = p;                                  //第二步

    return 0;
}

int process_chat(msg_t *msgp, linklist_t *h, struct sockaddr_in *addr, int sockfd) //聊天函数
{
    int ret;
    //遍历链表，给除了自己的发送聊天消息
    linklist_t *p = h->next; // p指向头结点的下一个节点(头结点不参与运算)
    while (p != NULL)
    {
        if (memcmp(&p->addr, addr, sizeof(*addr)) != 0) // 除了自己(addr)以外的所有地址
        {
            ret = sendto(sockfd, msgp, sizeof(msg_t), 0, (struct sockaddr *)&p->addr, sizeof(p->addr));
            if (ret < 0)
            {
                perror("sendto");
                exit(-1);
            }
        }
        p = p->next; // 移动节点
    }
    return 0;
}

int process_quit(msg_t *msgp, linklist_t *h, struct sockaddr_in *addr, int sockfd) //退出函数
{
    // 正常删除链表中的1个节点, 需要把要删除链表的前一个节点和后一个节点连接
    // 需要找到要删除节点的前一个节点才可以删除
    int ret;
    //遍历链表，给除了自己的发送自己的退出的消息（删除时释放节点）
    strcpy(msgp->mtext, "quit");
    linklist_t *p = h->next; // p指向头结点的下一个节点(头结点不参与运算)
    linklist_t *q;           //用来保存要删除的节点
    while (p != NULL)
    {
        if (memcmp(&p->addr, addr, sizeof(*addr)) != 0) // 除了自己(addr)以外的所有地址
        {
            ret = sendto(sockfd, msgp, sizeof(msg_t), 0, (struct sockaddr *)&p->addr, sizeof(p->addr));
            if (ret < 0)
            {
                perror("sendto");
                exit(-1);
            }
        }
        p = p->next; // 移动节点
    }

    // 从链表中删除 退出的成员
    // p 要定位到 删除节点的前一个节点
    p = h; // p 定位到头结点, 就是地址信息的前一个节点
           
    while (p->next != NULL)
    {
        //头结点的下一个节点：p->next->addr，addr：传进来的地址。两者进行比较
        if (memcmp(&p->next->addr, addr, sizeof(*addr)) == 0)
        {
            // 删除节点
            q = p->next;       // 保存要删除的节点
            p->next = q->next; // 接链表
            free(q);           //释放节点
            return 0;          // 函数直接返回即可 , 这个不能省略, 省略后会造成最后一个节点崩溃
        }
        p = p->next; // 移动节点
    }

    return 0;
}

int main(int argc, char const *argv[])
{
    int ret, sockfd;
    struct sockaddr_in server_addr, client_addr;
    char buf[N] = {0};

    socklen_t addrlen = sizeof(client_addr);

    if (argc != 3)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./06-udp_server ip port)\n");
        exit(-1);
    }
    msg_t msg;
    memset(&msg, 0, sizeof(msg)); // 清空结构体

    // 用户数据包 , 使用udp协议
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(-1);
    }
    printf("sockfd=%d\n", sockfd);

    memset(&server_addr, 0, sizeof(server_addr));     // 清空结构体
    memset(&client_addr, 0, sizeof(client_addr));     // 清空结构体
    server_addr.sin_family = AF_INET;                 // 什么类型的通信
    server_addr.sin_port = htons(atoi(argv[2]));      // 设置socket 的固定端口
    server_addr.sin_addr.s_addr = inet_addr(argv[1]); // 把字符串ip转换成网络二进制的ip地址
    ret = bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));//绑定主机的IP和端口
    if (ret < 0)
    {
        perror("bind");
        exit(-1);
    }
    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork");
        exit(-1);
    }
    else if (pid == 0) // 子进程（广播）
    {
        msg.mtype = 'C';
        strcpy(msg.mname, "server");
        while (1)
        {
            printf("请输入要广播的内容>:");
            fgets(msg.mtext, N, stdin);
            msg.mtext[strlen(msg.mtext) - 1] = 0;
            ret = sendto(sockfd, &msg, sizeof(msg_t), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
            if (ret < 0)
            {
                perror("sendto");
                exit(-1);
            }
        }
    }
    else // 父进程
    {
        linklist_t *H = create_empty_linklinst_head();
        while (1)
        {
            // &client_addr：发送数据的源地址， 一般是客户端的地址
            ret = recvfrom(sockfd, &msg, sizeof(msg), 0, (struct sockaddr *)&client_addr, &addrlen);//接受数据
            if (ret < 0)
            {
                perror("recvfrom");
                exit(-1);
            }
            printf("%s&%d:%c\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port), msg.mtype);
            switch (msg.mtype)
            {
            case 'L':
                process_login(&msg, H, &client_addr, sockfd);
                break;
            case 'C':
                process_chat(&msg, H, &client_addr, sockfd);
                break;
            case 'Q':
                process_quit(&msg, H, &client_addr, sockfd);
                break;
            default:
                break;
            }
        }
    }

    return 0;
}
