#include "adc.h"
#include <stdio.h>
#include <oled.h>






/*******************************************************************************
* 函 数 名         : adc_init
* 函数功能		   : IO端口时钟初始化函数	   
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/
void adc_battery_init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO|RCC_APB2Periph_ADC1,ENABLE);

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);//12M  最大14M 设置ADC时钟（ADCCLK）

	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0;//ADC 通道 IN0 
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AIN;	//模拟输入
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);


	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; 
	ADC_InitStructure.ADC_ScanConvMode = DISABLE; 
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; 
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; 
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; 
	ADC_InitStructure.ADC_NbrOfChannel = 1; // 转换的通道数量
	ADC_Init(ADC1, &ADC_InitStructure);
	
	//设置指定ADC的规则组通道，设置它们的转化顺序和采样时间
	ADC_RegularChannelConfig(ADC1,ADC_Channel_0,1,ADC_SampleTime_239Cycles5);
	
	ADC_Cmd(ADC1,ENABLE);	

	ADC_ResetCalibration(ADC1);//重置指定的ADC的校准寄存器
	while(ADC_GetResetCalibrationStatus(ADC1));//获取ADC重置校准寄存器的状态
	
	ADC_StartCalibration(ADC1);//开始指定ADC的校准状态
	while(ADC_GetCalibrationStatus(ADC1));//获取指定ADC的校准程序
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);//使能或者失能指定的ADC的软件转换启动功能
}

void adc_temperature_init(void)
{
//	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO|RCC_APB2Periph_ADC1,ENABLE);

	RCC_ADCCLKConfig(RCC_PCLK2_Div6);//12M  最大14M 设置ADC时钟（ADCCLK）

//	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_1;//ADC 通道 IN1
//	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0;//ADC 通道 IN0 
//	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AIN;	//模拟输入
//	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
//	GPIO_Init(GPIOA,&GPIO_InitStructure);


	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; 
	ADC_InitStructure.ADC_ScanConvMode = DISABLE; 
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; 
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; 
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; 
	ADC_InitStructure.ADC_NbrOfChannel = 1; // 转换的通道数量
	ADC_Init(ADC1, &ADC_InitStructure);
	
	
	//内部温度传感器是在ADC1通道16的。
	ADC_RegularChannelConfig(ADC1,ADC_Channel_16,1,ADC_SampleTime_239Cycles5);
	ADC_TempSensorVrefintCmd(ENABLE);//打开内部温度传感器使能
	ADC_Cmd(ADC1,ENABLE);	

	ADC_ResetCalibration(ADC1);//重置指定的ADC的校准寄存器
	while(ADC_GetResetCalibrationStatus(ADC1));//获取ADC重置校准寄存器的状态
	
	ADC_StartCalibration(ADC1);//开始指定ADC的校准状态
	while(ADC_GetCalibrationStatus(ADC1));//获取指定ADC的校准程序
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);//使能或者失能指定的ADC的软件转换启动功能
}
void adc_vr_init(void)
{

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO|RCC_APB2Periph_ADC1,ENABLE);
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);//72/6=12M  最大14M 设置ADC时钟（ADCCLK）
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_1;//ADC 通道 IN1 pa1
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AIN;	//模拟输入
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);

	ADC_InitTypeDef ADC_InitStructure;
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; 
	ADC_InitStructure.ADC_ScanConvMode = DISABLE; 
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; 
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; 
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; 
	ADC_InitStructure.ADC_NbrOfChannel = 1; // 转换的通道数量
	ADC_Init(ADC1, &ADC_InitStructure);
	
	//设置指定ADC的规则组通道，设置它们的转化顺序和采样时间
	ADC_RegularChannelConfig(ADC1,ADC_Channel_1,1,ADC_SampleTime_239Cycles5);
	
	ADC_Cmd(ADC1,ENABLE);	

	ADC_ResetCalibration(ADC1);//重置指定的ADC的校准寄存器
	while(ADC_GetResetCalibrationStatus(ADC1));//获取ADC重置校准寄存器的状态
	
	ADC_StartCalibration(ADC1);//开始指定ADC的校准状态
	while(ADC_GetCalibrationStatus(ADC1));//获取指定ADC的校准程序
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);//使能或者失能指定的ADC的软件转换启动功能
}

void adc_vr_test(void)
{
		int32_t adc_val=0  ; 
		char str[100]={0};
		OLED_ShowStr(16,0,(unsigned char *)"ADC VR Test",2);     //测试6*16字符
	
		//设置指定ADC的规则组通道，设置它们的转化顺序和采样时间
		ADC_RegularChannelConfig(ADC1,ADC_Channel_1,1,ADC_SampleTime_239Cycles5);
		for(int i=0;i<10;i++)
		{ 	
			ADC_SoftwareStartConvCmd(ADC1, ENABLE); // 启动adc 
			while(!ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC));//adc 转换结束标志
			adc_val += ADC_GetConversionValue(ADC1);//返回最近一次ADCx规则组的转换结果	
		}
		adc_val=adc_val/10;
	
		// 0   -----0  v 
		// 4096-----3.3v
		// val -----x  v 
		float val = (adc_val*3.3/4096); 
	
		printf("voltage is %.2fV\n",val);
		sprintf((char * )str,"VR: %.2fV",val);
		OLED_ShowStr(0,5,(unsigned char *)str,2);     //测试6*16字符
}

void adc_battery_test(void)
{
		int32_t adc_val=0  ; 
		char str[100]={0};
		OLED_ShowStr(0,0,(unsigned char *)"ADC Battery Test",2);     //测试6*16字符
	
		//设置指定ADC的规则组通道，设置它们的转化顺序和采样时间
		ADC_RegularChannelConfig(ADC1,ADC_Channel_0,1,ADC_SampleTime_239Cycles5);
		for(int i=0;i<10;i++)
		{ 	
			ADC_SoftwareStartConvCmd(ADC1, ENABLE);
			while(!ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC));//adc 转换结束标志
			adc_val += ADC_GetConversionValue(ADC1);//返回最近一次ADCx规则组的转换结果	
		}
		adc_val = adc_val/10;
	
		// 0   -----0  v 
		// 4096-----3.3v
		// val -----x  v 
		float val = (adc_val*3.3/4096) *2; 
	
		printf("battery is %.2fV\n",val);
		sprintf((char * )str,"Bat: %.2fV",val);
		OLED_ShowStr(0,5,(unsigned char *)str,2);     //测试6*16字符
}

void adc_temperature_test(void)
{
		int32_t adc_val=0  ; 
		char str[100]={0};
		OLED_ShowStr(0,0,(unsigned char *)"ADC Battery Test",2);     //测试6*16字符
	
		//设置指定ADC的规则组通道，设置它们的转化顺序和采样时间
		ADC_RegularChannelConfig(ADC1,ADC_Channel_16,1,ADC_SampleTime_239Cycles5);
		for(int i=0;i<10;i++)
		{ 	
			ADC_SoftwareStartConvCmd(ADC1, ENABLE);
			while(!ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC));//adc 转换结束标志
			adc_val += ADC_GetConversionValue(ADC1);//返回最近一次ADCx规则组的转换结果	
		}
		adc_val = adc_val/10;
	
		// 0   -----0  v 
		// 4096-----3.3v
		// val -----x  v 
		float val=(1.43-adc_val*3.3/4096)/0.0043+25;
		printf("temp is %.1f C\n",val);
		sprintf((char * )str,"temp: %.1f C",val);
		OLED_ShowStr(0,5,(unsigned char *)str,2);     //测试6*16字符
}


