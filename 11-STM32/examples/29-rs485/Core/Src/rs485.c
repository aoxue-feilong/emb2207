	    
#include "rs485.h"	
#include "main.h"
#include "stdint.h"	
#include "oled.h"	
#include "usart.h"	
#include "string.h"	
////初始化IO 串口2
////pclk1:PCLK1时钟频率(Mhz)
////bound:波特率	  
//void RS485_Init(u32 bound)
//{  
//		GPIO_InitTypeDef  GPIO_InitStructure ;
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
//		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 ; // 	PB2  RS485_WR ENABLE
//		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz ; 
//		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ; 
//		GPIO_Init( GPIOB, &GPIO_InitStructure);
//		GPIO_ResetBits(GPIOB ,GPIO_Pin_2); // 低电平  默认为接收模式
//		UART2_Init(bound);
//	
//}

//RS485发送len个字节.
//buf:发送区首地址
//len:发送的字节数(为了和本代码的接收匹配,这里建议不要超过64个字节)
void RS485_Send_Data(uint8_t *pData,uint8_t Size)
{
//	 GPIO_SetBits(GPIOB ,GPIO_Pin_2); 
	   HAL_GPIO_WritePin(RS485WR_GPIO_Port,RS485WR_Pin,GPIO_PIN_SET);//  RS485_WR ENABLE 高电平  发送模式
//	 USART_SendBytes(USART2, buf,len);
	   HAL_UART_Transmit(&huart2,pData,Size,1000);
//	 GPIO_ResetBits(GPIOB ,GPIO_Pin_2); 
	   HAL_GPIO_WritePin(RS485WR_GPIO_Port,RS485WR_Pin,GPIO_PIN_RESET);// 低电平  默认为接收模式	
}
extern uint8_t Key_Value;  // 用来表示当前按下的按键 
void RS485_Test()
{
		  OLED_ShowStr(0+16,0,(unsigned char *)"RS485 Test",2);
			if( Key_Value == KEY_OK ) 
			{
				Key_Value = 0 ; 
				RS485_Send_Data((uint8_t *)"hello world",strlen("hello world")+1);
			}
}
		





















