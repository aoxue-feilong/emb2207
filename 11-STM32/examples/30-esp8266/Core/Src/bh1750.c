#include "bh1750.h"
#include "i2c.h"
#include "stdio.h"
#include "oled.h"


void BH1750_WriteByte(uint8_t data)
{
		// I2C_WriteByte(I2C1,BH1750_Addr,&data,1) ;
		// BH1750_Addr 器件地址 
		// addr 发送的数据地址 
		// 1 :发送的字节数 
		// 1000 : 超时时间 
		 HAL_I2C_Master_Transmit(&hi2c1, BH1750_Addr,&data, 1, 1000); 
}
uint16_t BH1750_Read(void)
{
		uint8_t msg[2] = {0,0}; 
		//I2C_ReadByte(I2C1,BH1750_Addr,msg,2) ;
		HAL_I2C_Master_Receive(&hi2c1,BH1750_Addr,msg,2,1000); 
		return ( (msg[0]<<8) + msg[1] ) ;
}
void BH1750_Init(void)
{
		BH1750_WriteByte(BH1750_POWER_ON);	 //power on
		BH1750_WriteByte(BH1750_RSET);	     //reset 
		BH1750_WriteByte(BH1750_CONTINUOUSLY);  //连续测量H分辨率模式 分别率1 lx，至少120ms，之后自动断电模式 
		HAL_Delay(130);
}

void BH1750_Test(void)
{			
		char str[100] = {0};
		uint16_t lx = 0; 
		OLED_ShowStr(16,0,(uint8_t *)"BH1750 TEST",2);      //测试6*16字符
		lx =  BH1750_Read(); 
		printf("lx=%.0f\n",(float)lx/1.2);
		sprintf(str,"lx: %6.0f",lx/1.2);
		OLED_ShowStr(0,5,(uint8_t *)str,2);      //测试6*16字符
		HAL_Delay(130);  // 光线传感器adc转换的等待时间 
		
		HAL_Delay(1000);  //  延时1秒 
		
}


