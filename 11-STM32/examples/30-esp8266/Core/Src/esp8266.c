
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "esp8266.h"
#include "main.h"
#include "usart.h"




void ESP8266_Connect_WIFI(void)
{
			uint8_t buf[100]={0}; 
			HAL_Delay(2000);
			strcpy((char *)buf,"AT+CWJAP=\"CMCC-ShengLi\",\"drbder6t\"\r\n");
			//USART_SendBytes(USART3,buf,sizeof(buf));
			HAL_UART_Transmit(&huart3, buf,sizeof(buf), 1000); // 发送一个字节的数据 
			HAL_Delay(2000);
			strcpy((char *)buf,"AT+CIFSR\r\n"); //,  获取ip 信息
			HAL_UART_Transmit(&huart3, buf,sizeof(buf), 1000); // 发送一个字节的数据 
			HAL_Delay(50);

			
}
void ESP8266_Connect_Host(void)
{
			uint8_t buf[100]={0}; 
			HAL_Delay(2000);
			strcpy((char *)buf,"AT\r\n");
			HAL_UART_Transmit(&huart3, buf,sizeof(buf), 1000); // 发送一个字节的数据 
			HAL_Delay(50);
			strcpy((char *)buf,"AT+CIPMUX=0\r\n"); // 单链接 
			HAL_UART_Transmit(&huart3, buf,sizeof(buf), 1000); // 发送一个字节的数据 
			HAL_Delay(50);
			strcpy((char *)buf,"AT+CIPMODE=1\r\n"); // 设置透传模式 
			HAL_UART_Transmit(&huart3, buf,sizeof(buf), 1000); // 发送一个字节的数据 
			HAL_Delay(50);
			strcpy((char *)buf,"AT+CIPSTART=\"TCP\",\"192.168.1.101\",9000\r\n");
			HAL_UART_Transmit(&huart3, buf,sizeof(buf), 1000); // 发送一个字节的数据 
			HAL_Delay(2000);
			strcpy((char *)buf,"AT+CIPSEND\r\n");  // 发送数据 
			HAL_UART_Transmit(&huart3, buf,sizeof(buf), 1000); // 发送一个字节的数据 
			
}
void ESP8266_Disconnect(void) // 退出透传模式 
{
			uint8_t buf[100]={0}; 
			strcpy((char *)buf,"+++");
			HAL_UART_Transmit(&huart3, buf,sizeof(buf), 1000); // 发送一个字节的数据 
			HAL_Delay(50);
}

void ESP8266_Test(void)
{
			if(Key_Value == KEY_UP)  // 连接服务器
			{
					ESP8266_Connect_Host();
			}
			else if(Key_Value == KEY_DOWN) // 连接wifi 
			{
					ESP8266_Connect_WIFI();
			}
			else if(Key_Value == KEY_LEFT) // 退出透传 
			{
					ESP8266_Disconnect();
			}
			Key_Value = 0 ; 
}
