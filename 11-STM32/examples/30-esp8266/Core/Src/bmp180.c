#include <stdio.h>
#include <math.h>
#include <string.h>

#include "i2c.h"
#include "bmp180.h"
#include "oled.h"



double  result_UT=0;  // 温度 
double  result_UP=0;  // 大气压 
double  altitude;	    // 海拔   
//s16 ac1;
//s16 ac2; 
//s16 ac3; 
//u16 ac4;
//u16 ac5;
//u16 ac6;
//s16 b1; 
//s16 b2;
//s16 mb;
//s16 mc;
//s16 md;

int16_t ac1;
int16_t ac2; 
int16_t ac3; 
uint16_t ac4;
uint16_t ac5;
uint16_t ac6;
int16_t b1; 
int16_t b2;
int16_t mb;
int16_t mc;
int16_t md;



uint16_t bmp180_read_byte(uint8_t addr)
{
	 uint8_t data[2] ={0} ; 
	// I2C_WriteByte(I2C1,BMP180_ADDR,&addr,1);
	// OLED_ADDRESS 器件地址 
	// addr 发送的数据地址 
	// 1 :发送的字节数 
	// 1000 : 超时时间 
	 HAL_I2C_Master_Transmit(&hi2c1, BMP180_ADDR,&addr, 1, 1000); 
	 
	 
	 //I2C_ReadByte (I2C1,BMP180_ADDR,data,2);
	 HAL_I2C_Master_Receive(&hi2c1,BMP180_ADDR,data,2,1000); 
	 return (data[0]<<8 | data[1]);	
}

void BMP180_Init(void)
{
		ac1 = bmp180_read_byte(0xAA);
		ac2 = bmp180_read_byte(0xAC);
		ac3 = bmp180_read_byte(0xAE);
		ac4 = bmp180_read_byte(0xB0);
		ac5 = bmp180_read_byte(0xB2);
		ac6 = bmp180_read_byte(0xB4);
		b1 =  bmp180_read_byte(0xB6);
		b2 =  bmp180_read_byte(0xB8);
		mb =  bmp180_read_byte(0xBA);
		mc =  bmp180_read_byte(0xBC);
		md =  bmp180_read_byte(0xBE);
	
}

// 读取温度的值 
uint16_t BMP180_Read_Temperature(void)
{

	 uint8_t data[2] ={0};
	 data[0] = 0xF4 ;
	 data[1] = 0x2E ;
	 //I2C_WriteByte(I2C1,BMP180_ADDR,data,2);
	 HAL_I2C_Master_Transmit(&hi2c1, BMP180_ADDR,data,2, 1000); 
	 HAL_Delay(10); // 等待adc转换结束 
	 data[0] = 0xF6 ;
	 //I2C_WriteByte(I2C1,BMP180_ADDR,&data[0],1);
	 HAL_I2C_Master_Transmit(&hi2c1, BMP180_ADDR,data,1, 1000); 
	 memset((void *)data,0,sizeof(data));
	 //I2C_ReadByte (I2C1,BMP180_ADDR,data,2);
	 HAL_I2C_Master_Receive(&hi2c1,BMP180_ADDR,data,2,1000); 
	 return ( (data[0]<<8) | data[1] ) ;	
}

// 读取大气压的值 
uint16_t BMP180_Read_Pressure(void)
{
	 uint8_t data[3] ={0};
	 data[0] = 0xF4 ;
	 data[1] = 0x34 ;
	 //I2C_WriteByte(I2C1,BMP180_ADDR,data,2);
	 HAL_I2C_Master_Transmit(&hi2c1, BMP180_ADDR,data,2, 1000); 
	 HAL_Delay(30); // 等待adc转换结束 
	 data[0] = 0xF6 ;
	 //I2C_WriteByte(I2C1,BMP180_ADDR,&data[0],1);
	 HAL_I2C_Master_Transmit(&hi2c1, BMP180_ADDR,data,1, 1000); 
	 memset((void *)data,0,sizeof(data));
	 //I2C_ReadByte (I2C1,BMP180_ADDR,data,3);
	 HAL_I2C_Master_Receive(&hi2c1,BMP180_ADDR,data,2,1000); 
	 return ( (data[0]<<16) | (data[1]<<8) | data[2] )>>8 ;
}


// 对数据进行校准  
void BMP180_Convert()
{
		unsigned int ut;
		unsigned long up;
		long x1, x2, b5, b6, x3, b3, p;
		unsigned long b4, b7;

		ut = BMP180_Read_Temperature();	   // 读取温度
		up = BMP180_Read_Pressure();  // 读取压强    return pressure;	
		//*************
		x1 = (((long)ut - (long)ac6)*(long)ac5) >> 15;
		x2 = ((long) mc << 11) / (x1 + md);
		b5 = x1 + x2;
		result_UT = ((b5 + 8) >> 4);
		//*************		
		b6 = b5 - 4000;
														 // Calculate B3
		x1 = (b2 * (b6 * b6)>>12)>>11;
		x2 = (ac2 * b6)>>11;
		x3 = x1 + x2;
		b3 = (((((long)ac1)*4 + x3)<<OSS) + 2)>>2;	
														 // Calculate B4
		x1 = (ac3 * b6)>>13;
		x2 = (b1 * ((b6 * b6)>>12))>>16;
		x3 = ((x1 + x2) + 2)>>2;
		b4 = (ac4 * (unsigned long)(x3 + 32768))>>15;
		
		b7 = ((unsigned long)(up - b3) * (50000>>OSS));
		if (b7 < 0x80000000)
		p = (b7<<1)/b4;
		else
		p = (b7/b4)<<1;
		
		x1 = (p>>8) * (p>>8);
		x1 = (x1 * 3038)>>16;
		x2 = (-7357 * p)>>16;
		result_UP = p+((x1 + x2 + 3791)>>4);
}

void BMP180_Test(void)
{
		char buf[100]={0};
		OLED_ShowStr(16,0,(unsigned char *)"BMP180 Test",2);     //测试6*16字符
		BMP180_Convert();
		altitude=44330.0*(1-pow((float)result_UP/101325,1/5.255));
		printf("temp    =%.1f  ",result_UT/10);
		printf("presure =%.1f KPa ",result_UP/1000);
		printf("altitude=%f M\n",altitude);
		
		sprintf(buf,"temp  =%.1f",result_UT/10);
		OLED_ShowStr(0,2,(unsigned char *)buf,2);     //测试6*16字符
		sprintf(buf,"presure=%.1fKPa",result_UP/1000);
		OLED_ShowStr(0,4,(unsigned char *)buf,2);     //测试6*16字符
		sprintf(buf,"altitude=%5.1fM",altitude);
		OLED_ShowStr(0,6,(unsigned char *)buf,2);     //测试6*16字符
		HAL_Delay(1000); // 延时1秒 
		
}

