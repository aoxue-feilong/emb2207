/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "tim.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/

/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles EXTI line[9:5] interrupts.
  */
void EXTI9_5_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI9_5_IRQn 0 */

  /* USER CODE END EXTI9_5_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(KEY_LEFT_Pin);
  HAL_GPIO_EXTI_IRQHandler(KEY_ESC_Pin);
  /* USER CODE BEGIN EXTI9_5_IRQn 1 */

  /* USER CODE END EXTI9_5_IRQn 1 */
}

/**
  * @brief This function handles EXTI line[15:10] interrupts.
  */
void EXTI15_10_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI15_10_IRQn 0 */

  /* USER CODE END EXTI15_10_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(KEY_OK_Pin);
  HAL_GPIO_EXTI_IRQHandler(KEY_UP_Pin);
  HAL_GPIO_EXTI_IRQHandler(KEY_DOWN_Pin);
  HAL_GPIO_EXTI_IRQHandler(KEY_RIGHT_Pin);
  /* USER CODE BEGIN EXTI15_10_IRQn 1 */

  /* USER CODE END EXTI15_10_IRQn 1 */
}

/* USER CODE BEGIN 1 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  //PB13 按键的UP
  if (GPIO_Pin==KEY_UP_Pin)//表示 UP按下的后触发的中断
  {
		//设置100%的占空比，高电平占整个周期的百分比
		//整个周期都是高电平 led 会亮
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_3,1000);
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_4,1000);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_3);//设置TIM4 的通道3 产生波形
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_4);//设置TIM4 的通道4 产生波形
  }	
	
	//PB14 按键DOwn
	if(GPIO_Pin==KEY_DOWN_Pin)//表示 DOWN按下的后触发的中断
	{
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_3,800);
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_4,800);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_3);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_4);
	}
	
	//PB5  按键left
	if(GPIO_Pin==KEY_LEFT_Pin)//表示 LEFT按下的后触发的中断
	{
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_3,600);
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_4,600);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_3);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_4);
	}
	
	//PB15  按键right
	if(GPIO_Pin==KEY_RIGHT_Pin)//表示 RIGHT按下的后触发的中断
	{
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_3,400);
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_4,400);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_3);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_4);
	}

	//PB12  按键ok 
	if(GPIO_Pin==KEY_OK_Pin)//表示 OK按下的后触发的中断
	{
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_3,200);
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_4,200);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_3);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_4);
	}
	
	//PB12  按键ESC
	if(GPIO_Pin==KEY_ESC_Pin)//表示 ESC按下的后触发的中断
	{
		//设置100%的占空比，高电平占整个周期的百分比
		//整个周期都是电平 led 会灭
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_3,0);
		__HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_4,0);
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_3);//设置TIM4 的通道3 产生波形
		HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_4);//设置TIM4 的通道4 产生波形
	}
}
/* USER CODE END 1 */
