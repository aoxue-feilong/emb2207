#include <stdio.h>
#include <string.h>
#include <oled.h>
#include "dht11.h"
#include "tim.h"
#include "main.h"

void Delay_Us(uint32_t us)
{
	uint16_t differ = 0xffff-us-5;
	__HAL_TIM_SET_COUNTER(&htim3,differ);
	HAL_TIM_Base_Start(&htim3);
	while(differ < 0xffff-5)
	{
		differ = __HAL_TIM_GET_COUNTER(&htim3);
	}
	HAL_TIM_Base_Stop(&htim3);
}

void DHT11_GPIO_Input(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = DHT11_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(DHT11_GPIO_Port, &GPIO_InitStruct);
}

void DHT11_GPIO_Output(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  GPIO_InitStruct.Pin = DHT11_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(DHT11_GPIO_Port, &GPIO_InitStruct);
}

void DHT11_Reset(void)
{
    // 按照DHT11手册步骤
    DHT11_GPIO_Output();
    DHT11_OUT_L; // 输出低电平 
    HAL_Delay(19); // 输出19ms 
    DHT11_OUT_H;
    Delay_Us(30);  // 延时30us  
    DHT11_GPIO_Input();
}

uint16_t DHT11_Scan(void)
{
    return DHT11_IN;
}
// 读取一个位的值  , 0 或则是1  
uint16_t DHT11_Read_Bit(void)
{
    while (DHT11_IN == RESET);
    Delay_Us(40);
    if (DHT11_IN == SET)
    {
        while (DHT11_IN == SET);
        return 1;
    }
    else
    {
        return 0;
    }
}

// 读取一个字节的数  
uint16_t DHT11_Read_Byte(void)
{
    uint16_t i;
    uint16_t data = 0;
    for (i = 0; i < 8; i++)
    {
        data <<= 1;
        data |= DHT11_Read_Bit();
    }
    return data;
}

uint16_t DHT11_Read_Data(uint8_t buffer[5])
{
    uint16_t i = 0;
		uint8_t checksum = 0 ; 
    DHT11_Reset();
		Delay_Us(20);
    if(DHT11_Scan() == RESET)
    {
        //检测到DHT11响应
        while (DHT11_Scan() == RESET);
        while (DHT11_Scan() == SET);
        for (i = 0; i < 5; i++)
        {
            buffer[i] = DHT11_Read_Byte();
        }
        while (DHT11_Scan() == RESET); // 读取结束 
        DHT11_GPIO_Output();
        DHT11_OUT_H;

			
        checksum = buffer[0] + buffer[1] + buffer[2] + buffer[3];
        if (checksum == buffer[4]) // 校验成功 
        {
            // checksum chenggong
            return 1;
        }
    }
    
    return 0;
}

void DHT11_Test(void)
{
	uint8_t buffer[5]={0};
	char str[64]={0};
	static float temp,hum;
	if(DHT11_Read_Data(buffer) == SET)
	{ 
		hum  = buffer[0] + buffer[1] / 10.0;
		temp = buffer[2] + buffer[3] / 10.0;
	}
	//通过串口输车仅串口调试工具
	printf("hum :%.1f %%RH \n",hum);
	printf("temp:%.1f C\n",temp);
	OLED_ShowStr(32,0, (unsigned char*)"DHT11 Test", 2);
	sprintf(str,"hum :%.1f %%RH",hum);
	OLED_ShowStr(0,2, (unsigned char *)str, 2);
	memset(str,0,32);
	sprintf(str,"temp:%.1f C",temp);
	OLED_ShowStr(0,4, (unsigned char *)str, 2);
	HAL_Delay(1000);
}



