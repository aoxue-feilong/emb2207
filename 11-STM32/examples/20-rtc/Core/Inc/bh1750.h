#ifndef __BH1750_H
#define __BH1750_H
#include "stdint.h"


#define I2C_TIMEOUT               40000
#define BH1750_Addr               0x46
#define BH1750_POWER_ON           0x01
#define BH1750_CONTINUOUSLY       0x10 
#define BH1750_ONE_TIME           0x20
#define BH1750_RSET               0x07


void BH1750_Init(void);
uint16_t  BH1750_Read(void) ;
void BH1750_Test(void);

#endif /* __BH1750_H */
