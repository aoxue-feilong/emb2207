#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "oled.h"
#include "control.h"
#include "bh1750.h"
#include "queue.h"
#include "main.h"
#include "dht11.h"
#include "adc.h"
#include "bmp180.h"
#include "adc.h"
#include "rc522.h"
#include "usart.h"
void STM32_Control_Init(void)
{
		QUART1	= Create_Empty_Queue();	 // usb转串口 
		QUART3	= Create_Empty_Queue();	 // wifi 模块 
}

void STM32_Control(sequeue_t *UART_SQ)
{
			
			uint8_t RecvData =0;
			while( !Queue_Is_Empty(UART_SQ) )
			{ // 队列内有数据时, 执行if 语句 
				 Dequeue(UART_SQ,&RecvData) ;
				 if(RecvData == 'a'){
					  Dequeue(UART_SQ,&RecvData) ;
						switch(RecvData){
							case '1':
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b'){
										//	UART1_Data_Ready_Flag = 0;
										//	led_on(1);
											HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET); // LED1 亮 
											printf("c1sd");
									}
									else { printf("c1fd");}
									break ;
							case '2':
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{
											//UART1_Data_Ready_Flag = 0;
											//led_off(1); //
											HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET); // LED1 灭 										
											printf("c2sd");										
									}
									else { printf("c2fd");}
									break ;
							case '3':
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{
											//UART1_Data_Ready_Flag = 0;
											//led_on(2);	 // 
											HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET); // LED2 亮 
                      printf("c3sd");																
									}
									else { printf("c3fd");}
									break ; 
							case '4':
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{
											//UART1_Data_Ready_Flag = 0;
											//led_off(2);	 //	 
											HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET); // LED2 灭 		
                      printf("c4sd");														
									}
									else { printf("c4fd");}
									break;
							case '5':
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{
											//beep_on();	 //	 
											HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);	 // beep 响 
                      printf("c5sd");														
									}
									else { printf("c5fd");}
									break;
						  case '6':
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{
											//beep_off();	 //	 
											HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);	 // beep 灭
                      printf("c6sd");														
									}
									else { printf("c6fd");}
									break;
							case '7':  // 锂电池电压 
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{         
											      OLED_CLS();//清屏
				                		uint8_t str[100]={0};
														uint32_t adc_value = 0 ; 
														for(uint8_t i=0;i<10;i++)
														{
																HAL_ADC_Start(&hadc1); // 启动ADC , 启动一次工作一次 
																if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK )   //  等待adc 转换结束 
																{
																		adc_value += HAL_ADC_GetValue(&hadc1) ; 
																}
														
														}
														adc_value = adc_value /10 ;  // 10次求平均 
																			
														// 0   -----0  v 
														// 4095-----3.3v
														// val -----x  v 
														float val = (adc_value*3.3/4095); 
														printf("voltage is %.2fV\n",val*2); // 锂电池电压范围 3.6-4.2 之间 
														sprintf((char * )str,"BAT: %.2fV",val*2);
														OLED_ShowStr(0,5,(unsigned char *)str,2);     //测试6*16字符	

									}
									else { printf("c7fd");}
									break;
							case '8': // dht11 
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{
										  	OLED_CLS();//清屏
											uint8_t buffer[5]={0};
											float temp,hum;
											for(int i=0;i<5;i++)
											{
													if(DHT11_Read_Data(buffer) == SET)
													{ 
														hum  = buffer[0] + buffer[1] / 10.0;
														temp = buffer[2] + buffer[3] / 10.0;
														printf("cB%.1fd ",temp);
														printf("cC%.1fd ",hum);	
														break ;
													}
													HAL_Delay(5);
													
											}
									}
									else { printf("c8fd");}
									break;
							case '9': // 1750
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{
										    OLED_CLS();//清屏
										    BH1750_Init();
										    char str[100] = {0};
												uint16_t lx = 0; 
												OLED_ShowStr(16,0,(uint8_t *)"BH1750 TEST",2);      //测试6*16字符
												lx =  BH1750_Read(); 
												printf("lx=%.0f\n",(float)lx/1.2);
												sprintf(str,"lx: %6.0f",lx/1.2);
												OLED_ShowStr(0,5,(uint8_t *)str,2);      //测试6*16字符
												HAL_Delay(130);  // 光线传感器adc转换的等待时间 
												HAL_Delay(1000);  //  延时1秒
									}
									else { printf("c9fd");}
									break;
             case 'A': // 大气压传感器
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{   
										OLED_CLS();//清屏
                    char buf[100]={0};
										OLED_ShowStr(16,0,(unsigned char *)"BMP180 Test",2);     //测试6*16字符
										BMP180_Convert();
										altitude=44330.0*(1-pow((float)result_UP/101325,1/5.255));
										printf("altitude=%f M\n",altitude);
										

										sprintf(buf,"altitude=%5.1fM",altitude);
										OLED_ShowStr(0,6,(unsigned char *)buf,2);     //测试6*16字符
										HAL_Delay(1000); // 延时1秒 
									}
									else { printf("cAfd");}
									break;
                  case 'B': //电位器
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{   
										  OLED_CLS();//清屏
                     	uint8_t str[100]={0};
											uint32_t adc_value=0;
											for(uint8_t i=0;i<10;i++)
											{
												HAL_ADC_Start(&hadc1);	//启动adc 采集
												if(HAL_ADC_PollForConversion(&hadc1,100) == HAL_OK)//等待dac 转换结束
												{
													adc_value += HAL_ADC_GetValue(&hadc1);
												}
												
											}
										
											adc_value=adc_value/10;//10次求平均 
											// 0   -----0  v 
											// 4095-----3.3v
											// val -----x  v 
											float val = (adc_value*3.3/4095); 
											printf("voltage is %.2fV\n",val);
											sprintf((char * )str,"VR: %.2fV",val);
											OLED_ShowStr(0,5,(unsigned char *)str,2);     //测试6*16字符		
									}
									else { printf("cBfd");}
									break;	
                  case 'C': // CPU温度
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{   
                      uint8_t str[100]={0};
											uint32_t adc_value = 0 ; 
											for(uint8_t i=0;i<10;i++)
											{
													HAL_ADC_Start(&hadc1); // 启动ADC , 启动一次工作一次 
													if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK )   //  等待adc 转换结束 
													{
															adc_value += HAL_ADC_GetValue(&hadc1) ; 
													}
											
											}
											adc_value = adc_value /10 ;  // 10次求平均 
																
											// 0   -----0  v 
											// 4095-----3.3v
											// val -----x  v 
										
											float val=(1.43-adc_value*3.3/4095)/0.0043+25;
											printf("temp is %.1f C\n",val);
											sprintf((char * )str,"temp: %.1f C",val);
											OLED_ShowStr(0,5,(unsigned char *)str,2);     //Ӣ˔6*16ؖػ

									}
									else { printf("cCfd");}
									break;
                  case 'D': //门禁卡
									Dequeue(UART_SQ,&RecvData) ;
									if( RecvData == 'b')
									{ 
                        unsigned char CT[2];//卡类型
												unsigned char SN[4]; //卡号
												unsigned char RFID[16];			//存放RFID 
												uint8_t KEY[6]={0xff,0xff,0xff,0xff,0xff,0xff};
												unsigned char s=0x08; 										
                        unsigned char status;

												char str[100]={0};
												uint32_t count =0;
												OLED_ShowStr(0+16,0,(unsigned char *)"RFID Test",2);				//测试8*16字符
												OLED_ShowStr(0,3,(unsigned char *)"rc522 read write",1);      //测试6*16字符
												while(count <5)
												{
													count ++;
													status = PcdRequest(PICC_REQALL,CT);/*尋卡*/
													//printf("1:PcdRequest:status=%d\n",status);
													if(status==MI_OK)//尋卡成功
													{
														status=MI_ERR;
														status = PcdAnticoll(SN);//防冲撞 , 得到卡的sn 
													}	
													if (status==MI_OK)//防衝撞成功
													{
														status=MI_ERR;		
														printf("ID:%02x %02x %02x %02x\n",SN[0],SN[1],SN[2],SN[3]);//发送卡号
														sprintf(str,(char *)"ID:%02x %02x %02x %02x ",SN[0],SN[1],SN[2],SN[3]);//发送卡号
														OLED_ShowStr(0,5,(unsigned char *)str,1);      //测试6*16字符
														status =PcdSelect(SN);
													}
													if(status==MI_OK)//选卡成功
													{
														//printf("PcdSelect_MI_OK  \n");
														status=MI_ERR;
														status =PcdAuthState(0x60,0x09,KEY,SN); // 认证卡的秘钥 
													}
													if(status==MI_OK)//驗證成功
													{
														//printf("PcdAuthState_MI_OK  \n");
														status=MI_ERR;
														status=PcdRead(s,RFID);
													}
													if(status==MI_OK)//讀卡成功
													{
														//printf("READ_MI_OK \n");
														status=MI_ERR;
													}
												} 

									}
									else { printf("cDfd");}
									break;	  																														
							default: break ; }
				  }
		}	
}



