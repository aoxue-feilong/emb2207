
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "esp8266.h"
#include "main.h"
#include "usart.h"
#include "oled.h"

int32_t ESP8266_OK_Flag = 0 ; 

int8_t ESP8266OK_Status(void)
{
			uint32_t Timeout = 10000 ;
			while(ESP8266_OK_Flag == RESET)  // 10秒的超时 
			{
				if((Timeout--) == 0) return RESET ;
				HAL_Delay(1);	
			}
			ESP8266_OK_Flag = 0 ;
			return SET; 
}

void ESP8266_Connect_WIFI(void)
{
			uint8_t buf[100]={0}; 
			OLED_CLS();//清屏
			OLED_ShowStr(0+16,0,(unsigned char *)"ESP8266 Test",2);				//测试8*16字符
			
			strcpy((char *)buf,"AT+CWJAP=\"CMCC-ShengLi\",\"drbder6t\"\r\n");
			HAL_UART_Transmit(&huart3,buf,strlen((char *)buf)+1,1000);
			OLED_ShowStr(0+16,2,(unsigned char *)"AT+CWJAP",2);				//测试8*16字符
			
			if(ESP8266OK_Status() == RESET)  return ;  // 等待OK返回 
			OLED_ShowStr(0+16,4,(unsigned char *)"OK",2);				//测试8*16字符
				
			strcpy((char *)buf,"AT+CIFSR\r\n"); //,  获取ip 信息
			HAL_UART_Transmit(&huart3,buf,strlen((char *)buf)+1,1000);
			OLED_ShowStr(0+16,2,(unsigned char *)"AT+CIFSR",2);				//测试8*16字符
			
			if(ESP8266OK_Status() == RESET)  return ; 
			OLED_ShowStr(0+16,4,(unsigned char *)"OK",2);				//测试8*16字符
			
 

			
}
void ESP8266_Connect_Server(void)
{
			uint8_t buf[100]={0};
			OLED_CLS();//清屏
			OLED_ShowStr(0+16,0,(unsigned char *)"ESP8266 Test",2);				//测试8*16字符			
			strcpy((char *)buf,"AT\r\n");
			HAL_UART_Transmit(&huart3,buf,strlen((char *)buf),1000);
			OLED_ShowStr(0+16,2,(unsigned char *)"AT",2);				//测试8*16字符
			if(ESP8266OK_Status() == RESET)  return ; 
			OLED_ShowStr(0+16,4,(unsigned char *)"OK",2);				//测试8*16字符
			
	
			strcpy((char *)buf,"AT+CIPMUX=0\r\n"); // 单链接 
			HAL_UART_Transmit(&huart3,buf,strlen((char *)buf),1000);
			OLED_ShowStr(0+16,2,(unsigned char *)"AT+CIPMUX",2);				//测试8*16字符
			if(ESP8266OK_Status() == RESET)  return ; 
			OLED_ShowStr(0+16,4,(unsigned char *)"OK",2);				//测试8*16字符
			
			
			strcpy((char *)buf,"AT+CIPMODE=1\r\n"); // 设置透传模式 
			HAL_UART_Transmit(&huart3,buf,strlen((char *)buf),1000);
			OLED_ShowStr(0+16,2,(unsigned char *)"AT+CIPMODE=1",2);				//测试8*16字符
			if(ESP8266OK_Status() == RESET)  return ; 
			OLED_ShowStr(0+16,4,(unsigned char *)"OK",2);				//测试8*16字符
			
			
			strcpy((char *)buf,"AT+CIPSTART=\"TCP\",\"192.168.1.103\",8000\r\n");
			HAL_UART_Transmit(&huart3,buf,strlen((char *)buf),1000);
			OLED_ShowStr(0+16,2,(unsigned char *)"AT+CIPSTART",2);				//测试8*16字符
			if(ESP8266OK_Status() == RESET)  return ; 
			OLED_ShowStr(0+16,4,(unsigned char *)"OK",2);				//测试8*16字符
			
			
			strcpy((char *)buf,"AT+CIPSEND\r\n");  // 发送数据 
			HAL_UART_Transmit(&huart3,buf,strlen((char *)buf),1000);
			OLED_ShowStr(0+16,2,(unsigned char *)"AT+CIPSEND",2);				//测试8*16字符
			OLED_ShowStr(0+16,4,(unsigned char *)"TouChuan...",2);				//测试8*16字符
			
}
void ESP8266_Disconnect(void)
{
			uint8_t buf[100]={0}; 
			strcpy((char *)buf,"+++");
			HAL_UART_Transmit(&huart3,buf,strlen((const char *)buf),1000);
			OLED_ShowStr(0+16,2,(unsigned char *)"+++         ",2);				//测试8*16字符
			//if(ESP8266OK_Status() == RESET)  return ; 
			OLED_ShowStr(0+16,4,(unsigned char *)"Exit TouChuan",2);				//测试8*16字符
			printf("Exit TouChuan\n");
}

void ESP8266_Test(void)
{

			if(Key_Value == KEY_OK)  // OK 按键 连接服务器
			{
					Key_Value = 0 ; 
					printf("KEY OK Pressed\n");
					ESP8266_Connect_Server();
			}
			else if(Key_Value == KEY_UP) // UP按键 连接wifi 
			{
					Key_Value = 0 ; 
					printf("KEY UP Pressed\n");
					ESP8266_Connect_WIFI();
			}
			else if(Key_Value == KEY_ESC) // ESC按键 退出透传 
			{
					Key_Value = 0 ; 
					printf("KEY ESC Pressed\n");
					ESP8266_Disconnect();
			}
			
}
