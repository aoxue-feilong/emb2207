#ifndef _CONTROL_H
#define _CONTROL_H

#include "queue.h"




extern sequeue_t *UART1_SQ;
extern sequeue_t *UART3_SQ;
void STM32_Control_Init(void);
void STM32_Control(sequeue_t *UART_SQ);
#endif
