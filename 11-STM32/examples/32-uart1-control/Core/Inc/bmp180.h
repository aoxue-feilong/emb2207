#ifndef __BMP180_H
#define __BMP180_H


#define   OSS   0	 //	BMP085使用
#define   BMP180_ADDR   0xee 

extern double  result_UT;  // 温度 
extern double  result_UP;  // 大气压 
extern double  altitude;	    // 海拔 

void BMP180_Init(void);
void BMP180_Convert(void);
void BMP180_Test(void);


#endif /* __BMP180_H */
