#ifndef __DHT11_H__
#define __DHT11_H__

#include <stdint.h>

//#define DHT11_OUT_H GPIO_SetBits(DHT11_GPIO_TYPE, DHT11_GPIO_PIN)
//#define DHT11_OUT_L GPIO_ResetBits(DHT11_GPIO_TYPE, DHT11_GPIO_PIN)
//#define DHT11_IN    GPIO_ReadInputDataBit(DHT11_GPIO_TYPE, DHT11_GPIO_PIN)

#define DHT11_OUT_H HAL_GPIO_WritePin(DHT11_GPIO_Port, DHT11_Pin, GPIO_PIN_SET)
#define DHT11_OUT_L HAL_GPIO_WritePin(DHT11_GPIO_Port, DHT11_Pin, GPIO_PIN_RESET)
#define DHT11_IN    HAL_GPIO_ReadPin(DHT11_GPIO_Port,DHT11_Pin)

void DHT11_GPIO_Input(void);
void DHT11_GPIO_Output(void);
uint16_t DHT11_Scan(void);
uint16_t DHT11_Read_Bit(void);
uint16_t DHT11_Read_Byte(void);
uint16_t DHT11_Read_Data(uint8_t buffer[5]);
void DHT11_Test(void);

#endif
