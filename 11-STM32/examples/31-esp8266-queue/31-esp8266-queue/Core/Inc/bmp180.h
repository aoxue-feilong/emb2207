#ifndef __BMP180_H
#define __BMP180_H


#define   OSS   0	 //	BMP085使用
#define   BMP180_ADDR   0xee 

void BMP180_Init(void);
void BMP180_Convert(void);
void BMP180_Test(void);


#endif /* __BMP180_H */
