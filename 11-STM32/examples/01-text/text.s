STACK_TOP EQU 0X20002000
  AREA    RESET, CODE,READONLY ,ALIGN=2   
  DCD   STACK_TOP              
  DCD   Reset_Handler                 
array
  DCD   0X12345678

      
      
 ENTRY                      
; Reset handler
Reset_Handler    PROC
                 EXPORT  Reset_Handler             [WEAK] 

     add  r1,r2,r3   ;  r1 = r2 + r3
     sub  r5,r4,r3   ;  r5 = r4 - r3 

     
STOP    B  STOP  ; while(1)    
                 ENDP
              
 END