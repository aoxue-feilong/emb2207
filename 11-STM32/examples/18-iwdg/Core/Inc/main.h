/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DHT11_Pin GPIO_PIN_4
#define DHT11_GPIO_Port GPIOA
#define KEY_OK_Pin GPIO_PIN_12
#define KEY_OK_GPIO_Port GPIOB
#define KEY_OK_EXTI_IRQn EXTI15_10_IRQn
#define KEY_UP_Pin GPIO_PIN_13
#define KEY_UP_GPIO_Port GPIOB
#define KEY_UP_EXTI_IRQn EXTI15_10_IRQn
#define KEY_DOWN_Pin GPIO_PIN_14
#define KEY_DOWN_GPIO_Port GPIOB
#define KEY_DOWN_EXTI_IRQn EXTI15_10_IRQn
#define KEY_RIGHT_Pin GPIO_PIN_15
#define KEY_RIGHT_GPIO_Port GPIOB
#define KEY_RIGHT_EXTI_IRQn EXTI15_10_IRQn
#define KEY_ESC_Pin GPIO_PIN_8
#define KEY_ESC_GPIO_Port GPIOA
#define KEY_ESC_EXTI_IRQn EXTI9_5_IRQn
#define TXD1_Pin GPIO_PIN_9
#define TXD1_GPIO_Port GPIOA
#define RXD1_Pin GPIO_PIN_10
#define RXD1_GPIO_Port GPIOA
#define KEY_LEFT_Pin GPIO_PIN_5
#define KEY_LEFT_GPIO_Port GPIOB
#define KEY_LEFT_EXTI_IRQn EXTI9_5_IRQn

/* USER CODE BEGIN Private defines */
#define KEY_UP    1
#define KEY_DOWN  2
#define KEY_LEFT  3
#define KEY_RIGHT 4
#define KEY_OK    5
#define KEY_ESC   6
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
