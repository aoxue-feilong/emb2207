/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "control.h"
#include "esp8266.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId myTaskUart1Handle;
osThreadId myTaskEsp8266Handle;
osThreadId myTaskUart3Handle;
osMutexId myMutex1Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartTaskUart1(void const * argument);
void StartTaskEsp8266(void const * argument);
void StartTaskUart3(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* definition and creation of myMutex1 */
  osMutexDef(myMutex1);
  myMutex1Handle = osMutexCreate(osMutex(myMutex1));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTaskUart1 */
  osThreadDef(myTaskUart1, StartTaskUart1, osPriorityHigh, 0, 128);
  myTaskUart1Handle = osThreadCreate(osThread(myTaskUart1), NULL);

  /* definition and creation of myTaskEsp8266 */
  osThreadDef(myTaskEsp8266, StartTaskEsp8266, osPriorityHigh, 0, 128);
  myTaskEsp8266Handle = osThreadCreate(osThread(myTaskEsp8266), NULL);

  /* definition and creation of myTaskUart3 */
  osThreadDef(myTaskUart3, StartTaskUart3, osPriorityHigh, 0, 128);
  myTaskUart3Handle = osThreadCreate(osThread(myTaskUart3), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(500);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTaskUart1 */
/**
* @brief Function implementing the myTaskUart1 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskUart1 */
void StartTaskUart1(void const * argument)
{
  /* USER CODE BEGIN StartTaskUart1 */
  /* Infinite loop */
	printf("Uart1 Thread is running\n");
  for(;;)
  {
		osMutexWait (myMutex1Handle, osWaitForever) ;  // 获取锁 ,获取后, 锁的状态为上锁状态  
		STM32_Control(QUART1,&huart1);
		osMutexRelease (myMutex1Handle) ; // 释放锁 , 释放房间1, 房间1门开 
    osDelay(100);
		
  }
  /* USER CODE END StartTaskUart1 */
}

/* USER CODE BEGIN Header_StartTaskEsp8266 */
/**
* @brief Function implementing the myTaskEsp8266 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskEsp8266 */
void StartTaskEsp8266(void const * argument)
{
  /* USER CODE BEGIN StartTaskEsp8266 */
  /* Infinite loop */
	printf("ESP8266 Thread is running\n");
  for(;;)
  {
		osMutexWait (myMutex1Handle, osWaitForever) ;  // 获取锁 ,获取后, 锁的状态为上锁状态  
		ESP8266_Test();
		osMutexRelease (myMutex1Handle) ; // 释放锁 , 释放房间1, 房间1门开 
 
    osDelay(100);
  }
  /* USER CODE END StartTaskEsp8266 */
}

/* USER CODE BEGIN Header_StartTaskUart3 */
/**
* @brief Function implementing the myTaskUart3 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskUart3 */
void StartTaskUart3(void const * argument)
{
  /* USER CODE BEGIN StartTaskUart3 */
  /* Infinite loop */
	printf("Uart3 Thread is running\n");
  for(;;)
  {

		osMutexWait (myMutex1Handle, osWaitForever) ;  // 获取锁 ,获取后, 锁的状态为上锁状态  
		STM32_Control(QUART3,&huart3);
		osMutexRelease (myMutex1Handle) ; // 释放锁 , 释放房间1, 房间1门开 
    osDelay(100);
  }
  /* USER CODE END StartTaskUart3 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

