/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    adc.c
  * @brief   This file provides code for the configuration
  *          of the ADC instances.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "adc.h"

/* USER CODE BEGIN 0 */
#include "oled.h"
#include <stdio.h>
/* USER CODE END 0 */

ADC_HandleTypeDef hadc1;

/* ADC1 init function */
void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

void HAL_ADC_MspInit(ADC_HandleTypeDef* adcHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspInit 0 */

  /* USER CODE END ADC1_MspInit 0 */
    /* ADC1 clock enable */
    __HAL_RCC_ADC1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**ADC1 GPIO Configuration
    PA0-WKUP     ------> ADC1_IN0
    PA1     ------> ADC1_IN1
    */
    GPIO_InitStruct.Pin = ADC_BAT_Pin|ADC_VR_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN ADC1_MspInit 1 */

  /* USER CODE END ADC1_MspInit 1 */
  }
}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* adcHandle)
{

  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspDeInit 0 */

  /* USER CODE END ADC1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_ADC1_CLK_DISABLE();

    /**ADC1 GPIO Configuration
    PA0-WKUP     ------> ADC1_IN0
    PA1     ------> ADC1_IN1
    */
    HAL_GPIO_DeInit(GPIOA, ADC_BAT_Pin|ADC_VR_Pin);

  /* USER CODE BEGIN ADC1_MspDeInit 1 */

  /* USER CODE END ADC1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
void ADC_VR_Test(void)
{
		uint8_t str[100]={0};
		uint32_t adc_value = 0 ; 
		for(uint8_t i=0;i<10;i++)
		{
				HAL_ADC_Start(&hadc1); // 启动ADC , 启动一次工作一次 
				if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK )   //  等待adc 转换结束 
				{
						adc_value += HAL_ADC_GetValue(&hadc1) ; 
				}
		
		}
		adc_value = adc_value /10 ;  // 10次求平均 
							
		// 0   -----0  v 
		// 4095-----3.3v
		// val -----x  v 
		float val = (adc_value*3.3/4095); 
		printf("voltage is %.2fV\n",val);
		sprintf((char * )str,"VR: %.2fV",val);
		OLED_ShowStr(0,5,(unsigned char *)str,2);     //测试6*16字符	


}

void ADC_BAT_Test(void)
{
		uint8_t str[100]={0};
		uint32_t adc_value = 0 ; 
		for(uint8_t i=0;i<10;i++)
		{
				HAL_ADC_Start(&hadc1); // 启动ADC , 启动一次工作一次 
				if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK )   //  等待adc 转换结束 
				{
						adc_value += HAL_ADC_GetValue(&hadc1) ; 
				}
		
		}
		adc_value = adc_value /10 ;  // 10次求平均 
							
		// 0   -----0  v 
		// 4095-----3.3v
		// val -----x  v 
		float val = (adc_value*3.3/4095); 
		printf("voltage is %.2fV\n",val*2); // 锂电池电压范围 3.6-4.2 之间 
		sprintf((char * )str,"BAT: %.2fV",val*2);
		OLED_ShowStr(0,5,(unsigned char *)str,2);     //测试6*16字符	


}

void ADC_CPU_Test(void)
{
		uint8_t str[100]={0};
		uint32_t adc_value = 0 ; 
		for(uint8_t i=0;i<10;i++)
		{
				HAL_ADC_Start(&hadc1); // 启动ADC , 启动一次工作一次 
				if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK )   //  等待adc 转换结束 
				{
						adc_value += HAL_ADC_GetValue(&hadc1) ; 
				}
		
		}
		adc_value = adc_value /10 ;  // 10次求平均 
							
		// 0   -----0  v 
		// 4095-----3.3v
		// val -----x  v 
	
		float val=(1.43-adc_value*3.3/4095)/0.0043+25;
		printf("temp is %.1f C\n",val);
		sprintf((char * )str,"temp: %.1f C",val);
		OLED_ShowStr(0,5,(unsigned char *)str,2);     //Ӣ˔6*16ؖػ

}
/* USER CODE END 1 */
