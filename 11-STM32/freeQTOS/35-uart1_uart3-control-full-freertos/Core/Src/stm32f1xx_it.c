/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include "squeue.h"
#include "esp8266.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */
uint8_t Key_Value=0 ; // 是按键的键值
/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern CAN_HandleTypeDef hcan;
extern DMA_HandleTypeDef hdma_usart1_tx;
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart3_rx;
extern DMA_HandleTypeDef hdma_usart3_tx;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern TIM_HandleTypeDef htim1;

/* USER CODE BEGIN EV */
void USER_UART_IRQHandler(UART_HandleTypeDef *huart);
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */
	printf("NMI_Handler error\n");
  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */
	printf("HardFault_Handler error\n");
  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */
	printf("MemManage_Handler error\n");
  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */
	printf("BusFault_Handler error\n");
  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */
	printf("UsageFault_Handler error\n");
  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */
	
  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles DMA1 channel2 global interrupt.
  */
void DMA1_Channel2_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel2_IRQn 0 */

  /* USER CODE END DMA1_Channel2_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart3_tx);
  /* USER CODE BEGIN DMA1_Channel2_IRQn 1 */

  /* USER CODE END DMA1_Channel2_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel3 global interrupt.
  */
void DMA1_Channel3_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel3_IRQn 0 */

  /* USER CODE END DMA1_Channel3_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart3_rx);
  /* USER CODE BEGIN DMA1_Channel3_IRQn 1 */

  /* USER CODE END DMA1_Channel3_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel4 global interrupt.
  */
void DMA1_Channel4_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel4_IRQn 0 */

  /* USER CODE END DMA1_Channel4_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart1_tx);
  /* USER CODE BEGIN DMA1_Channel4_IRQn 1 */

  /* USER CODE END DMA1_Channel4_IRQn 1 */
}

/**
  * @brief This function handles DMA1 channel5 global interrupt.
  */
void DMA1_Channel5_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel5_IRQn 0 */

  /* USER CODE END DMA1_Channel5_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart1_rx);
  /* USER CODE BEGIN DMA1_Channel5_IRQn 1 */

  /* USER CODE END DMA1_Channel5_IRQn 1 */
}

/**
  * @brief This function handles USB low priority or CAN RX0 interrupts.
  */
void USB_LP_CAN1_RX0_IRQHandler(void)
{
  /* USER CODE BEGIN USB_LP_CAN1_RX0_IRQn 0 */

  /* USER CODE END USB_LP_CAN1_RX0_IRQn 0 */
  HAL_CAN_IRQHandler(&hcan);
  /* USER CODE BEGIN USB_LP_CAN1_RX0_IRQn 1 */

  /* USER CODE END USB_LP_CAN1_RX0_IRQn 1 */
}

/**
  * @brief This function handles EXTI line[9:5] interrupts.
  */
void EXTI9_5_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI9_5_IRQn 0 */

  /* USER CODE END EXTI9_5_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(KEY_LEFT_Pin);
  HAL_GPIO_EXTI_IRQHandler(KEY_ESC_Pin);
  /* USER CODE BEGIN EXTI9_5_IRQn 1 */

  /* USER CODE END EXTI9_5_IRQn 1 */
}

/**
  * @brief This function handles TIM1 update interrupt.
  */
void TIM1_UP_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_IRQn 0 */

  /* USER CODE END TIM1_UP_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_UP_IRQn 1 */

  /* USER CODE END TIM1_UP_IRQn 1 */
}

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */
//	uint32_t tmpFlag = 0;
//	uint32_t temp;
//	tmpFlag =__HAL_UART_GET_FLAG(&huart1,UART_FLAG_IDLE); //获取IDLE标志位
//	if((tmpFlag != RESET)) //idle标志被置位 , 条件为真, 表示置位 
//	{ 
//		__HAL_UART_CLEAR_IDLEFLAG(&huart1);//清除标志位
//		
//		HAL_UART_DMAStop(&huart1); // 停止DMA
//		temp  =  __HAL_DMA_GET_COUNTER(&hdma_usart1_rx);// 获取DMA中未传输的数据个数   
//		RX1_Length  =  BUFFER_SIZE - temp; //总计数减去未传输的数据个数，得到已经接收的数据个数
//		RX1_EndFlag  = 1;	// 接受完成标志位置1	
//		HAL_UART_Transmit_DMA(&huart3, RX1_Buf, RX1_Length); //  把收到的数据进行转发 
//		RX1_Length = 0;//清除计数
//		RX1_EndFlag = 0;//清除接收结束标志位

//		memset(RX1_Buf,0,RX1_Length);
//		HAL_UART_Receive_DMA(&huart1, RX1_Buf, BUFFER_SIZE);//重新打开DMA接收，不然只能接收一次数据
//	 }
  /* USER CODE END USART1_IRQn 0 */
  HAL_UART_IRQHandler(&huart1);
  /* USER CODE BEGIN USART1_IRQn 1 */
	USER_UART_IRQHandler(&huart1);
  /* USER CODE END USART1_IRQn 1 */
}

/**
  * @brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */

  /* USER CODE END USART2_IRQn 0 */
  HAL_UART_IRQHandler(&huart2);
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}

/**
  * @brief This function handles USART3 global interrupt.
  */
void USART3_IRQHandler(void)
{
  /* USER CODE BEGIN USART3_IRQn 0 */

  /* USER CODE END USART3_IRQn 0 */
  HAL_UART_IRQHandler(&huart3);
  /* USER CODE BEGIN USART3_IRQn 1 */
	USER_UART_IRQHandler(&huart3);
  /* USER CODE END USART3_IRQn 1 */
}

/**
  * @brief This function handles EXTI line[15:10] interrupts.
  */
void EXTI15_10_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI15_10_IRQn 0 */

  /* USER CODE END EXTI15_10_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(KEY_OK_Pin);
  HAL_GPIO_EXTI_IRQHandler(KEY_UP_Pin);
  HAL_GPIO_EXTI_IRQHandler(KEY_DOWN_Pin);
  HAL_GPIO_EXTI_IRQHandler(KEY_RIGHT_Pin);
  /* USER CODE BEGIN EXTI15_10_IRQn 1 */

  /* USER CODE END EXTI15_10_IRQn 1 */
}

/* USER CODE BEGIN 1 */


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
		
		// PB13  按键的UP 
		if(GPIO_Pin == KEY_UP_Pin) // 表示 Up 按键按下后触发的中断 
		{
				// led1  亮
				//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);	
				Key_Value= KEY_UP;
		}
		
		// PB14  按键的Down
		if( GPIO_Pin == KEY_DOWN_Pin ) // 表示 Down 按键按下后触发的中断 
		{
				// led1 灭
				//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
				Key_Value= KEY_DOWN;			
		}
		
		// PB5  按键的LEFT
		if( GPIO_Pin == KEY_LEFT_Pin ) // 表示 LEFT 按键按下后触发的中断 
		{
				// led2 亮
				//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);	
				Key_Value= KEY_LEFT;			
		}
		// PB15  按键的RIGHT
		if( GPIO_Pin == KEY_RIGHT_Pin ) // 表示 RIGHT 按键按下后触发的中断 
		{
				// led2 灭
				//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);		
				Key_Value= KEY_RIGHT;			
		}
		
		// PB12  按键的OK
		if( GPIO_Pin == KEY_OK_Pin ) // 表示 OK 按键按下后触发的中断 
		{
				// beep 响
				//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);	
				Key_Value= KEY_OK;			
		}
		// PA8  按键的ESC
		if( GPIO_Pin == KEY_ESC_Pin ) // 表示 ESC 按键按下后触发的中断 
		{
				// beep 灭
				//HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);		
				Key_Value= KEY_ESC;			
		}
		
				
	
}
// 50ms 执行一次中断函数 
//void HAL_WWDG_EarlyWakeupCallback(WWDG_HandleTypeDef *hwwdg)
//{
//		static  uint32_t  wwdg_count = 0 ; 
//		wwdg_count ++; // 50ms 加1 
//		// 获取按键 OK , 如果OK 按键按下就喂狗 , 否则不喂狗, 让系统复位 
//		if(Key_Value == KEY_OK) 
//		{
//				Key_Value = 0 ; // 键值清空为0 
//				wwdg_count = 0 ; // 会让定时器可以再多工作2秒 
////				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET); // beep 响
////				HAL_Delay(2);
////				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET); // beep 不响
//		}
//		
//		if(wwdg_count <=40)  // 2秒 
//		{
//				//  中断处理函数中喂狗
//				HAL_WWDG_Refresh(hwwdg) ; // 喂狗指令  
//		}
//		else if(wwdg_count >40) // 2秒后 , 不喂狗 就会复位
//		{
//				
//			
//		}

//}

uint8_t RX_Message[8] = {0};
CAN_RxHeaderTypeDef RXHeader;
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	if(hcan->Instance == CAN1)
	{
			HAL_CAN_GetRxMessage(hcan,CAN_RX_FIFO0,&RXHeader,RX_Message); 
			for(uint8_t i =0 ; i< 8;i++)
			{
					printf("RX_Message[%d]:%d\n",i,RX_Message[i]);
			}
	}
}


void USER_UART_IRQHandler(UART_HandleTypeDef *huart)
{

		if(huart->Instance==USART1)
		{
				if((__HAL_UART_GET_FLAG(&huart1,UART_FLAG_IDLE) != RESET)) //idle标志被置位 , 条件为真, 表示置位 
				{ 
						__HAL_UART_CLEAR_IDLEFLAG(&huart1);//清除标志位
						
						uint32_t Length  =  BUFFER_SIZE - __HAL_DMA_GET_COUNTER(&hdma_usart1_rx) - RX1_Offset; //总计数减去未传输的数据个数，得到已经接收的数据个数
						//printf("DLength=%d\n",Length);
						if(Enqueue_Bytes(QUART1,RX1_Buf+RX1_Offset,Length) < 0 ) //  入队列  
						{
							//printf("queue is full\n"); 
						}
						RX1_Offset += Length; 
				}
		}
		else if(huart->Instance==USART3)
		{
				if((__HAL_UART_GET_FLAG(&huart3,UART_FLAG_IDLE) != RESET)) //idle标志被置位 , 条件为真, 表示置位 
				{ 
						__HAL_UART_CLEAR_IDLEFLAG(&huart3);//清除标志位
						
						uint32_t Length  =  BUFFER_SIZE - __HAL_DMA_GET_COUNTER(&hdma_usart3_rx) - RX3_Offset; //总计数减去未传输的数据个数，得到已经接收的数据个数
						//printf("DLength=%d\n",Length);
						if(Enqueue_Bytes(QUART3,RX3_Buf+RX3_Offset,Length) < 0 ) //  入队列  
						{
							//printf("queue is full\n"); 
						}
						RX3_Offset += Length; 
				}
		}

}

// DMA 接收到一半的中断 
void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
		if(huart->Instance==USART1)
		{
				uint32_t Length  =  BUFFER_SIZE/2 - RX1_Offset ; 
				//printf("HLength=%d\n",Length);
				if(Enqueue_Bytes(QUART1,RX1_Buf+RX1_Offset,Length) < 0 ) // 入队一半字节 
				{
					//printf("queue is full\n"); 
				}
				RX1_Offset += Length; 
				//printf("HLength=%d\n",RX3_Offset);
				
		}
		else	if(huart->Instance==USART3)
		{
				uint32_t Length  =  BUFFER_SIZE/2 - RX3_Offset ; 
				//printf("HLength=%d\n",Length);
				if(Enqueue_Bytes(QUART3,RX3_Buf+RX3_Offset,Length) < 0 ) // 入队一半字节 
				{
					//printf("queue is full\n"); 
				}
				RX3_Offset += Length; 
				//printf("HLength=%d\n",RX3_Offset);
				
		}

}

// DMA传输完成中断 
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
		if(huart->Instance==USART1)
		{
			uint32_t Length  =  BUFFER_SIZE - RX1_Offset ; 
			//printf("CLength=%d\n",Length);
			if(Enqueue_Bytes(QUART1,RX1_Buf+RX1_Offset,Length) < 0 )
			{
				//printf("queue is full\n"); 
			}

			RX1_Offset = 0 ; // 清空dma 位置基准值 

		}
		else if(huart->Instance==USART2)
		{

		}
		else if(huart->Instance==USART3)
		{
			uint32_t Length  =  BUFFER_SIZE - RX3_Offset ; 
			//printf("CLength=%d\n",Length);
			if(Enqueue_Bytes(QUART3,RX3_Buf+RX3_Offset,Length) < 0 )
			{
				//printf("queue is full\n"); 
			}

			RX3_Offset = 0 ; // 清空dma 位置基准值 
		}

}
/* USER CODE END 1 */
