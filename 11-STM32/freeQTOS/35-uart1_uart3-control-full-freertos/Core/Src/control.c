#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "control.h"
#include "bh1750.h"
#include "squeue.h"
#include "main.h"
#include "dht11.h"
#include "adc.h"
#include "bmp180.h"
#include "rc522.h"
#include "cmsis_os.h"

void STM32_Control_Init(void)
{
		QUART1	= Create_Empty_Queue();	 // usb转串口 
		QUART3	= Create_Empty_Queue();	 // wifi 模块 
}

void STM32_Control(sequeue_t *UART_SQ,UART_HandleTypeDef *huart)
{
			
			uint8_t RecvData =0;
			uint8_t retBuf[50]={0};
			while( !Queue_Is_Empty(UART_SQ) )
			{ // 队列内有数据时, 执行if 语句 
				 Dequeue(UART_SQ,&RecvData) ;
				 if(RecvData == 'a'){
					  if(Dequeue(UART_SQ,&RecvData) <0) return ; 
						switch(RecvData){
							case '1':
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b'){
										//	UART1_Data_Ready_Flag = 0;
										//	led_on(1);
											HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET); // LED1 亮 
											strcpy((char *)retBuf,"c1sd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据 
									}
									else {
											strcpy((char *)retBuf,"c1fd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据
									}
									break ;
							case '2':
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											//UART1_Data_Ready_Flag = 0;
											//led_off(1); //
											HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET); // LED1 灭 										
											//printf("c2sd");	
											strcpy((char *)retBuf,"c2sd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据										
									}
									else { 
											//printf("c2fd");
											strcpy((char *)retBuf,"c2fd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据
									}
									break ;
							case '3':
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											//UART1_Data_Ready_Flag = 0;
											//led_on(2);	 // 
											HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET); // LED2 亮 
                      //printf("c3sd");	
											strcpy((char *)retBuf,"c3sd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据										
									}
									else { 
											//printf("c3fd");
											strcpy((char *)retBuf,"c3fd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									break ; 
							case '4':
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											//UART1_Data_Ready_Flag = 0;
											//led_off(2);	 //	 
											HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET); // LED2 灭 		
                      //printf("c4sd");	
											strcpy((char *)retBuf,"c4sd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
										
									}
									else { 
											//printf("c4fd");
											strcpy((char *)retBuf,"c4fd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据
									}
									break;
							case '5':
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											//beep_on();	 //	 
											HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);	 // beep 响 
                      //printf("c5sd");	
											strcpy((char *)retBuf,"c5sd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据										
									}
									else { 
											//printf("c5fd");
											strcpy((char *)retBuf,"c5fd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
										
									}
									break;
						  case '6':
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											//beep_off();	 //	 
											HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);	 // beep 灭
                      //printf("c6sd");	
											strcpy((char *)retBuf,"c6sd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据										
									}
									else { 
											//printf("c6fd");
											strcpy((char *)retBuf,"c6fd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据											
									}
									break;
							case '7':  // 锂电池电压 
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											uint32_t adc_value = 0 ; 
											ADC_ChannelConfTypeDef sConfig = {0};
											/** Configure Regular Channel
											*/
											sConfig.Channel = ADC_CHANNEL_0;
											sConfig.Rank = ADC_REGULAR_RANK_1;
											sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
											if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
											{
												Error_Handler();
											}
											for(uint8_t i=0;i<10;i++)
											{
													HAL_ADC_Start(&hadc1); // 启动ADC , 启动一次工作一次 
													if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK )   //  等待adc 转换结束 
													{
															adc_value += HAL_ADC_GetValue(&hadc1) ; 
													}
											
											}
											adc_value = adc_value /10 ;  // 10次求平均 
																
											// 0   -----0  v 
											// 4095-----3.3v
											// val -----x  v 
											float val = (adc_value*3.3/4095); 
											sprintf((char * )retBuf,"cA%.2fd",val*2);
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									else { 
											sprintf((char *)retBuf,"cAfd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									break;
							case '8': // dht11 
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											uint8_t buffer[5]={0};
											float temp,hum;
											for(int i=0;i<10;i++)
											{
													if(DHT11_Read_Data(buffer) == SET)
													{ 
														hum  = buffer[0] + buffer[1] / 10.0;
														temp = buffer[2] + buffer[3] / 10.0;
														sprintf((char *)retBuf,"cB%.1fd",temp);
														HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
														sprintf((char *)retBuf,"cC%.1fd",hum);	
														HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
														break ;
													}
													osDelay(100);
													
											}
											osDelay(100);// dht11 连续采样不小于100ms 

								
									}
									else { 
											sprintf((char *)retBuf,"cBfd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
											sprintf((char *)retBuf,"cCfd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									break;
							case '9': // 1750
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
												uint16_t lx = 0; 
												lx =  BH1750_Read(); 
												sprintf((char *)retBuf,"cD%.0fd",lx/1.2);
												HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
												osDelay(130);  // 光线传感器adc转换的等待时间 
									}
									else { 
											sprintf((char *)retBuf,"cDfd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									break;	 
							case 'A': // bmp180
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
												BMP180_Convert();												
												sprintf((char *)retBuf,"cE%.1fd",result_UP/1000); // result_UP 是全局变量 
												HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
												osDelay(10); 

									}
									else { 
											sprintf((char *)retBuf,"cEfd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}	
									break;	
							case 'B': // 电位器 
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											uint32_t adc_value = 0 ; 
											ADC_ChannelConfTypeDef sConfig = {0};
											/** Configure Regular Channel
											*/
											sConfig.Channel = ADC_CHANNEL_1; // vr 电位器 
											sConfig.Rank = ADC_REGULAR_RANK_1;
											sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
											if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
											{
												Error_Handler();
											}
											for(uint8_t i=0;i<10;i++)
											{
													HAL_ADC_Start(&hadc1); // 启动ADC , 启动一次工作一次 
													if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK )   //  等待adc 转换结束 
													{
															adc_value += HAL_ADC_GetValue(&hadc1) ; 
													}
											
											}
											adc_value = adc_value /10 ;  // 10次求平均 
																
											// 0   -----0  v 
											// 4095-----3.3v
											// val -----x  v 
											float val = (adc_value*3.3/4095); 
											sprintf((char * )retBuf,"cF%.2fd",val);
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									else { 
											sprintf((char *)retBuf,"cFfd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									break;	
							case 'C': // cpu 温度 
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
											uint32_t adc_value = 0 ; 
											ADC_ChannelConfTypeDef sConfig = {0};
											/** Configure Regular Channel
											*/
											sConfig.Channel = ADC_CHANNEL_TEMPSENSOR; // 内部cpu温度 
											sConfig.Rank = ADC_REGULAR_RANK_1;
											sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
											if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
											{
												Error_Handler();
											}
											for(uint8_t i=0;i<10;i++)
											{
													HAL_ADC_Start(&hadc1); // 启动ADC , 启动一次工作一次 
													if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK )   //  等待adc 转换结束 
													{
															adc_value += HAL_ADC_GetValue(&hadc1) ; 
													}
											
											}
											adc_value = adc_value /10 ;  // 10次求平均 
																
											// 0   -----0  v 
											// 4095-----3.3v
											// val -----x  v 
											float val=(1.43-adc_value*3.3/4095)/0.0043+25;
											sprintf((char * )retBuf,"cG%.1fd",val);
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									else { 
											sprintf((char *)retBuf,"cGfd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}
									break;
							case 'D': // RFID
									if(Dequeue(UART_SQ,&RecvData) <0) return ; 
									if( RecvData == 'b')
									{
												unsigned char status;
												unsigned char CT[2];//卡类型
												unsigned char SN[4]; //卡号
												uint32_t count =0;
												while(count <5)
												{
														count ++;
														status = PcdRequest(PICC_REQALL,CT);/*尋卡*/
														if(status==MI_OK)//尋卡成功
														{
															status=MI_ERR;
															status = PcdAnticoll(SN);//防冲撞 , 得到卡的sn 
														}	
														if (status==MI_OK)//防衝撞成功
														{
															status=MI_ERR;		
															sprintf((char * )retBuf,(char *)"cH%02X%02X%02X%02Xd",SN[0],SN[1],SN[2],SN[3]);//发送卡号
															HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
															break; 
														}
															
												}
									}
									else { 
											sprintf((char *)retBuf,"cHfd");
											HAL_UART_Transmit(huart, retBuf, strlen((char *)retBuf), 0xffff); // 发送一个字节的数据	
									}	
									break;
									
							default: break ; }
				  }
		}	
}



