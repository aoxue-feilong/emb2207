/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include "stm32f1xx_it.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId myTaskLED1Handle;
osThreadId myTaskLED2Handle;
osThreadId myTaskBEEPHandle;
osMessageQId testQueueHandle;
osTimerId myTimerHandle;
osSemaphoreId myBinarySem1Handle;
osSemaphoreId myBinarySem2Handle;
osSemaphoreId myBinarySem3Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartTaskLED1(void const * argument);
void StartTaskLED2(void const * argument);
void StartTaskBEEP(void const * argument);
void timerCallback(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* GetTimerTaskMemory prototype (linked to static allocation support) */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/* USER CODE BEGIN GET_TIMER_TASK_MEMORY */
static StaticTask_t xTimerTaskTCBBuffer;
static StackType_t xTimerStack[configTIMER_TASK_STACK_DEPTH];

void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize )
{
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCBBuffer;
  *ppxTimerTaskStackBuffer = &xTimerStack[0];
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
  /* place for user code */
}
/* USER CODE END GET_TIMER_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of myBinarySem1 */
  osSemaphoreDef(myBinarySem1);
  myBinarySem1Handle = osSemaphoreCreate(osSemaphore(myBinarySem1), 1);

  /* definition and creation of myBinarySem2 */
  osSemaphoreDef(myBinarySem2);
  myBinarySem2Handle = osSemaphoreCreate(osSemaphore(myBinarySem2), 1);

  /* definition and creation of myBinarySem3 */
  osSemaphoreDef(myBinarySem3);
  myBinarySem3Handle = osSemaphoreCreate(osSemaphore(myBinarySem3), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
	//把信号量的值设置  为
	//myBinarySem1 =1;
	//myBinarySem2 =0;
	//myBinarySem3 =0;
	
	//portMax_DELAY 最大超时时间，则任务一直阻塞在该信号量上（即没有超时时间）
	//信号量值为0
	osSemaphoreWait(myBinarySem2Handle,portMAX_DELAY);
	//portMax_DELAY 最大超时时间，则任务一直阻塞在该信号量上（即没有超时时间）
	//信号量值为0
	osSemaphoreWait(myBinarySem3Handle,portMAX_DELAY);
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of myTimer */
  osTimerDef(myTimer, timerCallback);
  myTimerHandle = osTimerCreate(osTimer(myTimer), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
	osTimerStart(myTimerHandle,1000);//启动定时器,1s一次
	//创建线程必须在osKernelStart();创建
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of testQueue */
  osMessageQDef(testQueue, 16, uint32_t);
  testQueueHandle = osMessageCreate(osMessageQ(testQueue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTaskLED1 */
  osThreadDef(myTaskLED1, StartTaskLED1, osPriorityNormal, 0, 128);
  myTaskLED1Handle = osThreadCreate(osThread(myTaskLED1), NULL);

  /* definition and creation of myTaskLED2 */
  osThreadDef(myTaskLED2, StartTaskLED2, osPriorityNormal, 0, 128);
  myTaskLED2Handle = osThreadCreate(osThread(myTaskLED2), NULL);

  /* definition and creation of myTaskBEEP */
  osThreadDef(myTaskBEEP, StartTaskBEEP, osPriorityNormal, 0, 128);
  myTaskBEEPHandle = osThreadCreate(osThread(myTaskBEEP), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTaskLED1 */
/**
* @brief Function implementing the myTaskLED1 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskLED1 */
void StartTaskLED1(void const * argument)
{
  /* USER CODE BEGIN StartTaskLED1 */
  /* Infinite loop */
  for(;;)
  {
		//LED1  默认可以获取资源
		osSemaphoreWait(myBinarySem1Handle,portMAX_DELAY);//P 操作，获取信号量
		HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,GPIO_PIN_RESET);//点亮LED1
    osDelay(1000);
	  //释放LED2的资源
		osSemaphoreRelease(myBinarySem2Handle);//V 操作，释放信号量
  }
  /* USER CODE END StartTaskLED1 */
}

/* USER CODE BEGIN Header_StartTaskLED2 */
/**
* @brief Function implementing the myTaskLED2 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskLED2 */
void StartTaskLED2(void const * argument)
{
  /* USER CODE BEGIN StartTaskLED2 */
  /* Infinite loop */
  for(;;)
  {
		//LED2  默认不可以获取资源
		osSemaphoreWait(myBinarySem2Handle,portMAX_DELAY);//P 操作，获取信号量
		HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,GPIO_PIN_RESET);//点亮LED2
    osDelay(1000);
	  //释放BEEP的资源
		osSemaphoreRelease(myBinarySem3Handle);//V 操作，释放信号量
  }
  /* USER CODE END StartTaskLED2 */
}

/* USER CODE BEGIN Header_StartTaskBEEP */
/**
* @brief Function implementing the myTaskBEEP thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskBEEP */
void StartTaskBEEP(void const * argument)
{
  /* USER CODE BEGIN StartTaskBEEP */
  /* Infinite loop */
  for(;;)
  {
		//BEEP  默认不可以获取资源
		osSemaphoreWait(myBinarySem3Handle,portMAX_DELAY);//P 操作，获取信号量
		HAL_GPIO_WritePin(BEEP_GPIO_Port,BEEP_Pin,GPIO_PIN_SET);//BEEP 响
    osDelay(200);
		HAL_GPIO_WritePin(BEEP_GPIO_Port,BEEP_Pin,GPIO_PIN_RESET);//BEEP 不响
		HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,GPIO_PIN_SET);//LED1 熄灭
		HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,GPIO_PIN_SET);//LED2 熄灭
		osDelay(800);
	  //释放LED1的资源
		osSemaphoreRelease(myBinarySem1Handle);//V 操作，释放信号量
    osDelay(1);
  }
  /* USER CODE END StartTaskBEEP */
}

/* timerCallback function */
void timerCallback(void const * argument)
{
  /* USER CODE BEGIN timerCallback */
  printf("Timer Timeout\n");
  /* USER CODE END timerCallback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

