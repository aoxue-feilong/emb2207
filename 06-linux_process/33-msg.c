#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>

#define N 128

typedef struct msg
{
    long mtype;    /* message type, must be > 0 */
    char mtext[N]; /* message data */
} msg_t;

int main(int argc, char const *argv[])
{
    int ret;
    msg_t msg;
    memset(&msg, 0, sizeof(msg)); // 初始化结构体中所有的成员
    // 生成一个IPC 唯一key值
    // 因为shm 用了/home , 1 参数, 此时需要设置一个不一样的, 要不然会冲突
    key_t key = ftok("/home", 2);
    if (key < 0)
    {
        perror("ftok");
        exit(-1);
    }
    printf("key=%#x\n", key);
    int msgid = msgget(key,IPC_CREAT | 0664); // 如果不存在就创建, 存在就打开
    if (msgid < 0)
    {
        perror("msgget");
        exit(-1);
    }
    printf("msgid=%d\n", msgid);
    system("ipcs -q"); // 执行linux系统的一个命令 ,  查看系统的消息队列

    msg.mtype = 'A';//消息队列的标志
    strcpy(msg.mtext, "hello world");//向msg.mtext里面输入"hello world"
    ret = msgsnd(msgid, &msg, sizeof(msg.mtext), 0); // 往消息队列中发送一个消息
    if (ret < 0)
    {
        perror("msgsnd");
        exit(-1);
    }
    system("ipcs -q"); // 执行linux系统的一个命令 ,  查看系统的消息队列

    memset(&msg, 0, sizeof(msg));
    ret = msgrcv(msgid, &msg, sizeof(msg.mtext), 'A', 0);//从msgid队列中接受消息,0,表示直到消息发送完消息才返回
    if (ret < 0)                                         //A表示消息对列的标志
    {
        perror("msgrcv");
        exit(-1);
    }
    printf("msg.mtype=%c\n",(char)msg.mtype); 
    printf("msg.mtext=%s\n",msg.mtext);
    system("ipcs -q"); // 执行linux系统的一个命令 ,  查看系统的消息队列

    ret = msgctl(msgid, IPC_RMID, NULL); // 删除消息队列
    if (ret < 0)
    {
        perror("msgctl");
        exit(-1);
    }
    system("ipcs -q"); // 执行linux系统的一个命令 ,  查看系统的消息队列

    return 0;
}
