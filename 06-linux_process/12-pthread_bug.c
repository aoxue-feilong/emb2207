#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>


// 编译时要使用 -pthread 选项
// gcc  10-ptread.c -o 10-pthread -pthread

#define   N   128 
char buf[N+1] = {0}; 

void *pthread_handler1(void *arg)
{
    printf("arg1=%d\n", *(int *)arg);
    printf("pthread_handler1 is running\n");
    while (1)
    {
        memset(buf,'O',64); 
        usleep(1000) ; // 1000us 
        memset(buf+64,'O',64); // 保证最后一个元素是0  
        usleep(1000) ; // 1000us 
    }
}
void *pthread_handler2(void *arg)
{
    printf("arg2=%d\n", *(int *)arg);
    printf("pthread_handler2 is running\n");
    while (1)
    {
        memset(buf,'-',64); 
        usleep(1000) ; // 1000us 
        memset(buf+64,'-',64); // 保证最后一个元素是0  
        usleep(1000) ; // 1000us 
    }
}
void *pthread_handler3(void *arg)
{
    printf("arg3=%d\n", *(int *)arg);
    printf("pthread_handler3 is running\n");
    while (1)
    {
        printf("buf=%s\n",buf); 
        sleep(1);
    }
}

int main(int argc, char const *argv[])
{
    pthread_t tid1,tid2,tid3; // thread id
    int data1 = 100,data2=200,data3=300;
    // 创建子线程1
    int ret = pthread_create(&tid1, NULL, pthread_handler1, (void *)&data1);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    // 创建子线程2
    ret = pthread_create(&tid2, NULL, pthread_handler2, (void *)&data2);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    // 创建子线程3
    ret = pthread_create(&tid3, NULL, pthread_handler3, (void *)&data3);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    printf("tid1=%ld\n",tid1);
    printf("tid2=%ld\n",tid2);
    printf("tid3=%ld\n",tid3);
    printf("main pthread is running\n"); 
    while(1)
    {
        printf("buf=%s\n",buf); 
        sleep(1);
    }
    return 0;
}