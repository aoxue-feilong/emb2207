#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

pid_t pid_child1, pid_child2;

void parent_sighandler(int signum)
{
    static int count = 0;
    if (signum == SIGINT)//捕捉来的SIGINT信号
    {
        kill(pid_child1, SIGUSR1); // 给子进程1 发送SIGUSR1信号
        kill(pid_child2, SIGUSR1); // 给子进程2 发送SIGUSR1信号
    }
    else if (signum == SIGCHLD)//捕捉来的SIGCHLD信号
    {
        count++;
        if (count == 2) // 表示收到了2个信号后, 再退出父进程 
        {
            printf("Parent process exit!\n");
            exit(0);
        }
    }
}
void child1_sighandler(int signum)
{
    printf("Child process 1 is killed by parent!\n");
    exit(0); // 子进程退出后, 发送SIGCHLD信号给父进程
}
void child2_sighandler(int signum)
{
    sleep(1);
    printf("Child process 2 is killed by parent!\n");
    exit(0); // 子进程退出后, 发送SIGCHLD信号给父进程
}

int main(int argc, char const *argv[])
{
    pid_t pid = fork();
    if (pid < 0) // 错误
    {
        perror("fork");
        exit(-1);
    }
    else if (pid == 0) // 表示的是子进程1
    {
        //pid_child1=getpid();
        signal(SIGINT, SIG_IGN);            // 设置SIG_IGN 忽略信号
        signal(SIGUSR1, child1_sighandler); // 设置SIGUSR1 的信号处理函数
        
        while (1)
        {
            sleep(1);
        }
    }
    else // 表示父进程 pid > 0 q
    {
        pid_child1 = pid; // 获取子进程1 的进程号
        pid = fork();
        if (pid < 0) // 错误
        {
            perror("fork");
            exit(-1);
        }
        else if (pid == 0) // 表示的是子进程2
        {
            //pid_child2=getpid();
            signal(SIGINT, SIG_IGN);            // 设置SIG_IGN 忽略信号
            signal(SIGUSR1, child2_sighandler); // 设置SIGUSR1 的信号处理函数
           
            while (1)
            {
                sleep(1);
            }
        }
        else//pid_t定义的类型都是进程号类型，fork函数后父进程返回子进程号，并将子进程号给了pid
        {   //子进程返回0

            pid_child2 = pid;                  // 获取子进程2 的进程号
            signal(SIGINT, parent_sighandler); // 设置SIGINT 的信号处理函数

            // 默认父进程对SIGCHLD是忽略的
            signal(SIGCHLD, parent_sighandler); // 设置SIGCHLD 的信号处理函数
            while (1)                           // 父进程
            {
                sleep(1);
            }
        }
    }
    return 0;
}
