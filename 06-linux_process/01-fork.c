#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
int main(int argc, char const *argv[])
{
    pid_t pid = fork();//创建一个进程
    if (pid < 0) // 错误
    {
        perror("fork");
        exit(-1);
    }
    else if (pid == 0) // 表示的是子进程
    {
        printf("child :pid =%d\n", getpid());  // 获取当前进程的进程号
        printf("child :ppid=%d\n", getppid()); // 获取父进程的进程号
        while (1)
        {
            printf("child :process is running\n");
            sleep(1);
        }
    }
    else // 表示父进程 pid > 0 q
    {
        printf("parent:pid =%d\n", getpid());  // 获取当前进程的进程号
        printf("parent:ppid=%d\n", getppid()); // 获取父进程的进程号
        while (1)
        {
            printf("parent:process is running\n");
            sleep(1);
        }
    }
    return 0;
}