#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <signal.h>
void sighandler(int signum)
{
    printf("signum=%d\n", signum);
}

int main(int argc, char const *argv[])
{
    // 设置信号的处理函数
    signal(SIGINT, sighandler);  // SIGINT 是ctrl + c 发出的信号
    signal(SIGQUIT, sighandler); // SIGINT 是ctrl + \ 发出的信号
    signal(SIGUSR1, sighandler); // 可以使用kill 命令发出信号
    signal(SIGTSTP, sighandler); // SIGTSTP 是ctrl + z 发出的信号

    while (1)
    {
        sleep(1);
    }

    return 0;
}