#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/stat.h>

int main(int argc, char const *argv[])
{
    int count = 100; // 定义一个局部变量

    pid_t pid = fork();
    if (pid < 0) // 错误
    {
        perror("fork");
        exit(-1);
    }
    else if (pid > 0) // 父进程   , 第1步: 创建子进程, 父进程退出, 防止产生僵尸进程
    {
        exit(0);
    }
    else // 子进程
    {
        setsid();      // 第2步: 创建一个新的会话组
        chdir("/tmp"); // 第3步: 切换到根目录下的tmp目录
        umask(0);      // 第4步: ~0 = 0777 , 不对文件权限进行任何屏蔽
        for (int i = 0; i < getdtablesize(); i++)
        {
            close(i); // 第5步: 关闭已经打开的文件
        }
        while (1)
        {
            system("date >>log"); // 可以调用shell中的任何一个命令
            sleep(1);
        }

        //
    }
    return 0;
}