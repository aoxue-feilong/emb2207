#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
    int count = 100; // 定义一个局部变量

    pid_t pid = fork();
    if (pid < 0) // 错误
    {
        perror("fork");
        exit(-1);
    }
    else if (pid == 0) // 表示的是子进程 , 子进程调用myls程序
    {
        printf("child :pid =%d\n", getpid());  // 获取当前进程的进程号
        printf("child :ppid=%d\n", getppid()); // 获取父进程的进程号
        execl("/home/linux/work/emb2207/05-linuxio/30-myls",
              "30-myls", "-l", "/home/linux",NULL); 
        exit(0); 
    }
    else // 表示父进程 pid > 0 q
    {
        printf("parent:pid =%d\n", getpid());  // 获取当前进程的进程号
        printf("parent:ppid=%d\n", getppid()); // 获取父进程的进程号
        while (1)
        {
            count--;
            printf("parent:process is running:%d\n", count);
            sleep(1);
        }
    }
    return 0;
}