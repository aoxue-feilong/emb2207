#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define N 128

int main(int argc, char const *argv[])
{
    // 创建1个管道
    int fd[2];
    char buf[N] = {0};
    int ret = pipe(fd);
    if (ret < 0)
    {
        perror("pipe");
        exit(-1);
    }
    printf("fd[0]=%d\n", fd[0]); // 读端
    printf("fd[1]=%d\n", fd[1]); // 写端

    // fork 后 , 因为管道被创建在内核中, 并不会创建克隆
    // 但是 fd[0] 会变成2个 , fd[1] 也会变成2个 , 把多余fd[0]和fd[1]关闭
    pid_t pid = fork();
    if (pid < 0) // 错误
    {
        perror("fork");
        exit(-1);
    }
    else if (pid == 0) // 表示的是子进程
    {
        close(fd[0]);                          // 关闭接收端
        printf("child :pid =%d\n", getpid());  // 获取当前进程的进程号
        printf("child :ppid=%d\n", getppid()); // 获取父进程的进程号
        while (1)
        {
            printf(">:");
            fgets(buf, N, stdin); // 从终端读数据
            ret = write(fd[1], buf, N);
            if (ret < 0)
            {
                perror("write");
                exit(-1);
            }
        }
    }
    else // 表示父进程 pid > 0 q
    {
        close(fd[1]);                          // 关闭发送端
        printf("parent:pid =%d\n", getpid());  // 获取当前进程的进程号
        printf("parent:ppid=%d\n", getppid()); // 获取父进程的进程号
        while (1)
        {
            ret = read(fd[0], buf, N);
            if (ret < 0)
            {
                perror("read");
                exit(-1);
            }
            printf("buf=%s\n",buf); 
            memset(buf,0,N); 
        }
    }
    return 0;
}