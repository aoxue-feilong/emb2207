#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#define N 128

int main(int argc, char const *argv[])
{
    char buf[N] = {0};
    int ret, fd;
    if (argc != 2)//判断参数
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./20-fifo_write filename)\n");
        exit(-1);
    }
    // 测试这个文件是否存在  , 存在返回为0 , 错误返回-1
    // 文件存在的话, 就不需要创建管道
    // 文件不存在则创建管道文件
    if (access(argv[1], F_OK) < 0) // 文件不存在则创建管道，文件存在不管
    {
        ret = mkfifo(argv[1], 0664);//创建有名管道，第一个参数：创建的管道文件，第二个参数：文件权限
        if (ret < 0)
        {
            perror("mkfifo");
            exit(-1);
        }
    }
    fd = open(argv[1], O_RDWR);//打开这个管道文件
    if (fd < 0) // file descriptor（文件描述符）
    {
        perror("open");
        exit(-1);
    }
    printf("fd=%d\n",fd); 
    while (1)
    {
        printf(">:");
        fgets(buf, N, stdin);//把buf里面的内容输出到终端里面，stdin标准输出
        buf[strlen(buf) - 1] = '\0'; // 去除'\n';strlen字符串计算函数，将最后一个字符赋值为\0,将输入换行符，换成\0
        ret = write(fd, buf, N); //将buf里面的字符串，写到文件里面
        if (ret < 0)
        {
            perror("write");
            exit(-1);
        }
        if(strncmp(buf,"quit",4) == 0) break ;//strcmp字符串比较函数，判断当输入quit时，程序结束
    }
    close(fd); 

    return 0;
}