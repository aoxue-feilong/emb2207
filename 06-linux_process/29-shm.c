#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define N 128
int main(int argc, char const *argv[])
{
    //生成一个pc唯一的key值
    key_t key = ftok("/home", 1);
    if (key < 0)
    {
        perror("ftok");
        exit(-1);
    }
    printf("key=%#x\n", key);
    int shmid = shmget(key, N, IPC_CREAT | 0664); //如果不存在就创建，存在就打开
    if (shmid < 0)
    {
        perror("shmid");
        exit(-1);
    }
    system("ipcs -m"); //执行linux系统命令，查看系统的共享内存

    void *shmaddr = shmat(shmid, NULL, 0); //把共享内存映射到进程内，进程就可以访问这段内存
    if (shmaddr == (void *)-1)
    {
        perror("shmadder");
        exit(-1);
    }
    system("ipcs -m");        //执行linux系统命令，查看系统的共享内存
    memset(shmaddr, 'O', N);  //对共享内存进行操作
    int ret = shmdt(shmaddr); //去掉映射，不能在访问这个共享内存
    if (ret < 0)
    {
        perror("shmdt");
        exit(-1);
    }
    system("ipcs -m");                    //执行linux系统命令，查看系统的共享内存
    ret = shmctl(shmid, IPC_RMID, NULL); //删除共享内存
    if (ret < 0)
    {
        perror("shmctl");
        exit(-1);
    }
    system("ipcs -m"); //执行linux系统命令，查看系统的共享内存
    return 0;
}
