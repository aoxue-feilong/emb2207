#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#define N 128

int main(int argc, char const *argv[])
{
    char buf[N] = {0};
    int ret, fd;
    if (argc != 2)
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./24-fifo_buffer_size filename)\n");
        exit(-1);
    }

    // 测试这个文件是否存在  , 存在返回为0 , 错误返回-1
    // 文件存在的话, 就不需要创建管道
    // 文件不存在则创建管道文件
    if (access(argv[1], F_OK) < 0) // 文件不存在
    {
        ret = mkfifo(argv[1], 0664);
        if (ret < 0)
        {
            perror("mkfifo");
            exit(-1);
        }
    }
    fd = open(argv[1], O_RDWR);
    if (fd < 0) // file descriptor
    {
        perror("open");
        exit(-1);
    }
    printf("fd=%d\n",fd); 

    // 管道就是一个队列, 按照先进先出的原则 
    // 可以一直往管道内写入数据, 不去读, 这个时候可以测出管道最多可以存放多少字节的数据
    int size=0;
    while(1)
    {
        ret = write(fd,buf,1);
        if(ret < 0)
        {
            perror("write"); 
            exit(-1);
        } 
        size ++; 
        printf("size=%d (%d KB)\n",size,size/1024);
    }

   
    return 0;
}
