#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

pid_t pid_child;

void parent_sighandler(int signum)
{
    if (signum == SIGUSR1)
    {
        printf("let’s gogogo\n");
    }
    else if (signum == SIGUSR2)
    {
        printf("stop the bus\n");
    }
    else if (signum == SIGTSTP)
    {
        kill(pid_child, SIGUSR1); // 给子进程(售票员)发送 SIGUSR1信号
    }
}
void child_sighandler(int signum)
{
    if (signum == SIGINT)
    {
        kill(getppid(), SIGUSR1); // 给父进程(司机)发送 SIGUSR1信号
    }
    else if (signum == SIGQUIT)
    {
        kill(getppid(), SIGUSR2); // 给父进程(司机)发送 SIGUSR2信号
    }
    else if (signum == SIGUSR1)
    {
        printf("please get off the bus\n");
        kill(pid_child, SIGKILL); // 杀死子进程后, 父进程退出, 避免程序逻辑出问题(避免出现孤儿进程，下次运行会出错)
        exit(0);                  // 父进程退出
    }
}
int main(int argc, char const *argv[])
{
    pid_t pid = fork();
    if (pid < 0) // 错误
    {
        perror("fork");
        exit(-1);
    }
    else if (pid == 0) // 表示的是子进程 售票员
    {
        signal(SIGINT, child_sighandler);  // 设置SIGINT 的信号处理函数
        signal(SIGQUIT, child_sighandler); // 设置SIGQUIT 的信号处理函数
        signal(SIGTSTP, SIG_IGN);          // 设置SIGTSTP 忽略信号
        signal(SIGUSR1, child_sighandler); // 设置SIGUSR1 的信号处理函数

        while (1)
        {
            sleep(1);
        }
    }
    else // 表示父进程 pid > 0    司机
    {
        pid_child = pid;
        signal(SIGINT, SIG_IGN);            // 设置SIGINT 忽略信号
        signal(SIGUSR1, parent_sighandler); // 设置SIGUSR1 的信号处理函数
        signal(SIGQUIT, SIG_IGN);           // 设置SIGQUIT 忽略信号
        signal(SIGUSR2, parent_sighandler); // 设置SIGUSR2 的信号处理函数
        signal(SIGTSTP, parent_sighandler); // 设置SIGTSTP 的信号处理函数

        while (1) // 父进程
        {
            sleep(1);
        }
    }
    return 0;
}