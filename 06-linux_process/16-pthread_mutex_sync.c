#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
// 编译时要使用 -pthread 选项
// gcc  10-ptread.c -o 10-pthread -pthread

#define N 128
char buf[N + 1] = {0};
pthread_mutex_t mutex_w1, mutex_w2, mutex_r1, mutex_r2;

void *pthread_handler1(void *arg)
{
    printf("arg1=%d\n", *(int *)arg);
    printf("pthread_handler1 is running\n");
    while (1)
    {
        pthread_mutex_lock(&mutex_w1); // P操作
        memset(buf, 'O', 64);
        usleep(1000);                    // 1000us
        memset(buf + 64, 'O', 64);       // 保证最后一个元素是0
        usleep(1000);                    // 1000us
        pthread_mutex_unlock(&mutex_r1); // V操作
    }
}
void *pthread_handler2(void *arg)
{
    printf("arg2=%d\n", *(int *)arg);
    printf("pthread_handler2 is running\n");
    while (1)
    {
        pthread_mutex_lock(&mutex_w2); // P操作
        memset(buf, '-', 64);
        usleep(1000);                    // 1000us
        memset(buf + 64, '-', 64);       // 保证最后一个元素是0
        usleep(1000);                    // 1000us
        pthread_mutex_unlock(&mutex_r2); // V操作
    }
}
void *pthread_handler3(void *arg)
{
    printf("arg3=%d\n", *(int *)arg);
    printf("pthread_handler3 is running\n");
    while (1)
    {
        pthread_mutex_lock(&mutex_r1); // P操作
        printf("buf=%s\n", buf);
        pthread_mutex_unlock(&mutex_w2); // V操作

        sleep(1);
    }
}

int main(int argc, char const *argv[])
{
   pthread_t tid1,tid2,tid3;//pthread id 线程 id
    int data1=100,data2=200,data3=300;
    int ret;
    ret = pthread_mutex_init(&mutex_w1,NULL);
    if (ret != 0)
    {
        printf("pthread_mutex_init error\n");
        exit(-1);
    }

    ret = pthread_mutex_init(&mutex_w2,NULL);
    pthread_mutex_lock(&mutex_w2);//上锁
    if (ret != 0)
    {
        printf("pthread_mutex_init error\n");
        exit(-1);
    }

    ret = pthread_mutex_init(&mutex_r1,NULL);
    pthread_mutex_lock(&mutex_r1);//上锁
    if (ret != 0)
    {
        printf("pthread_mutex_init error\n");
        exit(-1);
    }

    ret = pthread_mutex_init(&mutex_r2,NULL);
    pthread_mutex_lock(&mutex_r2);//上锁
    if (ret != 0)
    {
        printf("pthread_mutex_init error\n");
        exit(-1);
    }
    // 创建子线程1
    ret = pthread_create(&tid1, NULL, pthread_handler1, (void *)&data1);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    // 创建子线程2
    ret = pthread_create(&tid2, NULL, pthread_handler2, (void *)&data2);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    // 创建子线程3
    ret = pthread_create(&tid3, NULL, pthread_handler3, (void *)&data3);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    printf("tid1=%ld\n", tid1);
    printf("tid2=%ld\n", tid2);
    printf("tid3=%ld\n", tid3);
    printf("main pthread is running\n");
    while (1)
    {
        pthread_mutex_lock(&mutex_r2); // P操作
        printf("buf=%s\n", buf);
        pthread_mutex_unlock(&mutex_w1); // V操作
        sleep(1);
    }
    return 0;
}