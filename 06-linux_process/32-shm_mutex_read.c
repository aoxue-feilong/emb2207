#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <fcntl.h>

#define N 128

int main(int argc, char const *argv[])
{

    sem_t *sem_w1;
    sem_t *sem_w2;
    sem_t *sem_r1;
    sem_t *sem_r2;


    sem_w1=sem_open("sem_w1",O_RDWR|O_CREAT,0664,1);
    if(sem_w1 == SEM_FAILED)
    {
        perror("sem_open");
        exit(-1);
    }
    sem_w2=sem_open("sem_w2",O_RDWR|O_CREAT,0664,0);
    if(sem_w2 == SEM_FAILED)
    {
        perror("sem_open");
        exit(-1);
    }
    sem_r1=sem_open("sem_r1",O_RDWR|O_CREAT,0664,0);
    if(sem_r1 == SEM_FAILED)
    {
        perror("sem_open");
        exit(-1);
    }
    sem_r2=sem_open("sem_r2",O_RDWR|O_CREAT,0664,0);
    if(sem_r2 == SEM_FAILED)
    {
        perror("sem_open");
        exit(-1);
    }
    int value;
    sem_getvalue(sem_w1,&value);
    printf("sem_w1value=%d\n",value);
    sem_getvalue(sem_w2,&value);
    printf("sem_w2value=%d\n",value);
    sem_getvalue(sem_r1,&value);
    printf("sem_r1value=%d\n",value);
    sem_getvalue(sem_r2,&value);
    printf("sem_r2value=%d\n",value);
    //先生成一个IPC 唯一key值
    key_t key = ftok("/home",1);
    if(key < 0)
    {
        perror("ftok");
        exit(-1);
    }
    printf("key=%#x\n",key);
    int shmid = shmget(key,N,IPC_CREAT|0664);//如果不存在就创建，存在就打开
    if(shmid < 0)
    {
        perror("shmget");
        exit(-1);
    }
    printf("shmid=%d\n",shmid);
    system("ipcs _m");// 执行linux系统的一个命令 ,查看系统的共享内存

    //映射内存
    void * shmaddr = shmat(shmid,NULL,0);
    if(shmaddr == (void * )-1)
    {
        perror("shmat");
        exit(-1);
    }
    system("ipcs _m");// 执行linux系统的一个命令 ,查看系统的共享内存
  
    pid_t pid = fork();
    if(pid < 0)
    {
        perror("fork");
        exit(-1);
    }
    else if(pid == 0)//子进程
    {
        while(1)
        {
          sem_wait(sem_r1);//p操作,申请信号量
          printf("buf=%s\n",(char * )shmaddr);
          sleep(1);
          sem_post(sem_w2);//释放信号量

        }
    }
    else
    {
        while(1)
        {
           sem_wait(sem_r2);//p操作,申请信号量
          printf("buf=%s\n",(char * )shmaddr);
          sleep(1);
          sem_post(sem_w1);//释放信号量
        }
    }
    int ret = shmdt(shmaddr);//取消共享内存的映射关系， 去掉后就不能访问这块共享内存了
    if(ret<0)
    {
        perror("shmdt");
        exit(-1);
    }
    system("ipcs _m");//查看系统的共享内存
    ret = shmctl(shmid,IPC_RMID,NULL) ; // 删除共享内存
    if(ret < 0)
    {
        perror("shmctl");
        exit(-1); 
    }
    system("ipcs -m") ; // 执行linux系统的一个命令 ,  查看系统的共享内存
    
    return 0;
}
