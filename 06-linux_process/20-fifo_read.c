#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#define N 128

int main(int argc, char const *argv[])
{
    char buf[N] = {0};
    int ret, fd;
    if (argc != 2) //判断参数
    {
        fprintf(stderr, "错误:运行程序时请带入参数(./20-fifo_write filename)\n");
        exit(-1);
    }
    // 测试这个文件是否存在  , 存在返回为0 , 错误返回-1
    // 文件存在的话, 就不需要创建管道
    // 文件不存在则创建管道文件
    if (access(argv[1], F_OK) < 0) // 文件不存在，access：文件存在返回值为0，不存在返回-1
    {
        ret = mkfifo(argv[1], 0664);
        if (ret < 0)
        {
            perror("mkfifo");
            exit(-1);
        }
    }
    fd = open(argv[1], O_RDWR);//打开文件，并设置权限
    if (fd < 0) // file descriptor文件标识符
    {
        perror("open");
        exit(-1);
    }
    printf("fd=%d\n",fd); 
    while (1)
    {
        ret = read(fd, buf, N);//buf,写到fd里面
        if (ret < 0)
        {
            perror("read");
            exit(-1);
        }
        if(strncmp(buf,"quit",4) == 0) break ; 
        printf("buf=%s\n",buf); 
    }
    close(fd); 

    return 0;
}