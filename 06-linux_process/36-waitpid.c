#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h> 


void sighandler(int signum)
{
    pid_t pid = waitpid(-1,NULL,0); 
    printf("回收子进程为:pid=%d\n",pid); 
}


int main(int argc, char const *argv[])
{
    // 这个程序使用SIGCHLD , 会发生信号的丢失 , 有bug , 解决思路使用实时信号
    //signal(SIGCHLD,sighandler) ; // 设置子进程退出时的信号捕捉

    // 设置一个带缓存的信号 
    signal(SIGRTMIN,sighandler) ; // 设置子进程退出时信号捕捉
    
    
    for (int i = 0; i < 10; i++)
    {
        if (fork() == 0) // 子进程
        {
            printf("child pid=%d\n",getpid()); 
            sleep(10);
            kill(getppid(),SIGRTMIN) ; // 给父进程发送一个信号SIGRTMIN
            exit(0);
        }
    }
    printf("main process is running\n"); 
    while(1)
    {
        
        sleep(1); 
    }

    return 0;
}
