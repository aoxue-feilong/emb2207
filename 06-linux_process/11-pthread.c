#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

// 编译时要使用 -pthread 选项
// gcc  10-ptread.c -o 10-pthread -pthread

void *pthread_handler1(void *arg)
{
    static int retval = 0; // 默认成功
    printf("arg1=%d\n", *(int *)arg);
    printf("pthread_handler1 is running\n");
    sleep(10);
    pthread_exit(&retval);
}
void *pthread_handler2(void *arg)
{
    static int retval = 0; // 默认成功
    printf("arg2=%d\n", *(int *)arg);
    printf("pthread_handler2 is running\n");
    sleep(15);
    retval = -1;
    pthread_exit(&retval);
}
void *pthread_handler3(void *arg)
{
    static int retval = 0; // 默认成功
    printf("arg3=%d\n", *(int *)arg);
    printf("pthread_handler3 is running\n");
    sleep(20);
    printf("pthread_handler3 is exiting\n");
    retval = 1;
    pthread_exit(&retval);
}

int main(int argc, char const *argv[])
{
    pthread_t tid1, tid2, tid3; // thread id
    int data1 = 100, data2 = 200, data3 = 300;
    // 创建子线程1
    int ret = pthread_create(&tid1, NULL, pthread_handler1, (void *)&data1);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    // 创建子线程2
    ret = pthread_create(&tid2, NULL, pthread_handler2, (void *)&data2);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    // 创建子线程3
    ret = pthread_create(&tid3, NULL, pthread_handler3, (void *)&data3);
    if (ret != 0)
    {
        printf("pthread_create error\n");
        exit(-1);
    }
    printf("tid1=%ld\n", tid1);
    printf("tid2=%ld\n", tid2);
    printf("tid3=%ld\n", tid3);
    pthread_cancel(tid3); // 取消线程3
    void *retval;
    pthread_join(tid1, &retval); // 等待线程完成 , 获取返回值状态
    printf("pthread1 retval=%d\n", *(int *)retval);

    pthread_join(tid2, &retval); // 等待线程完成 , 获取返回值状态
    printf("pthread2 retva2=%d\n", *(int *)retval);
    while (1)
    {
        printf("main pthread is running\n");
        sleep(1);
    }
    return 0;
}