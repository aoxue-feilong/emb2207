#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#define N 128

int main(int argc, char const *argv[])
{
    char buf[N] = {0};
    int ret;
    int fd[2];
    ret = pipe(fd);
    if (ret < 0)
    {
        perror("pipe");
        exit(-1);
    }
    printf("fd[0]=%d\n", fd[0]); // 读端
    printf("fd=%d\n", fd[1]); // 写端
/*********************************************************************/

    // 1. 管道就是一个队列, 按照先进先出的原则
    // 2. 读端关闭, 写管道, 管道崩裂
    #if 0
    close(fd[0]) ; // 关闭读端 
    ret = write(fd[1],buf,N); // 写管道, 管道崩裂
    if(ret < 0)
    {
        perror("write");
        exit(-1); 
    }
    #endif 

    printf("test is ok\n");
   
    return 0;
}
