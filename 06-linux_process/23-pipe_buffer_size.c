#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define N 128

int main(int argc, char const *argv[])
{
    // 创建1个管道
    int fd[2];
    char buf[N] = {0};
    int ret = pipe(fd);
    if (ret < 0)
    {
        perror("pipe");
        exit(-1);
    }
    printf("fd[0]=%d\n", fd[0]); // 读端
    printf("fd[1]=%d\n", fd[1]); // 写端

    // 管道就是一个队列, 按照先进先出的原则 
    // 可以一直往管道内写入数据, 不去读, 这个时候可以测出管道最多可以存放多少字节的数据
    int size=0;
    while(1)
    {
        ret = write(fd[1],buf,1);
        if(ret < 0)
        {
            perror("write"); 
            exit(-1);
        } 
        size ++; 
        printf("size=%d (%d KB)\n",size,size/1024);
    }

   
    return 0;
}
